<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Ajax extends Controller {

	function Ajax ()
	{
		parent::Controller();	
		$this->load->library('flexigrid');
		$this->load->model('function2');
		$this->load->model('m_user');
		$this->load->model('m_report');
		$this->load->model('m_pengajar');
		$this->load->model('m_kabupaten');
		$this->load->model('m_provinsi');
		$this->load->model('m_angkatan');
		$this->load->model('m_golongan');
		$this->load->model('m_jenis_diklat');
		$this->load->model('m_mata_diklat');
		$this->load->model('m_diklat');
		$this->load->model('m_kategori_pengajar');
		$this->load->model('m_lokasi_diklat');
		$this->load->model('m_instansi');
		$this->load->model('m_peserta');
		$this->load->model('m_pelaksanaan');
	}
	
	function ajax_pelaksanaan()
	{
		$model_default = "m_pelaksanaan";
		$page_default = "c_pelaksanaan";
		$valid_fields = array('id'
							 ,'dt_from'
							 ,'dt_to'
							 ,'lokasi'
							 ,'waktu'
							 ,'diklat'
							 ,'angkatan'
							 ,'tahun'
		);

		$this->flexigrid->validate_post('id','asc',$valid_fields);
		$records = $this->$model_default->get_all_data();
		$this->output->set_header($this->config->item('json_header'));
		$record_items = array();
		$no=1;
		foreach ($records['records']->result() as $row)
		{
			if ($this->session->userdata('access') == "1") {
				$record_items[] = array($row->id
										,$no
										,"<a href=".$page_default."/detail/".$row->id."><img border=0 width=15 height=15 title='Show Detail' src=".$this->config->item('base_url')."public/images/edit.png></a>
										 <a href=".$page_default."/delete/".$row->id."><img border=0 width=15 height=15 title='Hapus' src=".$this->config->item('base_url')."public/images/delete.png></a>"
										,"<a href=c_pelaksanaan_view/detail/".$row->id."><img border=0 width=15 height=15 title='Show Detail' src=".$this->config->item('base_url')."public/images/magnifier.png></a>"
										,$row->dt_from." s/d ".$row->dt_to
										,$this->function2->get_data_detail('tbl_lokasi_diklat', 'name', 'name', $row->lokasi)
										,$row->waktu
										,$this->function2->get_data_detail('tbl_diklat', 'name', 'name', $row->diklat)
										,$this->function2->get_data_detail('tbl_angkatan', 'name', 'name', $row->angkatan)
										,$row->tahun
				);
			} else {
				$record_items[] = array($row->id
										,$no
										,"<a href=c_pelaksanaan_view/detail/".$row->id."><img border=0 width=15 height=15 title='Show Detail' src=".$this->config->item('base_url')."public/images/magnifier.png></a>"
										,$row->dt_from." s/d ".$row->dt_to
										,$this->function2->get_data_detail('tbl_lokasi_diklat', 'name', 'name', $row->lokasi)
										,$row->waktu
										,$this->function2->get_data_detail('tbl_diklat', 'name', 'name', $row->diklat)
										,$this->function2->get_data_detail('tbl_angkatan', 'name', 'name', $row->angkatan)
										,$row->tahun
				);
			}
			$no++;
		}

		$this->output->set_output($this->flexigrid->json_build($records['record_count'],$record_items));
	}	

	function ajax_instansi()
	{
		$model_default = "m_instansi";
		$page_default = "c_instansi";
		$valid_fields = array('id'
							 ,'name'
							 ,'alamat'
							 ,'provinsi'
							 ,'kabupaten'
							 ,'kode_pos'
							 ,'telp_kode_daerah'
							 ,'no_telp'
							 ,'fax_kode_daerah'
							 ,'no_fax'
							 ,'website'
							 ,'email'
		);

		$this->flexigrid->validate_post('id','asc',$valid_fields);
		$records = $this->$model_default->get_all_data();
		$this->output->set_header($this->config->item('json_header'));
		$record_items = array();
		$no=1;
		foreach ($records['records']->result() as $row)
		{
			if ($this->session->userdata('access') == "1") {
				$record_items[] = array($row->id
										,$no
										,"<a href=".$page_default."/update/".$row->id."><img border=0 width=15 height=15 title='Rubah' src=".$this->config->item('base_url')."public/images/edit.png></a>
										 <a href=".$page_default."/delete/".$row->id."><img border=0 width=15 height=15 title='Hapus' src=".$this->config->item('base_url')."public/images/delete.png></a>"
										,"<a href=".$page_default."/detail/".$row->id."><img border=0 width=15 height=15 title='Show Detail' src=".$this->config->item('base_url')."public/images/magnifier.png></a>"
										,$row->name
										,$row->alamat
										,$this->function2->get_data_detail('tbl_provinsi', 'name', 'name', $row->provinsi)
										,$this->function2->get_data_detail('tbl_kabupaten', 'name', 'name', $row->kabupaten)
										,$row->kode_pos
										,$row->telp_kode_daerah."-".$row->no_telp
										,$row->fax_kode_daerah."-".$row->no_fax
										,$row->website
										,$row->email
				);
			} else {
				$record_items[] = array($row->id
										,$no
										,"<a href=".$page_default."/detail/".$row->id."><img border=0 width=15 height=15 title='Show Detail' src=".$this->config->item('base_url')."public/images/magnifier.png></a>"
										,$row->name
										,$row->alamat
										,$this->function2->get_data_detail('tbl_provinsi', 'name', 'name', $row->provinsi)
										,$this->function2->get_data_detail('tbl_kabupaten', 'name', 'name', $row->kabupaten)
										,$row->kode_pos
										,$row->telp_kode_daerah."-".$row->no_telp
										,$row->fax_kode_daerah."-".$row->no_fax
										,$row->website
										,$row->email
				);
			}
			$no++;
		}

		$this->output->set_output($this->flexigrid->json_build($records['record_count'],$record_items));
	}

	function ajax_lokasi_diklat()
	{
		$model_default = "m_lokasi_diklat";
		$page_default = "c_lokasi_diklat";
		$valid_fields = array('id'
							 ,'name'
							 ,'alamat'
							 ,'provinsi'
							 ,'kabupaten'
							 ,'kode_pos'
							 ,'telp_kode_daerah'
							 ,'no_telp'
							 ,'fax_kode_daerah'
							 ,'no_fax'
							 ,'website'
							 ,'email'
		);

		$this->flexigrid->validate_post('id','asc',$valid_fields);
		$records = $this->$model_default->get_all_data();
		$this->output->set_header($this->config->item('json_header'));
		$record_items = array();
		$no=1;
		foreach ($records['records']->result() as $row)
		{
			if ($this->session->userdata('access') == "1") {
				$record_items[] = array($row->id
										,$no
										,"<a href=".$page_default."/update/".$row->id."><img border=0 width=15 height=15 title='Rubah' src=".$this->config->item('base_url')."public/images/edit.png></a>
										 <a href=".$page_default."/delete/".$row->id."><img border=0 width=15 height=15 title='Hapus' src=".$this->config->item('base_url')."public/images/delete.png></a>"
										,"<a href=".$page_default."/detail/".$row->id."><img border=0 width=15 height=15 title='Show Detail' src=".$this->config->item('base_url')."public/images/magnifier.png></a>"
										,$row->name
										,$row->alamat
										,$this->function2->get_data_detail('tbl_provinsi', 'name', 'name', $row->provinsi)
										,$this->function2->get_data_detail('tbl_kabupaten', 'name', 'name', $row->kabupaten)
										,$row->kode_pos
										,$row->telp_kode_daerah."-".$row->no_telp
										,$row->fax_kode_daerah."-".$row->no_fax
										,$row->website
										,$row->email
				);
			} else {
				$record_items[] = array($row->id
										,$no
										,"<a href=".$page_default."/detail/".$row->id."><img border=0 width=15 height=15 title='Show Detail' src=".$this->config->item('base_url')."public/images/magnifier.png></a>"
										,$row->name
										,$row->alamat
										,$this->function2->get_data_detail('tbl_provinsi', 'name', 'name', $row->provinsi)
										,$this->function2->get_data_detail('tbl_kabupaten', 'name', 'name', $row->kabupaten)
										,$row->kode_pos
										,$row->telp_kode_daerah."-".$row->no_telp
										,$row->fax_kode_daerah."-".$row->no_fax
										,$row->website
										,$row->email
				);
			}
			$no++;
		}

		$this->output->set_output($this->flexigrid->json_build($records['record_count'],$record_items));
	}

	function ajax_kategori_pengajar()
	{
		$model_default = "m_kategori_pengajar";
		$page_default = "c_kategori_pengajar";
		$valid_fields = array('id'
							 ,'name'
		);

		$this->flexigrid->validate_post('id','asc',$valid_fields);
		$records = $this->$model_default->get_all_data();
		$this->output->set_header($this->config->item('json_header'));
		$record_items = array();
		$no=1;
		foreach ($records['records']->result() as $row)
		{
			if ($this->session->userdata('access') == "1") {
				$record_items[] = array($row->id
										,$no
										,"<a href=".$page_default."/update/".$row->id."><img border=0 width=15 height=15 title='Rubah' src=".$this->config->item('base_url')."public/images/edit.png></a>
										 <a href=".$page_default."/delete/".$row->id."><img border=0 width=15 height=15 title='Hapus' src=".$this->config->item('base_url')."public/images/delete.png></a>"
										,$row->name
				);
			} else {
				$record_items[] = array($row->id
										,$no
										,$row->name
				);
			}
			$no++;
		}

		$this->output->set_output($this->flexigrid->json_build($records['record_count'],$record_items));
	}

	function ajax_diklat()
	{
		$model_default = "m_diklat";
		$page_default = "c_diklat";
		$valid_fields = array('id'
							 ,'name'
							 ,'jenis'
		);

		$this->flexigrid->validate_post('id','asc',$valid_fields);
		$records = $this->$model_default->get_all_data();
		$this->output->set_header($this->config->item('json_header'));
		$record_items = array();
		$no=1;
		foreach ($records['records']->result() as $row)
		{
			if ($this->session->userdata('access') == "1") {
				$record_items[] = array($row->id
										,$no
										,"<a href=".$page_default."/update/".$row->id."><img border=0 width=15 height=15 title='Rubah' src=".$this->config->item('base_url')."public/images/edit.png></a>
										 <a href=".$page_default."/delete/".$row->id."><img border=0 width=15 height=15 title='Hapus' src=".$this->config->item('base_url')."public/images/delete.png></a>"
										,$row->name
										,$this->function2->get_data_detail('tbl_jenis_diklat', 'name', 'name', $row->jenis)
				);
			} else {
				$record_items[] = array($row->id
										,$no
										,$row->name
										,$this->function2->get_data_detail('tbl_jenis_diklat', 'name', 'name', $row->jenis)
				);
			}
			$no++;
		}

		$this->output->set_output($this->flexigrid->json_build($records['record_count'],$record_items));
	}
	
	function ajax_mata_diklat()
	{
		$model_default = "m_mata_diklat";
		$page_default = "c_mata_diklat";
		$valid_fields = array('id'
							 ,'name'
							 ,'diklat'
		);

		$this->flexigrid->validate_post('id','asc',$valid_fields);
		$records = $this->$model_default->get_all_data();
		$this->output->set_header($this->config->item('json_header'));
		$record_items = array();
		$no=1;
		foreach ($records['records']->result() as $row)
		{
			if ($this->session->userdata('access') == "1") {
				$record_items[] = array($row->id
										,$no
										,"<a href=".$page_default."/update/".$row->id."><img border=0 width=15 height=15 title='Rubah' src=".$this->config->item('base_url')."public/images/edit.png></a>
										 <a href=".$page_default."/delete/".$row->id."><img border=0 width=15 height=15 title='Hapus' src=".$this->config->item('base_url')."public/images/delete.png></a>"
										,$row->name
										,$this->function2->get_data_detail('tbl_diklat', 'name', 'name', $row->diklat)
				);
			} else {
				$record_items[] = array($row->id
										,$no
										,$row->name
										,$this->function2->get_data_detail('tbl_diklat', 'name', 'name', $row->diklat)
				);
			}
			$no++;
		}

		$this->output->set_output($this->flexigrid->json_build($records['record_count'],$record_items));
	}
	
	function ajax_jenis_diklat()
	{
		$model_default = "m_jenis_diklat";
		$page_default = "c_jenis_diklat";
		$valid_fields = array('id'
							 ,'name'
		);

		$this->flexigrid->validate_post('id','asc',$valid_fields);
		$records = $this->$model_default->get_all_data();
		$this->output->set_header($this->config->item('json_header'));
		$record_items = array();
		$no=1;
		foreach ($records['records']->result() as $row)
		{
			if ($this->session->userdata('access') == "1") {
				$record_items[] = array($row->id
										,$no
										,"<a href=".$page_default."/update/".$row->id."><img border=0 width=15 height=15 title='Rubah' src=".$this->config->item('base_url')."public/images/edit.png></a>
										 <a href=".$page_default."/delete/".$row->id."><img border=0 width=15 height=15 title='Hapus' src=".$this->config->item('base_url')."public/images/delete.png></a>"
										,$row->name
				);
			} else {
				$record_items[] = array($row->id
										,$no
										,$row->name
				);
			}
			$no++;
		}

		$this->output->set_output($this->flexigrid->json_build($records['record_count'],$record_items));
	}
	
	function ajax_golongan()
	{
		$model_default = "m_golongan";
		$page_default = "c_golongan";
		$valid_fields = array('id'
							 ,'name'
							 ,'pangkat'
		);

		$this->flexigrid->validate_post('id','asc',$valid_fields);
		$records = $this->$model_default->get_all_data();
		$this->output->set_header($this->config->item('json_header'));
		$record_items = array();
		$no=1;
		foreach ($records['records']->result() as $row)
		{
			if ($this->session->userdata('access') == "1") {
				$record_items[] = array($row->id
										,$no
										,"<a href=".$page_default."/update/".$row->id."><img border=0 width=15 height=15 title='Rubah' src=".$this->config->item('base_url')."public/images/edit.png></a>
										 <a href=".$page_default."/delete/".$row->id."><img border=0 width=15 height=15 title='Hapus' src=".$this->config->item('base_url')."public/images/delete.png></a>"
										,$row->name
										,$row->pangkat
				);
			} else {
				$record_items[] = array($row->id
										,$no
										,$row->name
										,$row->pangkat
				);
			}
			$no++;
		}

		$this->output->set_output($this->flexigrid->json_build($records['record_count'],$record_items));
	}
	
	function ajax_angkatan()
	{
		$model_default = "m_angkatan";
		$page_default = "c_angkatan";
		$valid_fields = array('id'
							 ,'name'
		);

		$this->flexigrid->validate_post('id','asc',$valid_fields);
		$records = $this->$model_default->get_all_data();
		$this->output->set_header($this->config->item('json_header'));
		$record_items = array();
		$no=1;
		foreach ($records['records']->result() as $row)
		{
			if ($this->session->userdata('access') == "1") {
				$record_items[] = array($row->id
										,$no
										,"<a href=".$page_default."/update/".$row->id."><img border=0 width=15 height=15 title='Rubah' src=".$this->config->item('base_url')."public/images/edit.png></a>
										 <a href=".$page_default."/delete/".$row->id."><img border=0 width=15 height=15 title='Hapus' src=".$this->config->item('base_url')."public/images/delete.png></a>"
										,$row->name
				);
			} else {
				$record_items[] = array($row->id
										,$no
										,$row->name
				);
			}
			$no++;
		}

		$this->output->set_output($this->flexigrid->json_build($records['record_count'],$record_items));
	}

	function ajax_provinsi()
	{
		$model_default = "m_provinsi";
		$page_default = "c_provinsi";
		$valid_fields = array('id'
							 ,'name'
		);

		$this->flexigrid->validate_post('id','asc',$valid_fields);
		$records = $this->$model_default->get_all_data();
		$this->output->set_header($this->config->item('json_header'));
		$record_items = array();
		$no=1;
		foreach ($records['records']->result() as $row)
		{
			if ($this->session->userdata('access') == "1") {
				$record_items[] = array($row->id
										,$no
										,"<a href=".$page_default."/update/".$row->id."><img border=0 width=15 height=15 title='Rubah' src=".$this->config->item('base_url')."public/images/edit.png></a>
										 <a href=".$page_default."/delete/".$row->id."><img border=0 width=15 height=15 title='Hapus' src=".$this->config->item('base_url')."public/images/delete.png></a>"
										,$row->name
				);
			} else {
				$record_items[] = array($row->id
										,$no
										,$row->name
				);
			}
			$no++;
		}

		$this->output->set_output($this->flexigrid->json_build($records['record_count'],$record_items));
	}

	function ajax_kabupaten()
	{
		$model_default = "m_kabupaten";
		$page_default = "c_kabupaten";
		$valid_fields = array('id'
							 ,'name'
							 ,'provinsi'
		);

		$this->flexigrid->validate_post('id','asc',$valid_fields);
		$records = $this->$model_default->get_all_data();
		$this->output->set_header($this->config->item('json_header'));
		$record_items = array();
		$no=1;
		foreach ($records['records']->result() as $row)
		{
			if ($this->session->userdata('access') == "1") {
				$record_items[] = array($row->id
										,$no
										,"<a href=".$page_default."/update/".$row->id."><img border=0 width=15 height=15 title='Rubah' src=".$this->config->item('base_url')."public/images/edit.png></a>
										 <a href=".$page_default."/delete/".$row->id."><img border=0 width=15 height=15 title='Hapus' src=".$this->config->item('base_url')."public/images/delete.png></a>"
										,$row->name
										,$this->function2->get_data_detail('tbl_provinsi', 'name', 'name', $row->provinsi)
				);
			} else {
				$record_items[] = array($row->id
										,$no
										,$row->name
										,$this->function2->get_data_detail('tbl_provinsi', 'name', 'name', $row->provinsi)
				);
			}
			$no++;
		}

		$this->output->set_output($this->flexigrid->json_build($records['record_count'],$record_items));
	}
	
	function ajax_user()
	{
		$model_default = "m_user";
		$page_default = "c_user";
		$valid_fields = array('id'
							 ,'name'
							 ,'username'
							 ,'password'
							 ,'access'
		);

		$this->flexigrid->validate_post('id','asc',$valid_fields);
		$records = $this->$model_default->get_all_data();
		$this->output->set_header($this->config->item('json_header'));
		$record_items = array();
		$no=1;
		foreach ($records['records']->result() as $row)
		{
			if ($this->session->userdata('access') == "1") {
				$record_items[] = array($row->id
										,$no
										,"<a href=".$page_default."/update/".$row->id."><img border=0 width=15 height=15 title='Rubah' src=".$this->config->item('base_url')."public/images/edit.png></a>
										 <a href=".$page_default."/delete/".$row->id."><img border=0 width=15 height=15 title='Hapus' src=".$this->config->item('base_url')."public/images/delete.png></a>"
										,$row->name
										,$row->username
										,$row->password
										,$this->function2->get_data_detail('tbl_access', 'name', 'name', $row->access)
				);
			} else {
				$record_items[] = array($row->id
										,$no
										,$row->name
										,$row->username
										,$row->password
										,$this->function2->get_data_detail('tbl_access', 'name', 'name', $row->access)
										
				);
			}
			$no++;
		}

		$this->output->set_output($this->flexigrid->json_build($records['record_count'],$record_items));
	}

	function ajax_report()
	{
		$model_default = "m_report";
		$page_default = "c_report";
		$valid_fields = array('id'
							 ,'name'
		);

		$this->flexigrid->validate_post('id','asc',$valid_fields);
		$records = $this->$model_default->get_all_data();
		$this->output->set_header($this->config->item('json_header'));
		$record_items = array();
		$no=1;
		foreach ($records['records']->result() as $row)
		{
			if ($this->session->userdata('access') == "1") {
				$record_items[] = array($row->id
										,$no
										,"<a href=".$page_default."/update/".$row->id."><img border=0 width=15 height=15 title='Rubah' src=".$this->config->item('base_url')."public/images/edit.png></a>
										 <a href=".$page_default."/delete/".$row->id."><img border=0 width=15 height=15 title='Hapus' src=".$this->config->item('base_url')."public/images/delete.png></a>"
										,$row->name
										,"<a target=_new href=".base_url()."public/file/report/index.php?id=".$row->id.">View Report</a>"
				);
			} else {
				$record_items[] = array($row->id
										,$no
										,$row->name
										,"<a target=_new href=".base_url()."public/file/report/index.php?id=".$row->id.">View Report</a>"
				);
			}
			$no++;
		}

		$this->output->set_output($this->flexigrid->json_build($records['record_count'],$record_items));
	}

	function ajax_pengajar()
	{
		$model_default = "m_pengajar";
		$page_default = "c_pengajar";
		$valid_fields = array('id'
							 ,'name'
							 ,'nip'
							 ,'tempat_lahir'
							 ,'tanggal_lahir'
							 ,'kategori'
							 ,'jabatan'
							 ,'golongan'
							 ,'instansi'
							 ,'alamat_rumah'
							 ,'provinsi'
							 ,'kabupaten'
							 ,'kode_pos'
							 ,'telp_kode_daerah'
							 ,'no_telp'
							 ,'no_hp'
							 ,'email'
		);

		$this->flexigrid->validate_post('id','asc',$valid_fields);
		$records = $this->$model_default->get_all_data();
		$this->output->set_header($this->config->item('json_header'));
		$record_items = array();
		$no=1;
		foreach ($records['records']->result() as $row)
		{
			if ($this->session->userdata('access') == "1") {
				$record_items[] = array($row->id
										,$no
										,"<a href=".$page_default."/update/".$row->id."><img border=0 width=15 height=15 title='Rubah' src=".$this->config->item('base_url')."public/images/edit.png></a>
										 <a href=".$page_default."/delete/".$row->id."><img border=0 width=15 height=15 title='Hapus' src=".$this->config->item('base_url')."public/images/delete.png></a>"
										,"<a href=".$page_default."/detail/".$row->id."><img border=0 width=15 height=15 title='Show Detail' src=".$this->config->item('base_url')."public/images/magnifier.png></a>"
										,$row->name
										,$row->nip
										,$row->tempat_lahir
										,$row->tanggal_lahir
										,$this->function2->get_data_detail('tbl_kategori_pengajar', 'name', 'name', $row->kategori)
										,$row->jabatan
										,$this->function2->get_data_detail('tbl_golongan', 'name', 'name', $row->golongan)
										,$this->function2->get_data_detail('tbl_instansi', 'name', 'name', $row->instansi)
										,$row->alamat_rumah
										,$this->function2->get_data_detail('tbl_provinsi', 'name', 'name', $row->provinsi)
										,$this->function2->get_data_detail('tbl_kabupaten', 'name', 'name', $row->kabupaten)
										,$row->kode_pos
										,$row->telp_kode_daerah."-".$row->no_telp
										,$row->no_hp
										,$row->email
				);
			} else {
				$record_items[] = array($row->id
										,$no
										,"<a href=".$page_default."/detail/".$row->id."><img border=0 width=15 height=15 title='Show Detail' src=".$this->config->item('base_url')."public/images/magnifier.png></a>"
										,$row->name
										,$row->nip
										,$row->tempat_lahir
										,$row->tanggal_lahir
										,$this->function2->get_data_detail('tbl_kategori_pengajar', 'name', 'name', $row->kategori)
										,$row->jabatan
										,$this->function2->get_data_detail('tbl_golongan', 'name', 'name', $row->golongan)
										,$this->function2->get_data_detail('tbl_instansi', 'name', 'name', $row->instansi)
										,$row->alamat_rumah
										,$this->function2->get_data_detail('tbl_provinsi', 'name', 'name', $row->provinsi)
										,$this->function2->get_data_detail('tbl_kabupaten', 'name', 'name', $row->kabupaten)
										,$row->kode_pos
										,$row->telp_kode_daerah."-".$row->no_telp
										,$row->no_hp
										,$row->email
				);
			}
			$no++;
		}

		$this->output->set_output($this->flexigrid->json_build($records['record_count'],$record_items));
	}

	function ajax_peserta()
	{
		$model_default = "m_peserta";
		$page_default = "c_peserta";
		$valid_fields = array('id'
							 ,'name'
							 ,'gelar_depan'
							 ,'gelar_belakang'
							 ,'nip'
							 ,'tempat_lahir'
							 ,'tanggal_lahir'
							 ,'jabatan'
							 ,'golongan'
							 ,'instansi'
							 ,'alamat_rumah'
							 ,'provinsi'
							 ,'kabupaten'
							 ,'kode_pos'
							 ,'telp_kode_daerah'
							 ,'no_telp'
							 ,'no_hp'
							 ,'email'
		);

		$this->flexigrid->validate_post('id','asc',$valid_fields);
		$records = $this->$model_default->get_all_data();
		$this->output->set_header($this->config->item('json_header'));
		$record_items = array();
		$no=1;
		foreach ($records['records']->result() as $row)
		{
			if ($this->session->userdata('access') == "1") {
				$record_items[] = array($row->id
										,$no
										,"<a href=".$page_default."/update/".$row->id."><img border=0 width=15 height=15 title='Rubah' src=".$this->config->item('base_url')."public/images/edit.png></a>
										 <a href=".$page_default."/delete/".$row->id."><img border=0 width=15 height=15 title='Hapus' src=".$this->config->item('base_url')."public/images/delete.png></a>"
										,"<a href=".$page_default."/detail/".$row->id."><img border=0 width=15 height=15 title='Show Detail' src=".$this->config->item('base_url')."public/images/magnifier.png></a>"
										,$row->gelar_depan." ".$row->name." ".$row->gelar_belakang
										,$row->nip
										,$row->tempat_lahir
										,$row->tanggal_lahir
										,$row->jabatan
										,$this->function2->get_data_detail('tbl_golongan', 'name', 'name', $row->golongan)
										,$this->function2->get_data_detail('tbl_instansi', 'name', 'name', $row->instansi)
										,$row->alamat_rumah
										,$this->function2->get_data_detail('tbl_provinsi', 'name', 'name', $row->provinsi)
										,$this->function2->get_data_detail('tbl_kabupaten', 'name', 'name', $row->kabupaten)
										,$row->kode_pos
										,$row->telp_kode_daerah."-".$row->no_telp
										,$row->no_hp
										,$row->email
				);
			} else {
				$record_items[] = array($row->id
										,$no
										,"<a href=".$page_default."/detail/".$row->id."><img border=0 width=15 height=15 title='Show Detail' src=".$this->config->item('base_url')."public/images/magnifier.png></a>"
										,$row->gelar_depan." ".$row->name." ".$row->gelar_belakang
										,$row->nip
										,$row->tempat_lahir
										,$row->tanggal_lahir
										,$row->jabatan
										,$this->function2->get_data_detail('tbl_golongan', 'name', 'name', $row->golongan)
										,$this->function2->get_data_detail('tbl_instansi', 'name', 'name', $row->instansi)
										,$row->alamat_rumah
										,$this->function2->get_data_detail('tbl_provinsi', 'name', 'name', $row->provinsi)
										,$this->function2->get_data_detail('tbl_kabupaten', 'name', 'name', $row->kabupaten)
										,$row->kode_pos
										,$row->telp_kode_daerah."-".$row->no_telp
										,$row->no_hp
										,$row->email
				);
			}
			$no++;
		}

		$this->output->set_output($this->flexigrid->json_build($records['record_count'],$record_items));
	}

}
?>
<?php
class C_home extends Controller {

	function C_home  ()
	{
		parent::Controller();	
		$this->load->helper('flexigrid');
		$this->load->model('function2', '', TRUE);
	}

	public $page_default 	= "c_home";
	public $ket_index 		= "Home";
	
	function index()
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$data['main_view']   	= 'v_home';
			$data['ket_header'] 	= $this->ket_index;
			$this->load->view('v_template', $data);
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}
}
?>
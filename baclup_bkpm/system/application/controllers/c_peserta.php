<?php
class C_peserta extends Controller {

	function C_peserta  ()
	{
		parent::Controller();	
		$this->load->helper('flexigrid');
		$this->load->model('function2', '', TRUE);
	}

	public $ajax_default 	= "ajax_peserta";
	public $table_default 	= "tbl_peserta";
	public $page_default 	= "c_peserta";
	public $ket_index 		= "Peserta";
	public $ket_add			= "Tambah Peserta";
	public $ket_update 		= "Rubah Peserta";
	public $ket_excel 		= "Data Peserta";

	function index()
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$data['ket_header'] 	= $this->ket_index;
			$data['main_view'] 		= 'v_table';
			//--------------------------Table---------------------------//
			$colModel['no'] 				= array('No.',20,FALSE,'left',3,FALSE);
			if ($this->session->userdata('access') == "1") $colModel['action'] = array('',40,FALSE,'left',3,FALSE);
			$colModel['detail'] 			= array('Show',40,TRUE,'center',2,FALSE);
			$colModel['name'] 				= array('Nama Peserta',200,TRUE,'left',2,FALSE);
			$colModel['nip'] 				= array('NIP',120,TRUE,'left',2,FALSE);
			$colModel['tempat_lahir'] 		= array('Tempat Lahir',120,TRUE,'left',2,FALSE);
			$colModel['tanggal_lahir'] 		= array('Tanggal Lahir',120,TRUE,'left',3,TRUE);
			$colModel['jabatan'] 			= array('Jabatan',120,TRUE,'left',3,TRUE);
			$colModel['golongan'] 			= array('Golongan',120,TRUE,'left',3,FALSE);
			$colModel['instansi'] 			= array('Instansi',300,TRUE,'left',2,FALSE);
			$colModel['alamat_rumah'] 		= array('Alamat Rumah',120,TRUE,'left',2,TRUE);
			$colModel['provinsi'] 			= array('Provinsi',120,TRUE,'left',3,TRUE);
			$colModel['kabupaten'] 			= array('Kabupaten',120,TRUE,'left',3,TRUE);
			$colModel['kode_pos'] 			= array('Kode Pos',120,TRUE,'left',2,TRUE);
			$colModel['no_telp'] 			= array('No Telp',120,TRUE,'left',2,TRUE);
			$colModel['no_hp'] 				= array('No HP',120,TRUE,'left',2,TRUE);
			$colModel['email'] 				= array('Email',120,TRUE,'left',2,TRUE);
			//---------------------------------------------------------//			
			$gridParams = array(
				'title' 				=> 'Selamat datang '.$this->session->userdata('name'),
				'rp' 					=> $this->function2->rp,
				'rpOptions' 			=> $this->function2->rpOptions,
				'pagestat' 				=> $this->function2->pagestat,
				'blockOpacity' 			=> $this->function2->blockOpacity,
				'showTableToggleBtn' 	=> $this->function2->showTableToggleBtn
			);

			if ($this->session->userdata('access') == "1") {
				$buttons[] = array('<a href='.base_url().'index.php/'.$this->page_default.'/add>Tambah Data</a>','add','test');
				$buttons[] = array('separator');
			}
			$buttons[] = array('<a href='.base_url().'index.php/'.$this->page_default.'/excel>Export Excel</a>','','test');
			
			$grid_js = build_grid_js('flex1',site_url("ajax/".$this->ajax_default),$colModel,'name','ASC',$gridParams,$buttons);
			
			$data['js_grid'] 		= $grid_js;
			$data['version'] 		= "0.36";
			$data['download_file'] 	= "Flexigrid_CI_v0.36.rar";
			
			$this->load->view('v_template',$data);
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}
	
	function add()
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$data['main_view']   	= 'v_form';
			$data['ket_header'] 	= $this->ket_add;
			$data['action_form']   	= $this->page_default.'/add_process/';
			$data['btn_value']   	= 'Tambah';
			$data['id']   			= '';
			//--------------------------Form---------------------------//
			$data_golongan 		= $this->function2->get_data_detail('tbl_golongan', 'name', 'name')->result();
			$data_instansi 		= $this->function2->get_data_detail('tbl_instansi', 'name', 'name')->result();
			$data_provinsi 		= $this->function2->get_data_detail('tbl_provinsi', 'name', 'name')->result();
			$data_kabupaten 	= $this->function2->get_data_detail('tbl_kabupaten', 'name', 'name')->result();
			
			$data['form'] 	= "";
			$data['form']  .= $this->function2->get_row('Nama Peserta', 'name', 80, 1, 0, '', '', '', '');
			$data['form']  .= $this->function2->get_row('Gelar Depan', 'gelar_depan', 10, 1, 0, '', '', '', '');
			$data['form']  .= $this->function2->get_row('Gelar Belakang', 'gelar_belakang', 10, 1, 0, '', '', '', '');
			$data['form']  .= $this->function2->get_row('NIP', 'nip', 20, 1, 0, '', '', '', '');
			$data['form']  .= $this->function2->get_row('Tempat Lahir', 'tempat_lahir', 20, 1, 0, '', '', '', '');
			$data['form']  .= $this->function2->get_row('Tanggal Lahir', 'tanggal_lahir', 20, 3, 0, '', '', '', 'datepicker1');
			$data['form']  .= $this->function2->get_row('Jabatan', 'jabatan', 40, 1, 0, '', '', '', '');
			$data['form']  .= $this->function2->get_row('Golongan', 'golongan', 20, 2, $data_golongan, '', 'id', 'name', '');
			$data['form']  .= $this->function2->get_row('Instansi', 'instansi', 20, 2, $data_instansi, '', 'id', 'name', '');
			$data['form']  .= $this->function2->get_row('Alamat Rumah', 'alamat_rumah', 0, 5, 0, '', '', '', '');
			$data['form']  .= $this->function2->get_row('Provinsi', 'provinsi', 20, 2, $data_provinsi, '', 'id', 'name', '');
			$data['form']  .= $this->function2->get_row('Kabupaten dan Kota', 'kabupaten', 20, 2, $data_kabupaten, '', 'id', 'name', '');
			$data['form']  .= $this->function2->get_row('Kode Pos', 'kode_pos', 20, 1, 0, '', '', '', '');
			$data['form']  .= $this->function2->get_row('Kode Daerah Telp', 'telp_kode_daerah', 5, 1, 0, '', '', '', '');
			$data['form']  .= $this->function2->get_row('No. Telp', 'no_telp', 20, 1, 0, '', '', '', '');
			$data['form']  .= $this->function2->get_row('No. HP', 'no_hp', 20, 1, 0, '', '', '', '');
			$data['form']  .= $this->function2->get_row('Email', 'email', 20, 1, 0, '', '', '', '');
			//---------------------------------------------------------//	
			$this->load->view('v_template', $data);
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}
	
	function update($id)
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$data['main_view']   	= 'v_form';
			$data['ket_header'] 	= $this->ket_update;
			$data['action_form']   	= $this->page_default.'/update_process/';
			$data['btn_value']   	= 'Simpan';
			$data['id']   			= $id;
			//--------------------------Form---------------------------//
			$table_row 		= $this->function2->get_table_by_id($this->table_default, 'id', $id)->row();
			$data_golongan 		= $this->function2->get_data_detail('tbl_golongan', 'name', 'name')->result();
			$data_instansi 		= $this->function2->get_data_detail('tbl_instansi', 'name', 'name')->result();
			$data_provinsi 		= $this->function2->get_data_detail('tbl_provinsi', 'name', 'name')->result();
			$data_kabupaten 	= $this->function2->get_data_detail('tbl_kabupaten', 'name', 'name')->result();

			$data['form'] 	= "";
			$data['form']  .= $this->function2->get_row('Nama Peserta', 'name', 80, 1, 0, $table_row->name, '', '', '');
			$data['form']  .= $this->function2->get_row('Gelar Depan', 'gelar_depan', 10, 1, 0, $table_row->nip, '', '', '');
			$data['form']  .= $this->function2->get_row('Gelar Belakang', 'gelar_belakang', 10, 1, 0, $table_row->nip, '', '', '');
			$data['form']  .= $this->function2->get_row('NIP', 'nip', 20, 1, 0, $table_row->nip, '', '', '');
			$data['form']  .= $this->function2->get_row('Tempat Lahir', 'tempat_lahir', 20, 1, 0, $table_row->tempat_lahir, '', '', '');
			$data['form']  .= $this->function2->get_row('Tanggal Lahir', 'tanggal_lahir', 20, 3, 0, $table_row->tanggal_lahir, '', '', 'datepicker1');
			$data['form']  .= $this->function2->get_row('Jabatan', 'jabatan', 40, 1, 0, $table_row->jabatan, '', '', '');
			$data['form']  .= $this->function2->get_row('Golongan', 'golongan', 20, 2, $data_golongan, $table_row->golongan, 'id', 'name', '');
			$data['form']  .= $this->function2->get_row('Instansi', 'instansi', 20, 2, $data_instansi, $table_row->instansi, 'id', 'name', '');
			$data['form']  .= $this->function2->get_row('Alamat Rumah', 'alamat_rumah', 0, 5, 0, $table_row->alamat_rumah, '', '', '');
			$data['form']  .= $this->function2->get_row('Provinsi', 'provinsi', 20, 2, $data_provinsi, $table_row->provinsi, 'id', 'name', '');
			$data['form']  .= $this->function2->get_row('Kabupaten dan Kota', 'kabupaten', 20, 2, $data_kabupaten, $table_row->kabupaten, 'id', 'name', '');
			$data['form']  .= $this->function2->get_row('Kode Pos', 'kode_pos', 20, 1, 0, $table_row->kode_pos, '', '', '');
			$data['form']  .= $this->function2->get_row('Kode Daerah Telp', 'telp_kode_daerah', 5, 1, 0, $table_row->telp_kode_daerah, '', '', '');
			$data['form']  .= $this->function2->get_row('No. Telp', 'no_telp', 20, 1, 0, $table_row->no_telp, '', '', '');
			$data['form']  .= $this->function2->get_row('No. HP', 'no_hp', 20, 1, 0, $table_row->no_hp, '', '', '');
			$data['form']  .= $this->function2->get_row('Email', 'email', 20, 1, 0, $table_row->email, '', '', '');
			//---------------------------------------------------------//			
			$this->load->view('v_template',$data);
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}

	function delete($id)
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$data['main_view']   	= 'v_form';
			$data['ket_header'] 	= 'Konfirmasi delete';
			$data['action_form']   	= $this->page_default.'/delete_process/';
			$data['btn_value']   	= 'Konfirmasi';
			$data['id']   			= $id;
			//--------------------------Form---------------------------//
			$data['form'] 	= "";
			$data['form']  .= $this->function2->get_row('Masukkan password admin', 'password', 20, 7, 0, '', '', '', '');
			//---------------------------------------------------------//			
			$this->load->view('v_template',$data);
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}

	function add_process()
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$table_col = array('name'  				=> $this->input->post('name')
							  ,'gelar_depan'  		=> $this->input->post('gelar_depan')
							  ,'gelar_belakang'  	=> $this->input->post('gelar_belakang')
							  ,'nip'  				=> $this->input->post('nip')
							  ,'tempat_lahir'  		=> $this->input->post('tempat_lahir')
							  ,'tanggal_lahir'  	=> $this->input->post('tanggal_lahir')
							  ,'jabatan'  			=> $this->input->post('jabatan')
							  ,'golongan'  			=> $this->input->post('golongan')
							  ,'instansi'  			=> $this->input->post('instansi')
							  ,'alamat_rumah'  		=> $this->input->post('alamat_rumah')
							  ,'provinsi'  			=> $this->input->post('provinsi')
							  ,'kabupaten'  		=> $this->input->post('kabupaten')
							  ,'kode_pos'  			=> $this->input->post('kode_pos')
							  ,'telp_kode_daerah'  	=> $this->input->post('telp_kode_daerah')
							  ,'no_telp'  			=> $this->input->post('no_telp')
							  ,'no_hp'  			=> $this->input->post('no_hp')
							  ,'email'  			=> $this->input->post('email')
			);
			$this->function2->add($this->table_default, $table_col);
			redirect($this->page_default);
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}

	function update_process()
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$table_col = array('name'  				=> $this->input->post('name')
							  ,'gelar_depan'  		=> $this->input->post('gelar_depan')
							  ,'gelar_belakang'  	=> $this->input->post('gelar_belakang')
							  ,'nip'  				=> $this->input->post('nip')
							  ,'tempat_lahir'  		=> $this->input->post('tempat_lahir')
							  ,'tanggal_lahir'  	=> $this->input->post('tanggal_lahir')
							  ,'jabatan'  			=> $this->input->post('jabatan')
							  ,'golongan'  			=> $this->input->post('golongan')
							  ,'instansi'  			=> $this->input->post('instansi')
							  ,'alamat_rumah'  		=> $this->input->post('alamat_rumah')
							  ,'provinsi'  			=> $this->input->post('provinsi')
							  ,'kabupaten'  		=> $this->input->post('kabupaten')
							  ,'kode_pos'  			=> $this->input->post('kode_pos')
							  ,'telp_kode_daerah'  	=> $this->input->post('telp_kode_daerah')
							  ,'no_telp'  			=> $this->input->post('no_telp')
							  ,'no_hp'  			=> $this->input->post('no_hp')
							  ,'email'  			=> $this->input->post('email')
			);
			$this->function2->update($this->table_default, $table_col, 'id', $this->input->post('id'));
			redirect($this->page_default);
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}
	
	function delete_process()
	{
		if ($this->session->userdata('id') == TRUE)
		{
			if ($this->function2->check_password($this->input->post('password')) == TRUE)
			{
				$this->function2->delete($this->table_default, 'id', $this->input->post('id'));
				redirect($this->page_default);
			} else {
				redirect($this->page_default);
				
			}
		}
	}
	
	function excel()
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$data['ket_header'] = $this->ket_excel;
			$data['filename'] = 'user '.date('Y-m-d H:i:s');

			$coll_header = array('Nama'
							  ,'NIP'							  
							  ,'Tempat Lahir'
							  ,'Tanggal Lahir'
							  ,'Jabatan'
							  ,'Golongan'
							  ,'Instansi'
							  ,'Alamat Rumah'
							  ,'Provinsi'
							  ,'Kabupaten dan Kota'
							  ,'Kode Pos'
							  ,'Kode Daerah Telp'
							  ,'No. Telp'
							  ,'No. HP'
							  ,'Email'
			);
			$coll_detail = array('name'
							  ,'nip'
							  ,'tempat_lahir'
							  ,'tanggal_lahir'
							  ,'jabatan'
							  ,'golongan'
							  ,'instansi'
							  ,'alamat_rumah'
							  ,'provinsi'
							  ,'kabupaten'
							  ,'kode_pos'
							  ,'telp_kode_daerah'
							  ,'no_telp'
							  ,'no_hp'
							  ,'email'
			);
			$coll_function = array('default'
							  ,'default'
							  ,'default'
							  ,'default'
							  ,'default'
							  ,'tbl_golongan'
							  ,'tbl_instansi'
							  ,'default'
							  ,'tbl_provinsi'
							  ,'tbl_kabupaten'
							  ,'default'
							  ,'default'
							  ,'default'
							  ,'default'
							  ,'default'
			);
			$data['data'] 		= $this->function2->get_excel($this->table_default, $coll_header, $coll_detail, $coll_function);
			$this->load->view('v_excel',$data);
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}
	
	function detail($id)
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$data['main_view']   	= 'v_detail';
			$data['ket_active']   	= 1;
			$data['ket_page']   	= $this->page_default;
			$data['ket_header'] 	= 'Detail Pengajar';
			$data['ket_header'] 	= 'Detail Pengajar';
			
			$data['id']   			= $id;
			//--------------------------Form---------------------------//
			$table_row 			= $this->function2->get_table_by_id($this->table_default, 'id', $id)->row();
			$data_golongan 		= $this->function2->get_data_detail('tbl_golongan', 'name', 'name')->result();
			$data_instansi 		= $this->function2->get_data_detail('tbl_instansi', 'name', 'name')->result();
			$data_provinsi 		= $this->function2->get_data_detail('tbl_provinsi', 'name', 'name')->result();
			$data_kabupaten 	= $this->function2->get_data_detail('tbl_kabupaten', 'name', 'name')->result();

			$data['form'] 	= "";
			$data['form']  .= $this->function2->get_detail('Nama Peserta', 'name', 80, 1, 0, $table_row->name, '', '', '');
			$data['form']  .= $this->function2->get_detail('Gelar Depan', 'gelar_depan', 10, 1, 0, $table_row->nip, '', '', '');
			$data['form']  .= $this->function2->get_detail('Gelar Belakang', 'gelar_belakang', 10, 1, 0, $table_row->nip, '', '', '');
			$data['form']  .= $this->function2->get_detail('NIP', 'nip', 20, 1, 0, $table_row->nip, '', '', '');
			$data['form']  .= $this->function2->get_detail('Tempat Lahir', 'tempat_lahir', 20, 1, 0, $table_row->tempat_lahir, '', '', '');
			$data['form']  .= $this->function2->get_detail('Tanggal Lahir', 'tanggal_lahir', 20, 1, 0, $table_row->tanggal_lahir, '', '', '');
			$data['form']  .= $this->function2->get_detail('Jabatan', 'jabatan', 40, 1, 0, $table_row->jabatan, '', '', '');
			$data['form']  .= $this->function2->get_detail('Golongan', 'golongan', 20, 2, $data_golongan, $table_row->golongan, 'id', 'name', '');
			$data['form']  .= $this->function2->get_detail('Instansi', 'instansi', 20, 2, $data_instansi, $table_row->instansi, 'id', 'name', '');
			$data['form']  .= $this->function2->get_detail('Alamat Rumah', 'alamat_rumah', 0, 5, 0, $table_row->alamat_rumah, '', '', '');
			$data['form']  .= $this->function2->get_detail('Provinsi', 'provinsi', 20, 2, $data_provinsi, $table_row->provinsi, 'id', 'name', '');
			$data['form']  .= $this->function2->get_detail('Kabupaten dan Kota', 'kabupaten', 20, 2, $data_kabupaten, $table_row->kabupaten, 'id', 'name', '');
			$data['form']  .= $this->function2->get_detail('Kode Pos', 'kode_pos', 20, 1, 0, $table_row->kode_pos, '', '', '');
			$data['form']  .= $this->function2->get_detail('Kode Daerah Telp', 'telp_kode_daerah', 5, 1, 0, $table_row->telp_kode_daerah, '', '', '');
			$data['form']  .= $this->function2->get_detail('No. Telp', 'no_telp', 20, 1, 0, $table_row->no_telp, '', '', '');
			$data['form']  .= $this->function2->get_detail('No. HP', 'no_hp', 20, 1, 0, $table_row->no_hp, '', '', '');
			$data['form']  .= $this->function2->get_detail('Email', 'email', 20, 1, 0, $table_row->email, '', '', '');
			//---------------------------------------------------------//			
			$this->load->view('v_template',$data);
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}
	
	function detail_history($id)
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$data['ket_header'] 	= 'Detail Pengajar';
			$data['main_view'] 		= 'v_detail';
			$data['ket_page']   	= $this->page_default;
			$data['ket_active']   	= 2;
			$data['id']   			= $id;
			
			$form 			= $this->function2->get_history($id)->result();
			
			$data['form']	=	"";
			$no=1;
			foreach ($form as $form_coll) {
				$data['form']	.=	"<tr>";
				$data['form'] 	.= "<td>".$no."</td>";
				$data['form'] 	.= "<td><a href=pelaksanaan/detail/".$form_coll->diklat."><img border=0 width=15 height=15 title='Show Detail' src=".$this->config->item('base_url')."public/images/magnifier.png></a></td>";
				$data['form'] 	.= "<td>".$form_coll->dt_from." s/d ".$form_coll->dt_to."</td>";
				$data['form'] 	.= "<td>".$this->function2->get_data_detail('tbl_diklat', 'name', 'name', $form_coll->diklat)."</td>";
				$data['form'] 	.= "<td>".$this->function2->get_data_detail('tbl_angkatan', 'name', 'name', $form_coll->angkatan)." tahun ".$form_coll->tahun."</td>";
				$data['form'] 	.= "<td>".$this->function2->get_data_detail('tbl_lokasi_diklat', 'name', 'name', $form_coll->lokasi)."</td>";
				$data['form'] 	.= "<td>".$form_coll->waktu."</td>";
				$data['form']	.=	"</tr>";
				$no++;
			}
			$this->load->view('v_template',$data);
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}	
	
	
}
?>
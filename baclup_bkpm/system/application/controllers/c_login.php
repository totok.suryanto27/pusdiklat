<?php
class C_login extends Controller {

	function C_login  ()
	{
		parent::Controller();	
		$this->load->model('m_login', '', TRUE);	
	}
	
	function index()
	{
			$data['form_action'] = 'c_login/process_login';
			$this->load->view('v_login', $data);
	}

	function process_login()
	{

		$this->validation->set_rules('username', 'Username', 'required');
		$this->validation->set_rules('password', 'Password', 'required');
		
		if ($this->validation->run() == TRUE)
		{
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			if ($this->m_login->check_user($username, $password) == TRUE)
			{
				$query = $this->m_login->get_sess($username);
				$session = array('id'		=> $query->id
								,'name'		=> $query->name
								,'access'	=> $query->access
				);
				$this->session->set_userdata($session);
				redirect('c_home');
			}
			else
			{
				$this->session->set_flashdata('message', 'Username atau password salah!!');
				redirect('c_login');
			}
		}
		else
		{
			redirect('c_login');
		}
	}
	
	function process_logout()
	{
		$this->session->sess_destroy();
		redirect('c_login', 'refresh');
	}
	
	

}
?>
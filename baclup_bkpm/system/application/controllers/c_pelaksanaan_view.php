<?php
class C_pelaksanaan_view extends Controller {

	function C_pelaksanaan_view  ()
	{
		parent::Controller();	
		$this->load->helper('flexigrid');
		$this->load->model('function2', '', TRUE);
	}

	public $ajax_default 	= "ajax_pelaksanaan";
	public $table_default 	= "tbl_pelaksanaan";
	public $page_default 	= "c_pelaksanaan";
	public $ket_index 		= "Pelaksanaan";
	public $ket_add			= "Tambah Pelaksanaan";
	public $ket_update 		= "Rubah Pelaksanaan";
	public $ket_excel 		= "Data Pelaksanaan";
	
	function detail($id)
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$data['main_view']   	= 'v_detail_pelaksanaan_view';
			$data['ket_active']   	= 1;
			$data['ket_page']   	= $this->page_default;
			$data['ket_header'] 	= 'Detail Instansi';
			$data['id']   			= $id;
			$data['action_form']   	= $this->page_default.'/detail_update/';
			//--------------------------Form---------------------------//
			$data['data'] 				= $this->function2->get_table_by_id($this->table_default, 'id', $id)->row();
			$data['data_lokasi_diklat'] = $this->function2->get_data_detail('tbl_lokasi_diklat', 'name', 'name')->result();
			$data['data_diklat']		= $this->function2->get_data_detail('tbl_diklat', 'name', 'name')->result();
			$data['data_angkatan'] 		= $this->function2->get_data_detail('tbl_angkatan', 'name', 'id')->result();
			//---------------------------------------------------------//			
			$this->load->view('v_template',$data);
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}	

	function detail_peserta($id)
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$data['main_view']   	= 'v_detail_pelaksanaan_view';
			$data['ket_active']   	= 2;
			$data['ket_page']   	= $this->page_default;
			$data['ket_header'] 	= 'Detail Instansi';
			$data['id']   			= $id;
			$data['action_form']   	= $this->page_default.'/detail_peserta_update/';
			//--------------------------Form---------------------------//
			$data['data'] 			= $this->function2->get_peserta_pelaksanaan($id)->result();
			$data['data_peserta']   = $this->function2->get_data_detail('tbl_peserta', 'name', 'name')->result();
			
			//---------------------------------------------------------//			
			$this->load->view('v_template',$data);
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}	
	



	function detail_pengajar($id)
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$data['main_view']   	= 'v_detail_pelaksanaan_view';
			$data['ket_active']   	= 3;
			$data['ket_page']   	= $this->page_default;
			$data['ket_header'] 	= 'Detail Instansi';
			$data['id']   			= $id;
			$data['action_form']   	= $this->page_default.'/detail_pengajar_update/';
			//--------------------------Form---------------------------//
			$data['data'] 				= $this->function2->get_pengajar_pelaksanaan($id)->result();
			$data['data_mata_diklat']   = $this->function2->get_data_detail('tbl_mata_diklat', 'name', 'name')->result();
			$data['data_pengajar']   	= $this->function2->get_data_detail('tbl_pengajar', 'name', 'name')->result();
			
			//---------------------------------------------------------//			
			$this->load->view('v_template',$data);
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}	


	


	function detail_foto($id)
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$data['main_view']   	= 'v_detail_pelaksanaan_view';
			$data['ket_active']   	= 4;
			$data['ket_page']   	= $this->page_default;
			$data['ket_header'] 	= 'Detail Instansi';
			$data['id']   			= $id;
			$data['action_form']   	= $this->page_default.'/detail_foto_update/';
			//--------------------------Form---------------------------//
			$data['data'] 				= $this->function2->get_foto_pelaksanaan($id)->result();
			
			//---------------------------------------------------------//			
			$this->load->view('v_template',$data);
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}	
	

	
	function detail_report($id)
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$data['main_view']   	= 'v_detail_pelaksanaan_view';
			$data['ket_active']   	= 5;
			$data['ket_page']   	= $this->page_default;
			$data['ket_header'] 	= 'Detail Instansi';
			$data['id']   			= $id;
			$this->load->view('v_template',$data);
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}	
		


	
}
?>
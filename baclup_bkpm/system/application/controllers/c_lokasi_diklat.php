<?php
class C_lokasi_diklat extends Controller {

	function C_lokasi_diklat  ()
	{
		parent::Controller();	
		$this->load->helper('flexigrid');
		$this->load->model('function2', '', TRUE);
	}

	public $ajax_default 	= "ajax_lokasi_diklat";
	public $table_default 	= "tbl_lokasi_diklat";
	public $page_default 	= "c_lokasi_diklat";
	public $ket_index 		= "Lokasi Diklat";
	public $ket_add			= "Tambah Lokasi";
	public $ket_update 		= "Rubah Lokasi";
	public $ket_excel 		= "Data Lokasi";

	function index()
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$data['ket_header'] 	= $this->ket_index;
			$data['main_view'] 		= 'v_table';
			//--------------------------Table---------------------------//
			$colModel['no'] 				= array('No.',20,FALSE,'left',3,FALSE);
			if ($this->session->userdata('access') == "1") $colModel['action'] = array('',40,FALSE,'left',3,FALSE);
			$colModel['detail'] 			= array('Show',40,FALSE,'center',3,FALSE);
			$colModel['name'] 				= array('Nama Lokasi',200,TRUE,'left',2,FALSE);
			$colModel['alamat'] 			= array('Alamat',300,TRUE,'left',2,TRUE);
			$colModel['provinsi'] 			= array('Provinsi',120,TRUE,'left',3,TRUE);
			$colModel['kabupaten'] 			= array('Kabupaten',120,TRUE,'left',3,TRUE);
			$colModel['kode_pos'] 			= array('Kode Pos',120,TRUE,'left',2,TRUE);
			$colModel['no_telp'] 			= array('No Telp',120,TRUE,'left',2,FALSE);
			$colModel['no_fax'] 			= array('No Fax',120,TRUE,'left',2,TRUE);
			$colModel['website'] 			= array('Website',120,TRUE,'left',2,FALSE);
			$colModel['email'] 				= array('Email',120,TRUE,'left',2,FALSE);
			//---------------------------------------------------------//			
			$gridParams = array(
				'title' 				=> 'Selamat datang '.$this->session->userdata('name'),
				'rp' 					=> $this->function2->rp,
				'rpOptions' 			=> $this->function2->rpOptions,
				'pagestat' 				=> $this->function2->pagestat,
				'blockOpacity' 			=> $this->function2->blockOpacity,
				'showTableToggleBtn' 	=> $this->function2->showTableToggleBtn
			);

			if ($this->session->userdata('access') == "1") {
				$buttons[] = array('<a href='.base_url().'index.php/'.$this->page_default.'/add>Tambah Data</a>','add','test');
				$buttons[] = array('separator');
			}
			$buttons[] = array('<a href='.base_url().'index.php/'.$this->page_default.'/excel>Export Excel</a>','','test');
			
			$grid_js = build_grid_js('flex1',site_url("ajax/".$this->ajax_default),$colModel,'name','ASC',$gridParams,$buttons);
			
			$data['js_grid'] 		= $grid_js;
			$data['version'] 		= "0.36";
			$data['download_file'] 	= "Flexigrid_CI_v0.36.rar";
			
			$this->load->view('v_template',$data);
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}
	
	function add()
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$data['main_view']   	= 'v_form';
			$data['ket_header'] 	= $this->ket_add;
			$data['action_form']   	= $this->page_default.'/add_process/';
			$data['btn_value']   	= 'Tambah';
			$data['id']   			= '';
			//--------------------------Form---------------------------//
			$data_provinsi 		= $this->function2->get_data_detail('tbl_provinsi', 'name', 'name')->result();
			$data_kabupaten 	= $this->function2->get_data_detail('tbl_kabupaten', 'name', 'name')->result();
			
			$data['form'] 	= "";
			$data['form']  .= $this->function2->get_row('Nama Lokasi', 'name', 80, 1, 0, '', '', '', '');
			$data['form']  .= $this->function2->get_row('Alamat', 'alamat', 0, 5, 0, '', '', '', '');
			$data['form']  .= $this->function2->get_row('Provinsi', 'provinsi', 20, 2, $data_provinsi, '', 'id', 'name', '');
			$data['form']  .= $this->function2->get_row('Kabupaten dan Kota', 'kabupaten', 20, 2, $data_kabupaten, '', 'id', 'name', '');
			$data['form']  .= $this->function2->get_row('Kode Pos', 'kode_pos', 20, 1, 0, '', '', '', '');
			$data['form']  .= $this->function2->get_row('Kode Daerah Telp', 'telp_kode_daerah', 5, 1, 0, '', '', '', '');
			$data['form']  .= $this->function2->get_row('No. Telp', 'no_telp', 20, 1, 0, '', '', '', '');
			$data['form']  .= $this->function2->get_row('Kode Daerah Fax', 'fax_kode_daerah', 5, 1, 0, '', '', '', '');
			$data['form']  .= $this->function2->get_row('No. Fax', 'no_fax', 20, 1, 0, '', '', '', '');
			$data['form']  .= $this->function2->get_row('Website', 'website', 20, 1, 0, '', '', '', '');
			$data['form']  .= $this->function2->get_row('Email', 'email', 20, 1, 0, '', '', '', '');
			
			//---------------------------------------------------------//	
			$this->load->view('v_template', $data);
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}
	
	function update($id)
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$data['main_view']   	= 'v_form';
			$data['ket_header'] 	= $this->ket_update;
			$data['action_form']   	= $this->page_default.'/update_process/';
			$data['btn_value']   	= 'Simpan';
			$data['id']   			= $id;
			//--------------------------Form---------------------------//
			$table_row 			= $this->function2->get_table_by_id($this->table_default, 'id', $id)->row();
			$data_provinsi 		= $this->function2->get_data_detail('tbl_provinsi', 'name', 'name')->result();
			$data_kabupaten 	= $this->function2->get_data_detail('tbl_kabupaten', 'name', 'name')->result();

			$data['form'] 	= "";
			$data['form']  .= $this->function2->get_row('Nama Lokasi', 'name', 80, 1, 0, $table_row->name, '', '', '');
			$data['form']  .= $this->function2->get_row('Alamat', 'alamat', 0, 5, 0, $table_row->alamat, '', '', '');
			$data['form']  .= $this->function2->get_row('Provinsi', 'provinsi', 20, 2, $data_provinsi, $table_row->provinsi, 'id', 'name', '');
			$data['form']  .= $this->function2->get_row('Kabupaten dan Kota', 'kabupaten', 20, 2, $data_kabupaten, $table_row->kabupaten, 'id', 'name', '');
			$data['form']  .= $this->function2->get_row('Kode Pos', 'kode_pos', 20, 1, 0, $table_row->kode_pos, '', '', '');
			$data['form']  .= $this->function2->get_row('Kode Daerah Telp', 'telp_kode_daerah', 5, 1, 0, $table_row->telp_kode_daerah, '', '', '');
			$data['form']  .= $this->function2->get_row('No. Telp', 'no_telp', 20, 1, 0, $table_row->no_telp, '', '', '');
			$data['form']  .= $this->function2->get_row('Kode Daerah Fax', 'fax_kode_daerah', 5, 1, 0, $table_row->fax_kode_daerah, '', '', '');
			$data['form']  .= $this->function2->get_row('No. Fax', 'no_fax', 20, 1, 0, $table_row->no_fax, '', '', '');
			$data['form']  .= $this->function2->get_row('Website', 'no_fax', 20, 1, 0, $table_row->website, '', '', '');
			$data['form']  .= $this->function2->get_row('Email', 'email', 20, 1, 0, $table_row->email, '', '', '');
			//---------------------------------------------------------//			
			$this->load->view('v_template',$data);
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}

	function delete($id)
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$data['main_view']   	= 'v_form';
			$data['ket_header'] 	= 'Konfirmasi delete';
			$data['action_form']   	= $this->page_default.'/delete_process/';
			$data['btn_value']   	= 'Konfirmasi';
			$data['id']   			= $id;
			//--------------------------Form---------------------------//
			$data['form'] 	= "";
			$data['form']  .= $this->function2->get_row('Masukkan password admin', 'password', 20, 7, 0, '', '', '', '');
			//---------------------------------------------------------//			
			$this->load->view('v_template',$data);
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}

	function add_process()
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$table_col = array('name'  				=> $this->input->post('name')
							  ,'alamat'  			=> $this->input->post('alamat')
							  ,'provinsi'  			=> $this->input->post('provinsi')
							  ,'kabupaten'  		=> $this->input->post('kabupaten')
							  ,'kode_pos'  			=> $this->input->post('kode_pos')
							  ,'telp_kode_daerah'  	=> $this->input->post('telp_kode_daerah')
							  ,'no_telp'  			=> $this->input->post('no_telp')
							  ,'fax_kode_daerah'  	=> $this->input->post('fax_kode_daerah')
							  ,'no_fax'  			=> $this->input->post('no_fax')
							  ,'website'  			=> $this->input->post('website')
							  ,'email'  			=> $this->input->post('email')
			);
			$this->function2->add($this->table_default, $table_col);
			redirect($this->page_default);
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}

	function update_process()
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$table_col = array('name'  				=> $this->input->post('name')
							  ,'alamat'  			=> $this->input->post('alamat')
							  ,'provinsi'  			=> $this->input->post('provinsi')
							  ,'kabupaten'  		=> $this->input->post('kabupaten')
							  ,'kode_pos'  			=> $this->input->post('kode_pos')
							  ,'telp_kode_daerah'  	=> $this->input->post('telp_kode_daerah')
							  ,'no_telp'  			=> $this->input->post('no_telp')
							  ,'fax_kode_daerah'  	=> $this->input->post('fax_kode_daerah')
							  ,'no_fax'  			=> $this->input->post('no_fax')
							  ,'website'  			=> $this->input->post('website')
							  ,'email'  			=> $this->input->post('email')
			);
			$this->function2->update($this->table_default, $table_col, 'id', $this->input->post('id'));
			redirect($this->page_default);
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}
	
	function delete_process()
	{
		if ($this->session->userdata('id') == TRUE)
		{
			if ($this->function2->check_password($this->input->post('password')) == TRUE)
			{
				$this->function2->delete($this->table_default, 'id', $this->input->post('id'));
				redirect($this->page_default);
			} else {
				redirect($this->page_default);
				
			}
		}
	}
	
	function excel()
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$data['ket_header'] = $this->ket_excel;
			$data['filename'] = 'user '.date('Y-m-d H:i:s');

			$coll_header = array('Nama Lokasi'
							  ,'Alamat'
							  ,'Provinsi'
							  ,'Kabupaten dan Kota'
							  ,'Kode Pos'
							  ,'Kode Daerah Telp'
							  ,'No. Telp'
							  ,'Kode Daerah Fax'
							  ,'No. Fax'
							  ,'Website'
							  ,'Email'
			);
			$coll_detail = array('name'
							  ,'alamat'
							  ,'provinsi'
							  ,'kabupaten'
							  ,'kode_pos'
							  ,'telp_kode_daerah'
							  ,'no_telp'
							  ,'fax_kode_daerah'
							  ,'no_fax'
							  ,'website'
							  ,'email'
			);
			$coll_function = array('default'
							  ,'default'
							  ,'tbl_provinsi'
							  ,'tbl_kabupaten'
							  ,'default'
							  ,'default'
							  ,'default'
							  ,'default'
							  ,'default'
							  ,'default'
							  ,'default'
			);
			$data['data'] 		= $this->function2->get_excel($this->table_default, $coll_header, $coll_detail, $coll_function);
			$this->load->view('v_excel',$data);
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}
	
	function detail($id)
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$data['main_view']   	= 'v_detail';
			$data['ket_active']   	= 1;
			$data['ket_hidd']   	= true;
			$data['ket_page']   	= $this->page_default;
			$data['ket_header'] 	= 'Detail Lokasi';
			$data['id']   			= $id;
			//--------------------------Form---------------------------//
			$table_row 			= $this->function2->get_table_by_id($this->table_default, 'id', $id)->row();
			$data_provinsi 		= $this->function2->get_data_detail('tbl_provinsi', 'name', 'name')->result();
			$data_kabupaten 	= $this->function2->get_data_detail('tbl_kabupaten', 'name', 'name')->result();

			$data['form'] 	= "";
			$data['form']  .= $this->function2->get_detail('Nama Lokasi', 'name', 80, 1, 0, $table_row->name, '', '', '');
			$data['form']  .= $this->function2->get_detail('Alamat', 'alamat', 0, 5, 0, $table_row->alamat, '', '', '');
			$data['form']  .= $this->function2->get_detail('Provinsi', 'provinsi', 20, 2, $data_provinsi, $table_row->provinsi, 'id', 'name', '');
			$data['form']  .= $this->function2->get_detail('Kabupaten dan Kota', 'kabupaten', 20, 2, $data_kabupaten, $table_row->kabupaten, 'id', 'name', '');
			$data['form']  .= $this->function2->get_detail('Kode Pos', 'kode_pos', 20, 1, 0, $table_row->kode_pos, '', '', '');
			$data['form']  .= $this->function2->get_detail('Kode Daerah Telp', 'telp_kode_daerah', 5, 1, 0, $table_row->telp_kode_daerah, '', '', '');
			$data['form']  .= $this->function2->get_detail('No. Telp', 'no_telp', 20, 1, 0, $table_row->no_telp, '', '', '');
			$data['form']  .= $this->function2->get_detail('Kode Daerah Fax', 'fax_kode_daerah', 5, 1, 0, $table_row->fax_kode_daerah, '', '', '');
			$data['form']  .= $this->function2->get_detail('No. Fax', 'no_fax', 20, 1, 0, $table_row->no_fax, '', '', '');
			$data['form']  .= $this->function2->get_detail('Website', 'no_fax', 20, 1, 0, $table_row->website, '', '', '');
			$data['form']  .= $this->function2->get_detail('Email', 'email', 20, 1, 0, $table_row->email, '', '', '');
			//---------------------------------------------------------//			
			$this->load->view('v_template',$data);
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}	
}
?>
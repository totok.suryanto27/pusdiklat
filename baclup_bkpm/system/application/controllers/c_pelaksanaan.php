<?php
class C_pelaksanaan extends Controller {

	function C_pelaksanaan  ()
	{
		parent::Controller();	
		$this->load->helper('flexigrid');
		$this->load->model('function2', '', TRUE);
	}

	public $ajax_default 	= "ajax_pelaksanaan";
	public $table_default 	= "tbl_pelaksanaan";
	public $page_default 	= "c_pelaksanaan";
	public $ket_index 		= "Pelaksanaan";
	public $ket_add			= "Tambah Pelaksanaan";
	public $ket_update 		= "Rubah Pelaksanaan";
	public $ket_excel 		= "Data Pelaksanaan";

	function index()
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$data['ket_header'] 	= $this->ket_index;
			$data['main_view'] 		= 'v_table';
			//--------------------------Table---------------------------//
			$colModel['no'] 				= array('No.',20,FALSE,'left',3,FALSE);
			if ($this->session->userdata('access') == "1") $colModel['action'] = array('',40,FALSE,'left',3,FALSE);
			$colModel['detail'] 			= array('Show',40,FALSE,'center',3,FALSE);
			$colModel['dt_from'] 			= array('Tanggal',150,FALSE,'left',2,FALSE);
			$colModel['lokasi'] 			= array('Lokasi',300,FALSE,'left',3,TRUE);
			$colModel['waktu'] 				= array('Waktu',120,FALSE,'left',2,TRUE);
			$colModel['diklat'] 			= array('Diklat',120,FALSE,'left',3,FALSE);
			$colModel['angkatan'] 			= array('Angkatan',120,FALSE,'left',3,FALSE);
			$colModel['tahun'] 				= array('Tahun',120,FALSE,'left',2,FALSE);
			//---------------------------------------------------------//			
			$gridParams = array(
				'title' 				=> 'Selamat datang '.$this->session->userdata('name'),
				'rp' 					=> $this->function2->rp,
				'rpOptions' 			=> $this->function2->rpOptions,
				'pagestat' 				=> $this->function2->pagestat,
				'blockOpacity' 			=> $this->function2->blockOpacity,
				'showTableToggleBtn' 	=> $this->function2->showTableToggleBtn
			);

			if ($this->session->userdata('access') == "1") {
				$buttons[] = array('<a href='.base_url().'index.php/'.$this->page_default.'/add>Tambah Data</a>','add','test');
				$buttons[] = array('separator');
			}
			$buttons[] = array('<a href='.base_url().'index.php/'.$this->page_default.'/excel>Export Excel</a>','','test');
			
			$grid_js = build_grid_js('flex1',site_url("ajax/".$this->ajax_default),$colModel,'diklat','ASC',$gridParams,$buttons);
			
			$data['js_grid'] 		= $grid_js;
			$data['version'] 		= "0.36";
			$data['download_file'] 	= "Flexigrid_CI_v0.36.rar";
			
			$this->load->view('v_template',$data);
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}
	
	function add()
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$data['main_view']   	= 'v_form';
			$data['ket_header'] 	= $this->ket_add;
			$data['action_form']   	= $this->page_default.'/add_process/';
			$data['btn_value']   	= 'Tambah';
			$data['id']   			= '';
			//--------------------------Form---------------------------//
			$data_lokasi_diklat 	= $this->function2->get_data_detail('tbl_lokasi_diklat', 'name', 'name')->result();
			$data_diklat 			= $this->function2->get_data_detail('tbl_diklat', 'name', 'name')->result();
			$data_angkatan 			= $this->function2->get_data_detail('tbl_angkatan', 'name', 'id')->result();
			
			$data['form'] 	= "";
			$data['form']  .= $this->function2->get_row('Tanggal Mulai', 'dt_from', 10, 3, 0, '', '', '', 'datepicker1');
			$data['form']  .= $this->function2->get_row('Tanggal Selesai', 'dt_to', 10, 3, 0, '', '', '', 'datepicker2');
			$data['form']  .= $this->function2->get_row('Lokasi Diklat', 'lokasi', 20, 2, $data_lokasi_diklat, '', 'id', 'name', '');
			$data['form']  .= $this->function2->get_row('Waktu', 'waktu', 20, 1, 0, '', '', '', '');
			$data['form']  .= $this->function2->get_row('Diklat', 'diklat', 20, 2, $data_diklat, '', 'id', 'name', '');
			$data['form']  .= $this->function2->get_row('Angkatan', 'angkatan', 20, 2, $data_angkatan, '', 'id', 'name', '');
			$data['form']  .= $this->function2->get_row('Tahun', 'tahun', 10, 1, 0, '', '', '', '');
			//---------------------------------------------------------//	
			$this->load->view('v_template', $data);
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}
	
	function update($id)
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$data['main_view']   	= 'v_form';
			$data['ket_header'] 	= $this->ket_update;
			$data['action_form']   	= $this->page_default.'/update_process/';
			$data['btn_value']   	= 'Simpan';
			$data['id']   			= $id;
			//--------------------------Form---------------------------//
			$table_row 				= $this->function2->get_table_by_id($this->table_default, 'id', $id)->row();
			$data_lokasi_diklat 	= $this->function2->get_data_detail('tbl_lokasi_diklat', 'name', 'name')->result();
			$data_diklat 			= $this->function2->get_data_detail('tbl_diklat', 'name', 'name')->result();
			$data_angkatan 			= $this->function2->get_data_detail('tbl_angkatan', 'name', 'id')->result();
			
			$data['form'] 	= "";
			$data['form']  .= $this->function2->get_row('Tanggal Mulai', 'dt_from', 10, 3, 0, $table_row->dt_from, '', '', 'datepicker1');
			$data['form']  .= $this->function2->get_row('Tanggal Selesai', 'dt_to', 10, 3, 0, $table_row->dt_to, '', '', 'datepicker2');
			$data['form']  .= $this->function2->get_row('Lokasi Diklat', 'lokasi', 20, 2, $data_lokasi_diklat, $table_row->lokasi, 'id', 'name', '');
			$data['form']  .= $this->function2->get_row('Waktu', 'waktu', 20, 1, 0, $table_row->waktu, '', '', '');
			$data['form']  .= $this->function2->get_row('Diklat', 'diklat', 20, 2, $data_diklat, $table_row->diklat, 'id', 'name', '');
			$data['form']  .= $this->function2->get_row('Angkatan', 'angkatan', 20, 2, $data_angkatan, $table_row->angkatan, 'id', 'name', '');
			$data['form']  .= $this->function2->get_row('Tahun', 'tahun', 10, 1, 0, $table_row->tahun, '', '', '');
			//---------------------------------------------------------//			
			$this->load->view('v_template',$data);
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}

	function delete($id)
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$data['main_view']   	= 'v_form';
			$data['ket_header'] 	= 'Konfirmasi delete';
			$data['action_form']   	= $this->page_default.'/delete_process/';
			$data['btn_value']   	= 'Konfirmasi';
			$data['id']   			= $id;
			//--------------------------Form---------------------------//
			$data['form'] 	= "";
			$data['form']  .= $this->function2->get_row('Masukkan password admin', 'password', 20, 7, 0, '', '', '', '');
			//---------------------------------------------------------//			
			$this->load->view('v_template',$data);
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}

	function add_process()
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$table_col = array('dt_from'  	=> $this->input->post('dt_from')
							  ,'dt_to'  	=> $this->input->post('dt_to')
							  ,'lokasi'  	=> $this->input->post('lokasi')
							  ,'waktu'  	=> $this->input->post('waktu')
							  ,'diklat'  	=> $this->input->post('diklat')
							  ,'angkatan'  	=> $this->input->post('angkatan')
							  ,'tahun'  	=> $this->input->post('tahun')
			);
			$this->function2->add($this->table_default, $table_col);
			redirect($this->page_default);
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}

	function update_process()
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$table_col = array('dt_from'  	=> $this->input->post('dt_from')
							  ,'dt_to'  	=> $this->input->post('dt_to')
							  ,'lokasi'  	=> $this->input->post('lokasi')
							  ,'waktu'  	=> $this->input->post('waktu')
							  ,'diklat'  	=> $this->input->post('diklat')
							  ,'angkatan'  	=> $this->input->post('angkatan')
							  ,'tahun'  	=> $this->input->post('tahun')
			);
			$this->function2->update($this->table_default, $table_col, 'id', $this->input->post('id'));
			redirect($this->page_default);
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}
	
	function delete_process()
	{
		if ($this->session->userdata('id') == TRUE)
		{
			if ($this->function2->check_password($this->input->post('password')) == TRUE)
			{
				$this->function2->delete($this->table_default, 'id', $this->input->post('id'));
				redirect($this->page_default);
			} else {
				redirect($this->page_default);
				
			}
		}
	}
	
	function excel()
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$data['ket_header'] = $this->ket_excel;
			$data['filename'] = 'user '.date('Y-m-d H:i:s');

			$coll_header = array('Tanggal Mulai'
							  ,'Tanggal Selesai'
							  ,'Lokasi'
							  ,'Waktu'
							  ,'Diklat'
							  ,'Angkatan'
							  ,'Tahun'
			);
			$coll_detail = array('dt_from'
							  ,'dt_to'
							  ,'lokasi'
							  ,'waktu'
							  ,'diklat'
							  ,'angkatan'
							  ,'tahun'
			);
			$coll_function = array('default'
							  ,'default'
							  ,'tbl_lokasi_diklat'
							  ,'default'
							  ,'tbl_diklat'
							  ,'tbl_angkatan'
							  ,'default'
			);
			$data['data'] 		= $this->function2->get_excel($this->table_default, $coll_header, $coll_detail, $coll_function, "dt_from");
			$this->load->view('v_excel',$data);
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}
	
	function detail($id)
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$data['main_view']   	= 'v_detail_pelaksanaan';
			$data['ket_active']   	= 1;
			$data['ket_page']   	= $this->page_default;
			$data['ket_header'] 	= 'Detail Instansi';
			$data['id']   			= $id;
			$data['action_form']   	= $this->page_default.'/detail_update/';
			//--------------------------Form---------------------------//
			$data['data'] 				= $this->function2->get_table_by_id($this->table_default, 'id', $id)->row();
			$data['data_lokasi_diklat'] = $this->function2->get_data_detail('tbl_lokasi_diklat', 'name', 'name')->result();
			$data['data_diklat']		= $this->function2->get_data_detail('tbl_diklat', 'name', 'name')->result();
			$data['data_angkatan'] 		= $this->function2->get_data_detail('tbl_angkatan', 'name', 'id')->result();
			//---------------------------------------------------------//			
			$this->load->view('v_template',$data);
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}	

	function detail_update()
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$table_col = array('dt_from'  	=> $this->input->post('dt_from')
							  ,'dt_to'  	=> $this->input->post('dt_to')
							  ,'lokasi'  	=> $this->input->post('lokasi')
							  ,'waktu'  	=> $this->input->post('waktu')
							  ,'diklat'  	=> $this->input->post('diklat')
							  ,'angkatan'  	=> $this->input->post('angkatan')
							  ,'tahun'  	=> $this->input->post('tahun')
			);
			$this->function2->update($this->table_default, $table_col, 'id', $this->input->post('id'));
			
			//----------------Upload------------------//
			$this->function2->upload_detail('susunan_acara', $this->input->post('id').'.xls', 'susunan_acara');
			$this->function2->upload_detail('absensi', $this->input->post('id').'.doc', 'absensi');
			$this->function2->upload_detail('doa', $this->input->post('id').'.doc', 'doa');
			$this->function2->upload_detail('laporan_panitia', $this->input->post('id').'.doc', 'laporan_panitia');
			$this->function2->upload_detail('sambutan_kapusdiklat', $this->input->post('id').'.doc', 'sambutan_kapusdiklat');
			$this->function2->upload_detail('hasil_evaluasi', $this->input->post('id').'.doc', 'hasil_evaluasi');
			//----------------------------------------//
			
			redirect($this->page_default."/detail/".$this->input->post('id'));
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}

	function detail_peserta($id)
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$data['main_view']   	= 'v_detail_pelaksanaan';
			$data['ket_active']   	= 2;
			$data['ket_page']   	= $this->page_default;
			$data['ket_header'] 	= 'Detail Instansi';
			$data['id']   			= $id;
			$data['action_form']   	= $this->page_default.'/detail_peserta_update/';
			//--------------------------Form---------------------------//
			$data['data'] 			= $this->function2->get_peserta_pelaksanaan($id)->result();
			$data['data_peserta']   = $this->function2->get_data_detail('tbl_peserta', 'name', 'name')->result();
			
			//---------------------------------------------------------//			
			$this->load->view('v_template',$data);
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}	
	
	function detail_peserta_update()
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$table_col = array('pelaksanaan'  	=> $this->input->post('id')
							  ,'peserta'  		=> $this->input->post('peserta')
			
			);
			$this->function2->add("peserta_per_pelaksanaan", $table_col);
			
			redirect($this->page_default."/detail_peserta/".$this->input->post('id'));
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}
	
	function delete_peserta($id, $path)
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$this->function2->delete("peserta_per_pelaksanaan", 'id', $id);
			redirect($this->page_default."/detail_peserta/".$path);
		}
	}


	function detail_pengajar($id)
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$data['main_view']   	= 'v_detail_pelaksanaan';
			$data['ket_active']   	= 3;
			$data['ket_page']   	= $this->page_default;
			$data['ket_header'] 	= 'Detail Instansi';
			$data['id']   			= $id;
			$data['action_form']   	= $this->page_default.'/detail_pengajar_update/';
			//--------------------------Form---------------------------//
			$data['data'] 				= $this->function2->get_pengajar_pelaksanaan($id)->result();
			$data['data_mata_diklat']   = $this->function2->get_data_detail('tbl_mata_diklat', 'name', 'name')->result();
			$data['data_pengajar']   	= $this->function2->get_data_detail('tbl_pengajar', 'name', 'name')->result();
			
			//---------------------------------------------------------//			
			$this->load->view('v_template',$data);
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}	


	
	function detail_pengajar_update()
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$table_col = array('pelaksanaan'  	=> $this->input->post('id')
							  ,'mata_diklat'  		=> $this->input->post('mata_diklat')
							  ,'pengajar'  		=> $this->input->post('pengajar')
			
			);
			$this->function2->add("pengajar_per_pelaksanaan", $table_col);
			
			redirect($this->page_default."/detail_pengajar/".$this->input->post('id'));
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}
	
	function delete_pengajar($id, $path)
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$this->function2->delete("pengajar_per_pelaksanaan", 'id', $id);
			redirect($this->page_default."/detail_pengajar/".$path);
		}
	}

	function detail_foto($id)
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$data['main_view']   	= 'v_detail_pelaksanaan';
			$data['ket_active']   	= 4;
			$data['ket_page']   	= $this->page_default;
			$data['ket_header'] 	= 'Detail Instansi';
			$data['id']   			= $id;
			$data['action_form']   	= $this->page_default.'/detail_foto_update/';
			//--------------------------Form---------------------------//
			$data['data'] 				= $this->function2->get_foto_pelaksanaan($id)->result();
			
			//---------------------------------------------------------//			
			$this->load->view('v_template',$data);
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}	
	
	function detail_foto_update()
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$table_col = array('pelaksanaan'  	=> $this->input->post('id')
							  ,'title'  	=> $this->input->post('title')
			);
			$this->function2->add("tbl_foto_per_pelaksanaan", $table_col);
			$id = $this->function2->get_last("tbl_foto_per_pelaksanaan");;
			$this->function2->upload_detail('foto_diklat', $id.'.jpg', 'file');
			redirect($this->page_default."/detail_foto/".$this->input->post('id'));
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}
	
	function delete_foto($id, $path)
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$this->function2->delete("tbl_foto_per_pelaksanaan", 'id', $id);
			redirect($this->page_default."/detail_foto/".$path);
		}
	}
	
	function detail_report($id)
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$data['main_view']   	= 'v_detail_pelaksanaan';
			$data['ket_active']   	= 5;
			$data['ket_page']   	= $this->page_default;
			$data['ket_header'] 	= 'Detail Instansi';
			$data['id']   			= $id;
			$this->load->view('v_template',$data);
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}	
		


	function delete_file($file, $name, $id)
	{
		if ($this->session->userdata('id') == TRUE)
		{
			unlink('./public/file/'.$file.'/'.$name); 			
			redirect($this->page_default."/detail/".$id);
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}
	
}
?>
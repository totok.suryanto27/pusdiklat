<?php
class C_mata_diklat extends Controller {

	function C_mata_diklat  ()
	{
		parent::Controller();	
		$this->load->helper('flexigrid');
		$this->load->model('function2', '', TRUE);
	}

	public $ajax_default 	= "ajax_mata_diklat";
	public $table_default 	= "tbl_mata_diklat";
	public $page_default 	= "c_mata_diklat";
	public $ket_index 		= "Mata Diklat";
	public $ket_add			= "Tambah Mata Diklat";
	public $ket_update 		= "Rubah Mata Diklat";
	public $ket_excel 		= "Data Mata Diklat";

	function index()
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$data['ket_header'] 	= $this->ket_index;
			$data['main_view'] 		= 'v_table';
			//--------------------------Table---------------------------//
			$colModel['no'] 		= array('No.',20,FALSE,'left',3,FALSE);
			if ($this->session->userdata('access') == "1") $colModel['action'] = array('',40,FALSE,'left',3,FALSE);
			$colModel['name'] 		= array('Mata Diklat',200,TRUE,'left',2,FALSE);
			$colModel['diklat'] 	= array('Diklat',120,TRUE,'left',3,FALSE);
			//---------------------------------------------------------//			
			$gridParams = array(
				'title' 				=> 'Selamat datang '.$this->session->userdata('name'),
				'rp' 					=> $this->function2->rp,
				'rpOptions' 			=> $this->function2->rpOptions,
				'pagestat' 				=> $this->function2->pagestat,
				'blockOpacity' 			=> $this->function2->blockOpacity,
				'showTableToggleBtn' 	=> $this->function2->showTableToggleBtn
			);

			if ($this->session->userdata('access') == "1") {
				$buttons[] = array('<a href='.base_url().'index.php/'.$this->page_default.'/add>Tambah Data</a>','add','test');
				$buttons[] = array('separator');
			}
			$buttons[] = array('<a href='.base_url().'index.php/'.$this->page_default.'/excel>Export Excel</a>','','test');
			
			$grid_js = build_grid_js('flex1',site_url("ajax/".$this->ajax_default),$colModel,'name','ASC',$gridParams,$buttons);
			
			$data['js_grid'] 		= $grid_js;
			$data['version'] 		= "0.36";
			$data['download_file'] 	= "Flexigrid_CI_v0.36.rar";
			
			$this->load->view('v_template',$data);
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}
	
	function add()
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$data['main_view']   	= 'v_form';
			$data['ket_header'] 	= $this->ket_add;
			$data['action_form']   	= $this->page_default.'/add_process/';
			$data['btn_value']   	= 'Tambah';
			$data['id']   			= '';
			//--------------------------Form---------------------------//
			$data_diklat 	= $this->function2->get_data_detail('tbl_diklat', 'name', 'name')->result();
			$data['form'] 	= "";
			$data['form']  .= $this->function2->get_row('Mata Diklat', 'name', 80, 1, 0, '', '', '', '');
			$data['form']  .= $this->function2->get_row('Diklat', 'diklat', 20, 2, $data_diklat, '', 'id', 'name', '');
			//---------------------------------------------------------//			
			$this->load->view('v_template', $data);
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}
	
	function update($id)
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$data['main_view']   	= 'v_form';
			$data['ket_header'] 	= $this->ket_update;
			$data['action_form']   	= $this->page_default.'/update_process/';
			$data['btn_value']   	= 'Simpan';
			$data['id']   			= $id;
			//--------------------------Form---------------------------//
			$table_row 		= $this->function2->get_table_by_id($this->table_default, 'id', $id)->row();
			$data_diklat 	= $this->function2->get_data_detail('tbl_diklat', 'name', 'name')->result();
			$data['form'] 	= "";
			$data['form']  .= $this->function2->get_row('Mata Diklat', 'name', 80, 1, 0, $table_row->name, '', '', '');
			$data['form']  .= $this->function2->get_row('Diklat', 'diklat', 20, 2, $data_diklat, $table_row->diklat, 'id', 'name', '');
			//---------------------------------------------------------//			
			$this->load->view('v_template',$data);
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}

	function delete($id)
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$data['main_view']   	= 'v_form';
			$data['ket_header'] 	= 'Konfirmasi delete';
			$data['action_form']   	= $this->page_default.'/delete_process/';
			$data['btn_value']   	= 'Konfirmasi';
			$data['id']   			= $id;
			//--------------------------Form---------------------------//
			$data['form'] 	= "";
			$data['form']  .= $this->function2->get_row('Masukkan password admin', 'password', 20, 7, 0, '', '', '', '');
			//---------------------------------------------------------//			
			$this->load->view('v_template',$data);
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}

	function add_process()
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$table_col = array('name'  		=> $this->input->post('name')
							  ,'diklat'  	=> $this->input->post('diklat')
			);
			$this->function2->add($this->table_default, $table_col);
			redirect($this->page_default);
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}

	function update_process()
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$table_col = array('name'  		=> $this->input->post('name')
							  ,'diklat'  	=> $this->input->post('diklat')
			);
			$this->function2->update($this->table_default, $table_col, 'id', $this->input->post('id'));
			redirect($this->page_default);
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}
	
	function delete_process()
	{
		if ($this->session->userdata('id') == TRUE)
		{
			if ($this->function2->check_password($this->input->post('password')) == TRUE)
			{
				$this->function2->delete($this->table_default, 'id', $this->input->post('id'));
				redirect($this->page_default);
			} else {
				redirect($this->page_default);
				
			}
		}
	}
	
	function excel()
	{
		if ($this->session->userdata('id') == TRUE)
		{
			$data['ket_header'] = $this->ket_excel;
			$data['filename'] = 'Mata Diklat '.date('Y-m-d H:i:s');

			$coll_header = array('Mata Diklat'
							  ,'Diklat'
			);
			$coll_detail = array('name'
							  ,'diklat'
			);
			$coll_function = array('default'
							  ,'tbl_diklat'
			);
			$data['data'] 		= $this->function2->get_excel($this->table_default, $coll_header, $coll_detail, $coll_function);
			$this->load->view('v_excel',$data);
		}
		else
		{
			redirect('c_login', 'refresh');
		}
	}
	
}
?>
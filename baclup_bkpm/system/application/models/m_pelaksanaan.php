<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_pelaksanaan extends Model 
{
	public function M_pelaksanaan()
    {
        parent::Model();
		$this->CI =& get_instance();
    }
	
	public function get_all_data() 
	{
        $this->db->select('*');
		$this->db->from('tbl_pelaksanaan');
		$this->db->order_by("tahun", "asc");
		$this->db->order_by("diklat", "asc"); 
		$this->db->order_by("angkatan", "asc"); 
 
        $this->CI->flexigrid->build_query();
        $return['records'] = $this->db->get();

        $this->db->select("count(id) as record_count")->from('tbl_pelaksanaan');
		
        $this->CI->flexigrid->build_query(FALSE);
        $record_count = $this->db->get();
        $row = $record_count->row();
        $return['record_count'] = $row->record_count;

        return $return;
	}

}
?>
<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Function2 extends Model 
{
	public function Function2()
    {
        parent::Model();
		$this->CI =& get_instance();
    }

//----------------Function---------------//	
	public $rp 		  			= 20;
	public $rpOptions 			= '[20,40,60,80,100]';
	public $pagestat  			= 'Displaying: {from} to {to} of {total} items.';
	public $blockOpacity		= 0.5;
	public $showTableToggleBtn	= true;
//----------------Transaction---------------//	
	public function get_last($table) 
	{
		$this->db->select('id');
		$this->db->from($table);
		$this->db->order_by("id", "DESC"); 
		$record_count = $this->db->get();
        $row = $record_count->row();
        $rett = $row->id;
		return $rett;
	}

	function check_password($password)
	{
		$query = $this->db->get_where('tbl_user', array('id' => $this->session->userdata('id'), 'password' => $password), 1, 0);
		
		if ($query->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	function get_table_by_id($table, $where_id, $where)
	{
		$this->db->select('*');
		$this->db->where($where_id, $where);
		return $this->db->get($table);
	}	
	
	function get_excel($table, $coll_header, $coll_detail, $coll_table, $sort_by=Null) 
	{
		$data = "<tr bgcolor='#DDDDDD'>";
		for ($d=0, $x=count($coll_header); $d<$x; $d++) {
			$data .= "<td>".$coll_header[$d]."</td>";
		}
		$data .= "</tr>";
		
		$this->db->select('*');
		$this->db->from($table);
		if($sort_by==Null) $sorted="name"; else $sorted=$sort_by;
		$this->db->order_by($sorted, "ASC"); 
		$record_count = $this->db->get();
		$row = $record_count->result();
		foreach ($row as $data_col) {
			$data .= "<tr>";
			for ($d=0, $x=count($coll_detail); $d<$x; $d++) {
				if($coll_table[$d]=="default")
					$data .= "<td>".$data_col->$coll_detail[$d]."</td>";
				else
					$data .= "<td>".$this->get_data_detail($coll_table[$d], 'name', 'name', $data_col->$coll_detail[$d])."</td>";
			}
			$data .= "</tr>";
		}
		return $data;
	}

	function upload_file($path, $name) 
	{
		if($_FILES['file']['name']!="") {
			if (file_exists('./public/file/'.$path.'/'.$name)) 	unlink('./public/file/'.$path.'/'.$name); 
			$config['upload_path'] 		= './public/file/'.$path;
			$config['allowed_types'] 	= '*';
			$config['max_size']			= '10000';
			$config['max_width']  		= '10000';
			$config['max_height']  		= '10000';
     		$config['file_name'] 		= $name;
			
			$this->load->library('upload', $config);
			if($this->upload->do_upload('file')) {
				$tp = $this->upload->data();
			}
		}
	}

	function upload_detail($path, $name, $file) 
	{
		if($_FILES[$file]['name']!="") {
			if (file_exists('./public/file/'.$path.'/'.$name)) 	unlink('./public/file/'.$path.'/'.$name); 
			$config['upload_path'] 		= './public/file/'.$path;
			$config['allowed_types'] 	= '*';
			$config['max_size']			= '10000';
			$config['max_width']  		= '10000';
			$config['max_height']  		= '10000';
     		$config['file_name'] 		= $name;


			
			$this->load->library('upload', $config);
			if($this->upload->do_upload($file)) {
				$tp = $this->upload->data();
			} else {
				echo $this->upload->display_errors();
				exit();	
			}
		}
	}
	
	
	
	function update($table, $table_value, $where, $where_value)
	{
		$this->db->where($where, $where_value);
		$this->db->update($table, $table_value);
	}
	
	function add($table, $table_value)
	{
		$this->db->insert($table, $table_value);
	}
	
	function delete($table, $where, $where_value)
	{
		$this->db->where($where, $where_value);
		$this->db->delete($table);
	}


//----------------Form----------------------//
	public function get_row($title, $name, $size, $type, $value_col, $value, $col1, $col2, $dt_id)
	{
		if($value==1) $checked = " checked"; else $checked = "";
		$form = "<tr>";
		$form .= "<td>".$title."</td>";
		$form .= "<td>:</td>";
	   	$form .= "<td>";
		if($type==1) $form .= "<input name='".$name."' type='text' size='".$size."' value='".$value."' />";
		if($type==2) {
			$form .= "<select name='".$name."'>";
			foreach ($value_col as $col) {
				if($value==$col->$col1) $string="selected"; else $string="";
				$form .= "<option value=".$col->$col1." ".$string.">".$col->$col2."</option>";
			}
			$form .= "</select>";
		}
		if($type==3) $form .= "<input id='".$dt_id."' name='".$name."' type='text' size='".$size."' value='".$value."'";
		if($type==4) $form .= "<input name='".$name."' type='file' size='".$size."' value='".$value."' />";
		if($type==5) $form .= "<textarea name='".$name."' cols='51' rows='6'>".$value."</textarea>";
		if($type==6) $form .= "<input type='checkbox' name='".$name."' value='1'". $checked.">".$dt_id;
		if($type==7) $form .= "<input name='".$name."' type='password' size='".$size."' value='".$value."' />";
      	$form .= "</td></tr>";
		return $form;
	}

	public function get_detail($title, $name, $size, $type, $value_col, $value, $col1, $col2, $dt_id)
	{
		if($value==1) $checked = " checked"; else $checked = "";
		$form = "<tr>";
		$form .= "<td>".$title."</td>";
		$form .= "<td>:</td>";
	   	$form .= "<td>";
		if($type==1) $form .= "<input name='".$name."' type='text' size='".$size."' value='".$value."' disabled='disabled' />";
		if($type==2) {
			$form .= "<select name='".$name."' disabled='disabled'>";
			foreach ($value_col as $col) {
				if($value==$col->$col1) $string="selected"; else $string="";
				$form .= "<option value=".$col->$col1." ".$string.">".$col->$col2."</option>";
			}
			$form .= "</select>";
		}
		if($type==3) $form .= "<input disabled='disabled' name='".$name."' id='".$dt_id."' type='text' size='".$size."' value='".$value."' onFocus='checkempty();return checkdate(this.form.fromdate.value)' />";
		if($type==4) $form .= "<input disabled='disabled' name='".$name."' type='file' size='".$size."' value='".$value."' />";
		if($type==5) $form .= "<textarea disabled='disabled' name='".$name."' cols='51' rows='6'>".$value."</textarea>";
		if($type==6) $form .= "<input disabled='disabled' type='checkbox' name='".$name."' value='1'". $checked.">".$dt_id;
		if($type==7) $form .= "<input disabled='disabled' name='".$name."' type='password' size='".$size."' value='".$value."' />";
      	$form .= "</td></tr>";
		return $form;
	}


//----------------Dropdown----------------------//
	public function get_access($id=Null) 
	{
		$this->db->select('id');
		$this->db->select('name');
		$this->db->from('tbl_access');
		$this->db->order_by("id", "ASC"); 
		if($id!=Null) $this->db->where("id", $id);
		$record_count = $this->db->get();
		if($id!=Null) {
        	$row = $record_count->row();
        	$rett = $row->name;
		} else {
			$rett = $record_count;
		}
		return $rett;
	}

	public function get_history($id) 
	{
		$this->db->select('t2.*');
		$this->db->from('pengajar_per_pelaksanaan t1');
		$this->db->join('tbl_pelaksanaan t2', 't1.pelaksanaan = t2.id', 'left');
		$this->db->where("t1.pengajar", $id);
		$this->db->order_by("t2.dt_from", "ASC"); 
		$record_count = $this->db->get();
		$rett = $record_count;
		return $rett;
		
	}

	public function get_peserta_pelaksanaan($id) 
	{
		$this->db->select('t1.id');
		$this->db->select('t2.name AS peserta');
		$this->db->select('t3.name AS instansi');
		$this->db->from('peserta_per_pelaksanaan t1');
		$this->db->join('tbl_peserta t2', 't1.peserta = t2.id', 'left');
		$this->db->join('tbl_instansi t3', 't2.instansi = t3.id', 'left');
		$this->db->where("t1.pelaksanaan", $id);
		$this->db->order_by("t2.name", "ASC"); 
		$record_count = $this->db->get();
		$rett = $record_count;
		return $rett;
	}

	public function get_pengajar_pelaksanaan($id) 
	{
		$this->db->select('t1.id');
		$this->db->select('t2.name AS pengajar');
		$this->db->select('t3.name AS mata_diklat');
		$this->db->from('pengajar_per_pelaksanaan t1');
		$this->db->join('tbl_pengajar t2', 't1.pengajar = t2.id', 'left');
		$this->db->join('tbl_mata_diklat t3', 't1.mata_diklat = t3.id', 'left');
		$this->db->where("t1.pelaksanaan", $id);
		$this->db->order_by("t1.id", "ASC"); 
		$record_count = $this->db->get();
		$rett = $record_count;
		return $rett;
	}

	public function get_foto_pelaksanaan($id) 
	{
		$this->db->select('id');
		$this->db->select('title');
		$this->db->from('tbl_foto_per_pelaksanaan');
		$this->db->where("pelaksanaan", $id);
		$this->db->order_by("id", "ASC"); 
		$record_count = $this->db->get();
		$rett = $record_count;
		return $rett;
	}

	public function get_data_detail($table, $value, $order, $id=Null) 
	{
		$this->db->select('id');
		$this->db->select($value);
		$this->db->from($table);
		$this->db->order_by($order, "ASC"); 
		if($id!=Null) $this->db->where("id", $id);
		$record_count = $this->db->get();
		$rett = "";
		if ($record_count->num_rows() > 0)
		{
			if($id!=Null) {
        		$row = $record_count->row();
        		$rett = $row->$value;
			} else {
				$rett = $record_count;
			}
		}
		return $rett;
	}

}
?>
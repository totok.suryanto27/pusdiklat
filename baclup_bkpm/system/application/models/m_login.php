<?php
class m_login extends Model {
	function m_login()
	{
		parent::Model();
	}
	
	function check_user($username, $password)
	{
		$query = $this->db->get_where('tbl_user', array('username' => $username, 'password' => $password), 1, 0);
		
		if ($query->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	function get_sess($username)
	{
		$this->db->select('id');
		$this->db->select('name');
		$this->db->select('access');
		$this->db->where('username', $username);
		$row = $this->db->get('tbl_user');
		$return = $row->row();
        return $return;
	}

}

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>jQuery Tab</title>

<style type="text/css">

a{outline:none;}

	#tabContaier{
		background:#ecece8;
		border:1px solid #f36500;
		margin:0px auto;
		padding:20px;
		position:relative;
		margin:0; padding:0;
		
	}
	#tabContaier ul{
		overflow:hidden;
		border-right:1px solid #fff;
		height:35px;
		position:absolute;
		z-index:100;
	}
	#tabContaier li{
		float:left;
		list-style:none;
	}	
	#tabContaier li a{
		background:#ddd;
		border:1px solid #fcfcfc;
		border-right:0;
		color:#666;
		cursor:pointer;
		display:block;
		height:35px;
		line-height:35px;
		padding:0 30px;
		text-decoration:none;
		text-transform:uppercase;
	}
	#tabContaier li a:hover{
		background:#eee;
	}
	#tabContaier li a.active{
		background:#fbfbfb;
		border:1px solid #fff;
		border-right:0;
		color:#333;
	}
	
	.tabDetails{
		background:#fbfbfb;
		border:1px solid #fff;
		margin:34px 0 0;
	}
	.tabContents{
		padding:20px
	}
	.tabContents h1{
		font:normal 24px/1.1em Georgia, "Times New Roman", Times, serif;
		padding:0 0 10px;
	}
	.tabContents p{
		padding:0 0 10px;
	}
	
#tabContaier .tabDetails #tab1 form .adminmember tr td {
	font-family: Verdana, Geneva, sans-serif;
	font-size:13px;

}
</style>


</head>

<body>
<br />
<div id="tabContaier">
	
    <ul>
    	<li><a class="<?php if($ket_active==1) echo "active"; ?>" href="<?=$this->config->item('base_url');?>index.php/<?=$ket_page?>/detail/<?=$id?>">Detail</a></li>
<?php if(!isset($ket_hidd)) { ?>
    	<li><a class="<?php if($ket_active==2) echo "active"; ?>" href="<?=$this->config->item('base_url');?>index.php/<?=$ket_page?>/detail_history/<?=$id?>">Sejarah Mengikuti Diklat</a></li>
<?php } ?>
    </ul>
    
    
<div class="tabDetails">
<?php if($ket_active==1) { ?>
<div id="tab1" class="tabContents">
      <table class="adminmember" cellpadding="0" cellspacing="10" border="0">
        <?=$form;?>
      </table>
</div>
<?php } ?>


<?php if($ket_active==2) { ?>
<div id="tab1" class="tabContents">
	<br />
    <table class="adminmember" cellpadding="5" cellspacing="0" border="1">
	<tr bgcolor="#BBBBBB">
    	<td>No.</td>
    	<td>View</td>
    	<td>Tanggal</td>
    	<td>Diklat</td>
    	<td>Angkatan</td>
    	<td>Lokasi</td>
    	<td>Waktu</td>
    </tr>
    <?=$form;?>
    </table>
</div>
<?php } ?>

</div>
</div>




<script type="text/javascript" src="<?=$this->config->item('base_url');?>public/tab/jquery-1.4.2.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$(".tabContents").hide(); // Hide all tab conten divs by default
		$(".tabContents:first").show(); // Show the first div of tab content by default
		
		$("#tabContaier ul li a").click(function(){ //Fire the click event
			
			var activeTab = $(this).attr("href"); // Catch the click link
			$("#tabContaier ul li a").removeClass("active"); // Remove pre-highlighted link
			$(this).addClass("active"); // set clicked link to highlight state
			$(".tabContents").hide(); // hide currently visible tab content div
			$(activeTab).fadeIn(); // show the target tab content div by matching clicked link.
		});
	});


	function perusahaanDiv() { 
		document.getElementById('nm').innerHTML = 'Nama Perusahaan';
		document.getElementById('ktr').innerHTML = 'No. Kantor';	
	}

	function peroranganDiv() { 
		document.getElementById('nm').innerHTML = 'Nama Perorangan';
		document.getElementById('ktr').innerHTML = 'No. Rumah';	
	}
</script>

</body>
</html>

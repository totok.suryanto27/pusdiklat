<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>jQuery Tab</title>

<style type="text/css">

a{outline:none;}

	#tabContaier{
		background:#ecece8;
		border:1px solid #f36500;
		margin:0px auto;
		padding:20px;
		position:relative;
		margin:0; padding:0;
		
	}
	#tabContaier ul{
		overflow:hidden;
		border-right:1px solid #fff;
		height:35px;
		position:absolute;
		z-index:100;
	}
	#tabContaier li{
		float:left;
		list-style:none;
	}	
	#tabContaier li a{
		background:#ddd;
		border:1px solid #fcfcfc;
		border-right:0;
		color:#666;
		cursor:pointer;
		display:block;
		height:35px;
		line-height:35px;
		padding:0 30px;
		text-decoration:none;
		text-transform:uppercase;
	}
	#tabContaier li a:hover{
		background:#eee;
	}
	#tabContaier li a.active{
		background:#fbfbfb;
		border:1px solid #fff;
		border-right:0;
		color:#333;
	}
	
	.tabDetails{
		background:#fbfbfb;
		border:1px solid #fff;
		margin:34px 0 0;
	}
	.tabContents{
		padding:20px
	}
	.tabContents h1{
		font:normal 24px/1.1em Georgia, "Times New Roman", Times, serif;
		padding:0 0 10px;
	}
	.tabContents p{
		padding:0 0 10px;
	}
	
#tabContaier .tabDetails #tab1 form .adminmember tr td {
	font-family: Verdana, Geneva, sans-serif;
	font-size:13px;

}
</style>

<link rel="stylesheet" href="<?=$this->config->item('base_url');?>public/calender/css/zebra_datepicker.css" type="text/css">
<script type="text/javascript" src="<?=$this->config->item('base_url');?>public/calender/js/jquery-1.7.2.js"></script>
<script type="text/javascript" src="<?=$this->config->item('base_url');?>public/calender/js/zebra_datepicker.js"></script>
<script type="text/javascript" src="<?=$this->config->item('base_url');?>public/calender/js/functions.js"></script>

</head>

<body>
<br />
<div id="tabContaier">
	
    <ul>
    	<li><a class="<?php if($ket_active==1) echo "active"; ?>" href="<?=$this->config->item('base_url');?>index.php/<?=$ket_page?>/detail/<?=$id?>">Detail</a></li>
    	<li><a class="<?php if($ket_active==2) echo "active"; ?>" href="<?=$this->config->item('base_url');?>index.php/<?=$ket_page?>/detail_peserta/<?=$id?>">Peserta</a></li>
    	<li><a class="<?php if($ket_active==3) echo "active"; ?>" href="<?=$this->config->item('base_url');?>index.php/<?=$ket_page?>/detail_pengajar/<?=$id?>">Pengajar</a></li>
    	<li><a class="<?php if($ket_active==4) echo "active"; ?>" href="<?=$this->config->item('base_url');?>index.php/<?=$ket_page?>/detail_foto/<?=$id?>">Foto</a></li>
    	<li><a class="<?php if($ket_active==5) echo "active"; ?>" href="<?=$this->config->item('base_url');?>index.php/<?=$ket_page?>/detail_report/<?=$id?>">Report</a></li>
    </ul>
    
    
<div class="tabDetails">
<?php if($ket_active==1) { ?>
<div id="tab1" class="tabContents">
	<form name="form1" method="post" action="<?=$this->config->item('base_url');?>index.php/<?=$action_form?>" enctype="multipart/form-data">
      <table class="adminmember" cellpadding="0" cellspacing="10" border="0">
        <tr>
        	<td>Tanggal Mulai</td>
            <td>:</td>
        	<td><input id="datepicker1" type="text" value="<?=$data->dt_from?>" name="dt_from" size="10" /></td>
        </tr>
        <tr>
        	<td>Tanggal Selesai</td>
            <td>:</td>
        	<td><input id="datepicker2" type="text" value="<?=$data->dt_to?>" name="dt_to" size="10" /></td>
        </tr>
        <tr>
        	<td>Diklat</td>
            <td>:</td>
        	<td>
            	<select name="diklat">
            		<?php foreach ($data_diklat as $col) { ?>
                    	<option <?php if($col->id==$data->diklat) echo "selected"; ?> value="<?=$col->id?>"><?=$col->name?></option>
            		<?php } ?>
                </select>
            </td>
        </tr>
        <tr>
        	<td>Angkatan</td>
            <td>:</td>
        	<td>
            	<select name="angkatan">
            		<?php foreach ($data_angkatan as $col) { ?>
                    	<option <?php if($col->id==$data->angkatan) echo "selected"; ?> value="<?=$col->id?>"><?=$col->name?></option>
            		<?php } ?>
                </select>
            </td>
        </tr>
        <tr>
        	<td>Tahun</td>
            <td>:</td>
        	<td><input type="text" value="<?=$data->tahun?>" name="tahun" size="10" /></td>
        </tr>

        <tr>
        	<td>Lokasi</td>
            <td>:</td>
        	<td>
            	<select name="lokasi_diklat">
            		<?php foreach ($data_lokasi_diklat as $col) { ?>
                    	<option <?php if($col->id==$data->lokasi) echo "selected"; ?> value="<?=$col->id?>"><?=$col->name?></option>
            		<?php } ?>
                </select>
            </td>
        </tr>
        <tr>
        	<td>Waktu</td>
            <td>:</td>
        	<td><input type="text" value="<?=$data->waktu?>" name="waktu" size="10" /></td>
        </tr>
        <tr>
        	<td>Susunan Acara</td>
            <td>:</td>
        	<td>
            	<?php $filex="susunan_acara"; $ex=".xls"; ?>
            	<?php if (file_exists('./public/file/'.$filex.'/'.$id.$ex)) { ?>
                <a href="<?=$this->config->item('base_url');?>public/file/<?=$filex?>/<?=$id?><?=$ex?>">View Data</a> | 
                <a href="<?=$this->config->item('base_url');?>index.php/c_pelaksanaan/delete_file/<?=$filex?>/<?=$id?><?=$ex?>/<?=$id?>"><img border="0" width="15" height="15" title="Hapus" src="<?=$this->config->item('base_url')?>public/images/delete.png"></a>
            	<?php } else { ?>
                <input type="file" name="<?=$filex?>" size="30" /> [<?=$ex?>]
             	<?php } ?>
             </td>
        </tr>
        <tr>
        	<td>Absensi</td>
            <td>:</td>
        	<td>
            	<?php $filex="absensi"; $ex=".doc"; ?>
            	<?php if (file_exists('./public/file/'.$filex.'/'.$id.$ex)) { ?>
                <a href="<?=$this->config->item('base_url');?>public/file/<?=$filex?>/<?=$id?><?=$ex?>">View Data</a> | 
                <a href="<?=$this->config->item('base_url');?>index.php/c_pelaksanaan/delete_file/<?=$filex?>/<?=$id?><?=$ex?>/<?=$id?>"><img border="0" width="15" height="15" title="Hapus" src="<?=$this->config->item('base_url')?>public/images/delete.png"></a>
            	<?php } else { ?>
                <input type="file" name="<?=$filex?>" size="30" /> [<?=$ex?>]
             	<?php } ?>
             </td>
        </tr>
        <tr>
        	<td>Doa</td>
            <td>:</td>
        	<td>
            	<?php $filex="doa"; $ex=".doc"; ?>
            	<?php if (file_exists('./public/file/'.$filex.'/'.$id.$ex)) { ?>
                <a href="<?=$this->config->item('base_url');?>public/file/<?=$filex?>/<?=$id?><?=$ex?>">View Data</a> | 
                <a href="<?=$this->config->item('base_url');?>index.php/c_pelaksanaan/delete_file/<?=$filex?>/<?=$id?><?=$ex?>/<?=$id?>"><img border="0" width="15" height="15" title="Hapus" src="<?=$this->config->item('base_url')?>public/images/delete.png"></a>
            	<?php } else { ?>
                <input type="file" name="<?=$filex?>" size="30" /> [<?=$ex?>]
             	<?php } ?>
             </td>
        </tr>
        <tr>
        	<td>Laporan Panitia</td>
            <td>:</td>
        	<td>
            	<?php $filex="laporan_panitia"; $ex=".doc"; ?>
            	<?php if (file_exists('./public/file/'.$filex.'/'.$id.$ex)) { ?>
                <a href="<?=$this->config->item('base_url');?>public/file/<?=$filex?>/<?=$id?><?=$ex?>">View Data</a> | 
                <a href="<?=$this->config->item('base_url');?>index.php/c_pelaksanaan/delete_file/<?=$filex?>/<?=$id?><?=$ex?>/<?=$id?>"><img border="0" width="15" height="15" title="Hapus" src="<?=$this->config->item('base_url')?>public/images/delete.png"></a>
            	<?php } else { ?>
                <input type="file" name="<?=$filex?>" size="30" /> [<?=$ex?>]
             	<?php } ?>
             </td>
        </tr>
        <tr>
        	<td>Sambutan Kapusdiklat</td>
            <td>:</td>
        	<td>
            	<?php $filex="sambutan_kapusdiklat"; $ex=".doc"; ?>
            	<?php if (file_exists('./public/file/'.$filex.'/'.$id.$ex)) { ?>
                <a href="<?=$this->config->item('base_url');?>public/file/<?=$filex?>/<?=$id?><?=$ex?>">View Data</a> | 
                <a href="<?=$this->config->item('base_url');?>index.php/c_pelaksanaan/delete_file/<?=$filex?>/<?=$id?><?=$ex?>/<?=$id?>"><img border="0" width="15" height="15" title="Hapus" src="<?=$this->config->item('base_url')?>public/images/delete.png"></a>
            	<?php } else { ?>
                <input type="file" name="<?=$filex?>" size="30" /> [<?=$ex?>]
             	<?php } ?>
             </td>
        </tr>
        <tr>
        	<td>Hasil Evaluasi</td>
            <td>:</td>
        	<td>
            	<?php $filex="hasil_evaluasi"; $ex=".doc"; ?>
            	<?php if (file_exists('./public/file/'.$filex.'/'.$id.$ex)) { ?>
                <a href="<?=$this->config->item('base_url');?>public/file/<?=$filex?>/<?=$id?><?=$ex?>">View Data</a> | 
                <a href="<?=$this->config->item('base_url');?>index.php/c_pelaksanaan/delete_file/<?=$filex?>/<?=$id?><?=$ex?>/<?=$id?>"><img border="0" width="15" height="15" title="Hapus" src="<?=$this->config->item('base_url')?>public/images/delete.png"></a>
            	<?php } else { ?>
                <input type="file" name="<?=$filex?>" size="30" /> [<?=$ex?>]
             	<?php } ?>
             </td>
        </tr>
        <tr>
          <td align="right" colspan="3">
          	<input type="hidden" name="user" value="1" />
            <input name="btn_cancel" type="button" value="&nbsp;&nbsp;&nbsp;&nbsp; Cancel &nbsp;&nbsp;&nbsp;&nbsp;" onclick="history.go(-1);return false;"  />
            <input name="btn_change" type="submit" value="&nbsp;&nbsp;&nbsp;&nbsp; Save &nbsp;&nbsp;&nbsp;&nbsp;" />
            <input name="id" type=hidden value="<?=$id?>" />
          </td>
        </tr>
      </table>
	</form>
</div>
<?php } ?>


<?php if($ket_active==2) { ?>
<div id="tab1" class="tabContents">
	<br />
	<form name="form1" method="post" action="<?=$this->config->item('base_url');?>index.php/<?=$action_form?>" enctype="multipart/form-data">
    <table class="adminmember" cellpadding="5" cellspacing="0" border="1">
	<tr>
    	<td colspan="2">&nbsp;</td>
    	<td colspan="2">
            <select name="peserta">
           		<?php foreach ($data_peserta as $col) { ?>
                   	<option value="<?=$col->id?>"><?=$col->name?></option>
           		<?php } ?>
            </select>
        </td>
    	<td>
            <input name="btn_change" type="submit" value="&nbsp;&nbsp;&nbsp;&nbsp; Masukkan &nbsp;&nbsp;&nbsp;&nbsp;" />
            <input name="id" type=hidden value="<?=$id?>" />
        </td>
    </tr>
	<tr bgcolor="#BBBBBB">
    	<td>No.</td>
    	<td></td>
    	<td>Nama Peserta</td>
    	<td>Instansi</td>
    	<td>&nbsp;</td>
    </tr>
    <?php $no=1; foreach ($data as $col) { ?>
    </tr>
    	<td><?=$no?></td>
    	<td><a href="<?=$this->config->item('base_url');?>index.php/c_pelaksanaan/delete_peserta/<?=$id?><?=$col->peserta?>"><img border="0" width="15" height="15" title="Hapus" src="<?=$this->config->item('base_url')?>public/images/magnifier.png"></a></td>
    	<td><?=$col->peserta?></td>
    	<td><?=$col->instansi?></td>
    	<td><a href="<?=$this->config->item('base_url');?>index.php/c_pelaksanaan/delete_peserta/<?=$col->id?>/<?=$id?>"><img border="0" width="15" height="15" title="Hapus" src="<?=$this->config->item('base_url')?>public/images/delete.png"></a></td>
	</tr>
    <?php $no++; } ?>



    </table>
	</form>
</div>
<?php } ?>

<?php if($ket_active==3) { ?>
<div id="tab1" class="tabContents">
	<br />
	<form name="form1" method="post" action="<?=$this->config->item('base_url');?>index.php/<?=$action_form?>" enctype="multipart/form-data">
    <table class="adminmember" cellpadding="5" cellspacing="0" border="1">
	<tr>
    	<td>&nbsp;</td>
    	<td>
            <select name="mata_diklat">
           		<?php foreach ($data_mata_diklat as $col) { ?>
                   	<option value="<?=$col->id?>"><?=$col->name?></option>
           		<?php } ?>
            </select>
        </td>
    	<td>
            <select name="pengajar">
           		<?php foreach ($data_pengajar as $col) { ?>
                   	<option value="<?=$col->id?>"><?=$col->name?></option>
           		<?php } ?>
            </select>
        </td>
    	<td>
            <input name="btn_change" type="submit" value="&nbsp;&nbsp;&nbsp;&nbsp; Masukkan &nbsp;&nbsp;&nbsp;&nbsp;" />
            <input name="id" type=hidden value="<?=$id?>" />
        </td>
    </tr>
	<tr bgcolor="#BBBBBB">
    	<td>No.</td>
    	<td>Mata Diklat</td>
    	<td>Nama Pengajar</td>
    	<td>&nbsp;</td>
    </tr>
    <?php $no=1; foreach ($data as $col) { ?>
    </tr>
    	<td><?=$no?></td>
    	<td><?=$col->mata_diklat?></td>
    	<td><?=$col->pengajar?></td>
    	<td><a href="<?=$this->config->item('base_url');?>index.php/c_pelaksanaan/delete_pengajar/<?=$col->id?>/<?=$id?>"><img border="0" width="15" height="15" title="Hapus" src="<?=$this->config->item('base_url')?>public/images/delete.png"></a></td>
	</tr>
    <?php $no++; } ?>



    </table>
	</form>
</div>
<?php } ?>

<?php if($ket_active==4) { ?>
<div id="tab1" class="tabContents">
	<br />
	<form name="form1" method="post" action="<?=$this->config->item('base_url');?>index.php/<?=$action_form?>" enctype="multipart/form-data">
    <table class="adminmember" cellpadding="5" cellspacing="0" border="1">
	<tr>
    	<td>Judul Foto</td>
    	<td><input type="text" name="title" size="30" /></td>
     </tr>   
	<tr>
    	<td>File Foto</td>
    	<td><input type="file" name="file" size="30" /></td>
     </tr>   
     <tr>   
    	<td colspan="2">
            <input name="btn_change" type="submit" value="&nbsp;&nbsp;&nbsp;&nbsp; Masukkan &nbsp;&nbsp;&nbsp;&nbsp;" />
            <input name="id" type=hidden value="<?=$id?>" />
        </td>
    </tr>

    </table>
	</form>
<br />    
<?php foreach ($data as $col) { ?>
	<?=$col->title?>
    <a href="<?=$this->config->item('base_url');?>index.php/c_pelaksanaan/delete_foto/<?=$col->id?>/<?=$id?>"><img border="0" width="15" height="15" title="Hapus" src="<?=$this->config->item('base_url')?>public/images/delete.png"></a>
    <br />
	<img src="<?=$this->config->item('base_url');?>public/file/foto_diklat/<?=$col->id?>.jpg" width="501" height="375" />
    <br /><br />
<?php } ?>
</div>
<?php } ?>

<?php if($ket_active==5) { ?>
<div id="tab1" class="tabContents">
	<br />
    <table class="adminmember" cellpadding="5" cellspacing="0" border="1">
	<tr bgcolor="#BBBBBB">
    	<td>No.</td>
    	<td>&nbsp;</td>
    	<td>Nama Report</td>
    </tr>
    </tr>
    	<td>1</td>
    	<td><a target="_new" href="<?=$this->config->item('base_url');?>public/file/report_fix/report_absensi.php?id=<?=$id?>"><img border="0" width="15" height="15" title="Hapus" src="<?=$this->config->item('base_url')?>public/images/magnifier.png"></a></td>
    	<td>Report Daftar Absensi</td>
	</tr>
    </tr>
    	<td>2</td>
    	<td><a target="_new" href="<?=$this->config->item('base_url');?>public/file/report_fix/report_pengajar.php?id=<?=$id?>"><img border="0" width="15" height="15" title="Hapus" src="<?=$this->config->item('base_url')?>public/images/magnifier.png"></a></td>
    	<td>Report Daftar kelas</td>
	</tr>

    </table>
</div>
<?php } ?>

</div>
</div>





</body>
</html>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>jQuery Tab</title>

<style type="text/css">
	#tabContaier{
		background:#ecece8;
		border:1px solid #f36500;
		margin:0px auto;
		padding:20px;
		position:relative;
	}
	#tabContaier li a{
		background:#ddd;
		border:1px solid #fcfcfc;
		border-right:0;
		color:#666;
		cursor:pointer;
		display:block;
		height:35px;
		line-height:35px;
		padding:0 30px;
		text-decoration:none;
		text-transform:uppercase;
	}
	.tabDetails{
		background:#fbfbfb;
		border:1px solid #fff;
		margin:34px 0 0;
	}
#tabContaier .tabDetails #tab1 form .adminmember tr td {
	font-family: Verdana, Geneva, sans-serif;
	font-size:13px;

}
</style>

<link rel="stylesheet" href="<?=$this->config->item('base_url');?>public/calender/css/zebra_datepicker.css" type="text/css">
<script type="text/javascript" src="<?=$this->config->item('base_url');?>public/calender/js/jquery-1.7.2.js"></script>
<script type="text/javascript" src="<?=$this->config->item('base_url');?>public/calender/js/zebra_datepicker.js"></script>
<script type="text/javascript" src="<?=$this->config->item('base_url');?>public/calender/js/functions.js"></script>

</head>

<body>
<br />
<div id="tabContaier">
	
    
    
<div class="tabDetails">
<div id="tab1" class="tabContents">
<form name="form1" method="post" action="<?=$this->config->item('base_url');?>index.php/<?=$action_form?>" enctype="multipart/form-data">
      <table class="adminmember" cellpadding="0" cellspacing="10" border="0">
        <?=$form?>
        <tr>
          <td align="right" colspan="3">
          	<input type="hidden" name="user" value="1" />
            <input name="btn_cancel" type="button" value="&nbsp;&nbsp;&nbsp;&nbsp; Cancel &nbsp;&nbsp;&nbsp;&nbsp;" onclick="history.go(-1);return false;"  />
            <input name="btn_change" type="submit" value="&nbsp;&nbsp;&nbsp;&nbsp; <?=$btn_value?> &nbsp;&nbsp;&nbsp;&nbsp;" />
            <input name="id" type=hidden value="<?=$id?>" />

          </td>
        </tr>
      </table>
</form>      
</div>

</div>
</div>






</body>
</html>

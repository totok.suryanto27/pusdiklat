<!DOCTYPE html>
<html lang="en">
<head>
<title>Pusdiklat</title>
<meta charset="utf-8">
<link rel="stylesheet" href="<?=$this->config->item('base_url');?>public/css/reset.css" type="text/css" media="all">
<link rel="stylesheet" href="<?=$this->config->item('base_url');?>public/css/layout_index.css" type="text/css" media="all">
<link rel="stylesheet" href="<?=$this->config->item('base_url');?>public/css/style_index.css" type="text/css" media="all">
<script type="text/javascript" src="<?=$this->config->item('base_url');?>public/js/jquery-1.5.2.js" ></script>
<script type="text/javascript" src="<?=$this->config->item('base_url');?>public/js/cufon-yui.js"></script>
<script type="text/javascript" src="<?=$this->config->item('base_url');?>public/js/cufon-replace.js"></script>
<script type="text/javascript" src="<?=$this->config->item('base_url');?>public/js/Molengo_400.font.js"></script>
<script type="text/javascript" src="<?=$this->config->item('base_url');?>public/js/Expletus_Sans_400.font.js"></script>
</head>

<body id="page1">
<div class="body1">
  <div class="main">
    <header>
      <div class="wrapper">
        <nav>
          <ul id="menu">
            <li class="end"></li>
          </ul>
        </nav>
        <ul id="icon">
          <li></li>
          <li></li>
          <li></li>
        </ul>
      </div>
      <div class="wrapper">
        <h1><a href="index.html" id="logo">Pusdiklat BKPM</a></h1>
      </div>
      <div id="slogan"> PUSDIKLAT<span>BKPM</span> </div>
      <ul class="banners">
        <form name="form1" method="post" action="<?=$this->config->item('base_url');?>index.php/<?=$form_action?>">
          <table cellpadding="0" cellspacing="0" border="0" align="left">
            <tr>
              <td height="181"><table cellpadding="0" cellspacing="10" width="270" border="0" align="center">
                  <tr>
                    <td valign="middle"><font class="font2">Username</font></td>
                    <td valign="middle" align="right"><input name="username" type="text"></td>
                  </tr>
                  <tr>
                    <td valign="middle"><font class="font2">Password</font></td>
                    <td valign="middle" align="right"><input name="password" type="password"></td>
                  </tr>
                  <tr>
                    <td colspan="2" align="right">
                    		<input name="submit" type="submit" value="Masuk">
                    <?php	$message = $this->session->flashdata('message');
							echo $message == '' ? '' : '<p id="message"><center>' . $message . '</center></p>';
					?>
                    </td>
                  </tr>
                </table></td>
            </tr>
          </table>
        </form>
      </ul>
    </header>
  </div>
</div>
<div class="body2"></div>
<script type="text/javascript"> Cufon.now(); </script>
</body>
</html>

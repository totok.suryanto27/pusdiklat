<link href="<?=$this->config->item('base_url');?>public/css/style.css" rel="stylesheet" type="text/css" />
<link href="<?=$this->config->item('base_url');?>public/css/flexigrid.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?=$this->config->item('base_url');?>public/js/jquery.pack.js"></script>
<script type="text/javascript" src="<?=$this->config->item('base_url');?>public/js/flexigrid.pack.js"></script>
<script>
function getWidth() {
 	var viewportwidth;
 	var viewportheight;
 	if (typeof window.innerWidth != 'undefined')
 	{
    	viewportwidth = window.innerWidth,
      	viewportheight = window.innerHeight
 	}
 	else if (typeof document.documentElement != 'undefined'
     && typeof document.documentElement.clientWidth !=
     'undefined' && document.documentElement.clientWidth != 0)
 	{
       	viewportwidth = document.documentElement.clientWidth,
       	viewportheight = document.documentElement.clientHeight
 	}
 	else
 	{
       	viewportwidth = document.getElementsByTagName('body')[0].clientWidth,
       	viewportheight = document.getElementsByTagName('body')[0].clientHeight
 	}
	return viewportwidth;
}
function getHeight() {
 	var viewportwidth;
 	var viewportheight;
 	if (typeof window.innerWidth != 'undefined')
 	{
    	viewportwidth = window.innerWidth,
      	viewportheight = window.innerHeight
 	}
 	else if (typeof document.documentElement != 'undefined'
     && typeof document.documentElement.clientWidth !=
     'undefined' && document.documentElement.clientWidth != 0)
 	{
       	viewportwidth = document.documentElement.clientWidth,
       	viewportheight = document.documentElement.clientHeight
 	}
 	else
 	{
       	viewportwidth = document.getElementsByTagName('body')[0].clientWidth,
       	viewportheight = document.getElementsByTagName('body')[0].clientHeight
 	}
	return viewportheight;
}
</script>
<?php
echo $js_grid;
?>
<script type="text/javascript">

function test(com,grid)
{
    if (com=='Select All')
    {
		$('.bDiv tbody tr',grid).addClass('trSelected');
    }
    
    if (com=='DeSelect All')
    {
		$('.bDiv tbody tr',grid).removeClass('trSelected');
    }
    
    if (com=='Delete')
        {
           if($('.trSelected',grid).length>0){
			   if(confirm('Delete ' + $('.trSelected',grid).length + ' items?')){
		            var items = $('.trSelected',grid);
		            var itemlist ='';
		        	for(i=0;i<items.length;i++){
						itemlist+= items[i].id.substr(3)+",";
					}
					$.ajax_fleets({
					   type: "POST",
					   url: "<?=site_url("/ajax_fleets/deletec");?>",
					   data: "items="+itemlist,
					   success: function(data){
					   	$('#flex1').flexReload();
					  	alert(data);
					   }
					});
				}
			} else {
				return false;
			} 
        }          
} 
</script>
<style type="text/css">
<!--
div.scroll {
height: 97%;
width: 98%;
overflow: auto;
padding: 8px;
}
-->
</style> 
<table id="flex1" style="display:none">
</table>

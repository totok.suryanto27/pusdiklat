<?php
$server = "localhost";
$database = "test";

function checkJavaExtension()
{
    if(!extension_loaded('java'))
    {
        $sapi_type = php_sapi_name();
        $port = (isset($_SERVER['SERVER_PORT']) && (($_SERVER['SERVER_PORT'])>1024)) ? $_SERVER['SERVER_PORT'] : '8080';
        if ($sapi_type == "cgi" || $sapi_type == "cgi-fcgi" || $sapi_type == "cli") 
        {
            if(!(PHP_SHLIB_SUFFIX=="so" && @dl('java.so'))&&!(PHP_SHLIB_SUFFIX=="dll" && @dl('php_java.dll'))&&!(@include_once("java/Java.inc"))&&!(require_once("http://pusdiklat.bkpm.go.id:$port/java/Java.inc"))) 
            {
                return "java extension not installed.";
            }
        } 
        else
        {
            if(!(@include_once("../../../JavaBridge/java/Java.inc")))
            {
                require_once("http://localhost:$port/JavaBridge/java/Java.inc");
            }
        }
    }
    if(!function_exists("java_get_server_name")) 
    {
        return "The loaded java extension is not the PHP/Java Bridge";
    }
    return true;
}

function convertValue($value, $className)  
{  
    try   
    {  
        if ($className == 'java.lang.String')  
        {  
            $temp = new Java('java.lang.String', $value);  
            return $temp;  
        }  
        else if ($className == 'java.lang.Boolean' ||  
            $className == 'java.lang.Integer' ||  
            $className == 'java.lang.Long' ||  
            $className == 'java.lang.Short' ||  
            $className == 'java.lang.Double' ||  
            $className == 'java.math.BigDecimal')  
        {  
            $temp = new Java($className, $value);  
            return $temp;  
        }  
        else if ($className == 'java.sql.Timestamp' ||  
            $className == 'java.sql.Time')  
        {  
            $temp = new Java($className);  
            $javaObject = $temp->valueOf($value);  
            return $javaObject;  
        }  
    }  
    catch (Exception $err)  
    {  
        echo (  'unable to convert value, ' . $value .  
                ' could not be converted to ' . $className);  
        return false;  
    }
  
    echo (  'unable to convert value, class name '.$className.  
            ' not recognised');  
    return false;  
}


checkJavaExtension();
$class = new JavaClass("java.lang.Class");
$class->forName("com.mysql.jdbc.Driver");
$driverManager = new JavaClass("java.sql.DriverManager");
$conn = $driverManager->getConnection("jdbc:mysql://".$server."/".$database."?user=root&password");
$compileManager = new JavaClass("net.sf.jasperreports.engine.JasperCompileManager");
$report = $compileManager->compileReport(realpath("jasper/".$_GET['id'].".jrxml"));
$fillManager = new JavaClass("net.sf.jasperreports.engine.JasperFillManager");
$params = new Java("java.util.HashMap");
$emptyDataSource = new Java("net.sf.jasperreports.engine.JREmptyDataSource");
$jasperPrint = $fillManager->fillReport($report, $params, $conn);
$outputPath = tempnam(realpath("tmp"), "jasper");
chmod($outputPath, 0766);  
$exportManager = new JavaClass("net.sf.jasperreports.engine.JasperExportManager");
$exportManager->exportReportToPdfFile($jasperPrint, $outputPath);

header("Content-type: application/pdf");
readfile($outputPath);
unlink($outputPath);



?>
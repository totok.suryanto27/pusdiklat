<?php
include_once("config.php"); 

function getKegiatan_diklat_byid($id) {
	$r = array();
	$q0 = "select t1.id 
				,t2.name as diklat 
				,t3.name as angkatan 
				,t4.name as lokasi 
				,t1.dt_from 
				,t1.tahun 
				from tbl_pelaksanaan t1 
			left join tbl_diklat t2 on t1.diklat=t2.id 
			left join tbl_angkatan t3 on t1.angkatan=t3.id 
			left join tbl_lokasi_diklat t4 on t1.lokasi=t4.id 
			where t1.id = ".$id." 
			order by t2.name, t3.id, t1.tahun";
			
	$q = mysql_query($q0);
	
	while($row = mysql_fetch_array($q)) {
		$r[] = $row;
	}	
	return $r;
}

?>
<?php
function checkJavaExtension()
{
    if(!extension_loaded('java'))
    {
        $sapi_type = php_sapi_name();
        $port = (isset($_SERVER['SERVER_PORT']) && (($_SERVER['SERVER_PORT'])>1024)) ? $_SERVER['SERVER_PORT'] : '8080';
        if ($sapi_type == "cgi" || $sapi_type == "cgi-fcgi" || $sapi_type == "cli") 
        {
            if(!(PHP_SHLIB_SUFFIX=="so" && @dl('java.so'))&&!(PHP_SHLIB_SUFFIX=="dll" && @dl('php_java.dll'))&&!(@include_once("java/Java.inc"))&&!(require_once("http://pusdiklat.bkpm.go.id:$port/java/Java.inc"))) 
            {
                return "java extension not installed.";
            }
        } 
        else
        {
            if(!(@include_once("../../../JavaBridge/java/Java.inc")))
            {
                require_once("http://localhost:$port/JavaBridge/java/Java.inc");
            }
        }
    }
    if(!function_exists("java_get_server_name")) 
    {
        return "The loaded java extension is not the PHP/Java Bridge";
    }

    return true;
}

/** 
 * convert a php value to a java one... 
 * @param string $value 
 * @param string $className 
 * @returns boolean success 
 */  
function convertValue($value, $className)  
{  
    // if we are a string, just use the normal conversion  
    // methods from the java extension...  
    try   
    {  
        if ($className == 'java.lang.String')  
        {  
            $temp = new Java('java.lang.String', $value);  
            return $temp;  
        }  
        else if ($className == 'java.lang.Boolean' ||  
            $className == 'java.lang.Integer' ||  
            $className == 'java.lang.Long' ||  
            $className == 'java.lang.Short' ||  
            $className == 'java.lang.Double' ||  
            $className == 'java.math.BigDecimal')  
        {  
            $temp = new Java($className, $value);  
            return $temp;  
        }  
        else if ($className == 'java.sql.Timestamp' ||  
            $className == 'java.sql.Time')  
        {  
            $temp = new Java($className);  
            $javaObject = $temp->valueOf($value);  
            return $javaObject;  
        }  
    }  
    catch (Exception $err)  
    {  
        echo (  'unable to convert value, ' . $value .  
                ' could not be converted to ' . $className);  
        return false;  
    }
  
    echo (  'unable to convert value, class name '.$className.  
            ' not recognised');  
    return false;  
}


$getKegiatan_diklat_byid = getKegiatan_diklat_byid($_GET['id']);

checkJavaExtension();
  $class = new JavaClass("java.lang.Class");
 $class->forName("com.mysql.jdbc.Driver");
 $driverManager = new JavaClass("java.sql.DriverManager");

$conn = $driverManager->getConnection("jdbc:mysql://localhost/test?user=root&password");

$compileManager = new JavaClass("net.sf.jasperreports.engine.JasperCompileManager");



$report = $compileManager->compileReport(realpath("jasper/absensi.jrxml"));



$fillManager = new JavaClass("net.sf.jasperreports.engine.JasperFillManager");



$params = new Java("java.util.HashMap");
$params->put("param_pelaksanaan", intval($_GET['id']));
$params->put("param_diklat", $getKegiatan_diklat_byid[0]['diklat']);
$params->put("param_angkatan", $getKegiatan_diklat_byid[0]['angkatan']." tahun ".$getKegiatan_diklat_byid[0]['tahun']);
$params->put("param_lokasi", $getKegiatan_diklat_byid[0]['dt_from']." ".$getKegiatan_diklat_byid[0]['lokasi']);













$emptyDataSource = new Java("net.sf.jasperreports.engine.JREmptyDataSource");


$jasperPrint = $fillManager->fillReport($report, $params, $conn);



//$outputPath = realpath(".")."/"."output.pdf";
$outputPath = tempnam(realpath("tmp"), "jasper");
chmod($outputPath, 0766);  


$exportManager = new JavaClass("net.sf.jasperreports.engine.JasperExportManager");


$exportManager->exportReportToPdfFile($jasperPrint, $outputPath);

header("Content-type: application/pdf");
readfile($outputPath);

unlink($outputPath);

?>
<?php
// Size � Denotes A4, Legal, A3, etc ��- size:8.5in 11.0in; for Legal size
// Margin � Set the margin of the word document � margin:0.5in 0.31in 0.42in 0.25in; [margin: top right bottom left]
error_reporting(E_ALL ^ E_NOTICE);
include('connection.php');
	
$word_xmlns = "xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:w='urn:schemas-microsoft-com:office:word' xmlns='http://www.w3.org/TR/REC-html40'";
$word_xml_settings = "<xml><w:WordDocument><w:View>Print</w:View><w:Zoom>100</w:Zoom></w:WordDocument></xml>";
$word_landscape_style ="@page Section1 {mso-footer:f1;} div.Section1{page:Section1;} p.MsoFooter, li.MsoFooter, div.MsoFooter{mso-pagination:widow-orphan;tab-stops:center 216.0pt right 432.0pt;}";
//$word_landscape_style = "@page {size:21cm 29,7cm; margin:2cm 2cm 2cm 2cm;} div.Section1{page:Section1;data-type:number}";
$word_landscape_div_start = "<div class='Section1'>";
$word_landscape_div_end = "</div>";


@header('Content-Type: application/msword');
//@header('Content-Length: '.strlen($content));
$row_judul=@mysql_fetch_array(@mysql_query("SELECT * FROM activity WHERE id='".$_GET['par_1']."'"));
@header('Content-disposition: inline; filename="Buku Kenangan '.$row_judul["activity"].' Angkatan '.$_GET['par_2'].' Tahun '.$_GET['par_3'].'.doc"');
echo'
<html '.$word_xmlns.'>
	<head>'.$word_xml_settings.'
		<style type="text/css">
			mso-page-numbers:1;
			@page Section1
				{size:21cm 29,7cm;
				margin:2cm 2cm 2cm 2cm;
				mso-header-margin:.5in;
				mso-footer-margin:.5in;
				mso-paper-source:0;}
			div.Section1
				{page:Section1;}
			@page Section2
				{mso-footer:f2;}
				{size:21cm 29,7cm;
				margin:2cm 2cm 2cm 2cm;
				mso-header-margin:.5in;
				mso-footer-margin:.5in;
				mso-page-numbers:1;
				mso-paper-source:0;
				counter-reset: page 1;}
			div.Section2
				{page:Section2;}
			p.MsoFooter, li.MsoFooter, div.MsoFooter
				{mso-pagination:widow-orphan;
				tab-stops:center;}
			body {font-family:Arial, Helvetica, sans-serif;font-size:14px;line-height:24px;} 
		</style>
</head>
<body>';

$row2=@mysql_fetch_array(@mysql_query("SELECT * FROM activity WHERE id='".$_GET['par_1']."'"));
$row_000=@mysql_fetch_array(@mysql_query("SELECT * FROM bkpm_events WHERE bkpm_events='".$_GET['par_1']."' AND angkatan='".$_GET['par_2']."' AND thnakt='".$_GET['par_3']."'")); 
$row_001=@mysql_fetch_array(@mysql_query("SELECT * FROM city WHERE id='".$row_000['city']."'"));
echo "<div class=\"Section1\">
		<div><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p></div>
		<div style=\"text-align: center; padding-top: 600px;\">
			<h1>Buku Kenangan</h1>
			<h1>Alumni DIKLAT</h1>
			<h1>".$row2['activity']."</h1>
			<h1>Angkatan ".$_GET['par_2']." Tahun ".$_GET['par_3']."</h1>
		</div>
		<div><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p></div>
		<div style=\"text-align: right\">
			<em><strong>".$row_000['place_do'].", ".$row_001['city']." Provinsi ".$row_000['province']."</strong></em>
		</div>
	</div>";


//Isi Buku Kenangan

$query_memory=@mysql_query("SELECT * FROM dataalumni WHERE 1");


while($row_memory=@mysql_fetch_array($query_memory)){
	$str_diklat=explode(";",$row_memory['diklat']);
	$str_angkatan=explode(";",$row_memory['angkatan']);
	$str_tahun=explode(";",$row_memory['tahun']);
	
	$i=1;
	$count_member=0;
	while($str_diklat[$i]!=""){
		if($str_diklat[$i]==$_GET['par_1'] && $str_angkatan[$i]==$_GET['par_2'] && $str_tahun[$i]==$_GET['par_3']){
			$count_member++;
		}
		$i++;
	}
	
	if($count_member!=0){
		echo 
			"<br clear=all style='page-break-before:always;mso-break-type:section-break'>
			<div class=\"Section2\">
			<div style=\"text-align: center; border-top: 10px solid #fa6600; border-bottom: 10px solid #fa6600; width: 100%;\">
				<h1>".$row_memory['nama']."</h1>
			</div>
			<p>&nbsp;</p>
			<table cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">
				<tr valign=\"top\">
					<td width=\"30%\" style=\"padding-left: 10px;\">Nomor Induk Pegawai</td>
					<td width=\"2%\">:</td>
					<td width=\"68%\">".$row_memory['nip']."</td>
				</tr>
			";
		
		$ttl=explode(';',$row_memory['ttl']);
		
		echo 
			"	<tr valign=\"top\">
					<td width=\"30%\" style=\"padding-left: 10px;\">Tempat Tanggal Lahir</td>
					<td width=\"2%\">:</td>
					<td width=\"68%\">".$ttl[0].", ".$ttl[1]."</td>
				</tr>
			";
		
		$instansi=explode(";",$row_memory['instansi']);
		$row_dept=@mysql_fetch_array(@mysql_query("SELECT * FROM dept WHERE id='".$instansi[$i-1]."'"));
		$tipe=explode(";",$row_memory['wilayah']);
		$row_tipe=@mysql_fetch_array(@mysql_query("SELECT * FROM city WHERE city='".$tipe[2*($i-1)-1]."'"));
		if($tipe[2*($i-1)-2]=="Provinsi"){ 
			$tipe_=$tipe[2*($i-1)-2]." ".$tipe[2*($i-1)-1]; 
		} else {
			$tipe_=$tipe[2*($i-1)-2]." ".$tipe[2*($i-1)-1]." Provinsi ".$row_tipe['provinsi']; 
		}
		
		$alamat_=explode(";",$row_memory['kantor_alamat']);
		$kantorkota=explode(";",$row_memory['kantor_kota']);
		$row_004=@mysql_fetch_array(@mysql_query("SELECT * FROM city WHERE id='".$kantorkota[$i-1]."'"));
		$alamat_ktr=$alamat_[$i-1].", ".$row_004['city']." Provinsi ".$row_004['provinsi'];
		
		$telpkantor=explode(";",$row_memory['kantor_telp']);
		$faxkantor=explode(";",$row_memory['fax']);
		$website=explode(";",$row_memory['website']);
		
		$jabatan=explode(";",$row_memory['jabatan']);
		$bagian=explode(";",$row_memory['bagian']);
		
		if($jabatan[$i-1]!=1000){
			$rowpos=@mysql_fetch_array(@mysql_query("SELECT * FROM position WHERE id='".$jabatan[$i-1]."'"));
			if(strlen($bagian[$i-1])>2){
				$jabatan_=$rowpos['position']." bagian/bidang ".$bagian[$i-1];
			} else {
				$jabatan_=$rowpos['position'];
			}
		} else {
			$jabatan_="Tidak ada info";
		}
		
		$pangkat=explode(";",$row_memory['pangkat']);
		$row_003=@mysql_fetch_array(@mysql_query("SELECT * FROM grade WHERE id='".$pangkat[$i-1]."'"));
			
		if($row_memory['rumah']==""){ 
			$alamat="";
		} else {
			$row_000=@mysql_fetch_array(@mysql_query("SELECT * FROM city WHERE id='".$row_memory['rumah_kota']."'"));
			$alamat=$row_memory['rumah'].", ".$row_000['city']." Provinsi ".$row_000['provinsi'];
		}
		
		echo 
			"	<tr valign=\"top\">
					<td width=\"30%\" style=\"padding-left: 10px;\">Instansi</td>
					<td width=\"2%\">:</td>
					<td width=\"68%\">".$row_dept['dept']." ".$tipe_."</td>
				</tr>
				<tr valign=\"top\">
					<td width=\"30%\" style=\"padding-left: 10px;\">Alamat</td>
					<td width=\"2%\">:</td>
					<td width=\"68%\">".$alamat_ktr."</td>
				</tr>
				<tr valign=\"top\">
					<td width=\"30%\" style=\"padding-left: 10px;\">Telp</td>
					<td width=\"2%\">:</td>
					<td width=\"68%\">".$telpkantor[$i-1]."</td>
				</tr>
				<tr valign=\"top\">
					<td width=\"30%\" style=\"padding-left: 10px;\">Fax</td>
					<td width=\"2%\">:</td>
					<td width=\"68%\">".$faxkantor[$i-1]."</td>
				</tr>
				<tr valign=\"top\">
					<td width=\"30%\" style=\"padding-left: 10px;\">Website</td>
					<td width=\"2%\">:</td>
					<td width=\"68%\">".$website[$i-1]."</td>
				</tr>
				<tr valign=\"top\">
					<td width=\"30%\" style=\"padding-left: 10px;\">Jabatan</td>
					<td width=\"2%\">:</td>
					<td width=\"68%\">".$jabatan_."</td>
				</tr>
				<tr valign=\"top\">
					<td width=\"30%\" style=\"padding-left: 10px;\">Pangkat (Golongan)</td>
					<td width=\"2%\">:</td>
					<td width=\"68%\">".$row_003['grade']."</td>
				</tr>
				<tr valign=\"top\">
					<td width=\"30%\" style=\"padding-left: 10px;\">Alamat Rumah</td>
					<td width=\"2%\">:</td>
					<td width=\"68%\">".$alamat."</td>
				</tr>
				<tr valign=\"top\">
					<td width=\"30%\" style=\"padding-left: 10px;\">Telp</td>
					<td width=\"2%\">:</td>
					<td width=\"68%\">".$row_memory['rumah_telp']."</td>
				</tr>
				<tr valign=\"top\">
					<td width=\"30%\" style=\"padding-left: 10px;\">Handphone</td>
					<td width=\"2%\">:</td>
					<td width=\"68%\">".$row_memory['hp_telp']."</td>
				</tr>
				<tr valign=\"top\">
					<td width=\"30%\" style=\"padding-left: 10px;\">E-mail</td>
					<td width=\"2%\">:</td>
					<td width=\"68%\">".$row_memory['email']."</td>
				</tr>
				<tr valign=\"top\">
					<td width=\"30%\" style=\"padding-left: 10px;\">Facebook</td>
					<td width=\"2%\">:</td>
					<td width=\"68%\">".$row_memory['facebook']."</td>
				</tr>
			
				<tr valign=\"top\">
					<td width=\"30%\" style=\"padding-left: 10px;\">Pesan dan Kesan</td>
					<td width=\"2%\">:</td>
					<td width=\"68%\">
						<table width=\"100%\">
							<tr><td width=\"100%\" style=\"border-bottom: 1px solid black\">&nbsp;</td></tr>
							<tr><td width=\"100%\" style=\"border-bottom: 1px solid black\">&nbsp;</td></tr>
							<tr><td width=\"100%\" style=\"border-bottom: 1px solid black\">&nbsp;</td></tr>
							<tr><td width=\"100%\" style=\"border-bottom: 1px solid black\">&nbsp;</td></tr>
							<tr><td width=\"100%\" style=\"border-bottom: 1px solid black\">&nbsp;</td></tr>
						</table>
					</td>
				</tr>
				<tr valign=\"top\">
					<td width=\"30%\" style=\"padding-left: 10px;\">
						<table style=\"border: 1px solid black; width: 113px; \" cellspacing=\"0\" cellpadding=\"0\">
							<tr><td height=\"170px\" align=\"center\"><br />&nbsp;<br />Pas Foto<br />4 x 6<br />&nbsp;<br />&nbsp;</td></tr>
						</table>
					</td>
					<td width=\"2%\">:</td>
					<td width=\"68%\">&nbsp;</td>
				</tr>
			</table>
			
			
			<div style=\"mso-element:footer\" id=\"f2\">
				<p class=\"MsoFooter\" align=\"center\" style=\"text-align:center\">
					<span style=\"mso-element:field-begin\"></span>
					<span style=\"mso-spacerun:yes\">&nbsp;</span>PAGE
				</p>
			</div>
		</div>";
		
		
		
	}
}
echo '</body>
</html>';
<?php include('connection.php'); ?>
				<form action="" method="post" onsubmit="return validationcons();">
					<div class="flexigrid" style="width: 650px;">
						<div class="mDiv" align="center">
							<div class="ftitle">
								Isikan pertanyaan Anda dalam isian berikut
								<br />Untuk jawaban dapat dilihat dalam halaman ini
							</div>
						</div>
						
						<div class="bDiv">
							<table class="flexme4" cellspacing="0" cellpadding="0" border="0" style="">
								<tbody>
									<tr id="row1">
										<td align="right">
											<div style="text-align: right; width: 150px;">Nama Lengkap dan Gelar<span style="color:red; font-weight: bold;">*</span></div>
										</td>
										<td align="left">
											<div style="text-align: left; width: 470px;">
												<input type="text" name="nama" id="nama" size="30" value="" />
											</div>
										</td>
									</tr>
									
									<tr id="row2" class="erow">
										<td align="right">
											<div style="text-align: right; width: 150px;">email</div>
										</td>
										<td align="left">
											<div style="text-align: left; width: 470px;">
												<input type="text" name="email" id="email" size="30" value=""/>
											</div>
										</td>
									</tr>
									
									<tr id="row3">
										<td align="right">
											<div style="text-align: right; width: 150px;">Instansi<span style="color:red; font-weight: bold;">*</span></div>
										</td>
										<td align="left">
											<div style="text-align: left; width: 470px;">
												<select name="instansi" id="instansi" style="width: 300px">
													<option value="">Pilih Satu</option>
													<?php
														$sql="SELECT * FROM dept";
														$query=@mysql_query($sql);
														while($row=@mysql_fetch_array($query)){
															echo
																"<option value=\"".$row['id']."\">".$row['dept']."</option>";
														}
													?>
												</select>
											</div>
										</td>
									</tr>
									
									<tr id="row2" class="erow">
										<td align="right">
											<div style="text-align: right; width: 150px; padding-top: 10px; padding-bottom: 10px;">
												<select name="daerahkantor" id="daerahkantor" onchange="showtipe('office',this.value,'showcityoffice')">
													<option value="">Pilih Satu</option>
													<option value="kota">Kota</option>
													<option value="kabupaten">Kabupaten</option>
													<option value="provinsi">Provinsi</option>
													<option value="pusat">Pusat</option>
												</select><span style="color:red; font-weight: bold;">*</span>
											
											</div>
										</td>
										<td align="left">
											<div style="text-align: left; width: 470px;">
												<div id="showcityoffice"></div>
											</div>
										</td>
									</tr>
									
									<tr id="row3">
										<td align="right">
											<div style="text-align: right; width: 150px;">
												Judul<span style="color:red; font-weight: bold;">*</span>
											</div>
										</td>
										<td align="left">
											<div style="text-align: left; width: 470px;">
												<input type="text" name="title" id="title" size="70" value=""/><br/>
												<span style="font-size: 10px;">cth: Penggantian calon peserta diklat dikarenakan pindah ke bagian lain
											</div>
										</td>
									</tr>
									
									<tr id="row4" class="erow">
										<td align="right">
											<div style="text-align: right; width: 150px;">
												Kategori<span style="color:red; font-weight: bold;">*</span>
											</div>
										</td>
										<td align="left">
											<div style="text-align: left; width: 470px;">
												<select name="kategori" id="kategori">
													<option value="">Pilih Satu</option>
													<option value="1">Pendidikan dan Pelatihan</option>
													<option value="2">Prosedur Perizinan dan Non-Perizinan PM</option>
													<option value="3">SPIPISE</option>
													<option value="4">LKPM</option>
													<option value="5">Lain-lain</option>
												</select>
											</div>
										</td>
									</tr>
									
									<tr id="row5">
										<td align="right" colspan="2">
											<div style="text-align: left;">
												Pertanyaan Anda:<br />
												<textarea cols="70" name="message" id="message" rows="7" onkeyup="countmessage()"></textarea>
												<div id="counter">Tersisa <strong>500</strong> Karakter</div>
											</div>
										</td>
										
									</tr>
									
									<tr id="row6">
										<td align="right" colspan="2">
											<div style="text-align: left;">
												<span style="color: red"><em>* Wajib diisi</em></span>
											</div>
										</td>
										
									</tr>
									
								</tbody>
							</table>
						</div>
						
						<div class="mDiv" align="center">
							<div class="ftitle">
								<input type="submit" name="submit" value="Kirim Pesan" />
							</div>
						</div>
						
					</div>
					</form>
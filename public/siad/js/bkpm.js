function showdatadiklat1(nilai){
	// Proses Tampilan Data
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("showdata").innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET","../../public/showdatadiklat1.php?page="+nilai,true);
	xmlhttp.send();
}

function showsyarat(nilai){
	// Proses Tampilan Data
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("syarat").innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET","http://pusdiklat.bkpm.go.id/public/syarat.php?id="+nilai,true);
	xmlhttp.send();
}

function showprov(root,nilai,target){
	// Proses Tampilan Data
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById(target).innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET","http://pusdiklat.bkpm.go.id/public/showprovsiad.php?id="+nilai,true);
	xmlhttp.send();
}

function submitnlecture(n){
	// Proses Tampilan Data
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("lecture").innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET","http://pusdiklat.bkpm.go.id/public/showlecture.php?n="+n,true);
	xmlhttp.send();
}

function activateCheckInput(){
	if(document.getElementById("kota").checked==true){
		document.getElementById("sel_kota").disabled=false;
		document.getElementById("sel_kabupaten").disabled=true;
		document.getElementById("sel_kabupaten").value="";
		
		showprov('../../../','','showprov');
	}
	
	if(document.getElementById("kabupaten").checked==true){
		document.getElementById("sel_kabupaten").disabled=false;
		document.getElementById("sel_kota").disabled=true;
		document.getElementById("sel_kota").value="";
		
		showprov('../../../','','showprov');
	}
	
	if(document.getElementById("tidakadainfo").checked==true){
		document.getElementById("sel_kabupaten").disabled=true;
		document.getElementById("sel_kota").disabled=true;
		document.getElementById("sel_kota").value="";
		document.getElementById("sel_kabupaten").value="";
		
		showprov('../../../','tidakadainfo','showprov');
	}
}

function validateFormDiklat(){

	if(document.getElementById("diklat").value==""){
		window.alert('Silahkan pilih diklat yang akan diselenggarakan!');
		return false;
	}
	
	if(document.getElementById("tahun").value==""){
		window.alert('Silahkan pilih tahun pelaksanaan!');
		return false;
	}
	
	if(document.getElementById("angkatan").value==""){
		window.alert('Silahkan pilih angkatan diklat!');
		return false;
	}
	
	var date1=new Date(document.getElementById("startyear").value+'-'+document.getElementById("startmonth").value+'-'+document.getElementById("startdate").value);
	var date2=new Date(document.getElementById("endyear").value+'-'+document.getElementById("endmonth").value+'-'+document.getElementById("enddate").value);
	if(date2<date1){
		window.alert('Tanggal berakhir harus lebih besar daripada Tanggal dimulainya Diklat!');
		return false;
	}
	
	if(document.getElementById("place").value==""){
		window.alert('Silahkan isi tempat pelaksanaan!');
		return false;
	}
	
	if(document.getElementById("kota").checked==true && document.getElementById("sel_kota").value==""){
		window.alert('Silahkan isi kota tempat pelaksanaan! Jika tidak ada info, pilih pilihan tidak ada info');
		return false;
	}
	
	if(document.getElementById("kabupaten").checked==true && document.getElementById("sel_kabupaten").value==""){
		window.alert('Silahkan isi kabupaten tempat pelaksanaan! Jika tidak ada info, pilih pilihan tidak ada info');
		return false;
	}
	
	if(document.getElementById("kota").checked==false && document.getElementById("kabupaten").checked==false && document.getElementById("tidakadainfo").checked==false){
		window.alert('Silahkan isi kota atau kabupaten tempat pelaksanaan! Jika tidak ada info, pilih pilihan tidak ada info');
		return false;
	}
	
	if(document.getElementById("nlecture").value==""){
		window.alert('Silahkan isi banyak pengajar (widiaiswara)!');
		return false;
	}
	
	for(var i=1;i<=document.getElementById("nlecture").value;i++){
		if(document.getElementById('lecture'+i).value==""){
			window.alert('Pengajar (widiaiswara) ke-'+i+' tidak boleh kosong!');
			return false;
		}
	}
}

function cek_nip(data){
	if(data==1){
		if(document.getElementById('nip_part1').value.length>7){
			document.getElementById('nip_part2').focus();
		}
	}
	
	if(data==11){
		if(document.getElementById('nip_part1').value.length>8){
			document.getElementById('nip_part2').focus();
		}
	}
	
	if(data==2){
		if(document.getElementById('nip_part2').value.length>5){
			document.getElementById('nip_part3').focus();
		}
	}
	
	if(data==22){
		if(document.getElementById('nip_part2').value.length>6){
			document.getElementById('nip_part3').focus();
		}
	}
	
	if(data==3){
		if(document.getElementById('nip_part3').value.length>0){
			document.getElementById('nip_part4').focus();
		}
	}
	
	if(data==33){
		if(document.getElementById('nip_part3').value.length>1){
			document.getElementById('nip_part4').focus();
		}
	}
	if(data==4){
		if(document.getElementById('nip_part4').value.length>2){
			document.getElementById('nip_part4').value=document.getElementById('nip_part4').value.substring(0, 3);
		}
	}
	
}

function carialumni(){
	if(document.getElementById('nip_part1').value=="" && document.getElementById('nip_part2').value=="" && document.getElementById('nip_part3').value=="" && document.getElementById('nip_part4').value==""){
		alert('Nomor Induk Pegawai tidak boleh kosong');
		return false;
	}
	var data=document.getElementById('nip_part1').value+' '+document.getElementById('nip_part2').value+' '+document.getElementById('nip_part3').value+' '+document.getElementById('nip_part4').value;
	
	var syarat=document.getElementById('syarat').value;
	var diklat=document.getElementById('id_diklat').value;
	// Proses Tampilan Data
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("showalumni").innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET",'http://pusdiklat.bkpm.go.id/public/alumni.php?nip='+data+'&syarat='+syarat+'&id_act='+diklat,true);
	xmlhttp.send();
}

function showtipe(tipe,nilai,target){
	// Proses Tampilan Data
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById(target).innerHTML=xmlhttp.responseText;
			if(tipe=="home"){
				document.getElementById("showprovhome").innerHTML = "";
			}
			
			if(tipe=="office"){
				document.getElementById("showprovoffice").innerHTML = "";
			}
			return false;
		}
	}
	xmlhttp.open("GET",'http://pusdiklat.bkpm.go.id/public/showtipe.php?tipe='+tipe+'&nilai='+nilai,true);
	xmlhttp.send();
}

function showcontact(n){
	// Proses Tampilan Data
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("showcontact").innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET","http://pusdiklat.bkpm.go.id/public/showcontact2.php?n="+n,true);
	xmlhttp.send();
}

function showdataalumni(page,diklat,angkatan,tahun){
	// Proses Tampilan Data
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("showdata").innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET","http://pusdiklat.bkpm.go.id/public/showdataalumni.php?page="+page+"&diklat="+diklat+"&angk="+angkatan+"&tahun="+tahun,true);
	xmlhttp.send();
}

function validateFormMember(){

	if(document.getElementById("nama").value==""){
		window.alert('Silahkan isi nama Peserta!');
		document.getElementById("nama").focus();
		return false;
	}

    if(document.getElementById("instansi").value==""){
		window.alert('Silahkan pilih Instansi tempat bekerja Peserta!');
		document.getElementById("instansi").focus();
		return false;
	}


        if(document.getElementById("kota").checked==false && document.getElementById("kabupaten").checked==false && document.getElementById("provinsi_btn").checked==false && document.getElementById("pusat").checked==false){
		window.alert('Alert::\nSilahkan pilih Jenis Wilayah!');
		return false;
	}

        if(document.getElementById("kota").checked==true && document.getElementById("wil_kantor").value==""){
		window.alert('Alert::\nSilahkan pilih Kota!');
		return false;
	}
	
	if(document.getElementById("kabupaten").checked==true && document.getElementById("wil_kantor").value==""){
		window.alert('Alert::\nSilahkan pilih Kabupaten!');
		return false;
	}
	
	if(document.getElementById("provinsi_btn").checked==true && document.getElementById("wil_kantor").value==""){
		window.alert('Alert::\nSilahkan pilih Provinsi!');
		return false;
	}
	
	if(document.getElementById("jabatan").value==""){
		window.alert('Silahkan pilih Jabatan Peserta!');
		return false;
	}
	
	if(document.getElementById("pangkat").value==""){
		window.alert('Silahkan pilih Pangkat/Golongan Peserta!');
		return false;
	}
	
}

function validateevent(evt) {
    var e = evt || window.event;
    var key = e.keyCode || e.which;

    if (!e.shiftKey && !e.altKey && !e.ctrlKey &&
    // numbers   
    key >= 48 && key <= 57 ||
    // Numeric keypad
    key >= 96 && key <= 105 || key == 8 ||
    // Backspace and Tab and Enter
    // key == 8 || key == 9 || key == 13 ||
    // Home and End
    // key == 35 || key == 36 ||
    // left and right arrows
    key == 37 || key == 39 ||
    // Del and Ins
    key == 46 ) { // || key == 45) {
		// input is VALID
    }
    else {
        // input is INVALID
        e.returnValue = false;
        if (e.preventDefault) e.preventDefault();
    }
}

function deletedataalumni(id,page,diklat,angkatan,tahun){
	// Proses Tampilan Data
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("showdata").innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET","http://pusdiklat.bkpm.go.id/public/deletedataalumni.php?id="+id+"&page="+page+"&diklat="+diklat+"&angk="+angkatan+"&tahun="+tahun,true);
	xmlhttp.send();
}

function showdatadiklat2(nilai){
	// Proses Tampilan Data
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("showdata").innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET","../../public/showdatadiklat2.php?page="+nilai,true);
	xmlhttp.send();
}

function carialumni2(){
	if(document.getElementById('nip_part1').value=="" && document.getElementById('nip_part2').value=="" && document.getElementById('nip_part3').value=="" && document.getElementById('nip_part4').value==""){
		alert('Nomor Induk Pegawai tidak boleh kosong');
		return false;
	}
	var data=document.getElementById('nip_part1').value+' '+document.getElementById('nip_part2').value+' '+document.getElementById('nip_part3').value+' '+document.getElementById('nip_part4').value;
	
	var syarat=document.getElementById('syarat').value;
	var diklat=document.getElementById('id_diklat').value;
	// Proses Tampilan Data
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("showalumni").innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET",'http://pusdiklat.bkpm.go.id/public/alumni2.php?nip='+data+'&syarat='+syarat+'&id_act='+diklat,true);
	xmlhttp.send();
}

function activateCheck(){
	if(document.getElementById("chk_diklat").checked==true){
		document.getElementById("sel_diklat").disabled=false;	
	} else {
		document.getElementById("sel_diklat").disabled=true;
		document.getElementById("sel_diklat").value="";
	}
	
	if(document.getElementById("chk_angkatan").checked==true){
		document.getElementById("sel_angkatan").disabled=false;	
	} else {
		document.getElementById("sel_angkatan").disabled=true;
		document.getElementById("sel_angkatan").value="";
	}
	
	if(document.getElementById("chk_tahun").checked==true){
		document.getElementById("sel_tahun").disabled=false;	
	} else {
		document.getElementById("sel_tahun").disabled=true;
		document.getElementById("sel_tahun").value="";
	}
	
	/*if(document.getElementById("chk_pangkat").checked==true){
		document.getElementById("sel_pangkat").disabled=false;	
	} else {
		document.getElementById("sel_pangkat").disabled=true;
		document.getElementById("sel_pangkat").value="";
	}*/
	
	if(document.getElementById("chk_jabatan").checked==true){
		document.getElementById("sel_jabatan").disabled=false;	
	} else {
		document.getElementById("sel_jabatan").disabled=true;
		document.getElementById("sel_jabatan").value="";
	}
	
	if(document.getElementById("chk_instansi").checked==true){
		document.getElementById("sel_instansi").disabled=false;	
	} else {
		document.getElementById("sel_instansi").disabled=true;
		document.getElementById("sel_instansi").value="";
	}
	
	if(document.getElementById("rad_kota").checked==true){
		document.getElementById("sel_kota").disabled=false;
		document.getElementById("sel_kabupaten").disabled=true;
		document.getElementById("sel_kabupaten").value="";
		document.getElementById("sel_provinsi").disabled=true;
		document.getElementById("sel_provinsi").value="";
	}
	
	if(document.getElementById("rad_kabupaten").checked==true){
		document.getElementById("sel_kabupaten").disabled=false;
		document.getElementById("sel_kota").disabled=true;
		document.getElementById("sel_kota").value="";
		document.getElementById("sel_provinsi").disabled=true;
		document.getElementById("sel_provinsi").value="";
	}
	
	if(document.getElementById("rad_provinsi").checked==true){
		document.getElementById("sel_provinsi").disabled=false;
		document.getElementById("sel_kabupaten").disabled=true;
		document.getElementById("sel_kabupaten").value="";
		document.getElementById("sel_kota").disabled=true;
		document.getElementById("sel_kota").value="";
	}
	
	if(document.getElementById("chk_name").checked==true){
		document.getElementById("sel_name").disabled=false;	
	} else {
		document.getElementById("sel_name").disabled=true;
		document.getElementById("sel_name").value="";
	}
}

function validateFormReport(){
	if(document.getElementById("rad_provinsi").checked==false && document.getElementById("rad_kabupaten").checked==false &&
		document.getElementById("rad_kota").checked==false && document.getElementById("chk_diklat").checked==false &&
		document.getElementById("chk_angkatan").checked==false && document.getElementById("chk_tahun").checked==false /*&& 
		document.getElementById("chk_pangkat").checked==false */&& document.getElementById("chk_jabatan").checked==false && 
		document.getElementById("chk_instansi").checked==false && document.getElementById("chk_name").checked==false){
		window.alert('Alert::\nMohon diceklist minimal satu kriteria!');
		return false;
	}
	
	if(document.getElementById("chk_diklat").checked==true && document.getElementById("sel_diklat").value==""){
		window.alert('Alert::\nSilahkan pilih Diklat!');
		return false;
	}
	
	if(document.getElementById("chk_angkatan").checked==true && document.getElementById("sel_angkatan").value==""){
		window.alert('Alert::\nSilahkan pilih Angkatan!');
		return false;
	}
	
	if(document.getElementById("chk_tahun").checked==true && document.getElementById("sel_tahun").value==""){
		window.alert('Alert::\nSilahkan pilih Tahun!');
		return false;
	}
	
	/*if(document.getElementById("chk_pangkat").checked==true && document.getElementById("sel_pangkat").value==""){
		window.alert('Alert::\nSilahkan pilih Pangkat (Golongan)!');
		return false;
	}*/
	
	if(document.getElementById("chk_jabatan").checked==true && document.getElementById("sel_jabatan").value==""){
		window.alert('Alert::\nSilahkan pilih Jabatan!');
		return false;
	}
	
	if(document.getElementById("chk_instansi").checked==true && document.getElementById("sel_instansi").value==""){
		window.alert('Alert::\nSilahkan pilih Instansi!');
		return false;
	}
	
	if(document.getElementById("rad_kota").checked==true && document.getElementById("sel_kota").value==""){
		window.alert('Alert::\nSilahkan pilih Kota!');
		return false;
	}
	
	if(document.getElementById("rad_kabupaten").checked==true && document.getElementById("sel_kabupaten").value==""){
		window.alert('Alert::\nSilahkan pilih Kabupaten!');
		return false;
	}
	
	if(document.getElementById("rad_provinsi").checked==true && document.getElementById("sel_provinsi").value==""){
		window.alert('Alert::\nSilahkan pilih Provinsi!');
		return false;
	}
	
	if(document.getElementById("chk_name").checked==true && document.getElementById("sel_name").value==""){
		window.alert('Alert::\nSilahkan isi Nama!');
		return false;
	}
}

function resetFormReport(){
	document.getElementById("rad_provinsi").checked=false;
	document.getElementById("sel_provinsi").value="";
	document.getElementById("sel_provinsi").disabled=true;
	document.getElementById("rad_kabupaten").checked=false;
	document.getElementById("sel_kabupaten").value="";
	document.getElementById("sel_kabupaten").disabled=true;
	document.getElementById("rad_kota").checked=false;
	document.getElementById("sel_kota").value="";
	document.getElementById("sel_kota").disabled=true;
	document.getElementById("chk_diklat").checked=false;
	document.getElementById("sel_diklat").value="";
	document.getElementById("sel_diklat").disabled=true;
	document.getElementById("chk_angkatan").checked=false;
	document.getElementById("sel_angkatan").value="";
	document.getElementById("sel_angkatan").disabled=true;
	document.getElementById("chk_tahun").checked=false;
	document.getElementById("sel_tahun").value="";
	document.getElementById("sel_tahun").disabled=true;
	/*document.getElementById("chk_pangkat").checked=false;
	document.getElementById("sel_pangkat").value="";
	document.getElementById("sel_pangkat").disabled=true;*/
	document.getElementById("chk_jabatan").checked=false;
	document.getElementById("sel_jabatan").value="";
	document.getElementById("sel_jabatan").disabled=true;
	document.getElementById("chk_instansi").checked=false;
	document.getElementById("sel_instansi").value="";
	document.getElementById("sel_instansi").disabled=true;
	document.getElementById("chk_name").checked=false;
	document.getElementById("sel_name").value="";
	document.getElementById("sel_name").disabled=true;
	
}

function showdata(section,type,data){
	if(section=="memory"){
		if(type=="tahun"){
			var targetshow="showtahun";
		}
		
		if(type=="angkatan"){
			var targetshow="showangkatan";
		}
		
		if(type=="submit"){
			var targetshow="showsubmit";
		}
		
		var xmlhttp;
		if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp=new XMLHttpRequest();
		} else {// code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function(){
			if (xmlhttp.readyState==4 && xmlhttp.status==200){
				document.getElementById(targetshow).innerHTML = xmlhttp.responseText;
				if(type=="tahun"){
					document.getElementById('showangkatan').innerHTML = "";
					document.getElementById('showsubmit').innerHTML = "";
				}
				
				if(type=="angkatan"){
					document.getElementById('showsubmit').innerHTML = "";
				}
				return false;
			}
		}
		xmlhttp.open("GET","http://pusdiklat.bkpm.go.id/public/datamemory.php?type="+type+"&id="+data,true);
		xmlhttp.send(null);
	}
	
}

function showdatareport(page, diklat, angkatan, tahun, jabatan, instansi, kota, kabupaten, provinsi, nama){
	// Proses Tampilan Data
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("showdata").innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET","http://pusdiklat.bkpm.go.id/public/showdatareport.php?page="+page+"&diklat="+diklat+"&angkatan="+angkatan+"&tahun="+tahun+"&jabatan="+jabatan+"&instansi="+instansi+"&kota="+kota+"&kabupaten="+kabupaten+"&provinsi="+provinsi+"&nama="+nama+"&pangkat=",true);
	xmlhttp.send();
}

function showdataactivity(nilai,text){
	// Proses Tampilan Data
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("showdata").innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET","http://pusdiklat.bkpm.go.id/public/showdataactivity.php?page="+nilai+"&text="+text,true);
	xmlhttp.send();
}

function submitnsyarat(n){
	// Proses Tampilan Data
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("syarat").innerHTML=xmlhttp.responseText;
			return false;
		}
	}
	xmlhttp.open("GET","http://pusdiklat.bkpm.go.id/asset/prasyarat.php?n="+n,true);
	xmlhttp.send();
}

function deleteactivity(id,page,text){
	// Proses Tampilan Data
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("showdata").innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET","http://pusdiklat.bkpm.go.id/public/deleteactivity.php?id="+id+"&page="+page+"&text="+text,true);
	xmlhttp.send();
}

function showdatadept(nilai,text){
	// Proses Tampilan Data
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("showdata").innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET","http://pusdiklat.bkpm.go.id/public/showdatadept.php?page="+nilai+"&text="+text,true);
	xmlhttp.send();
}

function showdatamember(nilai,id){
	// Proses Tampilan Data
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("showalumni").innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET",'http://pusdiklat.bkpm.go.id/public/showdatamember.php?id='+id+'&id_act='+nilai,true);
	xmlhttp.send();
}

function deletedept(id,page,text){
	// Proses Tampilan Data
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("showdata").innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET","http://pusdiklat.bkpm.go.id/public/deletedept.php?id="+id+"&page="+page+"&text="+text,true);
	xmlhttp.send();
}

function showdatalecture(nilai,text){
	// Proses Tampilan Data
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("showdata").innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET","http://pusdiklat.bkpm.go.id/public/showdatalecture.php?page="+nilai+"&text="+text,true);
	xmlhttp.send();
}

function deletelecture(id,page,text){
	// Proses Tampilan Data
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("showdata").innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET","http://pusdiklat.bkpm.go.id/public/deletelecture.php?id="+id+"&page="+page+"&text="+text,true);
	xmlhttp.send();
}

function showdataposition(nilai,text){
	// Proses Tampilan Data
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("showdata").innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET","http://pusdiklat.bkpm.go.id/public/showdataposition.php?page="+nilai+"&text="+text,true);
	xmlhttp.send();
}

function deleteposition(id,page,text){
	// Proses Tampilan Data
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("showdata").innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET","http://pusdiklat.bkpm.go.id/public/deleteposition.php?id="+id+"&page="+page+"&text="+text,true);
	xmlhttp.send();
}

function showdatagrade(nilai,text){
	// Proses Tampilan Data
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("showdata").innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET","http://pusdiklat.bkpm.go.id/public/showdatagrade.php?page="+nilai+"&text="+text,true);
	xmlhttp.send();
}

function deletegrade(id,page,text){
	// Proses Tampilan Data
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("showdata").innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET","http://pusdiklat.bkpm.go.id/public/deletegrade.php?id="+id+"&page="+page+"&text="+text,true);
	xmlhttp.send();
}

function showdatacity(nilai,text){
	// Proses Tampilan Data
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("showdata").innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET","http://pusdiklat.bkpm.go.id/public/showdatacity.php?page="+nilai+"&text="+text,true);
	xmlhttp.send();
}

function deletecity(id,page,text){
	// Proses Tampilan Data
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("showdata").innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET","http://pusdiklat.bkpm.go.id/public/deletecity.php?id="+id+"&page="+page+"&text="+text,true);
	xmlhttp.send();
}

function showProvOptional(nilai){
	// Proses Tampilan Data
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	document.getElementById("tipe1").disabled=false;
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("provopt").innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET","http://pusdiklat.bkpm.go.id/public/provopt.php?provopt="+nilai,true);
	xmlhttp.send();
	
}

function disableprov(){
	document.getElementById('tipe1').disabled=true;
	document.getElementById('tipe1').checked=false;
}

function showdatauser(nilai,text){
	// Proses Tampilan Data
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("showdata").innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET","http://pusdiklat.bkpm.go.id/public/showdatauser.php?page="+nilai+"&text="+text,true);
	xmlhttp.send();
}

function deleteuser(id,page,text){
	// Proses Tampilan Data
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("showdata").innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET","http://pusdiklat.bkpm.go.id/public/deleteuser.php?id="+id+"&page="+page+"&text="+text,true);
	xmlhttp.send();
}

function showdatadocument(nilai,text){
	// Proses Tampilan Data
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("showdata").innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET","http://pusdiklat.bkpm.go.id/public/showdatadocument.php?page="+nilai+"&text="+text,true);
	xmlhttp.send();
}

function deletedocument(id,page,text){
	// Proses Tampilan Data
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("showdata").innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET","http://pusdiklat.bkpm.go.id/public/deletedocument.php?id="+id+"&page="+page+"&text="+text,true);
	xmlhttp.send();
}

function showyear(nilai){
	// Proses Tampilan Data
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("showyear").innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET","http://pusdiklat.bkpm.go.id/public/showyear.php?id="+nilai,true);
	xmlhttp.send();
}

function showbutton(){
	
	var idact=document.getElementById('diklat').value;
	var year=document.getElementById('tahun').value;
	//alert('ini '+idact+' ini '+year);
	// Proses Tampilan Data
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("showbutton").innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET","http://pusdiklat.bkpm.go.id/public/showbutton.php?id="+idact+"&year="+year,true);
	xmlhttp.send();
}





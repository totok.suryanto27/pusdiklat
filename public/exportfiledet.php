<?php
	session_start();
	error_reporting(E_ALL ^ E_NOTICE);
	$connect=@mysql_connect("localhost","root","",true);
	$connect=@mysql_select_db('my_pusd',$connect);
	
	require_once '../asset/Classes/PHPExcel.php';

	// Get Nama Diklat
	
	$query="SELECT * FROM activity WHERE id='".$_GET['id']."'";
	$query=@mysql_query($query);
	while($row=@mysql_fetch_array($query)){
		$actname=$row['activity'];
	}
	
	$objPHPExcel = new PHPExcel();
	$sheet = $objPHPExcel->getActiveSheet();
	$objPHPExcel->getActiveSheet()->setTitle('Summary I');
			
	$sheet->setCellValue('B2', 'Data Jumlah Alumni Diklat yang telah mengikuti '.$actname.' dengan rincian sebagai berikut:');
	$sheet->setCellValue('B4', 'Kategori');
	$sheet->setCellValue('C4', 'Tahun');
	
	// Get Year From bkpm_events
	$query="SELECT * FROM bkpm_events WHERE bkpm_events='".$_GET['id']."' GROUP BY thnakt ORDER BY thnakt ASC";
	$query=@mysql_query($query);
	$abjad=array("","A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z");
	$i=3;
	while($row=@mysql_fetch_array($query)){
		$thnakt[$i-2]=$row['thnakt'];
		$thn[$row['thnakt']]=0;
		$sheet->setCellValue($abjad[$i].'5', $row['thnakt']);
		$i++;
	}
	
	$lastthn=$i;
	$sheet->setCellValue($abjad[$i+0].'4', 'Nip');
	$sheet->setCellValue($abjad[$i+1].'4', 'Tempat, Tanggal Lahir');
	$sheet->setCellValue($abjad[$i+2].'4', 'Alamat Rumah');
	$sheet->setCellValue($abjad[$i+3].'4', 'Instansi');
	$sheet->setCellValue($abjad[$i+4].'4', 'Jabatan');
	$sheet->setCellValue($abjad[$i+5].'4', 'Pangkat/Golongan');
	$sheet->setCellValue($abjad[$i+6].'4', 'Alamat Instansi');
	
	// ============ GENERAL SETTING =============//
	$default_border = array(
		'style' => PHPExcel_Style_Border::BORDER_THIN,
		'color' => array('rgb'=>'cccccc')
		);

	$style_header2 = array(
			'borders' => array(
				'allborders' => $default_border,
			)
		);

	$style_header = array(
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb'=>'b9c9fe'),
			),
			'font' => array(
				'bold' => true,
			),
			'borders' => array(
				'allborders' => $default_border,
			)
		);

	$style_header3 = array(
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb'=>'CCCCCC'),
			)
		);
	// ======== END OF GENERAL SETTING ==========//
	
	// ============ SPECIFIC SETTING =============//
	
	$sheet->getStyle('B4:N5')->getAlignment()->setWrapText(true);
	$sheet->getStyle('B4:N5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$sheet->getStyle('B4:N5')->applyFromArray( $style_header );
	//$sheet->getRowDimension(5)->setRowHeight(33);
	
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B4:B5');
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C4:'.$abjad[$i-1].'4');
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells($abjad[$i].'4:'.$abjad[$i].'5'); // NIP
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells($abjad[$i+1].'4:'.$abjad[$i+1].'5'); // TTL
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells($abjad[$i+2].'4:'.$abjad[$i+2].'5'); // ADDRESS
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells($abjad[$i+3].'4:'.$abjad[$i+3].'5'); // INSTANSI
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells($abjad[$i+4].'4:'.$abjad[$i+4].'5'); // Jabatan
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells($abjad[$i+5].'4:'.$abjad[$i+5].'5'); // Pangkat/Golongan
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells($abjad[$i+6].'4:'.$abjad[$i+6].'5'); // Alamat Instansi
	
	$sheet->getColumnDimension($abjad[$i])->setWidth(23);
	$sheet->getColumnDimension($abjad[$i+1])->setWidth(38);
	$sheet->getColumnDimension($abjad[$i+2])->setWidth(38);
	$sheet->getColumnDimension($abjad[$i+3])->setWidth(45);
	$sheet->getColumnDimension($abjad[$i+4])->setWidth(30);
	$sheet->getColumnDimension($abjad[$i+5])->setWidth(25);
	$sheet->getColumnDimension($abjad[$i+6])->setWidth(38);
	
	
	// ======== END OF SPECIFIC SETTING ==========//
	
	
	$minthn=$thnakt[1];
	$maxthn=$thnakt[$i-2-1];
	
	$query="SELECT * FROM city WHERE 1 GROUP BY provinsi";
	$query=@mysql_query($query);
	$i=6;
	$j=1;
	$pos=0;
	$ndata=0;
	while($row=@mysql_fetch_array($query)){
		if($j<10){ $no="0".$j; } else { $no=$j; }
		$sheet->setCellValue('B'.$i, $no.'. Provinsi '.$row['provinsi']);
		$l=1;
		while($thnakt[$l]!=""){
			$thn[$thnakt[$l]]="";
			$l++;
		}
		
		
		// ==== PROVINSI ====
		// Hitung berapa yang dari Provinsi tersebut
		$query01="SELECT wilayah, diklat, tahun, instansi FROM dataalumni WHERE diklat LIKE '%;".$_GET['id'].";%' AND wilayah LIKE '%Provinsi;".$row['provinsi']."%'";
		$query01=@mysql_query($query01);
		while($row01=@mysql_fetch_array($query01)){
			$diklat=explode(';',$row01['diklat']);
			$k=1;
			while($diklat[$k]!=""){
				if($diklat[$k]==$_GET['id']){
					$pos=$k;
				}
				$k++;
			}
			$wilayah=explode(';',$row01['wilayah']);
			$tahun=explode(';',$row01['tahun']);
			if($wilayah[2*$pos-2]=="Provinsi" && $wilayah[2*$pos-1]==$row['provinsi']){
				$thn[$tahun[$pos]]++;
			}
			
		}
			
		// tampilkan data berdasarkan tahun
		$l=1;
		while($thnakt[$l]!=""){
			$sheet->setCellValue($abjad[$l+2].$i, $thn[$thnakt[$l]]);
			$l++;
		}
		
		
		
		// Tampilkan Rinciannya
		$sql="SELECT * FROM dataalumni WHERE diklat LIKE '%;".$_GET['id'].";%' AND wilayah LIKE '%Provinsi;".$row['provinsi']."%' ORDER BY instansi ASC";
		$query00=@mysql_query($sql);
		$z=1;
		$strinst[0]="";
		$dept="";
		while($row1=@mysql_fetch_array($query00)){
			$instansi=explode(";", $row1['instansi']);
			$strins[$z]=$instansi[1];
			if($strins[$z]!=$strins[$z-1]){
				$i++;
				$row2=@mysql_fetch_array(@mysql_query("SELECT * FROM dept WHERE id='".$instansi[1]."'"));
				$sheet->setCellValue('B'.$i, '        - '.$row2['dept'].' Provinsi '.$row['provinsi']);
				$posprov=$i;
				$y=1;
				$x=1;
				
				
				while($thnakt[$x]!=""){
					$sql01=@mysql_query("SELECT * FROM dataalumni WHERE diklat LIKE '%;".$_GET['id'].";%' AND wilayah LIKE '%Provinsi;".$row['provinsi']."%' AND tahun LIKE '%;".$thnakt[$x].";%' AND instansi LIKE '%;".$instansi[1]."%;'");
					$countx=0;
					$nopeserta=0;
					while($row001=@mysql_fetch_array($sql01)){
						$diklat=explode(';',$row001['diklat']);
						$k=1;
						while($diklat[$k]!=""){
							if($diklat[$k]==$_GET['id']){
								$pos=$k;
							}
							$k++;
						}
						$wilayah=explode(';',$row001['wilayah']);
						$tahun=explode(';',$row001['tahun']);
						if($tahun[$pos]==$thnakt[$x] && $wilayah[2*$pos-2]=="Provinsi" && $wilayah[2*$pos-1]==$row['provinsi']){
							$i++;
							$nopeserta++;
							$sheet->setCellValue('B'.$i, '             '.$nopeserta.'. '.$row001['nama']);
							$sheet->setCellValue($abjad[$y+2].$i, "x");
							
							$str_diklat=explode(";",$row001['diklat']);
							$str_angkatan=explode(";",$row001['angkatan']);
							$str_tahun=explode(";",$row001['tahun']);
							$str_pangkat=explode(";",$row001['pangkat']);
							$str_wilayah=explode(";",$row001['wilayah']);
							$str_instansi=explode(";",$row001['instansi']);
							$str_kota=explode(";",$row001['kantor_kota']);
							$str_provinsi=explode(";",$row001['kantor_prov']);
							$tipe=explode(";",$row001['wilayah']);
							$str_jabatan=explode(";",$row001['jabatan']);
							$str_bagian=explode(";",$row001['bagian']);
							
							$sheet->setCellValue($abjad[$l+2].$i, $row001['nip']);
							$sheet->setCellValue($abjad[$l+3].$i, str_replace(";",", ",$row001['ttl']));
							
							if($row001['rumah']==""){ 
								$alamat="";
							} else {
								$row_000=@mysql_fetch_array(@mysql_query("SELECT * FROM city WHERE id='".$row001['rumah_kota']."'"));
								$alamat=$row001['rumah'].", ".$row_000['city']." Provinsi ".$row_000['provinsi']."\r\n"."No Telp Rumah: ".$row001['rumah_telp']
											."\r\n"."HP: ".$row001['hp_telp']."\r\n"."email: ".$row001['email']."\r\n"."Facebook: ".$row001['facebook'];
							}
							$sheet->setCellValue($abjad[$l+4].$i, $alamat);
							
							$strinstansi=explode(";",$row001['instansi']);
							$row2=@mysql_fetch_array(@mysql_query("SELECT * FROM dept WHERE id='".$strinstansi[$pos]."'"));
							$instansi=$row2['dept']." ".str_replace(";"," ",$row001['wilayah']);
							$sheet->setCellValue($abjad[$l+5].$i, $instansi);
							
							$strjabatan=explode(";",$row001['jabatan']);
							$strbagian=explode(";",$row001['bagian']);
							$row2=@mysql_fetch_array(@mysql_query("SELECT * FROM position WHERE id='".$strjabatan[$pos]."'"));
							$sheet->setCellValue($abjad[$l+6].$i, $row2['position']."  ".$strbagian[$pos]);
							
							$strpangkat=explode(";",$row001['pangkat']);
							$row2=@mysql_fetch_array(@mysql_query("SELECT * FROM grade WHERE id='".$strpangkat[$pos]."'"));
							$sheet->setCellValue($abjad[$l+7].$i, $row2['grade']);
							
							$stralamatins=explode(";",$row001['kantor_alamat']);
							$strkotains=explode(";",$row001['kantor_kota']);
							$strtelpins=explode(";",$row001['kantor_telp']);
							$strfaxins=explode(";",$row001['fax']);
							$strwebins=explode(";",$row001['website']);
							$row2=@mysql_fetch_array(@mysql_query("SELECT * FROM city WHERE id='".$strkotains[$pos]."'"));
								
							$alamatinstansi=$stralamatins[$i]."\r\n".$row2['tipe']." ".$row2['city']." Provinsi ".$row2['provinsi']."\r\nTelp: ".$strtelpins[$pos]."\r\nFax: ".$strfaxins[$i]."\r\nWebsite: ".$strwebins[$pos];
							
							$sheet->setCellValue($abjad[$l+8].$i, $alamatinstansi);
							
							
							$countx++;
						}
					}
					if($countx==0){ $countx=""; }
					$sheet->setCellValue($abjad[$y+2].$posprov, $countx);
					/* if($countx!=0){
						$sheet->setCellValue($abjad[$y+2].$i, "x");
					} */
					$y++;
					$x++;
				}
				
			}
			
			
			
			$z++;
		}
		
		// ==== END OF PROVINSI ====
		
		$query00="SELECT * FROM city WHERE provinsi='".$row['provinsi']."' AND tipe<>'Provinsi'";
		$query00=@mysql_query($query00);
		$k=1;
		while($row00=@mysql_fetch_array($query00)){
			if($k<10){ $no00="0".$k; } else { $no00=$k; }
			$i++;
			$sheet->setCellValue('B'.$i, '                '.$no00.'. '.$row00['tipe'].' '.$row00['city']);	
				
			$l=1;
			while($thnakt[$l]!=""){
				$thn02[$thnakt[$l]]="";
				$l++;
			}
				
			$query02="SELECT wilayah, diklat, tahun FROM dataalumni WHERE diklat LIKE '%;".$_GET['id'].";%'";
			//AND wilayah LIKE '%".$row00['tipe'].";".$row00['city']."%'";
				
			$query02=@mysql_query($query02);
			$pos02="";
			while($row02=@mysql_fetch_array($query02)){
				$diklat=explode(';',$row02['diklat']);
				// Lokalisasi posisi diklat
				$m=1;
				while($diklat[$m]!=""){
					if($diklat[$m]==$_GET['id']){
						$pos02=$m;
					}
					$m++;
				}
				$wilayah02=explode(';',$row02['wilayah']);
				$tahun02=explode(';',$row02['tahun']);
				if($wilayah02[2*$pos02-2]==$row00['tipe'] && $wilayah02[2*$pos02-1]==$row00['city']){
					$thn02[$tahun02[$pos02]]=$thn02[$tahun02[$pos02]]+1;
				}
			}
				
			//$sheet->setCellValue('G'.$i, $query02);
				
			// tampilkan data berdasarkan tahun
			$l=1;
			while($thnakt[$l]!=""){
				$sheet->setCellValue($abjad[$l+2].$i, $thn02[$thnakt[$l]]);
				$l++;
			}
				
			// Get Instansi
			$sql="SELECT * FROM dataalumni WHERE diklat LIKE '%;".$_GET['id'].";%' AND wilayah LIKE '%".$row00['tipe'].";".$row00['city']."%' ORDER BY instansi ASC";
			$query0001=@mysql_query($sql);
			$z=1;
			$strinst[0]="";
			$dept="";
			while($row0001=@mysql_fetch_array($query0001)){
				$instansi=explode(";", $row0001['instansi']);
				$strins[$z]=$instansi[1];
				if($strins[$z]!=$strins[$z-1]){
					$i++;
					$row2=@mysql_fetch_array(@mysql_query("SELECT * FROM dept WHERE id='".$instansi[1]."'"));
					$sheet->setCellValue('B'.$i, '                        - '.$row2['dept'].' '.$row00['tipe'].' '.$row00['city']);
					$posprov=$i;
					$y=1;
					$x=1;
						
					while($thnakt[$x]!=""){
						$sql0001=@@mysql_query("SELECT * FROM dataalumni WHERE diklat LIKE '%;".$_GET['id'].";%' AND wilayah LIKE '%".$row00['tipe'].";".$row00['city']."%' AND tahun LIKE '%;".$thnakt[$x].";%' AND instansi LIKE '%;".$instansi[1]."%;'");
						$countx=0;
						$nopeserta=0;
						while($row00001=@mysql_fetch_array($sql0001)){
							$diklat=explode(';',$row00001['diklat']);
							$k=1;
							while($diklat[$k]!=""){
								if($diklat[$k]==$_GET['id']){
									$pos=$k;
								}
								$k++;
							}
							$wilayah=explode(';',$row00001['wilayah']);
							$tahun=explode(';',$row00001['tahun']);
							if($tahun[$pos]==$thnakt[$x] && $wilayah[2*$pos-2]==$row00['tipe'] && $wilayah[2*$pos-1]==$row00['city']){
								$i++;
								$nopeserta++;
								$sheet->setCellValue('B'.$i, '                          '.$nopeserta.'. '.$row00001['nama']);
								$sheet->setCellValue($abjad[$y+2].$i, "x");
								
								$str_diklat=explode(";",$row00001['diklat']);
								$str_angkatan=explode(";",$row00001['angkatan']);
								$str_tahun=explode(";",$row00001['tahun']);
								$str_pangkat=explode(";",$row00001['pangkat']);
								$str_wilayah=explode(";",$row00001['wilayah']);
								$str_instansi=explode(";",$row00001['instansi']);
								$str_kota=explode(";",$row00001['kantor_kota']);
								$str_provinsi=explode(";",$row00001['kantor_prov']);
								$tipe=explode(";",$row00001['wilayah']);
								$str_jabatan=explode(";",$row00001['jabatan']);
								$str_bagian=explode(";",$row00001['bagian']);
								
								$sheet->setCellValue($abjad[$l+2].$i, $row00001['nip']);
								$sheet->setCellValue($abjad[$l+3].$i, str_replace(";",", ",$row00001['ttl']));
								
								if($row00001['rumah']==""){ 
									$alamat="";
								} else {
									$row_000=@mysql_fetch_array(@mysql_query("SELECT * FROM city WHERE id='".$row00001['rumah_kota']."'"));
									$alamat=$row00001['rumah'].", ".$row_000['city']." Provinsi ".$row_000['provinsi']."\r\n"."No Telp Rumah: ".$row00001['rumah_telp']
												."\r\n"."HP: ".$row00001['hp_telp']."\r\n"."email: ".$row00001['email']."\r\n"."Facebook: ".$row00001['facebook'];
								}
								$sheet->setCellValue($abjad[$l+4].$i, $alamat);
								
								$strinstansi=explode(";",$row00001['instansi']);
								$row2=@mysql_fetch_array(@mysql_query("SELECT * FROM dept WHERE id='".$strinstansi[$pos]."'"));
								$instansi=$row2['dept']." ".str_replace(";"," ",$row00001['wilayah']);
								$sheet->setCellValue($abjad[$l+5].$i, $instansi);
								
								$strjabatan=explode(";",$row00001['jabatan']);
								$strbagian=explode(";",$row00001['bagian']);
								$row2=@mysql_fetch_array(@mysql_query("SELECT * FROM position WHERE id='".$strjabatan[$pos]."'"));
								$sheet->setCellValue($abjad[$l+6].$i, $row2['position']."  ".$strbagian[$pos]);
								
								$strpangkat=explode(";",$row00001['pangkat']);
								$row2=@mysql_fetch_array(@mysql_query("SELECT * FROM grade WHERE id='".$strpangkat[$pos]."'"));
								$sheet->setCellValue($abjad[$l+7].$i, $row2['grade']);
								
								$stralamatins=explode(";",$row00001['kantor_alamat']);
								$strkotains=explode(";",$row00001['kantor_kota']);
								$strtelpins=explode(";",$row00001['kantor_telp']);
								$strfaxins=explode(";",$row00001['fax']);
								$strwebins=explode(";",$row00001['website']);
								$row2=@mysql_fetch_array(@mysql_query("SELECT * FROM city WHERE id='".$strkotains[$pos]."'"));
									
								$alamatinstansi=$stralamatins[$i]."\r\n".$row2['tipe']." ".$row2['city']." Provinsi ".$row2['provinsi']."\r\nTelp: ".$strtelpins[$pos]."\r\nFax: ".$strfaxins[$i]."\r\nWebsite: ".$strwebins[$pos];
								
								$sheet->setCellValue($abjad[$l+8].$i, $alamatinstansi);
								$countx++;
							}
						}
						if($countx==0){ $countx=""; }
						$sheet->setCellValue($abjad[$y+2].$posprov, $countx);
						$y++;
						$x++;
					}
						
				}
					
					
					
				$z++;
			} 
				
				
				$k++;
			} 
		
		$j++;
		$i++;
	}
	$sheet->setCellValue('A1', $maxthn);
	$sheet->getColumnDimension('B')->setWidth(52);
	$sheet->getStyle('B4:N'.($i-1))->getAlignment()->setWrapText(true);
	$sheet->getStyle('B4:N'.($i-1))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$sheet->getStyle('C6:'.$abjad[$lastthn-1].($i-1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	
	
	
	$objPHPExcel->setActiveSheetIndex(0);
			
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="report.xls"');
	header('Cache-Control: max-age=0');

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save('php://output');
			//echo "<script>alert('Success')</script>";?>
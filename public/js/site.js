function createRequestObject() {
	var ro;
	var browser = navigator.appName;
	if(browser == "Microsoft Internet Explorer"){
		ro = new ActiveXObject("Microsoft.XMLHTTP");
	}else{
		ro = new XMLHttpRequest();
	}
	return ro;
}

var xmlhttp = createRequestObject();

function cekdata(){
	if(document.getElementById('nip_part1').value=="" && document.getElementById('nip_part2').value=="" && document.getElementById('nip_part3').value=="" && document.getElementById('nip_part4').value==""){
		alert('Nomor Induk Pegawai tidak boleh kosong');
		return false;
	}
	var data=document.getElementById('nip_part1').value+' '+document.getElementById('nip_part2').value+' '+document.getElementById('nip_part3').value+' '+document.getElementById('nip_part4').value;
	
	var diklat=document.getElementById('id_diklat').value;
	var angkatan=document.getElementById('angkatan').value;
	xmlhttp.open('get', '../../../../public/data.php?nip='+data+'&id_act='+diklat+'&angk='+angkatan, true);
	xmlhttp.onreadystatechange = function() {
    	if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
            document.getElementById("showdata").innerHTML = xmlhttp.responseText;
		    return false;
        }
	xmlhttp.send(null);
}

function cekdata2(){
	
	if(document.getElementById('nip_part1').value=="" && document.getElementById('nip_part2').value=="" && document.getElementById('nip_part3').value=="" && document.getElementById('nip_part4').value==""){
		alert('Nomor Induk Pegawai tidak boleh kosong');
		return false;
	}
	var data=document.getElementById('nip_part1').value+' '+document.getElementById('nip_part2').value+' '+document.getElementById('nip_part3').value+' '+document.getElementById('nip_part4').value;
	
	var diklat=document.getElementById('id_diklat').value;
	var idform=document.getElementById('id_form').value;
	
	xmlhttp.open('get', '../../../../public/data2.php?nip='+data+'&id_act='+diklat+'&id_form='+idform, true);
	xmlhttp.onreadystatechange = function() {
    	if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
            document.getElementById("showdata").innerHTML = xmlhttp.responseText;
		    return false;
        }
	xmlhttp.send(null);
}

function batalkan(){
	if(document.getElementById('nip_part1').value=="" && document.getElementById('nip_part2').value=="" && document.getElementById('nip_part3').value=="" && document.getElementById('nip_part4').value==""){
		alert('Nomor Induk Pegawai tidak boleh kosong');
		return false;
	}
	var data=document.getElementById('nip_part1').value+' '+document.getElementById('nip_part2').value+' '+document.getElementById('nip_part3').value+' '+document.getElementById('nip_part4').value;
	
	var diklat=document.getElementById('id_diklat').value;
	var idform=document.getElementById('id_form').value;
	//alert(idform);
	xmlhttp.open('get', '../../../../public/data3.php?nip='+data+'&id_act='+diklat+'&id_form='+idform, true);
	xmlhttp.onreadystatechange = function() {
    	if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
            document.getElementById("showdata").innerHTML = xmlhttp.responseText;
		    return false;
        }
	xmlhttp.send(null);
}

function showprov(nilai){
	xmlhttp.open('get', '../../../public/showprov.php?kota='+nilai, true);
	xmlhttp.onreadystatechange = function() {
    	if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
            document.getElementById("prov").innerHTML = xmlhttp.responseText;
		    return false;
        }
	xmlhttp.send(null);
}

function showprov2(nilai){
	xmlhttp.open('get', '../../../public/showprov2.php?kota='+nilai, true);
	xmlhttp.onreadystatechange = function() {
    	if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
            document.getElementById("prov2").innerHTML = xmlhttp.responseText;
		    return false;
        }
	xmlhttp.send(null);
}

function validateform(){	
	
	if(document.getElementById('nama').value==""){
		alert("Silahkan isi terlebih dahulu Nama Calon Peserta!");
		document.getElementById('nama').focus();
		return false;
	}
			
	if(document.getElementById('birthplace').value==""){
		alert("Silahkan isi terlebih dahulu Tempat Lahir Calon Peserta!");
		document.getElementById('birthplace').focus();
		return false;
	}
		
	if(document.getElementById('tlyear').value==""){
		alert("Silahkan isi terlebih dahulu Tahun Lahir Calon Peserta!");
		document.getElementById('tlyear').focus();
		return false;
	}
		
	if(document.getElementById('instansi').value==""){
		alert("Silahkan isi terlebih dahulu Intansi tempat Calon Peserta bekerja!");
		return false;
	}

	if(document.getElementById('daerahkantor').value==""){
		alert("Silahkan pilih Wilayah Instansi tempat Calon Peserta bekerja!");
		return false;
	}
	
	if(document.getElementById('wil_kantor').value==""){
		alert("Silahkan pilih Kota Wilayah Instansi tempat Calon Peserta bekerja!");
		return false;
	}
	
	if(document.getElementById('alamat').value==""){
		alert("Silahkan isi terlebih dahulu Alamat Instansi tempat Calon Peserta bekerja!");
		document.getElementById('alamat').focus();
		return false;
	}
	
	if(document.getElementById('kota').value==""){
		alert("Silahkan isi terlebih dahulu Kota Instansi tempat Calon Peserta bekerja!");
		return false;
	}
			
	if(document.getElementById('katelp').value=="" && document.getElementById('notelp').value==""){
		alert("Silahkan isi terlebih dahulu Telp Instansi tempat Calon Peserta bekerja!");
		document.getElementById('katelp').focus();
		return false;
	}
			
	if(document.getElementById('kafax').value=="" && document.getElementById('nofax').value==""){
		alert("Silahkan isi terlebih dahulu Fax Instansi tempat Calon Peserta bekerja!");
		document.getElementById('kafax').focus();
		return false;
	}
			
	if(document.getElementById('jabatan').value==""){
		alert("Silahkan isi terlebih dahulu Jabatan Calon Peserta!");
		return false;
	}
	
	if(document.getElementById('bagian').value==""){
		alert("Silahkan isi terlebih dahulu Bagian Calon Peserta!");
		document.getElementById('bagian').focus();
		return false;
	}
			
	if(document.getElementById('golongan').value==""){
		alert("Silahkan isi terlebih dahulu Pangkat (Golongan) Calon Peserta!");
		return false;
	}
	
	if(document.getElementById('alamat2').value==""){
		alert("Silahkan isi terlebih dahulu Alamat Rumah tempat Calon Peserta bekerja!");
		document.getElementById('alamat2').focus();
		return false;
	}
	
	if(document.getElementById('kota2').value==""){
		alert("Silahkan isi terlebih dahulu Kota Rumah tempat Calon Peserta bekerja!");
		return false;
	}
		
	if(document.getElementById('katelp2').value=="" && document.getElementById('notelp2').value==""){
		alert("Silahkan isi terlebih dahulu Telp Calon Peserta!");
		document.getElementById('katelp2').focus();
		return false;
	}
	
	if(document.getElementById('contact').value==""){
		alert("Silahkan isi terlebih dahulu Nomor HP Calon Peserta!");
		return false;
	}
	
	if(document.getElementById('namaatasan').value==""){
		alert("Silahkan isi terlebih dahulu Nama Atasan Calon Peserta!");
		document.getElementById('namaatasan').focus();
		return false;
	}
	
	if(document.getElementById('contactatasan').value==""){
		alert("Silahkan isi terlebih dahulu No Telp/HP Atasan Calon Peserta!");
		document.getElementById('contactatasan').focus();
		return false;
	}
	
	/* if(document.getElementById('hp').value==""){
		alert("Silahkan isi terlebih dahulu Nomor HP Calon Peserta!");
		document.getElementById('hp').focus();
		return false;
	} */
			
	if(document.getElementById('email').value==""){
		alert("Silahkan isi terlebih dahulu Alamat Email Calon Peserta!");
		document.getElementById('email').focus();
		return false;
	}
	
	if(!validateEmail()){
		document.getElementById('email').focus();
		return false;	
	}
	
	if(confirm('Silahkan cek kembali sekali lagi alamat email Anda,\nkarena bila tidak sesuai, link yang berkaitan surat konfirmasi\ntidak akan diterima. Alamat email Anda adalah '+document.getElementById('email').value+'.\nJika benar klik Ya untuk melanjutkan!')){return true; } else { return false; }
}

function validateEmail() {
	var emailText = document.getElementById('email').value;
	var pattern = /^[a-zA-Z0-9\-_]+(\.[a-zA-Z0-9\-_]+)*@[a-z0-9]+(\-[a-z0-9]+)*(\.[a-z0-9]+(\-[a-z0-9]+)*)*\.[a-z]{2,4}$/;
	if (pattern.test(emailText)) {
		return true;
	} else {
		alert('Alamat Email Salah: ' + emailText);
		return false;
	}
}

function cek_nip(data){
	if(data==1){
		if(document.getElementById('nip_part1').value.length>7){
			document.getElementById('nip_part2').focus();
		}
	}
	
	if(data==11){
		if(document.getElementById('nip_part1').value.length>8){
			document.getElementById('nip_part2').focus();
		}
	}
	
	if(data==2){
		if(document.getElementById('nip_part2').value.length>5){
			document.getElementById('nip_part3').focus();
		}
	}
	
	if(data==22){
		if(document.getElementById('nip_part2').value.length>6){
			document.getElementById('nip_part3').focus();
		}
	}
	
	if(data==3){
		if(document.getElementById('nip_part3').value.length>0){
			document.getElementById('nip_part4').focus();
		}
	}
	
	if(data==33){
		if(document.getElementById('nip_part3').value.length>1){
			document.getElementById('nip_part4').focus();
		}
	}
	if(data==4){
		if(document.getElementById('nip_part4').value.length>2){
			document.getElementById('nip_part4').value=document.getElementById('nip_part4').value.substring(0, 3);
		}
	}
	
}

function validatekey(evt) {
    var e = evt || window.event;
    var key = e.keyCode || e.which;

    if (!e.shiftKey && !e.altKey && !e.ctrlKey &&
    // numbers   
    key >= 48 && key <= 57 ||
    // Numeric keypad
    key >= 96 && key <= 105 || key == 8 ||
    // Backspace and Tab and Enter
    // key == 8 || key == 9 || key == 13 ||
    // Home and End
    // key == 35 || key == 36 ||
    // left and right arrows
    key == 37 || key == 39 ||
    // Del and Ins
    key == 46 ) { // || key == 45) {
		// input is VALID
    }
    else {
        // input is INVALID
        e.returnValue = false;
        if (e.preventDefault) e.preventDefault();
    }
}

function showcontact(n){
	xmlhttp.open('get', '../../../public/showcontact.php?n='+n, true);
	xmlhttp.onreadystatechange = function() {
    	if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
            document.getElementById("showcontact").innerHTML = xmlhttp.responseText;
		    return false;
        }
	xmlhttp.send(null);
}

function showtipe(tipe,nilai,target){
	xmlhttp.open('get', '../../../public/showtipe.php?tipe='+tipe+'&nilai='+nilai, true);
	xmlhttp.onreadystatechange = function() {
		if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
			document.getElementById(target).innerHTML = xmlhttp.responseText;
			if(tipe=="home"){
				document.getElementById("showprovhome").innerHTML = "";
			}
			
			if(tipe=="office"){
				document.getElementById("showprovoffice").innerHTML = "";
			}
			return false;
		}
	xmlhttp.send(null);
	
}

function validationcons(){	
	if(document.getElementById('nama').value==""){
		alert("Silahkan isi terlebih dahulu input Nama!");
		document.getElementById('nama').focus();
		return false;
	}
		
	if(document.getElementById('instansi').value==""){
		alert("Silahkan isi terlebih dahulu Intansi tempat Calon Peserta bekerja!");
		return false;
	}

	if(document.getElementById('daerahkantor').value==""){
		alert("Silahkan pilih Wilayah Instansi tempat Calon Peserta bekerja!");
		return false;
	}
	
	if(document.getElementById('wil_kantor').value==""){
		alert("Silahkan pilih Kota Wilayah Instansi tempat Calon Peserta bekerja!");
		return false;
	}
	
	if(document.getElementById('title').value==""){
		alert("Judul tidak boleh kosong!");
		//document.getElementById('title').focus();
		return false;
	}
	
	if(document.getElementById('kategori').value==""){
		alert("Silahkan pilih Kategori Pertanyaan!");
		return false;
	}
	
	if(document.getElementById('message').value==""){
		alert("Silahkan isi terlebih dahulu Pertanyaan!");
		document.getElementById('message').focus();
		return false;
	}
			
	
}

function pagecons(nilai){
	xmlhttp.open('get', '../../public/showcons.php?page='+nilai, true);
	xmlhttp.onreadystatechange = function() {
		if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
			document.getElementById('showdata').innerHTML = xmlhttp.responseText;
			return false;
		}
	xmlhttp.send(null);
	
}

function pageindex(nilai,kategori){
	xmlhttp.open('get', '../../public/showindex.php?page='+nilai+'&cat='+kategori, true);
	xmlhttp.onreadystatechange = function() {
		if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
			document.getElementById('showdata').innerHTML = xmlhttp.responseText;
			return false;
		}
	xmlhttp.send(null);
	
}

function pageindex2(nilai,kategori){
	xmlhttp.open('get', '../../public/showindex2.php?page='+nilai+'&cat='+kategori, true);
	xmlhttp.onreadystatechange = function() {
		if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
			document.getElementById('showdata').innerHTML = xmlhttp.responseText;
			return false;
		}
	xmlhttp.send(null);
	
}

function pagealbum(nilai){
	xmlhttp.open('get', '../../public/showalbum.php?page='+nilai, true);
	xmlhttp.onreadystatechange = function() {
		if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
			document.getElementById('showphoto').innerHTML = xmlhttp.responseText;
			return false;
		}
	xmlhttp.send(null);
	
}

function pagephoto(nilai,kategori){
	xmlhttp.open('get', '../../public/showphoto.php?page='+nilai+'&cat='+kategori, true);
	xmlhttp.onreadystatechange = function() {
		if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
			document.getElementById('showphoto').innerHTML = xmlhttp.responseText;
			return false;
		}
	xmlhttp.send(null);
	
}

function showform(){
	xmlhttp.open('get', 'http://pusdiklat.bkpm.go.id/public/showform.php', true);
	xmlhttp.onreadystatechange = function() {
		if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
			document.getElementById('showform').innerHTML = xmlhttp.responseText;
			return false;
		}
	xmlhttp.send(null);
	
}

function countmessage(){
	var str=document.getElementById('message').value;
	var n=str.length;
	
	if(n>500){
		document.getElementById('message').value = str.substring(0,500);
	}
	
	xmlhttp.open('get', 'http://pusdiklat.bkpm.go.id/public/countmessage.php?n='+n+'&str='+str, true);
	xmlhttp.onreadystatechange = function() {
		if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
			document.getElementById('counter').innerHTML = xmlhttp.responseText;
			return false;
		}
	xmlhttp.send(null);
	
}

function showdataforum(nilai,text){
	// Proses Tampilan Data
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("showdata").innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET","http://pusdiklat.bkpm.go.id/public/showdataforum.php?page="+nilai+"&text="+text,true);
	xmlhttp.send();
}

function showquestion(pos,questid){
	// Proses Tampilan Data
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			for(var i=1;i<=document.getElementById("nofquest").value;i++){
				if(i==pos){
					document.getElementById("showquestion"+i).innerHTML=xmlhttp.responseText;
				} else {
					document.getElementById("showquestion"+i).innerHTML='';
				}
			}
			
		}
	}
	xmlhttp.open("GET","http://pusdiklat.bkpm.go.id/public/showquestion.php?questid="+questid+"&pos="+pos,true);
	xmlhttp.send();
}

function countshow(pos, hits){
	// Proses Tampilan Data
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("countshow"+pos).innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET","http://pusdiklat.bkpm.go.id/public/countshow.php?hits="+hits+"&pos="+pos,true);
	xmlhttp.send();
}


function showkurs(){
	xmlhttp.open('get', '../../../public/fungsi_kurs_bca.php', true);
	xmlhttp.onreadystatechange = function() {
    	if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
            document.getElementById("showkurs").innerHTML = xmlhttp.responseText;
		    return false;
        }
	xmlhttp.send(null);
}

function ajaxfunc(parameter, target, file){
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById(target).innerHTML=xmlhttp.responseText;
			return false;
		}
	}
	xmlhttp.open("GET", file+"?param="+parameter,true);
	xmlhttp.send();
}

String.prototype.replaceAll = function(s,r){return this.split(s).join(r)}


function encode(data){
	
	var encoded;
	encoded=data.replaceAll("$","%24");
	encoded=encoded.replaceAll("&","%26");
	encoded=encoded.replaceAll("+","%2B");
	encoded=encoded.replaceAll(",","%2C");
	encoded=encoded.replaceAll("/","%2F");
	encoded=encoded.replaceAll(":","%3A");
	encoded=encoded.replaceAll(";","%3B");
	encoded=encoded.replaceAll("=","%3D");
	encoded=encoded.replaceAll("?","%3F");
	encoded=encoded.replaceAll(" ","%20");
	encoded=encoded.replaceAll("@","%40");
	encoded=encoded.replaceAll('"',"%22");
	encoded=encoded.replaceAll("<","%3C");
	encoded=encoded.replaceAll(">","%3E");
	encoded=encoded.replaceAll("#","%23");
	encoded=encoded.replaceAll("{","%7B");
	encoded=encoded.replaceAll("}","%7D");
	encoded=encoded.replaceAll("[","%5B");
	encoded=encoded.replaceAll("]","%5D");
	encoded=encoded.replaceAll("|","%7C");
	encoded=encoded.replaceAll("\\","%5C");
	encoded=encoded.replaceAll("^","%5E");
	encoded=encoded.replaceAll("~","%7E");
	encoded=encoded.replaceAll("`","%60");
	
	return encoded;
}

function inputtesti(){
	var nama=document.getElementById('nama').value;
	var nama2=document.getElementById('nama').value;
	var nama=nama.replaceAll(" ","");
	var warning;
	var status='';
	warning='PERINGATAN!!!';
	if(nama.length<=3){
		warning=warning+"\n# Nama alumni tidak boleh kurang dari 3 huruf!";
		status='failed';
	}
		
	var instansi=document.getElementById('instansi').value;
	var instansi2=document.getElementById('instansi').value;
	var instansi=instansi.replaceAll(" ","");
	if(instansi.length=="0"){
		warning=warning+"\n# Nama Instansi tidak boleh dikosongkan!";
		status='failed';
	}
	var diklat=document.getElementById('diklat').value;
	var diklat2=document.getElementById('diklat').value;
	var diklat=diklat.replaceAll(" ","");
	if(diklat.length=="0"){
		warning=warning+"\n# Nama Diklat tidak boleh dikosongkan!";
		status='failed';
	}
	var testimoni=document.getElementById('testimonial').value;
	var testimoni2=document.getElementById('testimonial').value;
	var testimoni=testimoni.replaceAll(" ","");
	if(testimoni.length=="0"){
		warning=warning+"\n# Testimoni tidak boleh dikosongkan!";
		status='failed';
	}
	if(status=='failed'){
		alert(warning);
		return false;
	} else {
		warning="TERIMA KASIH \nTestimoni Anda sudah kami terima,\nsetelah selesai dimoderasi, testimoni Anda akan kami tampilkan";
		alert(warning);
		document.getElementById('nama').value='';
		document.getElementById('instansi').value='';
		document.getElementById('diklat').value='';
		document.getElementById('testimonial').value='';
		ajaxfunc('start&type=addtestimoni&nama='+encode(nama2)+'&instansi='+encode(instansi2)+'&diklat='+encode(diklat2)+'&testimoni='+encode(testimoni2),'warning','../public/ajax/ajax.php');
	}
	
}
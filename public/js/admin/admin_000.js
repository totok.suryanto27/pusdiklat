function createRequestObject() {
	var ro;
	var browser = navigator.appName;
	if(browser == "Microsoft Internet Explorer"){
		ro = new ActiveXObject("Microsoft.XMLHTTP");
	}else{
		ro = new XMLHttpRequest();
	}
	return ro;
}

var xmlhttp = createRequestObject();

function countmessage(){
	
	var str=document.getElementById('messages').value;
	var n=str.length;
	
	if(n>100){
		document.getElementById('messages').value = str.substring(0,100);
	}
	
	xmlhttp.open('get', '../public/countmessage.php?n='+n+'&str='+str, true);
	xmlhttp.onreadystatechange = function() {
		if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
			document.getElementById('counter').innerHTML = xmlhttp.responseText;
			return false;
		}
	xmlhttp.send(null);
	
}

function countmessage2(){
	
	var str=document.getElementById('metacontent').value;
	var n=str.length;
	
	if(n>156){
		document.getElementById('metacontent').value = str.substring(0,156);
	}
	
	xmlhttp.open('get', '../public/countmessage2.php?n='+n+'&str='+str, true);
	xmlhttp.onreadystatechange = function() {
		if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
			document.getElementById('counter').innerHTML = xmlhttp.responseText;
			return false;
		}
	xmlhttp.send(null);
	
}

function ajaxfunc(parameter, target, file){
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById(target).innerHTML=xmlhttp.responseText;
			return false;
		}
	}
	xmlhttp.open("GET", file+"?param="+parameter,true);
	xmlhttp.send();
}

String.prototype.replaceAll = function(s,r){return this.split(s).join(r)}


function encode(data){
	
	var encoded;
	encoded=data.replaceAll("$","%24");
	encoded=encoded.replaceAll("&","%26");
	encoded=encoded.replaceAll("+","%2B");
	encoded=encoded.replaceAll(",","%2C");
	encoded=encoded.replaceAll("/","%2F");
	encoded=encoded.replaceAll(":","%3A");
	encoded=encoded.replaceAll(";","%3B");
	encoded=encoded.replaceAll("=","%3D");
	encoded=encoded.replaceAll("?","%3F");
	encoded=encoded.replaceAll(" ","%20");
	encoded=encoded.replaceAll("@","%40");
	encoded=encoded.replaceAll('"',"%22");
	encoded=encoded.replaceAll("<","%3C");
	encoded=encoded.replaceAll(">","%3E");
	encoded=encoded.replaceAll("#","%23");
	encoded=encoded.replaceAll("{","%7B");
	encoded=encoded.replaceAll("}","%7D");
	encoded=encoded.replaceAll("[","%5B");
	encoded=encoded.replaceAll("]","%5D");
	encoded=encoded.replaceAll("|","%7C");
	encoded=encoded.replaceAll("\\","%5C");
	encoded=encoded.replaceAll("^","%5E");
	encoded=encoded.replaceAll("~","%7E");
	encoded=encoded.replaceAll("`","%60");
	
	return encoded;
}

function setup(){
		tinymce.init({
			selector: "textarea",
			theme: "modern",
			plugins: [
				"advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
				"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
				"table contextmenu directionality emoticons template textcolor paste textcolor filemanager"
				],
			toolbar1: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | formatselect fontselect fontsizeselect | image",
			menubar: false,
			resize: false

		});
}

function activatenewcluster(){
	if(document.getElementById('cluster').value!=""){
		document.getElementById('new-cluster').disabled = false;
		document.getElementById('submit-cluster').disabled = false;
	} else {
		document.getElementById('new-cluster').disabled = true;
		document.getElementById('submit-cluster').disabled = true;
		document.getElementById('new-cluster').value="";
	}
}

function editnews(){
	var title=document.getElementById('title').value;
	var ntitle=title.replaceAll(" ","");
	if(ntitle.length==0){
		document.getElementById("warning").innerHTML="<div class=\"popup\"><div class=\"pop-img\">&nbsp;</div><em>Title field can't empty!</em></div>";
		return false;
	}
		
	var content=tinyMCE.get('content').getContent();
	if(content.length==0){
		document.getElementById("warning").innerHTML="<div class=\"popup\"><div class=\"pop-img\">&nbsp;</div><em>Content field can't empty!</em></div>";
		return false;
	}
		
	ajaxfunc('start&sec=update&table=news&keyid=NewsID&field=starttabTitletabContent&page=1&col=2&th=starttabTitle&width=starttab1000px&item=starttab'+encode(title)+'tab'+encode(content)+'&image=&path=&action=edit&id='+document.getElementById('id-edit').value, 'warning', '../public/ajaxfile.php');
		
	// Clear
	document.getElementById('title').value="";
	tinyMCE.get('content').setContent('');
		//ajaxfunc('start&title='+encode(document.getElementById('title').value)+'&subtitle='+encode(document.getElementById('subtitle').value)+'&content='+encode(tinyMCE.get('content').getContent()), 'warning', '../public/editnews.php'); */
}

function addnews(){
	var title=document.getElementById('title').value;
	var ntitle=title.replaceAll(" ","");
	if(ntitle.length==0){
		document.getElementById("warning").innerHTML="<div class=\"popup\"><div class=\"pop-img\">&nbsp;</div><em>Title field can't empty!</em></div>";
		return false;
	}
		
	var content=tinyMCE.get('content').getContent();
	if(content.length==0){
		document.getElementById("warning").innerHTML="<div class=\"popup\"><div class=\"pop-img\">&nbsp;</div><em>Content field can't empty!</em></div>";
		return false;
	}
		
	ajaxfunc('start&sec=add&table=news&keyid=NewsID&field=starttabTitletabContent&page=1&col=2&th=starttabTitle&width=starttab1000px&item=starttab'+encode(title)+'tab'+encode(content)+'&image=&path=&action=edit', 'warning', '../public/ajaxfile.php');
		
	// Clear
	document.getElementById('title').value="";
	tinyMCE.get('content').setContent('');
}

function addfaq(){
	var title=document.getElementById('title').value;
	var ntitle=title.replaceAll(" ","");
	if(ntitle.length==0){
		document.getElementById("warning").innerHTML="<div class=\"popup\"><div class=\"pop-img\">&nbsp;</div><em>Question field can't empty!</em></div>";
		return false;
	}
		
	var content=tinyMCE.get('content').getContent();
	if(content.length==0){
		document.getElementById("warning").innerHTML="<div class=\"popup\"><div class=\"pop-img\">&nbsp;</div><em>Answer field can't empty!</em></div>";
		return false;
	}
		
	ajaxfunc('start&sec=add&table=faq&keyid=FaqID&field=starttabQuestiontabAnswer&page=1&col=3&th=starttabQuestiontabAnswer&width=starttab500pxtab500px&item=starttab'+encode(title)+'tab'+encode(content)+'&image=&path=&action=edit', 'warning', '../public/ajaxfile.php');
		
	// Clear
	document.getElementById('title').value="";
	tinyMCE.get('content').setContent('');
}

function editfaq(){
	var title=document.getElementById('title').value;
	var ntitle=title.replaceAll(" ","");
	if(ntitle.length==0){
		document.getElementById("warning").innerHTML="<div class=\"popup\"><div class=\"pop-img\">&nbsp;</div><em>Question field can't empty!</em></div>";
		return false;
	}
		
	var content=tinyMCE.get('content').getContent();
	if(content.length==0){
		document.getElementById("warning").innerHTML="<div class=\"popup\"><div class=\"pop-img\">&nbsp;</div><em>Answer field can't empty!</em></div>";
		return false;
	}
		
	ajaxfunc('start&sec=update&table=faq&keyid=FaqID&field=starttabQuestiontabAnswer&page=1&col=3&th=starttabQuestiontabAnswer&width=starttab500pxtab500px&item=starttab'+encode(title)+'tab'+encode(content)+'&image=&path=&action=edit&id='+document.getElementById('id-edit').value, 'warning', '../public/ajaxfile.php');
		
	// Clear
	document.getElementById('title').value="";
	tinyMCE.get('content').setContent('');
}
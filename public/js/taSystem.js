		document.onclick = handleClicks;
		
		
		var openExpandable = null;
		
		function handleClicks(evt)
		{
			var evt = (evt) ? evt : ((window.event) ? event : null);
			var objectTarget = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
			var objectId = objectTarget.id;
			var objectClass = objectTarget.className;
			
			if(objectClass.indexOf("expandableCourse") > -1 || (typeof objectTarget.parentNode.className != "undefined" && objectTarget.parentNode.className.indexOf("expandableCourse") > -1 && objectTarget.tagName == "H2"))
			{
				var rightId = (objectClass.indexOf("expandableCourse") > -1 ? objectId : objectTarget.parentNode.id);
				var height = getFullHeight(rightId);
				var currentHeight = document.getElementById(rightId).offsetHeight;
				
				if(currentHeight > 30)
				{
					initAnimate(rightId, 30);
					document.getElementById(rightId).getElementsByTagName("H2")[0].style.backgroundImage = "url(img/rightArrow.png)";
					openExpandable = null;
				}
				else
				{
					initAnimate(rightId, getFullHeight(rightId));
					document.getElementById(rightId).getElementsByTagName("H2")[0].style.backgroundImage = "url(img/downArrow.png)";
					if(openExpandable != null)
					{
						initAnimate(openExpandable, 30);
						document.getElementById(openExpandable).getElementsByTagName("H2")[0].style.backgroundImage = "url(img/rightArrow.png)";
					}
					openExpandable = rightId;
				}
			}
		}
		
		var lastKnownAnchor = "";
		homepage = false;
		
		documentSetup = function(){			
			
			homepage = true;
			var hash = window.location.href.textAfter("#");
			var tabNums = hashToNums(hash);
			
			vertNavController = new tabController("vertNav", "VertTab", ['certificationHeader', 'flyer'], tabNums[1]);
			crossNavController = new tabController("crossNav", "4Tab", ['sidebar', 'crossNavHeader'], tabNums[0]);
			allCoursesController = new pillNav("allCourses");
			
			var expandables = document.getElementById('mainShell').getElementsByTagName('DIV');
			var num = 0;
			for(var i = 0; i < expandables.length; i++)
			{
				if(expandables[i].className.indexOf("expandable") > -1)
				{
					expandables[i].id = "expandable" + num;
					if(num++%2 == 0) expandables[i].style.backgroundColor = "#f4fafd";
				}
			}
			
			doiPhone();
			
			//selectTab();
			//changeAnchor(hash);
			//setInterval("checkAnchors()", 100);
		};
		
		function checkAnchors()
		{
			var theAnchor = window.location.href.textAfter("#");
			if(theAnchor != lastKnownAnchor)
			{
				selectTab(theAnchor);
			}
		}
		
		function selectTab(theAnchor)
		{
			var tabNums = hashToNums(theAnchor);
			
			vertNavController.buttonClick(vertNavController, tabNums[1]);
			crossNavController.buttonClick(crossNavController, tabNums[0]);
		}
		
		function hashToNums(hash)
		{
			var selectedCrossTab = 1;
			var selectedVertTab = 1;
			
			switch (hash)
			{
				case "certification":
					break;
					
				case "creative":
					selectedVertTab = 2;
					break;
					
				case "it":
					selectedVertTab = 3;
					break;
					
				case "business":
					selectedVertTab = 4;
					break;
					
				case "education":
					selectedVertTab = 5;
					break;
					
				case "courses":
					selectedCrossTab = 2;
					break;
					
				case "news":
					selectedCrossTab = 3;
					break;
					
				case "events":
					selectedCrossTab = 4;
					break;
					
				default:
					break;
			}
			return [selectedCrossTab, selectedVertTab];
		}
		
		function tabController(prefix, styleSet, additionalAreas, initialSelected)
		{
			this.prefix = prefix;
			this.styleSet = styleSet;
			if(initialSelected === undefined) this.selectedTab = 1;	
			else this.selectedTab = initialSelected;
			
			this.controlledAreas = [];
			
			if(additionalAreas)
				this.controlledAreas = additionalAreas;

			this.controlledAreas.push(prefix);
	
			this.buttonClick = function(self, num)
			{
				if(self.prefix != "vertNav")
				{
					urchinTracker(window.location.href.textBefore("#") + " > " + document.getElementById(self.prefix + num).innerHTML.replace(/\s/g, "").replace(/^\s*(.*?)\s*$/,"$1"));
				}
				
				if(typeof self.controlledAreas == "undefined")
					return;
				for(var i = 0; i < self.controlledAreas.length; i++)
				{
					//document.getElementById(self.controlledAreas[i] + self.selectedTab + "_area").scrollPos = document.getElementById(self.controlledAreas[i] + self.selectedTab + "_area").scrollTop;
					document.getElementById(self.controlledAreas[i] + self.selectedTab + "_area").style.display = "none";
				}
				document.getElementById(self.prefix + self.selectedTab).className = "inactive" + self.styleSet;
				self.selectedTab = num;
				for(var i = 0; i < self.controlledAreas.length; i++)
				{
					document.getElementById(self.controlledAreas[i] + self.selectedTab + "_area").style.display = "block";
					
					/*if(typeof document.getElementById(self.controlledAreas[i] + self.selectedTab + "_area").scrollPos != "undefined")
					{
						document.getElementById(self.controlledAreas[i] + self.selectedTab + "_area").scrollTop = document.getElementById(self.controlledAreas[i] + self.selectedTab + "_area").scrollPos;
					}*/
				}
				document.getElementById(self.prefix + self.selectedTab).className = "selected" + self.styleSet;
				
				if(homepage)
				{
					if(prefix == "crossNav")
					{
						switch (num)
						{
							case 1:
								changeAnchor("certification");
								break;
							case 2:
								changeAnchor("courses");
								break;
							case 3:
								changeAnchor("news");
								break;
							case 4:
								changeAnchor("events");
								break;
						}
					}
					else if (prefix == "vertNav")
					{
						switch (num)
						{
							case 1:
								changeAnchor("certification");
								break;
							case 2:
								changeAnchor("creative");
								break;
							case 3:
								changeAnchor("it");
								break;
							case 4:
								changeAnchor("business");
								break;
							case 5:
								changeAnchor("education");
								break;
						}
					}
				}
			}
			
			
			// init
			this.buttonList = document.getElementById(prefix).getElementsByTagName("DIV");
			for(var i = 0; i < this.buttonList.length; i++)
			{
				this.buttonList[i].onclick = bindArguments(this.buttonClick, this, i+1);
			}
			
			this.buttonClick(this, this.selectedTab);
			
			var loadingDiv = document.getElementById(this.prefix + "_loading");
			if(loadingDiv)
				loadingDiv.style.display = "none";
		}
		
		function changeAnchor(to)
		{
			if (to == "")
				return;
			if(to != lastKnownAnchor)
			{
				var baseURL = window.location.href.textBefore("#");
				var newURL = baseURL + "#" + to;
				window.location.href = newURL;
				lastKnownAnchor = to;
			}
			
		}
		
		// determines the height of the content of an object that may be hidden
		function getFullHeight(objectId)
		{
			document.getElementById(objectId).style.overflow = 'scroll';
			var height = document.getElementById(objectId).scrollHeight;
			document.getElementById(objectId).style.overflow = 'hidden';
			return height;
		}

		function changeBlurb(buttonClicked)
		{
			switch(buttonClicked)
			{
				case "vertNav1":
					document.getElementById('certificationBlurb').innerHTML = "Apple offers comprehensive certification programs for creative and IT professionals in business, education and other fields. Choose your path below, tell us what you want to achieve, and we'll recommend the Apple Certifications to help you meet your goals. Click on a certification to learn more.";
					break;
				case "vertNav2":
					document.getElementById('certificationBlurb').innerHTML = "Certifications for professional photographers, filmmakers, editors, sound designers, music producers, and multimedia artists who work with Aperture, Color, DVD Studio Pro, Final Cut Pro, Final Cut Server, Logic Pro, Motion, Soundtrack Pro, and Xsan.";
					break;
				case "vertNav3":
					document.getElementById('certificationBlurb').innerHTML = "Certifications for system administrators, technical coordinators, and support professionals responsible for planning, supporting, and integrating Mac OS X, Mac OS X Server and  in a multi-platform environment.";
					break;
				case "vertNav4":
					document.getElementById('certificationBlurb').innerHTML = "Certifications for IT professionals who need to support, integrate, and provide security for Macs in commercial businesses and government.";
					break;
				case "vertNav5":
					document.getElementById('certificationBlurb').innerHTML = "Certifications for IT professionals who need to deploy, manage, and maintain Macs in their institutions, as well as creative professionals and educators who want to create and share digital content to foster creativity and empower students.";
					break;
			}
		}
		
		
		
		Element.prototype.getClass = function()
		{
			 var currentTarget = this;
			 var theClass = "";
			 while(currentTarget != document)
			 {
				  if(currentTarget.className != "")
				  {
						theClass = currentTarget.className;
						break;
				  }
				  else
						currentTarget = currentTarget.parentNode;
			 }
			 return theClass;
		}
		
		Element.prototype.getId = function()
		{
			 var currentTarget = this;
			 var theId = "";
			 while(currentTarget != document)
			 {
				  if(currentTarget.id != "")
				  {
						theId = currentTarget.id;
						break;
				  }
				  else
						currentTarget = currentTarget.parentNode;
			 }
			 return theId;
		}



	
	
	
	     var gBeginElementID = '';

        function myAddListener(obj, evt, handler, captures)
        {
            if ( document.addEventListener )
                obj.addEventListener(evt, handler, captures);
            else
                obj.attachEvent('on' + evt, handler); // IE requires this form
        }

        function getTargetId(event)
        {
            // an event has the target element as a property. the property has a 
            //  different name in IE so handle that here
            var target = event.target ? event.target : event.srcElement;
            return target.id;
        }

        function pluginBegin(evt)
        {
            // the plug-in has been loaded, so grab the ID of the element the browser used
            //  (some use the <object> and some use the <embed>))
            gBeginElementID = getTargetId(evt);

            //logMsg("'" + evt.type + "' from element id '" + gBeginElementID + "'");
            onEvent(evt);       
        }

        function onEvent(evt)
        {
           if ( '' == gBeginElementID )
               gBeginElementID = getTargetId(evt);
               
           //logMsg("ID = '" +gBeginElementID + "' onEvent: " + evt.type);
				switch (evt.type) 
				{
				
					case "qt_ended":
						var idToUse = "qtmovie_embed";
						if(document.getElementById(idToUse) == null)
							idToUse = "qtmovie_object";
							
						if(document.getElementById(idToUse).GetURL().indexOf("mov") > -1)
						{
							document.getElementById(idToUse).SetURL("../img/certPoster.png");
							document.getElementById(idToUse).SetHREF("../img/AATC_30sec_Spot.mov");
							document.getElementById(idToUse).SetTarget("myself");
						}
						break;
				}
        }

        function movieSetup()
        {
            // register for events on the <object> element, our handler will be triggered even if
            //  the browser uses the <embed> because events pass through the parent element
            var plug = document.getElementById('qtmovie_object');

            if ( null == plug )
            {
                //alert("QT plug-in not instantiated on <body> load event???");
                return;
            }

            myAddListener(plug,'qt_begin',       pluginBegin, false);
            myAddListener(plug,'qt_ended',           onEvent, false);
        }
	 
			// a utility function to extract the text after an underscore
			String.prototype.textAfterUnderscore = function()
			{
				var underscorePos = this.lastIndexOf('_');
				return this.substr((underscorePos + 1), aString.length);
					
			}
			
			String.prototype.textAfter = function(aChar)
			{
				var underscorePos = this.lastIndexOf(aChar);
				return (underscorePos > -1 ? this.substr((underscorePos + 1), this.length) : "");
					
			}
			
			// a utility function to extract the text before an underscore
			String.prototype.textBefore = function(aChar)
			{
				var underscorePos = this.lastIndexOf(aChar);
				return (underscorePos > -1 ? this.substr(0, underscorePos) : this);
			}
			
			
			// returns an anonymous function for applications that want a function but need a function with arguments
			function bindArguments(fn)
			{
			  var args = [];
			  for (var n = 1; n < arguments.length; n++)
				 args.push(arguments[n]);
			  return function () { return fn.apply(this, args); };
			}
			
			
	var proAppsLink = "/certification/proapps";
	var masterProLink = "/certification/masterpro";
	var xsanLink = "/certification/xsan";
	var actLink = "/act";
	var osXLink = "/certification/macosx";
	var associateLink = "/certification/associate";
	var macIntegLink = "/itpro/macinteg";
	
	
	var certifications =         [
								/* 0 */			["Apple Certified Trainer for Pro Applications", actLink],
								/* 1 */			["Aperture Level One", proAppsLink],
								/* 2 */			["Color Correction in Final Cut Studio Level One", proAppsLink],
								/* 3 */			["Final Cut Studio Master Pro", masterProLink],
								/* 4 */			["DVD Studio Pro Level One", proAppsLink],	
								/* 5 */			["Final Cut Express Level One", proAppsLink],
								/* 6 */			["Final Cut Pro Level One", proAppsLink],	
								/* 7 */			["Final Cut Pro Level Two", proAppsLink],	
								/* 8 */			["Apple Certified Media Administrator", xsanLink],
								/* 9 */			["Final Cut Server Level One", proAppsLink],
								/* 10 */			["Motion Level One", proAppsLink],	
								/* 11 */			["Final Cut Studio Motion Graphics Level One", proAppsLink],
								/* 12	*/			["Sound Editing in Final Cut Studio Level One", proAppsLink],
								/* 13	*/			["Logic Pro Level One", proAppsLink],
								/* 14 */			["Logic Pro Level Two", proAppsLink],
								/* 15	*/			["Logic Studio Master Pro", proAppsLink],
											
								/* 16 */			["Apple Certified Trainer for IT", actLink],
								/* 17 */			["Apple Certified Support Professional 10.6", osXLink],
								/* 18 */			["Apple Certified Technical Coordinator 10.6", osXLink],
								/* 19 */			["Apple Certified Specialist - Directory Services 10.6", osXLink],
								/* 20 */			["Apple Certified Specialist - Deployment 10.6", osXLink],	
								/* 21 */			["Apple Certified Specialist - Security and Mobility 10.6", osXLink],
								/* 22 */			["Apple Certified System Administrator 10.6", osXLink],	
								/* 23 */			["Xsan 2 Administrator", xsanLink],	
								/* 24	 */		["Apple Certified Macintosh Technician", "/certification/acmt"],
								/* 25 */			["Apple Certified Associate - iLife", [associateLink] ],
								/* 26 */			["Apple Certified Associate - iWork", [associateLink] ],
								/* 27 */			["Compressor Level One", [proAppsLink] ],
								/* 28 */			["Mac Integration Basics", [macIntegLink] ],
										 ];
	
	var creativeOptions = 		  [
											["Edit, adjust and deliver digital photography", [1] ],
											["Color correct and grade", [2, 3] ],
											["Author DVDs", [4, 3, 27] ],
											["Edit video or film", [5, 6, 7, 3, 8, 27] ],
											["Manage media assets and workflow", [9, 8, 27] ],
											["Add motion graphics and effects to video and images", [10, 3] ],
											["Create, perform and record music and audio", [12, 13, 14, 15] ],
											["Become an Apple Certified Trainer", [0, 27] ]
							   		  ];
	
	var itOptions =             [
										  ["Add a Mac to a Windows network", [28]],
										  ["Support Mac OS X users and systems", [17]],
										  ["Provide network services with Mac OS X Server", [18]],
										  ["Integrate Mac OS X and Mac OS X Server into a mixed platform environment", [18, 19]],
										  ["Deploy Mac systems and software", [18, 20]],
										  ["Provide secure network services to desktop and mobile devices (iPhone, iPod Touch, and notebooks)", [18, 21]],
										  ["Manage the integration and deployment of Mac systems in a mixed platform environment", [22]],
										  ["Provide high speed shared storage for media professionals or high traffic services such as mail servers", [23, 8]],
										  ["Service and support Mac hardware", [24]],
										  ["Become an Apple Certified Trainer", [16]]
										 ];
	
	var businessOptions =       [["Support Mac OS X users and systems", [17]],
										  ["Provide network services with Mac OS X Server", [18]],
										  ["Integrate a Mac into a mixed platform environment", [28, 18, 19]],
										  ["Provide secure network services to desktop and mobile devices (iPhone, iPod Touch, and notebooks)", [18, 21]],
										  ["Deploy Mac systems and software", [18, 20]],
										  ["Manage the integration and deployment of Mac systems in a large scale environment", [22]],
										  ["Provide high speed shared storage for media professionals", [23, 8]],
										  ["Service and support Mac hardware", [24]]
										 ];
	
	var educationOptions =      [["Support Mac OS X users and systems in my institution", [17]],
										  ["Provide network services with Mac OS X Server", [18]],
										  ["Integrate a Mac into a mixed platform environment", [28, 18, 19]],
										  ["Deploy Mac systems and software at my institution", [18, 20]],
										  ["Provide secure network services to desktop and mobile devices (iPhone, iPod Touch, and notebooks)", [18, 21]],
										  ["Manage network services and the deployment and integration of Macs in my institution", [22]],
										  ["Provide high speed shared storage for media labs or high traffic services (such as Podcast Producer, mail and iCal)", [23, 8]],
										  ["Create and share presentations, spreadsheets, and more", [25, 26]]
										 ];

	
	function displayRelevantCertifications(category)
	{
		var usedCertifications = [];
		var checkboxes = document.getElementById(category + "Table").getElementsByTagName("INPUT");
		var outputHTML = '<div style="float: left; color: black; font-weight: bold;">Suggested Certifications:</div><div style="float: right;"><a href="javascript:printSuggestions(\'' + category + '\');">Printable Version</a></div><div class="myclear"></div><div id="' + category + 'Suggestions">';
		var blue = true;
		var cookieString = "";
		
		for(var i = 0; i < checkboxes.length; i++)
		{
			if(checkboxes[i].checked)
			{
				var checkNum = checkboxes[i].id.textAfter("_") - 0;
				var matches = eval(category + "Options")[checkNum][1];
				cookieString += checkNum + ",";
				for(var j = 0; j < matches.length; j++)
				{
						var used = false;
						for(var k = 0; k < usedCertifications.length; k++)
						{
							if(usedCertifications[k] == matches[j])
							{
								used = true;
								break;	
							}
						}
						
						if(!used)
						{
							usedCertifications.push(matches[j]);
							outputHTML += '<div style="padding-left: 9px; padding-top: 6px; width: 100%; height: 24px; background-color: ' + (blue ? '#f2f2f2' : 'ffffff') + ';"><a style="text-decoration: none;" href="' + certifications[matches[j]][1] + '">' + certifications[matches[j]][0] + "</a></div>";
							blue = !blue;
						}
					}
				
			}
		}
		
		cookieString = cookieString.substring(0, cookieString.length - 1);
		createCookie(category + "Checkboxes", cookieString, 5);
		
		document.getElementById(category + "Area").innerHTML = outputHTML + '</div>';
	}
	
	function makeCheckboxes(prefix)
	{
		var cookies = readCookie(prefix + "Checkboxes");
		if(cookies === null)
		{
			cookies = [];	
		}
		else
		{
			cookies = cookies.split(",");
		}
		document.write('<div><span class="blackText boldText">I would like to...</span> (check all that apply)</div><br />');
		document.write('<table id="' + prefix + 'Table"><tr><td valign="top">');
		for(var i = 0; i < eval(prefix + 'Options').length; i+=2)
		{
			document.write('<div style="margin-bottom: 8px;"><label onclick="displayRelevantCertifications(\'' + prefix + '\')"><input type="checkbox" id="' + prefix + "_" + i + '" ' + (cookies.hasMember(i + "") ? 'checked="checked" ' : '') + '/> ' + eval(prefix + 'Options')[i][0] + "</label></div>");
		}	
		document.write('</td><td valign="top">');
		for(var i = 1; i < eval(prefix + 'Options').length; i+=2)
		{
			document.write('<div style="margin-bottom: 8px;"><label onclick="displayRelevantCertifications(\'' + prefix + '\')"><input type="checkbox" id="' + prefix + "_" + i + '" ' + (cookies.hasMember(i + "") ? 'checked="checked" ' : '') + '/> ' + eval(prefix + 'Options')[i][0] + "</label></div>");
		}	
		
		document.write('</td></tr></table>');
		document.write('<div id="' + prefix + 'Area"><div style="float: left; color: black; font-weight: bold;">Suggested Certifications:</div><div style="float: right;"><a href="javascript:printSuggestions(\'' + prefix + '\');">Printable Version</a></div></div>');
		
		displayRelevantCertifications(prefix);
	}
	
	function printSuggestions(prefix)
	{
		var newWindow = window.open("","wildebeast","width=640,height=480,scrollbars=1,resizable=1");
		var html = '<html><head><title>Suggested Certifications</title><style type="text/css" media="all"><!--@media print {#printButton {display:none;}}--></style></head><body style=\'font: 12px "Lucida Grande", Geneva, Arial, Verdana, sans-serif;\'><div style="float: left;">I would like to:</div><div style="float: right;" id="printButton"><a href="javascript:window.print()">Print</a></div><div style="clear: both;"></div><hr style="border: none; border-bottom: 1px solid black;"/><ul>';
		
		var checkboxes = document.getElementById(prefix + "Table").getElementsByTagName("INPUT");
		for(var i = 0; i < checkboxes.length; i++)
		{
			if(checkboxes[i].checked)
			{
				html += "<li>" + checkboxes[i].parentNode.innerHTML.replace(/<input[^>]*>/g, "") + "</li>";	
			}
		}
		
		html += '</ul><br /><b>Suggested Certifications:</b><div style="height: 5px;"></div><table width="480" style="border-collapse: collapse; font-size: 12px;" cellpadding="5">';
		html += document.getElementById(prefix + "Suggestions").innerHTML.replace(/ style="[^"]*"/g, "").replace(/<a href="([^"]*)">([^<]*)<\/a>/g,'<tr><td style="border: 1px solid black;">$2</td><td style="border: 1px solid black;"> training.apple.com$1</td></tr>');
		html += '</table></body></html>';
		
		newWindow.document.open();
		newWindow.document.write(html);
		
	}
	
	/*
ANIMATION FUNCTIONS
*/


// controls the distance elements are moved on each iteration
var animateSpeed = 5;

// initAnimate and animateObject control elements whose height are animated
function initAnimate(objectID, y) {
	var anObject = document.getElementById(objectID);
	var fY = y - 15;
	var cY = anObject.offsetHeight - 15;
	var dY = Math.abs(fY-cY);
	var stepY = dY/20;
		
	if (cY<fY) stepY= stepY;
	else if (cY>fY) stepY =  -stepY;
	animateObject(objectID, stepY, fY, cY, dY);
}

function animateObject(objId, stepY, fY, cY, dY)
{
	var anObject = document.getElementById(objId);
	if ((dY > 0)) {
		anObject.style.height = Math.round(cY) + 'px';
		cY = cY + stepY;
		dY = dY - Math.abs(stepY);
		setTimeout ('animateObject("' + objId + '", ' + stepY + ', ' + fY + ', ' + cY + ', ' + dY + ')', 0);
	}else {
		anObject.style.height = fY + 'px';
}
	return;
}

// control the distance by which elements are scrolled in horizontal and
// vertical animations, respectively
scrollSpeed = 75;
vertScrollSpeed = 25;

// takes a CSS formated pixel value, i.e. "40px" and returns the corresponding integer
function getNumValue(anObjValPX){
	anObjOff = anObjValPX.lastIndexOf('px');
	return (anObjValPX.substr(0,anObjOff) - 0);
}


// the pillNav class manages navs using the animated pill bar metaphor
function pillNav(prefix)
{
	this.prefix = prefix;
	this.allowedToSelect = true;
	this.selectedTab = prefix + "_button1";
	this.stepXScroll;
	this.fXScroll;
	this.cXScroll;
	this.dXScroll;
	this.objectToScroll = document.getElementById(prefix + 'Courses');
	this.pillGraphic = document.getElementById(prefix + "Lozenge");
	this.pillToSlide;
	this.stepXPill;
	this.fXPill;
	this.cXPill;
	this.dXPill;
	this.distanceBetweenLastAndCurrent;
	this.scrollSpeed = 25;
	this.width = 690;
	
	this.numTabs = 0;
	
	var tabs = document.getElementById(prefix + "PillHolder").getElementsByTagName("DIV");
	for(var i = 0; i < tabs.length; i++)
	{
		this.numTabs++;
		if(tabs[i].className == "grayBarButton")
		{
			var html = tabs[i].innerHTML;
			var outputHTML = '';
			for(var j = 0; j < courses[html].length; j++)
			{
				var info = courses[html][j];
				outputHTML += '<div class="expandableCourse"><h2>' + info[0] + '</h2><a href="' + info[3].substr(1) + '"><img src="img/moreInfo.png" style="float: right; margin-right: 10px;"/></a><div class="myclear"></div><p style="margin-top: 1px; width: 80%;">' + info[1] + '<br />' + (info[2] != "" ? '<em>(' + info[2] + ')</em>' : "") + '</p></div>';
			}
			document.getElementById(prefix + this.numTabs).innerHTML = outputHTML;
			
		}
	}
	
	
	
	this.onPillClick = function(self, tabToSelect)
	{
		if(self.allowedToSelect)
		{
			var selectedOne = self.selectedTab.substring(self.selectedTab.length - 1) - 0;
			var desiredOne = tabToSelect.substring(tabToSelect.length - 1) - 0;
			var scrollDelta = 0;
			document.getElementById(self.prefix + desiredOne).style.display="block";
			self.pillToSlide = document.getElementById(tabToSelect);
			
			
			if(desiredOne > selectedOne) scrollDelta = self.width;
			else if(desiredOne < selectedOne)
			{
				scrollDelta = -self.width;
				self.objectToScroll.scrollLeft = self.width;
			}
			
			self.initScroll(scrollDelta);
		}
	}
	
	for(var i = 0; i < this.numTabs; i++)
	{
		var buttonName = prefix + "_button" + (i + 1);
		document.getElementById(buttonName).onclick = bindArguments(this.onPillClick, this, buttonName);	
	}
	
	this.initScroll = function(scrollDelta) {
		this.fXScroll = scrollDelta;
		this.cXScroll = this.objectToScroll.scrollLeft;
		this.dXScroll = Math.abs(this.fXScroll - this.cXScroll);
		if(this.dXScroll == 0) return;
		this.allowedToSelect = false;
			
		if (this.cXScroll < this.fXScroll) this.stepXScroll = this.scrollSpeed;
		else if (this.cXScroll > this.fXScroll) this.stepXScroll = -this.scrollSpeed;
		
		this.doScroll();
		this.initPillSlide(this.pillToSlide.offsetLeft);
	}
	
	this.doScroll= function()
	{
		if ((this.dXScroll > 0)) {
			this.objectToScroll.scrollLeft = Math.round(this.cXScroll);
			this.cXScroll = this.cXScroll + this.stepXScroll;
			this.dXScroll = this.dXScroll - Math.abs(this.stepXScroll);
			setTimeout (this.prefix + 'Controller.doScroll()', 0);
		}else {
			document.getElementById(this.prefix + this.selectedTab.substring(this.selectedTab.length - 1)).style.display = 'none';
			this.selectedTab = this.prefix + "_button" + this.pillToSlide.id.substring(this.pillToSlide.id.length - 1);
			
			this.objectToScroll.scrollLeft = 0;
			
			this.allowedToSelect = true;
		}
		return;
	}
	
	this.initPillSlide= function(x) {
		this.fXPill = x;
		this.cXPill = this.pillGraphic.offsetLeft;
		this.dXPill = Math.abs(this.fXPill - this.cXPill);
		this.distanceBetweenLastAndCurrent = this.dXPill;
		
		var newSpeed = (this.dXPill/this.width) * this.scrollSpeed;
			
		if (this.cXPill < this.fXPill) this.stepXPill = newSpeed;
		else if (this.cXPill > this.fXPill) this.stepXPill =  -newSpeed;
		this.doPillSlide();
	}
	
	this.doPillSlide= function()
	{
		if ((this.dXPill > 0)) {
			this.pillGraphic.style.left = Math.round(this.cXPill) + 'px';
			this.cXPill = this.cXPill + this.stepXPill;
			this.dXPill = this.dXPill - Math.abs(this.stepXPill);
			
			var percentageDone = this.dXPill/this.distanceBetweenLastAndCurrent;
			var whiteColor = Math.round(percentageDone*255);
			var blackColor = 255 - whiteColor;
			document.getElementById(this.selectedTab).style.color = 'rgb(' + blackColor +  ',' + blackColor + ',' + blackColor + ')';
			this.pillToSlide.style.color = 'rgb(' + whiteColor +  ',' + whiteColor + ',' + whiteColor + ')';
			
			setTimeout (this.prefix + 'Controller.doPillSlide()', 0);
		}else {
			this.pillGraphic.style.left = this.fXPill + 'px';
			
	}
		return;
	}
	
	
	this.onPillClick(this, this.prefix + "1");
	document.getElementById(this.selectedTab).style.color = "black";
	
}

var courses = [];


courses["Mac OS X"] = [
							  ["Online: Mac Integration Basics 10.6", "Self-paced learning content that illustrates how to integrate Macs into a standards-based network, such as with a Windows Server.", "Online", "/itpro/macinteg"],
							  ["Snow 100: Introduction to Mac OS X 10.6", "Provides an introduction to Mac OS X for those new to the Mac.", "1 day", "/itpro/snow100"],
							  ["Snow 101: Mac OS X Support Essentials 10.6", "Provides an intensive and in-depth exploration of troubleshooting on Mac OS X, touring the breadth of functionality of Mac OS X systems.", "3 days", "/itpro/snow101"],
							  ["Snow 201: Mac OS X Server Essentials 10.6", "Gives technical coordinators and entry-level system administrators the knowledge to implement a Mac OS X Server-based system.", "4 days", "/itpro/snow201"],
							  ["Snow 101+ 201: Apple Certified Technical Coordinator 10.6 Bootcamp", "Combined delivery of Mac OS X Support Essentials and Mac OS X Server Essentials courses, with the associated certification exams.", "length varies", "/itpro/snowactc"],
							  ["Snow 301: Mac OS X Directory Services 10.6", "Teaches the skills to configure Mac OS X computers to access directory services, and configure Mac OS X Server to provide directory services in a mixed-platform environment.", "4 days", "/itpro/snow301"],
							  ["Snow 302: Mac OS X Deployment 10.6", "Focuses on solutions for deploying and maintaining software, ranging from the installation of individual files to the deployment of complete system images to multiple computers.", "3 days", "/itpro/snow302"],
							  ["Snow 303: Mac OS X Security and Mobility 10.6", "Teaches the skills to configure Mac OS X Server to provide network services to allow mobile computers and devices such as the iPhone to access private networks and services in a secure manner.", "3 days", "/itpro/snow303"]
							  
							  ];


courses["Pro Applications"] = [
											["Aperture 101: Introduction to Aperture 3", "Starts with the basics of image management and takes students step-by-step through Aperture's powerful photo editing, image retouching, proofing, publishing, and archiving features.", "2 days", "/creative/aperture101"],
											["Color 101: Introduction to Color Correction in Final Cut Studio", "Introduces students to Final Cut Studio's color correcting capabilities. Start with the basics of color correction in Final Cut Pro and move on to the fine points of secondary grading in Color.", "2 days", "/creative/color101"],
											["DVDSP 101: Introduction to DVD Studio Pro 4", "Delves into the world of DVD Studio Pro, covering all aspects of DVD authoring.", "3 days", "/creative/dvdsp101"],
											["FCE 101: Introduction to Final Cut Express 4", "Introduces students to the primary feature set and basic interface of Final Cut Express.", "3 days", "/creative/fce101"],
											["FCP 100: Overview of Final Cut Pro 7", "Introduces students to the primary feature set and basic interface of Final Cut Pro.", "2 days", "/creative/fcp100"],
											["FCP 101: Introduction to Final Cut Pro 7", "Teaches basic editing function while familiarizing students with the basic user interface.", "3 days", "/creative/fcp101"],
											["FCP 102: Introduction to Final Cut Pro 7, Prime Time", "Teaches basic editing function while familiarizing students with the basic user interface.", "evening sessions", "/creative/fcp102"],
											["FCP 200: Comprehensive Study of Final Cut Pro 7", "Provides an in-depth study of the primary feature set and basic interface of Final Cut Pro.", "5 days", "/creative/fcp200"],
											["FCP 300: Final Cut Pro 7 Advanced Editing", "Covers advanced editing techniques, including creating polished transitions, fixing screen direction errors, editing multi-camera projects, and compositing like a pro. Also covers advanced workflows for managing media and working with film.", "3 days", "/creative/fcp300"],
											["FCSvr 101: Introduction to Administering Final Cut Server", "Guides you through administering Final Cut Server, Apple's powerful new media asset management and workflow automation software. This course combines lectures and hands-on case study exercises to provide practical real-world experience.", "2 days", "/creative/fcsvr101"],
											["Logic 101: Introduction to Logic Express 9 and Logic Pro 9", "Introduces students to the primary feature set and basic user interface of Logic Express and Logic Pro.", "3 days", "/creative/logic101"],
											["Logic 102: Introduction to Logic Express 9 and Logic Pro 9, Prime Time", "Introduces students to the primary feature set and basic user interface of Logic Express and Logic Pro.", "evening sessions", "/creative/logic102"],
											["Logic 301:  Advanced Music Production in Logic Pro 9", "Takes students through Logic Pro's powerful advanced features, covering everything from production to editing and mixing.", "3 days", "/creative/logic301"],
											["Motion 101: Introduction to Motion 4", "Covers interface fundamentals, particles, blend modes, layer organization, and fundamental multi-layer editing.", "3 days", "/creative/motion101"],
											["Sound 101: Introduction to Sound Editing in Final Cut Studio", "Introduces students to Final Cut Studio's sound editing capabilities. Covers editing audio files, repairing field recordings, multi-track arranging, and other creative sound design techniques.", "2 days", "/creative/sound101"]
							  ];


courses["Xsan"] = [
							  ["Xsan 301: Xsan Administration", "Teaches the skills needed to deploy, configure, performance tune, and troubleshoot a storage area network using Apple's Xsan 2 tools.", "3 days", "/itpro/xsan301"],
							  ];



courses["iWork"] = [
							  ["iWork 101: Introduction to iWork", "Learn to use the iWork writing and graphics tools to create beautiful documents. This suite of productivity tools includes Pages for producing professional documents, Numbers for spreadsheets, tables and charts, and Keynote for high-quality presentations.", "2 days", "/iapps/iwork101"]
							  ];


courses["IT T3"] = [
							  ["Snow 101 T3: Mac OS X Support Essentials 10.6 Train-the-Trainer", "Prepares training professionals to deliver the Snow 101 course. Acceptance into Apple Certified Trainer program is required.", "4 days", "/itpro/snow101-t3"],
							  ["Snow 201 T3: Mac OS X Server Essentials 10.6 Train-the-Trainer", "Prepares training professionals to deliver the Snow 201 course. Acceptance into Apple Certified Trainer program is required.", "4 days", "/itpro/snow201-t3"],
							  ["Snow 301 T3: Mac OS X Directory Services 10.6 Train-the-Trainer", "Prepares training professionals to deliver the Snow 301 course. Acceptance into Apple Certified Trainer program is required.", "4 days", "/itpro/snow301-t3"],
							  ["Snow 302 T3: Mac OS X Deployment 10.6 Train-the-Trainer", "Prepares training professionals to deliver the Snow 302 course. Acceptance into Apple Certified Trainer program is required.", "3 days", "/itpro/snow302-t3"],
							  ["Snow 303 T3: Mac OS X Security and Mobility 10.6 Train-the-Trainer", "Prepares training professionals to deliver the Snow 303 course. Acceptance into Apple Certified Trainer program is required.", "4 days", "/itpro/snow303-t3"],
							  ["Xsan 301 T3: Xsan 2 Administration Train-the-Trainer", "Prepares training professionals to deliver the Xsan 301 course. Acceptance into Apple Certified Trainer program is required.", "Length varies", "/itpro/xsan301-t3"]
							  ];


courses["Pro Apps T3"] = [
							  ["Aperture T3: Aperture 3 Train-the-Trainer", "Prepares training professionals to deliver the Aperture courses. Acceptance into Apple Certified Trainer program is required.", "3 days", "/creative/aperture-t3"],
							  ["FCP T3: Final Cut Pro 7 Train-the-Trainer", "Prepares training professionals to deliver the Final Cut Pro and Final Cut Studio courses. Acceptance into Apple Certified Trainer program is required.", "4 days", "/creative/fcp-t3"],
							  ["Logic T3: Logic Pro 9 Train-the-Trainer", "Prepares training professionals to deliver the Logic Pro courses. Acceptance into Apple Certified Trainer program is required.", "4 days", "/creative/logic-t3"],
							  ["Motion T3: Motion 4 (online certification)", "Apple Certified Trainer - Motion Level One is an online certification and does not require Train-the-Trainer course attendance. Acceptance into Apple Certified Trainer program is required.", "", "/creative/motion-t3"],
							  
								  ];

courses["See All"] = courses["Mac OS X"].concat(courses["Pro Applications"]).concat(courses["Xsan"]).concat(courses["iWork"]).concat(courses["IT T3"]).concat(courses["Pro Apps T3"]);

courses["See All"].sort(sortCourses);

function sortCourses(firstCourse, secondCourse){
var a = firstCourse[0].toUpperCase();
var b = secondCourse[0].toUpperCase();
if(a < b)
	return -1;
else if(a > b)
	return 1;
else
	return 0;
}


function createCookie(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function eraseCookie(name) {
	createCookie(name,"",-1);
}

Array.prototype.hasMember = function (value)
{
	for (var i=0; i < this.length; i++)
	{
		if (this[i] == value)
		{
			return true;
		}
		
	}
	return false;
};

function doiPhone()
{
	if(isiPhone())
	{
		alert("Welcome. Please use two fingers to scroll through boxes of content on this site.");
	}
}

function isiPhone()
{
	return ((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i)) || (navigator.userAgent.match(/iPad/i)));
}
<?php
	include('connection.php');
	
  function str_cut($string,$length) {

    //### Get the string length
    $strLen = strlen($string);

    //### If the string length is less than $length, just return the string
    if($strLen < $length) {

      return $string;

    } else {

      //### Find the next space
      $nextSpace = stripos($string, ' ', $length);

      //### Check if we are in a <a> element
      if($string[$nextSpace-2].$string[$nextSpace-1] == '<a') {
        $nextSpace = stripos($string, ' ', $nextSpace+1); 
      }

      //### If we find the next space, cut the string and add "..."
      //### If we don't find the next space, return the whole string
      if(is_int($nextSpace)) {
        return substr($string,0,$nextSpace).'...';
      } else {
        return substr($string,0,$strLen);
      }

    }

  }
	
	// Dapatkan total semua data
	$sql="SELECT * FROM posts WHERE CatID='".$_GET['cat']."'";
	$query=@mysql_query($sql);
	$ndata=@mysql_num_rows($query);
	// Dapatkan Banyak Halaman
	$itemperpage=15;
	$totalpage=ceil($ndata/$itemperpage);
	
	// Tampilkan data
	$sql="SELECT * FROM posts WHERE CatID='".$_GET['cat']."' ORDER BY PostID DESC LIMIT ".($itemperpage*($_GET['page']-1)).",".$itemperpage;
	$query=@mysql_query($sql);
	while($row=@mysql_fetch_array($query)){
		echo "<div class=\"courseTitle\" style=\"padding: 0; margin-bottom: 10px;\">".$row['TitlePost']."</div>";
		echo strip_tags(trim(str_cut($row['ContentPost'],200)));
		echo "<br /><a href=\"http://pusdiklat.bkpm.go.id/read/".$row['Url']."\">Selanjutnya</a>";
		echo "<div style=\"padding-top: 20px; clear: both;\"></div>";
	}
	// Halaman
	echo 
		"<div class=\"pagination\">";
	if($_GET['page']!=1){
		echo
			"<span class=\"pg-normal\" onclick=\"pageindex('".($_GET['page']-1)."','".$_GET['cat']."')\">Prev</span>";
	} else {
		echo
			"<span class=\"pg-selected\" >Prev</span>";
	}
	
	if($_GET['page']>3){ 
		$batas=$_GET['page']+2; 
	} else {
		$batas=5;
	}
	$i=1;
	$status="";
	$status_="";
	$status__="";
	while($i<=$totalpage){
		if($i!=$_GET['page']){
			if($totalpage>=10){
				if($i<=$batas || $i>$totalpage-2){
					if($_GET['page']>6){
						if($i<=2){
							echo "<span class=\"pg-normal\" onclick=\"pageindex2('".$i."','".$_GET['cat']."')\">".$i."</span>";
						} else {
							if($status_==""){
								$status_="ada";
								echo " ... ";
							}
						}
						
						if($i>=$_GET['page']-2 && $i<=$_GET['page']+2){
							echo "<span class=\"pg-normal\" onclick=\"pageindex2('".$i."','".$_GET['cat']."')\">".$i."</span>";
						}
						
						if($i>$totalpage-2 && $i>$_GET['page']+2){
							echo "<span class=\"pg-normal\" onclick=\"pageindex2('".$i."','".$_GET['cat']."')\">".$i."</span>";
						}
						
					} else {
						echo "<span class=\"pg-normal\" onclick=\"pageindex2('".$i."','".$_GET['cat']."')\">".$i."</span>";
					}
				} else {
					if($status==""){
						$status="ada";
						echo " ... ";
					}
				}
			} else {
				echo "<span class=\"pg-normal\" onclick=\"pageindex2('".$i."','".$_GET['cat']."')\">".$i."</span>";
			}
		} else {
			echo "<span class=\"pg-selected\" >".$i."</span>";
		}
		$i++;
	}
	
	if($_GET['page']!=$totalpage){
		echo 
			"	<span class=\"pg-normal\" onclick=\"pageindex2('".($_GET['page']+1)."','".$_GET['cat']."')\">Next</span>";
	} else {
		echo 
			"	<span class=\"pg-selected\" >Next</span>";
	}
	echo
		"</div>";
	?>
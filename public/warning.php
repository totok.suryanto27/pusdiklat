<?php //if($this->session->userdata('warning_diklat')!=""){ ?>

<div id="container">
	<!-- Left Panel -->
	<div class="left-panel">
		<div id="tabBar">
			<div id="courseNav">
				<div class="inactive2Tab" id="courseNav1" >Kuota Peserta</div>
				<div class="myclear"></div>
				
				<div id="courseNav1_area" class="courseContentSmall off" style="min-height: 300px;">
					<div class="courseTitle" style="padding: 0; margin-bottom: 10px; border-bottom: 2px solid blue; padding-bottom: 15px;">Maksimum Kuota Peserta</div>
					<div class="flexigrid" style="width: 650px;">
						<div class="mDiv" align="center">
							<div class="ftitle">
								Maaf!!! Maksimum kuota dari wilayah yang sama dibatasi
								<br />maksimum hanya 5 peserta per diklat, silahkan daftar kembali
								<br />di diklat yang sama tetapi angkatan yang berbeda
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Right Panel -->
	<div class="right-panel">
		<div class="certColumn">
			<div class="certBox">
				<h3>Link Eksternal</h3>
                    <div class="certBoxContent">
                        <div style="padding: 10px; font-size: 90%;">
							<ul>
								
								<?php
									$this->db->select('*');
									$this->db->from('extlink');
									$this->db->order_by("ExtID","desc");
									$this->db->limit(10);
									$query=$this->db->get();
									$result=$query->result();
									foreach($result as $row){
										echo"<li><a href=\"http://".$row->ExtLink."\" target=\"_blank\">".$row->ExtLink."</a></li>";
									}
								?>
								
							</ul>
						</div>
						<div class="myclear"></div>
                    </div>
                    <div class="certBoxFooter"></div>
			</div>
        </div>
		
		<div class="certColumn">
			<div class="certBox">
				<h3>Ikuti Kami ...</h3>
					<div class="certBoxContent">
						<div style="padding: 10px; font-size: 90%;" align="center">
							<?php
								$this->db->select('*');
								$this->db->from('sosmed');
								$query=$this->db->get();
								$result=$query->result();
								foreach($result as $row){
									if($row->Url!=""){
										if($row->SosMed=="Facebook"){ $img="fb-icon.png"; }
										if($row->SosMed=="Twitter"){ $img="tw-icon.png"; }
										if($row->SosMed=="Google Plus"){ $img="gp-icon.png"; }
										if($row->SosMed=="Instagram"){ $img="insta-icon.png"; }
										echo
											"<a href=\"".$row->Url."\" title=\"Pusdiklat BKPM di ".$row->SosMed."\"	target=\"_blank\">
												<img src=\"".base_url()."public/images/".$img."\" /></a>";
									}
								}
							?>
						</div>
						<div class="myclear"></div>
					</div>
					<div class="certBoxFooter"></div>
			</div>
		</div>
	</div>
	
</div>
<?php 
//$this->session->set_userdata('warning_diklat','');
//} else {
	//echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."\" />";
//} ?>
<?php
	session_start();
	error_reporting(E_ALL ^ E_NOTICE);
	require('connection.php');
	
	$diklat=$_GET['iddik'];
	$angkatan=$_GET['idakt'];
	$tahun=$_GET['tahun'];
	
	require_once 'Classes/PHPExcel.php';
	$objPHPExcel = new PHPExcel();
	$sheet = $objPHPExcel->getActiveSheet();
	$objPHPExcel->getActiveSheet()->setTitle('Report');
	
	// ============ GENERAL SETTING =============//
	$default_border = array(
		'style' => PHPExcel_Style_Border::BORDER_THIN,
		'color' => array('rgb'=>'cccccc')
		);

	$style_header2 = array(
			'borders' => array(
				'allborders' => $default_border,
			)
		);

	$style_header = array(
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb'=>'b9c9fe'),
			),
			'font' => array(
				'bold' => true,
			),
			'borders' => array(
				'allborders' => $default_border,
			)
		);

	$style_header3 = array(
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb'=>'CCCCCC'),
			)
		);
	// ======== END OF GENERAL SETTING ==========//
	
	// ============ SPECIFIC SETTING =============//
	
	$sheet->getStyle('B5:J5')->getAlignment()->setWrapText(true);
	$sheet->getStyle('B5:J5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$sheet->getStyle('B5:J5')->applyFromArray( $style_header );
	$sheet->getRowDimension(5)->setRowHeight(33);
	
	// ======== END OF SPECIFIC SETTING ==========//
	
	// ============ CODING ==============//
	$sql="SELECT * FROM dataalumni";
	$query=@mysql_query($sql);
	$j=1;
	while($row_alumni=@mysql_fetch_array($query)){
		$str_diklat=explode(";",$row_alumni['diklat']);
		$str_angkatan=explode(";",$row_alumni['angkatan']);
		$str_tahun=explode(";",$row_alumni['tahun']);
		$str_instansi=explode(";",$row_alumni['instansi']);
		$str_wilayah=explode(";",$row_alumni['wilayah']);
		$str_jabatan=explode(";",$row_alumni['jabatan']);
		$str_bagian=explode(";",$row_alumni['bagian']);
		
		$i=1;
		while($str_diklat[$i]!=""){
			if($str_diklat[$i]==$diklat && $str_angkatan[$i]==$angkatan && $str_tahun[$i]==$tahun){	
				$row_dept=@mysql_fetch_array(@mysql_query("SELECT * FROM dept WHERE id='".$str_instansi[$i]."'"));
				$dept=$row_dept['dept'];
						
				// Data Diklat
				$row_diklat=@mysql_fetch_array(@mysql_query("SELECT * FROM activity WHERE id='".$str_diklat[$i]."'"));
				$diklatname=$row_diklat['activity'];

				$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
						
				// Data Jabatan
				$row_jabatan=@mysql_fetch_array(@mysql_query("SELECT * FROM position WHERE id='".$str_jabatan[$i]."'"));
				$jabatan=$row_jabatan['position'];
						
				if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
					$jabatan=$jabatan." ".$str_bagian[$i];
				}
				
				$sheet->setCellValue('B'.($j+5), $j);
				
				$sheet->getStyle('B'.($j+5))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		
				$sheet->setCellValue('C'.($j+5), $row_alumni['nama']);
				$sheet->getStyle('C'.($j+5))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				
				$sheet->setCellValue('D'.($j+5), $row_alumni['nip']);
				$sheet->getStyle('D'.($j+5))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		
				$sheet->setCellValue('E'.($j+5), str_replace(";",", ",$row_alumni['ttl']));
				$sheet->getStyle('E'.($j+5))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		
				if($row_alumni['rumah']==""){ 
					$alamat="";
				} else {
					$row_000=@mysql_fetch_array(@mysql_query("SELECT * FROM city WHERE id='".$row_alumni['rumah_kota']."'"));
					$alamat=$row_alumni['rumah'].", ".$row_000['city']." Provinsi ".$row_000['provinsi']."\r\n"."No Telp Rumah: ".$row_alumni['rumah_telp']
								."\r\n"."HP: ".$row_alumni['hp_telp']."\r\n"."email: ".$row_alumni['email']."\r\n"."Facebook: ".$row_alumni['facebook'];
				}
				$sheet->setCellValue('F'.($j+5), $alamat);
				$sheet->getStyle('F'.($j+5))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		
				$strinstansi=explode(";",$row_alumni['instansi']);
				$row2=@mysql_fetch_array(@mysql_query("SELECT * FROM dept WHERE id='".$strinstansi[1]."'"));
				$instansi=$row2['dept']." ".str_replace(";"," ",$row_alumni['wilayah']);
		
				$sheet->setCellValue('G'.($j+5),$instansi);
				$sheet->getStyle('G'.($j+5))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		
				$strjabatan=explode(";",$row_alumni['jabatan']);
				$strbagian=explode(";",$row_alumni['bagian']);
				$row2=@mysql_fetch_array(@mysql_query("SELECT * FROM position WHERE id='".$strjabatan[1]."'"));
		
				$sheet->setCellValue('H'.($j+5),$row2['position']." / ".$strbagian[1]);
				$sheet->getStyle('H'.($j+5))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		
				$strpangkat=explode(";",$row_alumni['pangkat']);
				$row2=@mysql_fetch_array(@mysql_query("SELECT * FROM grade WHERE id='".$strpangkat[1]."'"));
		
				$sheet->setCellValue('I'.($j+5),$row2['grade']);
				$sheet->getStyle('I'.($j+5))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		
				$stralamatins=explode(";",$row_alumni['kantor_alamat']);
				$strkotains=explode(";",$row_alumni['kantor_kota']);
				$strtelpins=explode(";",$row_alumni['kantor_telp']);
				$strfaxins=explode(";",$row_alumni['fax']);
				$strwebins=explode(";",$row_alumni['website']);
				$row2=@mysql_fetch_array(@mysql_query("SELECT * FROM city WHERE id='".$strkotains[1]."'"));
						
				$alamatinstansi=$stralamatins[1]."\r\n".$row2['tipe']." ".$row2['city']." Provinsi ".$row2['provinsi']."\r\nTelp: ".$strtelpins[1]."\r\nFax: ".$strfaxins[1]."\r\nWebsite: ".$strwebins[1];
		
				$sheet->setCellValue('J'.($j+5),$alamatinstansi);
				$sheet->getStyle('J'.($j+5))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			
				$sheet->getStyle('B'.($j+5).':J'.($j+5))->applyFromArray( $style_header2 );
				$sheet->getStyle('B'.($j+5).':J'.($j+5))->getAlignment()->setWrapText(true);

				$j++;
			}
			$i++;
		}
		
		/* 
		 */
		

	}
	
	// ======== END OF CODING ===========//
	
	$rowdiklat=@mysql_fetch_array(@mysql_query("SELECT * FROM activity WHERE id='".$diklat."'"));
	
	$sheet->setCellValue('B2', 'Rekapitulasi Alumni Diklat BKPM');
	$sheet->setCellValue('B3', 'Diklat '.$rowdiklat['activity']." Angkatan ".$angkatan." Tahun ".$tahun);
	
	$sheet->setCellValue('B5', 'NO');
	$sheet->getColumnDimension('B')->setWidth(6.14);

	$sheet->setCellValue('C5', 'NAMA');
	$sheet->getColumnDimension('C')->setWidth(40);

	$sheet->setCellValue('D5', 'NIP');
	$sheet->getColumnDimension('D')->setWidth(22);

	$sheet->setCellValue('E5', 'TEMPAT TANGGAL LAHIR');
	$sheet->getColumnDimension('E')->setWidth(25);

	$sheet->setCellValue('F5', 'ALAMAT RUMAH');
	$sheet->getColumnDimension('F')->setWidth(41.14);

	$sheet->setCellValue('G5', 'INSTANSI');
	$sheet->getColumnDimension('G')->setWidth(45);

	$sheet->setCellValue('H5', 'JABATAN');
	$sheet->getColumnDimension('H')->setWidth(28);

	$sheet->setCellValue('I5', 'PANGKAT/GOLONGAN');
	$sheet->getColumnDimension('I')->setWidth(22);

	$sheet->setCellValue('J5', 'ALAMAT INSTANSI');
	$sheet->getColumnDimension('J')->setWidth(35);
	
	$objPHPExcel->setActiveSheetIndex(0);


	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="List Alumni Diklat '.$rowdiklat['activity'].' Angkatan '.$angkatan.' Tahun '.$tahun.'"');
	header('Cache-Control: max-age=0');

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save('php://output');
	
?>
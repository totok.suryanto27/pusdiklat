function validate(){
	// Untuk Konten
	var page=document.getElementById("page").value;
	var msg="<div id=\"tips\"><ul>";
	var error="";
	if(page=="content"){
		if (tinyMCE){ tinyMCE.triggerSave(); }
		// Cek Judul
		var title=document.getElementById("titlepost").value;
		var validatetitlepost=title.replace(/ /g,"");
		if(validatetitlepost.length==0){
			msg=msg+"<li>Judul konten tidak boleh kosong!</li>";
			document.getElementById("titlepost").value="";
			error="ada";
		}
		
		// Cek Konten
		var contentpost=document.getElementById("contentpost").value;
		var validatecontentpost=contentpost.replace(/ /g,"");
		if(validatecontentpost.length==0){
			msg=msg+"<li>Isi konten tidak boleh kosong!</li>";
			error="ada";
		}
		
		var duplicate=document.getElementById("duplikasi").value;
		if(duplicate!=""){
			msg=msg+"<li>Judul konten yang Anda masukkan sudah ada!</li>";
			error="ada";
		}
	}
	
	if(page=="category"){
		// Cek Judul
		var title=document.getElementById("titlecategory").value;
		var validatetitle=title.replace(/ /g,"");
		if(validatetitle.length==0){
			msg=msg+"<li>Nama kategori tidak boleh kosong!</li>";
			document.getElementById("titlecategory").value="";
			error="ada";
		}
		
		var duplicate=document.getElementById("duplikasi").value;
		if(duplicate!=""){
			msg=msg+"<li>Nama kategori yang Anda masukkan sudah ada!</li>";
			error="ada";
		}
	}
	
	if(page=="extlink"){
		// Cek Judul
		var title=document.getElementById("titleext").value;
		var validatetitle=title.replace(/ /g,"");
		if(validatetitle.length==0){
			msg=msg+"<li>Nama Tautan tidak boleh kosong!</li>";
			document.getElementById("titleext").value="";
			error="ada";
		}
		
		var duplicate=document.getElementById("duplikasi").value;
		if(duplicate!=""){
			msg=msg+"<li>Nama Tautan yang Anda masukkan sudah ada!</li>";
			error="ada";
		}
	}
	
	if(page=="albumph"){
		// Cek Judul
		var title=document.getElementById("titlealbum").value;
		var validatetitle=title.replace(/ /g,"");
		if(validatetitle.length==0){
			msg=msg+"<li>Nama Album tidak boleh kosong!</li>";
			document.getElementById("titlealbum").value="";
			error="ada";
		}
		
		var duplicate=document.getElementById("duplikasi").value;
		if(duplicate!=""){
			msg=msg+"<li>Nama Album yang Anda masukkan sudah ada!</li>";
			error="ada";
		}
	}
	
	if(page=="addphoto"){
		// Cek Album sudah diselect atau belum
		var title=document.getElementById("albumphoto").value;
		var validatetitle=title.replace(/ /g,"");
		if(validatetitle.length==0){
			msg=msg+"<li>Silahkan pilih Album Foto terlebih dahulu!</li>";
			error="ada";
		}
		
		var fileimage=document.getElementById("imagefile").value;
		if(fileimage==""){
			msg=msg+"<li>Silahkan pilih file yang akan diupload!</li>";
			error="ada";
		}
	}
	
	if(page=="pages"){
		if (tinyMCE){ tinyMCE.triggerSave(); }
		// Cek Judul
		var title=document.getElementById("titlepage").value;
		var validatetitlepost=title.replace(/ /g,"");
		if(validatetitlepost.length==0){
			msg=msg+"<li>Judul halaman tidak boleh kosong!</li>";
			document.getElementById("titlepage").value="";
			error="ada";
		}
		
		// Cek Konten
		var contentpost=document.getElementById("contentpage").value;
		var validatecontentpost=contentpost.replace(/ /g,"");
		if(validatecontentpost.length==0){
			msg=msg+"<li>Isi halaman tidak boleh kosong!</li>";
			error="ada";
		}
		
		var duplicate=document.getElementById("duplikasi").value;
		if(duplicate!=""){
			msg=msg+"<li>Judul halaman yang Anda masukkan sudah ada!</li>";
			error="ada";
		}
	}
	
	if(page=="menu-level1"){
		//if (tinyMCE){ tinyMCE.triggerSave(); }
		// Cek Judul
		var title=document.getElementById("titlemenulvl1").value;
		var validatetitlepost=title.replace(/ /g,"");
		if(validatetitlepost.length==0){
			msg=msg+"<li>Nama Menu Level 1 tidak boleh kosong!</li>";
			document.getElementById("titlemenulvl1").value="";
			document.getElementById("titlemenulvl1").focus();
			error="ada";
		}
		
		var submenu=document.getElementById("haveSub").checked;
		var status="";
		if(submenu==false){
			var radios = document.post.opt;
			for (var i=0, iLen=radios.length; i<iLen; i++) {
				if(radios[i].checked == true){
					status="ada";
				}
			}
			if(status==""){
				msg=msg+"<li>Silahkan pilih tautan yang akan dilinkkan dengan menu!</li>";
				error="ada";
			}
		} 
		var duplicate=document.getElementById("duplikasi").value;
		if(duplicate!=""){
			msg=msg+"<li>Nama Menu yang Anda masukkan sudah ada!</li>";
			error="ada";
		}
	}
	
	if(page=="menu-level2"){
		//if (tinyMCE){ tinyMCE.triggerSave(); }
		// Cek Judul
		var title=document.getElementById("titlemenulvl2").value;
		var validatetitlepost=title.replace(/ /g,"");
		if(validatetitlepost.length==0){
			msg=msg+"<li>Nama Menu Level 2 tidak boleh kosong!</li>";
			document.getElementById("titlemenulvl2").value="";
			document.getElementById("titlemenulvl2").focus();
			error="ada";
		}
		var duplicate=document.getElementById("duplikasi").value;
		if(duplicate!=""){
			msg=msg+"<li>Nama Menu yang Anda masukkan sudah ada!</li>";
			error="ada";
		} 
		/* var submenu=document.getElementById("haveSub").checked;
		var status="";
		if(submenu==false){
			var radios = document.post.opt;
			for (var i=0, iLen=radios.length; i<iLen; i++) {
				if(radios[i].checked == true){
					status="ada";
				}
			}
			if(status==""){
				msg=msg+"<li>Silahkan pilih tautan yang akan dilinkkan dengan menu!</li>";
				error="ada";
			}
		} 
		*/
	}
	
	if(error!=""){
		document.getElementById("warning").innerHTML=msg;
		return false;
	}
}

function checktitle(path,type,str,id){
	// Cek Duplikasi Judul
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("duplicate").innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET",path+"asset/warning.php?type="+type+"&title="+str+"&id="+id,true);
	xmlhttp.send();
}


function showlevel2(nilai){
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("menulevel2").innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET","../asset/showlevel2.php?nilai="+nilai,true);
	xmlhttp.send();
}

function disabledlink(level){
	if(level=="level1"){
		var radios = document.post.opt;
		if(document.getElementById('haveSub').checked==true){
			for (var i=0, iLen=radios.length; i<iLen; i++) {
			  radios[i].disabled = true;
			  radios[i].checked = false;
			} 
			document.getElementById('optinpages').value = "";
			document.getElementById('optincat').value = "";
			document.getElementById('otherlink').value = "";
		} else {
			for (var i=0, iLen=radios.length; i<iLen; i++) {
			  radios[i].disabled = false;
			} 
		}
	}
}

function activateinput(nilai){
	if(nilai=="pages"){
		document.getElementById('optinpages').disabled = false;
		document.getElementById('optincat').disabled = true;
		document.getElementById('optinreg').disabled = true;
		document.getElementById('otherlink').disabled = true;
		document.getElementById('optincat').value = "";
		document.getElementById('optinreg').value = "";
		document.getElementById('otherlink').value = "";
	}
	
	if(nilai=="category"){
		document.getElementById('optincat').disabled = false;
		document.getElementById('optinpages').disabled = true;
		document.getElementById('optinreg').disabled = true;
		document.getElementById('otherlink').disabled = true;
		document.getElementById('optinpages').value = "";
		document.getElementById('optinreg').value = "";
		document.getElementById('otherlink').value = "";
	}
	
	if(nilai=="registration"){
		document.getElementById('optinreg').disabled = false;
		document.getElementById('optinpages').disabled = true;
		document.getElementById('optincat').disabled = true;
		document.getElementById('otherlink').disabled = true;
		document.getElementById('optincat').value = "";
		document.getElementById('optinpages').value = "";
		document.getElementById('otherlink').value = "";
	}
	
	if(nilai=="other"){
		document.getElementById('optinpages').disabled = true;
		document.getElementById('optincat').disabled = true;
		document.getElementById('optinreg').disabled = true;
		document.getElementById('otherlink').disabled = false;
		document.getElementById('optinpages').value = "";
		document.getElementById('optinreg').value = "";
		document.getElementById('optincat').value = "";
	}
	if(nilai=="photo" || nilai=="video" || nilai=="consultation"){
		document.getElementById('optinpages').disabled = true;
		document.getElementById('optincat').disabled = true;
		document.getElementById('optinreg').disabled = true;
		document.getElementById('otherlink').disabled = true;
		document.getElementById('optinpages').value = "";
		document.getElementById('optincat').value = "";
		document.getElementById('optinreg').value = "";
		document.getElementById('otherlink').value = "";
	}
}

function showdatacandidate(nilai, txt){
	/*if(document.getElementById("filter-diklat").value==""){
		alert("Mohon tentukan nama diklat yang akan ditampilkan");
		return false;
	}
	
	if(document.getElementById("filter-angk").value==""){
		alert("Mohon tentukan angkatan diklat yang akan ditampilkan");
		return false;
	}*/
	
	var iddik=document.getElementById("filter-diklat").value;
	var idakt=document.getElementById("filter-angk").value;
	var tahun=document.getElementById("filter-tahun").value;
	
	// Proses Tampilan Data
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("showdata").innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET","../../asset/showdatacandidate.php?page="+nilai+"&iddik="+iddik+"&idakt="+idakt+"&txt="+txt+"&wil="+document.getElementById("searchcity").value+"&kota="+document.getElementById("kota").value+"&tahun="+tahun,true);
	xmlhttp.send();
}

function showdatacandidate2(nilai, txt){
	/*if(document.getElementById("filter-diklat").value==""){
		alert("Mohon tentukan nama diklat yang akan ditampilkan");
		return false;
	}
	
	if(document.getElementById("filter-angk").value==""){
		alert("Mohon tentukan angkatan diklat yang akan ditampilkan");
		return false;
	}*/
	
	var iddik=document.getElementById("filter-diklat").value;
	var idakt=document.getElementById("filter-angk").value;
	
	// Proses Tampilan Data
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("showdata").innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET","../../asset/showdatacandidate.php?page="+nilai+"&iddik="+iddik+"&idakt="+idakt+"&txt="+txt+"&wil="+document.getElementById("searchcity").value+"&kota="+document.getElementById("kota").value,true);
	xmlhttp.send();
}

function showalbum(nilai){
	// Proses Tampilan Data
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("showdata").innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET","../../asset/showalbum.php?idalbum="+nilai,true);
	xmlhttp.send();
}

function showquestforum(nilai,text){
	// Proses Tampilan Data
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("showdata").innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET","http://pusdiklat.bkpm.go.id/asset/showquestforum.php?page="+nilai+"&text="+text,true);
	xmlhttp.send();
}


function answerquest(pos,questid){
	// Proses Tampilan Data
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			for(var i=1;i<=document.getElementById("nofquest").value;i++){
				if(i==pos){
					document.getElementById("answerquest"+i).innerHTML=xmlhttp.responseText;
				} else {
					document.getElementById("answerquest"+i).innerHTML='';
				}
			}
			
		}
	}
	xmlhttp.open("GET","http://pusdiklat.bkpm.go.id/asset/answerquest.php?questid="+questid+"&pos="+pos,true);
	xmlhttp.send();
}

function saveanswer(pos,questid){
	// Proses Tampilan Data
	var xmlhttp;
	var message=document.getElementById("answer"+pos).value;
	if(message==""){
		alert('Jawaban tidak boleh kosong');
		return false;
	}
	//alert(message);
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	xmlhttp.open("GET","http://pusdiklat.bkpm.go.id/asset/saveanswer.php?questid="+questid+"&pos="+pos+"&msg="+message,true);
	xmlhttp.send();
}

function ajaxfunc(parameter, target, file){
	var xmlhttp;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById(target).innerHTML=xmlhttp.responseText;
			return false;
		}
	}
	xmlhttp.open("GET", file+"?param="+parameter,true);
	xmlhttp.send();
}
<?php
	session_start();
	error_reporting(E_ALL ^ E_NOTICE);
	require('connection.php');
	
	$sqltahun="";
	if($_GET['tahun']!=""){
		$sqltahun=" AND tahun='".$_GET['tahun']."'";
	}
		
	require_once 'Classes/PHPExcel.php';
	$objPHPExcel = new PHPExcel();
	$sheet = $objPHPExcel->getActiveSheet();
	$objPHPExcel->getActiveSheet()->setTitle('Report');
	
	// ============ GENERAL SETTING =============//
	$default_border = array(
		'style' => PHPExcel_Style_Border::BORDER_THIN,
		'color' => array('rgb'=>'cccccc')
		);

	$style_header2 = array(
			'borders' => array(
				'allborders' => $default_border,
			)
		);

	$style_header = array(
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb'=>'b9c9fe'),
			),
			'font' => array(
				'bold' => true,
			),
			'borders' => array(
				'allborders' => $default_border,
			)
		);

	$style_header3 = array(
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb'=>'CCCCCC'),
			)
		);
	// ======== END OF GENERAL SETTING ==========//
	
	// ============ SPECIFIC SETTING =============//
	
	$sheet->getStyle('B5:M5')->getAlignment()->setWrapText(true);
	$sheet->getStyle('B5:M5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$sheet->getStyle('B5:M5')->applyFromArray( $style_header );
	$sheet->getRowDimension(5)->setRowHeight(33);
	
	// ======== END OF SPECIFIC SETTING ==========//
	
	// ============ CODING ==============//
	if($_GET['iddik']=="" && $_GET['idakt']==""){
		$resultsreport=" Seluruh Diklat & Seluruh Angkatan";
		if($sqltahun==""){
			$sql="SELECT * FROM datapeserta ORDER BY `diklat` ASC, AngkatanNumber ASC";
		} else {
			$sql="SELECT * FROM datapeserta WHERE ".$sqltahun." ORDER BY `diklat` ASC, AngkatanNumber ASC";
		}
		
	}
	
	if($_GET['iddik']!="" && $_GET['idakt']==""){
		$diklatname=@mysql_fetch_array(@mysql_query("SELECT * FROM activity WHERE id='".$_GET['iddik']."'".$sqltahun));
		
		$resultsreport=$diklatname['activity']." & Seluruh Angkatan";
		$sql="SELECT * FROM datapeserta WHERE diklat LIKE '%;".$_GET['iddik'].";%'".$sqltahun." ORDER BY AngkatanNumber ASC";
	}
	
	if($_GET['iddik']=="" && $_GET['idakt']!=""){
		$resultsreport=" Seluruh Diklat & Angkatan ".$_GET['idakt'];
		$sql="SELECT * FROM datapeserta WHERE angkatan LIKE '%;".$_GET['idakt'].";%'".$sqltahun." ORDER BY `diklat` ASC";
	}
	
	if($_GET['iddik']!="" && $_GET['idakt']!=""){
		$diklatname=@mysql_fetch_array(@mysql_query("SELECT * FROM activity WHERE id='".$_GET['iddik']."'".$sqltahun));
		
		$resultsreport=$diklatname['activity']." & Angkatan ".$_GET['idakt'];
		$sql="SELECT * FROM datapeserta WHERE diklat LIKE '%;".$_GET['iddik'].";%' AND angkatan LIKE '%;".$_GET['idakt'].";%'".$sqltahun;
	}
	
	$query=@mysql_query($sql);
	$i=1;
	while($row=@mysql_fetch_array($query)){
		$sheet->setCellValue('B'.($i+5), $i);
		$sheet->getStyle('B'.($i+5))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		
		$sheet->setCellValue('C'.($i+5), $row['nama']);
		$sheet->getStyle('C'.($i+5))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				
		$sheet->setCellValue('D'.($i+5), $row['nip']);
		$sheet->getStyle('D'.($i+5))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		
		$sheet->setCellValue('E'.($i+5), str_replace(";",", ",$row['ttl']));
		$sheet->getStyle('E'.($i+5))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		
		if($row['rumah']==""){ 
			$alamat="";
		} else {
			$row_000=@mysql_fetch_array(@mysql_query("SELECT * FROM city WHERE id='".$row['rumah_kota']."'"));
			$alamat=$row['rumah'].", ".$row_000['city']." Provinsi ".$row_000['provinsi']."\r\n"."No Telp Rumah: ".$row['rumah_telp']
						."\r\n"."HP: ".$row['hp_telp']."\r\n"."email: ".$row['email']."\r\n"."Facebook: ".$row['facebook'];
		}
		$sheet->setCellValue('F'.($i+5), $alamat);
		$sheet->getStyle('F'.($i+5))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		
		$strinstansi=explode(";",$row['instansi']);
		$row2=@mysql_fetch_array(@mysql_query("SELECT * FROM dept WHERE id='".$strinstansi[1]."'"));
		$instansi=$row2['dept']." ".str_replace(";"," ",$row['wilayah']);
		
		$sheet->setCellValue('G'.($i+5),$instansi);
		$sheet->getStyle('G'.($i+5))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		
		$strjabatan=explode(";",$row['jabatan']);
		$strbagian=explode(";",$row['bagian']);
		$row2=@mysql_fetch_array(@mysql_query("SELECT * FROM position WHERE id='".$strjabatan[1]."'"));
		
		$sheet->setCellValue('H'.($i+5),$row2['position']." / ".$strbagian[1]);
		$sheet->getStyle('H'.($i+5))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		
		$strpangkat=explode(";",$row['pangkat']);
		$row2=@mysql_fetch_array(@mysql_query("SELECT * FROM grade WHERE id='".$strpangkat[1]."'"));
		
		$sheet->setCellValue('I'.($i+5),$row2['grade']);
		$sheet->getStyle('I'.($i+5))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		
		$stralamatins=explode(";",$row['kantor_alamat']);
		$strkotains=explode(";",$row['kantor_kota']);
		$strtelpins=explode(";",$row['kantor_telp']);
		$strfaxins=explode(";",$row['fax']);
		$strwebins=explode(";",$row['website']);
		$row2=@mysql_fetch_array(@mysql_query("SELECT * FROM city WHERE id='".$strkotains[1]."'"));
						
		$alamatinstansi=$stralamatins[1]."\r\n".$row2['tipe']." ".$row2['city']." Provinsi ".$row2['provinsi']."\r\nTelp: ".$strtelpins[1]."\r\nFax: ".$strfaxins[1]."\r\nWebsite: ".$strwebins[1];
		
		$sheet->setCellValue('J'.($i+5),$alamatinstansi);
		$sheet->getStyle('J'.($i+5))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		
		$strdiklat=explode(";",$row['diklat']);
		$strangk=explode(";",$row['angkatan']);
		$diklatname=@mysql_fetch_array(@mysql_query("SELECT * FROM activity WHERE id='".$strdiklat[1]."'"));
		$diklatnakt=$diklatname['activity']." Angkatan ".$strangk[1];
		
		$sheet->setCellValue('K'.($i+5),$diklatnakt);
		$sheet->getStyle('K'.($i+5))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		
		$stratasan=explode(";",$row['AtasanInfo']);
		
		$sheet->setCellValue('L'.($i+5),$stratasan[0]."\r\n(".$stratasan[1].")");
		$sheet->getStyle('L'.($i+5))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		
		if($row['confirmation']==1){ $confirm="Sudah Konfirmasi"; } else { $confirm="Belum Konfirmasi"; }
		
		$sheet->setCellValue('M'.($i+5),$confirm);
		$sheet->getStyle('M'.($i+5))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				
		$sheet->getStyle('B'.($i+5).':M'.($i+5))->applyFromArray( $style_header2 );
		$sheet->getStyle('B'.($i+5).':M'.($i+5))->getAlignment()->setWrapText(true);
		
		$i++;
	}
	
	// ======== END OF CODING ===========//
	
	$sheet->setCellValue('B2', 'REKAPITULASI CALON PESERTA DIKLAT BKPM');
	$sheet->setCellValue('B3', 'KRITERIA LAPORAN: '.$resultsreport);
	
	$sheet->setCellValue('B5', 'NO');
	$sheet->getColumnDimension('B')->setWidth(6.14);

	$sheet->setCellValue('C5', 'NAMA');
	$sheet->getColumnDimension('C')->setWidth(40);

	$sheet->setCellValue('D5', 'NIP');
	$sheet->getColumnDimension('D')->setWidth(22);

	$sheet->setCellValue('E5', 'TEMPAT TANGGAL LAHIR');
	$sheet->getColumnDimension('E')->setWidth(25);

	$sheet->setCellValue('F5', 'ALAMAT RUMAH');
	$sheet->getColumnDimension('F')->setWidth(41.14);

	$sheet->setCellValue('G5', 'INSTANSI');
	$sheet->getColumnDimension('G')->setWidth(45);

	$sheet->setCellValue('H5', 'JABATAN');
	$sheet->getColumnDimension('H')->setWidth(28);

	$sheet->setCellValue('I5', 'PANGKAT/GOLONGAN');
	$sheet->getColumnDimension('I')->setWidth(22);

	$sheet->setCellValue('J5', 'ALAMAT INSTANSI');
	$sheet->getColumnDimension('J')->setWidth(35);
	
	$sheet->setCellValue('K5', 'DIKLAT');
	$sheet->getColumnDimension('K')->setWidth(50);
	
	$sheet->setCellValue('L5', 'Nama Atasan & No Telp/Hp');
	$sheet->getColumnDimension('L')->setWidth(50);
	
	$sheet->setCellValue('M5', 'STATUS');
	$sheet->getColumnDimension('M')->setWidth(22);
	
	$objPHPExcel->setActiveSheetIndex(0);


	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="calon_peserta.xls"');
	header('Cache-Control: max-age=0');

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save('php://output');
	
?>
<?php include('connection.php'); ?>

<script>
							$(function(){
								$("div.holder").jPages({
								  containerID : "movies",
								  previous : "Sebelumnya",
								  next : "Berikutnya",
								  perPage : 10,
								  delay : 0
								});
							  });
							  </script>

							  <style type="text/css">
							  table{ width: 100%; margin-top: 30px; }
							  td, th{ text-align: left; height:25px; }
							  th { background: #f5f5f5; }
							  th:nth-child(1){ width:10%; }
							  th:nth-child(2){ width:10%; }
							  th:nth-child(3){ width:60%; }
							  th:nth-child(4){ width:20%;}

							  form { float: right; margin-right: 10px; }
							  form label { margin-right: 5px; }
							  form select { margin-right: 75px; }
							  </style>

<div class="holder"></div>
		
		<div class="jtable-main-container">
			<div class="jtable-busy-panel-background" style="display: none; width: 940px; height: 123px;"></div>
			<div class="jtable-busy-message" style="display: none;"></div>
			<div class="jtable-title">
				<div class="jtable-title-text">
					Daftar Gambar
				</div>
			</div>
			<table class="jtable" style="margin-top:0">
				<thead>
					<tr>
						<th class="jtable-column-header" style="width: 47%;">
							<div class="jtable-column-header-container" align="center">Gambar Preview</div>
						</th>
						<th class="jtable-column-header" style="width: 47%;">
							<div class="jtable-column-header-container" align="center">Deskripsi</div>
						</th>
						<th class="jtable-column-header" style="width: 6%;">
							<div class="jtable-column-header-container" align="center">Aksi</div>
						</th>
					</tr>
				</thead>
				
				<tbody id="movies">
					<?php
					$sql="SELECT * FROM photos WHERE AlbPhID='".$_GET['idalbum']."'";
					$query=@mysql_query($sql);
					$ndata=@mysql_num_rows($query);
					
					if($ndata==0){
						echo "<tr><td colspan=\"3\" align=\"center\">Tidak ada data</td></tr>";
					} else {
						$i=1;
						$j=1;
						$sql="SELECT * FROM photos WHERE AlbPhID='".$_GET['idalbum']."'";
						$query=@mysql_query($sql);
						while($row=@mysql_fetch_array($query)){
							if($i==2*$j-1){ 
								$even=" jtable-row-even";
								$j++;
							} else { 
								$even=""; 
							}
							echo 
								"<tr class=\"jtable-data-row jtable-row-even\">
									<td><img src=\"../../uploads/gallery/photo/".$row['ImageThumb']."\" alt=\"Preview\" /></td>
									<td>".$row['ImageDesc']."</td>
									<td><div align=\"center\"><img src=\"../../asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Gambar\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus ".$row['ImageName']." dari database?')){document.location='../content/delete-ext/?post=".$row['PhotoID']."'; }\" /></div></td>
								</tr>";
							$i++;
						}
					}
					
					?>
				</tbody>
			</table>
		</div>
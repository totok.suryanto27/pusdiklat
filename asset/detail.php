<!doctype html>
<?php
	include('connection.php');
	$row=@mysql_fetch_array(@mysql_query("SELECT * FROM datapeserta WHERE id='".$_GET['id']."'"));
?>
	<head>
		<title>Detail Calon Peserta :: <?php echo $row['nama']; ?></title>
		<style>
			html,body { margin:0; padding:0; width: 750px; background-attachment:fixed; background-position:center top; font: 12px/18px "Lucida Grande",Geneva,Arial,Verdana,sans-serif; color: rgb(107, 102, 102); }
			#layout { margin: 0 auto 0 auto; width: 650px; font-size: 16px; }
		</style>
	</head>
	<body>
		<div id="layout">
			<div align="center" style="margin-bottom: 50px;"><h1><?php echo $row['nama']; ?></h1></div>
			<div class="data">
				<table border="0" width="100%">
					<tr>
						<td width="30%">Nama Lengkap & Gelar</td>
						<td width="5%">:</td>
						<td width="65%"><?php echo $row['nama']; ?></td>
					</tr>
				
					<tr>
						<td width="30%">NIP</td>
						<td width="5%">:</td>
						<td width="65%"><?php echo $row['nip']; ?></td>
					</tr>
					<tr>
						<td width="30%">Tempat, Tanggal Lahir</td>
						<td width="5%">:</td>
						<td width="65%"><?php echo str_replace(";",", ",$row['ttl']); ?></td>
					</tr>
					
					<?php
						$strinstansi=explode(";",$row['instansi']);
						$row2=@mysql_fetch_array(@mysql_query("SELECT * FROM dept WHERE id='".$strinstansi[1]."'"));
						$instansi=$row2['dept']." ".str_replace(";"," ",$row['wilayah']);
					?>
					
					<tr>
						<td width="30%">Instansi</td>
						<td width="5%">:</td>
						<td width="65%"><?php echo $instansi; ?></td>
					</tr>
					
					<?php
						$stralamatins=explode(";",$row['kantor_alamat']);
						$strkotains=explode(";",$row['kantor_kota']);
						$strtelpins=explode(";",$row['kantor_telp']);
						$strfaxins=explode(";",$row['fax']);
						$strwebins=explode(";",$row['website']);
						$row2=@mysql_fetch_array(@mysql_query("SELECT * FROM city WHERE id='".$strkotains[1]."'"));
						
						$alamatinstansi=$stralamatins[1]."<br />".$row2['tipe']." ".$row2['city']." Provinsi ".$row2['provinsi']."<br />Telp: ".$strtelpins[1]."<br />Fax: ".$strfaxins[1]."<br />Website: ".$strwebins[1];
					?>
					
					<tr>
						<td width="30%" valign="top">Alamat Instansi</td>
						<td width="5%" valign="top">:</td>
						<td width="65%" valign="top"><?php echo $alamatinstansi; ?></td>
					</tr>
					
					<?php
						$strjabatan=explode(";",$row['jabatan']);
						$strbagian=explode(";",$row['bagian']);
						$row2=@mysql_fetch_array(@mysql_query("SELECT * FROM position WHERE id='".$strjabatan[1]."'"));
					?>
					
					<tr>
						<td width="30%" valign="top">Jabatan / Bagian</td>
						<td width="5%" valign="top">:</td>
						<td width="65%" valign="top"><?php echo $row2['position']." / ".$strbagian[1]; ?></td>
					</tr>
					
					<?php
						$strpangkat=explode(";",$row['pangkat']);
						$row2=@mysql_fetch_array(@mysql_query("SELECT * FROM grade WHERE id='".$strpangkat[1]."'"));
					?>
					
					<tr>
						<td width="30%" valign="top">Golongan (Pangkat)</td>
						<td width="5%" valign="top">:</td>
						<td width="65%" valign="top"><?php echo $row2['grade']; ?></td>
					</tr>
					
					<?php
						$stratasan=explode(";",$row['AtasanInfo']);
						
					?>
					
					<tr>
						<td width="30%" valign="top">Nama Atasan</td>
						<td width="5%" valign="top">:</td>
						<td width="65%" valign="top"><?php echo $stratasan[0]; ?></td>
					</tr>
					
					<tr>
						<td width="30%" valign="top">No Telp/Hp Atasan</td>
						<td width="5%" valign="top">:</td>
						<td width="65%" valign="top"><?php echo $stratasan[1]; ?></td>
					</tr>

					<?php
						$hpstr=explode(';',$row['hp_telp']);
						$row2=@mysql_fetch_array(@mysql_query("SELECT * FROM city WHERE id='".$row['rumah_kota']."'"));
						$alamat=$row['rumah']."<br />".$row2['tipe']." ".$row2['city']." Provinsi ".$row2['provinsi']."<br />Telp: ".$row['rumah_telp']."<br />Hp: ".$hpstr[0]."<br />e-mail: ".$row['email'];
					?>
					
					<tr>
						<td width="30%" valign="top">Alamat Rumah</td>
						<td width="5%" valign="top">:</td>
						<td width="65%" valign="top"><?php echo $alamat; ?></td>
					</tr>					
					
				</table>
			</div>
		</div>
	</body>
</html>
<?php
	error_reporting(E_ALL ^ E_NOTICE);
	require_once 'Classes/PHPExcel.php';
	include('connection.php');
	$objPHPExcel = new PHPExcel();
	$sheet = $objPHPExcel->getActiveSheet();
	$objPHPExcel->setActiveSheetIndex(0);
	$objPHPExcel->getActiveSheet()->setTitle('Provinsi');
	
	// ============ GENERAL SETTING =============//
	$default_border = array(
		'style' => PHPExcel_Style_Border::BORDER_THIN,
		'color' => array('rgb'=>'cccccc')
		);

	$style_header2 = array(
			'borders' => array(
				'allborders' => $default_border,
			)
		);

	$style_header = array(
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb'=>'b9c9fe'),
			),
			'font' => array(
				'bold' => true,
			),
			'borders' => array(
				'allborders' => $default_border,
			)
		);

	$style_header3 = array(
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb'=>'CCCCCC'),
			)
		);
	// ======== END OF GENERAL SETTING ==========//
	
	// ============ SPECIFIC SETTING =============//
	
	/* $sheet->getStyle('B5:J5')->getAlignment()->setWrapText(true);
	$sheet->getStyle('B5:J5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$sheet->getStyle('B5:J5')->applyFromArray( $style_header );
	$sheet->getRowDimension(5)->setRowHeight(33); */
	
	// ======== END OF SPECIFIC SETTING ==========//
	
	
	# CODING #
	$row=@mysql_fetch_array(@mysql_query("SELECT * FROM activity WHERE id='".$_GET['id']."'"));
	$judul=$row['activity'];
	
	// Dapatkan data dari Provinsi
	$sql="SELECT * FROM city GROUP BY provinsi";
	$query=@mysql_query($sql);
	$no=6;
	while($row=@mysql_fetch_array($query)){
		$sql0="SELECT * FROM dataalumni WHERE wilayah LIKE '%Provinsi;".$row['provinsi'].";%'";
		$query0=@mysql_query($sql0);
		$countalumni=0;
		while($row0=@mysql_fetch_array($query0)){
			$str_diklat=explode(";",$row0['diklat']);
			$str_tahun=explode(";",$row0['tahun']);
			
			$i=1;
			while($str_diklat[$i]!=""){
				if($str_diklat[$i]==$_GET['id'] && $str_tahun[$i]==$_GET['year']){
					$countalumni++;
				}
				$i++;
			}
		}
		if($countalumni!=0){
			$sheet->setCellValue('B'.$no, ($no-5));
			$sheet->setCellValue('C'.$no, $row['provinsi']);
			$sheet->setCellValue('D'.$no, $countalumni);
			$no++;
		}
	}
	# END OF CODING
	
	
	
	$sheet->setCellValue('B2', 'Statistik Alumni Diklat BKPM');
	$sheet->setCellValue('B3', $judul);
	
	$sheet->setCellValue('B5', 'NO');
	$sheet->getColumnDimension('B')->setWidth(6.14);

	$sheet->setCellValue('C5', 'Provinsi');
	$sheet->getColumnDimension('C')->setWidth(40);

	$sheet->setCellValue('D5', $_GET['year']);
	$sheet->getColumnDimension('D')->setWidth(12);
	
	
	
	// Create NewSheet
	$objPHPExcel->createSheet(NULL, 1);
	$objPHPExcel->setActiveSheetIndex(1);
	$objPHPExcel->getActiveSheet()->setTitle('Kota');
	$sheet2 = $objPHPExcel->getActiveSheet();
	
	# CODING #
	$row=@mysql_fetch_array(@mysql_query("SELECT * FROM activity WHERE id='".$_GET['id']."'"));
	$judul=$row['activity'];
	
	// Dapatkan data dari Kota
	$sql="SELECT * FROM city WHERE tipe='Kota' GROUP BY city";
	$query=@mysql_query($sql);
	$no=6;
	while($row=@mysql_fetch_array($query)){
		$sql0="SELECT * FROM dataalumni WHERE wilayah LIKE '%Kota;".$row['city'].";%'";
		$query0=@mysql_query($sql0);
		$countalumni=0;
		while($row0=@mysql_fetch_array($query0)){
			$str_diklat=explode(";",$row0['diklat']);
			$str_tahun=explode(";",$row0['tahun']);
			
			$i=1;
			while($str_diklat[$i]!=""){
				if($str_diklat[$i]==$_GET['id'] && $str_tahun[$i]==$_GET['year']){
					$countalumni++;
				}
				$i++;
			}
		}
		if($countalumni!=0){
			$sheet2->setCellValue('B'.$no, ($no-5));
			$sheet2->setCellValue('C'.$no, $row['city']);
			$sheet2->setCellValue('D'.$no, $countalumni);
			$no++;
		}
	}
	# END OF CODING
	
	
	
	$sheet2->setCellValue('B2', 'Statistik Alumni Diklat BKPM');
	$sheet2->setCellValue('B3', $judul);
	
	$sheet2->setCellValue('B5', 'NO');
	$sheet2->getColumnDimension('B')->setWidth(6.14);

	$sheet2->setCellValue('C5', 'Kota');
	$sheet2->getColumnDimension('C')->setWidth(40);

	$sheet2->setCellValue('D5', $_GET['year']);
	$sheet2->getColumnDimension('D')->setWidth(12);
	
	
	
	// Create NewSheet
	$objPHPExcel->createSheet(NULL, 2);
	$objPHPExcel->setActiveSheetIndex(2);
	$objPHPExcel->getActiveSheet()->setTitle('Kabupaten');
	$sheet3 = $objPHPExcel->getActiveSheet();
	
	# CODING #
	$row=@mysql_fetch_array(@mysql_query("SELECT * FROM activity WHERE id='".$_GET['id']."'"));
	$judul=$row['activity'];
	
	// Dapatkan data dari Kota
	$sql="SELECT * FROM city WHERE tipe='Kabupaten' GROUP BY city";
	$query=@mysql_query($sql);
	$no=6;
	while($row=@mysql_fetch_array($query)){
		$sql0="SELECT * FROM dataalumni WHERE wilayah LIKE '%Kabupaten;".$row['city'].";%'";
		$query0=@mysql_query($sql0);
		$countalumni=0;
		while($row0=@mysql_fetch_array($query0)){
			$str_diklat=explode(";",$row0['diklat']);
			$str_tahun=explode(";",$row0['tahun']);
			
			$i=1;
			while($str_diklat[$i]!=""){
				if($str_diklat[$i]==$_GET['id'] && $str_tahun[$i]==$_GET['year']){
					$countalumni++;
				}
				$i++;
			}
		}
		if($countalumni!=0){
			$sheet3->setCellValue('B'.$no, ($no-5));
			$sheet3->setCellValue('C'.$no, $row['city']);
			$sheet3->setCellValue('D'.$no, $countalumni);
			$no++;
		}
	}
	# END OF CODING
	
	
	
	$sheet3->setCellValue('B2', 'Statistik Alumni Diklat BKPM');
	$sheet3->setCellValue('B3', $judul);
	
	$sheet3->setCellValue('B5', 'NO');
	$sheet3->getColumnDimension('B')->setWidth(6.14);

	$sheet3->setCellValue('C5', 'Kabupaten');
	$sheet3->getColumnDimension('C')->setWidth(40);

	$sheet3->setCellValue('D5', $_GET['year']);
	$sheet3->getColumnDimension('D')->setWidth(12);
	
	
	// Create NewSheet
	$objPHPExcel->createSheet(NULL, 3);
	$objPHPExcel->setActiveSheetIndex(3);
	$objPHPExcel->getActiveSheet()->setTitle('Instansi (Prov)');
	$sheet3 = $objPHPExcel->getActiveSheet();
	
	# CODING #
	$row=@mysql_fetch_array(@mysql_query("SELECT * FROM activity WHERE id='".$_GET['id']."'"));
	$judul=$row['activity'];
	
	// Dapatkan data dari Kota
	$sql="SELECT * FROM dept";
	$query=@mysql_query($sql);
	$no=6;
	while($row=@mysql_fetch_array($query)){
		$sql0="SELECT * FROM city GROUP BY provinsi";
		$query0=@mysql_query($sql0);
		while($row0=@mysql_fetch_array($query0)){	
			$sql1="SELECT * FROM dataalumni WHERE instansi LIKE '%;".$row['id'].";%' AND wilayah LIKE '%Provinsi;".$row0['provinsi'].";%'";
			$query1=@mysql_query($sql1);
			$countalumni=0;
			while($row1=@mysql_fetch_array($query1)){
				$str_diklat=explode(";",$row1['diklat']);
				$str_tahun=explode(";",$row1['tahun']);
				
				$i=1;
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$_GET['id'] && $str_tahun[$i]==$_GET['year']){
						$countalumni++;
					}
					$i++;
				}
			}
			if($countalumni!=0){
				$sheet3->setCellValue('B'.$no, ($no-5));
				$sheet3->setCellValue('C'.$no, $row['dept']." Provinsi ".$row0['provinsi']);
				$sheet3->setCellValue('D'.$no, $countalumni);
				$no++;
			}
		}
	}
	# END OF CODING
	
	
	
	$sheet3->setCellValue('B2', 'Statistik Alumni Diklat BKPM');
	$sheet3->setCellValue('B3', $judul);
	
	$sheet3->setCellValue('B5', 'NO');
	$sheet3->getColumnDimension('B')->setWidth(6.14);

	$sheet3->setCellValue('C5', 'Instansi');
	$sheet3->getColumnDimension('C')->setWidth(40);

	$sheet3->setCellValue('D5', $_GET['year']);
	$sheet3->getColumnDimension('D')->setWidth(12);
	
	// Create NewSheet
	$objPHPExcel->createSheet(NULL, 4);
	$objPHPExcel->setActiveSheetIndex(4);
	$objPHPExcel->getActiveSheet()->setTitle('Instansi (Kota)');
	$sheet3 = $objPHPExcel->getActiveSheet();
	
	# CODING #
	$row=@mysql_fetch_array(@mysql_query("SELECT * FROM activity WHERE id='".$_GET['id']."'"));
	$judul=$row['activity'];
	
	// Dapatkan data dari Kota
	$sql="SELECT * FROM dept";
	$query=@mysql_query($sql);
	$no=6;
	while($row=@mysql_fetch_array($query)){
		$sql0="SELECT * FROM city WHERE tipe='Kota' GROUP BY city";
		$query0=@mysql_query($sql0);
		while($row0=@mysql_fetch_array($query0)){	
			$sql1="SELECT * FROM dataalumni WHERE instansi LIKE '%;".$row['id'].";%' AND wilayah LIKE '%Kota;".$row0['city'].";%'";
			$query1=@mysql_query($sql1);
			$countalumni=0;
			while($row1=@mysql_fetch_array($query1)){
				$str_diklat=explode(";",$row1['diklat']);
				$str_tahun=explode(";",$row1['tahun']);
				
				$i=1;
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$_GET['id'] && $str_tahun[$i]==$_GET['year']){
						$countalumni++;
					}
					$i++;
				}
			}
			if($countalumni!=0){
				$sheet3->setCellValue('B'.$no, ($no-5));
				$sheet3->setCellValue('C'.$no, $row['dept']." Kota ".$row0['city']);
				$sheet3->setCellValue('D'.$no, $countalumni);
				$no++;
			}
		}
	}
	# END OF CODING
	
	
	
	$sheet3->setCellValue('B2', 'Statistik Alumni Diklat BKPM');
	$sheet3->setCellValue('B3', $judul);
	
	$sheet3->setCellValue('B5', 'NO');
	$sheet3->getColumnDimension('B')->setWidth(6.14);

	$sheet3->setCellValue('C5', 'Instansi');
	$sheet3->getColumnDimension('C')->setWidth(40);

	$sheet3->setCellValue('D5', $_GET['year']);
	$sheet3->getColumnDimension('D')->setWidth(12);
	
	// Create NewSheet
	$objPHPExcel->createSheet(NULL, 5);
	$objPHPExcel->setActiveSheetIndex(5);
	$objPHPExcel->getActiveSheet()->setTitle('Instansi (Kab)');
	$sheet3 = $objPHPExcel->getActiveSheet();
	
	# CODING #
	$row=@mysql_fetch_array(@mysql_query("SELECT * FROM activity WHERE id='".$_GET['id']."'"));
	$judul=$row['activity'];
	
	// Dapatkan data dari Kota
	$sql="SELECT * FROM dept";
	$query=@mysql_query($sql);
	$no=6;
	while($row=@mysql_fetch_array($query)){
		$sql0="SELECT * FROM city WHERE tipe='Kabupaten' GROUP BY city";
		$query0=@mysql_query($sql0);
		while($row0=@mysql_fetch_array($query0)){	
			$sql1="SELECT * FROM dataalumni WHERE instansi LIKE '%;".$row['id'].";%' AND wilayah LIKE '%Kabupaten;".$row0['city'].";%'";
			$query1=@mysql_query($sql1);
			$countalumni=0;
			while($row1=@mysql_fetch_array($query1)){
				$str_diklat=explode(";",$row1['diklat']);
				$str_tahun=explode(";",$row1['tahun']);
				
				$i=1;
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$_GET['id'] && $str_tahun[$i]==$_GET['year']){
						$countalumni++;
					}
					$i++;
				}
			}
			if($countalumni!=0){
				$sheet3->setCellValue('B'.$no, ($no-5));
				$sheet3->setCellValue('C'.$no, $row['dept']." Kabupaten ".$row0['city']);
				$sheet3->setCellValue('D'.$no, $countalumni);
				$no++;
			}
		}
	}
	# END OF CODING
	
	
	
	$sheet3->setCellValue('B2', 'Statistik Alumni Diklat BKPM');
	$sheet3->setCellValue('B3', $judul);
	
	$sheet3->setCellValue('B5', 'NO');
	$sheet3->getColumnDimension('B')->setWidth(6.14);

	$sheet3->setCellValue('C5', 'Instansi');
	$sheet3->getColumnDimension('C')->setWidth(40);

	$sheet3->setCellValue('D5', $_GET['year']);
	$sheet3->getColumnDimension('D')->setWidth(12);
	
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="Report BKPM Statistik Peserta"');
	header('Cache-Control: max-age=0');

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save('php://output');
?>


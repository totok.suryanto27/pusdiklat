-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 29, 2013 at 02:02 PM
-- Server version: 5.5.25a
-- PHP Version: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `serious`
--

-- --------------------------------------------------------

--
-- Table structure for table `url`
--

CREATE TABLE IF NOT EXISTS `url` (
  `UrlID` int(4) NOT NULL AUTO_INCREMENT,
  `UrlType` text NOT NULL,
  `Url_1` text NOT NULL,
  `Url_2` text NOT NULL,
  `Url_3` text NOT NULL,
  `webtitle` text NOT NULL,
  `filename` text NOT NULL,
  PRIMARY KEY (`UrlID`),
  UNIQUE KEY `UrlID` (`UrlID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `url`
--

INSERT INTO `url` (`UrlID`, `UrlType`, `Url_1`, `Url_2`, `Url_3`, `webtitle`, `filename`) VALUES
(1, 'cpanel', 'dashboard', '', '', 'Control Panel | Dashboard', 'dashboard'),
(2, 'cpanel', 'content', 'all-posts', '', 'Control Panel | All Posts', 'all-posts'),
(3, 'cpanel', 'content', 'new-post', '', 'Control Panel | Tambah Konten Baru', 'new-post'),
(4, 'cpanel', 'content', 'post', '', 'Control Panel | Post Konten', 'post'),
(5, 'cpanel', 'content', 'edit', '', 'Control Panel | Edit Konten', 'edit-post'),
(6, 'cpanel', 'content', 'delete', '', 'Control Panel | Delete Konten', 'delete-post'),
(7, 'cpanel', 'content', 'all-categories', '', 'Control Panel | Semua Kategori', 'all-categories');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php if(!defined('BASEPATH')) exit ('No direct script access allowed'); ?>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equif="content-type" content="text/html; charset=utf-8" />
		<title><?php echo $title;?></title>
		<link rel="stylesheet" type="text/css" href="<?=base_url()?>public/css/admin/adminstyle.css" />
		<link rel="stylesheet" type="text/css" href="<?=base_url()?>public/css/admin/utopia-white.css" />
		<link rel="stylesheet" type="text/css" href="<?=base_url()?>public/css/admin/graph/jquery.jqplot.min.css" />
		<link rel="stylesheet" type="text/css" href="<?=base_url()?>public/css/admin/utopia-responsive.css" />
		
		<script type="text/javascript" src="<?=base_url()?>asset/admin/js/cpanel.js"></script>
		<script type="text/javascript" src="<?=base_url()?>public/js/admin/admin_000.js"></script>
		<script type="text/javascript" src="<?=base_url()?>public/js/admin/admin_001.js"></script>
		<script type="text/javascript" src="<?=base_url()?>public/js/admin/admin_003.js"></script>
		
		<!--[if IE 8]>
		<link href="css/ie8.css" rel="stylesheet">
		<![endif]-->

		<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

		<!--[if gte IE 9]>
		  <style type="text/css">
			.gradient {
			   filter: none;
			}
		  </style>
		<![endif]-->
		<?php $data=$this->session->userdata('isLoggedIn'); ?>
	</head>
	<body>
		<div class="container-fluid">
		<!-- Header starts -->
		<div class="row-fluid">
			<div class="span12">
				<div class="header-top">
					<div class="header-wrapper">
						<a href="#" class="utopia-logo"><img src="<?=base_url(); ?>public/images/admin/logo.png" alt="Logo" height="50px;"></a>
						<div class="header-right">
							<div class="header-divider">&nbsp;</div>
							
							<div class="user-panel header-divider">
								<div class="user-info">
									<img src="<?=base_url(); ?>public/images/admin/user.png" alt="">
									<a style="cursor: pointer;"><?=$data['nama']; ?></a>
								</div>
								<div style="display: none;" class="user-dropbox">
									<ul>
										<li class="user"><a href="">Profile</a></li>
										<li class="settings"><a href="">Account Settings</a></li>
										<li class="logout"><a href="<?=base_url(); ?>cpanel/logout">Logout</a></li>
									</ul>
								</div>
							</div><!-- User panel end -->
						</div><!-- End header right -->
					</div><!-- End header wrapper -->
				</div><!-- End header -->
			</div>
		</div>
		<!-- Header ends -->

		<div class="row-fluid">
			<!-- Sidebar statrt -->
			<div class="sidebar-container span2">
				<div class="sidebar">
					<div class="navbar sidebar-toggle">
						<div class="container">
							<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</a>
						</div>
					</div>
					<div style="height: 0px;" class="nav-collapse leftmenu collapse">
						<ul>
							<li <?php if($navigation=="dashboard") { echo "class=\"current\""; }?>><a href="<?=base_url();?>cpanel/dashboard" class="dashboard smronju" title="Dashboard"><span><em>Dashboard</em></span></a></li>
							<li <?php if($navigation=="all-pages" || $navigation=="new-page") { echo "class=\"current\""; }?>><a href="<?=base_url();?>cpanel/pages/all-pages" class="editor" style="cursor:pointer; background-position: -191px -8px;" title="Halaman" ><span><em>Halaman</em></span></a></li>
							<li <?php if($navigation=="all-posts" || $navigation=="edit-post" || $navigation=="new-post") { echo "class=\"current\""; }?>><a href="<?=base_url();?>cpanel/content/all-posts" class="editor" style="cursor:pointer; background-position: -82px -222px;" title="Konten" ><span><em>Konten</em></span></a></li>
							<li <?php if($navigation=="all-categories" || $navigation=="new-category" || $navigation=="edit-category") { echo "class=\"current\""; }?>><a href="<?=base_url();?>cpanel/content/all-categories" class="editor" style="cursor:pointer; background-position: -188px -439px;" title="Kategori" ><span><em>Kategori</em></span></a></li>
							<li <?php if($navigation=="ext-link" || $navigation=="edit-ext" || $navigation=="newext-link") { echo "class=\"current\""; }?>><a href="<?=base_url();?>cpanel/content/ext-link" class="editor" style="cursor:pointer; background-position: -153px -7px;" title="Link Eksternal" ><span><em>Link Eksternal</em></span></a></li>
							<li <?php if($navigation=="photo-album" || $navigation=="add-albumphoto" || $navigation=="edit-albph" || $navigation=="add-photo"|| $navigation=="add-video") { echo "class=\"current\""; }?>><a class="editor" style="cursor:pointer; background-position: -118px -438px;" title="Media" ><span><em>Media</em></span></a>
								<ul style="left: 222px; visibility: hidden; display: none;" class="dropdown">
									<li><a class="editor smronju" href="<?=base_url();?>cpanel/media/gallery-photo" title="Album Photo" style="cursor:pointer; background-position: -83px -440px;"><span><em>Album Photo</em></span></a></li>
									<li><a class="editor smronju" href="<?=base_url();?>cpanel/media/gallery-video" title="Album Video" style="cursor:pointer; background-position: -118px -438px;"><span><em>Album Video</em></span></a></li>
								</ul>
							</li>
							<li <?php if($navigation=="allmenu-level1" || $navigation=="menu-level1"|| $navigation=="editmenulvl1" || $navigation=="allmenu-level2" || $navigation=="menu-level2"|| $navigation=="editmenulvl2" || $navigation=="allmenu-level3" || $navigation=="menu-level3"|| $navigation=="editmenulvl3") { echo "class=\"current\""; }?>><a class="editor" style="cursor:pointer; background-position: -46px -367px;" title="Menu" ><span><em>Menu</em></span></a>
								<ul style="left: 222px; visibility: hidden; display: none;" class="dropdown">
									<li><a class="editor smronju" href="<?=base_url();?>cpanel/appereance/allmenu-level1" title="Menu Level 1" style="cursor:pointer; background-position: -46px -367px;"><span><em>Menu Level 1</em></span></a></li>
									<li><a class="editor smronju" href="<?=base_url();?>cpanel/appereance/allmenu-level2" title="Menu Level 2" style="cursor:pointer; background-position: -46px -367px;"><span><em>Menu Level 2</em></span></a></li>
									<li><a class="editor smronju" href="<?=base_url();?>cpanel/appereance/allmenu-level3" title="Menu Level 3" style="cursor:pointer; background-position: -46px -367px;"><span><em>Menu Level 3</em></span></a></li>
								</ul>
							</li>
							<li <?php if($navigation=="slider" || $navigation=="add-slider" || $navigation=="index-tab" || $navigation=="new-tab" || $navigation=="edit-tab" || $navigation=="general") { echo "class=\"current\""; }?>><a class="editor" style="cursor:pointer; background-position: -46px -367px;" title="Pengaturan" ><span><em>Pengaturan</em></span></a>
								<ul style="left: 222px; visibility: hidden; display: none;" class="dropdown">
									<li><a class="editor smronju" href="<?=base_url();?>cpanel/appereance/slider" title="Gambar Slider" style="cursor:pointer; background-position: -46px -405px;"><span><em>Gambar Slider</em></span></a></li>
									<li><a class="editor smronju" href="<?=base_url();?>cpanel/appereance/index-tab" title="Tab Index" style="cursor:pointer; background-position: -10px -476px;"><span><em>Tab Index</em></span></a></li>
									<li><a class="editor smronju" href="<?=base_url();?>cpanel/settings/general" title="Sosial Media" style="cursor:pointer; background-position: -117px -296px;"><span><em>Sosial Media</em></span></a></li>
								</ul>
							</li>
							<li <?php if($navigation=="all-schedule" || $navigation=="add-schedule" || $navigation=="bkpm-edit" || $navigation=="bkpm-delete" || $navigation=="candidate" || $navigation=="forum-unanswered") { echo "class=\"current\""; }?>><a class="editor" style="cursor:pointer; background-position: -118px -43px;" title="Kegiatan Pusdiklat" ><span><em>Kegiatan Pusdiklat</em></span></a>
								<ul style="left: 222px; visibility: hidden; display: none;" class="dropdown">
									<li><a class="editor smronju" href="<?=base_url();?>cpanel/bkpm/all-schedule" title="Jadwal Kegiatan" style="cursor:pointer; background-position: -10px -476px;"><span><em>Jadwal Kegiatan</em></span></a></li>
									<li><a class="editor smronju" href="<?=base_url();?>cpanel/bkpm/candidate" title="Daftar Calon Peserta" style="cursor:pointer; background-position: -10px -476px;"><span><em>Daftar Calon Peserta</em></span></a></li>
									<li><a class="editor smronju" href="<?=base_url();?>cpanel/bkpm/forum-unanswered" title="Pertanyaan" style="cursor:pointer; background-position: -153px -44px;"><span><em>Pertanyaan</em></span></a></li>
									<li><a class="editor smronju" href="<?=base_url();?>cpanel/bkpm/testimoni" title="Testimoni" style="cursor:pointer; background-position: -80px -404px;"><span><em>Testimoni</em></span></a></li>
								</ul>
							</li>
							<li><a href="<?=base_url();?>" target="_blank" class="dashboard smronju" title="Lihat Website" style="cursor:pointer; background-position: -117px -9px;"><span><em>Lihat Website</em></span></a></li>
						</ul>

					</div>

				</div>
			</div>

			<!-- Sidebar end -->

			<!-- Body start -->
				<div class="body-container span10">
					<?=$contents?>
				</div>
			<!-- Body end -->

		</div>

		<!-- Maincontent end -->

	</div> <!-- end of container -->
	
	
	
	<!-- javascript placed at the end of the document so the pages load faster -->
	<script type="text/javascript" src="<?=base_url()?>public/js/admin/admin_002.js"></script>
	<script type="text/javascript" src="<?=base_url()?>public/js/admin/admin_005.js"></script>
	<script type="text/javascript" src="<?=base_url()?>public/js/admin/admin_004.js"></script>
	</body>
</html>
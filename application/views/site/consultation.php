<script type="text/javascript" src="<?php echo base_url();?>public/js/jquery-1.3.2.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>public/js/jquery.limit.js"></script>
<script type="text/javascript">
 
$(document).ready(function() {
  $("#message").limita({
	limit: 500,
	id_result: "counter",
	alertClass: "alert"
});
	});
</script>

<style>
.alert{
    color: red;
}
#message {
    resize: none;
}
</style>
<div id="container">
	<!-- Left Panel -->
	<div class="left-panel">
		<div id="tabBar">
			<div id="courseNav">
				
				<div class="inactive2Tab" id="courseNav1" >Kolom Konsultasi</div>
				<div class="myclear"></div>
				
				<div id="courseNav1_area" class="courseContentSmall off" style="min-height: 300px;">
					<div class="courseTitle" style="padding: 0; margin-bottom: 10px; border-bottom: 2px solid blue; padding-bottom: 15px;">Kolom Konsultasi Pusdiklat BKPM</div>
					<form action="" method="post" onsubmit="return validationcons();">
					<div class="flexigrid" style="width: 650px;">
						<div class="mDiv" align="center">
							<div class="ftitle">
								Isikan pertanyaan Anda dalam isian berikut
								<br />Untuk jawaban dapat dilihat dalam halaman ini
							</div>
						</div>
						
						<div class="bDiv">
							<table class="flexme4" cellspacing="0" cellpadding="0" border="0" style="">
								<tbody>
									<tr id="row1">
										<td align="right">
											<div style="text-align: right; width: 150px;">Nama Lengkap dan Gelar<span style="color:red; font-weight: bold;">*</span></div>
										</td>
										<td align="left">
											<div style="text-align: left; width: 470px;">
												<input type="text" name="nama" id="nama" size="30" value="" />
											</div>
										</td>
									</tr>
									
									<tr id="row2" class="erow">
										<td align="right">
											<div style="text-align: right; width: 150px;">email</div>
										</td>
										<td align="left">
											<div style="text-align: left; width: 470px;">
												<input type="text" name="email" id="email" size="30" value=""/>
											</div>
										</td>
									</tr>
									
									<tr id="row3">
										<td align="right">
											<div style="text-align: right; width: 150px;">Instansi<span style="color:red; font-weight: bold;">*</span></div>
										</td>
										<td align="left">
											<div style="text-align: left; width: 470px;">
												<select name="instansi" id="instansi" style="width: 300px">
													<option value="">Pilih Satu</option>
													<?php
														$this->db->select('*');
														$this->db->from('dept');
														$result=$this->db->get();
														$result=$result->result();
														foreach($result as $row){
															echo
																"<option value=\"".$row->id."\">".$row->dept."</option>";
														}
													?>
												</select>
											</div>
										</td>
									</tr>
									
									<tr id="row2" class="erow">
										<td align="right">
											<div style="text-align: right; width: 150px; padding-top: 10px; padding-bottom: 10px;">
												<select name="daerahkantor" id="daerahkantor" onchange="showtipe('office',this.value,'showcityoffice')">
													<option value="">Pilih Satu</option>
													<option value="kota">Kota</option>
													<option value="kabupaten">Kabupaten</option>
													<option value="provinsi">Provinsi</option>
													<option value="pusat">Pusat</option>
												</select><span style="color:red; font-weight: bold;">*</span>
											
											</div>
										</td>
										<td align="left">
											<div style="text-align: left; width: 470px;">
												<div id="showcityoffice"></div>
											</div>
										</td>
									</tr>
									
									<tr id="row3">
										<td align="right" colspan="2">
											<div style="text-align: left;">
												Pertanyaan Anda:<br />
												<textarea cols="70" name="message" id="message" rows="7"></textarea>
												<div id="counter"></div>
											</div>
										</td>
										
									</tr>
									
								</tbody>
							</table>
						</div>
						
						<div class="mDiv" align="center">
							<div class="ftitle">
								<input type="submit" name="submit" value="Kirim Pesan" />
							</div>
						</div>
						
					</div>
					</form>
					
					<div style="padding:50px;"></div>
					
					<style>
						.statement {
							min-height: 50px;
							background-color: rgb(247, 247, 247);
							overflow: hidden;
							border: 1px solid rgb(204, 204, 204);
							margin-bottom: 10px;
							padding: 10px 10px 10px 10px;
							color: #000;
						}
						.statement-title {
							font-weight: bold;
						}
						.admin {
							background-color: rgb(247, 208, 208);
						}
					</style>
					
					<div id="showdata">
						<?php
							// Dapatkan total semua data
							
							$this->db->select('*');
							$this->db->from('consultation');
							$result=$this->db->get();
							$ndata=$result->num_rows();
							
							if($ndata==0){
								echo
									"<div class=\"statement admin\">
										<div class=\"statement-title\">Admin Website Pusdiklat</div>
										<div>Belum Ada Pertanyaan</div>
									</div>";
							} else {
								// Dapatkan Banyak Halaman
								$itemperpage=15;
								$totalpage=ceil($ndata/$itemperpage);
								
								// Halaman
								echo 
									"<div class=\"pagination\">
										<span class=\"pg-selected\" >Prev</span>";
										
								$i=1;
								$status="";
								while($i<=$totalpage){
									if($i!=1){
										if($totalpage>=10){
											if($i<=5 || $i>$totalpage-2){
												echo "<span class=\"pg-normal\" onclick=\"pagecons('".$i."')\">".$i."</span>";
											} else {
												if($status==""){
													$status="ada";
													echo " ... ";
												}
											}
										} else {
											echo "<span class=\"pg-normal\" onclick=\"pagecons('".$i."')\">".$i."</span>";
										}
									} else {
										echo "<span class=\"pg-selected\" >".$i."</span>";
									}
									$i++;
								}
								if($totalpage!=1){
									echo 
										"	<span class=\"pg-normal\" onclick=\"pagecons('2')\">Next</span>";
								} else {
									echo 
										"	<span class=\"pg-selected\" >Next</span>";
								}
								echo"	</div>";
								
								echo "<div style=\"padding-top: 20px;\"></div>";
								
								// Tampilkan data
								$this->db->select('*');
								$this->db->from('consultation');
								$this->db->order_by('ConsID','desc');
								$this->db->limit($itemperpage,0);
								$result=$this->db->get();
								$result=$result->result();
								foreach($result as $row){
									echo
										"<div class=\"statement ".$row->ClassUser."\">
											<div class=\"statement-title\">".$row->Nama." (".$row->Instansi.") pada ".$row->RecorDate."</div>
											<div>".$row->Statement."</div>
										</div>";
								}
								
								
								
								
								// Halaman
								echo 
									"<div class=\"pagination\">
										<span class=\"pg-selected\" >Prev</span>";
										
								$i=1;
								$status="";
								while($i<=$totalpage){
									if($i!=1){
										if($totalpage>=10){
											if($i<=5 || $i>$totalpage-2){
												echo "<span class=\"pg-normal\" onclick=\"pagecons('".$i."')\">".$i."</span>";
											} else {
												if($status==""){
													$status="ada";
													echo " ... ";
												}
											}
										} else {
											echo "<span class=\"pg-normal\" onclick=\"pagecons('".$i."')\">".$i."</span>";
										}
									} else {
										echo "<span class=\"pg-selected\" >".$i."</span>";
									}
									$i++;
								}
								
								if($totalpage!=1){
									echo 
										"	<span class=\"pg-normal\" onclick=\"pagecons('2')\">Next</span>";
								} else {
									echo 
										"	<span class=\"pg-selected\" >Next</span>";
								}
								echo"	</div>";
								
							}
							
							
						?>
						
					</div>
					
				</div>
				
			</div>
		</div>
	</div>
	
	<!-- Right Panel -->
	<div class="right-panel">
		<div class="certColumn">
			<div class="certBox">
				<h3>Link Eksternal</h3>
                    <div class="certBoxContent">
                        <div style="padding: 10px; font-size: 90%;">
							<ul>
								
								<?php
									$this->db->select('*');
									$this->db->from('extlink');
									$this->db->order_by("ExtID","desc");
									$this->db->limit(10);
									$query=$this->db->get();
									$result=$query->result();
									foreach($result as $row){
										echo"<li><a href=\"http://".$row->ExtLink."\" target=\"_blank\">".$row->ExtLink."</a></li>";
									}
								?>
								
							</ul>
						</div>
						<div class="myclear"></div>
                    </div>
                    <div class="certBoxFooter"></div>
			</div>
        </div>
		
		<div class="certColumn">
			<div class="certBox">
				<h3>Ikuti Kami ...</h3>
					<div class="certBoxContent">
						<div style="padding: 10px; font-size: 90%;" align="center">
							<?php
								$this->db->select('*');
								$this->db->from('sosmed');
								$query=$this->db->get();
								$result=$query->result();
								foreach($result as $row){
									if($row->Url!=""){
										if($row->SosMed=="Facebook"){ $img="fb-icon.png"; }
										if($row->SosMed=="Twitter"){ $img="tw-icon.png"; }
										if($row->SosMed=="Google Plus"){ $img="gp-icon.png"; }
										if($row->SosMed=="Instagram"){ $img="insta-icon.png"; }
										echo
											"<a href=\"".$row->Url."\" title=\"Pusdiklat BKPM di ".$row->SosMed."\"	target=\"_blank\">
												<img src=\"".base_url()."public/images/".$img."\" /></a>";
									}
								}
							?>
						</div>
						<div class="myclear"></div>
					</div>
					<div class="certBoxFooter"></div>
			</div>
		</div>
		
		<div class="certColumn">
			<div class="certBox">
				<h3>Galeri Foto & Video</h3>
					<div class="certBoxContent">
						<div style="padding: 10px; font-size: 90%;" align="center">
							<a href="<?php echo base_url(); ?>galeri-foto" title="Galeri Foto Kegiatan Pusdiklat BKPM"><img src="<?php echo base_url(); ?>public/images/pg.png" width="100px" /></a>
							<a href="<?php echo base_url(); ?>galeri-video" title="Galeri Video Kegiatan Pusdiklat BKPM"><img src="<?php echo base_url(); ?>public/images/vg.png" width="100px" /></a>
						</div>
						<div class="myclear"></div>
					</div>
					<div class="certBoxFooter"></div>
			</div>
		</div>
	</div>
	
</div>
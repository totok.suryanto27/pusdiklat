<?php
	function convert_url($str){
		$url=str_replace(' ','-',$str);
		$url=str_replace('#','-',$url);
		$url=str_replace('%','-',$url);
		$url=str_replace('&','-',$url);
		$url=str_replace('*','-',$url);
		$url=str_replace('{','-',$url);
		$url=str_replace('}','-',$url);
		$url=str_replace('\\','-',$url);
		$url=str_replace(':','-',$url);
		$url=str_replace('<','-',$url);
		$url=str_replace('>','-',$url);
		$url=str_replace('?','-',$url);
		$url=str_replace('/','-',$url);
		$url=str_replace('+','-',$url);
		$url=str_replace(',','',$url);
		$url=str_replace('&rsquo;','-',$url);
		$url=str_replace('&rdquo;','-',$url);
		$url=strtolower($url);
		return $url;
	}

?>

<div id="admin-content">
	<div class="admin-content-title"><h1>List Testimoni</h1></div>
	
	<div class="admin-content-content">
		<!--[if IE]>
			<style>
				.admin-content-content  input[type="text"] { 
					padding: 10px 5px 5px 5px;
				}
			</style>
		<![endif]-->
		<!--[if !IE]> -->
			<style>
				.admin-content-content  input[type="text"] { 
					padding: 5px 5px 5px 5px;
				}
			</style>
		<!-- <![endif]-->
		
		<style>
			.statement {
				min-height: 50px;
				background-color: rgb(247, 247, 247);
				overflow: hidden;
				border: 1px solid rgb(204, 204, 204);
				margin-bottom: 10px;
				padding: 10px 10px 10px 10px;
				color: #000;
			}
			.statement-title {
				font-weight: bold;
			}
			
			.tb11 {
				background:#FFFFFF url(http://pusdiklat.bkpm.go.id/public/images/search.png) no-repeat 4px 10px;
				padding:4px 20px 4px 22px;
				border:1px solid #CCCCCC;
				width:430px;
				height:18px;
				text-align: center;
			}
			
			.cat {
				color: #3E6D8E;
				background-color: #E0EAF1;
				border-bottom: 1px solid #B3CEE1;
				border-right: 1px solid #B3CEE1;
				padding: 3px 4px;
				margin: 2px 2px 2px 0px;
				text-decoration: none;
				font-size: 100%;
				line-height: 1.4;
				white-space: nowrap;
				display: inline-block;
			}

			
			.cat:hover {
				background-color: #C4DAE9;
				border-bottom: 1px solid #C4DAE9;
				border-right: 1px solid #C4DAE9;
				text-decoration: none;
				cursor: pointer;
			}

			.cat a:hover {
				text-decoration: none;
				cursor: pointer;
			}
		</style>
		
		<style>
			.activity-feeds ul {
				margin: 0px;
				position: relative !important;
			}
			.activity-feeds ul li {
				list-style: none outside none;
				background: -moz-linear-gradient(center top , #FEFFFF, #F9F9F9) repeat scroll 0% 0% padding-box transparent;
				border-radius: 5px;
				padding: 5px !important;
				margin-bottom: 20px !important;
				border: 1px solid #EBEBEB;
			}
			
			.label-info, .badge-info {
				background-color: #3A87AD;
			}
			.label {
				padding: 1px 4px 2px;
				border-radius: 3px;
			}
			.label, .badge {
				font-size: 10.998px;
				font-weight: bold;
				line-height: 14px;
				color: #FFF;
				vertical-align: baseline;
				white-space: nowrap;
				text-shadow: 0px -1px 0px rgba(0, 0, 0, 0.25);
				background-color: #999;
			}
			
			.btn-group:after {
				clear: both;
			}
			.btn-group:before, .btn-group:after {
				display: table;
				content: "";
			}
			*:before, *:after {
				box-sizing: border-box;
			}
			.btn-group {
				position: relative;
			}
			.btn-group > .btn:last-child, .btn-group > .dropdown-toggle {
				border-top-right-radius: 4px;
				border-bottom-right-radius: 4px;
			}
			.btn-group > .btn:first-child {
				margin-left: 0px;
				border-top-left-radius: 4px;
				border-bottom-left-radius: 4px;
			}
			.btn-group > .btn {
				position: relative;
				float: left;
				margin-left: -1px;
				border-radius: 0px;
			}
			.btn:first-child {
			}
			.btn {
				border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
			}
			.btn {
				display: inline-block;
				padding: 4px 10px;
				margin-bottom: 0px;
				font-size: 13px;
				line-height: 18px;
				color: #333;
				text-align: center;
				text-shadow: 0px 1px 1px rgba(255, 255, 255, 0.75);
				vertical-align: middle;
				cursor: pointer;
				background-color: #F5F5F5;
				background-image: -moz-linear-gradient(center top , #FFF, #E6E6E6);
				background-repeat: repeat-x;
				border-width: 1px;
				border-style: solid;
				border-color: #CCC #CCC #B3B3B3;
				-moz-border-top-colors: none;
				-moz-border-right-colors: none;
				-moz-border-bottom-colors: none;
				-moz-border-left-colors: none;
				border-image: none;
				border-radius: 4px;
				box-shadow: 0px 1px 0px rgba(255, 255, 255, 0.2) inset, 0px 1px 2px rgba(0, 0, 0, 0.05);
			}
			.btn {
				display: inline-block;
				margin-bottom: 0px;
				font-weight: 400;
				text-align: center;
				vertical-align: middle;
				cursor: pointer;
				background-image: none;
				border: 1px solid transparent;
				white-space: nowrap;
				padding: 6px 12px;
				font-size: 14px;
				line-height: 1.42857;
				border-radius: 4px;
				-moz-user-select: none;
			}
		</style>
		
		<!--<div align="center">
			<input class="tb11" type="text" name="searchtext" id="searchtext" placeholder="Gunakan untuk memfilter pertanyaan" onkeyup="showquestforum('1',this.value)" />
		</div>-->
		
		<div id="showdata">
			<?php
				echo "<img src=\"".base_url()."/public/images/admin/empty.gif\" alt=\"\" onload=\"ajaxfunc('start&type=showdatahottestimoni', 'showdata', '".base_url()."public/ajax/ajax.php');\" style=\"display: none\" />";
			
			
				/* $this->db->select('*');
				$this->db->from('question');
				$this->db->where('answer','');
				$result=$this->db->get();
				$ndata=$result->num_rows();
				$itemperpage=20;
				$totalpage=ceil($ndata/$itemperpage);
							
				if($ndata!=0){
					if($totalpage>1){
						# Halaman #
						echo "<div class=\"pagination\">";
						echo "<span class=\"pg-selected\" >Prev</span>";
						$i=1;
						$status="";
						$status_="";
						$status__="";
						$batas=5;
						while($i<=$totalpage){
							if($i!=1){
								if($totalpage>=10){
									if($i<=$batas || $i>$totalpage-2){
										echo "<span class=\"pg-normal\" onclick=\"showquestforum('".$i."',document.getElementById('searchtext').value)\">".$i."</span>";
									} else {
										if($status==""){
											$status="ada";
											echo " ... ";
										}
									}
								} else {
									echo "<span class=\"pg-normal\" onclick=\"showquestforum('".$i."',document.getElementById('searchtext').value)\">".$i."</span>";
								}
							} else {
								echo "<span class=\"pg-selected\" >".$i."</span>";
							}
										
							$i++;
						}
									
									
						if($totalpage!=1){
							echo "<span class=\"pg-normal\" onclick=\"showquestforum('2',document.getElementById('searchtext').value)\">Next</span>";
						} else {
							echo "<span class=\"pg-selected\" >Next</span>";
						}
						echo "</div>";
						echo "<div style=\"padding-top: 20px;\"></div>";
					}
					# Halaman #
								
					# Tampilan Data
					$this->db->select('*');
					$this->db->from('question');
					$this->db->where('answer','');
					$this->db->order_by('QuestID','DESC');
					$this->db->limit($itemperpage,0);
					$result=$this->db->get();
					$result=$result->result();
					$i=1;
					foreach($result as $row){
						echo "
							<div class=\"statement\">
								<div class=\"statement-title\">".$row->title."</div>
								<div><em>".$row->Nama." (".$row->Instansi.") pada ".$row->RecordDate."</em></div>
								<div style=\"margin-top: 10px;\">".$row->Question."</div>
								<div style=\"margin-top: 15px;\"><a class=\"cat\" title=\"Balas Pertanyaan\" onclick=\"answerquest('".$i."','".$row->QuestID."')\">Balas Pertanyaan</a></div>
								<div id=\"answerquest".$i."\"></div>
							</div>";
						$i++;
					}
					echo "<input type=\"hidden\" id=\"nofquest\" value=\"".($i-1)."\" />";
					# Tampilan Data
								
							
					# Halaman #
					if($totalpage>1){
						echo "<div class=\"pagination\" style=\"padding-top: 20px;\">";
						echo "<span class=\"pg-selected\" >Prev</span>";
						$i=1;
						$status="";
						$status_="";
						$status__="";
						while($i<=$totalpage){
							if($i!=1){
								if($totalpage>=10){
									if($i<=$batas || $i>$totalpage-2){
										echo "<span class=\"pg-normal\" onclick=\"showquestforum('".$i."',document.getElementById('searchtext').value)\">".$i."</span>";
									} else {
										if($status==""){
											$status="ada";
											echo " ... ";
										}
									}
								} else {
									echo "<span class=\"pg-normal\" onclick=\"showquestforum('".$i."',document.getElementById('searchtext').value)\">".$i."</span>";
								}
							} else {
								echo "<span class=\"pg-selected\" >".$i."</span>";
							}
							
							$i++;
						}
									
									
						if($totalpage!=1){
							echo "<span class=\"pg-normal\" onclick=\"showquestforum('2',document.getElementById('searchtext').value)\">Next</span>";
						} else {
							echo "<span class=\"pg-selected\" >Next</span>";
						}
						echo "</div>";
					}
					# Halaman #
				} else {
					echo "<div align=\"center\">Data yang Anda cari tidak ada</div>";
				} */
			?>	
		</div>
		
		
		
		
	</div>
</div>
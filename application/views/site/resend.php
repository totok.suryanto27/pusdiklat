<?php if($this->session->userdata('id_diklat')!=""){ ?>

<div id="container">
	<!-- Left Panel -->
	<div class="left-panel">
		<div id="tabBar">
			<div id="courseNav">
				<?php
					// Dapatkan Nama Diklat
					$this->db->select('*');
					$this->db->from('activity');
					$this->db->where('id', $this->session->userdata('id_diklat'));
					$query=$this->db->get();
					$query=$query->result();
					foreach($query as $row){
						$title=$row->activity;
					}
					
					// Dapatkan Jadwal
					$this->db->select('*');
					$this->db->from('jadwal');
					$this->db->where('SchedID', $this->session->userdata('id_form'));
					$query=$this->db->get();
					$query=$query->result();
					foreach($query as $row){
						$date=explode('-',$row->StartDate);
						$startdate=$date[2];
						$date2=explode('-',$row->EndDate);
						$enddate=$date2[2];
						if($date[1]==$date2[1]){
							$timetodo=$startdate." s.d ".$enddate." ".date("M", mktime(0,0,0,$date2[1],1,$date2[0]))." ".$date2[0];
						} else {
							$timetodo=$startdate." ".date("M", mktime(0,0,0,$date[1],1,$date[0]))." ".$date[0]." s.d ".$enddate." ".date("M", mktime(0,0,0,$date2[1],1,$date2[0]))." ".$date2[0];
						}
					}
					
					// Dapatkan email
					$this->db->select('*');
					$this->db->from('datapeserta');
					$this->db->where('id', $_GET['id']);
					$query=$this->db->get();
					$query=$query->result();
					foreach($query as $row){
						$id=$row->id;
						$nama=$row->nama;
						$email=$row->email;
					}
				?>
				<div class="inactive2Tab" id="courseNav1" ><?php echo $title; ?></div>
				<div class="myclear"></div>
				
				<div id="courseNav1_area" class="courseContentSmall off" style="min-height: 300px;">
					<div class="courseTitle" style="padding: 0; margin-bottom: 10px; border-bottom: 2px solid blue; padding-bottom: 15px;"><?php echo $title; ?></div>
					<div class="flexigrid" style="width: 650px;">
						<div class="mDiv" align="center">
							<div class="ftitle">
								Diklat yang Anda ikuti:
								<br /><?php echo $title; ?> [ <?php echo $timetodo; ?> ]
								<br />Kami telah mengirimkan ulang Link Konfirmasi ke email 
								<br /><?php echo $email; ?>
								<br />Silahkan Cek email Anda dan klik tautan yang diberikan
								<br />yang menandakan bahwa Anda telah memberikan konfirmasi atas data yang diberikan
							</div>
						</div>
					</div>
					
					<?php
						/*$to = $email;
						$subject = "[ Email Otomatis ] Link Konfirmasi Keikutsertaan dalam Kegiatan ".$title;
						$message = 
							"Kepada Yth. Bapak/Ibu/Saudara/i ".$nama.
							"\n\nBerikut adalah link konfirmasi atas keikutsertaan Bpk/Ibu/Saudara/i dalam Kegiatan yang akan\ndilaksanakan oleh Pusdiklat BKPM:\nhttp://pusdiklat.acp-creative.com/confirmation/?id=".$id."&diklat=".$this->session->userdata('id_diklat')."&schedid=".$this->session->userdata('id_form')."\nKlik tautan di atas untuk mengkonfirmasi keikutsertaan Anda dalam kegiatan tersebut.\nBila Anda tidak pernah melakukan pendaftaran, abaikan email ini.\n\nHormat Kami,\nAdmin Pusdiklat BKPM";
						$from = "no-reply@acp-creative.com";
						$headers = "From:" . $from;
						mail($to,$subject,$message,$headers);
						//echo "Mail Sent.";*/
						
						include("public/classes/class.phpmailer.php");
						$mail = new PHPMailer();
						$mail->IsSMTP();
						$mail->SMTPDebug = 1;
						$mail->SMTPAuth = true;
						//$mail->SMTPSecure = 'ssl';
						$mail->Host = "webmail.bkpm.go.id";
						$mail->Port = 25; // or 587
						$mail->IsHTML(true);
						$mail->Username = "pusdiklat@bkpm.go.id";
						$mail->Password = "pu5d1kl4t";
						//$mail->AddReplyTo("pusdiklat@bkpm.go.id", "Admin Diklat"); //reply-to address
						$mail->SetFrom("pusdiklat@bkpm.go.id", "Admin Diklat");
						$mail->Subject = "[ Email Otomatis ] Link Konfirmasi Keikutsertaan dalam Kegiatan ".$title;
						$mail->Body = "Kepada Yth. Bapak/Ibu/Saudara/i ".$nama."<p>Berikut adalah link konfirmasi atas keikutsertaan Bpk/Ibu/Saudara/i dalam Kegiatan yang akan\ndilaksanakan oleh Pusdiklat BKPM:<br />http://pusdiklat.bkpm.go.id/confirmation/?id=".$id."&diklat=".$this->session->userdata('id_diklat')."&schedid=".$this->session->userdata('id_form')."<br />Klik tautan di atas untuk mengkonfirmasi keikutsertaan Anda dalam kegiatan tersebut.\nBila Anda tidak pernah melakukan pendaftaran, abaikan email ini.</p><p>Hormat Kami<br />Admin Pusdiklat BKPM";
						$mail->AddAddress($email);
						$mail->Send();
						
						$this->session->set_userdata('id_diklat',$this->input->post(''));
						$this->session->set_userdata('id_form',$this->input->post(''));
					?> 
					
					
					
					
					
				</div>
				
			</div>
		</div>
	</div>
	
	<!-- Right Panel -->
	<div class="right-panel">
		<div class="certColumn">
			<div class="certBox">
				<h3>Link Eksternal</h3>
                    <div class="certBoxContent">
                        <div style="padding: 10px; font-size: 90%;">
							<ul>
								
								<?php
									$this->db->select('*');
									$this->db->from('extlink');
									$this->db->order_by("ExtID","desc");
									$this->db->limit(10);
									$query=$this->db->get();
									$result=$query->result();
									foreach($result as $row){
										echo"<li><a href=\"http://".$row->ExtLink."\" target=\"_blank\">".$row->ExtLink."</a></li>";
									}
								?>
								
							</ul>
						</div>
						<div class="myclear"></div>
                    </div>
                    <div class="certBoxFooter"></div>
			</div>
        </div>
		
		<div class="certColumn">
			<div class="certBox">
				<h3>Ikuti Kami ...</h3>
					<div class="certBoxContent">
						<div style="padding: 10px; font-size: 90%;" align="center">
							<?php
								$this->db->select('*');
								$this->db->from('sosmed');
								$query=$this->db->get();
								$result=$query->result();
								foreach($result as $row){
									if($row->Url!=""){
										if($row->SosMed=="Facebook"){ $img="fb-icon.png"; }
										if($row->SosMed=="Twitter"){ $img="tw-icon.png"; }
										if($row->SosMed=="Google Plus"){ $img="gp-icon.png"; }
										if($row->SosMed=="Instagram"){ $img="insta-icon.png"; }
										echo
											"<a href=\"".$row->Url."\" title=\"Pusdiklat BKPM di ".$row->SosMed."\"	target=\"_blank\">
												<img src=\"".base_url()."public/images/".$img."\" /></a>";
									}
								}
							?>
						</div>
						<div class="myclear"></div>
					</div>
					<div class="certBoxFooter"></div>
			</div>
		</div>
	</div>
	
</div>
<?php } else {
	echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."\" />";
} ?>
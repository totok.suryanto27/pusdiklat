<script type="text/javascript">

	$(function() {
		$('#gallery a').lightBox();
	});

</script>

<script type="text/javascript" src="<?=base_url(); ?>public/js/flowplayer-3.2.6.min.js"></script>

<div id="container">
	<!-- Left Panel -->
	<div class="left-panel">
		<div id="tabBar">
			<div id="courseNav">
				<div class="inactive2Tab" id="courseNav1" >Galeri Video Kegiatan Pusdiklat BKPM</div>
				<div class="myclear"></div>
				
				<div id="courseNav1_area" class="courseContentSmall off" style="min-height: 300px;">
					<?php
						$this->db->select('*');
						$this->db->from('albumvideo');
						$this->db->where('AlbVdID',$contentid);
						$result=$this->db->get();
						$result=$result->result();
						foreach($result as $row){
							$titlealbum=$row->TitleAlbum;
						}
					?>
					<div class="courseTitle" style="padding: 0; margin-bottom: 10px; border-bottom: 2px solid blue; padding-bottom: 15px;"><?php echo $titlealbum; ?></div>
					<div id="showphoto" align="center">
						<?php
						
							$query=@mysql_query("SELECT * FROM video WHERE AlbVdID='".$contentid."'");
							$i=1;
							while($row2=@mysql_fetch_array($query)){
								echo '
									<a  
										 href="'.base_url().'server/php/files/'.$row2['VideoName'].'"
										 style="display:block;width:520px;height:330px"  
										 id="player'.$i.'"> 
									</a>
							
									<script>
										
										flowplayer("player'.$i.'", "'.base_url().'public/player/flowplayer-3.2.7.swf", {
											  clip:  {
												  autoPlay: false,
												  autoBuffering: false
											  }
										  });
									</script>
									<div style="margin-top: 20px;"></div>';
								$i++;
							}
						?>
						
					</div>
				</div>
				
			</div>
		</div>
	</div>
	
	<!-- Right Panel -->
	<div class="right-panel">
		<div class="certColumn">
			<div class="certBox">
				<h3>Link Eksternal</h3>
                    <div class="certBoxContent">
                        <div style="padding: 10px; font-size: 90%;">
							<ul>
								
								<?php
									$this->db->select('*');
									$this->db->from('extlink');
									$this->db->order_by("ExtID","desc");
									$this->db->limit(10);
									$query=$this->db->get();
									$result=$query->result();
									foreach($result as $row){
										echo"<li><a href=\"http://".$row->ExtLink."\" target=\"_blank\">".$row->ExtLink."</a></li>";
									}
								?>
								
							</ul>
						</div>
						<div class="myclear"></div>
                    </div>
                    <div class="certBoxFooter"></div>
			</div>
        </div>
		
		<div class="certColumn">
			<div class="certBox">
				<h3>Ikuti Kami ...</h3>
					<div class="certBoxContent">
						<div style="padding: 10px; font-size: 90%;" align="center">
							<?php
								$this->db->select('*');
								$this->db->from('sosmed');
								$query=$this->db->get();
								$result=$query->result();
								foreach($result as $row){
									if($row->Url!=""){
										if($row->SosMed=="Facebook"){ $img="fb-icon.png"; }
										if($row->SosMed=="Twitter"){ $img="tw-icon.png"; }
										if($row->SosMed=="Google Plus"){ $img="gp-icon.png"; }
										if($row->SosMed=="Instagram"){ $img="insta-icon.png"; }
										echo
											"<a href=\"".$row->Url."\" title=\"Pusdiklat BKPM di ".$row->SosMed."\"	target=\"_blank\">
												<img src=\"".base_url()."public/images/".$img."\" /></a>";
									}
								}
							?>
						</div>
						<div class="myclear"></div>
					</div>
					<div class="certBoxFooter"></div>
			</div>
		</div>
		
		<div class="certColumn">
			<div class="certBox">
				<h3>Galeri Foto & Video</h3>
					<div class="certBoxContent">
						<div style="padding: 10px; font-size: 90%;" align="center">
							<a href="<?php echo base_url(); ?>galeri-foto" title="Galeri Foto Kegiatan Pusdiklat BKPM"><img src="<?php echo base_url(); ?>public/images/pg.png" width="100px" /></a>
							<a href="<?php echo base_url(); ?>galeri-video" title="Galeri Video Kegiatan Pusdiklat BKPM"><img src="<?php echo base_url(); ?>public/images/vg.png" width="100px" /></a>
						</div>
						<div class="myclear"></div>
					</div>
					<div class="certBoxFooter"></div>
			</div>
		</div>
	</div>
	
</div>
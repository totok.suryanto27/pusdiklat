<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php
		$this->db->select('*');
		$this->db->from('activity');
		$this->db->where('id', $_GET['diklat']);
		$query=$this->db->get();
		$query=$query->result();
		foreach($query as $row){
			$diklat=$row->activity;
		}
		
		$this->db->select('*');
		$this->db->from('datapeserta');
		$this->db->where('id', $_GET['id']);
		$query=$this->db->get();
		$query=$query->result();
		foreach($query as $row){
			$nama=$row->nama;
			$noconfirm=$row->NoConfirm;
			$inst_str=explode(";",$row->instansi);
			$this->db->select('*');
			$this->db->from('dept');
			$this->db->where('id', $inst_str[1]);
			$query=$this->db->get();
			$query=$query->result();
			foreach($query as $row2){
				$instansi=$row2->dept;
			}
			$wilayah=explode(";",$row->wilayah);
			$instansi=$instansi." ".$wilayah[0]." ".$wilayah[1];
		}
		
		$this->db->select('*');
		$this->db->from('jadwal');
		$this->db->where('SchedID', $_GET['schedid']);
		$query=$this->db->get();
		$query=$query->result();
		foreach($query as $row){
			$date=explode('-',$row->StartDate);
			$date2=explode('-',$row->EndDate);
			$place=$row->Place;
			$angk=$row->ClassDik;
		}
		
		$startdate=$date[2];
		$enddate=$date2[2];
		if($date[1]==$date2[1]){
			$timetodo=$startdate." s.d ".$enddate." ".date("M", mktime(0,0,0,$date2[1],1,$date2[0]))." ".$date2[0];
		} else {
			$timetodo=$startdate." ".date("M", mktime(0,0,0,$date[1],1,$date[0]))." ".$date[0]." s.d ".$startdate." ".date("M", mktime(0,0,0,$date2[1],1,$date2[0]))." ".$date2[0];
		}
		
		if($date[1]=="01"){ $month="I"; }
		if($date[1]=="02"){ $month="II"; }
		if($date[1]=="03"){ $month="III"; }
		if($date[1]=="04"){ $month="IV"; }
		if($date[1]=="05"){ $month="V"; }
		if($date[1]=="06"){ $month="VI"; }
		if($date[1]=="07"){ $month="VII"; }
		if($date[1]=="08"){ $month="VIII"; }
		if($date[1]=="09"){ $month="IX"; }
		if($date[1]=="10"){ $month="X"; }
		if($date[1]=="11"){ $month="XI"; }
		if($date[1]=="12"){ $month="XII"; }
	?>
	<meta http-equif="content-type" content="text/html; charset=utf-8" />
	<title>Surat Pemanggilan</title>
</head>
<style>
html,body { margin:0; padding:0; background-repeat: no-repeat; min-width: 1028px; background-attachment:fixed; background-position:center top; }

#layout {margin: 0 auto 0 auto; width: 1028px;}

.kopsurat { margin: 10px; }
.logo { float: left; width: 100px; }
.kop { float: left; width: 900px; padding-top: 10px;}
.nosurat { margin-top: 30px; }
.isisurat { margin-top: 60px; }
</style>


<body>
<div id="layout">
	<div class="kopsurat">
		<div class="logo">
			<img src="http://pusdiklat.bkpm.go.id/uploads/images/bkpm.jpg" width="100px"/>
		</div>
		<div class="kop" align="center">
		BADAN KOORDINASI PENANAMAN MODAL<br />JALAN JENDERAL GATOT SUBROTO NO.  44 JAKARTA 12190<br />TELEPON 6221 525 2008 (HUNTING) FAKSIMILE 6221 525 4945<br />SITUS : <a href="http://www.pusdiklat.bkpm.go.id" >www.pusdiklat.bkpm.go.id</a>, E-MAIL : pusdiklat@bkpm.go.id
		</div>
	</div>
	<div style="clear: both"></div>
	<div align="right"><p>Jakarta, <?php echo date("d F Y"); ?></p></div>
	<div class="nosurat">
		<table border="0">
			<tr>
				<td width="100px">Nomor</td>
				<td>:</td>
				<td width="400px" style="padding-left:10px"><?php echo $noconfirm; ?>/<?php echo $angk; ?>/B.2/A.3/<?php echo $diklat; ?>/<?php echo $month; ?>/<?php echo $date[0]; ?></td>
			</tr>
			
			<tr>
				<td width="100px">Lampiran</td>
				<td>:</td>
				<td width="400px" style="padding-left:10px">-</td>
			</tr>
			
			<tr>
				<td width="100px" valign="top">Hal</td>
				<td valign="top">:</td>
				<td width="400px" style="padding-left:10px">Konfirmasi Pendaftaran <?php echo $diklat; ?></td>
			</tr>
		</table>
	</div>

	<div class="isisurat" style="text-align:justify">
		<p>Kepada.<br />
		Yth. <strong><?php echo $nama; ?></strong></p>
		di
		<p><strong><?php echo $instansi; ?></strong></p>
		<p>&nbsp;</p>
		<p>Dengan Hormat</p>
		<p>Sehubungan dengan jadwal kegiatan diklat serta memperhatikan usulan calon peserta diklat dan atau biodata yang telah Bapak/Ibu kirimkan, dengan ini kami mengundang Bapak/Ibu untuk mengikuti Diklat <strong><?php echo $diklat; ?></strong> yang akan dilaksanakan pada <strong><?php echo $timetodo; ?></strong> di <strong><?php echo $place; ?></strong>.</p>
		
		<p>Berkaitan dengan pelaksanaan diklat dimaksud, beberapa hal yang kiranya perlu Bapak/Ibu perhatikan:</p>
		
		<ol>
			<li>Membawa Print Out konfirmasi pendaftaran ini</li>
			<li>Membawa Surat Tugas dari instansi masing-masing</li>
			<li>Membawa Sertifikat Diklat PTSP Bidang Penanaman Modal Tingkat Pertama/Tingkat Dasar  *)</li>
			<li>Membawa Sertifikat Diklat PTSP Bidang Penanaman Modal Tingkat Lanjutan **)</li>
			<li>Membawa pas photo sebanyak 3 lembar (4 x 6 sebanyak 1 lembar dan 3 x 4 sebanyak 2 lembar) ***)</li>
			<li>Membawa laptop dengan fasilitas wifi ****)</li>
			<li>Membawa obat-obatan pribadi</li>
			<li>Tidak diperkenankan memakai celana jeans dan kaos/t-shirt selama kegiatan belajar</li>
			<li>Tidak diperkenankan membawa keluarga/tamu</li>

		</ol>
		
		<p>Untuk informasi lebih lanjut terutama terkait waktu dan tempat regstrasi, dapat menghubungi Pusdiklat BKPM melalui telp. (021) 525 2008 ext. 1346, 1347.</p>
		<p>Demikian kami sampaikan, atas perhatian dan kerjasama yang baik, kami sampaikan terima kasih.</p>
		<p>&nbsp;</p>
		<table border="0" width="100%">
			<tr>
				<td width="60%">&nbsp;</td>
				<td width="40%" style="padding-left:10px" align="center">Pusat Pendidikan dan Pelatihan</td>
			</tr>
			<tr>
				<td width="60%">&nbsp;</td>
				<td width="40%" style="padding-left:10px" align="center">Badan Koordinasi Penanaman Modal</td>
			</tr>
		</table>
		<p>&nbsp;</p>
		<p>Keterangan:</p>
		<table border="0" width="100%">
			<tr>
				<td width="8%">*)</td>
				<td width="2%">:</td>
				<td width="90%" style="padding-left:10px">Hanya berlaku untuk peserta yang akan mengikuti Diklat PTSP Bidang PM Tigkat Lanjutan</td>
			</tr>
			
			<tr>
				<td width="8%">**)</td>
				<td width="2%">:</td>
				<td width="90%" style="padding-left:10px">Hanya berlaku untuk peserta yang akan mengikuti Diklat PTSP Bidang PM Tigkat Sektoral</td>
			</tr>
			
			<tr>
				<td width="8%">***)</td>
				<td width="2%">:</td>
				<td width="90%" style="padding-left:10px">Tidak berlaku untuk peserta yang akan mengikuti Bimbingan Teknis PTSP Bidang PM</td>
			</tr>
			
			<tr>
				<td width="8%">****)</td>
				<td width="2%">:</td>
				<td width="90%" style="padding-left:10px">Hanya berlaku untuk peserta yang akan mengikuti Diklat PTSP Bidang PM Tigkat Pertama dan
	 Diklat PTSP Bidang PM Tigkat Lanjutan</td>
			</tr>
		</table>
		Surat ini tidak memerlukan tanda tangan, karena bersifat konfirmasi pendaftaran.
		<p>&nbsp;</p>
		<p>&nbsp;</p>

		 
		
	</div>
</div>
</body>
</html>
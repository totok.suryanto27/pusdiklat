<script type="text/javascript">

	$(function() {
		$('#gallery a').lightBox();
	});

</script>
<div id="container">
	<!-- Left Panel -->
	<div class="left-panel">
		<div id="tabBar">
			<div id="courseNav">
				<div class="inactive2Tab" id="courseNav1" >Galeri Foto Kegiatan Pusdiklat BKPM</div>
				<div class="myclear"></div>
				
				<div id="courseNav1_area" class="courseContentSmall off" style="min-height: 300px;">
					<?php
						$this->db->select('*');
						$this->db->from('albumphoto');
						$this->db->where('AlbPhID',$contentid);
						$result=$this->db->get();
						$result=$result->result();
						foreach($result as $row){
							$titlealbum=$row->TitleAlbum;
						}
					?>
					<div class="courseTitle" style="padding: 0; margin-bottom: 10px; border-bottom: 2px solid blue; padding-bottom: 15px;"><?php echo $titlealbum; ?></div>
					<div id="showphoto">
						
						<?php
							// Dapatkan total semua data	
							$this->db->select('*');
							$this->db->from('photos');
							$this->db->where('AlbPhID',$contentid);
							$result=$this->db->get();
							$ndata=$result->num_rows();
									
							if($ndata!=0){
								// Dapatkan Banyak Halaman
								$itemperpage=100;
								$totalpage=ceil($ndata/$itemperpage);
										
								// Tampilkan data
								$this->db->select('*');
								$this->db->from('photos');
								$this->db->where('AlbPhID',$contentid);
								$this->db->order_by('PhotoID','desc');
								$this->db->limit($itemperpage,0);
								$result=$this->db->get();
								$result=$result->result();
								echo "
									<div id=\"wrapper\">
										<div id=\"gallery\">";
								$i=1;
								$j=1;
								foreach($result as $row1){
									$img="<img src=\"".base_url()."uploads/gallery/photo/".$row1->ImageThumb."\" height=\"118px\" width=\"138px\" />";
									
									echo
										"<div class=\"holder2\" title=\"".$row1->ImageDesc."\">
											<div class=\"thumb2\"><a href=\"".base_url()."uploads/gallery/photo/".$row1->ImageName."\" >".$img."</a></div>
										</div>";
									if($i==4*$j){
										echo "<br class=\"clearFloat\"/>";
										$j++;
									}
									$i++;
								}
								
								echo "
										</div>
									</div>
									<div style=\"clear: both; margin-top: 10px;\"></div>";
										
								// Halaman
								/* echo 
									"<div class=\"pagination\">
										<span class=\"pg-selected\" >Prev</span>";
												
								$j=1;
								$status="";
								while($j<=$totalpage){
									if($j!=1){
										if($totalpage>=10){
											if($j<=5 || $j>$totalpage-2){
												echo "<span class=\"pg-normal\" onclick=\"pagephoto('".$j."','".$contentid."')\">".$j."</span>";
											} else {
												if($status==""){
													$status="ada";
													echo " ... ";
												}
											}
										} else {
											echo "<span class=\"pg-normal\" onclick=\"pagephoto('".$j."','".$contentid."')\">".$j."</span>";
										}
									} else {
										echo "<span class=\"pg-selected\" >".$j."</span>";
									}
									$j++;
								}
										
								if($totalpage!=1){
									echo 
										"	<span class=\"pg-normal\" onclick=\"pagephoto('2')\">Next</span>";
								} else {
									echo 
										"	<span class=\"pg-selected\" >Next</span>";
								} 
								echo"	</div>";*/
										
							}
						
						?>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	
	<!-- Right Panel -->
	<div class="right-panel">
		<div class="certColumn">
			<div class="certBox">
				<h3>Link Eksternal</h3>
                    <div class="certBoxContent">
                        <div style="padding: 10px; font-size: 90%;">
							<ul>
								
								<?php
									$this->db->select('*');
									$this->db->from('extlink');
									$this->db->order_by("ExtID","desc");
									$this->db->limit(10);
									$query=$this->db->get();
									$result=$query->result();
									foreach($result as $row){
										echo"<li><a href=\"http://".$row->ExtLink."\" target=\"_blank\">".$row->ExtLink."</a></li>";
									}
								?>
								
							</ul>
						</div>
						<div class="myclear"></div>
                    </div>
                    <div class="certBoxFooter"></div>
			</div>
        </div>
		
		<div class="certColumn">
			<div class="certBox">
				<h3>Ikuti Kami ...</h3>
					<div class="certBoxContent">
						<div style="padding: 10px; font-size: 90%;" align="center">
							<?php
								$this->db->select('*');
								$this->db->from('sosmed');
								$query=$this->db->get();
								$result=$query->result();
								foreach($result as $row){
									if($row->Url!=""){
										if($row->SosMed=="Facebook"){ $img="fb-icon.png"; }
										if($row->SosMed=="Twitter"){ $img="tw-icon.png"; }
										if($row->SosMed=="Google Plus"){ $img="gp-icon.png"; }
										if($row->SosMed=="Instagram"){ $img="insta-icon.png"; }
										echo
											"<a href=\"".$row->Url."\" title=\"Pusdiklat BKPM di ".$row->SosMed."\"	target=\"_blank\">
												<img src=\"".base_url()."public/images/".$img."\" /></a>";
									}
								}
							?>
						</div>
						<div class="myclear"></div>
					</div>
					<div class="certBoxFooter"></div>
			</div>
		</div>
		
		<div class="certColumn">
			<div class="certBox">
				<h3>Galeri Foto & Video</h3>
					<div class="certBoxContent">
						<div style="padding: 10px; font-size: 90%;" align="center">
							<a href="<?php echo base_url(); ?>galeri-foto" title="Galeri Foto Kegiatan Pusdiklat BKPM"><img src="<?php echo base_url(); ?>public/images/pg.png" width="100px" /></a>
							<a href="<?php echo base_url(); ?>galeri-video" title="Galeri Video Kegiatan Pusdiklat BKPM"><img src="<?php echo base_url(); ?>public/images/vg.png" width="100px" /></a>
						</div>
						<div class="myclear"></div>
					</div>
					<div class="certBoxFooter"></div>
			</div>
		</div>
	</div>
	
</div>
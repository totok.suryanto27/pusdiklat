<?php
	function convert_url($str){
		$url=str_replace(' ','-',$str);
		$url=str_replace('#','-',$url);
		$url=str_replace('%','-',$url);
		$url=str_replace('&','-',$url);
		$url=str_replace('*','-',$url);
		$url=str_replace('{','-',$url);
		$url=str_replace('}','-',$url);
		$url=str_replace('\\','-',$url);
		$url=str_replace(':','-',$url);
		$url=str_replace('<','-',$url);
		$url=str_replace('>','-',$url);
		$url=str_replace('?','-',$url);
		$url=str_replace('/','-',$url);
		$url=str_replace('+','-',$url);
		$url=str_replace(',','',$url);
		$url=str_replace('&rsquo;','-',$url);
		$url=str_replace('&rdquo;','-',$url);
		$url=strtolower($url);
		return $url;
	}

?>
<style>
/* Image in Text Box */
.tb11 {
	background:#FFFFFF url(http://pusdiklat.bkpm.go.id/public/images/search.png) no-repeat 4px 4px;
	padding:4px 4px 4px 22px;
	border:1px solid #CCCCCC;
	width:430px;
	height:18px;
}

.status {
    float: left;
    margin: 0px 3px 0px 0px;
    padding: 5px;
    width: 48px;
    height: 38px;
	text-align: center;
	font-weight: normal;
}

.unanswered {
    background: none repeat scroll 0% 0% transparent;
    color: #9A4444;
}

.answered {
    background: none repeat scroll 0% 0% #75845C;
    color: #FFF;
    font-weight: normal;
}

.views {
    float: left;
    margin: 0px 7px 0px 0px;
    padding: 5px 0px 5px 4px;
    width: 48px;
    height: 38px;
}

.mini-counts {
    height: 25px;
    font-size: 160%;
    font-weight: normal;
	text-align: center;
}

.question-hyperlink {
    font-weight: bold;
}

.summary {
	float: left;
}

.judul {
	width: 500px;
}

.judul a {
	cursor: pointer;
}
.judul a:hover {
	text-decoration: underline;
	cursor: pointer;
}

.summary h3 {
    font-size: 130%;
	margin: 0px;
	font-weight: bold;
	margin-bottom: 5px;
	line-height: 1.3;
	padding: 0px;
	border: 0px none;
	vertical-align: baseline;
	background: none repeat scroll 0% 0% transparent;
}
.cat {
	color: #3E6D8E;
	background-color: #E0EAF1;
	border-bottom: 1px solid #B3CEE1;
	border-right: 1px solid #B3CEE1;
	padding: 3px 4px;
	margin: 2px 2px 2px 0px;
	text-decoration: none;
	font-size: 100%;
	line-height: 1.4;
	white-space: nowrap;
	display: inline-block;
}

.cat:hover {
	background-color: #C4DAE9;
	border-bottom: 1px solid #C4DAE9;
	border-right: 1px solid #C4DAE9;
	text-decoration: none;
}

.cat a:hover {
	text-decoration: none;
}
</style>


<div id="container">
	<!-- Left Panel -->
	<div class="left-panel">
		<div id="tabBar">
			<div id="courseNav">
				
				<div class="inactive2Tab" id="courseNav1" >Kolom Konsultasi</div>
				<div class="myclear"></div>
				
				<div id="courseNav1_area" class="courseContentSmall off" style="min-height: 300px;">
					<div class="courseTitle" style="padding: 0; margin-bottom: 10px; border-bottom: 2px solid blue; padding-bottom: 15px;">Kolom Konsultasi Pusdiklat BKPM</div>
					<div align="center">
						<p>Sebelum Anda mengajukan pertanyaan kepada kami, mohon dilakukan pencarian terkait apa yang Anda akan cari. Bila tidak diketemukan dalam database, silahkan gunakan tombol <strong>Ajukan Pertanyaan</strong> untuk mengirimkan pertanyaan.</p>
						<input class="tb11" type="text" name="searchtext" id="searchtext" placeholder="Temukan topik pertanyaan yang akan Anda cari di sini!" onkeyup="showdataforum('1',this.value)" />
						<p>Bila tidak diketemukan klik disini<br />
						<input type="button" value="Ajukan Pertanyaan" onclick="showform()"/></p>
					</div>
					
					<?php
						/* $success=$this->session->userdata('success');
						if(isset($success) && $success){
							echo $success;
						}
						if($this->session->userdata('success')!=""){
							$this->session->set_userdata('success','');
						} */
					?>
							
					<p>&nbsp;</p>
					
					<div id="showform"></div>
					
					<p>&nbsp;</p>
					
					<div id="showdata">
						<?php
							$this->db->select('*');
							$this->db->from('question');
							$result=$this->db->get();
							$ndata=$result->num_rows();
							$itemperpage=20;
							$totalpage=ceil($ndata/$itemperpage);
							
							if($ndata!=0){
								if($totalpage>1){
									# Halaman #
									echo "<div class=\"pagination\">";
									echo "<span class=\"pg-selected\" >Prev</span>";
									$i=1;
									$status="";
									$status_="";
									$status__="";
									$batas=5;
									while($i<=$totalpage){
										if($i!=1){
											if($totalpage>=10){
												if($i<=$batas || $i>$totalpage-2){
													echo "<span class=\"pg-normal\" onclick=\"showdataforum('".$i."',document.getElementById('searchtext').value)\">".$i."</span>";
												} else {
													if($status==""){
														$status="ada";
														echo " ... ";
													}
												}
											} else {
												echo "<span class=\"pg-normal\" onclick=\"showdataforum('".$i."',document.getElementById('searchtext').value)\">".$i."</span>";
											}
										} else {
											echo "<span class=\"pg-selected\" >".$i."</span>";
										}
										
										$i++;
									}
									
									
									if($totalpage!=1){
										echo "<span class=\"pg-normal\" onclick=\"showdataforum('2',document.getElementById('searchtext').value)\">Next</span>";
									} else {
										echo "<span class=\"pg-selected\" >Next</span>";
									}
									echo "</div>";
									echo "<div style=\"padding-top: 20px;\"></div>";
								}
								# Halaman #
								
								# Tampilan Data
								$this->db->select('*');
								$this->db->from('question');
								$this->db->order_by('QuestID','DESC');
								$this->db->limit($itemperpage,0);
								$result=$this->db->get();
								$result=$result->result();
								$i=1;
								foreach($result as $row){
									if($i==1){
										echo "<div style=\"border-top: 1px solid #EAEAEA; border-bottom: 1px solid #EAEAEA; padding: 11px 0px; overflow: hidden;\">";
									} else {
										echo "<div style=\"border-bottom: 1px solid #EAEAEA; padding: 11px 0px; overflow: hidden;\">";
									}
									
									if($row->answer==""){
										echo "<div class=\"status unanswered\">Belum<br />Dijawab</div>";
									} else {
										echo "<div class=\"status answered\">Sudah<br />Dijawab</div>";
									}
									
									echo "<div class=\"views\"><div class=\"mini-counts\"><div id=\"countshow".$i."\">".$row->hits."</div></div><div style=\"text-align: center\">x dilihat</div></div>";
									
									if($row->Category==1){ $cat="Pendidikan dan Pelatihan"; }
									if($row->Category==2){ $cat="Prosedur Perizinan dan Non-Perizinan PM"; }
									if($row->Category==3){ $cat="SPIPISE"; }
									if($row->Category==4){ $cat="LKPM"; }
									if($row->Category==5){ $cat="Lain-lain"; }
									
									
									echo "
										<div class=\"summary\">
											<div class=\"judul\"><h3><a class=\"question-hyperlink\" style=\"cursor: pointer\" onclick=\"showquestion(".$i.",".$row->QuestID.")\" title=\"".$row->title."\">".$row->title."</a></h3></div>
											<div><a class=\"cat\" title=\"".$cat."\">".$cat."</a></div>
											<div style=\"font-size: 9px; width: 500px;\"><strong>".$row->Nama." (".$row->Instansi.") pada ".$row->RecordDate."</strong></div>
											<div id=\"showquestion".$i."\"></div>
										</div>";
									
									echo "</div>";
									$i++;
								}
								echo "<input type=\"hidden\" id=\"nofquest\" value=\"".($i-1)."\" />";
								# Tampilan Data
								
								
								# Halaman #
								if($totalpage>1){
									echo "<div class=\"pagination\" style=\"padding-top: 20px;\">";
									echo "<span class=\"pg-selected\" >Prev</span>";
									$i=1;
									$status="";
									$status_="";
									$status__="";
									while($i<=$totalpage){
										if($i!=1){
											if($totalpage>=10){
												if($i<=$batas || $i>$totalpage-2){
													echo "<span class=\"pg-normal\" onclick=\"showdataforum('".$i."',document.getElementById('searchtext').value)\">".$i."</span>";
												} else {
													if($status==""){
														$status="ada";
														echo " ... ";
													}
												}
											} else {
												echo "<span class=\"pg-normal\" onclick=\"showdataforum('".$i."',document.getElementById('searchtext').value)\">".$i."</span>";
											}
										} else {
											echo "<span class=\"pg-selected\" >".$i."</span>";
										}
										
										$i++;
									}
									
									
									if($totalpage!=1){
										echo "<span class=\"pg-normal\" onclick=\"showdataforum('2',document.getElementById('searchtext').value)\">Next</span>";
									} else {
										echo "<span class=\"pg-selected\" >Next</span>";
									}
									echo "</div>";
								}
								# Halaman #
							} else {
								echo "<div align=\"center\">Maaf, belum ada pertanyaan untuk halaman konsultasi ini, silahkan klik tombol Ajukan Pertanyaan untuk membuat pertanyaan yang ditujukan kepada Admin Pusdiklat BKPM.</div>";
							}
						?>
					</div>
					
				</div>
				
			</div>
		</div>
	</div>
	
	<!-- Right Panel -->
	<div class="right-panel">
		<div class="certColumn">
			<div class="certBox">
				<h3>Link Eksternal</h3>
                    <div class="certBoxContent">
                        <div style="padding: 10px; font-size: 90%;">
							<ul>
								
								<?php
									$this->db->select('*');
									$this->db->from('extlink');
									$this->db->order_by("ExtID","desc");
									$this->db->limit(10);
									$query=$this->db->get();
									$result=$query->result();
									foreach($result as $row){
										echo"<li><a href=\"http://".$row->ExtLink."\" target=\"_blank\">".$row->ExtLink."</a></li>";
									}
								?>
								
							</ul>
						</div>
						<div class="myclear"></div>
                    </div>
                    <div class="certBoxFooter"></div>
			</div>
        </div>
		
		<div class="certColumn">
			<div class="certBox">
				<h3>Ikuti Kami ...</h3>
					<div class="certBoxContent">
						<div style="padding: 10px; font-size: 90%;" align="center">
							<?php
								$this->db->select('*');
								$this->db->from('sosmed');
								$query=$this->db->get();
								$result=$query->result();
								foreach($result as $row){
									if($row->Url!=""){
										if($row->SosMed=="Facebook"){ $img="fb-icon.png"; }
										if($row->SosMed=="Twitter"){ $img="tw-icon.png"; }
										if($row->SosMed=="Google Plus"){ $img="gp-icon.png"; }
										if($row->SosMed=="Instagram"){ $img="insta-icon.png"; }
										echo
											"<a href=\"".$row->Url."\" title=\"Pusdiklat BKPM di ".$row->SosMed."\"	target=\"_blank\">
												<img src=\"".base_url()."public/images/".$img."\" /></a>";
									}
								}
							?>
						</div>
						<div class="myclear"></div>
					</div>
					<div class="certBoxFooter"></div>
			</div>
		</div>
		
		<div class="certColumn">
			<div class="certBox">
				<h3>Galeri Foto & Video</h3>
					<div class="certBoxContent">
						<div style="padding: 10px; font-size: 90%;" align="center">
							<a href="<?php echo base_url(); ?>galeri-foto" title="Galeri Foto Kegiatan Pusdiklat BKPM"><img src="<?php echo base_url(); ?>public/images/pg.png" width="100px" /></a>
							<a href="<?php echo base_url(); ?>galeri-video" title="Galeri Video Kegiatan Pusdiklat BKPM"><img src="<?php echo base_url(); ?>public/images/vg.png" width="100px" /></a>
						</div>
						<div class="myclear"></div>
					</div>
					<div class="certBoxFooter"></div>
			</div>
		</div>
	</div>
	
</div>
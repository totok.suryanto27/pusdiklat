<div id="container">
	<!-- Left Panel -->
	<div class="left-panel">
		<div id="tabBar">
			<div id="courseNav">
				<?php
					// Berdasarkan ID Jadwal ?id=x dapatkan nama diklat untuk title
					$this->db->select('*');
					$this->db->from('jadwal');
					$this->db->where('SchedID', $_GET['id']);
					$query=$this->db->get();
					$query=$query->result();
					foreach($query as $row){
						$iddik=$row->DikID;
						$date=explode('-',$row->StartDate);
						$startdate=$date[2];
						$date2=explode('-',$row->EndDate);
						$enddate=$date2[2];
						if($date[1]==$date2[1]){
							$timetodo=$startdate." s.d ".$enddate." ".date("M", mktime(0,0,0,$date2[1],1,$date2[0]))." ".$date2[0];
						} else {
							$timetodo=$startdate." ".date("M", mktime(0,0,0,$date[1],1,$date[0]))." ".$date[0]." s.d ".$startdate." ".date("M", mktime(0,0,0,$date2[1],1,$date2[0]))." ".$date2[0];
						}
						$angkatan=$row->ClassDik;
					}
					
					$this->db->select('*');
					$this->db->from('activity');
					$this->db->where('id', $iddik);
					$query=$this->db->get();
					$query=$query->result();
					foreach($query as $row2){
						$title=$row2->activity;
						
					}
				?>
				<div class="inactive2Tab" id="courseNav1" ><?php echo $title; ?></div>
				<div class="myclear"></div>
				
				<div id="courseNav1_area" class="courseContentSmall off" style="min-height: 300px;" onsubmit="return validateform();">
					<div class="courseTitle" style="padding: 0; margin-bottom: 10px; border-bottom: 2px solid blue; padding-bottom: 15px;">Pembatalan Pendaftaran <?php echo $title; ?> <br />[ <?php echo $timetodo; ?> ]</div>
					
					<form method="post" action="<?php echo base_url(); ?>post-data" name="formulir" autocomplete="off">
						<input type="hidden" name="id_diklat" id="id_diklat" value="<?php echo $iddik; ?>" />
						<input type="hidden" name="angkatan" id="angkatan" value="<?php echo $angkatan; ?>" />
						<input type="hidden" name="tahun" id="tahun" value="<?php echo $date2[0]; ?>" />
						<input type="hidden" name="id_form" id="id_form" value="<?php echo $_GET['id']; ?>" />
						<?php
							if($url1!=""){
								$urllink=base_url().$url1;
							}
							if($url2!=""){
								$urllink=$urllink."/".$url2;
							}
							if($url3!=""){
								$urllink=$urllink."/success";
							}
						?>
						<input type="hidden" name="urllink" id="urllink" value="<?php echo $urllink; ?>" />
						<div class="flexigrid" style="width: 650px;">
							<div class="mDiv" align="center">
								<div class="ftitle">Ketikkan NIP Anda kemudian klik Batalkan Pendaftaran!</div>
							</div>
							<div class="bDiv">
								<table class="flexme4" cellspacing="0" cellpadding="0" border="0" style="">
									<tbody>
										<tr id="row1">
											<td align="right">
												<div style="text-align: right; width: 300px;">Nomor Induk Pegawai <span style="color:red; font-weight: bold;">*</span></div>
											</td>
											<td align="left">
												<div style="text-align: left; width: 320px;">
													<input type="text" size="8" name="nip_part1" id="nip_part1" style="text-align: center;" placeholder="12345678" onclick="cek_nip('11')" onkeyup="cek_nip('1')" onkeydown="validate(event)" />
													-
													<input type="text" size="6" name="nip_part2" id="nip_part2" style="text-align: center;" placeholder="123456" onclick="cek_nip('22')" onkeyup="cek_nip('2')" onkeydown="validate(event)" />
													-
													<input type="text" size="1" name="nip_part3" id="nip_part3" style="text-align: center;" placeholder="1" onclick="cek_nip('33')" onkeyup="cek_nip('3')" onkeydown="validate(event)" />
													-
													<input type="text" size="3" name="nip_part4" id="nip_part4" style="text-align: center;" placeholder="123"  onkeyup="cek_nip('4')" onkeydown="validate(event)" />
												</div>
											</td>
										</tr>
									</tbody>
								</table>
								<div class="iDiv" style="display: none;"></div>
							</div>
							<div class="mDiv" align="center">
								<div class="ftitle"><input type="button" onclick="batalkan()" value="Batalkan!"></input></div>
							</div>
						</div>
						
						<div id="showdata"></div>
						
					</form>
				</div>
				
			</div>
		</div>
	</div>
	
	<!-- Right Panel -->
	<div class="right-panel">
		<div class="certColumn">
			<div class="certBox">
				<h3>Link Eksternal</h3>
                    <div class="certBoxContent">
                        <div style="padding: 10px; font-size: 90%;">
							<ul>
								
								<?php
									$this->db->select('*');
									$this->db->from('extlink');
									$this->db->order_by("ExtID","desc");
									$this->db->limit(10);
									$query=$this->db->get();
									$result=$query->result();
									foreach($result as $row){
										echo"<li><a href=\"http://".$row->ExtLink."\" target=\"_blank\">".$row->ExtLink."</a></li>";
									}
								?>
								
							</ul>
						</div>
						<div class="myclear"></div>
                    </div>
                    <div class="certBoxFooter"></div>
			</div>
        </div>
		
		<div class="certColumn">
			<div class="certBox">
				<h3>Ikuti Kami ...</h3>
					<div class="certBoxContent">
						<div style="padding: 10px; font-size: 90%;" align="center">
							<?php
								$this->db->select('*');
								$this->db->from('sosmed');
								$query=$this->db->get();
								$result=$query->result();
								foreach($result as $row){
									if($row->Url!=""){
										if($row->SosMed=="Facebook"){ $img="fb-icon.png"; }
										if($row->SosMed=="Twitter"){ $img="tw-icon.png"; }
										if($row->SosMed=="Google Plus"){ $img="gp-icon.png"; }
										if($row->SosMed=="Instagram"){ $img="insta-icon.png"; }
										echo
											"<a href=\"".$row->Url."\" title=\"Pusdiklat BKPM di ".$row->SosMed."\"	target=\"_blank\">
												<img src=\"".base_url()."public/images/".$img."\" /></a>";
									}
								}
							?>
						</div>
						<div class="myclear"></div>
					</div>
					<div class="certBoxFooter"></div>
			</div>
		</div>
	</div>
	
</div>
<?php if(!$this->session->userdata('isLoggedIn')){ ?>

<div id="container" align="center">
	<div id="login-form-cpanel">
		<?php 
			$attribute=array('name'=>'frmLogin', 'id'=>'frmLogin', 'autocomplete'=>'off');
			echo form_open('',$attribute);
		?>
		<![if !IE]>
		<div id="txtBoxUser">
		<![endif]>
		<!--[if IE]>
		<div id="txtBoxUserIE">
		<![endif]-->
			<?php
				$attribute=array('name'=>'username', 'id'=>'username', 'autocomplete'=>'off', 'class'=>'watermarked-user', 'style'=>'height:22px; width:200px;', 'onKeyPress'=>'detectEnter(event,\'checkInput()\')');
				echo form_input($attribute); 
			?>
		</div>
		<![if !IE]>
		<div id="txtBoxPassw">
		<![endif]>
		<!--[if IE]>
		<div id="txtBoxPasswIE">
		<![endif]-->
			<?php
				$attribute=array('name'=>'password', 'id'=>'password', 'class'=>'watermarked-password', 'style'=>'height:22px; width:200px;', 'onKeyPress'=>'detectEnter(event,\'checkInput()\')');
				echo form_password($attribute); 
			?>
		</div>
		<div id="txtBoxButton">
			<input src="<?php echo base_url(); ?>asset/admin/images/login-submit.png" style="border-width:0px;" type="image" onclick="return checkInput(this)" value="submit" />
		</div>
		<?php echo form_close(); ?>
	</div>
	<?php
		if(date("Y")!=2013){
			$date="2013 - ".date("Y");
		} else {
			$date=2013;
		}
	?>
	<div id="login-footer">Copyright &copy; <?php echo $date; ?> Badan Koordinasi Penanaman Modal - All Right Reserved</div>

</div>
<?php } ?>
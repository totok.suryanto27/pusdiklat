<?php
	# Definisikan ID Events [bkpm_events], ID Diklat [dataalumni], Angkatan [dataalumni], dan Tahun [dataalumni]
	$diklat=$this->input->post('eventsid');
	$iddiklat=$this->input->post('diklat');
	$angkatan=$this->input->post('angkatan');
	$tahun=$this->input->post('tahun');
	
	# Hapus data di tabel bkpm_events
	$this->db->where('id',$diklat);
	$this->db->delete('bkpm_events');
	
	# Hapus data yang berkaitan di dataalumni
	$this->db->select('*');
	$this->db->from('dataalumni');
	$result=$this->db->get();
	$result=$result->result();
	foreach($result as $row_alumni){
		$str_diklat=explode(";",$row_alumni->diklat);
		$str_angkatan=explode(";",$row_alumni->angkatan);
		$str_tahun=explode(";",$row_alumni->tahun);
		$str_pangkat=explode(";",$row_alumni->pangkat);
		$str_kantoralamat=explode(";",$row_alumni->kantor_alamat);
		$str_kantortelp=explode(";",$row_alumni->kantor_telp);
		$str_instansi=explode(";",$row_alumni->instansi);
		$str_kota=explode(";",$row_alumni->kantor_kota);
		$str_provinsi=explode(";",$row_alumni->kantor_prov);
		$tipe=explode(";",$row_alumni->wilayah);
		$str_fax=explode(";",$row_alumni->fax);
		$str_jabatan=explode(";",$row_alumni->jabatan);
		$str_bagian=explode(";",$row_alumni->bagian);
		$str_website=explode(";",$row_alumni->website);
		
		$i=1;
		while($str_diklat[$i]!=""){
			$i++;
		}
		$totaldiklat=$i;
			
		$i=1;
		while($str_diklat[$i]!=""){
			if($str_diklat[$i]==$iddiklat && $str_angkatan[$i]==$angkatan && $str_tahun[$i]==$tahun){
				if($totaldiklat-1==1){
					$jabatan="0;";
					$instansi="0;";
					$wilayah="";
					$bagian="0;";
					$pangkat="0;";
					$alamatkantor="0;";
					$kantor_telp="0;";
					$kantor_kota="0;";
					$kantor_prov="0;";
					$fax="0;";
					$diklat2="0;";
					$angkatan2="0;";
					$tahun2="0;";
					$website="0;";
				} else {
					$jabatan=";";
					$instansi=";";
					$wilayah="";
					$bagian=";";
					$pangkat=";";
					$alamatkantor=";";
					$kantor_telp=";";
					$kantor_kota=";";
					$kantor_prov=";";
					$fax=";";
					$diklat2=";";
					$angkatan2=";";
					$tahun2=";";
					$website=";";
						
					$j=0;
					while($j<$i){
						$jabatan=$jabatan.$str_jabatan[$j].";";
						$instansi=$instansi.$str_instansi[$j].";";
						$bagian=$bagian.$str_bagian[$j].";";
						$pangkat=$pangkat.$str_pangkat[$j].";";
						$alamatkantor=$alamatkantor.$str_kantoralamat[$j].";";
						$kantor_telp=$kantor_telp.$str_kantortelp[$j].";";
						$kantor_kota=$kantor_kota.$str_kota[$j].";";
						$kantor_prov=$kantor_prov.$str_provinsi[$j].";";
						$fax=$fax.$str_fax[$j].";";
						$diklat2=$diklat.$str_diklat[$j].";";
						$angkatan2=$angkatan.$str_angkatan[$j].";";
						$tahun2=$tahun.$str_tahun[$j].";";
						$website=$website.$str_website[$j].";";
							
						$j++;
					}
						
					$k=0;
					while($k<2*$i-2){
						$wilayah=$wilayah.$tipe[$k].";";
						$k++;
					}
						
					$j=$j+1;
					while($j<$totaldiklat){
						$jabatan=$jabatan.$str_jabatan[$j].";";
						$instansi=$instansi.$str_instansi[$j].";";
						$bagian=$bagian.$str_bagian[$j].";";
						$pangkat=$pangkat.$str_pangkat[$j].";";
						$alamatkantor=$alamatkantor.$str_kantoralamat[$j].";";
						$kantor_telp=$kantor_telp.$str_kantortelp[$j].";";
						$kantor_kota=$kantor_kota.$str_kota[$j].";";
						$kantor_prov=$kantor_prov.$str_provinsi[$j].";";
						$fax=$fax.$str_fax[$j].";";
						$diklat2=$diklat2.$str_diklat[$j].";";
						$angkatan2=$angkatan2.$str_angkatan[$j].";";
						$tahun2=$tahun2.$str_tahun[$j].";";
						$website=$website.$str_website[$j].";";
							
						$j++;
					}
					$k=$k+2;
					while($k<=2*($totaldiklat-1)-1){
						$wilayah=$wilayah.$tipe[$k].";";
						$k++;
					}
				}
				
				$datasql=array(
					'jabatan'=>$jabatan, 
					'instansi'=>$instansi, 
					'wilayah'=>$wilayah, 
					'bagian'=>$bagian, 
					'kantor_alamat'=>$alamatkantor, 
					'kantor_telp'=>$kantor_telp, 
					'kantor_kota'=>$kantor_kota, 
					'kantor_prov'=>$kantor_prov, 
					'fax'=>$fax, 
					'diklat'=>$diklat2, 
					'angkatan'=>$angkatan2, 
					'tahun'=>$tahun2, 
					'website'=>$website, 
					'pangkat'=>$pangkat);
				$this->db->where('id',$row_alumni->id);
				$this->db->update('dataalumni',$datasql);
			}
			$i++;
		}
		
		
	}
	
	echo"<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."siad/input/berkesinambungan\" />";
	exit;
?>
<?php
	if($this->session->userdata('addMemberDiklat1')){			
		$data=$this->session->userdata('addMemberDiklat1');
		$diklat=$data['diklat'];
		$angkatan=$data['angkatan'];
		$tahun=$data['tahun'];
	} else {
		$diklat=$this->input->post('diklat');
		$angkatan=$this->input->post('angkatan');
		$tahun=$this->input->post('tahun');
	}
	$this->session->set_userdata('addMemberDiklat1', '');
	
	if($diklat=="" && $angkatan=="" && $tahun==""){
		echo"<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."siad/input/insidental\" />";
		exit;
	}
?>

<div style="border-bottom: 1px #fff solid; margin-bottom: 20px;">
	<span style="font-weight: bold;"><h1>Tambah Alumni Diklat</h1></span>
</div>

<?php
	
	
	$this->db->select('*');
	$this->db->from('activity');
	$this->db->where('id',$diklat);
	$result=$this->db->get();
	$result=$result->result();
	foreach($result as $row_diklat){
		$syarat=explode(';',$row_diklat->syarat);
		$operator=explode(";",$row_diklat->operator);
	}
	
	$i=1;
	$showsyarat="";
	while($syarat[$i]!=""){
		if($syarat[$i]==0){
			$showsyarat=$showsyarat."Tidak Ada<br />";
		} else {
			$this->db->select('*');
			$this->db->from('activity');
			$this->db->where('id',$syarat[$i]);
			$result=$this->db->get();
			$result=$result->result();
			foreach($result as $row_diklat2){
				$showsyarat=$showsyarat.$row_diklat2->activity." ".$operator[$i]."<br />";
			}
		}
		$i++;
	}
?>


<form method="post" action="" enctype="multipart/form-data" onsubmit="return validateFormMember()">
			
	<input type="hidden" name="syarat" id="syarat" value="<?php echo $row_diklat->syarat; ?>" />
	<input type="hidden" name="id_diklat" id="id_diklat" value="<?php echo $diklat; ?>" />
			
	<input name="diklat" type="hidden" value="<?php echo $diklat; ?>" />
	<input name="angkatan" type="hidden" value="<?php echo $angkatan; ?>" />
	<input name="tahun" type="hidden" value="<?php echo $tahun; ?>" />
		
	<div style="margin-top: 10px;">
		<div style="float:left; display: inline; height: 10px;">
			<input type="checkbox" disabled />
		</div>
				
		<div style="float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;">
			Diklat yang akan diselenggarakan
		</div>
				
		<div style="float:left; display: inline; padding-left: 10px;">
			<input type="text" size="70" name="diklat" id="diklat" value="<?php echo $row_diklat->activity; ?>" disabled style="text-align: center;"/>
		</div>
							
		<div style="clear: both; padding-top: 10px;"></div>
			
		<div style="float:left; display: inline; height: 10px;">
			<input type="checkbox" disabled />
		</div>
				
		<div style="float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;">
			Syarat Pendidikan
		</div>
				
		<div style="float:left; display: inline; padding-left: 10px;">
			<?php echo $showsyarat; ?>
		</div>
							
		<div style="clear: both; padding-top: 10px;"></div>
		
		<div style="float:left; display: inline; height: 10px;">
			<input type="checkbox" disabled />
		</div>
				
		<div style="float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;">
			Tahun
		</div>
				
		<div style="float:left; display: inline; padding-left: 10px;">
			<input type="text" size="5" name="tahun" id="tahun" value="<?php echo $tahun; ?>" disabled style="text-align: center;"/>
		</div>
							
		<div style="clear: both; padding-top: 10px;"></div>
				
		<div style="float:left; display: inline; height: 10px;">
			<input type="checkbox" disabled />
		</div>
				
		<div style="float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;">
			Angkatan
		</div>
				
		<div style="float:left; display: inline; padding-left: 10px; vertical-align: text-top;">
			<input type="text" size="5" name="angkatan" id="angkatan" value="<?php echo $angkatan; ?>" disabled style="text-align: center;"/>
		</div>
				
		<div style="clear: both; padding-top: 10px;"></div>
		
		<div style="float:left; display: inline; height: 10px;">
			<input type="checkbox" disabled />
		</div>
				
		<div style="float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;">
			Nomor Induk Pegawai (NIP)
		</div>
				
		<div style="float:left; display: inline; padding-left: 10px; vertical-align: text-top;">
			<input type="text" size="10" name="nip_part1" id="nip_part1" style="text-align: center;" placeholder="12345678" onclick="cek_nip('11')" onkeyup="cek_nip('1')" onkeydown="validateevent(event)" />
			-
			<input type="text" size="8" name="nip_part2" id="nip_part2" style="text-align: center;" placeholder="123456" onclick="cek_nip('22')" onkeyup="cek_nip('2')" onkeydown="validateevent(event)" />
			-
			<input type="text" size="3" name="nip_part3" id="nip_part3" style="text-align: center;" placeholder="1" onclick="cek_nip('33')" onkeyup="cek_nip('3')" onkeydown="validateevent(event)" />
			-
			<input type="text" size="5" name="nip_part4" id="nip_part4" style="text-align: center;" placeholder="123"  onkeyup="cek_nip('4')" onkeydown="validateevent(event)" />
			&nbsp;
			<input type="button" value="Periksa NIP" onclick="carialumni2()" />
					
		</div>
				
		<div style="clear: both; padding-top: 10px;"></div>
				
		<div id="showalumni"></div>
				
		<div style="clear: both; padding-top: 10px;"></div>
				
	</div>
</form>
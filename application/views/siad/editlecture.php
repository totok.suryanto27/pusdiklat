<div style="border-bottom: 1px #fff solid; margin-bottom: 20px;">
	<span style="font-weight: bold;"><h1>Ubah Data Pengajar (Widiaiswara)</h1></span>
</div>
<?php
	$error=$this->session->userdata('warning');
	$warning=str_replace('<p>','<li>',$error);
	$warning=str_replace('</p>','</li>',$warning);
	if(isset($error) && $error){
		echo "
			<div id=\"tips\" >
				<ul>".$warning."</ul>
			</div>";
	}
	if($this->session->userdata('warning')!=""){
		$this->session->set_userdata('warning','');
	}
	
	$this->db->select('*');
	$this->db->from('lecture');
	$this->db->where('id',$_GET['id']);
	$result=$this->db->get();
	$result=$result->result();
	foreach($result as $row){
		$oldname=$row->lecture;
	}
?>
Lengkapi informasi berikut:
<form method="post" action="" enctype="multipart/form-data">
	<input type="hidden" name="idlect" value="<?php echo $_GET['id']; ?>">
	<div style="margin-top: 10px;">
		<div style="float:left; display: inline; height: 10px;">
			<input type="checkbox" disabled />
		</div>
				
		<div style="float:left; width: 250px; padding-left: 10px; height: 7px; padding-top: 3px;">
			Nama Pengajar/Widiaiswara (Nama Lama)
		</div>
				
		<div style="float:left; display: inline; padding-left: 10px;">
			<input type="text" autocomplete="off" name="lect-oldname" id="lect-oldname" size="70" value="<?php echo $oldname; ?>" disabled/>
		</div>
							
		<div style="clear: both; padding-top: 10px;"></div>
		
		<div style="float:left; display: inline; height: 10px;">
			<input type="checkbox" disabled />
		</div>
				
		<div style="float:left; width: 250px; padding-left: 10px; height: 7px; padding-top: 3px;">
			Nama Pengajar/Widiaiswara (Nama Baru)
		</div>
				
		<div style="float:left; display: inline; padding-left: 10px;">
			<input type="text" autocomplete="off" name="lect-name" id="lect-name" size="70" value="<?php echo $lectname; ?>"/>
		</div>
							
		<div style="clear: both; padding-top: 10px;"></div>
				
		<input type="submit" value="Simpan Data" name="lect-editsubmit" />
	</div>
</form>
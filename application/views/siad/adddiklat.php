<div style="border-bottom: 1px #fff solid; margin-bottom: 20px;">
	<span style="font-weight: bold;"><h1>Tambah Kegiatan Diklat BKPM</h1></span>
</div>
<?php
	$error=$this->session->userdata('warning');
	$warning=str_replace('<p>','<li>',$error);
	$warning=str_replace('</p>','</li>',$warning);
	if(isset($error) && $error){
		echo "
			<div id=\"tips\" >
				<ul>".$warning."</ul>
			</div>";
	}
	if($this->session->userdata('warning')!=""){
		$this->session->set_userdata('warning','');
	}
?>
Lengkapi informasi berikut:
<form method="post" action="" enctype="multipart/form-data" onsubmit="return validateFormActivity()">
	<div style="margin-top: 10px;">
		<div style="float:left; display: inline; height: 10px;">
			<input type="checkbox" disabled />
		</div>
			
		<div style="float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;">
			Nama Kegiatan
		</div>
				
		<div style="float:left; display: inline; padding-left: 10px;">
			<input type="text" autocomplete="off" name="act-name" id="act-name" size="70" value="<?php echo $actname; ?>"/>
		</div>
							
		<div style="clear: both; padding-top: 10px;"></div>
			
		<div style="float:left; display: inline; height: 10px;">
			<input type="checkbox" disabled />
		</div>
				
		<div style="float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;">
			Tipe
		</div>
				
		<div style="float:left; display: inline; padding-left: 10px; vertical-align: text-top;">
			<select id="tipe" name="tipe">
				<?php
					if($acttipe==1){ $select1="selected"; } else { $select1=""; }
					if($acttipe==2){ $select2="selected"; } else { $select2=""; }
				?>
				<option value="">-= Pilih Salah Satu =-</option>
				<option value="1" <?php echo $select1; ?>>Berkesinambungan</option>
				<option value="2" <?php echo $select2; ?>>Insidental</option>
			</select>
		</div>
				
		<div style="clear: both; padding-top: 10px;"></div>
				
		<div style="float:left; display: inline; height: 10px;">
			<input type="checkbox" disabled />
		</div>
				
		<div style="float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;">
			Jumlah Pra Syarat
		</div>
				
		<div style="float:left; display: inline; padding-left: 10px; vertical-align: text-top;">
			<select id="nsyarat" onchange="submitnsyarat(this.value)" name="nsyarat">
				<option value="">-= Pilih Salah Satu =-</option>
				<option value="1">1 Syarat</option>
				<option value="2">2 Syarat</option>
				<option value="3">3 Syarat</option>
				
				
					
			</select> * Biarkan bila tidak ada pra syarat
		</div>
							
		<div style="clear: both; padding-top: 10px;"></div>
			
		<div id="syarat"></div>
				
		<div style="clear: both; padding-top: 10px;"></div>
			
		<input type="submit" value="Simpan Data" name="act-submit" />
	</div>
</form>
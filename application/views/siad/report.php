<div style="border-bottom: 1px #fff solid; margin-bottom: 20px;">
	<span style="font-weight: bold;"><h1>Laporan Peserta Diklat BKPM</h1></span>
</div>

<?php
	$this->db->select('*');
	$this->db->from('activity');
	$this->db->order_by('activity','ASC');
	$result=$this->db->get();
	$result=$result->result();
	$diklat="";
	foreach($result as $row_diklat){
		$diklat=$diklat."<option value=\"".$row_diklat->id."\">".$row_diklat->activity."</option>";
	}
			
	$tahun="";
	$i=2009;
	$j=$i+(date("Y")+3-$i);
	while($i<=$j){
		$tahun=$tahun."<option value=\"".$i."\">".$i."</option>";
		$i++;
	}
	
	$this->db->select('*');
	$this->db->from('grade');
	$result=$this->db->get();
	$result=$result->result();
	$pangkat="";
	foreach($result as $row_pangkat){
		$pangkat=$pangkat."<option value=\"".$row_pangkat->id."\">".$row_pangkat->grade."</option>";
	}
	
	$this->db->select('*');
	$this->db->from('position');
	$result=$this->db->get();
	$result=$result->result();
	$jabatan="";
	foreach($result as $row_jabatan){
		$jabatan=$jabatan."<option value=\"".$row_jabatan->id."\">".$row_jabatan->position."</option>";
	}
	
	$this->db->select('*');
	$this->db->from('dept');
	$result=$this->db->get();
	$result=$result->result();
	$instansi="";
	foreach($result as $row_instansi){
		$instansi=$instansi."<option value=\"".$row_instansi->id."\">".$row_instansi->dept."</option>";
	}
	
	$this->db->select('*');
	$this->db->from('city');
	$this->db->where('tipe','Kota');
	$result=$this->db->get();
	$result=$result->result();
	$kota="";
	$i=1;
	$optgrp[0]="";
	foreach($result as $row_kota){
		$optgrp[$i]=$row_kota->provinsi;
		if($i==1){
			$kota=$kota."<optgroup label=\"".$optgrp[1]."\">";
		} else {
			if($optgrp[$i]!=$optgrp[$i-1]){
				$kota=$kota."</optgroup><optgroup label=\"".$optgrp[$i]."\">";
			}
		}
		$kota=$kota."<option value=\"".$row_kota->id."\">".$row_kota->city."</option>";
		$i++;
	}
	$kota=$kota."</optgroup>";
	
	$this->db->select('*');
	$this->db->from('city');
	$this->db->where('tipe','Kabupaten');
	$result=$this->db->get();
	$result=$result->result();
	$kabupaten="";
	$i=1;
	$optgrp[0]="";
	foreach($result as $row_kabupaten){
		$optgrp[$i]=$row_kabupaten->provinsi;
		if($i==1){
			$kabupaten=$kabupaten."<optgroup label=\"".$optgrp[1]."\">";
		} else {
			if($optgrp[$i]!=$optgrp[$i-1]){
				$kabupaten=$kabupaten."</optgroup><optgroup label=\"".$optgrp[$i]."\">";
			}
		}
		$kabupaten=$kabupaten."<option value=\"".$row_kabupaten->id."\">".$row_kabupaten->city."</option>";
		$i++;
	}
	$kabupaten=$kabupaten."</optgroup>";
	
	$this->db->select('*');
	$this->db->from('city');
	$this->db->group_by('provinsi','ASC');
	$result=$this->db->get();
	$result=$result->result();
	$provinsi="";
	foreach($result as $row_provinsi){
		$provinsi=$provinsi."<option value=\"".$row_provinsi->provinsi."\">".$row_provinsi->provinsi."</option>";
	}
	
	
	echo
		"Tampilkan Laporan Peserta Diklat BKPM Berdasarkan: (klik check-box sebelah kiri untuk mengaktifkan pilihan)<br />
			<form method=\"post\" action=\"".base_url()."siad/report/results/?\" enctype=\"multipart/form-data\" onsubmit=\"return validateFormReport()\">
				<div style=\"margin-top: 10px;\">
					<div style=\"float:left; display: inline; height: 10px;\">
						<input type=\"checkbox\" name=\"chk_diklat\" id=\"chk_diklat\" onclick=\"activateCheck()\"/>
					</div>
					<div style=\"float:left; width: 130px; padding-left: 10px; height: 7px; padding-top: 3px;\">
						Diklat
					</div>
					<div style=\"float:left; display: inline; padding-left: 10px;\">
						: 
						<select name=\"sel_diklat\" id=\"sel_diklat\" style=\"width: 700px;\" disabled>
							<option value=\"\">Pilih Salah Satu</option>
							".$diklat."
						</select>
					</div>
							
					<div style=\"clear: both; padding-top: 10px;\"></div>
					
					<div style=\"float:left; display: inline; height: 10px;\">
						<input type=\"checkbox\" id=\"chk_angkatan\" name=\"chk_angkatan\" onclick=\"activateCheck()\"/>
					</div>
					<div style=\"float:left; width: 130px; padding-left: 10px; height: 7px; padding-top: 3px;\">
						Angkatan
					</div>
					<div style=\"float:left; display: inline; padding-left: 10px;\">
						: 
						<select name=\"sel_angkatan\" id=\"sel_angkatan\" disabled>
							<option value=\"\">Pilih Salah Satu</option>
							<option value=\"I\">I</option><option value=\"II\" >II</option><option value=\"III\" >III</option>
							<option value=\"IV\" >IV</option><option value=\"V\" >V</option><option value=\"VI\" >VI</option>
							<option value=\"VII\" >VII</option><option value=\"VIII\" >VIII</option><option value=\"IX\" >IX</option>
							<option value=\"X\" >X</option><option value=\"XI\" >XI</option><option value=\"XII\" >XII</option>
							<option value=\"XIII\" >XIII</option><option value=\"XIV\" >XIV</option><option value=\"XV\" >XV</option>
							<option value=\"XVI\" >XVI</option><option value=\"XVII\" >XVII</option><option value=\"XVIII\" >XVIII</option>
							<option value=\"XIX\" >XIX</option><option value=\"XX\" >XX</option>
						</select>
					</div>
					
					<div style=\"clear: both; padding-top: 10px;\"></div>
						
					<div style=\"float:left; display: inline; height: 10px;\">
						<input type=\"checkbox\" id=\"chk_tahun\" name=\"chk_tahun\" onclick=\"activateCheck()\"/>
					</div>
					<div style=\"float:left; width: 130px; padding-left: 10px; height: 7px; padding-top: 3px;\">
						Tahun
					</div>
					<div style=\"float:left; display: inline; padding-left: 10px;\">
						: 
						<select name=\"sel_tahun\" id=\"sel_tahun\" disabled>
							<option value=\"\">Pilih Salah Satu</option>
							".$tahun."
						</select>
					</div>
							
					<div style=\"clear: both; padding-top: 10px;\"></div>
					
					<!--<div style=\"float:left; display: inline; height: 10px;\">
						<input type=\"checkbox\" id=\"chk_pangkat\" name=\"chk_pangkat\" onclick=\"activateCheck()\"/>
					</div>
					<div style=\"float:left; width: 130px; padding-left: 10px; height: 7px; padding-top: 3px;\">
						Pangkat (Golongan)
					</div>
					<div style=\"float:left; display: inline; padding-left: 10px;\">
						: 
						<select name=\"sel_pangkat\" id=\"sel_pangkat\" disabled>
							<option value=\"\">Pilih Salah Satu</option>
							".$pangkat."
						</select>
					</div>
							
					<div style=\"clear: both; padding-top: 10px;\"></div>-->
					
					<div style=\"float:left; display: inline; height: 10px;\">
						<input type=\"checkbox\" id=\"chk_jabatan\" name=\"chk_jabatan\" onclick=\"activateCheck()\"/>
					</div>
					<div style=\"float:left; width: 130px; padding-left: 10px; height: 7px; padding-top: 3px;\">
						Jabatan
					</div>
					<div style=\"float:left; display: inline; padding-left: 10px;\">
						: 
						<select name=\"sel_jabatan\" id=\"sel_jabatan\" disabled>
							<option value=\"\">Pilih Salah Satu</option>
							".$jabatan."
						</select>
					</div>
						
					<div style=\"clear: both; padding-top: 10px;\"></div>
					
					<div style=\"float:left; display: inline; height: 10px;\">
						<input type=\"checkbox\" id=\"chk_instansi\" name=\"chk_instansi\" onclick=\"activateCheck()\"/>
					</div>
					<div style=\"float:left; width: 130px; padding-left: 10px; height: 7px; padding-top: 3px;\">
						Instansi
					</div>
					<div style=\"float:left; display: inline; padding-left: 10px;\">
						: 
						<select name=\"sel_instansi\" id=\"sel_instansi\" disabled>
							<option value=\"\">Pilih Salah Satu</option>
							".$instansi."
						</select>
					</div>
							
					<div style=\"clear: both; padding-top: 10px;\"></div>
						
					<div style=\"float:left; display: inline; height: 10px;\">
						<input type=\"radio\" id=\"rad_kota\" name=\"rad_wilayah\" value=\"kota\" onclick=\"activateCheck()\"/>
					</div>
					<div style=\"float:left; width: 130px; padding-left: 10px; height: 7px; padding-top: 3px;\">
						Kota
					</div>
					<div style=\"float:left; display: inline; padding-left: 10px;\">
						: 
						<select name=\"sel_kota\" id=\"sel_kota\" disabled>
							<option value=\"\" selected>Pilih Salah Satu</option>
							<option value=\"all\">Semua Kota</option>
							".$kota."
						</select>
					</div>
							
					<div style=\"clear: both; padding-top: 10px;\"></div>
					
					<div style=\"float:left; display: inline; height: 10px;\">
						<input type=\"radio\" id=\"rad_kabupaten\" name=\"rad_wilayah\" value=\"kabupaten\" onclick=\"activateCheck()\"/>
					</div>
					<div style=\"float:left; width: 130px; padding-left: 10px; height: 7px; padding-top: 3px;\">
						Kabupaten
					</div>
					<div style=\"float:left; display: inline; padding-left: 10px;\">
						: 
						<select name=\"sel_kabupaten\" id=\"sel_kabupaten\" disabled>
							<option value=\"\" selected>Pilih Salah Satu</option>
							<option value=\"all\">Semua Kabupaten</option>
							".$kabupaten."
						</select>
					</div>
							
							
					<div style=\"clear: both; padding-top: 10px;\"></div>
					
					<div style=\"float:left; display: inline; height: 10px;\">
						<input type=\"radio\" id=\"rad_provinsi\" name=\"rad_wilayah\" value=\"provinsi\" onclick=\"activateCheck()\"/>
					</div>
					<div style=\"float:left; width: 130px; padding-left: 10px; height: 7px; padding-top: 3px;\">
						Provinsi
					</div>
					<div style=\"float:left; display: inline; padding-left: 10px;\">
						: 
						<select name=\"sel_provinsi\" id=\"sel_provinsi\" disabled>
							<option value=\"\" selected>Pilih Salah Satu</option>
							<option value=\"all\">Semua Provinsi</option>
							".$provinsi."
						</select>
					</div>
					
					<div style=\"clear: both; padding-top: 10px;\"></div>
					
					<div style=\"float:left; display: inline; height: 10px;\">
						<input type=\"checkbox\" id=\"chk_name\" name=\"chk_name\" onclick=\"activateCheck()\"/>
					</div>
					<div style=\"float:left; width: 130px; padding-left: 10px; height: 7px; padding-top: 3px;\">
						Nama
					</div>
					<div style=\"float:left; display: inline; padding-left: 10px;\">
						: 
						<input type=\"text\" name=\"sel_name\" id=\"sel_name\" disabled size=\"70\" autocomplete=\"off\"/>
					</div>
							
				</div>
						
				<div style=\"clear: both; padding-top: 40px;\"></div>
					
				<div>
					<input type=\"submit\" name=\"submit\" value=\"Submit\" />&nbsp;<input type=\"button\" name=\"reset\" value=\"Reset\" onclick=\"resetFormReport()\"/>
				</div>
					
			</form>";
	
?>
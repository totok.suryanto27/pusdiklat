<?php
	if($this->input->post('diklat')=="" && $this->input->post('angkatan')=="" && $this->input->post('tahun')==""){
		echo"<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."siad/input\" />";
		exit;
	}
?>

<div style="border-bottom: 1px #fff solid; margin-bottom: 20px;">
	<span><h2>List Alumni <?php echo $this->input->post('diklat'); ?> Angkatan <?php echo $this->input->post('angkatan'); ?> Tahun <?php echo $this->input->post('tahun'); ?></h2></span>
</div>

<div id="showdata">
	<?php
		$this->db->select('*');
		$this->db->from('activity');
		$this->db->where('activity',$this->input->post('diklat'));
		$result=$this->db->get();
		$result=$result->result();
		foreach($result as $row_diklat){
			$diklat=$row_diklat->id;
		}
		
		
		// Dapatkan banyak data yang ada dengan ketentuan di atas
		$this->db->select('*');
		$this->db->from('dataalumni');
		$result=$this->db->get();
		$result=$result->result();
		$ndata=0;
		foreach($result as $row_alumni){
			$str_diklat=explode(";",$row_alumni->diklat);
			$str_angkatan=explode(";",$row_alumni->angkatan);
			$str_tahun=explode(";",$row_alumni->tahun);
			$i=1;
			while($str_diklat[$i]!=""){
				if($str_diklat[$i]==$diklat && $str_angkatan[$i]==$this->input->post('angkatan') && $str_tahun[$i]==$this->input->post('tahun')){
					$pos=$i;
					$ndata++;
				}
				$i++;
			}
			
		} 
		
		
		$itemperpage=20;
		$totalpage=ceil($ndata/$itemperpage);
		
		if($ndata!=0){
			
			# Halaman #
			echo "<div class=\"pagination\">";
			echo "<span class=\"pg-selected\" >Prev</span>";
			$i=1;
			$status="";
			$status_="";
			$status__="";
			while($i<=$totalpage){
				if($i!=1){
					if($totalpage>=10){
						if($i>$totalpage-2){
							echo "<span class=\"pg-normal\" onclick=\"showdataalumni('".$i."','".$diklat."','".$this->input->post('angkatan')."','".$this->input->post('tahun')."')\">".$i."</span>";
						} else {
							if($status==""){
								$status="ada";
								echo " ... ";
							}
						}
					} else {
						echo "<span class=\"pg-normal\" onclick=\"showdataalumni('".$i."','".$diklat."','".$this->input->post('angkatan')."','".$this->input->post('tahun')."')\">".$i."</span>";
					}
				} else {
					echo "<span class=\"pg-selected\" >".$i."</span>";
				}
				
				$i++;
			}
			
			
			if($totalpage!=1){
				echo "<span class=\"pg-normal\" onclick=\"showdataalumni('2','".$diklat."','".$this->input->post('angkatan')."','".$this->input->post('tahun')."')\">Next</span>";
			} else {
				echo "<span class=\"pg-selected\" >Next</span>";
			}
			echo "<span class=\"pg-normal\" onclick=\"document.location='http://pusdiklat.bkpm.go.id/asset/export2.php?iddik=".$diklat."&idakt=".$this->input->post('angkatan')."&tahun=".$this->input->post('tahun')."'\">Export ke xls</span></div>";
			echo "<div style=\"padding-top: 20px;\"></div>";
			# Halaman # 
			
			# Tampilan Data
			echo "
			<div class=\"jtable-main-container\">
				<div class=\"jtable-busy-panel-background\" style=\"display: none; width: 940px; height: 123px;\"></div>
				<div class=\"jtable-busy-message\" style=\"display: none;\"></div>
				<div class=\"jtable-title\">
					<div class=\"jtable-title-text\">
						Daftar Alumni Diklat ".$this->input->post('diklat')." Angkatan ".$this->input->post('angkatan')." Tahun ".$this->input->post('tahun')."
					</div>
				</div>
				
				<table class=\"jtable\">
					<thead>
						<tr>
							<th class=\"jtable-column-header\" style=\"width: 5%;\">
								<div class=\"jtable-column-header-container\" align=\"center\">No</div>
							</th>
							<th class=\"jtable-column-header\" style=\"width: 18%;\">
								<div class=\"jtable-column-header-container\" align=\"center\">Nama</div>
							</th>
							<th class=\"jtable-column-header\" style=\"width: 18%;\">
								<div class=\"jtable-column-header-container\" align=\"center\">NIP</div>
							</th>
							<th class=\"jtable-column-header\" style=\"width: 18%;\">
								<div class=\"jtable-column-header-container\" align=\"center\">Instansi</div>
							</th>
							<th class=\"jtable-column-header\" style=\"width: 18%;\">
								<div class=\"jtable-column-header-container\" align=\"center\">Diklat & Angkatan</div>
							</th>
							<th class=\"jtable-column-header\" style=\"width: 17%;\">
								<div class=\"jtable-column-header-container\" align=\"center\">Jabatan</div>
							</th>
							<th class=\"jtable-column-header\" style=\"width: 6%;\">
								<div class=\"jtable-column-header-container\" align=\"center\">Aksi</div>
							</th>
						</tr>
					</thead>
					<tbody>";
			
			$this->db->select('*');
			$this->db->from('dataalumni');
			$this->db->order_by('nama','ASC');
			$result=$this->db->get();
			$result=$result->result();
			$j=1;
			$k=1;
			foreach($result as $row_alumni){
				$str_diklat=explode(";",$row_alumni->diklat);
				$str_angkatan=explode(";",$row_alumni->angkatan);
				$str_tahun=explode(";",$row_alumni->tahun);
				$str_instansi=explode(";",$row_alumni->instansi);
				$str_wilayah=explode(";",$row_alumni->wilayah);
				$str_jabatan=explode(";",$row_alumni->jabatan);
				$str_bagian=explode(";",$row_alumni->bagian);
				$i=1;
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$diklat && $str_angkatan[$i]==$this->input->post('angkatan') && $str_tahun[$i]==$this->input->post('tahun') && $j<=$itemperpage){
						if($j==2*$k-1){ 
							$even=" jtable-row-even";
							$k++;
						} else { 
							$even=""; 
						}
						
						$this->db->select('*');
						$this->db->from('dept');
						$this->db->where('id',$str_instansi[$i]);
						$result=$this->db->get();
						$result=$result->result();
						foreach($result as $row2){
							$dept=$row2->dept;
						}
						
						// Data Diklat
						$this->db->select('*');
						$this->db->from('activity');
						$this->db->where('id',$str_diklat[$i]);
						$result=$this->db->get();
						$result=$result->result();
						foreach($result as $row2){
							$diklatname=$row2->activity;
						}
						$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
						
						// Data Jabatan
						$this->db->select('*');
						$this->db->from('position');
						$this->db->where('id',$str_jabatan[$i]);
						$result=$this->db->get();
						$result=$result->result();
						foreach($result as $row2){
							$jabatan=$row2->position;
						}
						
						if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
							$jabatan=$jabatan." ".$str_bagian[$i];
						}
						//echo $row_alumni->nama;
						if($str_wilayah[2*$i-2]!="pusat"){
							$showwilayah=$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1];
						} else {
							$showwilayah=$dept;
						}
						
						
						echo "<tr class=\"jtable-data-row".$even."\">
							<td><div align=\"center\">".$j."</div></td>
							<td>".$row_alumni->nama."</td>
							<td>".$row_alumni->nip."</td>
							<td><div align=\"center\">".$showwilayah."</div></td>
							<td><div align=\"center\">".$diklatnakt."</center></td>
							<td><div align=\"center\">".$jabatan."</div></td>
							<td><div align=\"center\">
								<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/edit/?id=".$row_alumni->id."&diklat=".$diklat."'\" />
								<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari list Diklat ".$this->input->post('diklat')." Angkatan ".$this->input->post('angkatan')." Tahun ".$this->input->post('tahun')."?')){ deletedataalumni('".$row_alumni->id."','1','".$diklat."','".$this->input->post('angkatan')."','".$this->input->post('tahun')."'); }\" />
							</td>
						</tr>";
						
						$j++;
					}
					$i++;
				}
				
			}

			echo"
					</tbody>
				</table>
			</div>";
			
			
			# Tampilan Data
			
			# Halaman #
			echo "<div class=\"pagination\" style=\"padding-top: 20px;\">";
			echo "<span class=\"pg-selected\" >Prev</span>";
			$i=1;
			$status="";
			$status_="";
			$status__="";
			while($i<=$totalpage){
				if($i!=1){
					if($totalpage>=10){
						if($i>$totalpage-2){
							echo "<span class=\"pg-normal\" onclick=\"showdataalumni('".$i."','".$diklat."','".$this->input->post('angkatan')."','".$this->input->post('tahun')."')\">".$i."</span>";
						} else {
							if($status==""){
								$status="ada";
								echo " ... ";
							}
						}
					} else {
						echo "<span class=\"pg-normal\" onclick=\"showdataalumni('".$i."','".$diklat."','".$this->input->post('angkatan')."','".$this->input->post('tahun')."')\">".$i."</span>";
					}
				} else {
					echo "<span class=\"pg-selected\" >".$i."</span>";
				}
				
				$i++;
			}
			
			
			if($totalpage!=1){
				echo "<span class=\"pg-normal\" onclick=\"showdataalumni('2','".$diklat."','".$this->input->post('angkatan')."','".$this->input->post('tahun')."')\">Next</span>";
			} else {
				echo "<span class=\"pg-selected\" >Next</span>";
			}
			echo "<span class=\"pg-normal\" onclick=\"document.location='http://pusdiklat.bkpm.go.id/asset/export2.php?iddik=".$diklat."&idakt=".$this->input->post('angkatan')."&tahun=".$this->input->post('tahun')."'\">Export ke xls</span></div>";
			# Halaman #
		}
	?>
</div>
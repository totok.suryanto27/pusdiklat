<div style="border-bottom: 1px #fff solid; margin-bottom: 20px;">
	<span style="font-weight: bold;"><h1>Diklat BKPM (Berkesinambungan) -- Tambah Diklat Baru</h1></span>
</div>

<?php
	$syarat="";
	$this->db->select('*');
	$this->db->from('activity');
	$result=$this->db->get();
	$ndata=$result->num_rows();
	
	$i=1;
	while($i<=$ndata){
		$syarat=$syarat."<option value=\"".$i."\">".$i." Syarat</option>";
		$i++;
	}
	
	$diklat="";
	$this->db->select('*');
	$this->db->from('activity');
	$this->db->where('tipe','1');
	$this->db->order_by('activity','ASC');
	$result=$this->db->get();
	$result=$result->result();
	foreach($result as $row_diklat){
		$diklat=$diklat.
			"<option value=\"".$row_diklat->id."\">".$row_diklat->activity."</option>";
	}
	
	$tahun="";
	$i=2009;
	$j=$i+(date("Y")+3-$i);
	while($i<=$j){
		if(date("Y")==$i) { $select="selected"; } else { $select=""; }
		$tahun=$tahun."<option value=\"".$i."\" ".$select.">".$i."</option>";
		$i++;
	}
	
	
	$this->db->select('*');
	$this->db->from('city');
	$this->db->where('tipe','Kota');
	$result=$this->db->get();
	$result=$result->result();
	$kota="";
	$i=1;
	$optgrp[0]="";
	foreach($result as $row_kota){
		$optgrp[$i]=$row_kota->provinsi;
		if($i==1){
			$kota=$kota."<optgroup label=\"".$optgrp[1]."\">";
		} else {
			if($optgrp[$i]!=$optgrp[$i-1]){
				$kota=$kota."</optgroup><optgroup label=\"".$optgrp[$i]."\">";
			}
		}
		$kota=$kota."<option value=\"".$row_kota->id."\">".$row_kota->city."</option>";
		$i++;
	}
	$kota=$kota."</optgroup>";
	
	$this->db->select('*');
	$this->db->from('city');
	$this->db->where('tipe','Kabupaten');
	$result=$this->db->get();
	$result=$result->result();
	$kabupaten="";
	$i=1;
	$optgrp[0]="";
	foreach($result as $row_kabupaten){
		$optgrp[$i]=$row_kabupaten->provinsi;
		if($i==1){
			$kabupaten=$kabupaten."<optgroup label=\"".$optgrp[1]."\">";
		} else {
			if($optgrp[$i]!=$optgrp[$i-1]){
				$kabupaten=$kabupaten."</optgroup><optgroup label=\"".$optgrp[$i]."\">";
			}
		}
		$kabupaten=$kabupaten."<option value=\"".$row_kabupaten->id."\">".$row_kabupaten->city."</option>";
		$i++;
	}
	$kabupaten=$kabupaten."</optgroup>";
	
	$syarat="";
	$this->db->select('*');
	$this->db->from('lecture');
	$result=$this->db->get();
	$ndata=$result->num_rows();
	$i=1;
	$lecture="";
	while($i<=$ndata){
		$lecture=$lecture."<option value=\"".$i."\">".$i." Orang</option>";
		$i++;
	}

?>
Lengkapi informasi berikut:



<form method="post" action="" enctype="multipart/form-data" onsubmit="return validateFormDiklat()">
	<div style="margin-top: 10px;">
		<div style="float:left; display: inline; height: 10px;">
			<input type="checkbox" disabled />
		</div>
				
		<div style="float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;">
			Diklat yang akan diselenggarakan
		</div>
				
		<div style="float:left; display: inline; padding-left: 10px;">
			<select id="diklat" name="diklat" onchange="showsyarat(this.value)" style="width: 450px;">
				<option value="">-= Pilih Salah Satu =-</option>
				<?php echo $diklat; ?>
			</select>
		</div>
							
		<div style="clear: both; padding-top: 10px;"></div>
				
		<div id="syarat"></div>
			
		<div style="clear: both; padding-top: 10px;"></div>
				
		<div style="float:left; display: inline; height: 10px;">
			<input type="checkbox" disabled />
		</div>
				
		<div style="float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;">
			Tahun
		</div>
				
		<div style="float:left; display: inline; padding-left: 10px;">
			<select name="tahun" id="tahun">
				<option value="">-= Pilih Salah Satu =-</option>
				<?php echo $tahun; ?>
			</select>
		</div>
							
		<div style="clear: both; padding-top: 10px;"></div>
				
		<div style="float:left; display: inline; height: 10px;">
			<input type="checkbox" disabled />
		</div>
				
		<div style="float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;">
			Angkatan
		</div>
				
		<div style="float:left; display: inline; padding-left: 10px; vertical-align: text-top;">
			<select name="angkatan" id="angkatan">
				<option value="">-= Pilih Salah Satu =-</option>
				<option value="I">I</option><option value="II" >II</option><option value="III" >III</option>
				<option value="IV" >IV</option><option value="V" >V</option><option value="VI" >VI</option>
				<option value="VII" >VII</option><option value="VIII" >VIII</option><option value="IX" >IX</option>
				<option value="X" >X</option><option value="XI" >XI</option><option value="XII" >XII</option>
				<option value="XIII" >XIII</option><option value="XIV" >XIV</option><option value="XV" >XV</option>
				<option value="XVI" >XVI</option><option value="XVII" >XVII</option><option value="XVIII" >XVIII</option>
				<option value="XIX" >XIX</option><option value="XX" >XX</option>
			</select>
		</div>
				
		<div style="clear: both; padding-top: 10px;"></div>
			
		<div style="float:left; display: inline; height: 10px;">
			<input type="checkbox" disabled />
		</div>
				
		<div style="float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;">
			Waktu Pelaksanaan
		</div>
				
		<div style="float:left; display: inline; padding-left: 10px; vertical-align: text-top;">
			<select name="startdate" id="startdate" style="float: left; margin-right: 10px;">
				<?php
					$i=1;
					while($i<=31){
						if($i==date("j")){ $select="selected"; } else { $select=""; }
						if($i<10){
							echo "<option value=\"0".$i."\" ".$select.">0".$i."</option>";
						} else {
							echo "<option value=\"".$i."\"".$select.">".$i."</option>";
						}
						$i++;
					}
				?>
			</select>
			<select name="startmonth" id="startmonth" style="float: left; margin-right: 10px;">
				<?php
					$i=1;
					while($i<=12){
						if($i==date("n")){ $select="selected"; } else { $select=""; }
						if($i<10){
							echo "<option value=\"".date("F",mktime(0, 0, 0, $i, 1, date("Y")))."\" ".$select.">".date("F",mktime(0, 0, 0, $i, 1, date("Y")))."</option>";
						} else {
							echo "<option value=\"".date("F",mktime(0, 0, 0, $i, 1, date("Y")))."\"".$select.">".date("F",mktime(0, 0, 0, $i, 1, date("Y")))."</option>";
						}
						$i++;
					}
				?>
			</select>
			<input type="text" name="startyear" id="startyear" size="4" value="<?php echo date("Y"); ?>" style="float: left; text-align: center;"/>
			<div style="float: left; padding-left: 5px; padding-right: 5px;">Sampai Dengan</div>
			<select name="enddate" id="enddate" style="float: left; margin-right: 10px;">
				<?php
					$i=1;
					while($i<=31){
						if($i==date("j")){ $select="selected"; } else { $select=""; }
						if($i<10){
							echo "<option value=\"0".$i."\" ".$select.">0".$i."</option>";
						} else {
							echo "<option value=\"".$i."\"".$select.">".$i."</option>";
						}
						$i++;
					}
				?>
			</select>
			<select name="endmonth" id="endmonth" style="float: left; margin-right: 10px;">
				<?php
					$i=1;
					while($i<=12){
						if($i==date("n")){ $select="selected"; } else { $select=""; }
						if($i<10){
							echo "<option value=\"".date("F",mktime(0, 0, 0, $i, 1, date("Y")))."\" ".$select.">".date("F",mktime(0, 0, 0, $i, 1, date("Y")))."</option>";
						} else {
							echo "<option value=\"".date("F",mktime(0, 0, 0, $i, 1, date("Y")))."\"".$select.">".date("F",mktime(0, 0, 0, $i, 1, date("Y")))."</option>";
						}
						$i++;
					}
				?>
			</select>
			<input type="text" name="endyear" id="endyear" size="4" value="<?php echo date("Y"); ?>" style="float: left; text-align: center;"/>
		</div>
							
		<div style="clear: both; padding-top: 10px;"></div>
			
		<div style="float:left; display: inline; height: 10px;">
			<input type="checkbox" disabled />
		</div>
				
		<div style="float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;">
			Tempat Pelaksanaan
		</div>
				
		<div style="float:left; display: inline; padding-left: 10px; vertical-align: text-top;">
			<textarea id="place" name="place" rows="5" cols="50"></textarea>
		</div>
							
		<div style="clear: both; padding-top: 10px;"></div>
				
		<div style="float:left; display: inline; height: 10px;">
			<input type="radio" id="kota" name="wilayah" value="kota" onclick="activateCheckInput()"/>
		</div>
		<div style="float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;">
			Kota
		</div>
		<div style="float:left; display: inline; padding-left: 10px;"> 
			<select name="kota" id="sel_kota" disabled onchange="showprov('../../../',this.value,'showprov')">
				<option value="">-= Pilih Salah Satu =-</option>
				<?php echo $kota; ?>
			</select>
		</div>
							
		<div style="clear: both; padding-top: 10px;"></div>
			
		<div style="float:left; display: inline; height: 10px;">
			<input type="radio" id="kabupaten" name="wilayah" value="kabupaten" onclick="activateCheckInput()"/>
		</div>
		<div style="float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;">
			Kabupaten
		</div>
		<div style="float:left; display: inline; padding-left: 10px;">
			<select name="kabupaten" id="sel_kabupaten" disabled onchange="showprov('../../../',this.value,'showprov')">
				<option value="">-= Pilih Salah Satu =-</option>
				<?php echo $kabupaten; ?>
			</select>
		</div>
							
		<div style="clear: both; padding-top: 10px;"></div>
			
		<div style="float:left; display: inline; height: 10px;">
			<input type="radio" id="tidakadainfo" name="wilayah" value="tidakadainfo" onclick="activateCheckInput()"/>
		</div>
		<div style="float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;">
			Tidak ada info
		</div>
		<div style="float:left; display: inline; padding-left: 10px;">
			&nbsp;
		</div>
							
		<div style="clear: both; padding-top: 10px;"></div>
		
		<div id="showprov"></div>
		
		<div style="clear: both; padding-top: 10px;"></div>
			
		<div style="float:left; display: inline; height: 10px;">
			<input type="checkbox" disabled />
		</div>
				
		<div style="float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;">
			Jumlah Pengajar
		</div>
			
		<div style="float:left; display: inline; padding-left: 10px;">
			<select name="nlecture" id="nlecture" onchange="submitnlecture(this.value)">
				<option value="">-= Pilih Salah Satu =-</option>
				<?php echo $lecture; ?>
			</select>
		</div>
							
		<div style="clear: both; padding-top: 10px;"></div>
			
		<div id="lecture"></div>
				
		<div style="clear: both; padding-top: 10px;"></div>
		
		<input type="submit" value="Simpan Data" name="diklat1-submit" />
	</div>
</form>
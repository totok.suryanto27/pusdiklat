<div class="content-title"><h1>Calon Peserta Diklat BKPM</h1></div>
<div>
<fieldset>
	<legend>Filter Berdasarkan</legend>
	<div class="filter-label">Diklat yang diikuti</div>
	<div style="float: left; padding-top: 7px; width: 20px;">:</div>
	<div class="filter-input">
		<select name="filter-diklat" id="filter-diklat" style="width:500px;" onchange="showdatacandidate('1',document.getElementById('searchtext').value)">
			<option value="">Tampilkan Semua</option>
			<?php
				$this->db->select('*');
				$this->db->from('jadwal');
				$this->db->group_by("DikID");
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row){
					$this->db->select('*');
					$this->db->from('activity');
					$this->db->where('id',$row->DikID);
					$result=$this->db->get();
					$result=$result->result();
					foreach($result as $row2){
						$actname=$row2->activity;
					}
					echo "<option value=\"".$row->DikID."\">".$actname."</option>";
				}
			?>
		</select>
	</div>
			
	<div style="clear: both;"></div>
	<div class="filter-label">Angkatan</div>
	<div style="float: left; padding-top: 7px; width: 20px;">:</div>
	<div class="filter-input">
		<select name="filter-angk" id="filter-angk" style="width:180px;" onchange="showdatacandidate('1',document.getElementById('searchtext').value)">
			<option value="">Tampilkan Semua</option>
			<?php
				$this->db->select('*');
				$this->db->from('jadwal');
				$this->db->group_by("ClassDik");
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row){
					echo "<option value=\"".$row->ClassDik."\">".$row->ClassDik."</option>";
				}
			?>
		</select>
	</div>
	
	<div style="clear: both;"></div>
	<div class="filter-label">Tahun</div>
	<div style="float: left; padding-top: 7px; width: 20px;">:</div>
	<div class="filter-input">
		<select name="filter-tahun" id="filter-tahun" style="width:180px;" onchange="showdatacandidate('1',document.getElementById('searchtext').value)">
			<option value="">Tampilkan Semua</option>
			<?php
				$this->db->select('*');
				$this->db->from('datapeserta');
				$this->db->group_by("tahun");
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row){
					$dummy=$row->tahun;
					$dummy=explode(";",$dummy);
					$tahun=$dummy[1];
					echo "<option value=\"".$row->tahun."\">".$tahun."</option>";
				}
			?>
		</select>
	</div>
	
	<div style="clear: both;"></div>
	<div class="filter-label">Nama</div>
	<div style="float: left; padding-top: 7px; width: 20px;">:</div>
	<div class="filter-input">
		<input type="text" name="searchtext" id="searchtext" size="70" onkeyup="showdatacandidate('1',document.getElementById('searchtext').value)"/>
	</div>
	<div style="clear: both;"></div>
	<div class="filter-label">
		<select id="searchcity" name="searchcity" onclick="ajaxfunc('start&type=showcity&id='+this.value, 'city', '<?=base_url(); ?>public/ajax/ajax2.php')">
			<option value="0">Tampilkan Semua</option>
			<option value="1">Kota</option>
			<option value="2">Kabupaten</option>
			<option value="3">Provinsi</option>
		</select>
	</div>
	<div style="float: left; padding-top: 7px; width: 20px;">:</div>
	<div class="filter-input" id="city">
	<input type="hidden" id="kota" value="" />
	</div>
		
	<div style="clear: both;"></div>
</fieldset>

<div style="clear: both; margin-bottom: 20px;"></div>

<div id="showdata">
	<?php
		$this->db->select('*');
		$this->db->from('datapeserta');
		$result=$this->db->get();
		$ndata=$result->num_rows();
		$itemperpage=20;
		$totalpage=ceil($ndata/$itemperpage);
		
		if($ndata!=0){
			
			# Halaman #
			echo "<div class=\"pagination\">";
			echo "<span class=\"pg-selected\" >Prev</span>";
			$i=1;
			$status="";
			$status_="";
			$status__="";
			$batas=5;
			while($i<=$totalpage){
				if($i!=1){
					if($totalpage>=10){
						if($i<=$batas || $i>$totalpage-2){
							echo "<span class=\"pg-normal\" onclick=\"showdatacandidate('".$i."',document.getElementById('searchtext').value)\">".$i."</span>";
						} else {
							if($status==""){
								$status="ada";
								echo " ... ";
							}
						}
					} else {
						echo "<span class=\"pg-normal\" onclick=\"showdatacandidate('".$i."',document.getElementById('searchtext').value)\">".$i."</span>";
					}
				} else {
					echo "<span class=\"pg-selected\" >".$i."</span>";
				}
				
				$i++;
			}
			
			
			if($totalpage!=1){
				echo "<span class=\"pg-normal\" onclick=\"showdatacandidate('2',document.getElementById('searchtext').value)\">Next</span>";
			} else {
				echo "<span class=\"pg-selected\" >Next</span>";
			}
			echo "<span class=\"pg-normal\" onclick=\"document.location='http://pusdiklat.bkpm.go.id/asset/export.php?iddik=&idakt='\">Export ke xls</span></div>";
			echo "<div style=\"padding-top: 20px;\"></div>";
			# Halaman #
			
			# Tampilan Data
			echo "
			<div class=\"jtable-main-container\">
				<div class=\"jtable-busy-panel-background\" style=\"display: none; width: 940px; height: 123px;\"></div>
				<div class=\"jtable-busy-message\" style=\"display: none;\"></div>
				<div class=\"jtable-title\">
					<div class=\"jtable-title-text\">
						Daftar Calon Peserta
					</div>
				</div>
				
				<table class=\"jtable\">
					<thead>
						<tr>
							<th class=\"jtable-column-header\" style=\"width: 5%;\">
								<div class=\"jtable-column-header-container\" align=\"center\">No</div>
							</th>
							<th class=\"jtable-column-header\" style=\"width: 20%;\">
								<div class=\"jtable-column-header-container\" align=\"center\">Nama</div>
							</th>
							<th class=\"jtable-column-header\" style=\"width: 20%;\">
								<div class=\"jtable-column-header-container\" align=\"center\">NIP</div>
							</th>
							<th class=\"jtable-column-header\" style=\"width: 20%;\">
								<div class=\"jtable-column-header-container\" align=\"center\">Instansi</div>
							</th>
							<th class=\"jtable-column-header\" style=\"width: 20%;\">
								<div class=\"jtable-column-header-container\" align=\"center\">Diklat & Angkatan</div>
							</th>
							<th class=\"jtable-column-header\" style=\"width: 9%;\">
								<div class=\"jtable-column-header-container\" align=\"center\">Status</div>
							</th>
							<th class=\"jtable-column-header\" style=\"width: 6%;\">
								<div class=\"jtable-column-header-container\" align=\"center\">Aksi</div>
							</th>
						</tr>
					</thead>
					<tbody>";
					
			$this->db->select('*');
			$this->db->from('datapeserta');
			$this->db->order_by('diklat','ASC');
			$this->db->limit($itemperpage,0);
			$this->db->order_by('AngkatanNumber','ASC');
			$result=$this->db->get();
			$result=$result->result();
			$i=1;
			$k=1;
			foreach($result as $row){
				$pos=1;
				$strangk=explode(";",$row->angkatan);
				$strdiklat=explode(";",$row->diklat);
				$strinstansi=explode(";",$row->instansi);
				$strwilayah=explode(";",$row->wilayah);
				$strjabatan=explode(";",$row->jabatan);
				$strbagian=explode(";",$row->bagian);

				if($i==2*$k-1){ 
					$even=" jtable-row-even";
					$k++;
				} else { 
					$even=""; 
				}
								
				// Data Instansi
				$this->db->select('*');
				$this->db->from('dept');
				$this->db->where('id',$strinstansi[$pos]);
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row2){
					$dept=$row2->dept;
				}
				
				// Data Diklat
				$this->db->select('*');
				$this->db->from('activity');
				$this->db->where('id',$strdiklat[$pos]);
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row2){
					$diklatname=$row2->activity;
				}
				$diklatnakt=$diklatname." Angkatan ".$strangk[$pos];
								
				// Status
				if($row->confirmation==0){ $status="Belum Konfirmasi"; }
				if($row->confirmation==1){ $status="Sudah Konfirmasi"; }
								
				echo "<tr class=\"jtable-data-row".$even."\">
						<td><div align=\"center\">".$i."</div></td>
						<td><a href=\"\" onClick=\"javascript:window.open('http://pusdiklat.bkpm.go.id/asset/detail.php?id=".$row->id."','Windows','width=800,height=550,toolbar=no,menubar=no,scrollbars=yes,resizable=no,location=no,directories=no,status=no');return false\">".$row->nama."</a></td>
						<td>".$row->nip."</td>
						<td>".$dept." ".$strwilayah[0]." ".$strwilayah[1]."</td>
						<td><div align=\"center\">".$diklatnakt."</center></td>
						<td><div align=\"center\">".$status."</div></td>
						<td><div align=\"center\"><img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row->nama." dari database?')){document.location='../siad/delete/?post=".$row->id."'; }\" /></td>
					</tr>";
				$i++;
			}

			
			echo"
					</tbody>
				</table>
			</div>";
			
			
			# Tampilan Data
			
			# Halaman #
			echo "<div class=\"pagination\" style=\"padding-top: 20px;\">";
			echo "<span class=\"pg-selected\" >Prev</span>";
			$i=1;
			$status="";
			$status_="";
			$status__="";
			while($i<=$totalpage){
				if($i!=1){
					if($totalpage>=10){
						if($i<=$batas || $i>$totalpage-2){
							echo "<span class=\"pg-normal\" onclick=\"showdatacandidate('".$i."',document.getElementById('searchtext').value)\">".$i."</span>";
						} else {
							if($status==""){
								$status="ada";
								echo " ... ";
							}
						}
					} else {
						echo "<span class=\"pg-normal\" onclick=\"showdatacandidate('".$i."',document.getElementById('searchtext').value)\">".$i."</span>";
					}
				} else {
					echo "<span class=\"pg-selected\" >".$i."</span>";
				}
				
				$i++;
			}
			
			
			if($totalpage!=1){
				echo "<span class=\"pg-normal\" onclick=\"showdatacandidate('2',document.getElementById('searchtext').value)\">Next</span>";
			} else {
				echo "<span class=\"pg-selected\" >Next</span>";
			}
			echo "<span class=\"pg-normal\" onclick=\"document.location='http://pusdiklat.bkpm.go.id/asset/export.php?iddik=&idakt='\">Export ke xls</span></div>";
			# Halaman #
		}
	?>
</div>


</div>
<!--<div align="center">
	<h1>Selamat Datang di</h1>
	<h2>Sistem Informasi Alumni Diklat</h2>
	<h2>BKPM</h2>
	<img src="<?php echo base_url(); ?>public/siad/images/siadbkpm_big.png" width="300px" />
</div>-->
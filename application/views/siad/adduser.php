<div style="border-bottom: 1px #fff solid; margin-bottom: 20px;">
	<span style="font-weight: bold;"><h1>Tambah Pengguna (User)</h1></span>
</div>
<?php
	$error=$this->session->userdata('warning');
	$warning=str_replace('<p>','<li>',$error);
	$warning=str_replace('</p>','</li>',$warning);
	if(isset($error) && $error){
		echo "
			<div id=\"tips\" >
				<ul>".$warning."</ul>
			</div>";
	}
	if($this->session->userdata('warning')!=""){
		$this->session->set_userdata('warning','');
	}
?>
Lengkapi informasi berikut:
<form method="post" action="" enctype="multipart/form-data">
	<div style="margin-top: 10px;">
		<div style="float:left; display: inline; height: 10px;">
			<input type="checkbox" disabled />
		</div>
				
		<div style="float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;">
			Nama Pengguna (User Name)
		</div>
				
		<div style="float:left; display: inline; padding-left: 10px;">
			<input type="text" autocomplete="off" name="user-name" id="user-name" size="70" value=""/>
		</div>
							
		<div style="clear: both; padding-top: 10px;"></div>
		
		<div style="float:left; display: inline; height: 10px;">
			<input type="checkbox" disabled />
		</div>
				
		<div style="float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;">
			Nama Asli Pengguna (Real Name)
		</div>
				
		<div style="float:left; display: inline; padding-left: 10px;">
			<input type="text" autocomplete="off" name="real-name" id="real-name" size="70" value=""/>
		</div>
		
		<div style="clear: both; padding-top: 10px;"></div>
		
		<div style="float:left; display: inline; height: 10px;">
			<input type="checkbox" disabled />
		</div>
				
		<div style="float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;">
			Password
		</div>
				
		<div style="float:left; display: inline; padding-left: 10px;">
			<input type="password" autocomplete="off" name="password" id="password" size="70" value=""/>
		</div>
		
		<div style="clear: both; padding-top: 10px;"></div>
		
		<div style="float:left; display: inline; height: 10px;">
			<input type="checkbox" disabled />
		</div>
				
		<div style="float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;">
			Password (Ulangi)
		</div>
				
		<div style="float:left; display: inline; padding-left: 10px;">
			<input type="password" autocomplete="off" name="password-repeat" id="password-repeat" size="70" value=""/>
		</div>
		
		<div style="clear: both; padding-top: 10px;"></div>
		
		<input type="submit" value="Simpan Data" name="user-submit" />
	</div>
</form>
<div style="border-bottom: 1px #fff solid; margin-bottom: 20px;">
	<span style="font-weight: bold;"><h1>Tambah Kota/Kabupaten</h1></span>
</div>
<?php
	$error=$this->session->userdata('warning');
	$warning=str_replace('<p>','<li>',$error);
	$warning=str_replace('</p>','</li>',$warning);
	if(isset($error) && $error){
		echo "
			<div id=\"tips\" >
				<ul>".$warning."</ul>
			</div>";
	}
	if($this->session->userdata('warning')!=""){
		$this->session->set_userdata('warning','');
	}
?>
Lengkapi informasi berikut:
<form method="post" action="" enctype="multipart/form-data">
	<div style="margin-top: 10px;">
		<div style="float:left; display: inline; height: 10px;">
			<input type="checkbox" disabled />
		</div>
				
		<div style="float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;">
			Nama Kota/Kabupaten
		</div>
				
		<div style="float:left; display: inline; padding-left: 10px;">
			<input type="text" autocomplete="off" name="city-name" id="city-name" size="70" value=""/>
		</div>
							
		<div style="clear: both; padding-top: 10px;"></div>
		
		<div style="float:left; display: inline; height: 10px;">
			<input type="checkbox" disabled />
		</div>
				
		<div style="float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;">
			Provinsi
		</div>
				
		<div style="float:left; display: inline; padding-left: 10px;">
			<select id="prov" name="prov" onchange="showProvOptional(this.value)">
				<option value="">-= Pilih Salah Satu =-</option>
				<?php
					$query=@mysql_query("SELECT * FROM city WHERE 1 GROUP BY provinsi");
					$prov="";
					while($row=@mysql_fetch_array($query)){
						$prov=$prov."<option value=\"".$row['provinsi']."\">".$row['provinsi']."</option>";
					}
					echo $prov;
				?>
				<option value="lainnya">Lainnya ...</option>
			</select> * Bila tidak ada dalam list, pilih <em><strong>Lainnya</strong></em>
		</div>
							
		<div style="clear: both; padding-top: 10px;"></div>
		
		<div id="provopt"></div>
				
		<div style="clear: both; padding-top: 10px;"></div>
				
		<div style="float:left; display: inline; height: 10px;">
			<input type="checkbox" disabled />
		</div>
				
		<div style="float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;">
			Wilayah Administratif
		</div>
				
		<div style="float:left; display: inline; padding-left: 10px; vertical-align: text-top;">
			<input type="radio" id="tipe1" name="tipe" value="provinsi" /> Ibu Kota Provinsi<br />
			<input type="radio" id="tipe2" name="tipe" value="kota" /> Kota<br />
			<input type="radio" id="tipe3" name="tipe" value="kabupaten" /> Kabupaten<br />
		</div>
							
		<div style="clear: both; padding-top: 10px;"></div>
			
		<input type="submit" value="Simpan Data" name="city-submit" />
	</div>
</form>
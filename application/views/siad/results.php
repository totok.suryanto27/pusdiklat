<?php
	if($this->input->post('submit')==""){
		echo"<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."siad/report\" />";
		exit;
	}
	$i=1;
	$resultsreport="";
	$sql="";
	if($this->input->post('chk_diklat')=="on"){
		$this->db->select('*');
		$this->db->from('activity');
		$this->db->where('id',$this->input->post('sel_diklat'));
		$result=$this->db->get();
		$result=$result->result();
		foreach($result as $row_diklat){
			$resultsreport=$resultsreport."<span style=\"color: #00FF00;\"><strong>".$i.". Diklat: ".$row_diklat->activity."</strong></span><br />";
		}
		if($i==1){
			$sql=$sql." diklat LIKE '%;".$this->input->post('sel_diklat').";%'";
		} else {
			$sql=$sql." AND diklat LIKE '%;".$this->input->post('sel_diklat').";%'";
		}
		$i++;
	}
	
	if($this->input->post('chk_angkatan')=="on"){
		$resultsreport=$resultsreport."<span style=\"color: #00FF00;\"><strong>".$i.". Angkatan: ".$this->input->post('sel_angkatan')."</strong></span><br />";
		if($i==1){
			$sql=$sql." angkatan LIKE '%;".$this->input->post('sel_angkatan').";%'";
		} else {
			$sql=$sql." AND angkatan LIKE '%;".$this->input->post('sel_angkatan').";%'";
		}
		$i++;
	}
	
	if($this->input->post('chk_tahun')=="on"){
		$resultsreport=$resultsreport."<span style=\"color: #00FF00;\"><strong>".$i.". Tahun: ".$this->input->post('sel_tahun')."</strong></span><br />";
		if($i==1){
			$sql=$sql." tahun LIKE '%;".$this->input->post('sel_tahun').";%'";
		} else {
			$sql=$sql." AND tahun LIKE '%;".$this->input->post('sel_tahun').";%'";
		}
		$i++;
	}
	
	if($this->input->post('chk_jabatan')=="on"){
		$this->db->select('*');
		$this->db->from('position');
		$this->db->where('id',$this->input->post('sel_jabatan'));
		$result=$this->db->get();
		$result=$result->result();
		foreach($result as $row_jabatan){
			$resultsreport=$resultsreport."<span style=\"color: #00FF00;\"><strong>".$i.". Pangkat: ".$row_jabatan->position."</strong></span><br />";
		}
		if($i==1){
			$sql=$sql." jabatan LIKE '%;".$this->input->post('sel_jabatan').";%'";
		} else {
			$sql=$sql." AND jabatan LIKE '%;".$this->input->post('sel_jabatan').";%'";
		}
		$i++;
	}
	
	if($this->input->post('chk_instansi')=="on"){
		$this->db->select('*');
		$this->db->from('dept');
		$this->db->where('id',$this->input->post('sel_instansi'));
		$result=$this->db->get();
		$result=$result->result();
		foreach($result as $row_instansi){
			$resultsreport=$resultsreport."<span style=\"color: #00FF00;\"><strong>".$i.". Instansi: ".$row_instansi->dept."</strong></span><br />";
		}
		if($i==1){
			$sql=$sql." instansi LIKE '%;".$this->input->post('sel_instansi').";%'";
		} else {
			$sql=$sql." AND instansi LIKE '%;".$this->input->post('sel_instansi').";%'";
		}
		$i++;
	}
	
	if($this->input->post('rad_wilayah')=="kota"){
		if($this->input->post('sel_kota')!="all"){
			$this->db->select('*');
			$this->db->from('city');
			$this->db->where('id',$this->input->post('sel_kota'));
			$result=$this->db->get();
			$result=$result->result();
			foreach($result as $row_kota){
				$resultsreport=$resultsreport."<span style=\"color: #00FF00;\"><strong>".$i.". Kota: ".$row_kota->city."</strong></span><br />";
			}
			if($i==1){
				$sql=$sql." wilayah LIKE '%Kota;".$row_kota->city.";%'";
			} else {
				$sql=$sql." AND wilayah LIKE '%Kota;".$row_kota->city.";%'";
			}
		} else {
			$resultsreport=$resultsreport."<span style=\"color: #00FF00;\"><strong>".$i.". Kota: Semua Kota</strong></span><br />";
			if($i==1){
				$sql=$sql." wilayah LIKE '%Kota;%'";
			} else {
				$sql=$sql." AND wilayah LIKE '%Kota;%'";
			}
		}
		$i++;
	}
	
	if($this->input->post('rad_wilayah')=="kabupaten"){
		if($this->input->post('sel_kabupaten')!="all"){
			$this->db->select('*');
			$this->db->from('city');
			$this->db->where('id',$this->input->post('sel_kabupaten'));
			$result=$this->db->get();
			$result=$result->result();
			foreach($result as $row_kota){
				$resultsreport=$resultsreport."<span style=\"color: #00FF00;\"><strong>".$i.". Kabupaten: ".$row_kota->city."</strong></span><br />";
			}
			if($i==1){
				$sql=$sql." wilayah LIKE '%Kabupaten;".$row_kota->city.";%'";
			} else {
				$sql=$sql." AND wilayah LIKE '%Kabupaten;".$row_kota->city.";%'";
			}
		} else {
			$resultsreport=$resultsreport."<span style=\"color: #00FF00;\"><strong>".$i.". Kabupaten: Semua Kabupaten</strong></span><br />";
			if($i==1){
				$sql=$sql." wilayah LIKE '%Kabupaten;%'";
			} else {
				$sql=$sql." AND wilayah LIKE '%Kabupaten;%'";
			}
		}
		$i++;
	}
	
	if($this->input->post('rad_wilayah')=="provinsi"){
		if($this->input->post('sel_provinsi')!="all"){
			$resultsreport=$resultsreport."<span style=\"color: #00FF00;\"><strong>".$i.". Provinsi: ".$this->input->post('sel_provinsi')."</strong></span><br />";
			if($i==1){
				$sql=$sql." wilayah LIKE '%Provinsi;".$this->input->post('sel_provinsi').";%'";
			} else {
				$sql=$sql." AND wilayah LIKE '%Provinsi;".$this->input->post('sel_provinsi').";%'";
			}
		} else {
			$resultsreport=$resultsreport."<span style=\"color: #00FF00;\"><strong>".$i.". Provinsi: Semua Provinsi</strong></span><br />";
			if($i==1){
				$sql=$sql." wilayah LIKE '%Provinsi;%'";
			} else {
				$sql=$sql." AND wilayah LIKE '%Provinsi;%'";
			}
		}
		$i++;
	}
	
	if($this->input->post('chk_name')=="on"){
		$resultsreport=$resultsreport."<span style=\"color: #00FF00;\"><strong>".$i.". Nama Alumni: ".$this->input->post('sel_name')."</strong></span><br />";
		if($i==1){
			$sql=$sql." nama LIKE '%".$this->input->post('sel_name')."%'";
		} else {
			$sql=$sql." AND nama LIKE '%".$this->input->post('sel_name')."%'";
		}
		$i++;
	}
	$total=$i-1;
	
?>
<div style="border-bottom: 1px #fff solid; margin-bottom: 20px;">
	<span style="font-weight: bold;"><h1>Hasil Pencarian</h1></span>
	Kriteria Pencarian:<br />
	<?php echo $resultsreport; ?>
</div>

<div id="showdata">
<?php
	# Untuk mendapatkan banyaknya data masing-masing kriteria yang diberikan
	//echo "SELECT * FROM dataalumni WHERE ".$sql;
	$query=$this->db->query("SELECT * FROM dataalumni WHERE ".$sql);
	$result=$query->result();
	$total_alumni=0;
	$j=0;
	$k=1;
	$l=1;
	$content="";
	$count_alumni=0;
	foreach($result as $row_alumni){
		$str_diklat=explode(";",$row_alumni->diklat);
		$str_angkatan=explode(";",$row_alumni->angkatan);
		$str_tahun=explode(";",$row_alumni->tahun);
		$str_pangkat=explode(";",$row_alumni->pangkat);
		$str_wilayah=explode(";",$row_alumni->wilayah);
		$str_instansi=explode(";",$row_alumni->instansi);
		$str_kota=explode(";",$row_alumni->kantor_kota);
		$str_provinsi=explode(";",$row_alumni->kantor_prov);
		$tipe=explode(";",$row_alumni->wilayah);
		$str_jabatan=explode(";",$row_alumni->jabatan);
		$str_bagian=explode(";",$row_alumni->bagian);
		
		
		# Bagian untuk mendapatkan total data berdasarkan kriteria
		if($total==1){

			if($this->input->post('sel_diklat')!=""){
				$i=1;
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat')){
						
						
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_angkatan')!=""){
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan')){
						
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						
						$count_alumni++;
						$pos=$i;
					}
					$i++;
				}
			}

			if($this->input->post('sel_tahun')!=""){
				$i=1;
				
				while($str_tahun[$i]!=""){
					if($str_tahun[$i]==$this->input->post('sel_tahun')){
						
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_jabatan')!=""){
				$i=1;
				
				while($str_jabatan[$i]!=""){
					if($str_jabatan[$i]==$this->input->post('sel_jabatan')){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_instansi')!=""){
				$i=1;
				
				while($str_instansi[$i]!=""){
					if($str_instansi[$i]==$this->input->post('sel_instansi')){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_kota')!=""){
				if($this->input->post('sel_kota')!="all"){
					$this->db->select('*');
					$this->db->from('city');
					$this->db->where('id',$this->input->post('sel_kota'));
					$result=$this->db->get();
					$result=$result->result();
					foreach($result as $row_kota){
						$city=$row_kota->city;
					}
		
					$i=1;
					
					$limitcity=0;
					while($str_diklat[$i]!=""){
						if($tipe[$i]==$city && $tipe[$i-1]=="Kota"){
							$limitcity++;
							if($limitcity==1){
								if($k<=20){
									if($k==2*$l-1){ 
										$even=" jtable-row-even";
										$l++;
									} else { 
										$even=""; 
									}
									
									$this->db->select('*');
									$this->db->from('dept');
									$this->db->where('id',$str_instansi[$i]);
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row2){
										$dept=$row2->dept;
									}
									
									$this->db->select('*');
									$this->db->from('activity');
									$this->db->where('id',$str_diklat[$i]);
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row2){
										$diklatname=$row2->activity;
									}
									$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
									
									// Data Jabatan
									$this->db->select('*');
									$this->db->from('position');
									$this->db->where('id',$str_jabatan[$i]);
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row2){
										$jabatan=$row2->position;
									}
									
									if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
										$jabatan=$jabatan." ".$str_bagian[$i];
									}
									
									$content=$content.
									"<tr class=\"jtable-data-row".$even."\">
										<td><div align=\"center\">".$k."</div></td>
										<td>".$row_alumni->nama."</td>
										<td><div align=\"center\">".$row_alumni->nip."</div></td>
										<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
										<td><div align=\"center\">".$diklatnakt."</center></td>
										<td><div align=\"center\">".$jabatan."</div></td>
										<td><div align=\"center\">
											<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
											<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
											</div>
										</td>
									</tr>";
								}
								
								
								$k++;
								$count_alumni++;
							}
						}
						$i++;
					}
				} else {
					$i=1;
					
					$limitcity=0;
					while($str_diklat[$i]!=""){
						if($tipe[$i-1]=="Kota"){
							$limitcity++;
							if($limitcity==1){
								if($k<=20){
									if($k==2*$l-1){ 
										$even=" jtable-row-even";
										$l++;
									} else { 
										$even=""; 
									}
									
									$this->db->select('*');
									$this->db->from('dept');
									$this->db->where('id',$str_instansi[$i]);
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row2){
										$dept=$row2->dept;
									}
									
									$this->db->select('*');
									$this->db->from('activity');
									$this->db->where('id',$str_diklat[$i]);
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row2){
										$diklatname=$row2->activity;
									}
									$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
									
									// Data Jabatan
									$this->db->select('*');
									$this->db->from('position');
									$this->db->where('id',$str_jabatan[$i]);
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row2){
										$jabatan=$row2->position;
									}
									
									if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
										$jabatan=$jabatan." ".$str_bagian[$i];
									}
									
									$content=$content.
									"<tr class=\"jtable-data-row".$even."\">
										<td><div align=\"center\">".$k."</div></td>
										<td>".$row_alumni->nama."</td>
										<td><div align=\"center\">".$row_alumni->nip."</div></td>
										<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
										<td><div align=\"center\">".$diklatnakt."</center></td>
										<td><div align=\"center\">".$jabatan."</div></td>
										<td><div align=\"center\">
											<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
											<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
											</div>
										</td>
									</tr>";
								}
								
								
								$k++;
								$count_alumni++;
							}
						}
						$i++;
					}
				}
				
			}
			
			if($this->input->post('sel_kabupaten')!=""){
				if($this->input->post('sel_kabupaten')!="all"){
					$this->db->select('*');
					$this->db->from('city');
					$this->db->where('id',$this->input->post('sel_kabupaten'));
					$result=$this->db->get();
					$result=$result->result();
					foreach($result as $row_kota){
						$city=$row_kota->city;
					}
					$i=1;
					
					$limitcity=0;
					while($str_diklat[$i]!=""){
						if($tipe[$i]==$city && $tipe[$i-1]=="Kabupaten"){
							$limitcity++;
							if($limitcity==1){
								if($k<=20){
									if($k==2*$l-1){ 
										$even=" jtable-row-even";
										$l++;
									} else { 
										$even=""; 
									}
									
									$this->db->select('*');
									$this->db->from('dept');
									$this->db->where('id',$str_instansi[$i]);
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row2){
										$dept=$row2->dept;
									}
									
									$this->db->select('*');
									$this->db->from('activity');
									$this->db->where('id',$str_diklat[$i]);
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row2){
										$diklatname=$row2->activity;
									}
									$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
									
									// Data Jabatan
									$this->db->select('*');
									$this->db->from('position');
									$this->db->where('id',$str_jabatan[$i]);
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row2){
										$jabatan=$row2->position;
									}
									
									if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
										$jabatan=$jabatan." ".$str_bagian[$i];
									}
									
									$content=$content.
									"<tr class=\"jtable-data-row".$even."\">
										<td><div align=\"center\">".$k."</div></td>
										<td>".$row_alumni->nama."</td>
										<td><div align=\"center\">".$row_alumni->nip."</div></td>
										<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
										<td><div align=\"center\">".$diklatnakt."</center></td>
										<td><div align=\"center\">".$jabatan."</div></td>
										<td><div align=\"center\">
											<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
											<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
											</div>
										</td>
									</tr>";
								}
								
								
								$k++;
								$count_alumni++;
							}
						}
						$i++;
					}
				} else {
					$i=1;
					
					$limitcity=0;
					while($str_diklat[$i]!=""){
						if($tipe[$i-1]=="Kabupaten"){
							$limitcity++;
							if($limitcity==1){
								if($k<=20){
									if($k==2*$l-1){ 
										$even=" jtable-row-even";
										$l++;
									} else { 
										$even=""; 
									}
									
									$this->db->select('*');
									$this->db->from('dept');
									$this->db->where('id',$str_instansi[$i]);
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row2){
										$dept=$row2->dept;
									}
									
									$this->db->select('*');
									$this->db->from('activity');
									$this->db->where('id',$str_diklat[$i]);
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row2){
										$diklatname=$row2->activity;
									}
									$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
									
									// Data Jabatan
									$this->db->select('*');
									$this->db->from('position');
									$this->db->where('id',$str_jabatan[$i]);
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row2){
										$jabatan=$row2->position;
									}
									
									if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
										$jabatan=$jabatan." ".$str_bagian[$i];
									}
									
									$content=$content.
									"<tr class=\"jtable-data-row".$even."\">
										<td><div align=\"center\">".$k."</div></td>
										<td>".$row_alumni->nama."</td>
										<td><div align=\"center\">".$row_alumni->nip."</div></td>
										<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
										<td><div align=\"center\">".$diklatnakt."</center></td>
										<td><div align=\"center\">".$jabatan."</div></td>
										<td><div align=\"center\">
											<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
											<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
											</div>
										</td>
									</tr>";
								}
								
								
								$k++;
								$count_alumni++;
							}
						}
						$i++;
					}
				}
			}
			
			if($this->input->post('sel_provinsi')!=""){
				if($this->input->post('sel_provinsi')!="all"){
					$i=1;
					
					$limitcity=0;
					while($str_diklat[$i]!=""){
						if($tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi"){
							$limitcity++;
							if($limitcity==1){
								if($k<=20){
									if($k==2*$l-1){ 
										$even=" jtable-row-even";
										$l++;
									} else { 
										$even=""; 
									}
									
									$this->db->select('*');
									$this->db->from('dept');
									$this->db->where('id',$str_instansi[$i]);
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row2){
										$dept=$row2->dept;
									}
									
									$this->db->select('*');
									$this->db->from('activity');
									$this->db->where('id',$str_diklat[$i]);
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row2){
										$diklatname=$row2->activity;
									}
									$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
									
									// Data Jabatan
									$this->db->select('*');
									$this->db->from('position');
									$this->db->where('id',$str_jabatan[$i]);
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row2){
										$jabatan=$row2->position;
									}
									
									if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
										$jabatan=$jabatan." ".$str_bagian[$i];
									}
									
									$content=$content.
									"<tr class=\"jtable-data-row".$even."\">
										<td><div align=\"center\">".$k."</div></td>
										<td>".$row_alumni->nama."</td>
										<td><div align=\"center\">".$row_alumni->nip."</div></td>
										<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
										<td><div align=\"center\">".$diklatnakt."</center></td>
										<td><div align=\"center\">".$jabatan."</div></td>
										<td><div align=\"center\">
											<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
											<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
											</div>
										</td>
									</tr>";
								}
								
								
								$k++;
								$count_alumni++;
							}
						}
						$i++;
					}
				} else {
					$i=1;
					
					$limitcity=0;
					while($str_diklat[$i]!=""){
						if($tipe[$i-1]=="Provinsi"){
							$limitcity++;
							if($limitcity==1){
								if($k<=20){
									if($k==2*$l-1){ 
										$even=" jtable-row-even";
										$l++;
									} else { 
										$even=""; 
									}
									
									$this->db->select('*');
									$this->db->from('dept');
									$this->db->where('id',$str_instansi[$i]);
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row2){
										$dept=$row2->dept;
									}
									
									$this->db->select('*');
									$this->db->from('activity');
									$this->db->where('id',$str_diklat[$i]);
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row2){
										$diklatname=$row2->activity;
									}
									$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
									
									// Data Jabatan
									$this->db->select('*');
									$this->db->from('position');
									$this->db->where('id',$str_jabatan[$i]);
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row2){
										$jabatan=$row2->position;
									}
									
									if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
										$jabatan=$jabatan." ".$str_bagian[$i];
									}
									
									$content=$content.
									"<tr class=\"jtable-data-row".$even."\">
										<td><div align=\"center\">".$k."</div></td>
										<td>".$row_alumni->nama."</td>
										<td><div align=\"center\">".$row_alumni->nip."</div></td>
										<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
										<td><div align=\"center\">".$diklatnakt."</center></td>
										<td><div align=\"center\">".$jabatan."</div></td>
										<td><div align=\"center\">
											<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
											<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
											</div>
										</td>
									</tr>";
								}
								
								
								$k++;
								$count_alumni++;
							}
						}
						$i++;
					}

				}
			}
			
			if($this->input->post('sel_name')!=""){
				
				if(preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama)){
					$i=0;
					while($str_diklat[$i]!=""){
						$i++;
					}
					$i=$i-1;
					
					if($k<=20){
						if($k==2*$l-1){ 
							$even=" jtable-row-even";
							$l++;
						} else { 
							$even=""; 
						}
						
						
						$diklatnakt="";
						$dept="";
						$jabatan="";
						$z=1;
						while($z<=$i){
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$z]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$dept."<p>".$row2->dept." ".$str_wilayah[2*$z-2]." ".$str_wilayah[2*$z-1]."</p>";
							}
								
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$z]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatnakt."<p>".$diklatname." Angkatan ".$str_angkatan[$z]." Tahun ".$str_tahun[$z]."</p>";
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$z]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan0=$row2->position;
							}
								
							if($str_bagian[$z]!="" || $str_bagian[$z]!=" "){
								$jabatan=$jabatan."<p>".$jabatan0." ".$str_bagian[$z]."</p>";
							}
							
							$z++;
						}
							
						$content=$content.
						"<tr class=\"jtable-data-row".$even."\">
							<td><div align=\"center\">".$k."</div></td>
							<td>".$row_alumni->nama."</td>
							<td><div align=\"center\">".$row_alumni->nip."</div></td>
							<td><div align=\"center\">".$dept."</div></td>
							<td><div align=\"center\">".$diklatnakt."</center></td>
							<td><div align=\"center\">".$jabatan."</div></td>
							<td><div align=\"center\">
								<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
								<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
								</div>
							</td>
						</tr>";
					}
						
						
					$k++;
					$count_alumni++;
				}
			}
						
		}
		
		if($total==2){
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!=""){ // Success
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan')){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_tahun')!=""){ // Success
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_tahun[$i]==$this->input->post('sel_tahun')){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_jabatan')!=""){ // Success
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_jabatan[$i]==$this->input->post('sel_jabatan')){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_instansi')!=""){ // Success
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_instansi[$i]==$this->input->post('sel_instansi')){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_kota')!=""){ // Success
				if($this->input->post('sel_kota')!="all"){
					$this->db->select('*');
					$this->db->from('city');
					$this->db->where('id',$this->input->post('sel_kota'));
					$this->db->where('tipe','Kota');
					$result=$this->db->get();
					$result=$result->result();
					foreach($result as $row_kota){
						$city=$row_kota->city;
					}
					$i=1;
					
					$limitcity=0;
					while($str_diklat[$i]!=""){
						if($str_diklat[$i]==$this->input->post('sel_diklat') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
							$limitcity++;
							if($limitcity==1){
								if($k<=20){
									if($k==2*$l-1){ 
										$even=" jtable-row-even";
										$l++;
									} else { 
										$even=""; 
									}
									
									$this->db->select('*');
									$this->db->from('dept');
									$this->db->where('id',$str_instansi[$i]);
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row2){
										$dept=$row2->dept;
									}
									
									$this->db->select('*');
									$this->db->from('activity');
									$this->db->where('id',$str_diklat[$i]);
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row2){
										$diklatname=$row2->activity;
									}
									$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
									
									// Data Jabatan
									$this->db->select('*');
									$this->db->from('position');
									$this->db->where('id',$str_jabatan[$i]);
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row2){
										$jabatan=$row2->position;
									}
									
									if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
										$jabatan=$jabatan." ".$str_bagian[$i];
									}
									
									$content=$content.
									"<tr class=\"jtable-data-row".$even."\">
										<td><div align=\"center\">".$k."</div></td>
										<td>".$row_alumni->nama."</td>
										<td><div align=\"center\">".$row_alumni->nip."</div></td>
										<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
										<td><div align=\"center\">".$diklatnakt."</center></td>
										<td><div align=\"center\">".$jabatan."</div></td>
										<td><div align=\"center\">
											<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
											<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
											</div>
										</td>
									</tr>";
								}
								
								
								$k++;
								$pos=$i;
								$count_alumni++;
							}
						}
						$i++;
					}
				} else {
					$i=1;
					
					$limitcity=0;
					while($str_diklat[$i]!=""){
						if($str_diklat[$i]==$this->input->post('sel_diklat') && $tipe[2*$i-2]=="Kota"){
							$limitcity++;
							if($limitcity==1){
								if($k<=20){
									if($k==2*$l-1){ 
										$even=" jtable-row-even";
										$l++;
									} else { 
										$even=""; 
									}
									
									$this->db->select('*');
									$this->db->from('dept');
									$this->db->where('id',$str_instansi[$i]);
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row2){
										$dept=$row2->dept;
									}
									
									$this->db->select('*');
									$this->db->from('activity');
									$this->db->where('id',$str_diklat[$i]);
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row2){
										$diklatname=$row2->activity;
									}
									$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
									
									// Data Jabatan
									$this->db->select('*');
									$this->db->from('position');
									$this->db->where('id',$str_jabatan[$i]);
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row2){
										$jabatan=$row2->position;
									}
									
									if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
										$jabatan=$jabatan." ".$str_bagian[$i];
									}
									
									$content=$content.
									"<tr class=\"jtable-data-row".$even."\">
										<td><div align=\"center\">".$k."</div></td>
										<td>".$row_alumni->nama."</td>
										<td><div align=\"center\">".$row_alumni->nip."</div></td>
										<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
										<td><div align=\"center\">".$diklatnakt."</center></td>
										<td><div align=\"center\">".$jabatan."</div></td>
										<td><div align=\"center\">
											<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
											<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
											</div>
										</td>
									</tr>";
								}
								
								
								$k++;
								$pos=$i;
								$count_alumni++;
							}
						}
						$i++;
					}
				}
			}
			
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_kabupaten')!=""){ // Success
				if($this->input->post('sel_kabupaten')!="all"){
					$this->db->select('*');
					$this->db->from('city');
					$this->db->where('id',$this->input->post('sel_kabupaten'));
					$this->db->where('tipe','Kabupaten');
					$result=$this->db->get();
					$result=$result->result();
					foreach($result as $row_kota){
						$city=$row_kota->city;
					}
					$i=1;
					
					while($str_diklat[$i]!=""){
						if($str_diklat[$i]==$this->input->post('sel_diklat') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
							if($k<=20){
								if($k==2*$l-1){ 
									$even=" jtable-row-even";
									$l++;
								} else { 
									$even=""; 
								}
								
								$this->db->select('*');
								$this->db->from('dept');
								$this->db->where('id',$str_instansi[$i]);
								$result=$this->db->get();
								$result=$result->result();
								foreach($result as $row2){
									$dept=$row2->dept;
								}
								
								$this->db->select('*');
								$this->db->from('activity');
								$this->db->where('id',$str_diklat[$i]);
								$result=$this->db->get();
								$result=$result->result();
								foreach($result as $row2){
									$diklatname=$row2->activity;
								}
								$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
								
								// Data Jabatan
								$this->db->select('*');
								$this->db->from('position');
								$this->db->where('id',$str_jabatan[$i]);
								$result=$this->db->get();
								$result=$result->result();
								foreach($result as $row2){
									$jabatan=$row2->position;
								}
								
								if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
									$jabatan=$jabatan." ".$str_bagian[$i];
								}
								
								$content=$content.
								"<tr class=\"jtable-data-row".$even."\">
									<td><div align=\"center\">".$k."</div></td>
									<td>".$row_alumni->nama."</td>
									<td><div align=\"center\">".$row_alumni->nip."</div></td>
									<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
									<td><div align=\"center\">".$diklatnakt."</center></td>
									<td><div align=\"center\">".$jabatan."</div></td>
									<td><div align=\"center\">
										<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
										<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
										</div>
									</td>
								</tr>";
							}
							
							
							$k++;
							$pos=$i;
							$count_alumni++;
						}
						$i++;
					}
				} else {
					$i=1;
					
					while($str_diklat[$i]!=""){
						if($str_diklat[$i]==$this->input->post('sel_diklat') && $tipe[2*$i-2]=="Kabupaten"){
							if($k<=20){
								if($k==2*$l-1){ 
									$even=" jtable-row-even";
									$l++;
								} else { 
									$even=""; 
								}
								
								$this->db->select('*');
								$this->db->from('dept');
								$this->db->where('id',$str_instansi[$i]);
								$result=$this->db->get();
								$result=$result->result();
								foreach($result as $row2){
									$dept=$row2->dept;
								}
								
								$this->db->select('*');
								$this->db->from('activity');
								$this->db->where('id',$str_diklat[$i]);
								$result=$this->db->get();
								$result=$result->result();
								foreach($result as $row2){
									$diklatname=$row2->activity;
								}
								$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
								
								// Data Jabatan
								$this->db->select('*');
								$this->db->from('position');
								$this->db->where('id',$str_jabatan[$i]);
								$result=$this->db->get();
								$result=$result->result();
								foreach($result as $row2){
									$jabatan=$row2->position;
								}
								
								if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
									$jabatan=$jabatan." ".$str_bagian[$i];
								}
								
								$content=$content.
								"<tr class=\"jtable-data-row".$even."\">
									<td><div align=\"center\">".$k."</div></td>
									<td>".$row_alumni->nama."</td>
									<td><div align=\"center\">".$row_alumni->nip."</div></td>
									<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
									<td><div align=\"center\">".$diklatnakt."</center></td>
									<td><div align=\"center\">".$jabatan."</div></td>
									<td><div align=\"center\">
										<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
										<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
										</div>
									</td>
								</tr>";
							}
							
							
							$k++;
							$pos=$i;
							$count_alumni++;
						}
						$i++;
					}

				}
			}
			
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_provinsi')!=""){ // Success
				if($this->input->post('sel_provinsi')!="all"){
					$i=1;
					
					while($str_diklat[$i]!=""){
						if($str_diklat[$i]==$this->input->post('sel_diklat') && $tipe[2*$i-1]==$this->input->post('sel_provinsi') && $tipe[2*$i-2]=="Provinsi"){
							if($k<=20){
								if($k==2*$l-1){ 
									$even=" jtable-row-even";
									$l++;
								} else { 
									$even=""; 
								}
								
								$this->db->select('*');
								$this->db->from('dept');
								$this->db->where('id',$str_instansi[$i]);
								$result=$this->db->get();
								$result=$result->result();
								foreach($result as $row2){
									$dept=$row2->dept;
								}
								
								$this->db->select('*');
								$this->db->from('activity');
								$this->db->where('id',$str_diklat[$i]);
								$result=$this->db->get();
								$result=$result->result();
								foreach($result as $row2){
									$diklatname=$row2->activity;
								}
								$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
								
								// Data Jabatan
								$this->db->select('*');
								$this->db->from('position');
								$this->db->where('id',$str_jabatan[$i]);
								$result=$this->db->get();
								$result=$result->result();
								foreach($result as $row2){
									$jabatan=$row2->position;
								}
								
								if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
									$jabatan=$jabatan." ".$str_bagian[$i];
								}
								
								$content=$content.
								"<tr class=\"jtable-data-row".$even."\">
									<td><div align=\"center\">".$k."</div></td>
									<td>".$row_alumni->nama."</td>
									<td><div align=\"center\">".$row_alumni->nip."</div></td>
									<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
									<td><div align=\"center\">".$diklatnakt."</center></td>
									<td><div align=\"center\">".$jabatan."</div></td>
									<td><div align=\"center\">
										<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
										<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
										</div>
									</td>
								</tr>";
							}
							
							
							$k++;
							$pos=$i;
							$count_alumni++;
						}
						$i++;
					}
				} else {
					$i=1;
					
					while($str_diklat[$i]!=""){
						if($str_diklat[$i]==$this->input->post('sel_diklat') && $tipe[2*$i-2]=="Provinsi"){
							if($k<=20){
								if($k==2*$l-1){ 
									$even=" jtable-row-even";
									$l++;
								} else { 
									$even=""; 
								}
								
								$this->db->select('*');
								$this->db->from('dept');
								$this->db->where('id',$str_instansi[$i]);
								$result=$this->db->get();
								$result=$result->result();
								foreach($result as $row2){
									$dept=$row2->dept;
								}
								
								$this->db->select('*');
								$this->db->from('activity');
								$this->db->where('id',$str_diklat[$i]);
								$result=$this->db->get();
								$result=$result->result();
								foreach($result as $row2){
									$diklatname=$row2->activity;
								}
								$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
								
								// Data Jabatan
								$this->db->select('*');
								$this->db->from('position');
								$this->db->where('id',$str_jabatan[$i]);
								$result=$this->db->get();
								$result=$result->result();
								foreach($result as $row2){
									$jabatan=$row2->position;
								}
								
								if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
									$jabatan=$jabatan." ".$str_bagian[$i];
								}
								
								$content=$content.
								"<tr class=\"jtable-data-row".$even."\">
									<td><div align=\"center\">".$k."</div></td>
									<td>".$row_alumni->nama."</td>
									<td><div align=\"center\">".$row_alumni->nip."</div></td>
									<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
									<td><div align=\"center\">".$diklatnakt."</center></td>
									<td><div align=\"center\">".$jabatan."</div></td>
									<td><div align=\"center\">
										<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
										<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
										</div>
									</td>
								</tr>";
							}
							
							
							$k++;
							$pos=$i;
							$count_alumni++;
						}
						$i++;
					}

				}
			}
			
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_name')!=""){ // Success
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama)){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!=""){
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun')){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_jabatan')!=""){
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_jabatan[$i]==$this->input->post('sel_jabatan')){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_instansi')!=""){
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_instansi[$i]==$this->input->post('sel_instansi')){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_kota')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_kabupaten')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && $tipe[2*$i-1]==$this->input->post('sel_provinsi') && $tipe[2*$i-2]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
						
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_name')!=""){
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama)){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
	
			if($this->input->post('sel_tahun')!="" && $this->input->post('sel_jabatan')!=""){
				$i=1;
				
				while($str_tahun[$i]!=""){
					if($str_tahun[$i]==$this->input->post('sel_tahun') && $str_jabatan[$i]==$this->input->post('sel_jabatan')){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_tahun')!="" && $this->input->post('sel_instansi')!=""){
				$i=1;
				
				while($str_tahun[$i]!=""){
					if($str_tahun[$i]==$this->input->post('sel_tahun') && $str_instansi[$i]==$this->input->post('sel_instansi')){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_tahun')!="" && $this->input->post('sel_kota')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_tahun[$i]!=""){
					if($str_tahun[$i]==$this->input->post('sel_tahun') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_tahun')!="" && $this->input->post('sel_kabupaten')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_tahun[$i]!=""){
					if($str_tahun[$i]==$this->input->post('sel_tahun') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_tahun')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_tahun[$i]!=""){
					if($str_tahun[$i]==$this->input->post('sel_tahun') && $tipe[2*$i-1]==$this->input->post('sel_provinsi') && $tipe[2*$i-2]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_tahun')!="" && $this->input->post('sel_name')!=""){
				$i=1;
				
				while($str_tahun[$i]!=""){
					if($str_tahun[$i]==$this->input->post('sel_tahun') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama)){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_jabatan')!="" && $this->input->post('sel_instansi')!=""){
				$i=1;
				
				while($str_jabatan[$i]!=""){
					if($str_jabatan[$i]==$this->input->post('sel_jabatan') && $str_instansi[$i]==$this->input->post('sel_instansi')){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_jabatan')!="" && $this->input->post('sel_kota')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_jabatan[$i]!=""){
					if($str_jabatan[$i]==$this->input->post('sel_jabatan') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_jabatan')!="" && $this->input->post('sel_kabupaten')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_jabatan[$i]!=""){
					if($str_jabatan[$i]==$this->input->post('sel_jabatan') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_jabatan')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_jabatan[$i]!=""){
					if($str_jabatan[$i]==$this->input->post('sel_jabatan') && $tipe[2*$i-1]==$this->input->post('sel_provinsi') && $tipe[2*$i-2]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_jabatan')!="" && $this->input->post('sel_name')!=""){
				$i=1;
				
				while($str_jabatan[$i]!=""){
					if($str_jabatan[$i]==$this->input->post('sel_jabatan') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama)){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_instansi')!="" && $this->input->post('sel_kota')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_instansi[$i]!=""){
					if($str_instansi[$i]==$this->input->post('sel_instansi') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_instansi')!="" && $this->input->post('sel_kabupaten')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_instansi[$i]!=""){
					if($str_instansi[$i]==$this->input->post('sel_instansi') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_instansi')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_instansi[$i]!=""){
					if($str_instansi[$i]==$this->input->post('sel_instansi') && $tipe[2*$i-1]==$this->input->post('sel_provinsi') && $tipe[2*$i-2]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_instansi')!="" && $this->input->post('sel_name')!=""){
				$i=1;
				
				while($str_instansi[$i]!=""){
					if($str_instansi[$i]==$this->input->post('sel_instansi') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama)){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_kota')!="" && $this->input->post('sel_name')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota" && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama)){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_kabupaten')!="" && $this->input->post('sel_name')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten" && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama)){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_provinsi')!="" && $this->input->post('sel_name')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($tipe[2*$i-1]==$this->input->post('sel_provinsi') && $tipe[2*$i-2]=="Provinsi" && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama)){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			
		}
	
		if($total==3){
		
		//<diklat--angkatan--dst>
		
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun')){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_jabatan')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_jabatan[$i]==$this->input->post('sel_jabatan')){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_instansi')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_instansi[$i]==$this->input->post('sel_instansi')){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_kota')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_kabupaten')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_name')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama)){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		
		//</diklat--angkatan--dst>
		
		//<diklat--tahun--dst>
		
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_jabatan')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $str_tahun[$i]==$this->input->post('sel_tahun')){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_instansi')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_tahun[$i]==$this->input->post('sel_tahun') && $str_instansi[$i]==$this->input->post('sel_instansi')){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_kota')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_tahun[$i]==$this->input->post('sel_tahun') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_kabupaten')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_tahun[$i]==$this->input->post('sel_tahun') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_tahun[$i]==$this->input->post('sel_tahun') && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_name')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_tahun[$i]==$this->input->post('sel_tahun') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama)){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		
		//</diklat--tahun--dst>
		
		
		//<diklat--jabatan--dst>
		
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_instansi')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $str_instansi[$i]==$this->input->post('sel_instansi')){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_kota')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_kabupaten')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_name')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama)){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		
		//</diklat--jabatan--dst>
		
		
		//<diklat--instansi--dst>
			
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_kota')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_instansi[$i]==$this->input->post('sel_instansi') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_kabupaten')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_instansi[$i]==$this->input->post('sel_instansi') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_instansi[$i]==$this->input->post('sel_instansi') && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_name')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_instansi[$i]==$this->input->post('sel_instansi') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama)){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		
		//</diklat--instansi--dst>
		
		
		//<diklat--kota--dst>
			
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_kota')!="" && $this->input->post('sel_name')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota" && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama)){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		
		//</diklat--kota--dst>
		
		
		//<diklat--kabupaten--dst>
			
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_kabupaten')!="" && $this->input->post('sel_name')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten" && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama)){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		
		//</diklat--kabupaten--dst>
		
		
		//<diklat--provinsi--dst>
			
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_provinsi')!="" && $this->input->post('sel_name')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi" && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama)){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		
		//</diklat--provinsi--dst>
		
		
		//<angkatan--tahun--dst>
		
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_jabatan')!=""){
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && $str_jabatan[$i]==$this->input->post('sel_jabatan')){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_instansi')!=""){
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && $str_instansi[$i]==$this->input->post('sel_instansi')){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_kota')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_kabupaten')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_name')!=""){
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama)){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
		//</angkatan--tahun--dst>	
			
		
		//<angkatan--jabatan--dst>
			
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_instansi')!=""){
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $str_instansi[$i]==$this->input->post('sel_instansi')){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_kota')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_kabupaten')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_name')!=""){
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama)){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
		//</angkatan--jabatan--dst>


		//<angkatan--instansi--dst>
			
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_kota')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_instansi[$i]==$this->input->post('sel_instansi') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_kabupaten')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_instansi[$i]==$this->input->post('sel_instansi') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_instansi[$i]==$this->input->post('sel_instansi') && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_name')!=""){
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_instansi[$i]==$this->input->post('sel_instansi') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama)){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
		//</angkatan--instansi--dst>
		
		
		//<angkatan--kota--dst>
			
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_kota')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
		//</angkatan--kota--dst>
		
		
		//<angkatan--kabupaten--dst>
		
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_kabupaten')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
		//</angkatan--kabupaten--dst>
		
		
		//<angkatan--provinsi--dst>
		
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
		//</angkatan--provinsi--dst>
		
		
		//<tahun--jabatan--dst>
		
			if($this->input->post('sel_tahun')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_instansi')!=""){
				$i=1;
				
				while($str_tahun[$i]!=""){
					if($str_tahun[$i]==$this->input->post('sel_tahun') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $str_instansi[$i]==$this->input->post('sel_instansi')){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_tahun')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_kota')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_tahun[$i]!=""){
					if($str_tahun[$i]==$this->input->post('sel_tahun') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_tahun')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_kabupaten')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_tahun[$i]!=""){
					if($str_tahun[$i]==$this->input->post('sel_tahun') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_tahun')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_tahun[$i]!=""){
					if($str_tahun[$i]==$this->input->post('sel_tahun') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_tahun')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_name')!=""){
				$i=1;
				
				while($str_tahun[$i]!=""){
					if($str_tahun[$i]==$this->input->post('sel_tahun') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama)){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		
		//</tahun--jabatan--dst>
		
		
		//<tahun--instansi--dst>
		
			if($this->input->post('sel_tahun')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_kota')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_tahun[$i]!=""){
					if($str_tahun[$i]==$this->input->post('sel_tahun') && $str_instansi[$i]==$this->input->post('sel_instansi') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_tahun')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_kabupaten')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_tahun[$i]!=""){
					if($str_tahun[$i]==$this->input->post('sel_tahun') && $str_instansi[$i]==$this->input->post('sel_instansi') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_tahun')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_tahun[$i]!=""){
					if($str_tahun[$i]==$this->input->post('sel_tahun') && $str_instansi[$i]==$this->input->post('sel_instansi') && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_tahun')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_name')!=""){
				$i=1;
				
				while($str_tahun[$i]!=""){
					if($str_tahun[$i]==$this->input->post('sel_tahun') && $str_instansi[$i]==$this->input->post('sel_instansi') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama)){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		
		//</tahun--instansi--dst>
		
		
		//<tahun--kota--dst>
		
			if($this->input->post('sel_tahun')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_kota')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_tahun[$i]!=""){
					if($str_tahun[$i]==$this->input->post('sel_tahun') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
		//</tahun--kota--dst>
		
		
		//<tahun--kabupaten--dst>
			
			if($this->input->post('sel_tahun')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_kabupaten')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_tahun[$i]!=""){
					if($str_tahun[$i]==$this->input->post('sel_tahun') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
		//</tahun--kabupaten--dst>
		
		
		//<tahun--provinsi--dst>
		
			if($this->input->post('sel_tahun')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_tahun[$i]!=""){
					if($str_tahun[$i]==$this->input->post('sel_tahun') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
			
		//</tahun--provinsi--dst>
		

		//<jabatan--instansi--dst>
			
			if($this->input->post('sel_jabatan')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_kota')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_jabatan[$i]!=""){
					if($str_jabatan[$i]==$this->input->post('sel_jabatan') && $str_instansi[$i]==$this->input->post('sel_instansi') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_jabatan')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_kabupaten')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_jabatan[$i]!=""){
					if($str_jabatan[$i]==$this->input->post('sel_jabatan') && $str_instansi[$i]==$this->input->post('sel_instansi') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_jabatan')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_jabatan[$i]!=""){
					if($str_jabatan[$i]==$this->input->post('sel_jabatan') && $str_instansi[$i]==$this->input->post('sel_instansi') && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_jabatan')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_name')!=""){
				$i=1;
				
				while($str_jabatan[$i]!=""){
					if($str_jabatan[$i]==$this->input->post('sel_jabatan') && $str_instansi[$i]==$this->input->post('sel_instansi') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama)){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$count_alumni++;
					}
					$i++;
				}
			}
			
		//</jabatan--instansi--dst>
		
		
		//<jabatan--kota--dst>
			
			if($this->input->post('sel_jabatan')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_kota')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_jabatan[$i]!=""){
					if($str_jabatan[$i]==$this->input->post('sel_jabatan') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$count_alumni++;
					}
					$i++;
				}
			}
			
		//</jabatan--kota--dst>
		
		
		//<jabatan--kabupaten--dst>
		
			if($this->input->post('sel_jabatan')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_kabupaten')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_jabatan[$i]!=""){
					if($str_jabatan[$i]==$this->input->post('sel_jabatan') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$count_alumni++;
					}
					$i++;
				}
			}
			
		//</jabatan--kabupaten--dst>
		
		
		//<jabatan--provinsi--dst>
		
			if($this->input->post('sel_jabatan')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_jabatan[$i]!=""){
					if($str_jabatan[$i]==$this->input->post('sel_jabatan') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$count_alumni++;
					}
					$i++;
				}
			}
			
		//</jabatan--instansi--dst>
		
		
		//<instansi--kota--dst>
			
			if($this->input->post('sel_instansi')!="" && $this->input->post('sel_kota')!="" && $this->input->post('sel_name')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_instansi[$i]!=""){
					if($str_instansi[$i]==$this->input->post('sel_instansi') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota" && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama)){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_instansi')!="" && $this->input->post('sel_kabupaten')!="" && $this->input->post('sel_name')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_instansi[$i]!=""){
					if($str_instansi[$i]==$this->input->post('sel_instansi') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten" && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama)){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_instansi')!="" && $this->input->post('sel_provinsi')!="" && $this->input->post('sel_name')!=""){
				$i=1;
				
				while($str_instansi[$i]!=""){
					if($str_instansi[$i]==$this->input->post('sel_instansi') && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi" && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama)){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$count_alumni++;
					}
					$i++;
				}
			}
			
		//</instansi--kota--dst>
			
		}
	
		if($total==4){
			
		//<diklat--angkatan--tahun--jabatan>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_jabatan')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && $str_jabatan[$i]==$this->input->post('sel_jabatan')){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--angkatan--tahun--jabatan>
		
		//<diklat--angkatan--tahun--instansi>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_instansi')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && $str_instansi[$i]==$this->input->post('sel_instansi')){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--angkatan--tahun--instansi>
			
		//<diklat--angkatan--tahun--kota>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_kota')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--angkatan--tahun--kota>
			
		//<diklat--angkatan--tahun--kabupaten>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_kabupaten')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--angkatan--tahun--kabupaten>
			
		//<diklat--angkatan--tahun--provinsi>		
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--angkatan--tahun--provinsi>
			
		//<diklat--angkatan--tahun--nama>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_name')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama)){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--angkatan--tahun--nama>
		
		//<diklat--angkatan--jabatan--instansi>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_instansi')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $str_instansi[$i]==$this->input->post('sel_instansi')){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--angkatan--jabatan--instansi>
			
		//<diklat--angkatan--jabatan--kota>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_kota')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--angkatan--jabatan--kota>
			
		//<diklat--angkatan--jabatan--kabupaten>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_kabupaten')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--angkatan--jabatan--kabupaten>
			
		//<diklat--angkatan--jabatan--provinsi>		
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--angkatan--jabatan--provinsi>
			
		//<diklat--angkatan--jabatan--nama>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_name')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama)){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--angkatan--jabatan--nama>
		
		//<diklat--angkatan--instansi--kota>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_kota')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_instansi[$i]==$this->input->post('sel_instansi') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--angkatan--instansi--kota>
			
		//<diklat--angkatan--instansi--kabupaten>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_kabupaten')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_instansi[$i]==$this->input->post('sel_instansi') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--angkatan--instansi--kabupaten>
			
		//<diklat--angkatan--instansi--provinsi>		
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_instansi[$i]==$this->input->post('sel_instansi') && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--angkatan--instansi--provinsi>
			
		//<diklat--angkatan--instansi--nama>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_name')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_instansi[$i]==$this->input->post('sel_instansi') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama)){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--angkatan--instansi--nama>
		
		//<diklat--angkatan--nama--kota>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_kota')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--angkatan--nama--kota>
			
		//<diklat--angkatan--nama--kabupaten>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_kabupaten')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--angkatan--nama--kabupaten>
			
		//<diklat--angkatan--nama--provinsi>		
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--angkatan--nama--provinsi>
		
		//<diklat--tahun--jabatan--instansi>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_instansi')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_tahun[$i]==$this->input->post('sel_tahun') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $str_instansi[$i]==$this->input->post('sel_instansi')){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--tahun--jabatan--instansi>
		
		//<diklat--tahun--jabatan--kota>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_kota')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_tahun[$i]==$this->input->post('sel_tahun') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--tahun--jabatan--kota>
			
		//<diklat--tahun--jabatan--kabupaten>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_kabupaten')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--tahun--jabatan--kabupaten>
			
		//<diklat--tahun--jabatan--provinsi>		
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--tahun--jabatan--provinsi>
				
		//<diklat--tahun--jabatan--nama>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_name')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama)){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--tahun--jabatan--nama>
		
		//<diklat--tahun--instansi--kota>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_kota')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_tahun[$i]==$this->input->post('sel_tahun') && $str_instansi[$i]==$this->input->post('sel_instansi') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--tahun--instansi--kota>
			
		//<diklat--tahun--instansi--kabupaten>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_kabupaten')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_instansi[$i]==$this->input->post('sel_instansi') && $str_tahun[$i]==$this->input->post('sel_tahun') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--tahun--instansi--kabupaten>
			
		//<diklat--tahun--instansi--provinsi>		
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_instansi[$i]==$this->input->post('sel_instansi') && $str_tahun[$i]==$this->input->post('sel_tahun') && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--tahun--instansi--provinsi>
				
		//<diklat--tahun--instansi--nama>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_name')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_instansi[$i]==$this->input->post('sel_instansi') && $str_tahun[$i]==$this->input->post('sel_tahun') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama)){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--tahun--instansi--nama>
		
		//<diklat--tahun--kota--nama>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_kota')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_tahun[$i]==$this->input->post('sel_tahun') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--tahun--kota--nama>
			
		//<diklat--tahun--kabupaten--nama>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_kabupaten')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $str_tahun[$i]==$this->input->post('sel_tahun') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--tahun--kabupaten--nama>
			
		//<diklat--tahun--provinsi--nama>		
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $str_tahun[$i]==$this->input->post('sel_tahun') && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--tahun--provinsi--nama>
		
		//<diklat--jabatan--instansi--kota>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_kota')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_instansi[$i]==$this->input->post('sel_instansi') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--jabatan--instansi--kota>
			
		//<diklat--jabatan--instansi--kabupaten>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_kabupaten')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $str_instansi[$i]==$this->input->post('sel_instansi') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--jabatan--instansi--kabupaten>
			
		//<diklat--jabatan--instansi--provinsi>		
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $str_instansi[$i]==$this->input->post('sel_instansi') && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--jabatan--instansi--provinsi>
				
		//<diklat--jabatan--instansi--nama>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_name')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $str_instansi[$i]==$this->input->post('sel_instansi') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama)){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--jabatan--instansi--nama>
		
		//<diklat--jabatan--kota--nama>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_kota')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--jabatan--kota--nama>
			
		//<diklat--jabatan--kabupaten--nama>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_kabupaten')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--jabatan--kabupaten--nama>
			
		//<diklat--jabatan--provinsi--nama>		
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--jabatan--provinsi--nama>
		
		//<diklat--instansi--kota--nama>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_kota')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $str_instansi[$i]==$this->input->post('sel_instansi') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--instansi--kota--nama>
			
		//<diklat--instansi--kabupaten--nama>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_kabupaten')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_instansi[$i]==$this->input->post('sel_instansi') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--instansi--kabupaten--nama>
			
		//<diklat--instansi--provinsi--nama>		
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_instansi[$i]==$this->input->post('sel_instansi') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--instansi--provinsi--nama>
		
				
		//<angkatan--tahun--jabatan--instansi>
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_instansi')!=""){
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && $str_jabatan[$i]==$this->input->post('sel_jabatan')  && $str_instansi[$i]==$this->input->post('sel_instansi')){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<angkatan--tahun--jabatan--instansi>
		
		//<angkatan--tahun--jabatan--kota>
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_kota')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && $str_jabatan[$i]==$this->input->post('sel_jabatan')  && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<angkatan--tahun--jabatan--kota>
		
		//<angkatan--tahun--jabatan--kabupaten>
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_kabupaten')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && $str_jabatan[$i]==$this->input->post('sel_jabatan')  && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<angkatan--tahun--jabatan--kabupaten>
	
		//<angkatan--tahun--jabatan--provinsi>
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && $str_jabatan[$i]==$this->input->post('sel_jabatan')  && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<angkatan--tahun--jabatan--provinsi>
		
		
		//<angkatan--tahun--jabatan--nama>
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_name')!=""){
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && $str_jabatan[$i]==$this->input->post('sel_jabatan')  && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama)){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<angkatan--tahun--jabatan--nama>
			
		//<angkatan--tahun--instansi--kota>
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_kota')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && $str_instansi[$i]==$this->input->post('sel_instansi')  && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<angkatan--tahun--instansi--kota>
		
		//<angkatan--tahun--instansi--kabupaten>
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_kabupaten')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && $str_instansi[$i]==$this->input->post('sel_instansi')  && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<angkatan--tahun--instansi--kabupaten>
		
		//<angkatan--tahun--instansi--provinsi>
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && $str_instansi[$i]==$this->input->post('sel_instansi')  && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<angkatan--tahun--instansi--provinsi>
		
		//<angkatan--tahun--instansi--nama>
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_name')!=""){
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && $str_instansi[$i]==$this->input->post('sel_instansi')  && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama)){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<angkatan--tahun--instansi--nama>
		
		//<angkatan--tahun--kota--nama>
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_kota')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<angkatan--tahun--kota--nama>
		
		//<angkatan--tahun--kabupaten--nama>
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_kabupaten')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<angkatan--tahun--kabupaten--nama>
		
		//<angkatan--tahun--provinsi--nama>
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<angkatan--tahun--provinsi--nama>
			
		//<tahun--jabatan--instansi--kota>	
			if($this->input->post('sel_tahun')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_kota')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_tahun[$i]!=""){
					if($str_tahun[$i]==$this->input->post('sel_tahun') && $str_instansi[$i]==$this->input->post('sel_instansi') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<tahun--jabatan--instansi--kota>
			
		//<tahun--jabatan--instansi--kabupaten>
			if($this->input->post('sel_tahun')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_kabupaten')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_tahun[$i]!=""){
					if($str_tahun[$i]==$this->input->post('sel_tahun') && $str_instansi[$i]==$this->input->post('sel_instansi') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<tahun--jabatan--instansi--kabupaten>

		//<tahun--jabatan--instansi--provinsi>
			if($this->input->post('sel_tahun')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_tahun[$i]!=""){
					if($str_tahun[$i]==$this->input->post('sel_tahun') && $str_instansi[$i]==$this->input->post('sel_instansi') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<tahun--jabatan--instansi--provinsi>
		
		//<tahun--jabatan--instansi--nama>
			if($this->input->post('sel_tahun')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_name')!=""){
				$i=1;
				
				while($str_tahun[$i]!=""){
					if($str_tahun[$i]==$this->input->post('sel_tahun') && $str_instansi[$i]==$this->input->post('sel_instansi') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama)){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<tahun--jabatan--instansi--nama>
			
		
		//<tahun--jabatan--kota--nama>	
			if($this->input->post('sel_tahun')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_kota')!="" && $this->input->post('sel_name')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_tahun[$i]!=""){
					if($str_tahun[$i]==$this->input->post('sel_tahun') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<tahun--jabatan--kota--nama>
			
		//<tahun--jabatan--kabupaten--nama>
			if($this->input->post('sel_tahun')!="" && $this->input->post('sel_kabupaten')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_name')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_tahun[$i]!=""){
					if($str_tahun[$i]==$this->input->post('sel_tahun') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<tahun--jabatan--kabupaten--nama>

		//<tahun--jabatan--provinsi--nama>
			if($this->input->post('sel_tahun')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_tahun[$i]!=""){
					if($str_tahun[$i]==$this->input->post('sel_tahun') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<tahun--jabatan--provinsi--nama>
		
		//<tahun--instansi--kota--nama>	
			if($this->input->post('sel_tahun')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_kota')!="" && $this->input->post('sel_name')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_tahun[$i]!=""){
					if($str_tahun[$i]==$this->input->post('sel_tahun') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $str_instansi[$i]==$this->input->post('sel_instansi') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<tahun--instansi--kota--nama>
			
		//<tahun--instansi--kabupaten--nama>
			if($this->input->post('sel_tahun')!="" && $this->input->post('sel_kabupaten')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_name')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_tahun[$i]!=""){
					if($str_tahun[$i]==$this->input->post('sel_tahun') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $str_instansi[$i]==$this->input->post('sel_instansi') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<tahun--instansi--kabupaten--nama>

		//<tahun--instansi--provinsi--nama>
			if($this->input->post('sel_tahun')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_tahun[$i]!=""){
					if($str_tahun[$i]==$this->input->post('sel_tahun') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $str_instansi[$i]==$this->input->post('sel_instansi') && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<tahun--instansi--provinsi--nama>
		
		//<jabatan--instansi--kota--nama>	
			if($this->input->post('sel_jabatan')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_kota')!="" && $this->input->post('sel_name')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_jabatan[$i]!=""){
					if($str_jabatan[$i]==$this->input->post('sel_jabatan') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $str_instansi[$i]==$this->input->post('sel_instansi') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<jabatan--instansi--kota--nama>
			
		//<jabatan--instansi--kabupaten--nama>
			if($this->input->post('sel_jabatan')!="" && $this->input->post('sel_kabupaten')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_name')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_jabatan[$i]!=""){
					if($str_jabatan[$i]==$this->input->post('sel_jabatan') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $str_instansi[$i]==$this->input->post('sel_instansi') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<jabatan--instansi--kabupaten--nama>

		//<jabatan--instansi--provinsi--nama>
			if($this->input->post('sel_jabatan')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_jabatan[$i]!=""){
					if($str_jabatan[$i]==$this->input->post('sel_jabatan') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $str_instansi[$i]==$this->input->post('sel_instansi') && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<jabatan--instansi--provinsi--nama>
			
		}
	
		if($total==5){
			
		//<diklat--angkatan--tahun--jabatan--instansi>	
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_instansi')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $str_instansi[$i]==$this->input->post('sel_instansi')){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--angkatan--tahun--jabatan--instansi>

		//<diklat--angkatan--tahun--jabatan--kota>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_kota')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--angkatan--tahun--jabatan--kota>
		
		//<diklat--angkatan--tahun--jabatan--kabupaten>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_kabupaten')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--angkatan--tahun--jabatan--kabupaten>
		
		//<diklat--angkatan--tahun--jabatan--provinsi>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--angkatan--tahun--jabatan--provinsi>
		
		//<diklat--angkatan--tahun--jabatan--nama>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_name')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama)){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--angkatan--tahun--jabatan--nama>
	
		//<diklat--angkatan--jabatan--instansi--kota>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_kota')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_instansi[$i]==$this->input->post('sel_instansi') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--angkatan--jabatan--instansi--kota>
		
		//<diklat--angkatan--jabatan--instansi--kabupaten>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_kabupaten')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_instansi[$i]==$this->input->post('sel_instansi') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--angkatan--jabatan--instansi--kabupaten>
		
		//<diklat--angkatan--jabatan--instansi--provinsi>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_instansi[$i]==$this->input->post('sel_instansi') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--angkatan--jabatan--instansi--provinsi>
		
		//<diklat--angkatan--jabatan--instansi--nama>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_name')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_instansi[$i]==$this->input->post('sel_instansi') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama)){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--angkatan--jabatan--instansi--nama>

		//<diklat--angkatan--jabatan--kota--nama>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_kota')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--angkatan--jabatan--kota--nama>
		
		//<diklat--angkatan--jabatan--kabupaten--nama>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_kabupaten')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--angkatan--jabatan--kabupaten--nama>
		
		//<diklat--angkatan--jabatan--provinsi--nama>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--angkatan--jabatan--provinsi--nama>


		//<diklat--tahun--jabatan--instansi--kota>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_kota')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_instansi[$i]==$this->input->post('sel_instansi') && $str_tahun[$i]==$this->input->post('sel_tahun') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--tahun--jabatan--instansi--kota>
		
		//<diklat--tahun--jabatan--instansi--kabupaten>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_kabupaten')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_instansi[$i]==$this->input->post('sel_instansi') && $str_tahun[$i]==$this->input->post('sel_tahun') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--tahun--jabatan--instansi--kabupaten>
		
		//<diklat--tahun--jabatan--instansi--provinsi>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_instansi[$i]==$this->input->post('sel_instansi') && $str_tahun[$i]==$this->input->post('sel_tahun') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--tahun--jabatan--instansi--provinsi>
		
		//<diklat--tahun--jabatan--instansi--nama>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_name')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_instansi[$i]==$this->input->post('sel_instansi') && $str_tahun[$i]==$this->input->post('sel_tahun') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama)){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--tahun--jabatan--instansi--nama>
		
		
		//<diklat--tahun--instansi--kota--nama>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_kota')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_instansi[$i]==$this->input->post('sel_instansi') && $str_tahun[$i]==$this->input->post('sel_tahun') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--tahun--instansi--kota--nama>
		
		//<diklat--tahun--instansi--kabupaten--nama>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_kabupaten')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_instansi[$i]==$this->input->post('sel_instansi') && $str_tahun[$i]==$this->input->post('sel_tahun') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--tahun--instansi--kabupaten--nama>
		
		//<diklat--tahun--instansi--provinsi--nama>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_instansi[$i]==$this->input->post('sel_instansi') && $str_tahun[$i]==$this->input->post('sel_tahun') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--tahun--instansi--provinsi--nama>
		
		
		//<diklat--jabatan--instansi--kota--nama>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_kota')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_instansi[$i]==$this->input->post('sel_instansi') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--jabatan--instansi--kota--nama>
		
		//<diklat--jabatan--instansi--kabupaten--nama>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_kabupaten')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_instansi[$i]==$this->input->post('sel_instansi') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--jabatan--instansi--kabupaten--nama>
		
		//<diklat--jabatan--instansi--provinsi--nama>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_instansi[$i]==$this->input->post('sel_instansi') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--jabatan--instansi--provinsi--nama>
				
		
		//<angkatan--tahun--jabatan--instansi--kota>
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_kota')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_instansi')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota" && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $str_instansi[$i]==$this->input->post('sel_instansi')){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<angkatan--tahun--jabatan--instansi--kota>

		//<angkatan--tahun--jabatan--instansi--kabupaten>
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_kabupaten')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $str_instansi[$i]==$this->input->post('sel_instansi')  && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<angkatan--tahun--jabatan--instansi--kabupaten>
		
		//<angkatan--tahun--jabatan--instansi--provinsi>	
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $str_instansi[$i]==$this->input->post('sel_instansi') && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<angkatan--tahun--jabatan--instansi--provinsi>

		//<angkatan--tahun--jabatan--instansi--nama>		
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_name')!=""){
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $str_instansi[$i]==$this->input->post('sel_instansi') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama)){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<angkatan--tahun--jabatan--instansi--nama>
			
		
		//<angkatan--tahun--instansi--kota--nama>
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_kota')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_instansi')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota" && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $str_instansi[$i]==$this->input->post('sel_instansi')){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<angkatan--tahun--instansi--kota--nama>

		//<angkatan--tahun--instansi--kabupaten--nama>
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_kabupaten')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $str_instansi[$i]==$this->input->post('sel_instansi')  && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<angkatan--tahun--instansi--kabupaten--nama>
		
		//<angkatan--tahun--instansi--provinsi--nama>	
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $str_instansi[$i]==$this->input->post('sel_instansi') && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<angkatan--tahun--instansi--provinsi--nama>

		
		//<tahun--jabatan--instansi--kota--nama>
			if($this->input->post('sel_jabatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_kota')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_instansi')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_tahun[$i]!=""){
					if($str_jabatan[$i]==$this->input->post('sel_jabatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota" && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $str_instansi[$i]==$this->input->post('sel_instansi')){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<tahun--jabatan--instansi--kota--nama>

		//<tahun--jabatan--instansi--kabupaten--nama>
			if($this->input->post('sel_jabatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_kabupaten')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_tahun[$i]!=""){
					if($str_jabatan[$i]==$this->input->post('sel_jabatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $str_instansi[$i]==$this->input->post('sel_instansi')  && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<tahun--jabatan--instansi--kabupaten--nama>
		
		//<tahun--jabatan--instansi--provinsi--nama>	
			if($this->input->post('sel_jabatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_tahun[$i]!=""){
					if($str_jabatan[$i]==$this->input->post('sel_jabatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $str_instansi[$i]==$this->input->post('sel_instansi') && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<tahun--jabatan--instansi--provinsi--nama>
			
			
		}
	
		if($total==6){
			
		//<diklat--angkatan--tahun--jabatan--instansi--kota>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_kota')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && $str_instansi[$i]==$this->input->post('sel_instansi') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--angkatan--tahun--jabatan--instansi--kota>
		
		//<diklat--angkatan--tahun--jabatan--instansi--kabupaten>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_kabupaten')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && $str_instansi[$i]==$this->input->post('sel_instansi') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--angkatan--tahun--jabatan--instansi--kabupaten>
		
		//<diklat--angkatan--tahun--jabatan--instansi--provinsi>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && $str_instansi[$i]==$this->input->post('sel_instansi') && $str_jabatan[$i]==$this->input->post('sel_jabatan')  && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--angkatan--tahun--jabatan--instansi--provinsi>
		
		//<diklat--angkatan--tahun--jabatan--instansi--nama>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_name')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && $str_instansi[$i]==$this->input->post('sel_instansi') && $str_jabatan[$i]==$this->input->post('sel_jabatan')  && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama)){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--angkatan--tahun--jabatan--instansi--nama>
		
		
		//<diklat--angkatan--tahun--jabatan--kota--nama>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_kota')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--angkatan--tahun--jabatan--kota--nama>
		
		//<diklat--angkatan--tahun--jabatan--kabupaten--nama>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_kabupaten')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--angkatan--tahun--jabatan--kabupaten--nama>
		
		//<diklat--angkatan--tahun--jabatan--provinsi--nama>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $str_jabatan[$i]==$this->input->post('sel_jabatan')  && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--angkatan--tahun--jabatan--provinsi--nama>
		
		
		//<diklat--angkatan--jabatan--instansi--kota--nama>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_kota')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $str_instansi[$i]==$this->input->post('sel_instansi') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--angkatan--jabatan--instansi--kota--nama>
		
		//<diklat--angkatan--jabatan--instansi--kabupaten--nama>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_kabupaten')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $str_instansi[$i]==$this->input->post('sel_instansi') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--angkatan--jabatan--instansi--kabupaten--nama>
		
		//<diklat--angkatan--jabatan--instansi--provinsi--nama>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $str_instansi[$i]==$this->input->post('sel_instansi') && $str_jabatan[$i]==$this->input->post('sel_jabatan')  && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--angkatan--jabatan--instansi--provinsi--nama>
		
		
		//<diklat--tahun--jabatan--instansi--kota--nama>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_kota')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_tahun[$i]==$this->input->post('sel_tahun') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $str_instansi[$i]==$this->input->post('sel_instansi') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--tahun--jabatan--instansi--kota--nama>
		
		//<diklat--tahun--jabatan--instansi--kabupaten--nama>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_kabupaten')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_tahun[$i]==$this->input->post('sel_tahun') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $str_instansi[$i]==$this->input->post('sel_instansi') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--tahun--jabatan--instansi--kabupaten--nama>
		
		//<diklat--tahun--jabatan--instansi--provinsi--nama>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_tahun[$i]==$this->input->post('sel_tahun') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $str_instansi[$i]==$this->input->post('sel_instansi') && $str_jabatan[$i]==$this->input->post('sel_jabatan')  && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--tahun--jabatan--instansi--provinsi--nama>
			
			
		//<angkatan--tahun--jabatan--instansi--kota--nama>
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_kota')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $str_instansi[$i]==$this->input->post('sel_instansi') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<angkatan--tahun--jabatan--instansi--kota--nama>
		
		//<angkatan--tahun--jabatan--instansi--kabupaten--nama>
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_kabupaten')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $str_instansi[$i]==$this->input->post('sel_instansi') && $str_jabatan[$i]==$this->input->post('sel_jabatan') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<angkatan--tahun--jabatan--instansi--kabupaten--nama>
		
		//<angkatan--tahun--jabatan--instansi--provinsi--nama>
			if($this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_angkatan[$i]!=""){
					if($str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $str_instansi[$i]==$this->input->post('sel_instansi') && $str_jabatan[$i]==$this->input->post('sel_jabatan')  && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<angkatan--tahun--jabatan--instansi--provinsi--nama>
			
		}
	
		if($total==7){
		
		//<diklat--angkatan--tahun--jabatan--instansi--kota--nama>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_kota')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kota'));
				$this->db->where('tipe','Kota');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $str_jabatan[$i]==$this->input->post('sel_jabatan')  && $str_instansi[$i]==$this->input->post('sel_instansi') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kota"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--angkatan--tahun--jabatan--instansi--kota--nama>

		//<diklat--angkatan--tahun--jabatan--instansi--kabupaten--nama>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_kabupaten')!=""){
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$this->input->post('sel_kabupaten'));
				$this->db->where('tipe','Kabupaten');
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row_kota){
					$city=$row_kota->city;
				}
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $str_jabatan[$i]==$this->input->post('sel_jabatan')  && $str_instansi[$i]==$this->input->post('sel_instansi') && $tipe[2*$i-1]==$city && $tipe[2*$i-2]=="Kabupaten"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--angkatan--tahun--jabatan--instansi--kabupaten--nama>
		
		//<diklat--angkatan--tahun--jabatan--instansi--provinsi--nama>
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_name')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_provinsi')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama) && $str_jabatan[$i]==$this->input->post('sel_jabatan')  && $str_instansi[$i]==$this->input->post('sel_instansi') && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi"){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$pos=$i;
						$count_alumni++;
					}
					$i++;
				}
			}
		//<diklat--angkatan--tahun--jabatan--instansi--provinsi--nama>

		
			
			
		}
	
		if($total==8){
			
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_pangkat')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_kota')!=""  && $this->input->post('sel_name')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && $str_pangkat[$i]==$this->input->post('sel_pangkat') && $str_jabatan[$i]==$this->input->post('sel_jabatan')  && $str_instansi[$i]==$this->input->post('sel_instansi') && $tipe[$i]==$this->input->post('sel_kota') && $tipe[$i-1]=="Kota" && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama)){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_pangkat')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_kabupaten')!=""  && $this->input->post('sel_name')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && $str_pangkat[$i]==$this->input->post('sel_pangkat') && $str_jabatan[$i]==$this->input->post('sel_jabatan')  && $str_instansi[$i]==$this->input->post('sel_instansi') && $tipe[$i]==$this->input->post('sel_kabupaten') && $tipe[$i-1]=="Kabupaten" && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama)){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$count_alumni++;
					}
					$i++;
				}
			}
			
			if($this->input->post('sel_diklat')!="" && $this->input->post('sel_angkatan')!="" && $this->input->post('sel_tahun')!="" && $this->input->post('sel_pangkat')!="" && $this->input->post('sel_jabatan')!="" && $this->input->post('sel_instansi')!="" && $this->input->post('sel_provinsi')!=""  && $this->input->post('sel_name')!=""){
				$i=1;
				
				while($str_diklat[$i]!=""){
					if($str_diklat[$i]==$this->input->post('sel_diklat') && $str_angkatan[$i]==$this->input->post('sel_angkatan') && $str_tahun[$i]==$this->input->post('sel_tahun') && $str_pangkat[$i]==$this->input->post('sel_pangkat') && $str_jabatan[$i]==$this->input->post('sel_jabatan')  && $str_instansi[$i]==$this->input->post('sel_instansi') && $tipe[$i]==$this->input->post('sel_provinsi') && $tipe[$i-1]=="Provinsi" && preg_match('/'.$this->input->post('sel_name').'/i',$row_alumni->nama)){
						if($k<=20){
							if($k==2*$l-1){ 
								$even=" jtable-row-even";
								$l++;
							} else { 
								$even=""; 
							}
							
							$this->db->select('*');
							$this->db->from('dept');
							$this->db->where('id',$str_instansi[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$dept=$row2->dept;
							}
							
							$this->db->select('*');
							$this->db->from('activity');
							$this->db->where('id',$str_diklat[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$diklatname=$row2->activity;
							}
							$diklatnakt=$diklatname." Angkatan ".$str_angkatan[$i]." Tahun ".$str_tahun[$i];
							
							// Data Jabatan
							$this->db->select('*');
							$this->db->from('position');
							$this->db->where('id',$str_jabatan[$i]);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$jabatan=$row2->position;
							}
							
							if($str_bagian[$i]!="" || $str_bagian[$i]!=" "){
								$jabatan=$jabatan." ".$str_bagian[$i];
							}
							
							$content=$content.
							"<tr class=\"jtable-data-row".$even."\">
								<td><div align=\"center\">".$k."</div></td>
								<td>".$row_alumni->nama."</td>
								<td><div align=\"center\">".$row_alumni->nip."</div></td>
								<td><div align=\"center\">".$dept." ".$str_wilayah[2*$i-2]." ".$str_wilayah[2*$i-1]."</div></td>
								<td><div align=\"center\">".$diklatnakt."</center></td>
								<td><div align=\"center\">".$jabatan."</div></td>
								<td><div align=\"center\">
									<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Alumni\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/input/alumni/editdata/?id=".$row_alumni->id."'\" />
									<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Peserta\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus peserta dengan nama ".$row_alumni->nama." dari database?')){ deletedatareport('".$row_alumni->id."'); }\" />
									</div>
								</td>
							</tr>";
						}
						
						
						$k++;
						$count_alumni++;
					}
					$i++;
				}
			}
			
		}
		
	}
	
	$ndata=$count_alumni;
	$itemperpage=20;
	$totalpage=ceil($ndata/$itemperpage);
	
	
	if($ndata!=0){
			
		# Halaman #
		echo "<div class=\"pagination\">";
		echo "<span class=\"pg-selected\" >Prev</span>";
		$i=1;
		$status="";
		$status_="";
		$status__="";
		$batas=5;
		while($i<=$totalpage){
			if($i!=1){
				if($totalpage>=10){
					if($i<=$batas || $i>$totalpage-2){
						echo "<span class=\"pg-normal\" onclick=\"showdatareport('".$i."', '".$this->input->post('sel_diklat')."', '".$this->input->post('sel_angkatan')."','".$this->input->post('sel_tahun')."','".$this->input->post('sel_jabatan')."','".$this->input->post('sel_instansi')."','".$this->input->post('sel_kota')."','".$this->input->post('sel_kabupaten')."','".$this->input->post('sel_provinsi')."','".$this->input->post('sel_name')."')\">".$i."</span>";
					} else {
						if($status==""){
							$status="ada";
							echo " ... ";
						}
					}
				} else {
					echo "<span class=\"pg-normal\" onclick=\"showdatareport('".$i."', '".$this->input->post('sel_diklat')."', '".$this->input->post('sel_angkatan')."','".$this->input->post('sel_tahun')."','".$this->input->post('sel_jabatan')."','".$this->input->post('sel_instansi')."','".$this->input->post('sel_kota')."','".$this->input->post('sel_kabupaten')."','".$this->input->post('sel_provinsi')."','".$this->input->post('sel_name')."')\">".$i."</span>";
				}
			} else {
				echo "<span class=\"pg-selected\" >".$i."</span>";
			}
			
			$i++;
		}
			
			
		if($totalpage!=1){
			echo "<span class=\"pg-normal\" onclick=\"showdatareport('2', '".$this->input->post('sel_diklat')."', '".$this->input->post('sel_angkatan')."','".$this->input->post('sel_tahun')."','".$this->input->post('sel_jabatan')."','".$this->input->post('sel_instansi')."','".$this->input->post('sel_kota')."','".$this->input->post('sel_kabupaten')."','".$this->input->post('sel_provinsi')."','".$this->input->post('sel_name')."')\">Next</span>";
		} else {
			echo "<span class=\"pg-selected\" >Next</span>";
		}
		echo "<span class=\"pg-normal\" onclick=\"document.location='http://pusdiklat.bkpm.go.id/asset/export3.php?diklat=".$this->input->post('sel_diklat')."&angkatan=".$this->input->post('sel_angkatan')."&tahun=".$this->input->post('sel_tahun')."&jabatan=".$this->input->post('sel_jabatan')."&instansi=".$this->input->post('sel_instansi')."&kota=".$this->input->post('sel_kota')."&kabupaten=".$this->input->post('sel_kabupaten')."&provinsi=".$this->input->post('sel_provinsi')."&nama=".$this->input->post('sel_name')."&pangkat='\">Export ke xls</span></div>";
		echo "<div style=\"padding-top: 20px;\"></div>";
		# Halaman # 
			
		# Tampilan Data
		echo "
		<div class=\"jtable-main-container\">
			<div class=\"jtable-busy-panel-background\" style=\"display: none; width: 940px; height: 123px;\"></div>
			<div class=\"jtable-busy-message\" style=\"display: none;\"></div>
			<div class=\"jtable-title\">
				<div class=\"jtable-title-text\">
					Daftar Alumni Diklat (Hasil Pencarian)
				</div>
			</div>
				
			<table class=\"jtable\">
				<thead>
					<tr>
						<th class=\"jtable-column-header\" style=\"width: 5%;\">
							<div class=\"jtable-column-header-container\" align=\"center\">No</div>
						</th>
						<th class=\"jtable-column-header\" style=\"width: 18%;\">
							<div class=\"jtable-column-header-container\" align=\"center\">Nama</div>
						</th>
						<th class=\"jtable-column-header\" style=\"width: 18%;\">
							<div class=\"jtable-column-header-container\" align=\"center\">NIP</div>
						</th>
						<th class=\"jtable-column-header\" style=\"width: 18%;\">
							<div class=\"jtable-column-header-container\" align=\"center\">Instansi</div>
						</th>
						<th class=\"jtable-column-header\" style=\"width: 18%;\">
							<div class=\"jtable-column-header-container\" align=\"center\">Diklat & Angkatan</div>
						</th>
						<th class=\"jtable-column-header\" style=\"width: 17%;\">
							<div class=\"jtable-column-header-container\" align=\"center\">Jabatan</div>
						</th>
						<th class=\"jtable-column-header\" style=\"width: 6%;\">
							<div class=\"jtable-column-header-container\" align=\"center\">Aksi</div>
						</th>
					</tr>
				</thead>
				<tbody>
					".$content."
				</tbody>
			</table>
		</div>";
			
			
		# Tampilan Data
		
		# Halaman #
		echo "<div class=\"pagination\" style=\"padding-top: 20px;\">";
		echo "<span class=\"pg-selected\" >Prev</span>";
		$i=1;
		$status="";
		$status_="";
		$status__="";
		while($i<=$totalpage){
			if($i!=1){
				if($totalpage>=10){
					if($i<=$batas || $i>$totalpage-2){
						echo "<span class=\"pg-normal\" onclick=\"showdatareport('".$i."', '".$this->input->post('sel_diklat')."', '".$this->input->post('sel_angkatan')."','".$this->input->post('sel_tahun')."','".$this->input->post('sel_jabatan')."','".$this->input->post('sel_instansi')."','".$this->input->post('sel_kota')."','".$this->input->post('sel_kabupaten')."','".$this->input->post('sel_provinsi')."','".$this->input->post('sel_name')."')\">".$i."</span>";
					} else {
						if($status==""){
							$status="ada";
							echo " ... ";
						}
					}
				} else {
					echo "<span class=\"pg-normal\" onclick=\"showdatareport('".$i."', '".$this->input->post('sel_diklat')."', '".$this->input->post('sel_angkatan')."','".$this->input->post('sel_tahun')."','".$this->input->post('sel_jabatan')."','".$this->input->post('sel_instansi')."','".$this->input->post('sel_kota')."','".$this->input->post('sel_kabupaten')."','".$this->input->post('sel_provinsi')."','".$this->input->post('sel_name')."')\">".$i."</span>";
				}
			} else {
				echo "<span class=\"pg-selected\" >".$i."</span>";
			}
				
			$i++;
		}
			
			
		if($totalpage!=1){
			echo "<span class=\"pg-normal\" onclick=\"showdatareport('2', '".$this->input->post('sel_diklat')."', '".$this->input->post('sel_angkatan')."','".$this->input->post('sel_tahun')."','".$this->input->post('sel_jabatan')."','".$this->input->post('sel_instansi')."','".$this->input->post('sel_kota')."','".$this->input->post('sel_kabupaten')."','".$this->input->post('sel_provinsi')."','".$this->input->post('sel_name')."')\">Next</span>";
		} else {
			echo "<span class=\"pg-selected\" >Next</span>";
		}
		echo "<span class=\"pg-normal\" onclick=\"document.location='http://pusdiklat.bkpm.go.id/asset/export3.php?diklat=".$this->input->post('sel_diklat')."&angkatan=".$this->input->post('sel_angkatan')."&tahun=".$this->input->post('sel_tahun')."&jabatan=".$this->input->post('sel_jabatan')."&instansi=".$this->input->post('sel_instansi')."&kota=".$this->input->post('sel_kota')."&kabupaten=".$this->input->post('sel_kabupaten')."&provinsi=".$this->input->post('sel_provinsi')."&nama=".$this->input->post('sel_name')."&pangkat='\">Export ke xls</span></div>";
		# Halaman #
	} else {
		echo "<div align=\"center\"><h1>Maaf, data dengan kategori yang dimaksud tidak ditemukan</h1></div>";
	}
		
		
?>
</div>


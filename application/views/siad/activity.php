<div style="border-bottom: 1px #fff solid; margin-bottom: 20px;">
	<span style="font-weight: bold;"><h1>Input Kegiatan Diklat BKPM</h1></span>
</div>

<div class="content-search" align="center">
	<input type="text" size="70" placeholder="Ketikkan Kata Kunci yang berhubungan dengan Nama Kegiatan!" name="searchtext" id="searchtext" autocomplete="off" style="text-align: center" onkeyup="showdataactivity('1',this.value)"/>
	<p><input type="button" value="Tambah Kegiatan Diklat" name="addsubmit" style="width: 200px" onclick="document.location='<?php echo base_url(); ?>siad/master/activity/adddiklat'" /></p>
</div>

<div id="showdata">
	<?php
		$this->db->select('*');
		$this->db->from('activity');
		$result=$this->db->get();
		$ndata=$result->num_rows();
		$itemperpage=20;
		$totalpage=ceil($ndata/$itemperpage);
		
		if($ndata!=0){
			
			# Halaman #
			echo "<div class=\"pagination\">";
			echo "<span class=\"pg-selected\" >Prev</span>";
			$i=1;
			$status="";
			$status_="";
			$status__="";
			$batas=5;
			while($i<=$totalpage){
				if($i!=1){
					if($totalpage>=10){
						if($i<=$batas || $i>$totalpage-2){
							echo "<span class=\"pg-normal\" onclick=\"showdataactivity('".$i."',document.getElementById('searchtext').value)\">".$i."</span>";
						} else {
							if($status==""){
								$status="ada";
								echo " ... ";
							}
						}
					} else {
						echo "<span class=\"pg-normal\" onclick=\"showdataactivity('".$i."',document.getElementById('searchtext').value)\">".$i."</span>";
					}
				} else {
					echo "<span class=\"pg-selected\" >".$i."</span>";
				}
				
				$i++;
			}
			
			
			if($totalpage!=1){
				echo "<span class=\"pg-normal\" onclick=\"showdataactivity('2',document.getElementById('searchtext').value)\">Next</span>";
			} else {
				echo "<span class=\"pg-selected\" >Next</span>";
			}
			echo "</div>";
			echo "<div style=\"padding-top: 20px;\"></div>";
			# Halaman #
			
			# Tampilan Data
			echo "
			<div class=\"jtable-main-container\">
				<div class=\"jtable-busy-panel-background\" style=\"display: none; width: 940px; height: 123px;\"></div>
				<div class=\"jtable-busy-message\" style=\"display: none;\"></div>
				<div class=\"jtable-title\">
					<div class=\"jtable-title-text\">
						Daftar Kegiatan Diklat BKPM
					</div>
				</div>
				
				<table class=\"jtable\">
					<thead>
						<tr>
							<th class=\"jtable-column-header\" style=\"width: 5%;\">
								<div class=\"jtable-column-header-container\" align=\"center\">No</div>
							</th>
							<th class=\"jtable-column-header\" style=\"width: 38%;\">
								<div class=\"jtable-column-header-container\" align=\"center\">Nama Kegiatan Diklat BKPM</div>
							</th>
							<th class=\"jtable-column-header\" style=\"width: 37%;\">
								<div class=\"jtable-column-header-container\" align=\"center\">Syarat Diklat</div>
							</th>
							<th class=\"jtable-column-header\" style=\"width: 10%;\">
								<div class=\"jtable-column-header-container\" align=\"center\">Tipe Diklat</div>
							</th>
							<th class=\"jtable-column-header\" style=\"width: 10%;\">
								<div class=\"jtable-column-header-container\" align=\"center\">Aksi</div>
							</th>
						</tr>
					</thead>
					<tbody>";
				
			$this->db->select('*');
			$this->db->from('activity');
			$this->db->order_by('id','DESC');
			$this->db->limit($itemperpage,0);
			$result=$this->db->get();
			$result=$result->result();
			$i=1;
			$k=1;
			foreach($result as $row){
				if($i==2*$k-1){ 
					$even=" jtable-row-even";
					$k++;
				} else { 
					$even=""; 
				}
				
				$syarat=explode(";",$row->syarat);
				$j=1;
				$syaratshow="";
				$syaratactivity="";
				while($syarat[$j]!=""){
					$this->db->select('*');
					$this->db->from('activity');
					$this->db->where('id',$syarat[$j]);
					$result=$this->db->get();
					$result=$result->result();
					foreach($result as $row2){
						$syaratactivity=$row2->activity;
					}
					if($syaratactivity==""){ 
						$syaratshow=$syaratshow."Tidak ada<br>"; 
					} else {
						$syaratshow=$syaratshow.$syaratactivity."<br>";
					}
					$j++;
				}
				
				if($row->tipe==1){
					$tipe[$i]="Berkesinambungan";
				}
				
				if($row->tipe==2){
					$tipe[$i]="Insidental";
				}

				
				echo "<tr class=\"jtable-data-row".$even."\">
						<td><div align=\"center\">".$i."</div></td>
						<td>".$row->activity."</td>
						<td><div align=\"center\">".$syaratshow."</div></td>
						<td><div align=\"center\">".$tipe[$i]."</div></td>
						<td><div align=\"center\">
						<img src=\"".base_url()."asset/admin/images/edit.png\" alt=\"ubah\"  title=\"Ubah Data Kegiatan\" style=\"cursor: pointer\" onclick=\"document.location='".base_url()."siad/master/activity/editdiklat/?id=".$row->id."'\" />
						<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Hapus Data Kegiatan\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus kegiatan dengan nama ".$row->activity." dari database?')){ deleteactivity('".$row->id."','1',document.getElementById('searchtext').value);}\" /></div></td>
					</tr>";
				$i++;
			}
			echo"
					</tbody>
				</table>
			</div>";
			
			
			# Tampilan Data
			
			# Halaman #
			echo "<div class=\"pagination\" style=\"padding-top: 20px;\">";
			echo "<span class=\"pg-selected\" >Prev</span>";
			$i=1;
			$status="";
			$status_="";
			$status__="";
			while($i<=$totalpage){
				if($i!=1){
					if($totalpage>=10){
						if($i<=$batas || $i>$totalpage-2){
							echo "<span class=\"pg-normal\" onclick=\"showdataactivity('".$i."',document.getElementById('searchtext').value)\">".$i."</span>";
						} else {
							if($status==""){
								$status="ada";
								echo " ... ";
							}
						}
					} else {
						echo "<span class=\"pg-normal\" onclick=\"showdataactivity('".$i."',document.getElementById('searchtext').value)\">".$i."</span>";
					}
				} else {
					echo "<span class=\"pg-selected\" >".$i."</span>";
				}
				
				$i++;
			}
			
			
			if($totalpage!=1){
				echo "<span class=\"pg-normal\" onclick=\"showdataactivity('2',document.getElementById('searchtext').value)\">Next</span>";
			} else {
				echo "<span class=\"pg-selected\" >Next</span>";
			}
			echo "</div>";
			# Halaman #
		}
	?>
</div>
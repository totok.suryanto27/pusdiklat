<div style="border-bottom: 1px #fff solid; margin-bottom: 20px;">
	<span style="font-weight: bold;"><h1>Input Pengguna (User)</h1></span>
</div>

<div class="content-search" align="center">
	<input type="text" size="70" placeholder="Ketikkan Kata Kunci yang berhubungan dengan User Name!" name="searchtext" id="searchtext" autocomplete="off" style="text-align: center" onkeyup="showdatauser('1',this.value)"/>
	<p><input type="button" value="Tambah Admin" name="addsubmit" style="width: 200px" onclick="document.location='<?php echo base_url(); ?>siad/master/user/adduser'" /></p>
</div>

<div id="showdata">
	<?php
		$this->db->select('*');
		$this->db->from('user');
		$result=$this->db->get();
		$ndata=$result->num_rows();
		$itemperpage=20;
		$totalpage=ceil($ndata/$itemperpage);
		
		if($ndata!=0){
			
			# Halaman #
			echo "<div class=\"pagination\">";
			echo "<span class=\"pg-selected\" >Prev</span>";
			$i=1;
			$status="";
			$status_="";
			$status__="";
			$batas=5;
			while($i<=$totalpage){
				if($i!=1){
					if($totalpage>=10){
						if($i<=$batas || $i>$totalpage-2){
							echo "<span class=\"pg-normal\" onclick=\"showdatauser('".$i."',document.getElementById('searchtext').value)\">".$i."</span>";
						} else {
							if($status==""){
								$status="ada";
								echo " ... ";
							}
						}
					} else {
						echo "<span class=\"pg-normal\" onclick=\"showdatauser('".$i."',document.getElementById('searchtext').value)\">".$i."</span>";
					}
				} else {
					echo "<span class=\"pg-selected\" >".$i."</span>";
				}
				
				$i++;
			}
			
			
			if($totalpage!=1){
				echo "<span class=\"pg-normal\" onclick=\"showdatauser('2',document.getElementById('searchtext').value)\">Next</span>";
			} else {
				echo "<span class=\"pg-selected\" >Next</span>";
			}
			echo "</div>";
			echo "<div style=\"padding-top: 20px;\"></div>";
			# Halaman #
			
			# Tampilan Data
			echo "
			<div class=\"jtable-main-container\">
				<div class=\"jtable-busy-panel-background\" style=\"display: none; width: 940px; height: 123px;\"></div>
				<div class=\"jtable-busy-message\" style=\"display: none;\"></div>
				<div class=\"jtable-title\">
					<div class=\"jtable-title-text\">
						Daftar Pengguna (User)
					</div>
				</div>
				
				<table class=\"jtable\">
					<thead>
						<tr>
							<th class=\"jtable-column-header\" style=\"width: 5%;\">
								<div class=\"jtable-column-header-container\" align=\"center\">No</div>
							</th>
							<th class=\"jtable-column-header\" style=\"width: 45%;\">
								<div class=\"jtable-column-header-container\" align=\"center\">User Name</div>
							</th>
							<th class=\"jtable-column-header\" style=\"width: 40%;\">
								<div class=\"jtable-column-header-container\" align=\"center\">Real Name</div>
							</th>
							<th class=\"jtable-column-header\" style=\"width: 10%;\">
								<div class=\"jtable-column-header-container\" align=\"center\">Aksi</div>
							</th>
						</tr>
					</thead>
					<tbody>";
				
			$this->db->select('*');
			$this->db->from('user');
			$this->db->order_by('id','DESC');
			$this->db->limit($itemperpage,0);
			$result=$this->db->get();
			$result=$result->result();
			$i=1;
			$k=1;
			foreach($result as $row){
				if($row->id!=1000){
					if($i==2*$k-1){ 
						$even=" jtable-row-even";
						$k++;
					} else { 
						$even=""; 
					}
					
					echo "<tr class=\"jtable-data-row".$even."\">
							<td><div align=\"center\">".$i."</div></td>
							<td>".$row->user."</td>
							<td>".$row->name."</td>
							<td><div align=\"center\">
							<img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"delete\"  title=\"Hapus Data Pengguna (User)\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus Pengguna (User) dengan nama ".$row->user." dari database?')){deleteuser('".$row->id."','1',document.getElementById('searchtext').value); }\" /></div></td>
						</tr>";
					
					$i++;
				}
			}
			echo"
					</tbody>
				</table>
			</div>";
			
			
			# Tampilan Data
			
			# Halaman #
			echo "<div class=\"pagination\" style=\"padding-top: 20px;\">";
			echo "<span class=\"pg-selected\" >Prev</span>";
			$i=1;
			$status="";
			$status_="";
			$status__="";
			while($i<=$totalpage){
				if($i!=1){
					if($totalpage>=10){
						if($i<=$batas || $i>$totalpage-2){
							echo "<span class=\"pg-normal\" onclick=\"showdatauser('".$i."',document.getElementById('searchtext').value)\">".$i."</span>";
						} else {
							if($status==""){
								$status="ada";
								echo " ... ";
							}
						}
					} else {
						echo "<span class=\"pg-normal\" onclick=\"showdatauser('".$i."',document.getElementById('searchtext').value)\">".$i."</span>";
					}
				} else {
					echo "<span class=\"pg-selected\" >".$i."</span>";
				}
				
				$i++;
			}
			
			
			if($totalpage!=1){
				echo "<span class=\"pg-normal\" onclick=\"showdatauser('2',document.getElementById('searchtext').value)\">Next</span>";
			} else {
				echo "<span class=\"pg-selected\" >Next</span>";
			}
			echo "</div>";
			# Halaman #
		}
	?>
</div>
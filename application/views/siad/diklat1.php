<div style="border-bottom: 1px #fff solid; margin-bottom: 20px;">
	<span style="font-weight: bold;"><h1>Diklat BKPM (Berkesinambungan)</h1></span>
</div>
<div align="center">
	<span class="pg-normal" onclick="document.location='<?php echo base_url(); ?>siad/input/berkesinambungan/adddiklat'" style="padding: 10px 10px 10px 10px; font-size: 20px;">Tambah Diklat</span>
</div>
<div style="clear: both; margin-bottom: 20px;"></div>

<div id="showdata">
	<?php
		// Dapatkan berapa jumlah diklat berkesinambungan
		$sql="SELECT *, bkpm_events.id as eventsId FROM bkpm_events, activity WHERE bkpm_events.bkpm_events=activity.id AND activity.tipe='1'";
		$ndata=@mysql_num_rows(@mysql_query($sql));
		$itemperpage=10;
		$totalpage=ceil($ndata/$itemperpage);
		
		if($ndata!=0){
			
			# Halaman #
			echo "<div class=\"pagination\">";
			echo "<span class=\"pg-selected\" >Prev</span>";
			$i=1;
			$status="";
			$status_="";
			$status__="";
			while($i<=$totalpage){
				if($i!=1){
					if($totalpage>=10){
						if($i>$totalpage-2){
							echo "<span class=\"pg-normal\" onclick=\"showdatadiklat1('".$i."')\">".$i."</span>";
						} else {
							if($status==""){
								$status="ada";
								echo " ... ";
							}
						}
					} else {
						echo "<span class=\"pg-normal\" onclick=\"showdatadiklat1('".$i."')\">".$i."</span>";
					}
				} else {
					echo "<span class=\"pg-selected\" >".$i."</span>";
				}
				
				$i++;
			}
			
			
			if($totalpage!=1){
				echo "<span class=\"pg-normal\" onclick=\"showdatadiklat1('2')\">Next</span>";
			} else {
				echo "<span class=\"pg-selected\" >Next</span>";
			}
			echo "</div>";
			echo "<div style=\"padding-top: 20px;\"></div>";
			# Halaman #
			
			# Tampilan Data
			echo "
			<div class=\"jtable-main-container\">
				<div class=\"jtable-busy-panel-background\" style=\"display: none; width: 940px; height: 123px;\"></div>
				<div class=\"jtable-busy-message\" style=\"display: none;\"></div>
				<div class=\"jtable-title\">
					<div class=\"jtable-title-text\">
						Daftar Diklat Berkesinambungan
					</div>
				</div>
				
				<table class=\"jtable\">
					<thead>
						<tr>
							<th class=\"jtable-column-header\" style=\"width: 5%;\">
								<div class=\"jtable-column-header-container\" align=\"center\">No</div>
							</th>
							<th class=\"jtable-column-header\" style=\"width: 24%;\">
								<div class=\"jtable-column-header-container\" align=\"center\">Nama Kegiatan</div>
							</th>
							<th class=\"jtable-column-header\" style=\"width: 20%;\">
								<div class=\"jtable-column-header-container\" align=\"center\">Waktu & Tempat Pelaksanaan</div>
							</th>
							<th class=\"jtable-column-header\" style=\"width: 25%;\">
								<div class=\"jtable-column-header-container\" align=\"center\">Pengajar (Widiaiswara)</div>
							</th>
							<th class=\"jtable-column-header\" style=\"width: 15%;\">
								<div class=\"jtable-column-header-container\" align=\"center\">Jumlah Peserta</div>
							</th>
							<th class=\"jtable-column-header\" style=\"width: 6%;\">
								<div class=\"jtable-column-header-container\" align=\"center\">Aksi</div>
							</th>
						</tr>
					</thead>
					<tbody>";
				
			$query=$this->db->query("SELECT *, bkpm_events.id as eventsId FROM bkpm_events, activity WHERE bkpm_events.bkpm_events=activity.id AND activity.tipe='1' ORDER BY bkpm_events.id DESC LIMIT 0,10");
			$result=$query->result();
			$i=1;
			$k=1;
			foreach($result as $row){
				$lect=explode(";",$row->lecture);
				$j=0;
				$lecture="";
				while($lect[$j]!=""){
					if($lect[$j]=="1000"){
						$lecture="<div align=\"center\">Tidak ada info</div>";
					} else {
						$this->db->select('*');
						$this->db->from('lecture');
						$this->db->where('id',$lect[$j]);
						$this->db->limit(1);
						$result=$this->db->get();
						$result=$result->result();
						foreach($result as $row2){
							$lecture=$lecture.($j+1).". ".$row2->lecture."<br />";
						}
					}
					$j++;
				}
				
				$this->db->select('*');
				$this->db->from('dataalumni');
				$result=$this->db->get();
				$result=$result->result();
				$total_alumni=0;
				$j=0;
				$count_alumni=0;
				foreach($result as $row2){
					$str_diklat=explode(";",$row2->diklat);
					$str_angkatan=explode(";",$row2->angkatan);
					$str_tahun=explode(";",$row2->tahun);
						
					$l=1;
					
					while($str_diklat[$l]!=""){
						if($str_diklat[$l]==$row->bkpm_events && $str_angkatan[$l]==$row->angkatan && $str_tahun[$l]==$row->thnakt){
							$count_alumni++;
						}
						$l++;
					}
					$j++;
				}
				
				if($i==2*$k-1){ 
					$even=" jtable-row-even";
					$k++;
				} else { 
					$even=""; 
				}
				
				$this->db->select('*');
				$this->db->from('city');
				$this->db->where('id',$row->city);
				$this->db->limit(1);
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row2){
					$city=$row2->city;
				}
				
				echo "<tr class=\"jtable-data-row".$even."\">
						<td><div align=\"center\">".$i."</div></td>
						<td><div align=\"center\">
								<form name=\"frmDiklat".$i."\" method=\"post\" id=\"frmDiklat".$i."\" action=\"".base_url()."siad/input/berkesinambungan/list/?\">
									<input name=\"diklat\" type=\"hidden\" value=\"".$row->activity."\" />
									<input name=\"angkatan\" type=\"hidden\" value=\"".$row->angkatan."\" />
									<input name=\"tahun\" type=\"hidden\" value=\"".$row->thnakt."\" />
									<a onclick=\"document.getElementById('frmDiklat".$i."').submit(); return false;\" style=\"cursor: pointer; text-decoration: underline; color: blue; \">".$row->activity." Angkatan ".$row->angkatan." Tahun ".$row->thnakt."</a>
								</form>
							</div>
						</td>
						<td><div align=\"center\">".$row->time_do.", ".$row->place_do." ".$city." - ".$row->province."</div></td>
						<td>".$lecture."</td>
						<td><div align=\"center\">".$count_alumni."</center></td>
						<td>
							<div align=\"center\" style=\"padding-left: 10px;\">
								
								<form name=\"frmAdd".$i."\" method=\"post\" id=\"frmAdd".$i."\" action=\"".base_url()."siad/input/berkesinambungan/addmember/?\" style=\"float: left;\">
									<input name=\"diklat\" type=\"hidden\" value=\"".$row->id."\" />
									<input name=\"angkatan\" type=\"hidden\" value=\"".$row->angkatan."\" />
									<input name=\"tahun\" type=\"hidden\" value=\"".$row->thnakt."\" />
									<a onclick=\"document.getElementById('frmAdd".$i."').submit(); return false;\" style=\"cursor: pointer;\"><img src=\"".base_url()."asset/admin/images/add.png\" alt=\"Tambah Peserta\" style=\"cursor:pointer\" width=\"16\" height=\"16\"></a>
								</form>
								<form name=\"frmDel".$i."\" method=\"post\" id=\"frmDel".$i."\" action=\"".base_url()."siad/input/berkesinambungan/delete/?\" style=\"float: left;\">
									<input name=\"eventsid\" type=\"hidden\" value=\"".$row->eventsId."\" />
									<input name=\"diklat\" type=\"hidden\" value=\"".$row->id."\" />
									<input name=\"angkatan\" type=\"hidden\" value=\"".$row->angkatan."\" />
									<input name=\"tahun\" type=\"hidden\" value=\"".$row->thnakt."\" />
									<a onClick=\"if(confirm('Anda ingin menghapus ".$row->activity." Angkatan ".$row->angkatan." Tahun ".$row->thnakt." dari database? Perhatian!! Semua data alumni yang berhubungan dengan Diklat ini akan terhapus juga.')){document.getElementById('frmDel".$i."').submit(); return false; } \" title=\"Delete\"><img src=\"".base_url()."asset/admin/images/delete.png\" alt=\"Delete\" style=\"cursor:pointer;\"></a>
								</form>
								
							</div></td>
					</tr>";
				$i++;
			}

			
			echo"
					</tbody>
				</table>
			</div>";
			
			
			# Tampilan Data
			
			# Halaman #
			echo "<div class=\"pagination\" style=\"padding-top: 20px;\">";
			echo "<span class=\"pg-selected\" >Prev</span>";
			$i=1;
			$status="";
			$status_="";
			$status__="";
			while($i<=$totalpage){
				if($i!=1){
					if($totalpage>=10){
						if($i>$totalpage-2){
							echo "<span class=\"pg-normal\" onclick=\"showdatadiklat1('".$i."')\">".$i."</span>";
						} else {
							if($status==""){
								$status="ada";
								echo " ... ";
							}
						}
					} else {
						echo "<span class=\"pg-normal\" onclick=\"showdatadiklat1('".$i."')\">".$i."</span>";
					}
				} else {
					echo "<span class=\"pg-selected\" >".$i."</span>";
				}
				
				$i++;
			}
			
			
			if($totalpage!=1){
				echo "<span class=\"pg-normal\" onclick=\"showdatadiklat1('2')\">Next</span>";
			} else {
				echo "<span class=\"pg-selected\" >Next</span>";
			}
			echo "</div>";
			# Halaman #
		}
	?>
</div>



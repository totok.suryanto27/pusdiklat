<?php
	if($_GET['id']=="" && $_GET['diklat']==""){
		echo"<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."siad/laporan\" />";
		exit;
	}
	
	$this->db->select('*');
	$this->db->from('dataalumni');
	$this->db->where('id',$_GET['id']);
	$result=$this->db->get();
	$result=$result->result();
	foreach($result as $row_alumni){
		$nip=explode(" ",$row_alumni->nip);
		$str_diklat=explode(";",$row_alumni->diklat);
		$nama=$row_alumni->nama;
		$ttl=$row_alumni->ttl;
		$rumah=$row_alumni->rumah;
		$rumah_kota=$row_alumni->rumah_kota;
		$rumah_prov=$row_alumni->rumah_prov;
		$rumah_telp=$row_alumni->rumah_telp;
		$hp_telp=$row_alumni->hp_telp;
		$emailaddress=$row_alumni->email;
		$facebook=$row_alumni->facebook;
	}
	
	$i=1;
	$diklat="";
	while($str_diklat[$i]!=""){
		$this->db->select('*');
		$this->db->from('activity');
		$this->db->where('id',$str_diklat[$i]);
		$result=$this->db->get();
		$result=$result->result();
		foreach($result as $row_activity){
			$activity=$row_activity->activity;
		}
		if($str_diklat[$i]==$_GET['diklat']){ $select="selected"; } else { $select=""; }
		$diklat=$diklat.
			"<option value=\"".$str_diklat[$i]."\" ".$select.">".$activity."</option>";
		$i++;
	}
	
	
?>
<div style="border-bottom: 1px #fff solid; margin-bottom: 20px;">
	<span style="font-weight: bold;"><h1>Edit Data Alumni Diklat BKPM</h1></span>
</div>
<form method="post" action="" enctype="multipart/form-data" onsubmit="return validateFormMember()">
	<input type="hidden" name="idmember" value="<?php echo $_GET['id']; ?>" />
    <input type="hidden" name="iddiklat" value="<?php echo $_GET['diklat']; ?>" />
	<div style="margin-top: 10px;">			
		<div style="float:left; display: inline; height: 10px;">
			<input type="checkbox" disabled />
		</div>
				
		<div style="float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;">
			Nomor Induk Pegawai (NIP)
		</div>
				
		<div style="float:left; display: inline; padding-left: 10px; vertical-align: text-top;">
			<input type="text" size="10" name="nip_part1" id="nip_part1" style="text-align: center;" value="<?php echo $nip[0]; ?>"  onkeydown="validateevent(event)"/>
			-
			<input type="text" size="8" name="nip_part2" id="nip_part2" style="text-align: center;" value="<?php echo $nip[1]; ?>"   onkeydown="validateevent(event)"/>
			-
			<input type="text" size="3" name="nip_part3" id="nip_part3" style="text-align: center;" value="<?php echo $nip[2]; ?>"   onkeydown="validateevent(event)"/>
			-
			<input type="text" size="5" name="nip_part4" id="nip_part4" style="text-align: center;" value="<?php echo $nip[3]; ?>"   onkeydown="validateevent(event)"/>
			&nbsp;
					
		</div>
				
		<div style="clear: both; padding-top: 10px;"></div>
			
		<div style="float:left; display: inline; height: 10px;">
			<input type="checkbox" disabled />
		</div>
				
		<div style="float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;">
			Diklat (data yang akan diubah)
		</div>
				
		<div style="float:left; display: inline; padding-left: 10px; vertical-align: text-top;">
			<select name="diklat" id="diklat" disabled>
				<option value="">-- Pilih Salah Satu --</option>
				<?php echo $diklat; ?>
			</select>
		</div>
				
		<div style="clear: both; padding-top: 10px;"></div>
			
		<div id="showalumni">
		
		<?php
			$this->db->select('*');
			$this->db->from('city');
			$this->db->where('id',$rumah_kota);
			$result=$this->db->get();
			$result=$result->result();
			$tipe="";
			foreach($result as $row_city){
				$city=$row_city->city;
				$tipe=$row_city->tipe;
			}
			
			
			if($tipe=="Kota"){
				
				//echo "ini kota";
				$checkedkotahome="checked";
				$showcityhome=
					"<select id=\"homecity\" name=\"homecity\" disabled>
						<option value=\"\">-= Pilih Salah Satu =-</option>
						<option value=\"".$rumah_kota."\" selected>".$city."</option>
					</select>";
			}
			
			if($tipe=="Kabupaten"){
				//echo "ini kabupaten";
				$checkedkabupatenhome="checked";
				$showcityhome=
					"<select id=\"homecity\" name=\"homecity\" disabled>
						<option value=\"\">-= Pilih Salah Satu =-</option>
						<option value=\"".$rumah_kota."\" selected>".$city."</option>
					</select>";
			}
			$checkedtidakadainfohome="";
			if($tipe==""){
				//echo "ini tidak ada info";
				$checkedtidakadainfohome="checked";
				$showcityhome="";
			}
			
			if($checkedtidakadainfohome=="checked"){
				$provhome="Tidak ada info";
			} else {
				$provhome=$rumah_prov;
			}
				
			$showprovhome=
				"<div style=\"float:left; display: inline; height: 10px;\">
					<input type=\"checkbox\" disabled />
				</div>
								
				<div style=\"float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;\">
					Provinsi
				</div>
					
				<div style=\"float:left; display: inline; padding-left: 10px; vertical-align: text-top;\">
					<input type=\"text\" name=\"provinsi\" id=\"provinsi\" size=\"40\" value=\"".$provhome."\" style=\"text-align: center;\" disabled />
				</div>";
				
			$notelprumah=explode('-',$rumah_telp);
			if($notelprumah[0]==""){
				$kodeareahome="";
			} else {
				$kodeareahome=$notelprumah[0];
			}
				
			if($notelprumah[1]==""){
				$notelphome="";
			} else {
				$notelphome=$notelprumah[1];
			}
				
			$hp=explode(';',$hp_telp);
			$i=0;
			$nhp=0;
			while($hp[$i]!=""){
				$nhp++;
				$i++;
			}
				
			if($nhp==1){ $status1="selected"; }
			if($nhp==2){ $status2="selected"; }
			if($nhp==3){ $status3="selected"; }
			if($nhp==4){ $status4="selected"; }
			if($nhp==5){ $status5="selected"; }
			if($nhp==0){ $status6="selected"; }
				
			$showcontact="";
			$i=1;
			while($i<=$nhp){
				$showcontact=$showcontact.
					"<div style=\"float:left; display: inline; height: 10px;\">
						<input type=\"checkbox\" disabled />
					</div>
									
					<div style=\"float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;\">
						Nomor Kontak ke-".$i."
					</div>
						
					<div style=\"float:left; display: inline; padding-left: 10px; vertical-align: text-top;\">
						<input type=\"text\" size=\"20\" name=\"contact".$i."\" id=\"contact".$i."\" placeholder=\"08123456789\" style=\"text-align: center;\" value=\"".$hp[$i-1]."\" />
					</div>
					
					<div style=\"clear: both;\"></div>";
				$i++;
			}
				
			if($emailaddress==""){ $email=""; } else { $email=$emailaddress; }
			if($facebook==""){ $fb=""; } else { $fb=$facebook; }	
			
			
			$strdiklat=explode(';',$row_alumni->diklat);
			$strinstansi=explode(';',$row_alumni->instansi);
			$strwilayah=explode(';',$row_alumni->wilayah);
			
			// Data instansi dan yang berhubungan ditampilkan adalah data yang terupdate
			
			$i=1;
			while($strdiklat[$i]!=""){
				$i++;
			}
			
			$pos=$i-1;
			
			// Get instansi di posisi yang akan diedit
			
			$this->db->select('*');
			$this->db->from('dept');
			$result=$this->db->get();
			$result=$result->result();
			$instansi="";
			foreach($result as $row_dept){
				if($row_dept->id==$strinstansi[$pos]){ $selected="selected"; } else { $selected=""; }
				$instansi=$instansi."<option value=\"".$row_dept->id."\" ".$selected.">".$row_dept->dept."</option>";
			}
			
			// Get wilayah Instansi
			if($strwilayah[2*($pos)-2]=="Kota"){
				$checkedkotaoffice="checked";
				$showcityoffice="<input type=\"text\" size=\"30\" name=\"wil_kantor\" id=\"wil_kantor\" value=\"".$strwilayah[2*($pos)-1]."\" style=\"text-align: center;\" />";
			}
			if($strwilayah[2*($pos)-2]=="Kabupaten"){
				$checkedkabupatenoffice="checked";
				$showcityoffice="<input type=\"text\" size=\"30\" name=\"wil_kantor\" id=\"wil_kantor\" value=\"".$strwilayah[2*($pos)-1]."\" style=\"text-align: center;\" />";
			}
			if($strwilayah[2*($pos)-2]=="Provinsi"){
				$checkedprovinsioffice="checked";
				$showcityoffice="<input type=\"text\" size=\"30\" name=\"wil_kantor\" id=\"wil_kantor\" value=\"".$strwilayah[2*($pos)-1]."\" style=\"text-align: center;\" />";
			}
			
			if($strwilayah[2*($pos)-2]=="pusat"){
				$checkedpusatoffice="checked";
				//$showcityoffice=
				//	"<input type=\"text\" size=\"30\" id=\"officecity\" name=\"officecity\" value=\"".$strwilayah[2*($pos)-1]."\" disabled style=\"text-align: center\" />";
			}
			
			// Get Info Jabatan
			
			$strjabatan=explode(';',$row_alumni->jabatan);
			
			$this->db->select('*');
			$this->db->from('position');
			$result=$this->db->get();
			$result=$result->result();
			$jabatan="";
			foreach($result as $row_jabatan){
				if($row_jabatan->id==$strjabatan[$pos]){ $selected="selected"; } else { $selected=""; }
				$jabatan=$jabatan."<option value=\"".$row_jabatan->id."\" ".$selected.">".$row_jabatan->position."</option>";
			}
			
			// Get Info Bagian/Bidang
			if($strjabatan[$pos]!=0){
				$bagian=explode(';', $row_alumni->bagian);
				$bidang=$bagian[$pos];
				if($bidang==" "){ $bidang=""; }
			}
			
			// Get Info Pangkat
			$strpangkat=explode(';', $row_alumni->pangkat);
			
			$this->db->select('*');
			$this->db->from('grade');
			$result=$this->db->get();
			$result=$result->result();
			$pangkat="";
			foreach($result as $row_pangkat){
				if($row_pangkat->id==$strpangkat[$pos]){ $selected="selected"; } else { $selected=""; }
				$pangkat=$pangkat."<option value=\"".$row_pangkat->id."\" ".$selected.">".$row_pangkat->grade."</option>";
			}
			
			// Get Info Alamat Kantor
			$kantoralamat=explode(';', $row_alumni->kantor_alamat);
			if($kantoralamat[$pos]!="0"){
				$alamatkantor=$kantoralamat[$pos];
			}
			
			// Get Info daerah kantor
			$kantor_kota=explode(';', $row_alumni->kantor_kota);
			
			$this->db->select('*');
			$this->db->from('city');
			$this->db->where('id',$kantor_kota[$pos]);
			$result=$this->db->get();
			$result=$result->result();
			foreach($result as $row_city2){
				$tipe2=$row_city2->tipe;
			}
			
			if($row_city2->tipe=="Kota"){
				$checkedkotaoffice2="checked";
				$showcityoffice2=
					"<input type=\"hidden\" size=\"30\" id=\"officecity2\" name=\"officecity2\" value=\"".$row_city2->id."\"/>
					<input type=\"text\" size=\"30\" value=\"".$row_city2->city."\" disabled style=\"text-align: center\" />";
			}
			
			if($row_city2->tipe=="Kabupaten"){
				$checkedkabupatenoffice2="checked";
				$showcityoffice2=
					"<input type=\"hidden\" size=\"30\" id=\"officecity2\" name=\"officecity2\" value=\"".$row_city2->id."\"/>
					<input type=\"text\" size=\"30\" value=\"".$row_city2->city."\" disabled style=\"text-align: center\" />";
			}
			
			$checkedtidakadainfooffice2="";
			if($row_city2->tipe==""){
				$checkedtidakadainfooffice2="checked";
				$showcityoffice2="";
			}
			
			$kantorprov=explode(';', $row_alumni->kantor_prov);
			
			if($checkedtidakadainfooffice2=="checked"){
				$provoffice2="Tidak ada info";
			} else {
				$provoffice2=$kantorprov[$pos];
			}
			
			$showprovoffice2=
				"<div style=\"float:left; display: inline; height: 10px;\">
					<input type=\"checkbox\" disabled />
				</div>
									
				<div style=\"float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;\">
					Provinsi
				</div>
						
				<div style=\"float:left; display: inline; padding-left: 10px; vertical-align: text-top;\">
					<input type=\"text\" name=\"provinsi_ktr\" id=\"provinsi_ktr\" size=\"40\" value=\"".$provoffice2."\" style=\"text-align: center;\" disabled />
				</div>";
			
			
			// Get Info no telp kantor
			$notelpkantor=explode(';',$row_alumni->kantor_telp);
			$notelpkantor2=explode('-',$notelpkantor[$pos]);
				
				
			if($notelpkantor2[0]=="" || $notelpkantor2[0]=="0"){
				$kodeareakantor="";
			} else {
				$kodeareakantor=$notelpkantor2[0];
			}
					
			if($notelpkantor2[1]=="" || $notelpkantor2[1]=="0"){
				$notelpkantor3="";
			} else {
				$notelpkantor3=$notelpkantor2[1];
			}
				
			$nofaxkantor=explode(';',$row_alumni->fax);
			$nofaxkantor2=explode('-',$nofaxkantor[$pos]);
				
			if($nofaxkantor2[0]=="" || $nofaxkantor2[0]=="0"){
				$kodeareafaxkantor="";
			} else {
				$kodeareafaxkantor=$nofaxkantor2[0];
			}
					
			if($nofaxkantor2[1]=="" || $nofaxkantor2[1]=="0"){
				$nofaxkantor3="";
			} else {
				$nofaxkantor3=$nofaxkantor2[1];
			}
			$situs=explode(';', $row_alumni->website);
				
			if($situs[$pos]!="0"){
				$website=$situs[$pos];
				if($website==" "){ $website=""; }
			}
			
			
		
			echo
				"<div class=\"content-alumni\">
					<input type=\"submit\" name=\"member-update\" value=\"Update Data\" />
				</div>
				<div class=\"content-alumni\" style=\"margin-top: 10px;\">
					<h2>~ Data Pribadi ~</h2>
						
					<div style=\"margin-top: 10px;\">
						<div style=\"float:left; display: inline; height: 10px;\">
							<input type=\"checkbox\" disabled />
						</div>
							
						<div style=\"float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;\">
							Nama Lengkap Peserta
						</div>
							
						<div style=\"float:left; display: inline; padding-left: 10px;\">
							<input type=\"text\" size=\"70\" name=\"nama\" id=\"nama\" value=\"".$nama."\"/> 
							<span style=\"color: red; font-weight: bold;\">*</span>
						</div>
										
						<div style=\"clear: both; padding-top: 10px;\"></div>
							
						<div style=\"float:left; display: inline; height: 10px;\">
							<input type=\"checkbox\" disabled />
						</div>
							
						<div style=\"float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;\">
							Tempat Lahir
						</div>";
			
			$ttl=explode(';',$ttl);
			
		echo			
			"			<div style=\"float:left; display: inline; padding-left: 10px;\">
							<input type=\"text\" size=\"70\" name=\"birthplace\" id=\"birthplace\" value=\"".$ttl[0]."\" /> 
						</div>
										
						<div style=\"clear: both; padding-top: 10px;\"></div>
						
						<div style=\"float:left; display: inline; height: 10px;\">
							<input type=\"checkbox\" disabled />
						</div>
							
						<div style=\"float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;\">
							Tanggal Lahir
						</div>
							
						<div style=\"float:left; display: inline; padding-left: 10px;\">
							<select name=\"birthdate\" id=\"birthdate\" style=\"float: left; margin-right: 10px;\" >";
		$birthdate=explode(' ',$ttl[1]);						
		$i=1;
		while($i<=31){
			if($i==$birthdate[0]){ $select="selected"; } else { $select=""; }
			if($i<10){
				echo "<option value=\"0".$i."\" ".$select.">0".$i."</option>";
			} else {
				echo "<option value=\"".$i."\"".$select.">".$i."</option>";
			}
			$i++;
		}
		echo
			"				</select>
							<select name=\"birthmonth\" id=\"birthmonth\" style=\"float: left; margin-right: 10px;\" >";
		if($birthdate[1]=="Januari" || $birthdate[1]=="January"){ $birthmonth=1; }
		if($birthdate[1]=="Februari" || $birthdate[1]=="February"){ $birthmonth=2; }
		if($birthdate[1]=="Maret" || $birthdate[1]=="March"){ $birthmonth=3; }
		if($birthdate[1]=="April"){ $birthmonth=4; }
		if($birthdate[1]=="Mei" || $birthdate[1]=="May"){ $birthmonth=5; }
		if($birthdate[1]=="Juni" || $birthdate[1]=="June"){ $birthmonth=6; }
		if($birthdate[1]=="Juli" || $birthdate[1]=="July"){ $birthmonth=7; }
		if($birthdate[1]=="Agustus" || $birthdate[1]=="August"){ $birthmonth=8; }
		if($birthdate[1]=="September"){ $birthmonth=9; }
		if($birthdate[1]=="Oktober" || $birthdate[1]=="October"){ $birthmonth=10; }
		if($birthdate[1]=="November"){ $birthmonth=11; }
		if($birthdate[1]=="Desember" || $birthdate[1]=="December"){ $birthmonth=12; }
		
		$i=1;
		while($i<=12){
			if($i==$birthmonth){ $select="selected"; } else { $select=""; }
			if($i<10){
				echo "<option value=\"".date("F",mktime(0, 0, 0, $i, 1, date("Y")))."\" ".$select.">".date("F",mktime(0, 0, 0, $i, 1, date("Y")))."</option>";
			} else {
				echo "<option value=\"".date("F",mktime(0, 0, 0, $i, 1, date("Y")))."\"".$select.">".date("F",mktime(0, 0, 0, $i, 1, date("Y")))."</option>";
			}
			$i++;
		}
		echo
			"				</select>
							<input type=\"text\" name=\"birthyear\" id=\"birthyear\" size=\"4\" value=\"".$birthdate[2]."\" style=\"float: left; text-align: center;\" onkeydown=\"validateevent(event)\"/>
						</div>
										
						<div style=\"clear: both; padding-top: 10px;\"></div>
							
						<div style=\"float:left; display: inline; height: 10px;\">
							<input type=\"checkbox\" disabled />
						</div>
							
						<div style=\"float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;\">
							Alamat Rumah
						</div>
							
						<div style=\"float:left; display: inline; padding-left: 10px;\">
							<textarea id=\"addreshome\" name=\"addreshome\" rows=\"5\" cols=\"50\" >".$rumah."</textarea>
						</div>
										
						<div style=\"clear: both; padding-top: 10px;\"></div>
							
						<div style=\"float:left; display: inline; height: 10px;\">
							<input type=\"checkbox\" disabled />
						</div>
							
						<div style=\"float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;\">
							Daerah
						</div>
							
						<div style=\"float:left; display: inline; padding-left: 10px;\">
							<div style=\"float: left;\">
								<input type=\"radio\" name=\"areahome\" id=\"kota_rmh\" onclick=\"showtipe('home',this.value,'showcityhome')\" value=\"kota\"  ".$checkedkotahome."/> Kota
								<input type=\"radio\" name=\"areahome\" id=\"kabupaten_rmh\" onclick=\"showtipe('home',this.value,'showcityhome')\" value=\"kabupaten\"  ".$checkedkabupatenhome."/> Kabupaten
								<input type=\"radio\" name=\"areahome\" id=\"tidakadainfo_rmh\" onclick=\"showtipe('home',this.value,'showcityhome')\" value=\"tidakadainfo\"  ".$checkedtidakadainfohome."/> Tidak ada info
								&nbsp;
							</div>
							<div id=\"showcityhome\" style=\"float: left;\">".$showcityhome."</div>
						</div>
										
						<div style=\"clear: both; padding-top: 10px;\"></div>
							
						<div id=\"showprovhome\">".$showprovhome."</div>
							
						<div style=\"clear: both; padding-top: 10px;\"></div>
							
						<div style=\"float:left; display: inline; height: 10px;\">
							<input type=\"checkbox\" disabled />
						</div>
							
						<div style=\"float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;\">
							No Telp Rumah
						</div>
							
						<div style=\"float:left; display: inline; padding-left: 10px;\">
							(<input type=\"text\" size=\"4\" name=\"kodearea\" id=\"kodearea\" placeholder=\"021\" style=\"text-align: center;\" value=\"".$kodeareahome."\" onkeydown=\"validateevent(event)\"/>)
							- 
							<input type=\"text\" size=\"10\" name=\"notelp\" id=\"notelp\" placeholder=\"12345678\" style=\"text-align: center;\" value=\"".$notelphome."\" onkeydown=\"validateevent(event)\"/>
						</div>
							
						<div style=\"clear: both; padding-top: 10px;\"></div>
							
						<div style=\"float:left; display: inline; height: 10px;\">
							<input type=\"checkbox\" disabled />
						</div>
							
						<div style=\"float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;\">
							No HP yang bisa dihubungi
						</div>
							
						<div style=\"float:left; display: inline; padding-left: 10px;\">
							<select id=\"ncontact\" name=\"ncontact\" onchange=\"showcontact(this.value)\" >
								<option value=\"\">-= Pilih Salah Satu =-</option>
								<option value=\"1\" ".$status1.">1 Nomor</option>
								<option value=\"2\" ".$status2.">2 Nomor</option>
								<option value=\"3\" ".$status3.">3 Nomor</option>
								<option value=\"4\" ".$status4.">4 Nomor</option>
								<option value=\"5\" ".$status5.">5 Nomor</option>
								<option value=\"none\" ".$status6.">Tidak ada info</option>
							</select>
						</div>
							
						<div style=\"clear: both; padding-top: 10px;\"></div>
							
						<div id=\"showcontact\">".$showcontact."</div>
							
						<div style=\"clear: both; padding-top: 10px;\"></div>
							
						<div style=\"float:left; display: inline; height: 10px;\">
							<input type=\"checkbox\" disabled />
						</div>
							
						<div style=\"float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;\">
							Alamat Email
						</div>
							
						<div style=\"float:left; display: inline; padding-left: 10px;\">
							<input type=\"text\" size=\"70\" name=\"email\" id=\"email\" placeholder=\"email@email.com\" value=\"".$email."\" />
						</div>
							
						<div style=\"clear: both; padding-top: 10px;\"></div>
							
						<div style=\"float:left; display: inline; height: 10px;\">
							<input type=\"checkbox\" disabled />
						</div>
							
						<div style=\"float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;\">
							Alamat Facebook
						</div>
							
						<div style=\"float:left; display: inline; padding-left: 10px;\">
							<input type=\"text\" size=\"70\" name=\"facebook\" id=\"facebook\" placeholder=\"email@email.com\" value=\"".$fb."\" />
						</div>
							
						<div style=\"clear: both; padding-top: 10px;\"></div>
						
						<div style=\"float:left; display: inline; height: 10px;\">
							<span style=\"color: red; font-weight: bold; font-style: italic; font-size: 10px;\">* Wajib diisi</span>
						</div>
							
					</div>
						
						
						
						
				</div>
					
				<div class=\"content-alumni\" style=\"margin-top: 10px; margin-bottom: 10px;\">
						<h2>~ Data Instansi ~</h2>
							
						<div style=\"margin-top: 10px;\">
								
							<div style=\"float:left; display: inline; height: 10px;\">
								<input type=\"checkbox\" disabled />
							</div>
								
							<div style=\"float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;\">
								Nama Instansi
							</div>
								
							<div style=\"float:left; display: inline; padding-left: 10px;\">
								<select name=\"instansi\" id=\"instansi\"  style=\"width: 450px;\">
									<option value=\"\">-= Pilih Salah Satu =-</option>
									".$instansi."
								</select> 
								<span style=\"font-size: 10px;\">Klik <a onclick=\"document.location='../../../../master/departement/adddept';\">disini</a> bila instansi yang anda cari tidak ada</span>
								<span style=\"color: red; font-weight: bold;\">*</span>
							</div>
											
							<div style=\"clear: both; padding-top: 10px;\"></div>
							
							<div style=\"float:left; display: inline; height: 10px;\">
								<input type=\"checkbox\" disabled />
							</div>
								
							<div style=\"float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;\">
								Daerah
							</div>
								
							<div style=\"float:left; display: inline; padding-left: 10px;\">
								<div style=\"float: left;\">
									<input type=\"radio\" name=\"daerahkantor\" id=\"kota\" onclick=\"showtipe('office',this.value,'showcityoffice')\" value=\"kota\" ".$checkedkotaoffice." /> Kota
									<input type=\"radio\" name=\"daerahkantor\" id=\"kabupaten\" onclick=\"showtipe('office',this.value,'showcityoffice')\" value=\"kabupaten\" ".$checkedkabupatenoffice." /> Kabupaten
									<input type=\"radio\" name=\"daerahkantor\" id=\"provinsi_btn\" onclick=\"showtipe('office',this.value,'showcityoffice')\" value=\"provinsi\" ".$checkedprovinsioffice." /> Provinsi
									<input type=\"radio\" name=\"daerahkantor\" id=\"pusat\" onclick=\"showtipe('office',this.value,'showcityoffice')\" value=\"pusat\" ".$checkedpusatoffice." /> Pusat
									&nbsp;
								</div>
								<div id=\"showcityoffice\" style=\"float: left;\">
									".$showcityoffice."
								</div>
								<span style=\"color: red; font-weight: bold;\">*</span>
							</div>
											
							<div style=\"clear: both; padding-top: 10px;\"></div>
							
							<div id=\"showprovoffice\">".$showprovoffice."</div>
							
							<div style=\"clear: both; padding-top: 10px;\"></div>
								
							<div style=\"float:left; display: inline; height: 10px;\">
								<input type=\"checkbox\" disabled />
							</div>
								
							<div style=\"float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;\">
								Jabatan
							</div>
								
							<div style=\"float:left; display: inline; padding-left: 10px;\">
								<select name=\"jabatan\" id=\"jabatan\" >
									<option value=\"\">-= Pilih Salah Satu =-</option>
									".$jabatan."
								</select> 
								<span style=\"font-size: 10px;\">Klik <a onclick=\"document.location='../../../../master/position/addposition';\">disini</a> bila jabatan yang anda cari tidak ada</span>
								<!--<span style=\"color: red; font-weight: bold;\">*</span>-->
							</div>
											
							<div style=\"clear: both; padding-top: 10px;\"></div>
								
							<div style=\"float:left; display: inline; height: 10px;\">
								<input type=\"checkbox\" disabled />
							</div>
								
							<div style=\"float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;\">
								Bagian (Bidang)
							</div>
								
							<div style=\"float:left; display: inline; padding-left: 10px;\">
								<input type=\"text\" size=\"70\" name=\"bagian\" id=\"bagian\" placeholder=\"Contoh: Keuangan, Promosi & Kerjasama, dll\"  value=\"".$bidang."\" />
							</div>
											
							<div style=\"clear: both; padding-top: 10px;\"></div>
								
							<div style=\"float:left; display: inline; height: 10px;\">
								<input type=\"checkbox\" disabled />
							</div>
								
							<div style=\"float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;\">
								Pangkat (Golongan)
							</div>
								
							<div style=\"float:left; display: inline; padding-left: 10px;\">
								<select name=\"pangkat\" id=\"pangkat\" >
									<option value=\"\">-= Pilih Salah Satu =-</option>
									".$pangkat."
								</select> 
								<span style=\"font-size: 10px;\">Klik <a onclick=\"document.location='../../../../master/grade/addgrade';\">disini</a> bila pangkat/golongan yang anda cari tidak ada</span>
							</div>
							
							<div style=\"clear: both; padding-top: 10px;\"></div>
							
							<div style=\"float:left; display: inline; height: 10px;\">
								<input type=\"checkbox\" disabled />
							</div>
								
							<div style=\"float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;\">
								Alamat Kantor
							</div>
								
							<div style=\"float:left; display: inline; padding-left: 10px;\">
								<textarea id=\"addresoffice\" name=\"addresoffice\" rows=\"5\" cols=\"50\" >".$alamatkantor."</textarea>
							</div>
											
							<div style=\"clear: both; padding-top: 10px;\"></div>
								
							<div style=\"float:left; display: inline; height: 10px;\">
								&nbsp;
							</div>
								
							<div style=\"float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;\">
								&nbsp;
							</div>
								
							<div style=\"float:left; display: inline; padding-left: 30px;\">
								<div style=\"float: left;\">
									<input type=\"radio\" name=\"daerahkantor2\" id=\"kota2\" onclick=\"showtipe('office2',this.value,'showcityoffice2')\" value=\"kota\" ".$checkedkotaoffice2." /> Kota
									<input type=\"radio\" name=\"daerahkantor2\" id=\"kabupaten2\" onclick=\"showtipe('office2',this.value,'showcityoffice2')\" value=\"kabupaten\" ".$checkedkabupatenoffice2." /> Kabupaten
									<input type=\"radio\" name=\"daerahkantor2\" id=\"tidakadainfo2\" onclick=\"showtipe('office2',this.value,'showcityoffice2')\" value=\"tidakadainfo\"  ".$checkedtidakadainfooffice2."/> Tidak ada info
									&nbsp;
								</div>
								<div id=\"showcityoffice2\" style=\"float: left;\">".$showcityoffice2."</div>
							</div>
											
							<div style=\"clear: both; padding-top: 10px;\"></div>
							
							<div id=\"showprovoffice2\">".$showprovoffice2."</div>
							
							<div style=\"clear: both; padding-top: 10px;\"></div>
							
							<div style=\"float:left; display: inline; height: 10px;\">
								<input type=\"checkbox\" disabled />
							</div>
								
							<div style=\"float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;\">
								No Telp Kantor
							</div>
								
							<div style=\"float:left; display: inline; padding-left: 10px;\">
								(<input type=\"text\" size=\"4\" name=\"kodeareaoffice\" id=\"kodeareaoffice\" placeholder=\"021\" style=\"text-align: center;\" value=\"".$kodeareakantor."\"  onkeydown=\"validateevent(event)\"/>)
								- 
								<input type=\"text\" size=\"10\" name=\"notelpoffice\" id=\"notelpoffice\" placeholder=\"12345678\" style=\"text-align: center;\" value=\"".$notelpkantor3."\" onkeydown=\"validateevent(event)\"/>
							</div>
								
							<div style=\"clear: both; padding-top: 10px;\"></div>
							
							<div style=\"float:left; display: inline; height: 10px;\">
								<input type=\"checkbox\" disabled />
							</div>
								
							<div style=\"float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;\">
								No Fax Kantor
							</div>
								
							<div style=\"float:left; display: inline; padding-left: 10px;\">
								(<input type=\"text\" size=\"4\" name=\"kodeareaofficefax\" id=\"kodeareaofficefax\" placeholder=\"021\" style=\"text-align: center;\" value=\"".$kodeareafaxkantor."\" onkeydown=\"validateevent(event)\"/>)
								- 
								<input type=\"text\" size=\"10\" name=\"notelpofficefax\" id=\"notelpofficefax\" placeholder=\"12345678\" style=\"text-align: center;\" value=\"".$nofaxkantor3."\" onkeydown=\"validateevent(event)\"/>
							</div>
								
							<div style=\"clear: both; padding-top: 10px;\"></div>
							
							<div style=\"float:left; display: inline; height: 10px;\">
								<input type=\"checkbox\" disabled />
							</div>
								
							<div style=\"float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;\">
								Website
							</div>
								
							<div style=\"float:left; display: inline; padding-left: 10px;\">
								<input type=\"text\" size=\"70\" name=\"website\" id=\"website\" placeholder=\"www.bkpm.go.id\" value=\"".$website."\"  />
							</div>
								
							<div style=\"clear: both; padding-top: 10px;\"></div>
								
							<div style=\"float:left; display: inline; height: 10px;\">
								<span style=\"color: red; font-weight: bold; font-style: italic; font-size: 10px;\">* Wajib diisi</span>
							</div>
								
						</div>
					</div>";
		
		?>
		
		
		</div>
		
		<div style="clear: both; padding-top: 10px;"></div>
				
	</div>
</form>





<div style="border-bottom: 1px #fff solid; margin-bottom: 20px;">
	<span style="font-weight: bold;"><h1>Ubah Kota/Kabupaten</h1></span>
</div>
<?php
	$error=$this->session->userdata('warning');
	$warning=str_replace('<p>','<li>',$error);
	$warning=str_replace('</p>','</li>',$warning);
	if(isset($error) && $error){
		echo "
			<div id=\"tips\" >
				<ul>".$warning."</ul>
			</div>";
	}
	if($this->session->userdata('warning')!=""){
		$this->session->set_userdata('warning','');
	}
	
	$this->db->select('*');
	$this->db->from('city');
	$this->db->where('id',$_GET['id']);
	$result=$this->db->get();
	$result=$result->result();
	foreach($result as $row){
		$oldname=$row->city;
		$oldprov=$row->provinsi;
		$oldtipe=$row->tipe;
	}
?>
Lengkapi informasi berikut:
<form method="post" action="" enctype="multipart/form-data">
	<input type="hidden" name="idcity" value="<?php echo $_GET['id']; ?>">
	<div style="margin-top: 10px;">
		<div style="float:left; display: inline; height: 10px;">
			<input type="checkbox" disabled />
		</div>
				
		<div style="float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;">
			Nama Kota/Kabupaten
		</div>
				
		<div style="float:left; display: inline; padding-left: 10px;">
			<input type="text" autocomplete="off" name="city-name" id="city-name" size="70" value="<?php echo $oldname; ?>"/>
		</div>
							
		<div style="clear: both; padding-top: 10px;"></div>
		
		<div style="float:left; display: inline; height: 10px;">
			<input type="checkbox" disabled />
		</div>
				
		<div style="float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;">
			Provinsi
		</div>
				
		<div style="float:left; display: inline; padding-left: 10px;">
			<select id="prov" name="prov" onchange="showProvOptional(this.value)">
				<option value="">-= Pilih Salah Satu =-</option>
				<?php
					$query=@mysql_query("SELECT * FROM city WHERE 1 GROUP BY provinsi");
					$prov="";
					while($row=@mysql_fetch_array($query)){
						if($row['provinsi']==$oldprov){ $select="selected"; } else { $select=""; }
						echo "<option value=\"".$row['provinsi']."\" ".$select.">".$row['provinsi']."</option>";
					}
				?>
				<option value="lainnya">Lainnya ...</option>
			</select> * Bila tidak ada dalam list, pilih <em><strong>Lainnya</strong></em>
		</div>
							
		<div style="clear: both; padding-top: 10px;"></div>
		
		<div id="provopt"></div>
				
		<div style="clear: both; padding-top: 10px;"></div>
				
		<div style="float:left; display: inline; height: 10px;">
			<input type="checkbox" disabled />
		</div>
				
		<div style="float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;">
			Wilayah Administratif
		</div>
				
		<div style="float:left; display: inline; padding-left: 10px; vertical-align: text-top;">
			<?php
				// Status Provinsi
				$ndata=@mysql_num_rows(@mysql_query("SELECT * FROM city WHERE tipe='Provinsi' AND provinsi='".$oldprov."'"));
				if($ndata!=0){ $status="disabled"; } else { $status=""; }
			?>
			<input type="radio" id="tipe1" name="tipe" value="provinsi" <?php if($oldtipe=="Provinsi"){ echo "checked"; } ?> <?php echo $status; ?>/> Ibu Kota Provinsi<br />
			<input type="radio" id="tipe2" name="tipe" value="kota" <?php if($oldtipe=="Kota"){ echo "checked"; } ?>/> Kota<br />
			<input type="radio" id="tipe3" name="tipe" value="kabupaten" <?php if($oldtipe=="Kabupaten"){ echo "checked"; } ?>/> Kabupaten<br />
		</div>
							
		<div style="clear: both; padding-top: 10px;"></div>
				
		<input type="submit" value="Simpan Data" name="city-editsubmit" />
	</div>
</form>
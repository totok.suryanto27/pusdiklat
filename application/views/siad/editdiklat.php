<div style="border-bottom: 1px #fff solid; margin-bottom: 20px;">
	<span style="font-weight: bold;"><h1>Ubah Kegiatan Diklat BKPM</h1></span>
</div>
<?php
	$error=$this->session->userdata('warning');
	$warning=str_replace('<p>','<li>',$error);
	$warning=str_replace('</p>','</li>',$warning);
	if(isset($error) && $error){
		echo "
			<div id=\"tips\" >
				<ul>".$warning."</ul>
			</div>";
	}
	if($this->session->userdata('warning')!=""){
		$this->session->set_userdata('warning','');
	}
	
	$this->db->select('*');
	$this->db->from('activity');
	$this->db->where('id',$_GET['id']);
	$result=$this->db->get();
	$result=$result->result();
	foreach($result as $row){
		$oldname=$row->activity;
		$acttipe=$row->tipe;
		$syarat=explode(";",$row->syarat);
	}
	
	$i=1;
	while($syarat[$i]!=""){
		$i++;
	}
	$nsyarat=$i-1;
	
	if($nsyarat==1){
		if($syarat[1]==0){
			$nsyarat=0;
		}
	}
?>
Lengkapi informasi berikut:
<form method="post" action="" enctype="multipart/form-data">
	<input type="hidden" name="idact" value="<?php echo $_GET['id']; ?>">
	<div style="margin-top: 10px;">
		<div style="float:left; display: inline; height: 10px;">
			<input type="checkbox" disabled />
		</div>
			
		<div style="float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;">
			Nama Kegiatan (Nama Lama)
		</div>
				
		<div style="float:left; display: inline; padding-left: 10px;">
			<input type="text" autocomplete="off" name="act-oldname" id="act-oldname" size="70" value="<?php echo $oldname; ?>" disabled/>
		</div>
							
		<div style="clear: both; padding-top: 10px;"></div>
		
		<div style="float:left; display: inline; height: 10px;">
			<input type="checkbox" disabled />
		</div>
			
		<div style="float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;">
			Nama Kegiatan (Nama Baru)
		</div>
				
		<div style="float:left; display: inline; padding-left: 10px;">
			<input type="text" autocomplete="off" name="act-name" id="act-name" size="70" value="<?php echo $actname; ?>"/>
		</div>
							
		<div style="clear: both; padding-top: 10px;"></div>
			
		<div style="float:left; display: inline; height: 10px;">
			<input type="checkbox" disabled />
		</div>
				
		<div style="float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;">
			Tipe
		</div>
				
		<div style="float:left; display: inline; padding-left: 10px; vertical-align: text-top;">
			<select id="tipe" name="tipe">
				<?php
					if($acttipe==1){ $select1="selected"; } else { $select1=""; }
					if($acttipe==2){ $select2="selected"; } else { $select2=""; }
				?>
				<option value="">-= Pilih Salah Satu =-</option>
				<option value="1" <?php echo $select1; ?>>Berkesinambungan</option>
				<option value="2" <?php echo $select2; ?>>Insidental</option>
			</select>
		</div>
				
		<div style="clear: both; padding-top: 10px;"></div>
				
		<div style="float:left; display: inline; height: 10px;">
			<input type="checkbox" disabled />
		</div>
				
		<div style="float:left; width: 230px; padding-left: 10px; height: 7px; padding-top: 3px;">
			Jumlah Pra Syarat
		</div>
				
		<div style="float:left; display: inline; padding-left: 10px; vertical-align: text-top;">
			<select id="nsyarat" onchange="submitnsyarat(this.value)" name="nsyarat">
				<option value="">-= Pilih Salah Satu =-</option>
				<option value="1" <?php if($nsyarat==1) { echo "selected"; } ?>>1 Syarat</option>
				<option value="2" <?php if($nsyarat==2) { echo "selected"; } ?>>2 Syarat</option>
				<option value="3" <?php if($nsyarat==3) { echo "selected"; } ?>>3 Syarat</option>
				
				
					
			</select> * Biarkan bila tidak ada pra syarat
		</div>
							
		<div style="clear: both; padding-top: 10px;"></div>
			
		<div id="syarat">
			<?php
				$i=1;
				while($i<=$nsyarat){
			?>
			
			<div style="float:left; display: inline; height: 10px;">
				<input type="checkbox" disabled />
			</div>
					
			<div style="float:left; width: 240px; padding-left: 10px; height: 7px; padding-top: 3px;">
				Syarat ke-<?php echo $i; ?>
			</div>
			
			<div style=\"float:left; display: inline; padding-left: 10px; vertical-align: text-top;\">
				<select id="prasyarat<?php echo $i; ?>" name="prasyarat<?php echo $i; ?>" style="width: 500px">
					<option value="">-= Pilih Salah Satu =-</option>
					<?php
						$query=@mysql_query("SELECT * FROM activity WHERE 1");
						while($row=@mysql_fetch_array($query)){
							if($row['id']==$syarat[$i]){ $select="selected"; } else { $select=""; }
							echo"<option value=\"".$row['id']."\" ".$select.">".$row['activity']."</option>";
						}
					?>
				</select>&nbsp;
				
				<?php if($i<$nsyarat){ ?>
				
				<select id="operator<?php echo $i; ?>" name="operator<?php echo $i; ?>">
					<option value="dan">dan</option>
					<option value="atau">atau</option>
				</select>
				
				<?php } ?>
			</div>
			
			<div style="clear: both; padding-top: 10px;"></div>
			
			
			<?php $i++; } ?>
		</div>
				
		<div style="clear: both; padding-top: 10px;"></div>
			
		<input type="submit" value="Simpan Data" name="act-editsubmit" />
	</div>
</form>
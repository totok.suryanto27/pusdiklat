<div style="border-bottom: 1px #fff solid; margin-bottom: 20px;">
	<span style="font-weight: bold;"><h1>Master Data</h1></span>
</div>

<div class="master" onclick="document.location='<?php echo base_url(); ?>siad/master/activity'">
	<img src="<?php echo base_url(); ?>asset/admin/images/activity.png" width="100" height="100" />
	<span style="font-size: 16px; font-weight: bold; ">Acara Diklat</span>
	<p>Menu ini dipergunakan untuk menambah, mengubah, dan menghapus data Diklat yang diselenggarakan oleh BKPM</p>
	Klik kotak ini untuk Input Acara Diklat!		
</div>
		
<div class="master" onclick="document.location='<?php echo base_url(); ?>siad/master/departement'">
	<img src="<?php echo base_url(); ?>asset/admin/images/instansi.png" width="100" height="100" />
	<span style="font-size: 16px; font-weight: bold; ">Instansi BKPM</span>
	<p>Menu ini dipergunakan untuk menambah, mengubah, dan menghapus data Instansi BKPM</p>
	Klik kotak ini untuk Input Instansi BKPM!		
</div>
		
<div class="master" onclick="document.location='<?php echo base_url(); ?>siad/master/lecture'">
	<img src="<?php echo base_url(); ?>asset/admin/images/widiaswara.png" width="100" height="100" />
	<span style="font-size: 16px; font-weight: bold; ">Pengajar Diklat</span>
	<p>Menu ini dipergunakan untuk menambah, mengubah, dan menghapus data Pengajar Diklat</p>
	Klik kotak ini untuk Input Pengajar (Widiaiswara)!			
</div>
		
<div class="master" onclick="document.location='<?php echo base_url(); ?>siad/master/position'">
	<img src="<?php echo base_url(); ?>asset/admin/images/jabatan.png" width="100" height="100" />
	<span style="font-size: 16px; font-weight: bold; ">Jabatan</span>
	<p>Menu ini dipergunakan untuk menambah, mengubah, dan menghapus data Jabatan</p>
	Klik kotak ini untuk Input Jabatan!		
</div>
		
<div class="master" onclick="document.location='<?php echo base_url(); ?>siad/master/grade'">
	<img src="<?php echo base_url(); ?>asset/admin/images/pangkat.png" width="100" height="100" />
	<span style="font-size: 16px; font-weight: bold; ">Pangkat (Golongan)</span>
	<p>Menu ini dipergunakan untuk menambah, mengubah, dan menghapus data Pangkat (Golongan)</p>
	Klik kotak ini untuk Input Pangkat (Golongan)!		
</div>
		
<div class="master" onclick="document.location='<?php echo base_url(); ?>siad/master/city'">
	<img src="<?php echo base_url(); ?>asset/admin/images/kota.png" width="100" height="100" />
	<span style="font-size: 16px; font-weight: bold; ">Kota, Kabupaten, & Provinsi</span>
	<p>Menu ini dipergunakan untuk menambah, mengubah, dan menghapus data Kota, Kabupaten, dan Provinsi</p>
	Klik kotak ini untuk Input Kota, Kabupaten, dan Provinsi!			
</div>
		
<div class="master" onclick="document.location='<?php echo base_url(); ?>siad/master/user'">
	<img src="<?php echo base_url(); ?>asset/admin/images/User.png" width="100" height="100" />
	<span style="font-size: 16px; font-weight: bold; ">Input Pengguna</span>
	<p>Menu ini dipergunakan untuk menambah, mengubah, dan menghapus data pengguna Sistem Informasi Alumni Diklat BKPM</p>
	Klik kotak ini untuk Input Pengguna!
</div>




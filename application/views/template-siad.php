<?php 
	if(!defined('BASEPATH')) exit ('No direct script access allowed');
	error_reporting(E_ALL ^ E_NOTICE);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equif="content-type" content="text/html; charset=utf-8" />
	<title><?php echo $title;?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/siad/css/style.css" />
	<script type="text/javascript" src="<?php echo base_url();?>public/siad/js/bkpm.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>asset/admin/js/cpanel.js"></script>
	<link href="<?php echo base_url(); ?>asset/admin/themes/redmond/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>asset/admin/js/jtable/themes/lightcolor/blue/jtable.css" rel="stylesheet" type="text/css" />
</head>
<body>

<?php if($login==""){ ?>

	<div id="layout">
		<?php
			if($styleerror!=""){
				$style=" style=\"margin-top: 80px;\"";
			} else {
		?>
		
		<div id="header">
			<div class="header-logo"></div>
			<div class="header-navigation" align="center">
				<div class="header-navigation-menu <?php if($navigasi=="home"){ echo "header-navigation-menu-active"; } ?>" onclick="document.location='<?php echo base_url(); ?>siad'">
					<img src="<?php echo base_url(); ?>public/siad/images/stat.png" width="36" height="30" /><br />
					Calon Peserta Diklat
				</div>
				<div class="header-navigation-menu <?php if($navigasi=="master"){ echo "header-navigation-menu-active"; } ?>" onclick="document.location='<?php echo base_url(); ?>siad/master'">
					<img src="<?php echo base_url(); ?>public/siad/images/master.png" width="27" height="30" /><br />
					Master Data
				</div>
				<div class="header-navigation-menu <?php if($navigasi=="input"){ echo "header-navigation-menu-active"; } ?>" onclick="document.location='<?php echo base_url(); ?>siad/input'">
					<img src="<?php echo base_url(); ?>public/siad/images/input.png" width="35" height="30" /><br />
					Input Data<br />
					Alumni
				</div>
				<div class="header-navigation-menu <?php if($navigasi=="report"){ echo "header-navigation-menu-active"; } ?>" onclick="document.location='<?php echo base_url(); ?>siad/report'">
					<img src="<?php echo base_url(); ?>public/siad/images/laporan.png" width="41" height="30" /><br />
					Laporan
				</div>
				<div class="header-navigation-menu <?php if($navigasi=="memory"){ echo "header-navigation-menu-active"; } ?>" onclick="document.location='<?php echo base_url(); ?>siad/memory'">
					<img src="<?php echo base_url(); ?>public/siad/images/book.png" width="29" height="30" /><br />
					Buku<br />
					Kenangan
				</div>
				<div class="header-navigation-menu <?php if($navigasi=="document"){ echo "header-navigation-menu-active"; } ?>" onclick="document.location='<?php echo base_url(); ?>siad/document'">
					<img src="<?php echo base_url(); ?>public/siad/images/dokumen.png" width="21" height="30" /><br />
					Dokumen<br />
					Terkait
				</div>
				<div class="header-navigation-menu <?php if($navigasi=="statistics"){ echo "header-navigation-menu-active"; } ?>" onclick="document.location='<?php echo base_url(); ?>siad/statistics'">
					<img src="<?php echo base_url(); ?>public/siad/images/stat.png" width="36" height="30" /><br />
					Statistik
				</div>
				<div class="header-navigation-menu" onclick="document.location='<?php echo base_url(); ?>siad/logout'">
					<img src="<?php echo base_url(); ?>public/siad/images/logout.png" width="28" height="30" /><br />
					Logout
				</div>
			</div>
		</div>
		
		<?php } ?>
		
		
		<div id="content" <?php echo $style; ?>>
			<?php echo $contents; ?>
		</div>
		<div id="footer">
			<?php
				if(date("Y")!=2013){
					$date="2013 - ".date("Y");
				} else {
					$date=2013;
				}
			?>
			Copyright &copy; <?php echo $date; ?> Badan Koordinasi Penanaman Modal
		</div>
	</div>
<?php } else { 
	echo $contents; 
} ?>
</body>
</html>
<?php 

if($this->session->userdata('warning')!=""){
	echo "<script>window.alert('".$this->session->userdata('warning')."')</script>";
	$this->session->set_userdata('warning','');
}
?>
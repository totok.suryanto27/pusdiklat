<?php if(!defined('BASEPATH')) exit ('No direct script access allowed'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equif="content-type" content="text/html; charset=utf-8" />
	<title><?php echo $title;?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/admin/css/loginstyle.css" />
	<script type="text/javascript" src="<?php echo base_url();?>asset/admin/js/siad.js"></script>
</head>
<body>
	<?php echo $contents; ?>
</body>
</html>
<?php 
if($this->session->userdata('warning')!=""){
	$string = str_replace("\n", "", $this->session->userdata('warning'));
	$string = str_replace("\r", "", $string);
	$string = str_replace("<p>", "", $string);
	$string = str_replace("</p>", "", $string);
	echo "<script>alert('".$string."')</script>";
	$this->session->set_userdata('warning','');
}
?>
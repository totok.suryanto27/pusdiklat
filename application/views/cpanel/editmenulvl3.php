<div id="admin-content">
	<?php
	$this->db->select('*');
	$this->db->from('menulvl3');
	$this->db->where('Lvl3ID',$_GET['post']);
	$result=$this->db->get();
	$result=$result->result();
	foreach($result as $row){
		$menuname=$row->MenuName;
		$webtitle=$row->webtitle;
		$url=$row->Url;
		$linkto=$row->LinkTo;
		$lvl2id=$row->Lvl2ID;
	}		
	?>
	<div class="admin-content-title"><h1>Ubah Menu Level 3 [ <span style="color: lime;"><?php echo $menuname; ?></span> ]</h1></div>
	<div class="admin-content-content">
		<!--[if IE]>
			<style>
				.admin-content-content  input[type="text"] { 
					padding: 10px 5px 5px 5px;
				}
				.admin-content-content  select { 
					padding: 10px 5px 5px 5px;
				}
			</style>
		<![endif]-->
		<!--[if !IE]> -->
			<style>
				.admin-content-content  input[type="text"] { 
					padding: 5px 5px 5px 5px;
				}
				.admin-content-content  select { 
					padding: 5px 5px 5px 5px;
				}
			</style>
		<!-- <![endif]-->
		<?php
			$error=$this->session->userdata('warning');
			$warning=str_replace('<p>','<li>',$error);
			$warning=str_replace('</p>','</li>',$warning);
			if(isset($error) && $error){
				echo "
					<div id=\"tips\" >
						<ul>".$warning."</ul>
					</div>";
			}
			if($this->session->userdata('warning')!=""){
				$this->session->set_userdata('warning','');
			}
		?>
		<p>Menu Level 3 adalah merupakan perluasan dari Menu Level 2</p>
		
		<img src="<?php echo base_url();?>asset/admin/images/menu-level3.jpg" width="920px"/>
		<form action="" method="post" name="post" id="post" autocomplete="off">
			<input type="hidden" name="idpost" value="<?php echo $_GET['post']; ?>" />
			<input type="hidden" name="oldpost" value="<?php echo $menuname; ?>" />
			<h4 style="margin-top: 10px;">Nama Menu Level 3</h4>
			<input type="text" name="titlemenulvl3" id="titlemenulvl3" size="109%" value="<?php echo $menuname; ?>"/>
			<div style="clear: both"></div>
			<h4 style="margin-top: 10px;">Menu Level 2</h4>
			<select name="menulvl2" id="menulvl2">
				<option value="">Pilih Satu</option>
				<?php
					$query=@mysql_query("SELECT * FROM menulvl2 WHERE HaveSub='Ada'");
					while($row=@mysql_fetch_array($query)){
						if($row['Lvl2ID']==$lvl2id){ $select="selected"; } else { $select=""; }
						echo "<option value=\"".$row['Lvl2ID']."\" ".$select.">".$row['MenuName']."</option>";
					}
				?>
			</select>
			<div id="link"><a onclick="document.location='<?php echo base_url(); ?>appereance/menu-level2'">+ Tambah Menu Level 2</a></div>
			<div style="clear: both"></div>
			<h4 style="margin-top: 10px;">Judul (cth: Selamat Datang di Website Pusdiklat BKPM)</h4>
			<input type="text" name="webtitle" id="webtitle" size="109%" value="<?php echo $webtitle; ?>"/>
			<div style="clear: both"></div>
			
			<?php 
				$url_=explode("/",$url);
				$table="none";
				$this->db->select('*');
				$this->db->from('url');
				$this->db->where('Url_1',$url_[0]);
				$this->db->where('Url_2',$url_[1]);
				$this->db->where('Url_3',$url_[2]);
				$result=$this->db->get();
				$result=$result->result();
				foreach($result as $row){
					$table=$row->TableSrc;
					$contentid=$row->ContentID;
				}
			?>
			
			<fieldset>
				<div class="optradio"><input type="radio" name="opt" id="opt" value="pages" onclick="activateinput(this.value)" <?php if($table=="pages"){ echo "checked"; } ?>></div>
				<div class="opttitle">Halaman</div>
				<div class="optinput">
					<select name="optinpages" id="optinpages" <?php if($table!="pages"){ echo "disabled"; } ?> style="width: 350px;">
						<option value="">Pilih Satu</option>
						<?php
							if($table=="pages"){
								$idselect=$contentid;
							} else {
								$idselect="";
							}
							$query=@mysql_query("SELECT * FROM pages WHERE 1");
							while($row=@mysql_fetch_array($query)){
								if($row['PageID']==$idselect){ $select="selected"; } else { $select=""; }
								echo "<option value=\"".$row['PageID']."\" ".$select.">".$row['TitlePage']."</option>";
							}
						?>
					</select>
				</div>
				<div style="clear: both"></div>
				<div class="optradio"><input type="radio" name="opt" id="opt" value="category" onclick="activateinput(this.value)" <?php if($table=="categories"){ echo "checked"; } ?>></div>
				<div class="opttitle">Kategori</div>
				<div class="optinput">
					<select name="optincat" id="optincat" <?php if($table!="categories"){ echo "disabled"; } ?> style="width: 350px;">
						<option value="">Pilih Satu</option>
						<?php
							if($table=="categories"){
								$idselect=$contentid;
							} else {
								$idselect="";
							}
							$query=@mysql_query("SELECT * FROM categories WHERE 1");
							while($row=@mysql_fetch_array($query)){
								if($row['CatID']==$idselect){ $select="selected"; } else { $select=""; }
								echo "<option value=\"".$row['CatID']."\" ".$select.">".$row['Categories']."</option>";
							}
						?>
					</select>
				</div>
				<div style="clear: both"></div>
				<div class="optradio"><input type="radio" name="opt" id="opt" value="other" onclick="activateinput(this.value)" <?php if($table==""){ echo "checked"; } ?>></div>
				<div class="opttitle">Link Luar [ http:// ]</div>
				<div class="optinput">
					<input type="text" name="otherlink" id="otherlink" size="40%" value="<?php if($table==""){ echo $linkto; } ?>" <?php if($table!=""){ echo "disabled"; } ?> />
				</div>
				<div style="clear: both"></div>
				<div class="optradio" style="height: 25px;"><input type="radio" name="opt" id="opt" value="photo" onclick="activateinput(this.value)" <?php if($table=="albumphoto"){ echo "checked"; } ?>></div>
				<div class="opttitle">Gallery Photo</div>
				<div class="optinput">
					&nbsp;
				</div>
				<div style="clear: both"></div>
				<div class="optradio" style="height: 25px;"><input type="radio" name="opt" id="opt" value="video" onclick="activateinput(this.value)" <?php if($table=="albumvideo"){ echo "checked"; } ?>></div>
				<div class="opttitle">Gallery Video</div>
				<div class="optinput">
					&nbsp;
				</div>
				<div style="clear: both"></div>
				<div class="optradio" style="height: 25px;"><input type="radio" name="opt" id="opt" value="consultation" onclick="activateinput(this.value)" <?php if($table=="consultation"){ echo "checked"; } ?>></div>
				<div class="opttitle">Konsultasi</div>
				<div class="optinput">
					&nbsp;
				</div>
				<div style="clear: both"></div>
			</fieldset>
			<div style="clear: both"></div>
			<input type="submit" name="submiteditlevel3" id="submiteditlevel3" value="Simpan" />
		</form>
	</div>
</div>
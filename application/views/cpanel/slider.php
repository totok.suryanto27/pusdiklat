<div id="admin-content">
	<div class="admin-content-title"><h1>Slider</h1></div>
	
	<div class="admin-content-content">
		<a onclick="document.location='<?php echo base_url(); ?>cpanel/appereance/add-slider'" class="button-add">Tambah</a>
		<!--[if IE]>
			<style>
				.admin-content-content  input[type="text"] { 
					padding: 10px 5px 5px 5px;
				}
			</style>
		<![endif]-->
		<!--[if !IE]> -->
			<style>
				.admin-content-content  input[type="text"] { 
					padding: 5px 5px 5px 5px;
				}
			</style>
		<!-- <![endif]-->
		
		<div class="holder"></div>
		
		<div class="jtable-main-container">
			<div class="jtable-busy-panel-background" style="display: none; width: 940px; height: 123px;"></div>
			<div class="jtable-busy-message" style="display: none;"></div>
			<div class="jtable-title">
				<div class="jtable-title-text">
					Daftar Slider
				</div>
			</div>
			<table class="jtable" style="margin-top:0">
				<thead>
					<tr>
						<th class="jtable-column-header" style="width: 93%;">
							<div class="jtable-column-header-container" align="center">Preview</div>
						</th>
						<th class="jtable-column-header" style="width: 7%;">
							<div class="jtable-column-header-container" align="center">Aksi</div>
						</th>
					</tr>
				</thead>
				
				<tbody id="movies">
					<?php
					$this->db->select('*');
					$this->db->from('slider');
					$result=$this->db->get();
					$ndata=$result->num_rows();
					if($ndata==0){
						echo "<tr><td colspan=\"2\" align=\"center\">Tidak ada data</td></tr>";
					} else {
						$result=$result->result();
						$i=1;
						$j=1;
						foreach($result as $row){
							if($i==2*$j-1){ 
								$even=" jtable-row-even";
								$j++;
							} else { 
								$even=""; 
							}
							echo 
								"<tr class=\"jtable-data-row jtable-row-even\">
									<td><div align=\"center\"><img src=\"".base_url()."/uploads/slider/".$row->SliderThumb."\" alt=\"Preview\" /></div></td>
									<td><div align=\"center\"><img src=\"".base_url()."/asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Slider\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus ".$row->SliderImage." dari database?')){document.location='../appereance/deleteslider/?post=".$row->SliderID."'; }\" /></div></td>
								</tr>";
							$i++;
						}
					}
					?>
				</tbody>
			</table>
		</div>
		
	</div>
</div>
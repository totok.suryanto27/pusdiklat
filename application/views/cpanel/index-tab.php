<div id="admin-content">
	<div class="admin-content-title"><h1>Tab Index [ Max 5 Tab ]</h1></div>
	
	<div class="admin-content-content">
		<a onclick="document.location='<?php echo base_url(); ?>cpanel/appereance/new-tab'" class="button-add">Tambah</a>
		<!--[if IE]>
			<style>
				.admin-content-content  input[type="text"] { 
					padding: 10px 5px 5px 5px;
				}
			</style>
		<![endif]-->
		<!--[if !IE]> -->
			<style>
				.admin-content-content  input[type="text"] { 
					padding: 5px 5px 5px 5px;
				}
			</style>
		<!-- <![endif]-->
		
		<div class="holder"></div>
		
		<div class="jtable-main-container">
			<div class="jtable-busy-panel-background" style="display: none; width: 940px; height: 123px;"></div>
			<div class="jtable-busy-message" style="display: none;"></div>
			<div class="jtable-title">
				<div class="jtable-title-text">
					Daftar Index Tab
				</div>
			</div>
			<table class="jtable" style="margin-top:0">
				<thead>
					<tr>
						<th class="jtable-column-header" style="width: 47%;">
							<div class="jtable-column-header-container" align="center">Nama Tab</div>
						</th>
						<th class="jtable-column-header" style="width: 47%;">
							<div class="jtable-column-header-container" align="center">Kategori yang ditampilkan</div>
						</th>
						<th class="jtable-column-header" style="width: 6%;">
							<div class="jtable-column-header-container" align="center">Aksi</div>
						</th>
					</tr>
				</thead>
				
				<tbody id="movies">
					<?php
					$this->db->select('*');
					$this->db->from('indextab');
					$result=$this->db->get();
					$ndata=$result->num_rows();
					if($ndata==0){
						echo "<tr><td colspan=\"3\" align=\"center\">Tidak ada data</td></tr>";
					} else {
						$result=$result->result();
						$i=1;
						$j=1;
						foreach($result as $row){
							
							$this->db->select('*');
							$this->db->from('categories');
							$this->db->where('CatID',$row->CatID);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row2){
								$catname=$row2->Categories;
							}
							
							echo 
								"<tr class=\"jtable-data-row jtable-row-even\">
									<td>".$row->TabName."</td>
									<td>".$catname."</td>
									<td><div align=\"center\"><img src=\"../../asset/admin/images/edit.png\" alt=\"edit\"  title=\"Edit Index Tab\" style=\"cursor: pointer\" onclick=\"document.location='../appereance/edit-tab/?post=".$row->TabID."'\" /> <img src=\"../../asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Index Tab\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus ".$row->TabName." dari database?')){document.location='../appereance/delete-tab/?post=".$row->TabID."'; }\" /></div></td>
								</tr>";
							$i++;
						}
					}
					?>
				</tbody>
			</table>
		</div>
		
		
	</div>
</div>
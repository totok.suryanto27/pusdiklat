<div class="row-fluid">
	<div class="span12">
		<ul class="breadcrumb">
			<li><a href="<?=base_url(); ?>cpanel/media/gallery-photo" style="cursor: pointer; ">Album Photo</a> <span class="divider">/</span></li>
			<li><a href="<?=base_url(); ?>cpanel/media/edit-albph/?post=<?=$_GET['post'];?>">Ubah Album Photo</a> <span class="divider">/</span></li>
		</ul>
	</div>
</div>

<?php
	$error=$this->session->userdata('warning');
	$warning=str_replace('<p>','<li>',$error);
	$warning=str_replace('</p>','</li>',$warning);
	if(isset($error) && $error){
		echo "
			<div class=\"alert alert-block\" >
				<h4 class=\"alert-heading\">Peringatan!</h4>
				<ul>".$warning."</ul>
			</div>";
	}
	if($this->session->userdata('warning')!=""){
		$this->session->set_userdata('warning','');
	}
	$row=@mysql_fetch_array(@mysql_query("SELECT * FROM albumphoto WHERE AlbPhID='".$_GET['post']."'"));	
?>

<div class="row-fluid">
	<section class="utopia-widget utopia-form-box section">
		<div class="utopia-widget-title">
			<img src="<?=base_url(); ?>public/images/admin/monitor.png" class="utopia-widget-icon">
			<span>Ubah Nama Album</span>
		</div>

		<div class="row-fluid">
			<div id="showform" style="padding: 10px 10px 10px 10px;">
				<form class="form-horizontal" action="" method="post" name="edit-albph" id="edit-albph">
                    <fieldset>
                        <div class="control-group">
                            <label class="control-label" for="Nama Album">Nama Album</label>
							<input type="hidden" name="postid" id="postid" value="<?=$_GET['post']; ?>" />
							<input type="hidden" name="oldname" id="oldname" size="109%" value="<?=$row['TitleAlbum']; ?>"/>
                            <div class="controls">
                                <input type="text" name="titlealbum" id="titlealbum" style="width: 500px" value="<?=$row['TitleAlbum']; ?>"/><br />
                            </div>
                        </div>
						
						<div class="control-group">
                            <label class="control-label" for="content">Deskripsi</label>
                            <div class="controls">
                              <textarea name="albumdesc" rows="15" value="" id="albumdesc"><?=$row['AlbumDesc']; ?></textarea>
                            </div>
                        </div>
						
						<p><input type="submit" value="Simpan" class="btn btn-primary span5"  name="submitalbum" style="width: 100px; margin-left: 20px;"/></p>
                    </fieldset>
                </form>
			</div>
		</div>
	</section>
</div>
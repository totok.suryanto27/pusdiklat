<div class="row-fluid">
	<div class="span12">
		<ul class="breadcrumb">
			<li><a style="cursor: pointer; ">Kegiatan Pusdiklat</a> <span class="divider">/</span></li>
			<li><a href="<?=base_url(); ?>cpanel/bkpm/all-schedule" style="cursor: pointer; ">Jadwal Kegiatan</a> <span class="divider">/</span></li>
			<li><a href="<?=base_url(); ?>cpanel/bkpm/bkpm-edit/?post=<?=$_GET['post'];?>">Ubah Jadwal Kegiatan</a> <span class="divider">/</span></li>
		</ul>
	</div>
</div>

<?php
	$error=$this->session->userdata('warning');
	$warning=str_replace('<p>','<li>',$error);
	$warning=str_replace('</p>','</li>',$warning);
	if(isset($error) && $error){
		echo "
			<div class=\"alert alert-block\" >
				<h4 class=\"alert-heading\">Peringatan!</h4>
				<ul>".$warning."</ul>
			</div>";
	}
	if($this->session->userdata('warning')!=""){
		$this->session->set_userdata('warning','');
	}
	$row_master1=@mysql_fetch_array(@mysql_query("SELECT * FROM jadwal WHERE SchedID='".$_GET['post']."'"));	
	$row_master2=@mysql_fetch_array(@mysql_query("SELECT * FROM activity WHERE id='".$row_master1['DikID']."'"));	
	$actname=$row_master2['activity'];
	$id=$row_master1['DikID'];
	$datestart=$row_master1['StartDate'];
	$dateend=$row_master1['EndDate'];
	$kuota=$row_master1['Quota'];
	$kuotatotal=$row_master1['TotalQuota'];
	$status=$row_master1['Status'];
	$class=$row_master1['ClassDik'];
	$place=$row_master1['Place'];
	$tipe=$row_master1['tipe'];
?>

<div class="row-fluid">
	<section class="utopia-widget utopia-form-box section">
		<div class="utopia-widget-title">
			<img src="<?=base_url(); ?>public/images/admin/monitor.png" class="utopia-widget-icon">
			<span>Ubah Jadwal Kegiatan</span>
		</div>

		<div class="row-fluid">
			<div id="showform" style="padding: 10px 10px 10px 10px;">
				<form class="form-horizontal" action="" method="post" name="post" id="post">
                    <input type="hidden" name="idpost" value="<?php echo $_GET['post']; ?>" />
					<fieldset>
                        <div class="control-group">
                            <label class="control-label" for="Diklat yang diselenggarakan">Diklat yang diselenggarakan</label>
                            <div class="controls">
                                <select name="diklat" style="width: 800px; float: none;">
									<option value="">Pilih Satu</option>
									<?php
										// Batasi Jadwal berdasarkan yang terinput di Menu
										$query=@mysql_query("SELECT * FROM url WHERE TableSrc='activity' GROUP BY ContentID");
										while($row=@mysql_fetch_array($query)){
											$query2=@mysql_query("SELECT * FROM activity WHERE id='".$row['ContentID']."'");
											while($row2=@mysql_fetch_array($query2)){
												if($row2['id']==$id){ $select="selected"; } else { $select=""; }
												echo "<option value=\"".$row2['id']."\" ".$select.">".$row2['activity']."</option>";
											}
										}
									?>
								</select><br />
                            </div>
                        </div>
						
						<div class="control-group">
                            <label class="control-label" for="Angkatan">Angkatan</label>
                            <div class="controls">
                                <select name="angkatan" style="float: none; width: 80px; text-align: center;">
									<option value="">Pilih Satu</option>
									<option value="I" <?php if($class=="I"){ echo "selected"; }?>>I</option>
									<option value="II" <?php if($class=="II"){ echo "selected"; }?>>II</option>
									<option value="III" <?php if($class=="III"){ echo "selected"; }?>>III</option>
									<option value="IV" <?php if($class=="IV"){ echo "selected"; }?>>IV</option>
									<option value="V" <?php if($class=="V"){ echo "selected"; }?>>V</option>
									<option value="VI" <?php if($class=="VI"){ echo "selected"; }?>>VI</option>
									<option value="VII" <?php if($class=="VII"){ echo "selected"; }?>>VII</option>
									<option value="VIII" <?php if($class=="VIII"){ echo "selected"; }?>>VIII</option>
									<option value="IX" <?php if($class=="IX"){ echo "selected"; }?>>IX</option>
									<option value="X" <?php if($class=="X"){ echo "selected"; }?>>X</option>
									<option value="XI" <?php if($class=="XI"){ echo "selected"; }?>>XI</option>
									<option value="XII" <?php if($class=="XII"){ echo "selected"; }?>>XII</option>
									<option value="XIII" <?php if($class=="XIII"){ echo "selected"; }?>>XIII</option>
									<option value="XIV" <?php if($class=="XIV"){ echo "selected"; }?>>XIV</option>
									<option value="XV" <?php if($class=="XV"){ echo "selected"; }?>>XV</option>
									<option value="XVI" <?php if($class=="XVI"){ echo "selected"; }?>>XVI</option>
									<option value="XVII" <?php if($class=="XVII"){ echo "selected"; }?>>XVII</option>
									<option value="XVIII" <?php if($class=="XVIII"){ echo "selected"; }?>>XVIII</option>
									<option value="XIX" <?php if($class=="XIX"){ echo "selected"; }?>>XIX</option>
									<option value="XX" <?php if($class=="XX"){ echo "selected"; }?>>XX</option>
								</select>
                            </div>
                        </div>
						
						<div class="control-group">
                            <label class="control-label" for="Tanggal Pelaksanaan (mulai)">Tanggal Pelaksanaan (mulai)</label>
							<?php 
								$startdate=explode('-',$datestart);
								$enddate=explode('-',$dateend);
							?>
                            <div class="controls">
                                <select name="startdate" style="float: left; margin-right: 10px; width: 50px;">
									<?php
										$i=1;
										while($i<=31){
											if($i==$startdate[2]){ $select="selected"; } else { $select=""; }
											if($i<10){
												echo "<option value=\"0".$i."\" ".$select.">0".$i."</option>";
											} else {
												echo "<option value=\"".$i."\"".$select.">".$i."</option>";
											}
											$i++;
										}
									?>
								</select>
								<select name="startmonth" style="float: left; margin-right: 10px;">
									<?php
										$i=1;
										while($i<=12){
											if($i==$startdate[1]){ $select="selected"; } else { $select=""; }
											if($i<10){
												echo "<option value=\"0".$i."\" ".$select.">".date("F",mktime(0, 0, 0, $i, 1, date("Y")))."</option>";
											} else {
												echo "<option value=\"".$i."\"".$select.">".date("F",mktime(0, 0, 0, $i, 1, date("Y")))."</option>";
											}
											$i++;
										}
									?>
								</select>
								<input type="text" name="startyear" size="4" style="width: 50px; text-align: center;" value="<?php echo $startdate[0]; ?>" />
                            </div>
                        </div>
						
						<div class="control-group">
                            <label class="control-label" for="Tanggal Pelaksanaan (berakhir)">Tanggal Pelaksanaan (berakhir)</label>
                            <div class="controls">
                                <select name="enddate" style="float: left; margin-right: 10px;  width: 50px;">
									<?php
										$i=1;
										while($i<=31){
											if($i==$enddate[2]){ $select="selected"; } else { $select=""; }
											if($i<10){
												echo "<option value=\"0".$i."\" ".$select.">0".$i."</option>";
											} else {
												echo "<option value=\"".$i."\"".$select.">".$i."</option>";
											}
											$i++;
										}
									?>
								</select>
								<select name="endmonth" style="float: left; margin-right: 10px;">
									<?php
										$i=1;
										while($i<=12){
											if($i==$enddate[1]){ $select="selected"; } else { $select=""; }
											if($i<10){
												echo "<option value=\"0".$i."\" ".$select.">".date("F",mktime(0, 0, 0, $i, 1, date("Y")))."</option>";
											} else {
												echo "<option value=\"".$i."\"".$select.">".date("F",mktime(0, 0, 0, $i, 1, date("Y")))."</option>";
											}
											$i++;
										}
									?>
								</select>
								<input type="text" name="endyear" size="4" style="width: 50px; text-align: center;" value="<?php echo $enddate[0]; ?>" />
                            </div>
                        </div>
						
						<div class="control-group">
                            <label class="control-label" for="Kuota Peserta">Kuota Peserta</label>
                            <div class="controls">
                                <input type="text" name="kuota" size="4" value="<?php echo $kuota; ?>" style="width: 50px; text-align: center;"/> Peserta
                            </div>
                        </div>
						
						<div class="control-group">
                            <label class="control-label" for="Kuota Peserta Total (Online + Manual)">Kuota Peserta Total (Online + Manual)</label>
                            <div class="controls">
                                <input type="text" name="kuotatotal" size="4" value="<?php echo $kuotatotal; ?>" style="width: 50px; text-align: center;"/> Peserta
                            </div>
                        </div>
						
						<div class="control-group">
                            <label class="control-label" for="Peserta Khusus dari Penanaman Modal">Peserta Khusus dari Penanaman Modal</label>
                            <div class="controls">
                                <select name="tipe">
									<option value="tidak" <?php if($tipe=="tidak"){ echo "selected"; } ?>>Tidak</value>
									<option value="ya" <?php if($tipe=="ya"){ echo "selected"; } ?>>Ya</value>
								</select>
                            </div>
                        </div>
						
						<div class="control-group">
                            <label class="control-label" for="Status">Status</label>
                            <div class="controls">
                                <select name="status">
									<option value="">Pilih Satu</option>
									<option value="0" <?php if($status==0){ echo "selected"; } ?>>Ditutup</option>
									<option value="1" <?php if($status==1){ echo "selected"; } ?>>Dibuka</option>
								</select>
                            </div>
                        </div>
						
						<div class="control-group">
                            <label class="control-label" for="Tempat Pelaksanaan">Tempat Pelaksanaan</label>
                            <div class="controls">
                                <input type="text" name="tempat" size="100" value="<?php echo $place; ?>" style="width: 500px;"/>
                            </div>
                        </div>
						
						<p><input type="submit" value="Simpan" class="btn btn-primary span5"  name="submitschedule" style="width: 100px; margin-left: 20px;"/></p>
                    </fieldset>
                </form>
			</div>
		</div>
	</section>
</div>
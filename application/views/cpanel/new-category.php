<div class="row-fluid">
	<div class="span12">
		<ul class="breadcrumb">
			<li><a href="<?=base_url(); ?>cpanel/content/all-categories" style="cursor: pointer; ">Kategori</a> <span class="divider">/</span></li>
			<li><a href="<?=base_url(); ?>cpanel/content/new-category">Buat Kategori</a> <span class="divider">/</span></li>
		</ul>
	</div>
</div>

<?php
	$error=$this->session->userdata('warning');
	$warning=str_replace('<p>','<li>',$error);
	$warning=str_replace('</p>','</li>',$warning);
	if(isset($error) && $error){
		echo "
			<div class=\"alert alert-block\" >
				<h4 class=\"alert-heading\">Peringatan!</h4>
				<ul>".$warning."</ul>
			</div>";
	}
	if($this->session->userdata('warning')!=""){
		$this->session->set_userdata('warning','');
	}
		
	if($this->session->userdata('isCategory')){			
		$data=$this->session->userdata('isCategory');
		$title=$data['titlecategory'];
		$desc=$data['categorydesc'];
	} else {
		$title="";
		$desc="";
	}
			
?>

<div class="row-fluid">
	<section class="utopia-widget utopia-form-box section">
		<div class="utopia-widget-title">
			<img src="<?=base_url(); ?>public/images/admin/monitor.png" class="utopia-widget-icon">
			<span>Buat Kategori</span>
		</div>

		<div class="row-fluid">
			<div id="showform" style="padding: 10px 10px 10px 10px;">
				<form class="form-horizontal" action="" method="post" name="post" id="post"">
                    <fieldset>
                        <div class="control-group">
                            <label class="control-label" for="Kategori">Nama Kategori</label>
                            <div class="controls">
                                <input type="text" name="titlecategory" id="titlecategory" style="width: 500px" value="<?=$title; ?>"/><br />
                            </div>
                        </div>
						
						<div class="control-group">
                            <label class="control-label" for="content">Deskripsi Kategori</label>
                            <div class="controls">
                               <textarea name="categorydesc" rows="15" value="" id="categorydesc"><?php echo $desc; ?></textarea>
                            </div>
                        </div>
						
						<p><input type="submit" value="Simpan" class="btn btn-primary span5"  name="submitcategory" style="width: 100px; margin-left: 20px;"/></p>
                    </fieldset>
                </form>
			</div>
		</div>
	</section>
</div>
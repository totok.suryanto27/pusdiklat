<div class="row-fluid">
	<div class="span12">
		<ul class="breadcrumb">
			<li><a style="cursor: pointer; ">Kegiatan Pusdiklat</a> <span class="divider">/</span></li>
			<li><a href="<?=base_url(); ?>cpanel/bkpm/all-schedule">Jadwal Kegiatan</a> <span class="divider">/</span></li>
			<li><a href="<?=base_url(); ?>cpanel/bkpm/add-schedule">Buat Jadwal Kegiatan</a> <span class="divider">/</span></li>
		</ul>
	</div>
</div>

<?php
	$error=$this->session->userdata('warning');
	$warning=str_replace('<p>','<li>',$error);
	$warning=str_replace('</p>','</li>',$warning);
	if(isset($error) && $error){
		echo "
			<div class=\"alert alert-block\" >
				<h4 class=\"alert-heading\">Peringatan!</h4>
				<ul>".$warning."</ul>
			</div>";
	}
	if($this->session->userdata('warning')!=""){
		$this->session->set_userdata('warning','');
	}
		
	if($this->session->userdata('isSchedule')){			
		$data=$this->session->userdata('isSchedule');
		$diklat=$data['diklat'];
		//$angkatan=$data['angkatan'];
	} else {
		$diklat="";
		//$angkatan="";
	}
?>

<div class="row-fluid">
	<section class="utopia-widget utopia-form-box section">
		<div class="utopia-widget-title">
			<img src="<?=base_url(); ?>public/images/admin/monitor.png" class="utopia-widget-icon">
			<span>Buat Jadwal Kegiatan</span>
		</div>

		<div class="row-fluid">
			<div id="showform" style="padding: 10px 10px 10px 10px;">
				<form class="form-horizontal" action="" method="post" name="post" id="post">
                    <fieldset>
                        <div class="control-group">
                            <label class="control-label" for="Diklat yang diselenggarakan">Diklat yang diselenggarakan</label>
                            <div class="controls">
                                <select name="diklat" style="width: 800px; float: none;">
									<option value="">Pilih Satu</option>
									<?php
										// Batasi Jadwal berdasarkan yang terinput di Menu
										$query=@mysql_query("SELECT * FROM url WHERE TableSrc='activity' GROUP BY ContentID");
										while($row=@mysql_fetch_array($query)){
											$query2=@mysql_query("SELECT * FROM activity WHERE id='".$row['ContentID']."'");
											while($row2=@mysql_fetch_array($query2)){
												if($diklat==$row2['id']){ $selected="selected"; } else { $selected=""; }
												echo "<option value=\"".$row2['id']."\" ".$selected.">".$row2['activity']."</option>";
											}
										}
									?>
								</select><br />
                            </div>
                        </div>
						
						<div class="control-group">
                            <label class="control-label" for="Angkatan">Angkatan</label>
                            <div class="controls">
                                <select name="angkatan" style="float: none;  width: 80px; text-align: center;">
									<option value="">Pilih Satu</option>
									<option value="I">I</option><option value="II" >II</option><option value="III" >III</option>
									<option value="IV" >IV</option><option value="V" >V</option><option value="VI" >VI</option>
									<option value="VII" >VII</option><option value="VIII" >VIII</option><option value="IX" >IX</option>
									<option value="X" >X</option><option value="XI" >XI</option><option value="XII" >XII</option>
									<option value="XIII" >XIII</option><option value="XIV" >XIV</option><option value="XV" >XV</option>
									<option value="XVI" >XVI</option><option value="XVII" >XVII</option><option value="XVIII" >XVIII</option>
									<option value="XIX" >XIX</option><option value="XX" >XX</option>
								</select>
                            </div>
                        </div>
						
						<div class="control-group">
                            <label class="control-label" for="Tanggal Pelaksanaan (mulai)">Tanggal Pelaksanaan (mulai)</label>
                            <div class="controls">
                                <select name="startdate" style="float: left; margin-right: 10px; width: 50px;">
									<?php
										$i=1;
										while($i<=31){
											if($i==date("j")){ $select="selected"; } else { $select=""; }
											if($i<10){
												echo "<option value=\"0".$i."\" ".$select.">0".$i."</option>";
											} else {
												echo "<option value=\"".$i."\"".$select.">".$i."</option>";
											}
											$i++;
										}
									?>
								</select>
								<select name="startmonth" style="float: left; margin-right: 10px;">
									<?php
										$i=1;
										while($i<=12){
											if($i==date("n")){ $select="selected"; } else { $select=""; }
											if($i<10){
												echo "<option value=\"0".$i."\" ".$select.">".date("F",mktime(0, 0, 0, $i, 1, date("Y")))."</option>";
											} else {
												echo "<option value=\"".$i."\"".$select.">".date("F",mktime(0, 0, 0, $i, 1, date("Y")))."</option>";
											}
											$i++;
										}
									?>
								</select>
								<input type="text" name="startyear" size="4" style="width: 50px; text-align: center;" value="<?php echo date("Y"); ?>" />
                            </div>
                        </div>
						
						<div class="control-group">
                            <label class="control-label" for="Tanggal Pelaksanaan (berakhir)">Tanggal Pelaksanaan (berakhir)</label>
                            <div class="controls">
                                <select name="enddate" style="float: left; margin-right: 10px;  width: 50px;">
									<?php
										$i=1;
										while($i<=31){
											if($i==date("j")){ $select="selected"; } else { $select=""; }
											if($i<10){
												echo "<option value=\"0".$i."\" ".$select.">0".$i."</option>";
											} else {
												echo "<option value=\"".$i."\"".$select.">".$i."</option>";
											}
											$i++;
										}
									?>
								</select>
								<select name="endmonth" style="float: left; margin-right: 10px;">
									<?php
										$i=1;
										while($i<=12){
											if($i==date("n")){ $select="selected"; } else { $select=""; }
											if($i<10){
												echo "<option value=\"0".$i."\" ".$select.">".date("F",mktime(0, 0, 0, $i, 1, date("Y")))."</option>";
											} else {
												echo "<option value=\"".$i."\"".$select.">".date("F",mktime(0, 0, 0, $i, 1, date("Y")))."</option>";
											}
											$i++;
										}
									?>
								</select>
								<input type="text" name="endyear" size="4" style="width: 50px; text-align: center;" value="<?php echo date("Y"); ?>" />
                            </div>
                        </div>
						
						<div class="control-group">
                            <label class="control-label" for="Kuota Peserta">Kuota Peserta</label>
                            <div class="controls">
                                <input type="text" name="kuota" size="4" value="" style="width: 50px; text-align: center;"/> Peserta
                            </div>
                        </div>
						
						<div class="control-group">
                            <label class="control-label" for="Kuota Peserta Total (Online + Manual)">Kuota Peserta Total (Online + Manual)</label>
                            <div class="controls">
                                <input type="text" name="kuotatotal" size="4" value="" style="width: 50px; text-align: center;"/> Peserta
                            </div>
                        </div>
						
						<div class="control-group">
                            <label class="control-label" for="Peserta Khusus dari Penanaman Modal">Peserta Khusus dari Penanaman Modal</label>
                            <div class="controls">
                                <select name="tipe">
									<option value="tidak">Tidak</value>
									<option value="ya">Ya</value>
								</select>
                            </div>
                        </div>
						
						<div class="control-group">
                            <label class="control-label" for="Status">Status</label>
                            <div class="controls">
                                <select name="status">
									<option value="">Pilih Satu</option>
									<option value="0">Ditutup</option>
									<option value="1">Dibuka</option>
								</select>
                            </div>
                        </div>
						
						<div class="control-group">
                            <label class="control-label" for="Tempat Pelaksanaan">Tempat Pelaksanaan</label>
                            <div class="controls">
                                <input type="text" name="tempat" size="100" value="" style="width: 500px;"/>
                            </div>
                        </div>
						
						<p><input type="submit" value="Simpan" class="btn btn-primary span5"  name="submitschedule" style="width: 100px; margin-left: 20px;"/></p>
                    </fieldset>
                </form>
			</div>
		</div>
	</section>
</div>
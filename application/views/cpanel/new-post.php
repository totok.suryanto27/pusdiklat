<script type="text/javascript" src="<?=base_url();?>asset/admin/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
	tinymce.init({
		selector: "textarea",
		theme: "modern",
		plugins: [
			"advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
			"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
			"table contextmenu directionality emoticons template textcolor paste textcolor filemanager"
			],
		toolbar1: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | formatselect fontselect fontsizeselect | image",
		menubar: false
	 });
</script>

<div class="row-fluid">
	<div class="span12">
		<ul class="breadcrumb">
			<li><a href="<?=base_url(); ?>cpanel/content/all-posts" style="cursor: pointer; ">Konten</a> <span class="divider">/</span></li>
			<li><a href="<?=base_url(); ?>cpanel/content/new-posts">Buat Konten</a> <span class="divider">/</span></li>
		</ul>
	</div>
</div>

<?php
	$error=$this->session->userdata('warning');
	$warning=str_replace('<p>','<li>',$error);
	$warning=str_replace('</p>','</li>',$warning);
	if(isset($error) && $error){
		echo "
			<div class=\"alert alert-block\" >
				<h4 class=\"alert-heading\">Peringatan!</h4>
				<ul>".$warning."</ul>
			</div>";
	}
	if($this->session->userdata('warning')!=""){
		$this->session->set_userdata('warning','');
	}
		
	if($this->session->userdata('isPost')){			
		$data=$this->session->userdata('isPost');
		$title=$data['titlepost'];
		$content=$data['contentpost'];
	} else {
		$title="";
		$content="";
	}
			
?>

<div class="row-fluid">
	<section class="utopia-widget utopia-form-box section">
		<div class="utopia-widget-title">
			<img src="<?=base_url(); ?>public/images/admin/monitor.png" class="utopia-widget-icon">
			<span>Buat Konten</span>
		</div>

		<div class="row-fluid">
			<div id="showform" style="padding: 10px 10px 10px 10px;">
				<form class="form-horizontal" action="" method="post" name="post" id="post"">
                    <fieldset>
                        <div class="control-group">
                            <label class="control-label" for="Judul Konten">Judul Konten</label>
                            <div class="controls">
                                <input type="text" name="titlepost" id="titlepost" style="width: 500px" value="<?=$title; ?>"/><br />
                            </div>
                        </div>
						
						<div class="control-group">
                            <label class="control-label" for="categories">Kategori</label>
                            <div class="controls">
								<select name="categories">
									<option value="0">Tidak Dikategorikan</option>
									<?php
										$query=@mysql_query("SELECT * FROM categories");
										while($row=@mysql_fetch_array($query)){
											if($row['CatID']!=0){
												echo "<option value=\"".$row['CatID']."\">".$row['Categories']."</option>";
											}
										}
									?>
								</select>
                            </div>
                        </div>
						
						<div class="control-group">
                            <label class="control-label" for="content">Isi Halaman</label>
                            <div class="controls">
                               <textarea name="contentpost" rows="15" value="" id="contentpost"><?php echo $content; ?></textarea>
                            </div>
                        </div>
						
						<p><input type="submit" value="Simpan" class="btn btn-primary span5"  name="submitcontent" style="width: 100px; margin-left: 20px;"/></p>
                    </fieldset>
                </form>
			</div>
		</div>
	</section>
</div>
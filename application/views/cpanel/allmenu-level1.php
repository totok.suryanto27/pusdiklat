<div class="row-fluid">
	<div class="span12">
		<ul class="breadcrumb">
			<li><a style="cursor: pointer; ">Menu</a> <span class="divider">/</span></li>
			<li><a href="<?=base_url()?>cpanel/appereance/allmenu-level1" style="cursor: pointer; ">Menu Level 1</a> <span class="divider">/</span></li>
		</ul>
	</div>
</div>
<div class="row-fluid">
	<section class="utopia-widget utopia-form-box section">
		<div class="utopia-widget-title">
			<img src="<?=base_url(); ?>public/images/admin/monitor.png" class="utopia-widget-icon">
			<span>Menu Level 1</span>
		</div>

		<div class="row-fluid">
			<div style="width: 200px; margin: 20px 20px 20px 20px;">
				<input type="button" class="btn btn-primary span5"  style="width: 200px; float: left;" value="Tambah Menu Baru" onclick="document.location='<?php echo base_url(); ?>cpanel/appereance/menu-level1'"/>
			</div>
			<div style="clear:both"></div>
			<div style="margin: 20px 20px 20px 20px;">
				<table class="table table-bordered">
					<colgroup>
						<col class="utopia-col-0"></col>
						<col class="utopia-col-1"></col>
						<col class="utopia-col-0"></col>
					</colgroup>
					<thead>
						<tr>
							<th style="width: 62%; text-align: center;">Menu</th>
							<th style="width: 31%; text-align: center;">Sub Menu</th>
							<th style="width: 7%; text-align: center;">Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$query=@mysql_query("SELECT * FROM menulvl1");
							$ndata=@mysql_num_rows($query);
							if($ndata==0){
								echo "
									<tr>
										<td colspan=\"3\" style=\"text-align: center;\">Belum ada data untuk menu level 1. Silahkan buat baru terlebih dahulu!</td>
									</tr>";
							} else {
								while($row=@mysql_fetch_array($query)){
									echo "
										<tr>
											<td>".$row['MenuName']."</td>
											<td style=\"text-align: center\">".$row['HaveSub']."</td>
											<td><div align=\"center\"><img src=\"../../asset/admin/images/edit.png\" alt=\"edit\"  title=\"Edit Menu Level 1\" style=\"cursor: pointer\" onclick=\"document.location='../appereance/editmenu-level1/?post=".$row['Lvl1ID']."'\" /> <img src=\"../../asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Menu Level 1\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus ".$row['MenuName']." dari database?')){document.location='../appereance/deletemenu-level1/?post=".$row['Lvl1ID']."'; }\" /></div></td>
										</tr>";
									
								}
							}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</section>
</div>
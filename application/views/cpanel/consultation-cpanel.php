<div id="admin-content">
	<div class="admin-content-title"><h1>Jawab Konsultasi</h1></div>
	
	<div class="admin-content-content">
		<!--[if IE]>
			<style>
				.admin-content-content  input[type="text"] { 
					padding: 10px 5px 5px 5px;
				}
			</style>
		<![endif]-->
		<!--[if !IE]> -->
			<style>
				.admin-content-content  input[type="text"] { 
					padding: 5px 5px 5px 5px;
				}
			</style>
		<!-- <![endif]-->
		
		<style>
			.statement {
				min-height: 50px;
				background-color: rgb(247, 247, 247);
				overflow: hidden;
				border: 1px solid rgb(204, 204, 204);
				margin-bottom: 10px;
				padding: 10px 10px 10px 10px;
				color: #000;
			}
			.statement-title {
				font-weight: bold;
			}
		</style>
		
		<form action="" method="post" name="post" id="post" onsubmit="return validate();">
			<h4>Jawaban</h4>
			<textarea name="message" rows="10" cols="70" value="" id="message"></textarea>
			<p><input type="submit" name="submitconsultation" value="Simpan" /></p>
		</form>
		
		<div>
		
		<div class="holder"></div>
		
		<table>
			<tbody id="movies">
				<?php
					$this->db->select('*');
					$this->db->from('consultation');
					$this->db->order_by('ConsID','DESC');
					$result=$this->db->get();
					$result=$result->result();
					foreach($result as $row){
						echo "<tr><td>
							<div class=\"statement\">
								<div class=\"statement-title\">".$row->Nama." (".$row->Instansi.") pada ".$row->RecorDate."</div>
								<div>".$row->Statement."</div>
							</div>
							</td></tr>";
					}
				?>
			</tbody>
		</table>
		</div>
	</div>
</div>
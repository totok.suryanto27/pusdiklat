<?php 
$content=
	"<div align=\"center\" style=\"margin-bottom: 10px;\"><img src=\"".base_url()."public/images/".$row->id.".gif\" alt=\"\" height=\"110px\" /></div>
		<div class=\"flexigrid\" style=\"width: 650px;\">
			<div class=\"mDiv\"><div class=\"ftitle\">Jadwal dan Waktu Pelaksanaan Diklat</div></div>
				<div class=\"hDiv\">
					<div class=\"hDivBox\">
						<table cellspacing=\"0\" cellpadding=\"0\">
							<thead>
								<tr>
									<th class=\"\" align=\"center\" axis=\"col0\"><div style=\"text-align: center; width: 40px; padding-top: 25px;\">Angk</div></th>
									<th class=\"\" align=\"center\" axis=\"col1\"><div style=\"text-align: center; width: 140px; padding-top: 15px;\">Tanggal<br />Pelatihan</div></th>
									<th class=\"\" align=\"center\" axis=\"col2\"><div style=\"text-align: center; width: 50px;\">Kuota<br />Peserta<br />Online</div></th>
									<th align=\"center\" axis=\"col3\"><div style=\"text-align: center; width: 50px;\">Peserta<br />Daftar<br />Online</div></th>
									<th align=\"center\" axis=\"col4\"><div style=\"text-align: center; width: 50px;\">Telah<br />Konfirmasi<br />Online</div></th>
									<th align=\"center\" axis=\"col4\"><div style=\"text-align: center; width: 85px; padding-top: 15px;\">Daftar<br />Online</div></th>
									<th align=\"center\" axis=\"col4\"><div style=\"text-align: center; width: 70px; padding-top: 15px;\">Surat<br />Konfirmasi</div></th>
									<th align=\"center\" axis=\"col4\"><div style=\"text-align: center; width: 65px; padding-top: 25px;\">Batal</div></th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
										
				<div class=\"bDiv\" style=\"min-height: 50px\">
					<table class=\"flexme4\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"width: 100%;\">
						<tbody>";
						$i=1;
						$j=1;
						$date=date("Y");
						$query = $this->db->query("SELECT * FROM jadwal WHERE DikID=".$contentid." AND StartDate>='".$date."-01-01' ORDER BY AngkatanNumber ASC");
						if($query->num_rows() > 0){
							//echo "DikID=".$contentid;
							foreach($query->result() as $row){
								if($url1!=""){ $urllink=base_url().$url1; }
								if($url2!=""){ $urllink=$urllink."/".$url2; }
								if($url3!=""){ $urllink=$urllink."/".$url3; }
									
								$link3="<input type=\"button\" value=\"BATAL\" disabled/>";
								$link="<input type=\"button\" value=\"DITUTUP\" disabled/>";
								//$link2="<img src=\"".base_url()."public/images/doc.png\" width=\"20px\" />";
								$link2="<img src=\"".base_url()."assets/img/site/doc.png\" width=\"20px\" onclick=\"document.location='".$urllink."/letter/?id=".$row->SchedID."'\" style=\"cursor: pointer\"/>";
									
								if($row->Status==1 && $row->StartDate>date("Y-m-d") && $row->Confirm<$row->Quota){
									$link="<input type=\"button\" onclick=\"document.location='".$urllink."/registration/?id=".$row->SchedID."'\" value=\"DAFTAR\"/>";
								}
									
								if($row->Status==1 && $row->StartDate>date("Y-m-d")){
									$link2="<img src=\"".base_url()."assets/img/site/doc.png\" width=\"20px\" onclick=\"document.location='".$urllink."/letter/?id=".$row->SchedID."'\" style=\"cursor: pointer\"/>";
								}
									
								$datenow=strtotime(date("Y-m-d"));
								$datestarting=strtotime($row->StartDate);
									
								if((($datestarting-$datenow)/3600/24)>4 && $row->Status==1){
									$link3="<input type=\"button\" onclick=\"document.location='".$urllink."/cancel/?id=".$row->SchedID."'\" value=\"BATAL\"/>";
								}
									
								$date=explode('-',$row->StartDate);
								$startdate=$date[2];
								$date2=explode('-',$row->EndDate);
								$enddate=$date2[2];
								if($date[1]==$date2[1]){
									$timetodo=$startdate." s.d ".$enddate." ".date("M", mktime(0,0,0,$date2[1],1,$date2[0]))." ".$date2[0];
								} else {
									$timetodo=$startdate." ".date("M", mktime(0,0,0,$date[1],1,$date[0]))." ".$date[0]." s.d ".$enddate." ".date("M", mktime(0,0,0,$date2[1],1,$date2[0]))." ".$date2[0];
								}
									
								if($i==2*$j-1){
									$content=$content. 
										"<tr id=\"row".$i."\">
											<td align=\"center\"><div style=\"text-align: center; width: 40px;\">".$row->ClassDik."</div></td>
											<td align=\"center\"><div style=\"text-align: center; width: 140px;\">".$timetodo."<br /><strong>Total Kuota <br />(Online & Manual):<br />".$row->TotalQuota." Peserta</strong></div></td>
											<td align=\"center\"><div style=\"text-align: center; width: 50px;\">".$row->Quota."</div></td>
											<td align=\"center\"><div style=\"text-align: center; width: 50px;\">".$row->Apply."</div></td>
											<td align=\"center\"><div style=\"text-align: center; width: 50px;\">".$row->Confirm."</div></td>
											<td align=\"center\"><div style=\"text-align: center; width: 85px;\">".$link."</div></td>
											<td align=\"center\"><div style=\"text-align: center; width: 70px;\">".$link2."</div></td>
											<td align=\"center\"><div style=\"text-align: center; width: 65px;\">".$link3."</div></td>
										</tr>";
									$j++;
								} else {
									$content=$content.
										"<tr id=\"row".$i."\" class=\"erow\">
											<td align=\"center\"><div style=\"text-align: center; width: 40px;\">".$row->ClassDik."</div></td>
											<td align=\"center\"><div style=\"text-align: center; width: 140px;\">".$timetodo."<br /><strong>Total Kuota <br />(Online & Manual):<br />".$row->TotalQuota." Peserta</strong></div></div></td>
											<td align=\"center\"><div style=\"text-align: center; width: 50px;\">".$row->Quota."</div></td>
											<td align=\"center\"><div style=\"text-align: center; width: 50px;\">".$row->Apply."</div></td>
											<td align=\"center\"><div style=\"text-align: center; width: 50px;\">".$row->Confirm."</div></td>
											<td align=\"center\"><div style=\"text-align: center; width: 85px;\">".$link."</div></td>
											<td align=\"center\"><div style=\"text-align: center; width: 70px;\">".$link2."</div></td>
											<td align=\"center\"><div style=\"text-align: center; width: 65px;\">".$link3."</div></td>
										</tr>";
								}
								$i++;
							}
						} else {
							$content=$content.
								'<tr><td colspan="8" style="text-align: center; padding: 20px;">Maaf belum ada jadwal untuk kegiatan Diklat ini</td></tr>';
						}

								$content=$content.					
									"			</tbody>
											</table>
											<div class=\"iDiv\" style=\"display: none;\"></div>
										</div>
									
									
									</div>
									
									<div class=\"warning\" style=\"color: red;\">
										<h2>PERHATIAN!</h2>
										<p>MOHON PERHATIKAN JADWAL DAN WAKTU PELAKSANAAN DIKLAT. APA BILA TELAH MENDAFTAR DAN TIDAK HADIR PADA DIKLAT YANG DIPILIH SERTA TIDAK MEMBERIKAN PEMBATALAN, MAKA ANDA TIDAK DIPRIORITASKAN DAPAT MENGIKUTI DIKLAT DI TAHUN YANG SAMA.</p>
										<p>JIKA TELAH BERHASIL MELAKUKAN PENDAFTARAN AKAN MELAKUKAN PEMBATALAN, MAKA KLIK TOMBOL <strong>BATAL</strong> KEMUDIAN ISIKAN NIP ANDA. LINK PEMBATALAN AKAN DIKIRIMKAN VIA EMAIL. DAN JIKA AKAN PINDAH KE ANGKATAN YANG LAIN, MAKA LAKUKAN PEMBATALAN TERLEBIH DAHULU KEMUDIAN PROSES PENDAFTARAN KEMBALI. PROSES PEMBATALAN DIPERBOLEHKAN DALAM WAKTU SEBELUM 5 HARI WAKTU PELAKSANAAN DIKLAT.</p>
										<p>PENDAFTARAN AKAN DITUTUP APABILA JUMLAH PESERTA YANG TELAH KONFIRMASI SESUAI DENGAN KUOTA DAN ATAU MELEBIHI TANGGAL PELAKSANAAN DIKLAT</p>
										<p>PESERTA YANG DIPERBOLEHKAN MENGIKUTI DIKLAT ADALAH PESERTA YANG TELAH MELAKUKAN KONFIRMASI VIA EMAIL</p>
										<p>JADWAL DAPAT BERUBAH SETIAP SAAT, MOHON DIMONITOR MELALUI WEBSITE INI DAN FACEBOOK PUSDIKLAT BKPM</p>
									</div>";
						

?>
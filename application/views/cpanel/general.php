<div id="admin-content">
	<div class="admin-content-title"><h1>Social Media</h1></div>
	<div class="admin-content-content">
		<!--[if IE]>
			<style>
				.admin-content-content  input[type="text"] { 
					padding: 10px 5px 5px 5px;
				}
				.admin-content-content  select { 
					padding: 10px 5px 5px 5px;
				}
			</style>
		<![endif]-->
		<!--[if !IE]> -->
			<style>
				.admin-content-content  input[type="text"] { 
					padding: 5px 5px 5px 5px;
				}
				.admin-content-content  select { 
					padding: 5px 5px 5px 5px;
				}
			</style>
		<!-- <![endif]-->
		<?php
			$error=$this->session->userdata('warning');
			$warning=str_replace('<p>','<li>',$error);
			$warning=str_replace('</p>','</li>',$warning);
			if(isset($error) && $error){
				echo "
					<div id=\"tips\" >
						<ul>".$warning."</ul>
					</div>";
			}
			if($this->session->userdata('warning')!=""){
				$this->session->set_userdata('warning','');
			}
		
			$this->db->select('*');
			$this->db->from('sosmed');
			$result=$this->db->get();
			$result=$result->result();
			foreach($result as $row){
				if($row->SosMed=="Facebook"){
					$fblink=$row->Url;
				}
				if($row->SosMed=="Twitter"){
					$twtlink=$row->Url;
				}
				if($row->SosMed=="Google Plus"){
					$googleplus=$row->Url;
				}
				if($row->SosMed=="Instagram"){
					$instagram=$row->Url;
				}
			}		
		?>
		<form action="" method="post" name="post" id="post" autocomplete="off">
			<h4>Facebook</h4>
			<input type="text" name="fblink" id="fblink" size="109%" value="<?php echo $fblink; ?>"/>
			<div style="clear: both"></div>
			<h4>Twitter</h4>
			<input type="text" name="twtlink" id="twtlink" size="109%" value="<?php echo $twtlink; ?>"/>
			<div style="clear: both"></div>
			<h4>Google Plus</h4>
			<input type="text" name="googleplus" id="googleplus" size="109%" value="<?php echo $googleplus; ?>"/>
			<div style="clear: both"></div>
			<h4>Instagram</h4>
			<input type="text" name="instagram" id="instagram" size="109%" value="<?php echo $instagram; ?>"/>
			<div style="clear: both"></div>
			<p><input type="submit" name="submitupdatesosmed" value="Update" /></p>
		</form>
	</div>
</div>
<!-- Bootstrap styles -->
<!--<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
<!-- Generic page styles -->
<link rel="stylesheet" href="<?=base_url()?>public/css/admin/style.css">
<!-- blueimp Gallery styles -->
<link rel="stylesheet" href="http://blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href="<?=base_url()?>public/css/admin/jquery.fileupload.css">
<link rel="stylesheet" href="<?=base_url()?>public/css/admin/jquery.fileupload-ui.css">
<!-- CSS adjustments for browsers with JavaScript disabled -->
<noscript><link rel="stylesheet" href="<?=base_url()?>public/css/admin/jquery.fileupload-noscript.css"></noscript>
<noscript><link rel="stylesheet" href="<?=base_url()?>public/css/admin/jquery.fileupload-ui-noscript.css"></noscript>
<div class="row-fluid">
	<div class="span12">
		<ul class="breadcrumb">
			<li><a style="cursor: pointer; ">Media</a> <span class="divider">/</span></li>
			<li><a href="<?=base_url(); ?>cpanel/media/gallery-video">Album Video</a> <span class="divider">/</span></li>
			<li><a href="<?=base_url(); ?>cpanel/media/add-video">Tambah Video</a> <span class="divider">/</span></li>
		</ul>
	</div>
</div>

<div class="row-fluid">
	<section class="utopia-widget utopia-form-box section">
		<div class="utopia-widget-title">
			<img src="<?=base_url(); ?>public/images/admin/play.png" class="utopia-widget-icon">
			<span>Upload Video</span>
		</div>

		<div class="row-fluid">
			<div id="showform" style="padding: 10px 10px 10px 10px;">			
				<div class="container" style="width: 1050px;">
					<!-- The file upload form used as target for the file upload widget -->
					<form id="fileupload" action="<?=base_url()?>server/php/index.php" method="POST" enctype="multipart/form-data" data-ng-app="demo" data-ng-controller="FileUploadController" data-file-upload="options" data-ng-class="{'fileupload-processing': processing() || loadingFiles}">
						<fieldset>
							<div class="control-group">
								<div class="controls">
									<select name="albumvideo" id="albumvideo" disabled>
										<option value="">Pilih Album</option>
										<?php
											$query=@mysql_query("SELECT * FROM albumvideo");
											while($row=@mysql_fetch_array($query)){
												if($_GET['album']==$row['AlbVdID']){ $selected="selected"; } else { $selected=""; }
												echo "<option value=\"".$row['AlbVdID']."\" ".$selected.">".$row['TitleAlbum']."</option>";
											}
										?>
									</select>
								</div>
							</div>
						</fieldset>
						
						<!-- Redirect browsers with JavaScript disabled to the origin page -->
						<noscript><input type="hidden" name="redirect" value="<?=base_url()?>server/php/"></noscript>
						<!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
						<div class="row fileupload-buttonbar" style="margin: 0; width: 1050px;">
							<div class="col-lg-7" style="width: 500px;">
								<!-- The fileinput-button span is used to style the file input field as button -->
								<span class="btn btn-success fileinput-button" ng-class="{disabled: disabled}">
									<i class="glyphicon glyphicon-plus"></i>
									<span>Add files...</span>
									<input type="file" name="files" id="files" ng-disabled="disabled">
								</span>
								<button type="button" class="btn btn-primary start" data-ng-click="submit()" style="width: 120px;">
									<i class="glyphicon glyphicon-upload"></i>
									<span>Start upload</span>
								</button>
								<button type="button" class="btn btn-warning cancel" data-ng-click="cancel()" style="width: 120px;">
									<i class="glyphicon glyphicon-ban-circle"></i>
									<span>Cancel upload</span>
								</button>
								<!-- The global file processing state -->
								<span class="fileupload-process"></span>
							</div>
							<!-- The global progress state -->
							<div class="col-lg-5 fade" data-ng-class="{in: active()}" style="width: 1050px;">
								<!-- The global progress bar -->
								<div class="progress progress-striped active" data-file-upload-progress="progress()"><div class="progress-bar progress-bar-success" data-ng-style="{width: num + '%'}"></div></div>
								<!-- The extended global progress state -->
								<div class="progress-extended">&nbsp;</div>
							</div>
						</div>
						<!-- The table listing the files available for upload/download -->
						<table class="table table-striped files ng-cloak" style="width:1050px">
							<tr data-ng-repeat="file in queue" data-ng-class="{'processing': file.$processing()}">
								<td data-ng-switch data-on="!!file.thumbnailUrl">
									<div class="preview" data-ng-switch-when="true">
										<a data-ng-href="{{file.url}}" title="{{file.name}}" download="{{file.name}}" data-gallery><img data-ng-src="{{file.thumbnailUrl}}" alt=""></a>
									</div>
									<div class="preview" data-ng-switch-default data-file-upload-preview="file"></div>
								</td>
								<td>
									<p class="name" data-ng-switch data-on="!!file.url">
										<span data-ng-switch-when="true" data-ng-switch data-on="!!file.thumbnailUrl">
											<a data-ng-switch-when="true" data-ng-href="{{file.url}}" title="{{file.name}}" download="{{file.name}}" data-gallery>{{file.name}}</a>
											<a data-ng-switch-default data-ng-href="{{file.url}}" title="{{file.name}}" download="{{file.name}}">{{file.name}}</a>
										</span>
										<span data-ng-switch-default>{{file.name}}</span>
										<input type="hidden" id="fileuploadname" value="{{file.name}}" />
									</p>
									<strong data-ng-show="file.error" class="error text-danger">{{file.error}}</strong>
								</td>
								<td>
									<p class="size">{{file.size | formatFileSize}}</p>
									<div class="progress progress-striped active fade" data-ng-class="{pending: 'in'}[file.$state()]" data-file-upload-progress="file.$progress()"><div class="progress-bar progress-bar-success" data-ng-style="{width: num + '%'}"></div></div>
								</td>
								<td>
									<button type="button" class="btn btn-primary start" data-ng-click="file.$submit()" data-ng-hide="!file.$submit || options.autoUpload" data-ng-disabled="file.$state() == 'pending' || file.$state() == 'rejected'" style="width: 100px;">
										<i class="glyphicon glyphicon-upload"></i>
										<span>Start</span>
									</button>
									<button type="button" class="btn btn-warning cancel" data-ng-click="file.$cancel()" data-ng-hide="!file.$cancel" style="width: 100px;">
										<i class="glyphicon glyphicon-ban-circle"></i>
										<span>Cancel</span>
									</button>
									<button data-ng-controller="FileDestroyController" type="button" class="btn btn-danger destroy" data-ng-click="file.$destroy()" data-ng-hide="!file.$destroy" style="width: 100px;">
										<i class="glyphicon glyphicon-trash"></i>
										<span>Delete</span>
									</button>
								</td>
							</tr>
						</table>
					</form>
					<div style="clear: both"></div>
					<input type="button" class="btn btn-primary span5"  style="width: 200px; float: left;" value="Simpan" onclick="savedata()"/>
					<div style="clear: both; margin-bottom: 20px;"></div>
				<div>
					<table class="table table-bordered">
						<colgroup>
							<col class="utopia-col-0"></col>
							<col class="utopia-col-1"></col>
							<col class="utopia-col-1"></col>
						</colgroup>
						<thead>
							<tr>
								<th style="width: 94%; text-align: center;">Nama Video</th>
								<th style="width: 6%; text-align: center;">Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php
								$query=@mysql_query("SELECT * FROM video WHERE AlbVdID='".$_GET['album']."' ORDER BY VideoID DESC");
								$ndata=@mysql_num_rows($query);
								if($ndata==0){
									echo "
										<tr>
											<td colspan=\"2\" style=\"text-align: center;\">Belum ada video untuk album ini. Silahkan tambahkan terlebih dahulu!</td>
										</tr>";
								} else {
									while($row=@mysql_fetch_array($query)){
										echo "
											<tr>
												<td>&nbsp;".$row['VideoName']."</td>
												<td><div align=\"center\"><img src=\"".base_url()."/asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Gambar\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus ".$row['VideoName']." dari database?')){document.location='../delete-vid/?post=".$row['VideoID']."'; }\" /></div></td>
											</tr>";
										
									}
								}
							?>
						</tbody>
					</table>
				</div>
				</div>
				<div id="save"></div>
			</div>
		</div>
	</section>
</div>

<script>
function savedata(){
	//alert(document.getElementById('fileuploadname').value);
	ajaxfunc('start&type=addvideodata&album='+document.getElementById('albumvideo').value+'&filename='+encode(document.getElementById('fileuploadname').value),'save','<?=base_url()?>public/ajax/ajax.php');
}
</script>
<!-- The blueimp Gallery widget -->
				<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
					<div class="slides"></div>
					<h3 class="title"></h3>
					<a class="prev">�</a>
					<a class="next">�</a>
					<a class="close">�</a>
					<a class="play-pause"></a>
					<ol class="indicator"></ol>
				</div>
				<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
				<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.12/angular.min.js"></script>
				<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
				<script src="<?=base_url()?>public/js/admin/vendor/jquery.ui.widget.js"></script>
				<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
				<script src="http://blueimp.github.io/JavaScript-Load-Image/js/load-image.min.js"></script>
				<!-- The Canvas to Blob plugin is included for image resizing functionality -->
				<script src="http://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
				<!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
				<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
				<!-- blueimp Gallery script -->
				<script src="http://blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
				<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
				<script src="<?=base_url()?>public/js/admin/jquery.iframe-transport.js"></script>
				<!-- The basic File Upload plugin -->
				<script src="<?=base_url()?>public/js/admin/jquery.fileupload.js"></script>
				<!-- The File Upload processing plugin -->
				<script src="<?=base_url()?>public/js/admin/jquery.fileupload-process.js"></script>
				<!-- The File Upload image preview & resize plugin -->
				<script src="<?=base_url()?>public/js/admin/jquery.fileupload-image.js"></script>
				<!-- The File Upload audio preview plugin -->
				<script src="<?=base_url()?>public/js/admin/jquery.fileupload-audio.js"></script>
				<!-- The File Upload video preview plugin -->
				<script src="<?=base_url()?>public/js/admin/jquery.fileupload-video.js"></script>
				<!-- The File Upload validation plugin -->
				<script src="<?=base_url()?>public/js/admin/jquery.fileupload-validate.js"></script>
				<!-- The File Upload Angular JS module -->
				<script src="<?=base_url()?>public/js/admin/jquery.fileupload-angular.js"></script>
				<!-- The main application script -->
				<script src="<?=base_url()?>public/js/admin/app.js"></script>
<div class="row-fluid">
	<div class="span12">
		<ul class="breadcrumb">
			<li><a href="<?=base_url(); ?>cpanel/media/gallery-photo" style="cursor: pointer; ">Album Photo</a> <span class="divider">/</span></li>
			<li><a href="<?=base_url(); ?>cpanel/media/add-photo/?album=<?=$_GET['album'];?>">Tambah Photo</a> <span class="divider">/</span></li>
		</ul>
	</div>
</div>

<?php
	$error=$this->session->userdata('warning');
	$warning=str_replace('<p>','<li>',$error);
	$warning=str_replace('</p>','</li>',$warning);
	if(isset($error) && $error){
		echo "
			<div class=\"alert alert-block\" >
				<h4 class=\"alert-heading\">Peringatan!</h4>
				<ul>".$warning."</ul>
			</div>";
	}
	if($this->session->userdata('warning')!=""){
		$this->session->set_userdata('warning','');
	}
	$row=@mysql_fetch_array(@mysql_query("SELECT * FROM albumphoto WHERE AlbPhID='".$_GET['post']."'"));	
?>

<div class="row-fluid">
	<section class="utopia-widget utopia-form-box section">
		<div class="utopia-widget-title">
			<img src="<?=base_url(); ?>public/images/admin/monitor.png" class="utopia-widget-icon">
			<span>Tambah Photo</span>
		</div>

		<div class="row-fluid">
			<div id="showform" style="padding: 10px 10px 10px 10px;">
				<form class="form-horizontal" action="" method="post" name="post" id="post" enctype="multipart/form-data">
                    <fieldset>
                        <div class="control-group">
                            <label class="control-label" for="Album">Album Photo</label>
							<input type="hidden" name="albumphoto" value="<?php echo $_GET['album']; ?>" />
                            <div class="controls">
                                <select name="albumphoto" id="albumphoto" disabled>
									<option value="">Pilih Album</option>
									<?php
										$query=@mysql_query("SELECT * FROM albumphoto");
										while($row=@mysql_fetch_array($query)){
											if($_GET['album']==$row['AlbPhID']){ $selected="selected"; } else { $selected=""; }
											echo "<option value=\"".$row['AlbPhID']."\" ".$selected.">".$row['TitleAlbum']."</option>";
										}
									?>
								</select>
                            </div>
                        </div>
						
						<div class="control-group">
                            <label class="control-label" for="content">Deskripsi Foto</label>
                            <div class="controls">
                              <textarea name="desc-photo" rows="15" value="" id="desc-photo"></textarea>
                            </div>
                        </div>
						
						<div class="control-group">
                            <label class="control-label" for="file">File Gambar (ukuran < 2 MB)</label>
                            <div class="controls">
								<input type="file" name="userfile[]" id="imagefile" multiple="true" accept="image/*" size="50" />
                            </div>
                        </div>
						
						<p><input type="submit" value="Simpan" class="btn btn-primary span5"  name="submit" style="width: 100px; margin-left: 20px;"/></p>
                    </fieldset>
                </form>
				
				<div style="clear: both"></div>
				
				<div>
					<table class="table table-bordered">
					<colgroup>
						<col class="utopia-col-0"></col>
						<col class="utopia-col-1"></col>
						<col class="utopia-col-1"></col>
					</colgroup>
					<thead>
						<tr>
							<th style="width: 47%; text-align: center;">Preview</th>
							<th style="width: 47%; text-align: center;">Deskripsi</th>
							<th style="width: 6%; text-align: center;">Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$query=@mysql_query("SELECT * FROM photos WHERE AlbPhID='".$_GET['album']."' ORDER BY PhotoID DESC");
							$ndata=@mysql_num_rows($query);
							if($ndata==0){
								echo "
									<tr>
										<td colspan=\"3\" style=\"text-align: center;\">Belum ada photo untuk album ini. Silahkan tambahkan terlebih dahulu!</td>
									</tr>";
							} else {
								while($row=@mysql_fetch_array($query)){
									echo "
										<tr>
											<td><div align=\"center\"><img src=\"".base_url()."/uploads/gallery/photo/".$row['ImageThumb']."\" alt=\"Preview\" /></div></td>
											<td>&nbsp;".$row['ImageDesc']."</td>
											<td><div align=\"center\"><img src=\"".base_url()."/asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Gambar\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus ".$row['ImageName']." dari database?')){document.location='../delete-pic/?post=".$row['PhotoID']."'; }\" /></div></td>
										</tr>";
									
								}
							}
						?>
					</tbody>
				</table>
				</div>
				
				
			</div>
		</div>
	</section>
</div>
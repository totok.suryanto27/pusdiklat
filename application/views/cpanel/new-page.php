<script type="text/javascript" src="<?=base_url();?>asset/admin/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
	tinymce.init({
		selector: "textarea",
		theme: "modern",
		plugins: [
			"advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
			"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
			"table contextmenu directionality emoticons template textcolor paste textcolor filemanager"
			],
		toolbar1: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | formatselect fontselect fontsizeselect | image",
		menubar: false
	 });
</script>

<div class="row-fluid">
	<div class="span12">
		<ul class="breadcrumb">
			<li><a href="<?=base_url(); ?>cpanel/pages/all-pages" style="cursor: pointer; ">Halaman</a> <span class="divider">/</span></li>
			<li><a href="<?=base_url(); ?>cpanel/pages/new-page">Buat Halaman</a> <span class="divider">/</span></li>
		</ul>
	</div>
</div>

<?php
	$error=$this->session->userdata('warning');
	$warning=str_replace('<p>','<li>',$error);
	$warning=str_replace('</p>','</li>',$warning);
	if(isset($error) && $error){
		echo "
			<div class=\"alert alert-block\" >
				<h4 class=\"alert-heading\">Peringatan!</h4>
				<ul>".$warning."</ul>
			</div>";
	}
	if($this->session->userdata('warning')!=""){
		$this->session->set_userdata('warning','');
	}
		
	if($this->session->userdata('isPage')){			
		$data=$this->session->userdata('isPage');
		$titlepage=$data['titlepage'];
		$contentpage=$data['contentpage'];
	} else {
		$titlepage="";
		$contentpage="";
	}
			
?>

<div class="row-fluid">
	<section class="utopia-widget utopia-form-box section">
		<div class="utopia-widget-title">
			<img src="<?=base_url(); ?>public/images/admin/monitor.png" class="utopia-widget-icon">
			<span>Buat Halaman</span>
		</div>

		<div class="row-fluid">
			<div id="showform" style="padding: 10px 10px 10px 10px;">
				<form class="form-horizontal" action="" method="post" name="post" id="post"">
                    <fieldset>
                        <div class="control-group">
                            <label class="control-label" for="Judul Halaman">Judul Halaman</label>
							<input type="hidden" name="page" id="page" value="pages" />
							<input type="hidden" name="idpost" id="idpost" value="new-page" />
                            <div class="controls">
                                <input type="text" name="titlepage" id="titlepage" style="width: 500px" value="<?php echo $titlepage; ?>"/><br />
                            </div>
                        </div>
						
						 <div class="control-group">
                            <label class="control-label" for="content">Isi Halaman</label>
                            <div class="controls">
                               <textarea name="contentpage" rows="15" value="" id="contentpage"><?php echo $contentpage; ?></textarea>
                            </div>
                        </div>
						
						<p><input type="submit" value="Simpan" class="btn btn-primary span5"  name="submitpages" style="width: 100px; margin-left: 20px;"/></p>
                    </fieldset>
                </form>
			</div>
		</div>
	</section>
</div>
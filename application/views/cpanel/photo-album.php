<div class="row-fluid">
	<div class="span12">
		<ul class="breadcrumb">
			<li><a style="cursor: pointer; ">Media</a> <span class="divider">/</span></li>
			<li><a href="<?=base_url(); ?>cpanel/media/gallery-photo">Album Photo</a> <span class="divider">/</span></li>
		</ul>
	</div>
</div>

<div class="row-fluid">
	<section class="utopia-widget utopia-form-box section">
		<div class="utopia-widget-title">
			<img src="<?=base_url(); ?>public/images/admin/monitor.png" class="utopia-widget-icon">
			<span>Album Photo</span>
		</div>

		<div class="row-fluid">
			<div style="width: 200px; margin: 20px 20px 20px 20px;">
				<input type="button" class="btn btn-primary span5"  style="width: 200px; float: left;" value="Buat Album Photo Baru" onclick="document.location='<?php echo base_url(); ?>cpanel/media/new-albumphoto'"/>
			</div>
			<div style="clear:both"></div>
			<div style="margin: 20px 20px 20px 20px;">
				<table class="table table-bordered">
					<colgroup>
						<col class="utopia-col-0"></col>
						<col class="utopia-col-1"></col>
						<col class="utopia-col-1"></col>
					</colgroup>
					<thead>
						<tr>
							<th style="width: 40%; text-align: center;">Judul Album</th>
							<th style="width: 40%; text-align: center;">Deskripsi</th>
							<th style="width: 10%; text-align: center;">n Gambar</th>
							<th style="width: 10%; text-align: center;">Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$query=@mysql_query("SELECT * FROM albumphoto WHERE 2 ORDER BY AlbPhID DESC");
							$ndata=@mysql_num_rows($query);
							if($ndata==0){
								echo "
									<tr>
										<td colspan=\"4\" style=\"text-align: center;\">Belum ada data untuk album photo. Silahkan buat baru terlebih dahulu!</td>
									</tr>";
							} else {
								while($row=@mysql_fetch_array($query)){
									$ndata2=@mysql_num_rows(@mysql_query("SELECT * FROM photos WHERE AlbPhID='".$row['AlbPhID']."'"));
									echo "
										<tr>
											<td>".$row['TitleAlbum']."</td>
											<td>".$row['AlbumDesc']."</td>
											<td style=\"text-align: center;\">".$ndata2."</td>
											<td><div align=\"center\"><img src=\"../../asset/admin/images/add.png\" alt=\"add\" title=\"Tambah Video\" style=\"cursor: pointer\" onclick=\"document.location='../media/add-photo/?album=".$row['AlbPhID']."'\" /> <img src=\"../../asset/admin/images/edit.png\" alt=\"edit\"  title=\"Edit Album\" style=\"cursor: pointer\" onclick=\"document.location='../media/edit-albph/?post=".$row['AlbPhID']."'\" /> <img src=\"../../asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Album\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus ".$row['TitleAlbum']." dari database?')){document.location='../media/delete-albph/?post=".$row['AlbPhID']."'; }\" /></div></td>
										</tr>";
									
								}
							}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</section>
</div>
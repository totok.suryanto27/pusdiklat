<?php
	// Dapatkan info dari tabel albumphoto
	$this->db->select('*');
	$this->db->from('albumphoto');
	$this->db->where('AlbPhID',$_GET['post']);
	$result=$this->db->get();
	$dbimage=$result->result();
	foreach($dbimage as $row){
		$url=$row->Url;
	}
	
	// Hapus di tabel
	$this->db->where('AlbPhID',$_GET['post']);
	$this->db->delete('albumphoto');
	
	// Akses tabel photos
	$this->db->select('*');
	$this->db->from('photos');
	$this->db->where('AlbPhID',$_GET['post']);
	$result=$this->db->get();
	$dbimage=$result->result();
	$filestring=PUBPATH.'uploads/gallery/photo/';
	foreach($dbimage as $row){
		$imagename=$row->ImageName;
		$imagethumb=$row->ImageThumb;
		// Hapus file yang berhubungan
		unlink($filestring.$imagename);
		unlink($filestring.$imagethumb);
	}
	
	// Hapus di url juga
	$this->db->where('Url_1','galeri-foto');
	$this->db->where('Url_2',$url);
	$this->db->delete('url');
	
	echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/media/gallery-photo\" />";
	
?>
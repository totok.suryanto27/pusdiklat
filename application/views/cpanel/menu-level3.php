<div id="admin-content">
	<div class="admin-content-title"><h1>Tambah Menu Level 3</h1></div>
	<div class="admin-content-content">
		<!--[if IE]>
			<style>
				.admin-content-content  input[type="text"] { 
					padding: 10px 5px 5px 5px;
				}
				.admin-content-content  select { 
					padding: 10px 5px 5px 5px;
				}
			</style>
		<![endif]-->
		<!--[if !IE]> -->
			<style>
				.admin-content-content  input[type="text"] { 
					padding: 5px 5px 5px 5px;
				}
				.admin-content-content  select { 
					padding: 5px 5px 5px 5px;
				}
			</style>
		<!-- <![endif]-->
		<?php
			$error=$this->session->userdata('warning');
			$warning=str_replace('<p>','<li>',$error);
			$warning=str_replace('</p>','</li>',$warning);
			if(isset($error) && $error){
				echo "
					<div id=\"tips\" >
						<ul>".$warning."</ul>
					</div>";
			}
			if($this->session->userdata('warning')!=""){
				$this->session->set_userdata('warning','');
			}
			
			if($this->session->userdata('isMenulvl3')){			
				$data=$this->session->userdata('isMenulvl3');
				$titlemenulvl3=$data['titlemenulvl3'];
				$menulvl2=$data['menulvl2'];
				$webtitle=$data['webtitle'];
			} else {
				$titlemenulvl3="";
				$menulvl2="";
				$webtitle="";
			}
		?>
		<p>Menu Level 3 adalah merupakan perluasan dari Menu Level 2</p>
		
		<img src="<?php echo base_url();?>asset/admin/images/menu-level3.jpg" width="920px"/>
		<form action="" method="post" name="post" id="post" autocomplete="off">
			<h4 style="margin-top: 10px;">Nama Menu Level 3</h4>
			<input type="text" name="titlemenulvl3" id="titlemenulvl3" size="109%" value="<?php echo $titlemenulvl3; ?>" />
			<div style="clear: both"></div>
			<h4 style="margin-top: 10px;">Menu Level 2</h4>
			<select name="menulvl2" id="menulvl2">
				<option value="">Pilih Satu</option>
				<?php
					$query=@mysql_query("SELECT * FROM menulvl2 WHERE HaveSub='Ada'");
					while($row=@mysql_fetch_array($query)){
						if($menulvl2==$row['Lvl2ID']){ $selected="selected"; } else { $selected=""; }
						echo "<option value=\"".$row['Lvl2ID']."\" ".$selected.">".$row['MenuName']."</option>";
					}
				?>
			</select>
			<div id="link"><a onclick="document.location='<?php echo base_url(); ?>cpanel/appereance/menu-level2'">+ Tambah Menu Level 2</a></div>
			<div style="clear: both"></div>
			<h4 style="margin-top: 10px;">Judul (Cth: Sejarah Pusdiklat BKPM)</h4>
			<input type="text" name="webtitle" id="webtitle" size="109%" value="<?php echo $webtitle; ?>"/>
			<div style="clear: both"></div>
			<fieldset>
				<div class="optradio"><input type="radio" name="opt" id="opt" value="pages" onclick="activateinput(this.value)"></div>
				<div class="opttitle">Halaman</div>
				<div class="optinput">
					<select name="optinpages" id="optinpages" disabled style="width: 350px;">
						<option value="">Pilih Satu</option>
						<?php
							$query=@mysql_query("SELECT * FROM pages WHERE 1");
							while($row=@mysql_fetch_array($query)){
								echo "<option value=\"".$row['PageID']."\">".$row['TitlePage']."</option>";
							}
						?>
					</select>
				</div>
				<div style="clear: both"></div>
				<div class="optradio"><input type="radio" name="opt" id="opt" value="category" onclick="activateinput(this.value)"></div>
				<div class="opttitle">Kategori</div>
				<div class="optinput">
					<select name="optincat" id="optincat" disabled style="width: 350px;">
						<option value="">Pilih Satu</option>
						<?php
							$query=@mysql_query("SELECT * FROM categories WHERE 1");
							while($row=@mysql_fetch_array($query)){
								echo "<option value=\"".$row['CatID']."\">".$row['Categories']."</option>";
							}
						?>
					</select>
				</div>
				<div style="clear: both"></div>
				<div class="optradio"><input type="radio" name="opt" id="opt" value="other" onclick="activateinput(this.value)"></div>
				<div class="opttitle">Link Luar [ http:// ]</div>
				<div class="optinput">
					<input type="text" name="otherlink" id="otherlink" size="40%" value="" disabled/>
				</div>
				<div style="clear: both"></div>
				<div class="optradio" style="height: 25px;"><input type="radio" name="opt" id="opt" value="photo" onclick="activateinput(this.value)"></div>
				<div class="opttitle">Gallery Photo</div>
				<div class="optinput">
					&nbsp;
				</div>
				<div style="clear: both"></div>
				<div class="optradio" style="height: 25px;"><input type="radio" name="opt" id="opt" value="video" onclick="activateinput(this.value)"></div>
				<div class="opttitle">Gallery Video</div>
				<div class="optinput">
					&nbsp;
				</div>
				<div style="clear: both"></div>
				<div class="optradio" style="height: 25px;"><input type="radio" name="opt" id="opt" value="consultation" onclick="activateinput(this.value)"></div>
				<div class="opttitle">Konsultasi</div>
				<div class="optinput">
					&nbsp;
				</div>
				<div style="clear: both"></div>
			</fieldset>
			<div style="clear: both"></div>
			<input type="submit" name="submitlevel3" id="submitlevel3" value="Simpan" />
		</form>
	</div>
</div>
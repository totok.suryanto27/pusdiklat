<div class="row-fluid">
	<div class="span12">
		<ul class="breadcrumb">
			<li><a style="cursor: pointer; ">Konten</a> <span class="divider">/</span></li>
		</ul>
	</div>
</div>
<div class="row-fluid">
	<section class="utopia-widget utopia-form-box section">
		<div class="utopia-widget-title">
			<img src="<?=base_url(); ?>public/images/admin/monitor.png" class="utopia-widget-icon">
			<span>Konten</span>
		</div>

		<div class="row-fluid">
			<div style="width: 200px; margin: 20px 20px 20px 20px;">
				<input type="button" class="btn btn-primary span5"  style="width: 200px; float: left;" value="Buat Konten Baru" onclick="document.location='<?php echo base_url(); ?>cpanel/content/new-post'"/>
			</div>
			<div style="clear:both"></div>
			<div style="margin: 20px 20px 20px 20px;">
				<table class="table table-bordered">
					<colgroup>
						<col class="utopia-col-0"></col>
						<col class="utopia-col-1"></col>
						<col class="utopia-col-0"></col>
					</colgroup>
					<thead>
						<tr>
							<th style="width: 50%; text-align: center;">Judul Konten</th>
							<th style="width: 20%; text-align: center;">Kategori</th>
							<th style="width: 20%; text-align: center;">Tanggal Pembuatan</th>
							<th style="width: 10%; text-align: center;">Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$query=@mysql_query("SELECT * FROM posts ORDER BY RecordDate DESC");
							$ndata=@mysql_num_rows($query);
							if($ndata==0){
								echo "
									<tr>
										<td colspan=\"4\" style=\"text-align: center;\">Belum ada data untuk halaman. Silahkan buat baru terlebih dahulu!</td>
									</tr>";
							} else {
								while($row=@mysql_fetch_array($query)){
									$row2=@mysql_fetch_array(@mysql_query("SELECT * FROM categories WHERE CatID='".$row['CatID']."'"));
									$catname=$row2['Categories'];
									if($row['CatID']==0){ $catname="Tidak dikategorikan"; }
									echo "
										<tr>
											<td>".$row['TitlePost']."</td>
											<td style=\"text-align: center;\">".$catname."</td>
											<td style=\"text-align: center;\">".$row['RecordDate']."</td>
											<td><div align=\"center\"><img src=\"../../asset/admin/images/edit.png\" alt=\"edit\"  title=\"Edit Halaman\" style=\"cursor: pointer\" onclick=\"document.location='../content/edit-post/?post=".$row['PostID']."'\" /> <img src=\"../../asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Halaman\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus ".$row['TitlePost']." dari database?')){document.location='../content/delete-post/?post=".$row['PostID']."'; }\" /></div></td>
										</tr>";
									
								}
							}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</section>
</div>
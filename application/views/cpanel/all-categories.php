<div class="row-fluid">
	<div class="span12">
		<ul class="breadcrumb">
			<li><a style="cursor: pointer; ">Kategori</a> <span class="divider">/</span></li>
		</ul>
	</div>
</div>
<div class="row-fluid">
	<section class="utopia-widget utopia-form-box section">
		<div class="utopia-widget-title">
			<img src="<?=base_url(); ?>public/images/admin/monitor.png" class="utopia-widget-icon">
			<span>Kategori</span>
		</div>

		<div class="row-fluid">
			<div style="width: 200px; margin: 20px 20px 20px 20px;">
				<input type="button" class="btn btn-primary span5"  style="width: 200px; float: left;" value="Buat Kategori Baru" onclick="document.location='<?php echo base_url(); ?>cpanel/content/new-category'"/>
			</div>
			<div style="clear:both"></div>
			<div style="margin: 20px 20px 20px 20px;">
				<table class="table table-bordered">
					<colgroup>
						<col class="utopia-col-0"></col>
						<col class="utopia-col-1"></col>
						<col class="utopia-col-0"></col>
					</colgroup>
					<thead>
						<tr>
							<th style="width: 20%; text-align: center;">Kategori</th>
							<th style="width: 40%; text-align: center;">Deskripsi</th>
							<th style="width: 20%; text-align: center;">Tautan</th>
							<th style="width: 10%; text-align: center;">Jumlah Konten</th>
							<th style="width: 10%; text-align: center;">Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$query=@mysql_query("SELECT * FROM categories ORDER BY CatDesc ASC");
							$ndata=@mysql_num_rows($query);
							if($ndata==0){
								echo "
									<tr>
										<td colspan=\"5\" style=\"text-align: center;\">Belum ada data untuk halaman. Silahkan buat baru terlebih dahulu!</td>
									</tr>";
							} else {
								while($row=@mysql_fetch_array($query)){
									echo "
										<tr>
											<td>".$row['Categories']."</td>
											<td>&nbsp;".$row['CatDesc']."</td>
											<td style=\"text-align: center\">".$row['Slugs']."</td>
											<td style=\"text-align: center\">".$row['Posts']."</td>
											<td><div align=\"center\"><img src=\"../../asset/admin/images/edit.png\" alt=\"edit\"  title=\"Edit Halaman\" style=\"cursor: pointer\" onclick=\"document.location='../content/edit-cat/?post=".$row['CatID']."'\" /> <img src=\"../../asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Halaman\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus ".$row['Categories']." dari database?')){document.location='../content/delete-cat/?post=".$row['CatID']."'; }\" /></div></td>
										</tr>";
									
								}
							}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</section>
</div>
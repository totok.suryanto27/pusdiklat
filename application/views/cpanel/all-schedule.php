<div class="row-fluid">
	<div class="span12">
		<ul class="breadcrumb">
			<li><a style="cursor: pointer; ">Kegiatan Pusdiklat</a> <span class="divider">/</span></li>
			<li><a href="<?=base_url()?>cpanel/bkpm/all-schedule" style="cursor: pointer; ">Jadwal Kegiatan</a> <span class="divider">/</span></li>
		</ul>
	</div>
</div>
<div class="row-fluid">
	<section class="utopia-widget utopia-form-box section">
		<div class="utopia-widget-title">
			<img src="<?=base_url(); ?>public/images/admin/monitor.png" class="utopia-widget-icon">
			<span>Jadwal Kegiatan</span>
		</div>

		<div class="row-fluid">
			<div style="width: 200px; margin: 20px 20px 20px 20px;">
				<input type="button" class="btn btn-primary span5"  style="width: 200px; float: left;" value="Buat Jadwal Kegiatan" onclick="document.location='<?php echo base_url(); ?>cpanel/bkpm/add-schedule'"/>
			</div>
			<div style="clear:both"></div>
			<div style="margin: 20px 20px 20px 20px;">
				<table class="table table-bordered">
					<colgroup>
						<col class="utopia-col-0"></col>
						<col class="utopia-col-1"></col>
						<col class="utopia-col-0"></col>
					</colgroup>
					<thead>
						<tr>
							<th style="width: 48%; text-align: center;">Diklat</th>
							<th style="width: 15%; text-align: center;">Tanggal Pelatihan</th>
							<th style="width: 15%; text-align: center;">Kuota Peserta</th>
							<th style="width: 15%; text-align: center;">Status</th>
							<th style="width: 7%; text-align: center;">Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$query=@mysql_query("SELECT * FROM jadwal ORDER BY SchedID ASC");
							$ndata=@mysql_num_rows($query);
							if($ndata==0){
								echo "
									<tr>
										<td colspan=\"5\" style=\"text-align: center;\">Belum ada data untuk jadwal kegiatan. Silahkan buat baru terlebih dahulu!</td>
									</tr>";
							} else {
								while($row=@mysql_fetch_array($query)){
									$row2=@mysql_fetch_array(@mysql_query("SELECT * FROM activity WHERE id='".$row['DikID']."'"));
									$date=explode('-',$row['StartDate']);
									$startdate=$date[2];
									$date2=explode('-',$row['EndDate']);
									$enddate=$date2[2];
									if($date[1]==$date2[1]){
										$timetodo=$startdate." s.d ".$enddate." ".date("M", mktime(0,0,0,$date2[1],1,$date2[0]))." ".$date2[0];
									} else {
										$timetodo=$startdate." ".date("M", mktime(0,0,0,$date[1],1,$date[0]))." ".$date[0]." s.d ".$enddate." ".date("M", mktime(0,0,0,$date2[1],1,$date2[0]))." ".$date2[0];
									}
									$kuota=$row['Quota'];
									if($row['Status']==0){
										$status="Ditutup";
									} else {
										$status="Dibuka";
									}
									echo "
										<tr>
											<td>".$row2['activity']."</td>
											<td><div align=\"center\">".$timetodo."</div></td>
											<td><div align=\"center\">".$kuota."</div></td>
											<td><div align=\"center\">".$status."</div></td>
											<td><div align=\"center\"><img src=\"../../asset/admin/images/edit.png\" alt=\"edit\"  title=\"Edit Jadwal Kegiatan\" style=\"cursor: pointer\" onclick=\"document.location='../bkpm/bkpm-edit/?post=".$row['SchedID']."'\" /> <img src=\"../../asset/admin/images/delete.png\" alt=\"delete\"  title=\"Delete Jadwal Kegiatan\" style=\"cursor: pointer\" onclick=\"if(confirm('Anda ingin menghapus jadwal untuk".$row2['activity']." tanggal ".$timetodo." dari database?')){document.location='../bkpm/bkpm-delete/?post=".$row['SchedID']."'; }\" /></div></td>
										</tr>";
									
								}
							}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</section>
</div>
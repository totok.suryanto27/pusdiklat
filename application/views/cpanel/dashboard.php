
<div class="row-fluid">
	<div class="span12">
		<ul class="breadcrumb">
			<li><a href="<?=base_url(); ?>cpanel/dashboard" style="cursor: pointer; ">Dashboard</a> <span class="divider">/</span></li>
		</ul>
	</div>
</div>

<div class="row-fluid">
	<section class="utopia-widget utopia-form-box section">
		<div class="utopia-widget-title">
			<img src="<?=base_url(); ?>public/images/admin/monitor.png" class="utopia-widget-icon">
			<span>Informasi Konten</span>
		</div>

		<div class="row-fluid">
			<div style="margin: 20px 20px 20px 20px;">
				<div id="daftar_konten">
					<?php $ndata=@mysql_num_rows(@mysql_query("SELECT * FROM posts")); ?>
					<h3>Jumlah konten yang sudah ada: <a href="<?=base_url(); ?>cpanel/content/all-posts"><?php echo $ndata; ?> Konten</a></h3>
					Konten Terbaru:<br />
					<ul>
					<?php
						$query=@mysql_query("SELECT * FROM posts WHERE 1 ORDER BY RecordDate DESC LIMIT 0,5 ");
						while($row=@mysql_fetch_array($query)){
							$date=explode("-",$row['RecordDate']);
							if($date[1]=="01"){ $month="Januari"; }
							if($date[1]=="02"){ $month="Februari"; }
							if($date[1]=="03"){ $month="Maret"; }
							if($date[1]=="04"){ $month="April"; }
							if($date[1]=="05"){ $month="Mei"; }
							if($date[1]=="06"){ $month="Juni"; }
							if($date[1]=="07"){ $month="Juli"; }
							if($date[1]=="08"){ $month="Agustus"; }
							if($date[1]=="09"){ $month="September"; }
							if($date[1]=="10"){ $month="Oktober"; }
							if($date[1]=="11"){ $month="November"; }
							if($date[1]=="12"){ $month="Desember"; }
									
							echo "<li>".$date[2]." ".$month." ".$date[0].", ".$row['TitlePost']."</li>";
						}
					?>
					</ul>
					<p>&nbsp;</p>
					<?php $ndata=@mysql_num_rows(@mysql_query("SELECT * FROM pages")); ?>
					<h3>Jumlah halaman yang sudah ada: <a href="<?=base_url(); ?>cpanel/pages/all-pages"><?php echo $ndata; ?> Halaman</a></h3>
					Halaman Terbaru:<br />
					<ul>
					<?php
						$query=@mysql_query("SELECT * FROM pages WHERE 1 ORDER BY RecordDate DESC LIMIT 0,5 ");
						while($row=@mysql_fetch_array($query)){
							$date=explode("-",$row['RecordDate']);
							if($date[1]=="01"){ $month="Januari"; }
							if($date[1]=="02"){ $month="Februari"; }
							if($date[1]=="03"){ $month="Maret"; }
							if($date[1]=="04"){ $month="April"; }
							if($date[1]=="05"){ $month="Mei"; }
							if($date[1]=="06"){ $month="Juni"; }
							if($date[1]=="07"){ $month="Juli"; }
							if($date[1]=="08"){ $month="Agustus"; }
							if($date[1]=="09"){ $month="September"; }
							if($date[1]=="10"){ $month="Oktober"; }
							if($date[1]=="11"){ $month="November"; }
							if($date[1]=="12"){ $month="Desember"; }
									
							echo "<li>".$date[2]." ".$month." ".$date[0].", ".$row['TitlePage']."</li>";
						}
					?>
					</ul>
				</div>
			</div>
		</div>
	</section>
</div>
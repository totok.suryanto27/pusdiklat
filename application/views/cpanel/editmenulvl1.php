<div class="row-fluid">
	<div class="span12">
		<ul class="breadcrumb">
			<li><a style="cursor: pointer; ">Menu</a> <span class="divider">/</span></li>
			<li><a href="<?=base_url(); ?>cpanel/appereance/allmenu-level1">Menu Level 1</a> <span class="divider">/</span></li>
			<li><a href="<?=base_url(); ?>cpanel/appereance/editmenu-level1/?post=<?=$_GET['post'];?>">Ubah Menu Level 1</a> <span class="divider">/</span></li>
		</ul>
	</div>
</div>

<?php
	$error=$this->session->userdata('warning');
	$warning=str_replace('<p>','<li>',$error);
	$warning=str_replace('</p>','</li>',$warning);
	if(isset($error) && $error){
		echo "
			<div class=\"alert alert-block\" >
				<h4 class=\"alert-heading\">Peringatan!</h4>
				<ul>".$warning."</ul>
			</div>";
	}
	if($this->session->userdata('warning')!=""){
		$this->session->set_userdata('warning','');
	}
	$row=@mysql_fetch_array(@mysql_query("SELECT * FROM menulvl1 WHERE Lvl1ID='".$_GET['post']."'"));	
?>

<div class="row-fluid">
	<section class="utopia-widget utopia-form-box section">
		<div class="utopia-widget-title">
			<img src="<?=base_url(); ?>public/images/admin/monitor.png" class="utopia-widget-icon">
			<span>Ubah Menu Level 1</span>
		</div>
		
		<div class="row-fluid">
			<div id="showform" style="padding: 10px 10px 10px 10px;">
				<p>Menu Level 1 adalah menu yang dilihat dalam tampilan awal</p>
				<p>Catatan: Menu Beranda merupakan menu defaut sehingga tidak perlu ditambahkan</p>
				<img src="<?=base_url();?>asset/admin/images/menu-level1.jpg" width="920px"/>
			</div>
		</div>

		<div class="row-fluid">
			<div id="showform" style="padding: 10px 10px 10px 10px;">
				<form class="form-horizontal" action="" method="post" name="edit-post" id="edit-post">
                    <input type="hidden" name="idpost" value="<?=$_GET['post']; ?>" />
					<input type="hidden" name="oldpost" value="<?=$row['MenuName']; ?>" />
					<fieldset>
                        <div class="control-group">
                            <label class="control-label" for="Nama Menu Level 1">Nama Menu Level 1</label>
                            <div class="controls">
                                <input type="text" name="titlemenulvl1" id="titlemenulvl1" style="width: 500px" value="<?=$row['MenuName']; ?>"/><br />
                            </div>
                        </div>
						
						<div class="control-group">
                            <label class="control-label" for="categories">Kategori</label>
                            <div class="controls">
								<select name="categories">
									<option value="0">Tidak Dikategorikan</option>
									<?php
										$query=@mysql_query("SELECT * FROM categories");
										while($row2=@mysql_fetch_array($query)){
											if($row2['CatID']!=0){
												if($row2['CatID']==$row['CatID']){ $select="selected"; } else { $select=""; }
												echo "<option value=\"".$row2['CatID']."\" ".$select.">".$row2['Categories']."</option>";
											}
										}
									?>
								</select>
                            </div>
                        </div>
						
						<div class="control-group">
                            <label class="control-label" for="content">Isi Halaman</label>
                            <div class="controls">
                              <textarea name="contentpost" rows="15" value="" id="contentpost"><?=$row['ContentPost']; ?></textarea>
                            </div>
                        </div>
						
						<p><input type="submit" value="Simpan" class="btn btn-primary span5"  name="submitcontent" style="width: 100px; margin-left: 20px;"/></p>
                    </fieldset>
                </form>
			</div>
		</div>
	</section>
</div>
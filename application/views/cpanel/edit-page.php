<script type="text/javascript" src="<?=base_url();?>asset/admin/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
	tinymce.init({
		selector: "textarea",
		theme: "modern",
		plugins: [
			"advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
			"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
			"table contextmenu directionality emoticons template textcolor paste textcolor filemanager"
			],
		toolbar1: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | formatselect fontselect fontsizeselect | image",
		menubar: false
	 });
</script>

<div class="row-fluid">
	<div class="span12">
		<ul class="breadcrumb">
			<li><a href="<?=base_url(); ?>cpanel/pages/all-pages" style="cursor: pointer; ">Halaman</a> <span class="divider">/</span></li>
			<li><a href="<?=base_url(); ?>cpanel/pages/edit-page/?post=<?=$_GET['post'];?>">Ubah Halaman</a> <span class="divider">/</span></li>
		</ul>
	</div>
</div>

<?php
	$error=$this->session->userdata('warning');
	$warning=str_replace('<p>','<li>',$error);
	$warning=str_replace('</p>','</li>',$warning);
	if(isset($error) && $error){
		echo "
			<div class=\"alert alert-block\" >
				<h4 class=\"alert-heading\">Peringatan!</h4>
				<ul>".$warning."</ul>
			</div>";
	}
	if($this->session->userdata('warning')!=""){
		$this->session->set_userdata('warning','');
	}
	$row=@mysql_fetch_array(@mysql_query("SELECT * FROM pages WHERE PageID='".$_GET['post']."'"));	
?>

<div class="row-fluid">
	<section class="utopia-widget utopia-form-box section">
		<div class="utopia-widget-title">
			<img src="<?=base_url(); ?>public/images/admin/monitor.png" class="utopia-widget-icon">
			<span>Ubah Halaman</span>
		</div>

		<div class="row-fluid">
			<div id="showform" style="padding: 10px 10px 10px 10px;">
				<form class="form-horizontal" action="" method="post" name="edit-page" id="edit-page">
                    <fieldset>
                        <div class="control-group">
                            <label class="control-label" for="Judul Halaman">Judul Halaman</label>
							<input type="hidden" name="postid" id="postid" value="<?=$_GET['post']; ?>" />
                            <div class="controls">
                                <input type="text" name="titlepage" id="titlepage" style="width: 500px" value="<?=$row['TitlePage']; ?>"/><br />
                            </div>
                        </div>
						
						 <div class="control-group">
                            <label class="control-label" for="content">Isi Halaman</label>
                            <div class="controls">
                              <textarea name="contentpage" rows="15" value="" id="contentpage"><?=$row['ContentPage']; ?></textarea>
                            </div>
                        </div>
						
						<p><input type="submit" value="Simpan" class="btn btn-primary span5"  name="submitpages" style="width: 100px; margin-left: 20px;"/></p>
                    </fieldset>
                </form>
			</div>
		</div>
	</section>
</div>
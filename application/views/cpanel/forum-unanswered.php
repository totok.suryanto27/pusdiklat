<?php
	function convert_url($str){
		$url=str_replace(' ','-',$str);
		$url=str_replace('#','-',$url);
		$url=str_replace('%','-',$url);
		$url=str_replace('&','-',$url);
		$url=str_replace('*','-',$url);
		$url=str_replace('{','-',$url);
		$url=str_replace('}','-',$url);
		$url=str_replace('\\','-',$url);
		$url=str_replace(':','-',$url);
		$url=str_replace('<','-',$url);
		$url=str_replace('>','-',$url);
		$url=str_replace('?','-',$url);
		$url=str_replace('/','-',$url);
		$url=str_replace('+','-',$url);
		$url=str_replace(',','',$url);
		$url=str_replace('&rsquo;','-',$url);
		$url=str_replace('&rdquo;','-',$url);
		$url=strtolower($url);
		return $url;
	}

?>

<!--[if IE]>
	<style>
		.admin-content-content  input[type="text"] { 
			padding: 10px 5px 5px 5px;
		}
	</style>
	<![endif]-->
	<!--[if !IE]> -->
	<style>
		.admin-content-content  input[type="text"] { 
			padding: 5px 5px 5px 5px;
		}
	</style>
<!-- <![endif]-->
		
<style>
	.statement { min-height: 50px; background-color: rgb(247, 247, 247); overflow: hidden; border: 1px solid rgb(204, 204, 204); margin-bottom: 10px; padding: 10px 10px 10px 10px; color: #000; }
	.statement-title { font-weight: bold; }
			
	.tb11 { background:#FFFFFF url(http://pusdiklat.bkpm.go.id/public/images/search.png) no-repeat 4px 10px; padding:4px 20px 4px 22px; border:1px solid #CCCCCC; width:430px; height:38px; text-align: center; }
			
	.cat { color: #3E6D8E; background-color: #E0EAF1; border-bottom: 1px solid #B3CEE1; border-right: 1px solid #B3CEE1; padding: 3px 4px; margin: 2px 2px 2px 0px; text-decoration: none; font-size: 100%; line-height: 1.4; white-space: nowrap; display: inline-block; }

	.cat:hover { background-color: #C4DAE9; border-bottom: 1px solid #C4DAE9; border-right: 1px solid #C4DAE9; text-decoration: none; cursor: pointer; }

	.cat a:hover { text-decoration: none; cursor: pointer; }
</style>


<div class="row-fluid">
	<div class="span12">
		<ul class="breadcrumb">
			<li><a style="cursor: pointer; ">Pertanyaan</a> <span class="divider">/</span></li>
		</ul>
	</div>
</div>

<div class="row-fluid">
	<section class="utopia-widget utopia-form-box section">
		<div class="utopia-widget-title">
			<img src="<?=base_url(); ?>public/images/admin/monitor.png" class="utopia-widget-icon">
			<span>List Pertanyaan yang Belum Terjawab</span>
		</div>

		<div class="row-fluid">
			<div align="center" style="margin-top: 20px; padding-top: 5px;">
				<input class="tb11" type="text" name="searchtext" id="searchtext" placeholder="Gunakan untuk memfilter pertanyaan" onkeyup="showquestforum('1',this.value)" />
			</div>
			
			<div id="showdata" style="padding: 20px 20px 20px 20px;">
				<?php
					$this->db->select('*');
					$this->db->from('question');
					$this->db->where('answer','');
					$result=$this->db->get();
					$ndata=$result->num_rows();
					$itemperpage=20;
					$totalpage=ceil($ndata/$itemperpage);
								
					if($ndata!=0){
						if($totalpage>1){
							# Halaman #
							echo "<div class=\"pagination\">";
							echo "<span class=\"pg-selected\" >Prev</span>";
							$i=1;
							$status="";
							$status_="";
							$status__="";
							$batas=5;
							while($i<=$totalpage){
								if($i!=1){
									if($totalpage>=10){
										if($i<=$batas || $i>$totalpage-2){
											echo "<span class=\"pg-normal\" onclick=\"showquestforum('".$i."',document.getElementById('searchtext').value)\">".$i."</span>";
										} else {
											if($status==""){
												$status="ada";
												echo " ... ";
											}
										}
									} else {
										echo "<span class=\"pg-normal\" onclick=\"showquestforum('".$i."',document.getElementById('searchtext').value)\">".$i."</span>";
									}
								} else {
									echo "<span class=\"pg-selected\" >".$i."</span>";
								}
											
								$i++;
							}
										
										
							if($totalpage!=1){
								echo "<span class=\"pg-normal\" onclick=\"showquestforum('2',document.getElementById('searchtext').value)\">Next</span>";
							} else {
								echo "<span class=\"pg-selected\" >Next</span>";
							}
							echo "</div>";
							echo "<div style=\"padding-top: 20px;\"></div>";
						}
						# Halaman #
									
						# Tampilan Data
						$this->db->select('*');
						$this->db->from('question');
						$this->db->where('answer','');
						$this->db->order_by('QuestID','DESC');
						$this->db->limit($itemperpage,0);
						$result=$this->db->get();
						$result=$result->result();
						$i=1;
						foreach($result as $row){
							echo "
								<div class=\"statement\">
									<div class=\"statement-title\">".$row->title."</div>
									<div><em>".$row->Nama." (".$row->Instansi.") pada ".$row->RecordDate."</em></div>
									<div style=\"margin-top: 10px;\">".$row->Question."</div>
									<div style=\"margin-top: 15px;\"><a class=\"cat\" title=\"Balas Pertanyaan\" onclick=\"answerquest('".$i."','".$row->QuestID."')\">Balas Pertanyaan</a></div>
									<div id=\"answerquest".$i."\"></div>
								</div>";
							$i++;
						}
						echo "<input type=\"hidden\" id=\"nofquest\" value=\"".($i-1)."\" />";
						# Tampilan Data
									
								
						# Halaman #
						if($totalpage>1){
							echo "<div class=\"pagination\" style=\"padding-top: 20px;\">";
							echo "<span class=\"pg-selected\" >Prev</span>";
							$i=1;
							$status="";
							$status_="";
							$status__="";
							while($i<=$totalpage){
								if($i!=1){
									if($totalpage>=10){
										if($i<=$batas || $i>$totalpage-2){
											echo "<span class=\"pg-normal\" onclick=\"showquestforum('".$i."',document.getElementById('searchtext').value)\">".$i."</span>";
										} else {
											if($status==""){
												$status="ada";
												echo " ... ";
											}
										}
									} else {
										echo "<span class=\"pg-normal\" onclick=\"showquestforum('".$i."',document.getElementById('searchtext').value)\">".$i."</span>";
									}
								} else {
									echo "<span class=\"pg-selected\" >".$i."</span>";
								}
								
								$i++;
							}
										
										
							if($totalpage!=1){
								echo "<span class=\"pg-normal\" onclick=\"showquestforum('2',document.getElementById('searchtext').value)\">Next</span>";
							} else {
								echo "<span class=\"pg-selected\" >Next</span>";
							}
							echo "</div>";
						}
						# Halaman #
					} else {
						echo "<div align=\"center\">Data yang Anda cari tidak ada</div>";
					}
				?>	
			</div>
		</div>
	</section>
</div>
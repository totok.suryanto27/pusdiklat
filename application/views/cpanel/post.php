<div id="admin-content">
	<div class="admin-content-content2">
		<?php
		if($this->input->post('submitcontent')!=""){
			if($this->input->post('titlepost')!="" && $this->input->post('contentpost')!="" && $this->input->post('submitcontent')!=""){
				// Simpan data ke database posts
				// Note untuk user masih belum disetting
				$titlepost=str_replace("'","&rsquo;",$this->input->post('titlepost'));
				$titlepost=str_replace('"',"&rdquo;",$titlepost);
				
				// Replace ' dan " dengan &rsquo; dan &quot
				$url=str_replace(' ','-',$this->input->post('titlepost'));
				$url=str_replace('#','-',$url);
				$url=str_replace('%','-',$url);
				$url=str_replace('&','-',$url);
				$url=str_replace('*','-',$url);
				$url=str_replace('{','-',$url);
				$url=str_replace('}','-',$url);
				$url=str_replace('\\','-',$url);
				$url=str_replace(':','-',$url);
				$url=str_replace('<','-',$url);
				$url=str_replace('>','-',$url);
				$url=str_replace('?','-',$url);
				$url=str_replace('/','-',$url);
				$url=str_replace('+','-',$url);
				$url=str_replace(',','',$url);
				$url=strtolower($url);
				$data=$this->session->userdata('isLoggedIn');
				//$contentpost=str_replace('../asset',base_url()."asset",$this->input->post('contentpost'));
				$contentpost=$this->input->post('contentpost');
				if($this->input->post('idpost')=="new-post"){
					// Tambah 1 untuk Kategori
					$this->db->select('Posts');
					$this->db->from('categories');
					$this->db->where('CatID',$this->input->post('categories'));
					$result=$this->db->get();
					$result=$result->result();
					foreach($result as $row){
						$posts=$row->Posts;
					}
					$posts=$posts+1;
					$datasql=array('Posts'=>$posts);
					$this->db->where('CatID',$this->input->post('categories'));
					$this->db->update('categories',$datasql);	
						
					/* // Cek banyak konten yang ada
					$this->db->select('*');
					$this->db->from('posts');
					$result=$this->db->get();
					$ndata=$result->num_rows();
					$id=$ndata+1; */
					
					
					
					// Masukkan data ke database posts
					$datasql=array('TitlePost'=>$titlepost, 'ContentPost'=>$contentpost, 'Author'=>$data['nama'], 'CatID'=>$this->input->post('categories'), 'RecordDate'=>date("Y-m-d"), 'Url'=>$url);
					$this->db->set($datasql);
					$this->db->insert('posts');
						
					// Dapatkan ID Post terakhir yang dimasukkan
					$this->db->select('*');
					$this->db->from('posts');
					$this->db->where('TitlePost',$titlepost);
					$result=$this->db->get();
					$result=$result->result();
					foreach($result as $row){
						$contentid=$row->PostID;
					}
						
					// Masukkan URL ke database URL
					$datasql=array('UrlType'=>'site', 'Url_1'=>'read', 'Url_2'=>$url, 'TableSrc'=>'posts', 'ContentID'=>$contentid, 'webtitle'=>$this->input->post('titlepost'), 'filename'=>'read');
					$this->db->set($datasql);
					$this->db->insert('url');
				}
				
				if($this->input->post('idpost')=="edit-post"){
					$alert="";
					$this->db->select('PostID');
					$this->db->from('posts');
					$this->db->where('Url',$url);
					$result=$this->db->get();
					$result_data=$result->result();
					foreach($result_data as $row){
						if($row->PostID!=$this->input->post('postid')){
							$alert="exists";
						}
					}
					
					if($alert!="exists"){
						$datasql=array('TitlePost'=>$titlepost, 'ContentPost'=>$contentpost, 'Author'=>$data['nama'], 'CatID'=>$this->input->post('categories'), 'RecordDate'=>date("Y-m-d"), 'Url'=>$url);
						$this->db->where('PostID',$this->input->post('postid'));
						$this->db->update('posts',$datasql);
						
						$datasql=array('Url_2'=>$url, 'webtitle'=>$titlepost);
						$this->db->where('UrlID',$this->input->post('urlid'));
						$this->db->update('url',$datasql);
					}
				}
		?>
				<div id="success" align="center">
					<meta http-equiv="refresh" content="0;url=<?php echo base_url(); ?>cpanel/content/all-posts" />
				</div>
		<?php
			}
		}
		?>
				
		<?php
		if($this->input->post('submitcategory')!=""){
			if($this->input->post('titlecategory')!="" && $this->input->post('submitcategory')!=""){
				// Simpan data ke database posts
				// Note untuk user masih belum disetting
				$titlecategory=str_replace("'","&rsquo;",$this->input->post('titlecategory'));
				$titlecategory=str_replace('"',"&rdquo;",$titlecategory);
				
				$slugs=str_replace(' ','-',$this->input->post('titlecategory'));
				$slugs=str_replace('#','-',$slugs);
				$slugs=str_replace('%','-',$slugs);
				$slugs=str_replace('&','-',$slugs);
				$slugs=str_replace('*','-',$slugs);
				$slugs=str_replace('{','-',$slugs);
				$slugs=str_replace('}','-',$slugs);
				$slugs=str_replace('\\','-',$slugs);
				$slugs=str_replace(':','-',$slugs);
				$slugs=str_replace('<','-',$slugs);
				$slugs=str_replace('>','-',$slugs);
				$slugs=str_replace('?','-',$slugs);
				$slugs=str_replace('/','-',$slugs);
				$slugs=str_replace('+','-',$slugs);
				$slugs=str_replace(',','',$slugs);
				$slugs=strtolower($slugs);
				
				if($this->input->post('idpost')=="new-category"){
					/* // Cek banyak kategori yang ada
					$this->db->select('*');
					$this->db->from('categories');
					$result=$this->db->get();
					$ndata=$result->num_rows();
					$id=$ndata; */
						
					$datasql=array('Categories'=>$titlecategory, 'CatDesc'=>$this->input->post('categorydesc'), 'Slugs'=>$slugs);
					$this->db->set($datasql);
					$this->db->insert('categories');
				}
				
				if($this->input->post('idpost')=="edit-category"){
					$alert="";
					$this->db->select('CatID');
					$this->db->from('categories');
					$this->db->where('Slugs',$slugs);
					$result=$this->db->get();
					$result_data=$result->result();
					foreach($result_data as $row){
						if($row->CatID!=$this->input->post('postid')){
							$alert="exists";
						}
					}
					
					if($alert!="exists"){
						$datasql=array('Categories'=>$titlecategory, 'CatDesc'=>$this->input->post('categorydesc'), 'Slugs'=>$slugs);
						$this->db->where('CatID',$this->input->post('postid'));
						$this->db->update('categories',$datasql);
					}
				} 
		?>
				<div id="success" align="center">
					<meta http-equiv="refresh" content="0;url=<?php echo base_url(); ?>cpanel/content/all-categories" />
				</div>
		<?php
			}
		}
		?>
		
		<?php
		if($this->input->post('submitext')!=""){
			if($this->input->post('titleext')!="" && $this->input->post('submitext')!=""){
				$titleext=str_replace("'","&rsquo;",$this->input->post('titleext'));
				$titleext=str_replace('"',"&rdquo;",$titleext);
				
				if($this->input->post('idpost')=="newext-link"){
					/* // Cek banyak tautan yang ada
					$this->db->select('*');
					$this->db->from('extlink');
					$result=$this->db->get();
					$ndata=$result->num_rows();
					$id=$ndata+1; */
						
					$datasql=array('ExtLink'=>$titleext, 'ExtDesc'=>$this->input->post('extdesc'));
					$this->db->set($datasql);
					$this->db->insert('extlink');
				}
				
				if($this->input->post('idpost')=="edit-ext"){
					$alert="";
					$this->db->select('ExtID');
					$this->db->from('extlink');
					$this->db->where('ExtLink',$titleext);
					$result=$this->db->get();
					$result_data=$result->result();
					foreach($result_data as $row){
						if($row->ExtID!=$this->input->post('postid')){
							$alert="exists";
						}
					}
					
					if($alert!="exists"){
						$datasql=array('ExtLink'=>$titleext, 'ExtDesc'=>$this->input->post('extdesc'));
						$this->db->where('ExtID',$this->input->post('postid'));
						$this->db->update('extlink',$datasql);
					}
				}
		?>
				<div id="success" align="center">
					<meta http-equiv="refresh" content="0;url=<?php echo base_url(); ?>cpanel/content/ext-link" />
				</div>
		<?php
			}
		}
		?>
	
		<?php
		if($this->input->post('submitalbum')!=""){
			$url=str_replace(' ','-',$this->input->post('titlealbum'));
			$url=str_replace('#','-',$url);
			$url=str_replace('%','-',$url);
			$url=str_replace('&','-',$url);
			$url=str_replace('*','-',$url);
			$url=str_replace('{','-',$url);
			$url=str_replace('}','-',$url);
			$url=str_replace('\\','-',$url);
			$url=str_replace(':','-',$url);
			$url=str_replace('<','-',$url);
			$url=str_replace('>','-',$url);
			$url=str_replace('?','-',$url);
			$url=str_replace('/','-',$url);
			$url=str_replace('+','-',$url);
			$url=str_replace(',','',$url);
			$url=strtolower($url);
			
			if($this->input->post('titlealbum')!="" && $this->input->post('submitalbum')!=""){
				$titlealbum=str_replace("'","&rsquo;",$this->input->post('titlealbum'));
				$titlealbum=str_replace('"',"&rdquo;",$titlealbum);
				
				if($this->input->post('idpost')=="new-albumphoto"){
					$datasql=array('TitleAlbum'=>$titlealbum, 'AlbumDesc'=>$this->input->post('albumdesc'), 'Url'=>$url);
					$this->db->set($datasql);
					$this->db->insert('albumphoto');
					
					$this->db->select('*');
					$this->db->from('albumphoto');
					$this->db->where('TitleAlbum',$titlealbum);
					$result=$this->db->get();
					$result_data=$result->result();
					foreach($result_data as $row){
						$albphid=$row->AlbPhID;
					}
					
					// Masuk ke URL
					$datasql=array('UrlType'=>'site', 'Url_1'=>'galeri-foto', 'Url_2'=>$url, 'filename'=>'galleryshow', 'TableSrc'=>'photos', 'ContentID'=>$albphid);
					$this->db->set($datasql);
					$this->db->insert('url');
				}
				
				if($this->input->post('idpost')=="edit-albph"){
					$alert="";
					$this->db->select('*');
					$this->db->from('albumphoto');
					$this->db->where('TitleAlbum',$this->input->post('oldname'));
					$result=$this->db->get();
					$result_data=$result->result();
					foreach($result_data as $row){
						if($row->AlbPhID!=$this->input->post('postid')){
							$alert="exists";
						}
					}
					
					if($alert!="exists"){
						$datasql=array('TitleAlbum'=>$titlealbum, 'AlbumDesc'=>$this->input->post('albumdesc'), 'Url'=>$url);
						$this->db->where('AlbPhID',$this->input->post('postid'));
						$this->db->update('albumphoto',$datasql);
						
						$url_=str_replace(' ','-',$this->input->post('oldname'));
						$url_=str_replace('#','-',$url_);
						$url_=str_replace('%','-',$url_);
						$url_=str_replace('&','-',$url_);
						$url_=str_replace('*','-',$url_);
						$url_=str_replace('{','-',$url_);
						$url_=str_replace('}','-',$url_);
						$url_=str_replace('\\','-',$url_);
						$url_=str_replace(':','-',$url_);
						$url_=str_replace('<','-',$url_);
						$url_=str_replace('>','-',$url_);
						$url_=str_replace('?','-',$url_);
						$url_=str_replace('/','-',$url_);
						$url_=str_replace('+','-',$url_);
						$url_=str_replace(',','',$url_);
						$url_=strtolower($url_);
						
						
						$datasql=array('Url_2'=>$url);
						$this->db->where('UrlType','site');
						$this->db->where('Url_1','galeri-foto');
						$this->db->where('Url_2',$url_);
						$this->db->update('url',$datasql);
					}
				}
		?>
				<div id="success" align="center">
					<meta http-equiv="refresh" content="0;url=<?php echo base_url(); ?>cpanel/media/gallery-photo" />
				</div>
		<?php
			}
		}
		?>
	
		<?php
		if($this->input->post('submitpages')!=""){
			if($this->input->post('titlepage')!="" && $this->input->post('contentpage')!="" && $this->input->post('submitpages')!=""){
				// Simpan data ke database posts
				// Note untuk user masih belum disetting
				$titlepage=str_replace("'","&rsquo;",$this->input->post('titlepage'));
				$titlepage=str_replace('"',"&rdquo;",$titlepage);
				
				// Replace ' dan " dengan &rsquo; dan &quot
				$url=str_replace(' ','-',$this->input->post('titlepage'));
				$url=str_replace('#','-',$url);
				$url=str_replace('%','-',$url);
				$url=str_replace('&','-',$url);
				$url=str_replace('*','-',$url);
				$url=str_replace('{','-',$url);
				$url=str_replace('}','-',$url);
				$url=str_replace('\\','-',$url);
				$url=str_replace(':','-',$url);
				$url=str_replace('<','-',$url);
				$url=str_replace('>','-',$url);
				$url=str_replace('?','-',$url);
				$url=str_replace('/','-',$url);
				$url=str_replace('+','-',$url);
				$url=str_replace(',','',$url);
				$url=strtolower($url);
				$data=$this->session->userdata('isLoggedIn');
				
				if($this->input->post('idpost')=="new-page"){
					// Masukkan data ke database pages
					$datasql=array('TitlePage'=>$titlepage, 'ContentPage'=>$this->input->post('contentpage'), 'Author'=>$data['nama'], 'RecordDate'=>date("Y-m-d"), 'Url'=>$url);
					$this->db->set($datasql);
					$this->db->insert('pages');
				}
				
				if($this->input->post('idpost')=="edit-page"){
					$alert="";
					$this->db->select('PageID');
					$this->db->from('pages');
					$this->db->where('Url',$url);
					$result=$this->db->get();
					$result_data=$result->result();
					foreach($result_data as $row){
						if($row->PageID!=$this->input->post('postid')){
							$alert="exists";
						}
					}
					
					if($alert!="exists"){
						$datasql=array('TitlePage'=>$titlepage, 'ContentPage'=>$this->input->post('contentpage'), 'Author'=>$data['nama'], 'RecordDate'=>date("Y-m-d"), 'Url'=>$url);
						$this->db->where('PageID',$this->input->post('postid'));
						$this->db->update('pages',$datasql);
					}
				}
		?>
				<div id="success" align="center">
					<meta http-equiv="refresh" content="0;url=<?php echo base_url(); ?>cpanel/pages/all-pages" />
				</div>
		<?php
			}
		}
		?>
	</div>
</div>
<div id="admin-content">
	<div class="admin-content-title"><h1>Tambah Menu Level 2</h1></div>
	<div class="admin-content-content">
		<!--[if IE]>
			<style>
				.admin-content-content  input[type="text"] { 
					padding: 10px 5px 5px 5px;
				}
				.admin-content-content  select { 
					padding: 10px 5px 5px 5px;
				}
			</style>
		<![endif]-->
		<!--[if !IE]> -->
			<style>
				.admin-content-content  input[type="text"] { 
					padding: 5px 5px 5px 5px;
				}
				.admin-content-content  select { 
					padding: 5px 5px 5px 5px;
				}
			</style>
		<!-- <![endif]-->
		<?php
			$error=$this->session->userdata('warning');
			$warning=str_replace('<p>','<li>',$error);
			$warning=str_replace('</p>','</li>',$warning);
			if(isset($error) && $error){
				echo "
					<div id=\"tips\" >
						<ul>".$warning."</ul>
					</div>";
			}
			if($this->session->userdata('warning')!=""){
				$this->session->set_userdata('warning','');
			}
			
			if($this->session->userdata('isMenulvl2')){			
				$data=$this->session->userdata('isMenulvl2');
				$titlemenulvl2=$data['titlemenulvl2'];
				$menulvl1=$data['menulvl1'];
				$webtitle=$data['webtitle'];
			} else {
				$titlemenulvl2="";
				$menulvl1="";
				$webtitle="";
			}
		?>
		<p>Menu Level 2 adalah merupakan perluasan dari Menu Level 1</p>
		
		<img src="<?php echo base_url();?>asset/admin/images/menu-level2.jpg" width="920px"/>
		<form action="" method="post" name="post" id="post" autocomplete="off">
			<h4 style="margin-top: 10px;">Nama Menu Level 2</h4>
			<input type="text" name="titlemenulvl2" id="titlemenulvl2" size="109%" value="<?php echo $titlemenulvl2; ?>" />
			<div style="clear: both"></div>
			<h4 style="margin-top: 10px;">Menu Level 1</h4>
			<select name="menulvl1" id="menulvl1">
				<option value="">Pilih Satu</option>
				<?php
					$query=@mysql_query("SELECT * FROM menulvl1 WHERE HaveSub='Ada'");
					while($row=@mysql_fetch_array($query)){
						if($menulvl1==$row['Lvl1ID']){ $selected="selected"; } else { $selected=""; }
						echo "<option value=\"".$row['Lvl1ID']."\" ".$selected.">".$row['MenuName']."</option>";
					}
				?>
			</select>
			<div id="link"><a onclick="document.location='<?php echo base_url(); ?>cpanel/appereance/menu-level1'">+ Tambah Menu Level 1</a></div>
			<div style="clear: both"></div>
			<h4 style="margin-top: 10px;">Judul (Cth: Sejarah Pusdiklat BKPM)</h4>
			<input type="text" name="webtitle" id="webtitle" size="109%" value="<?php echo $webtitle; ?>"/>
			<div style="clear: both"></div>
			<input type="checkbox" name="haveSub" id="haveSub" value="Ada" checked onclick="disabledlink('level1')" /> Memiliki Sub Menu (Level 3)
			<div style="clear: both"></div>
			<fieldset>
				<div class="optradio"><input type="radio" name="opt" id="opt" value="pages" disabled onclick="activateinput(this.value)"></div>
				<div class="opttitle">Halaman</div>
				<div class="optinput">
					<select name="optinpages" id="optinpages" disabled style="width: 350px;">
						<option value="">Pilih Satu</option>
						<?php
							$query=@mysql_query("SELECT * FROM pages WHERE 1");
							while($row=@mysql_fetch_array($query)){
								echo "<option value=\"".$row['PageID']."\">".$row['TitlePage']."</option>";
							}
						?>
					</select>
				</div>
				<div style="clear: both"></div>
				<div class="optradio"><input type="radio" name="opt" id="opt" value="category" disabled onclick="activateinput(this.value)"></div>
				<div class="opttitle">Kategori</div>
				<div class="optinput">
					<select name="optincat" id="optincat" disabled style="width: 350px;">
						<option value="">Pilih Satu</option>
						<?php
							$query=@mysql_query("SELECT * FROM categories WHERE 1");
							while($row=@mysql_fetch_array($query)){
								echo "<option value=\"".$row['CatID']."\">".$row['Categories']."</option>";
							}
						?>
					</select>
				</div>
				<div style="clear: both"></div>
				<div class="optradio"><input type="radio" name="opt" id="opt" value="registration" disabled onclick="activateinput(this.value)"></div>
				<div class="opttitle">Pendaftaran Online</div>
				<div class="optinput">
					<select name="optinreg" id="optinreg" disabled style="width: 350px;">
						<option value="">Pilih Satu</option>
						<?php
							$query=@mysql_query("SELECT * FROM activity WHERE 1");
							while($row=@mysql_fetch_array($query)){
								echo "<option value=\"".$row['id']."\">".$row['activity']."</option>";
							}
						?>
					</select>
				</div>
				<div style="clear: both"></div>
				<div class="optradio"><input type="radio" name="opt" id="opt" value="other" disabled onclick="activateinput(this.value)"></div>
				<div class="opttitle">Link Luar [ http:// ]</div>
				<div class="optinput">
					<input type="text" name="otherlink" id="otherlink" size="40%" value="" disabled/>
				</div>
				<div style="clear: both"></div>
				<div class="optradio" style="height: 25px;"><input type="radio" name="opt" id="opt" value="photo" disabled onclick="activateinput(this.value)"></div>
				<div class="opttitle">Gallery Photo</div>
				<div class="optinput">
					&nbsp;
				</div>
				<div style="clear: both"></div>
				<div class="optradio" style="height: 25px;"><input type="radio" name="opt" id="opt" value="video" disabled onclick="activateinput(this.value)"></div>
				<div class="opttitle">Gallery Video</div>
				<div class="optinput">
					&nbsp;
				</div>
				<div style="clear: both"></div>
				<div class="optradio" style="height: 25px;"><input type="radio" name="opt" id="opt" value="consultation" disabled onclick="activateinput(this.value)"></div>
				<div class="opttitle">Konsultasi</div>
				<div class="optinput">
					&nbsp;
				</div>
				<div style="clear: both"></div>
			</fieldset>
			<div style="clear: both"></div>
			<input type="submit" name="submitlevel2" id="submitlevel2" value="Simpan" />
		</form>
	</div>
</div>

<div class="row-fluid">
	<div class="span12">
		<ul class="breadcrumb">
			<li><a style="cursor: pointer; ">Media</a> <span class="divider">/</span></li>
			<li><a href="<?=base_url(); ?>cpanel/media/gallery-video">Album Video</a> <span class="divider">/</span></li>
			<li><a href="<?=base_url(); ?>cpanel/media/new-albumvideo">Tambah Album Video</a> <span class="divider">/</span></li>
		</ul>
	</div>
</div>

<?php
	$error=$this->session->userdata('warning');
	$warning=str_replace('<p>','<li>',$error);
	$warning=str_replace('</p>','</li>',$warning);
	if(isset($error) && $error){
		echo "
			<div class=\"alert alert-block\" >
				<h4 class=\"alert-heading\">Peringatan!</h4>
				<ul>".$warning."</ul>
			</div>";
	}
	if($this->session->userdata('warning')!=""){
		$this->session->set_userdata('warning','');
	}
		
	if($this->session->userdata('isAlbumVideo')){			
		$data=$this->session->userdata('isAlbumVideo');
		$title=$data['titlealbum'];
		$desc=$data['albumdesc'];
	} else {
		$title="";
		$desc="";
	}
?>

<div class="row-fluid">
	<section class="utopia-widget utopia-form-box section">
		<div class="utopia-widget-title">
			<img src="<?=base_url(); ?>public/images/admin/play.png" class="utopia-widget-icon">
			<span>Tambah Album Video</span>
		</div>

		<div class="row-fluid">
			<div id="showform" style="padding: 10px 10px 10px 10px;">
				<form class="form-horizontal" action="" method="post" name="post" id="post"">
                    <fieldset>
                        <div class="control-group">
                            <label class="control-label" for="Nama Album">Nama Album</label>
                            <div class="controls">
                                <input name="titlealbum" id="titlealbum" style="width: 400px;" type="text" value="<?=$title; ?>"><br />
                            </div>
                        </div>
						
						 <div class="control-group">
                            <label class="control-label" for="Deskripsi">Deskripsi Album</label>
                            <div class="controls">
                                <textarea name="albumdesc" rows="10" cols="70" value="" id="albumdesc"><?=$desc; ?></textarea>
                            </div>
                        </div>
						
						<p><input type="submit" value="Simpan" class="btn btn-primary span5"  name="submitalbumvideo" style="width: 100px; margin-left: 20px;"/></p>
                    </fieldset>
                </form>
			</div>
		</div>
	</section>
</div>
<div class="row-fluid">
	<div class="span12">
		<ul class="breadcrumb">
			<li><a style="cursor: pointer; ">Kegiatan Pusdiklat</a> <span class="divider">/</span></li>
			<li><a href="<?=base_url()?>cpanel/bkpm/candidate" style="cursor: pointer; ">Daftar Calon Peserta Diklat Pusdiklat BKPM</a> <span class="divider">/</span></li>
		</ul>
	</div>
</div>
<div class="row-fluid">
	<section class="utopia-widget utopia-form-box section">
		<div class="utopia-widget-title">
			<img src="<?=base_url(); ?>public/images/admin/monitor.png" class="utopia-widget-icon">
			<span>Daftar Calon Peserta Diklat Pusdiklat BKPM</span>
		</div>

		<div class="row-fluid">
			<div id="showform" style="padding: 10px 10px 10px 10px;">
				<form class="form-horizontal" action="" method="post" name="edit-ext" id="edit-ext">
					<fieldset>
						<div class="control-group">
							<label class="control-label" for="Diklat yang diikuti">Diklat yang diikuti</label>
								<div class="controls">
									<select name="filter-diklat" id="filter-diklat" style="width:500px;">
										<option value="">Tampilkan Semua</option>
										<?php
											$this->db->select('*');
											$this->db->from('jadwal');
											$this->db->group_by("DikID");
											$result=$this->db->get();
											$result=$result->result();
											foreach($result as $row){
												$this->db->select('*');
												$this->db->from('activity');
												$this->db->where('id',$row->DikID);
												$result=$this->db->get();
												$result=$result->result();
												foreach($result as $row2){
													$actname=$row2->activity;
												}
												echo "<option value=\"".$row->DikID."\">".$actname."</option>";
											}
										?>
									</select><br />
								</div>
						</div>
						
						<div class="control-group">
							<label class="control-label" for="Angkatan">Angkatan</label>
								<div class="controls">
									<select name="filter-angk" id="filter-angk" style="width:180px;">
										<option value="">Tampilkan Semua</option>
										<?php
											$this->db->select('*');
											$this->db->from('jadwal');
											$this->db->group_by("ClassDik");
											$result=$this->db->get();
											$result=$result->result();
											foreach($result as $row){
												echo "<option value=\"".$row->ClassDik."\">".$row->ClassDik."</option>";
											}
										?>
									</select>
								</div>
						</div>
						
						<div class="control-group">
							<label class="control-label" for="Nama">Nama</label>
								<div class="controls">
									<input type="text" name="searchtext" id="searchtext" style="width: 600px;"/>
								</div>
						</div>
						
						<div class="control-group">
							<label class="control-label" for="Nama">
								<select style="width: 150px;" id="searchcity" name="searchcity" onclick="ajaxfunc('start&type=showcity&id='+this.value, 'city', '<?=base_url(); ?>public/ajax/ajax.php')">
									<option value="0">Tampilkan Semua</option>
									<option value="1">Kota</option>
									<option value="2">Kabupaten</option>
									<option value="3">Provinsi</option>
								</select>
							
							</label>
								<div class="controls">
									<div id="city">
										<input type="hidden" id="kota" value="" />
									</div>
								</div>
						</div>
						
						<div class="control-group">
							<label class="control-label" for="Nama"><input type="button" name="showdatacandidate" id="showdatacandidate" value="Proses" class="btn btn-primary span5"  style="width: 200px; float: left;" onclick="showdatacandidate2('1',document.getElementById('searchtext').value)" /></label>
						</div>
						
						
					</fieldset>
				</form>
			</div>
			<div style="clear:both"></div>
			<div style="margin: 20px 20px 20px 20px;">
				<div id="showdata"></div>
			</div>
		</div>
	</section>
</div>
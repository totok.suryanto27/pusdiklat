<div id="admin-content">
	<?php
	$this->db->select('*');
	$this->db->from('indextab');
	$this->db->where('TabID',$_GET['post']);
	$result=$this->db->get();
	$result=$result->result();
	foreach($result as $row){
		$tabname=$row->TabName;
		$catid=$row->CatID;
		$tabid=$row->TabID;
	}		
	?>
	<div class="admin-content-title"><h1>Ubah Menu Level 2 [ <span style="color: lime;"><?php echo $tabname; ?></span> ]</h1></div>
	<div class="admin-content-content">
		<!--[if IE]>
			<style>
				.admin-content-content  input[type="text"] { 
					padding: 10px 5px 5px 5px;
				}
				.admin-content-content  select { 
					padding: 10px 5px 5px 5px;
				}
			</style>
		<![endif]-->
		<!--[if !IE]> -->
			<style>
				.admin-content-content  input[type="text"] { 
					padding: 5px 5px 5px 5px;
				}
				.admin-content-content  select { 
					padding: 5px 5px 5px 5px;
				}
			</style>
		<!-- <![endif]-->
		<?php
			$error=$this->session->userdata('warning');
			$warning=str_replace('<p>','<li>',$error);
			$warning=str_replace('</p>','</li>',$warning);
			if(isset($error) && $error){
				echo "
					<div id=\"tips\" >
						<ul>".$warning."</ul>
					</div>";
			}
			if($this->session->userdata('warning')!=""){
				$this->session->set_userdata('warning','');
			}
		?>
		<form action="" method="post" name="post" id="post" autocomplete="off">
			<h4>Judul Index Tab</h4>
			<input type="hidden" name="idpost" value="<?php echo $tabid; ?>" />
			<input type="text" name="titletab" id="titletab" size="109%" value="<?php echo $tabname; ?>"/>
			<div style="clear: both"></div>
			<h4>Kategori yang akan ditampilkan</h4>
			<select name="category" id="category">
				<option value="">Pilih Satu</option>
				<?php
					$query=@mysql_query("SELECT * FROM categories WHERE 1");
					while($row=@mysql_fetch_array($query)){
						if($row['CatID']==$catid){ $select="selected"; } else { $select=""; }
						echo "<option value=\"".$row['CatID']."\" ".$select.">".$row['Categories']."</option>";
					}
				?>
			</select>
			<div id="link"><a onclick="document.location='<?php echo base_url(); ?>cpanel/content/new-category'">+ Tambah Kategori</a></div>
			<div style="clear: both"></div>
			<p><input type="submit" name="submiteditindex" value="Simpan" /></p>
		</form>
	</div>
</div>
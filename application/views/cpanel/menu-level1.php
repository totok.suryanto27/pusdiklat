<div id="admin-content">
	<div class="admin-content-title"><h1>Tambah Menu Level 1</h1></div>
	<div class="admin-content-content">
		<!--[if IE]>
			<style>
				.admin-content-content  input[type="text"] { 
					padding: 10px 5px 5px 5px;
				}
				.admin-content-content  select { 
					padding: 10px 5px 5px 5px;
				}
			</style>
		<![endif]-->
		<!--[if !IE]> -->
			<style>
				.admin-content-content  input[type="text"] { 
					padding: 5px 5px 5px 5px;
				}
				.admin-content-content  select { 
					padding: 5px 5px 5px 5px;
				}
			</style>
		<!-- <![endif]-->
		<?php
			$error=$this->session->userdata('warning');
			$warning=str_replace('<p>','<li>',$error);
			$warning=str_replace('</p>','</li>',$warning);
			if(isset($error) && $error){
				echo "
					<div id=\"tips\" >
						<ul>".$warning."</ul>
					</div>";
			}
			if($this->session->userdata('warning')!=""){
				$this->session->set_userdata('warning','');
			}
			
			if($this->session->userdata('isMenulvl1')){			
				$data=$this->session->userdata('isMenulvl1');
				$titlemenulvl1=$data['titlemenulvl1'];
				$webtitle=$data['webtitle'];
			} else {
				$titlemenulvl1="";
				$webtitle="";
			}
		?>
		<p>Menu Level 1 adalah menu yang dilihat dalam tampilan awal</p>
		<p>Catatan: Menu Beranda merupakan menu defaut sehingga tidak perlu ditambahkan</p>
		<img src="<?php echo base_url();?>asset/admin/images/menu-level1.jpg" width="920px"/>
		<form action="" method="post" name="post" id="post" autocomplete="off">
			<h4 style="margin-top: 10px;">Nama Menu Level 1</h4>
			<input type="text" name="titlemenulvl1" id="titlemenulvl1" size="109%" value="<?php echo $titlemenulvl1; ?>"/>
			<div style="clear: both"></div>
			<h4 style="margin-top: 10px;">Judul (cth: Selamat Datang di Website Pusdiklat BKPM)</h4>
			<input type="text" name="webtitle" id="webtitle" size="109%" value="<?php echo $webtitle; ?>"/>
			<div style="clear: both"></div>
			<input type="checkbox" name="haveSub" id="haveSub" value="Ada" checked onclick="disabledlink('level1')" /> Memiliki Sub Menu (Level 2)
			<div style="clear: both"></div>
			<fieldset>
				<div class="optradio"><input type="radio" name="opt" id="opt" value="pages" disabled onclick="activateinput(this.value)"></div>
				<div class="opttitle">Halaman</div>
				<div class="optinput">
					<select name="optinpages" id="optinpages" disabled style="width: 350px;">
						<option value="">Pilih Satu</option>
						<?php
							$query=@mysql_query("SELECT * FROM pages WHERE 1");
							while($row=@mysql_fetch_array($query)){
								echo "<option value=\"".$row['PageID']."\">".$row['TitlePage']."</option>";
							}
						?>
					</select>
				</div>
				<div style="clear: both"></div>
				<div class="optradio"><input type="radio" name="opt" id="opt" value="category" disabled onclick="activateinput(this.value)"></div>
				<div class="opttitle">Kategori</div>
				<div class="optinput">
					<select name="optincat" id="optincat" disabled style="width: 350px;">
						<option value="">Pilih Satu</option>
						<?php
							$query=@mysql_query("SELECT * FROM categories WHERE 1");
							while($row=@mysql_fetch_array($query)){
								echo "<option value=\"".$row['CatID']."\">".$row['Categories']."</option>";
							}
						?>
					</select>
				</div>
				<div style="clear: both"></div>
				<div class="optradio"><input type="radio" name="opt" id="opt" value="registration" disabled onclick="activateinput(this.value)"></div>
				<div class="opttitle">Pendaftaran Online</div>
				<div class="optinput">
					<select name="optinreg" id="optinreg" disabled style="width: 350px;">
						<option value="">Pilih Satu</option>
						<?php
							$query=@mysql_query("SELECT * FROM activity WHERE 1");
							while($row=@mysql_fetch_array($query)){
								echo "<option value=\"".$row['id']."\">".$row['activity']."</option>";
							}
						?>
					</select>
				</div>
				<div style="clear: both"></div>
				<div class="optradio"><input type="radio" name="opt" id="opt" value="other" disabled onclick="activateinput(this.value)"></div>
				<div class="opttitle">Link Luar [ http:// ]</div>
				<div class="optinput">
					<input type="text" name="otherlink" id="otherlink" size="40%" value="" disabled/>
				</div>
				<div style="clear: both"></div>
				<div class="optradio" style="height: 25px;"><input type="radio" name="opt" id="opt" value="photo" disabled onclick="activateinput(this.value)"></div>
				<div class="opttitle">Gallery Photo</div>
				<div class="optinput">
					&nbsp;
				</div>
				<div style="clear: both"></div>
				<div class="optradio" style="height: 25px;"><input type="radio" name="opt" id="opt" value="video" disabled onclick="activateinput(this.value)"></div>
				<div class="opttitle">Gallery Video</div>
				<div class="optinput">
					&nbsp;
				</div>
				<div style="clear: both"></div>
				<div class="optradio" style="height: 25px;"><input type="radio" name="opt" id="opt" value="consultation" disabled onclick="activateinput(this.value)"></div>
				<div class="opttitle">Konsultasi</div>
				<div class="optinput">
					&nbsp;
				</div>
				<div style="clear: both"></div>
			</fieldset>
			<div style="clear: both"></div>
			<input type="submit" name="submitlevel1" id="submitlevel1" value="Simpan" />
		</form>
	</div>
</div>
<div class="row-fluid">
	<div class="span12">
		<ul class="breadcrumb">
			<li><a style="cursor: pointer; ">Pengaturan</a> <span class="divider">/</span></li>
			<li><a href="<?=base_url(); ?>cpanel/appereance/slider">Gambar Slider</a> <span class="divider">/</span></li>
			<li><a href="<?=base_url(); ?>cpanel/appereance/add-slider">Tambah Gambar Slider</a> <span class="divider">/</span></li>
		</ul>
	</div>
</div>

<?php
	$error=$this->session->userdata('warning');
	$warning=str_replace('<p>','<li>',$error);
	$warning=str_replace('</p>','</li>',$warning);
	if(isset($error) && $error){
		echo "
			<div class=\"alert alert-block\" >
				<h4 class=\"alert-heading\">Peringatan!</h4>
				<ul>".$warning."</ul>
			</div>";
	}
	if($this->session->userdata('warning')!=""){
		$this->session->set_userdata('warning','');
	}
?>

<div class="row-fluid">
	<section class="utopia-widget utopia-form-box section">
		<div class="utopia-widget-title">
			<img src="<?=base_url(); ?>public/images/admin/monitor.png" class="utopia-widget-icon">
			<span>Tambah Gambar Slider</span>
		</div>

		<div class="row-fluid">
			<div id="showform" style="padding: 10px 10px 10px 10px;">
				<h3>Peringatan! Untuk tampilan yang lebih baik, gunakan image dengan dimensi 940px x 300px</h3>
				<p>&nbsp;</p>
				<form class="form-horizontal" action="" method="post" name="post" id="post" enctype="multipart/form-data">
                    <fieldset>
                        <div class="control-group">
                            <label class="control-label" for="Nama File">Nama File:</label>
                            <div class="controls">
								<input type="file" accept="image/*" multiple name="userfile[]" />
                            </div>
                        </div>
						
						<p><input type="submit" value="Upload File!" class="btn btn-primary span5"  name="submit-file" style="width: 100px; margin-left: 20px;"/></p>
                    </fieldset>
                </form>
			</div>
		</div>
	</section>
</div>
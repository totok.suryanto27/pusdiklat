<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set( "memory_limit","128M");
set_time_limit(0);
date_default_timezone_set('Asia/Jakarta');
session_start();
class Controller_Pages extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->clear_cache();
	}
	
	function clear_cache(){
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
	}
	
	function check_menulvl1($titlemenulvl1){
		$this->db->select('*');
		$this->db->from('menulvl1');
		$this->db->where('MenuName',$titlemenulvl1);
		$result=$this->db->get();
		$ndata=$result->num_rows();
		if($ndata>0){
			$this->form_validation->set_message('check_menulvl1', 'Menu '.$titlemenulvl1.' sudah ada dalam database!');
			return FALSE;
		} else {
			return TRUE;
		}
	}
	
	function convert_url($str){
		$url=str_replace(' ','-',$str);
		$url=str_replace('#','-',$url);
		$url=str_replace('%','-',$url);
		$url=str_replace('&','-',$url);
		$url=str_replace('*','-',$url);
		$url=str_replace('{','-',$url);
		$url=str_replace('}','-',$url);
		$url=str_replace('\\','-',$url);
		$url=str_replace(':','-',$url);
		$url=str_replace('<','-',$url);
		$url=str_replace('>','-',$url);
		$url=str_replace('?','-',$url);
		$url=str_replace('/','-',$url);
		$url=str_replace('+','-',$url);
		$url=str_replace(',','',$url);
		$url=str_replace('&rsquo;','-',$url);
		$url=str_replace('&rdquo;','-',$url);
		$url=strtolower($url);
		return $url;
	}
	
	function check_ndata(){
		$this->db->select('*');
		$this->db->from('menulvl1');
		$result=$this->db->get();
		$ndata=$result->num_rows();
		if($ndata>8){
			$this->form_validation->set_message('check_ndata', 'Maksimal Menu adalah 10 Menu!');
			return FALSE;
		} else {
			return TRUE;
		}
	}
	
	function check_menulvl1_edit($titlemenulvl1, $id){
		$this->db->select('*');
		$this->db->from('menulvl1');
		$this->db->where('MenuName',$titlemenulvl1);
		$result=$this->db->get();
		$ndata=$result->num_rows();
		if($ndata>0){
			$result_lvl1=$result->result();
			foreach($result_lvl1 as $row){
				$lvl1id=$row->Lvl1ID;
			}
			
			if($lvl1id!=$id){
				$this->form_validation->set_message('check_menulvl1_edit', 'Menu '.$titlemenulvl1.' sudah ada dalam database!');
				return FALSE;
			} else {
				return TRUE;
			}
		} else {
			return TRUE;
		}
	}
	
	function check_menulvl2($titlemenulvl2){
		$this->db->select('*');
		$this->db->from('menulvl2');
		$this->db->where('MenuName',$titlemenulvl2);
		$result=$this->db->get();
		$ndata=$result->num_rows();
		if($ndata>0){
			$this->form_validation->set_message('check_menulvl2', 'Menu '.$titlemenulvl2.' sudah ada dalam database!');
			return FALSE;
		} else {
			return TRUE;
		}
	}
	
	function check_menulvl2_edit($titlemenulvl2, $id){
		$this->db->select('*');
		$this->db->from('menulvl2');
		$this->db->where('MenuName',$titlemenulvl2);
		$result=$this->db->get();
		$ndata=$result->num_rows();
		if($ndata>0){
			$result_lvl2=$result->result();
			foreach($result_lvl2 as $row){
				$lvl2id=$row->Lvl2ID;
			}
			
			if($lvl2id!=$id){
				$this->form_validation->set_message('check_menulvl2_edit', 'Menu '.$titlemenulvl2.' sudah ada dalam database!');
				return FALSE;
			} else {
				return TRUE;
			}
		} else {
			return TRUE;
		}
	}
	
	function check_menulvl3($titlemenulvl3){
		$this->db->select('*');
		$this->db->from('menulvl3');
		$this->db->where('MenuName',$titlemenulvl3);
		$result=$this->db->get();
		$ndata=$result->num_rows();
		if($ndata>0){
			$this->form_validation->set_message('check_menulvl3', 'Menu '.$titlemenulvl3.' sudah ada dalam database!');
			return FALSE;
		} else {
			return TRUE;
		}
	}
	
	function check_menulvl3_edit($titlemenulvl3, $id){
		$this->db->select('*');
		$this->db->from('menulvl3');
		$this->db->where('MenuName',$titlemenulvl3);
		$result=$this->db->get();
		$ndata=$result->num_rows();
		if($ndata>0){
			$result_lvl3=$result->result();
			foreach($result_lvl3 as $row){
				$lvl3id=$row->Lvl3ID;
			}
			
			if($lvl3id!=$id){
				$this->form_validation->set_message('check_menulvl3_edit', 'Menu '.$titlemenulvl3.' sudah ada dalam database!');
				return FALSE;
			} else {
				return TRUE;
			}
		} else {
			return TRUE;
		}
	}
	
	function check_tab($str){
		$this->db->select('*');
		$this->db->from('indextab');
		$this->db->where('TabName',$str);
		$result=$this->db->get();
		$ndata=$result->num_rows();
		if($ndata>0){
			$this->form_validation->set_message('check_tab', 'Menu '.$str.' sudah ada dalam database!');
			return FALSE;
		} else {
			return TRUE;
		}
	}
	
	function check_ndatatab(){
		$this->db->select('*');
		$this->db->from('indextab');
		$result=$this->db->get();
		$ndata=$result->num_rows();
		if($ndata>5){
			$this->form_validation->set_message('check_ndatatab', 'Maksimal Menu adalah 5 Tab!');
			return FALSE;
		} else {
			return TRUE;
		}
	}
	
	function check_tabedit($str, $id){
		$this->db->select('*');
		$this->db->from('indextab');
		$this->db->where('TabName',$str);
		$result=$this->db->get();
		$ndata=$result->num_rows();
		if($ndata>0){
			$result_tab=$result->result();
			foreach($result_tab as $row){
				$tabid=$row->TabID;
			}
			
			if($tabid!=$id){
				$this->form_validation->set_message('check_tabedit', 'Tab '.$str.' sudah ada dalam database!');
				return FALSE;
			} else {
				return TRUE;
			}
		} else {
			return TRUE;
		}
	}
	
	function check_database($password){
		
		$username=$this->input->post('username');
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('user',$username);
		$this->db->limit(1);
		$result=$this->db->get();
		$ndata=$result->num_rows();
		
		if($ndata==1){
			$result=$result->result();
			foreach($result as $row){
				if($row->password==$password){
					$sess_array = array();
					$sess_array = array(
						'id' => $row->id,
						'username' => $row->user,
						'nama' => $row->name
						);
					$this->session->set_userdata('isLoggedIn', $sess_array);
					return TRUE;
				} else {
					$this->form_validation->set_message('check_database', 'Username atau Password salah!');
					return false;
				}
				
			}
		} else {
			$this->form_validation->set_message('check_database', 'Username atau Password salah!');
			return false;
		}	
	}
	
	function check_pages($pages){
		$url=$this->convert_url($pages);
		$this->db->select('*');
		$this->db->from('pages');
		$this->db->where('url',$url);
		$this->db->limit(1);
		$result=$this->db->get();
		$ndata=$result->num_rows();
		
		if($ndata>0){
			$this->form_validation->set_message('check_pages', 'Judul Halaman yang Anda masukkan sudah ada dalam database!');
			return false;
		} else {
			return true;
		}
	}
	
	function check_pages_edit($str, $id){
		$url=$this->convert_url($str);
		$this->db->select('*');
		$this->db->from('pages');
		$this->db->where('url',$url);
		$result=$this->db->get();
		$ndata=$result->num_rows();
		if($ndata>0){
			$result=$result->result();
			foreach($result as $row){
				$pageid=$row->PageID;
			}
			
			if($pageid!=$this->input->post('postid')){
				$this->form_validation->set_message('check_pages_edit', 'Judul Halaman '.$str.' sudah ada dalam database!');
				return FALSE;
			} else {
				return TRUE;
			}
		} else {
			return TRUE;
		}
	}
	
	function check_post(){
		$url=$this->convert_url($this->input->post('titlepost'));
		$this->db->select('*');
		$this->db->from('posts');
		$this->db->where('url',$url);
		$this->db->limit(1);
		$result=$this->db->get();
		$ndata=$result->num_rows();
		
		if($ndata>0){
			$this->form_validation->set_message('check_post', 'Judul Konten yang Anda masukkan sudah ada dalam database!');
			return false;
		} else {
			return true;
		}
	}
	
	function check_post_edit(){
		$url=$this->convert_url($this->input->post('titlepost'));
		$this->db->select('*');
		$this->db->from('posts');
		$this->db->where('url',$url);
		$result=$this->db->get();
		$ndata=$result->num_rows();
		if($ndata>0){
			$result=$result->result();
			foreach($result as $row){
				$pageid=$row->PostID;
			}
			
			if($pageid!=$this->input->post('postid')){
				$this->form_validation->set_message('check_post_edit', 'Judul Konten '.$this->input->post('titlepost').' sudah ada dalam database!');
				return FALSE;
			} else {
				return TRUE;
			}
		} else {
			return TRUE;
		}
	}
	
	function check_cat(){
		$url=$this->convert_url($this->input->post('titlecategory'));
		$this->db->select('*');
		$this->db->from('categories');
		$this->db->where('Slugs',$url);
		$this->db->limit(1);
		$result=$this->db->get();
		$ndata=$result->num_rows();
		
		if($ndata>0){
			$this->form_validation->set_message('check_cat', 'Nama Kategori yang Anda masukkan sudah ada dalam database!');
			return false;
		} else {
			return true;
		}
	}
	
	function check_cat_edit(){
		$url=$this->convert_url($this->input->post('titlecategory'));
		$this->db->select('*');
		$this->db->from('categories');
		$this->db->where('Slugs',$url);
		$result=$this->db->get();
		$ndata=$result->num_rows();
		if($ndata>0){
			$result=$result->result();
			foreach($result as $row){
				$pageid=$row->CatID;
			}
			
			if($pageid!=$this->input->post('postid')){
				$this->form_validation->set_message('check_cat_edit', 'Nama Kategori '.$this->input->post('titlecategory').' sudah ada dalam database!');
				return FALSE;
			} else {
				return TRUE;
			}
		} else {
			return TRUE;
		}
	}
	
	function check_ext(){
		$this->db->select('*');
		$this->db->from('extlink');
		$this->db->where('ExtLink',$this->input->post('titleext'));
		$this->db->limit(1);
		$result=$this->db->get();
		$ndata=$result->num_rows();
		
		if($ndata>0){
			$this->form_validation->set_message('check_ext', 'Tautan Luar yang Anda masukkan sudah ada dalam database!');
			return false;
		} else {
			return true;
		}
	}
	
	function check_ext_edit(){
		$this->db->select('*');
		$this->db->from('extlink');
		$this->db->where('ExtLink',$this->input->post('titleext'));
		$result=$this->db->get();
		$ndata=$result->num_rows();
		if($ndata>0){
			$result=$result->result();
			foreach($result as $row){
				$pageid=$row->ExtID;
			}
			
			if($pageid!=$this->input->post('postid')){
				$this->form_validation->set_message('check_ext_edit', 'Tautan Luar '.$this->input->post('titleext').' sudah ada dalam database!');
				return FALSE;
			} else {
				return TRUE;
			}
		} else {
			return TRUE;
		}
	}
	
	function check_albumphoto(){
		$url=$this->convert_url($this->input->post('titlealbum'));
		$this->db->select('*');
		$this->db->from('albumphoto');
		$this->db->where('Url',$url);
		$this->db->limit(1);
		$result=$this->db->get();
		$ndata=$result->num_rows();
		
		if($ndata>0){
			$this->form_validation->set_message('check_albumphoto', 'Judul Album yang Anda masukkan sudah ada dalam database!');
			return false;
		} else {
			return true;
		}
	}
	
	function check_albumphoto_edit(){
		$url=$this->convert_url($this->input->post('titlealbum'));
		$this->db->select('*');
		$this->db->from('albumphoto');
		$this->db->where('url',$url);
		$result=$this->db->get();
		$ndata=$result->num_rows();
		if($ndata>0){
			$result=$result->result();
			foreach($result as $row){
				$pageid=$row->AlbPhID;
			}
			
			if($pageid!=$this->input->post('postid')){
				$this->form_validation->set_message('check_albumphoto_edit', 'Nama Album Foto '.$this->input->post('titlealbum').' sudah ada dalam database!');
				return FALSE;
			} else {
				return TRUE;
			}
		} else {
			return TRUE;
		}
	}
	
	function check_albumvideo(){
		$url=$this->convert_url($this->input->post('titlealbum'));
		$this->db->select('*');
		$this->db->from('albumvideo');
		$this->db->where('Url',$url);
		$this->db->limit(1);
		$result=$this->db->get();
		$ndata=$result->num_rows();
		
		if($ndata>0){
			$this->form_validation->set_message('check_albumvideo', 'Judul Album yang Anda masukkan sudah ada dalam database!');
			return false;
		} else {
			return true;
		}
	}
	
	
	public function index($url1="",$url2="", $url3="", $url4="") {

		/* Cek keberadaan alamat URL yang diketikkan, bila ada dalam database maka halaman diload
		   jika tidak ada tampilkan halaman situs utama */
		$ndata=@mysql_num_rows(@mysql_query("SELECT * FROM url WHERE Url_1='".$url1."' AND Url_2='".$url2."' AND Url_3='".$url3."' AND Url_4='".$url4."'"));
		
		if($ndata==1){
			/* URL ditemukan di database, dapatkan judul web & nama file yang akan ditampilkan */
			
			$query=@mysql_query("SELECT * FROM url WHERE Url_1='".$url1."' AND Url_2='".$url2."' AND Url_3='".$url3."' AND Url_4='".$url4."'");
			
			while($row=@mysql_fetch_array($query)){
				$views=$row['filename'];
				if($url1=="galeri-foto" || $url1=="galeri-video"){ $titlemenulvl1="Galeri Foto & Video"; }
				// Dapatkan Webtitle
				$row2=@mysql_fetch_array(@mysql_query("SELECT * FROM menulvl1 WHERE Url='".$url1."'"));
				$titlemenulvl1=$row2['webtitle'];
				
				if($row['webtitle']==""){
					if($row['Url_1']!="index"){
						
						if($row['TableSrc']=="pages"){
							$row3=@mysql_fetch_array(@mysql_num_rows("SELECT * FROM pages WHERE PageID='".$row['ContentID']."'"));
							$title2=$row3['TitlePage'];
						}
						
						if($row['TableSrc']=="categories"){
							$row3=@mysql_fetch_array(@mysql_num_rows("SELECT * FROM categories WHERE CatID='".$row['ContentID']."'"));
							$title2=$row3['Categories'];
						}
						
						if($row['TableSrc']=="activity"){
							$row3=@mysql_fetch_array(@mysql_num_rows("SELECT * FROM activity WHERE id='".$row['ContentID']."'"));
							$title2=$row3['activity'];
						}
						
						if($row['TableSrc']=="albumphoto"){
							$title2="Gallery Foto Kegiatan Pusdiklat BKPM";
						}
						
						if($row['TableSrc']=="photos"){
							$row3=@mysql_fetch_array(@mysql_num_rows("SELECT * FROM albumphoto WHERE AlbPhID='".$row['ContentID']."'"));
							$title2=$row3['TitleAlbum'];
						}
						
						if($row['TableSrc']=="albumvideo"){
							$title2="Gallery Video Kegiatan Pusdiklat BKPM";
						}
						
						if($row['TableSrc']=="videos"){
							$row3=@mysql_fetch_array(@mysql_num_rows("SELECT * FROM albumvideo WHERE AlbVdID='".$row['ContentID']."'"));
							$title2=$row3['TitleAlbum'];
						}
						
						if($row['TableSrc']=="consultation"){
							$title2="Kolom Konsultasi Pusdiklat BKPM";
						}
						
						$this->template->set('title',$titlemenulvl1." | ".$title2);
					} else {						
						// Cari nama kategorinya
						$this->db->select('*');
						$this->db->from('categories');
						$this->db->where('CatID', $row->ContentID);
						$query=$this->db->get();
						$query=$query->result();
						foreach($query as $rowz){
							$catname=$rowz->Categories;
						}
						$this->template->set('title',"Index | ".$catname);
					}
				} else {
					$this->template->set('title',$row['webtitle']);
				}
				
				/* ========= Syntax yang berkaitan dengan CPANEL ==== */
				if($row['UrlType']=="cpanel"){					
					
					$jquery="";
					
					if($this->session->userdata('isLoggedIn')==FALSE && $views!="login"){
						echo"<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/login\" />";
						exit;
					} 
					
					// #login-form-cpanel#
					if($views=="login"){
						if($this->session->userdata('isLoggedIn')){
							//Jika session ada maka redirect ke Dashboard
							echo"<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/dashboard\" />";
							exit;
						} else {
							$data['username']=$this->input->post('username',true);
							$data['password']=$this->input->post('password',true);
										
							$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
							$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');
										
							if($this->form_validation->run() == FALSE){
								$this->session->set_userdata('warning',validation_errors());
							} else {
								//Bila sudah login tetapi coba akses menu login akan diredirect ke Dashboard
								echo"<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/dashboard\" />";
							}
						}
					}
					
					/* // #dashboard#					
					if($views=="dashboard"){
						$jquery=
							"<link rel=\"stylesheet\" href=\"".base_url()."asset/admin/css/jquery-ui.css\">
							<script src=\"".base_url()."asset/admin/js/jquery-ui.js\"></script>
							<script>
								$(function() {
								$( \"#accordion\" ).accordion({
								heightStyle: \"content\"
								});
								});
							</script>";
					} */
					
					// #all-pages# || #all-posts# || #all-categories# || #ext-link# || #photo-album# || #add-photo#
					if($views=="consultation-cpanel" || $views=="index-tab" || $views=="all-schedule" || $views=="slider" || $views=="allmenu-level3" || $views=="allmenu-level2" || $views=="allmenu-level1" || $views=="photo-album" || $views=="ext-link" || $views=="all-categories" || $views=="all-posts" || $views=="all-pages" || $views=="add-photo"){
						/* $jquery=
							"<link href=\"".base_url()."asset/admin/themes/redmond/jquery-ui-1.8.16.custom.css\" rel=\"stylesheet\" type=\"text/css\" />
							<link href=\"".base_url()."asset/admin/js/jtable/themes/lightcolor/blue/jtable.css\" rel=\"stylesheet\" type=\"text/css\" />
							<link href=\"".base_url()."asset/admin/css/jPages.css\" rel=\"stylesheet\" type=\"text/css\" />
							<script src=\"".base_url()."asset/admin/js/jPages.js\" type=\"text/javascript\"></script>
							<script>
							$(function(){
								$(\"div.holder\").jPages({
								  containerID : \"movies\",
								  previous : \"Sebelumnya\",
								  next : \"Berikutnya\",
								  perPage : 10,
								  delay : 0
								});
							  });
							  </script>

							  <style type=\"text/css\">
							  table{ width: 100%; margin-top: 30px; }
							  td, th{ text-align: left; height:25px; }
							  th { background: #f5f5f5; }
							  th:nth-child(1){ width:10%; }
							  th:nth-child(2){ width:10%; }
							  th:nth-child(3){ width:60%; }
							  th:nth-child(4){ width:20%;}

							  form {margin-right: 10px; }
							  form label { margin-right: 5px; }
							  form select { margin-right: 75px; }
							  </style>"; */
						
							$this->session->set_userdata('isPage', '');
							$this->session->set_userdata('isPost', '');
							$this->session->set_userdata('isCategory', '');
							$this->session->set_userdata('isExtlink', '');
							$this->session->set_userdata('isAlbum', '');
							$this->session->set_userdata('isAlbumVideo', '');
							$this->session->set_userdata('isMenulvl1', '');
							$this->session->set_userdata('isMenulvl2', '');
							$this->session->set_userdata('isMenulvl3', '');
							$this->session->set_userdata('isSlider', '');
							$this->session->set_userdata('isTab', '');
							$this->session->set_userdata('isSchedule', '');
							$this->session->set_userdata('warning', '');
					}
					
					
					if($views=="candidate"){
						$jquery=
							"<link href=\"".base_url()."asset/admin/themes/redmond/jquery-ui-1.8.16.custom.css\" rel=\"stylesheet\" type=\"text/css\" />
							<link href=\"".base_url()."asset/admin/js/jtable/themes/lightcolor/blue/jtable.css\" rel=\"stylesheet\" type=\"text/css\" />
							<script src=\"".base_url()."asset/admin/js/jquery-ui-1.8.16.custom.min.js\" type=\"text/javascript\"></script>
							<script src=\"".base_url()."asset/admin/js/jtable/jquery.jtable.js\" type=\"text/javascript\"></script>";
					}
					
					// #new-page# || #edit-page# || #new-post# || #edit-post#
					if($views=="new-post" || $views=="edit-post" || $views=="new-page" || $views=="edit-page"){
						/* $jquery=
							"<script type=\"text/javascript\" src=\"".base_url()."asset/admin/tinymce/tinymce.min.js\"></script>
							<script type=\"text/javascript\">
							tinymce.init({
								selector: \"textarea\",
								theme: \"modern\",
								plugins: [
										\"advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker\",
										\"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking\",
										\"table contextmenu directionality emoticons template textcolor paste textcolor filemanager\"
										],
								toolbar1: \"bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | formatselect fontselect fontsizeselect | image\",


								menubar: false
							 });
							</script>"; */
						
						if($views=="new-page"){
							
							if($this->input->post('submitpages')){
								$this->form_validation->set_rules('titlepage', 'Judul Halaman', 'trim|required|xss_clean|callback_check_pages');
								$this->form_validation->set_rules('contentpage', 'Isi Konten Halaman', 'trim|required|xss_clean');
								if($this->form_validation->run() == FALSE){
									$sess_array = array();
									$sess_array = array(
										'titlepage' => $this->input->post('titlepage'),
										'contentpage' => $this->input->post('contentpage')
										);
									$this->session->set_userdata('isPage', $sess_array);
									
									$this->session->set_userdata('warning',validation_errors());
									echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/pages/new-page\" />";
									exit;
								} else {
									$titlepage=str_replace("'","&rsquo;",$this->input->post('titlepage'));
									$titlepage=str_replace('"',"&rdquo;",$titlepage);
									$url=$this->convert_url($this->input->post('titlepage'));
									$data=$this->session->userdata('isLoggedIn');
									
									// Masukkan data ke database pages
									$datasql=array('TitlePage'=>$titlepage, 'ContentPage'=>$this->input->post('contentpage'), 'Author'=>$data['nama'], 'RecordDate'=>date("Y-m-d"), 'Url'=>$url);
									$this->db->set($datasql);
									$this->db->insert('pages');
									echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/pages/all-pages\" />";
									exit;
								}
							}
						}
						
						if($views=="edit-page"){
							
							if($this->input->post('submitpages')){
								$this->form_validation->set_rules('titlepage', 'Judul Halaman', 'trim|required|xss_clean|callback_check_pages_edit');
								$this->form_validation->set_rules('contentpage', 'Isi Konten Halaman', 'trim|required|xss_clean');
								if($this->form_validation->run() == FALSE){
									$this->session->set_userdata('warning',validation_errors());
									echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/pages/edit-page/?post=".$this->input->post('postid')."\" />";
									exit;
								} else {
									$titlepage=str_replace("'","&rsquo;",$this->input->post('titlepage'));
									$titlepage=str_replace('"',"&rdquo;",$titlepage);
									$url=$this->convert_url($this->input->post('titlepage'));
									$data=$this->session->userdata('isLoggedIn');
									
									$alert="";
									$this->db->select('PageID');
									$this->db->from('pages');
									$this->db->where('Url',$url);
									$result=$this->db->get();
									$result_data=$result->result();
									foreach($result_data as $row){
										if($row->PageID!=$this->input->post('postid')){
											$alert="exists";
										}
									}
					
									if($alert!="exists"){
										$datasql=array('TitlePage'=>$titlepage, 'ContentPage'=>$this->input->post('contentpage'), 'Author'=>$data['nama'], 'RecordDate'=>date("Y-m-d"), 'Url'=>$url);
										$this->db->where('PageID',$this->input->post('postid'));
										$this->db->update('pages',$datasql);
									}
													

									echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/pages/all-pages\" />";
									exit;
								}
							}
						}
						
						if($views=="new-post"){
							
							if($this->input->post('submitcontent')){
								$this->form_validation->set_rules('titlepost', 'Judul Konten', 'trim|required|xss_clean|callback_check_post');
								$this->form_validation->set_rules('contentpost', 'Isi Konten', 'trim|required|xss_clean');
								if($this->form_validation->run() == FALSE){
									$sess_array = array();
									$sess_array = array(
										'titlepost' => $this->input->post('titlepost'),
										'contentpost' => $this->input->post('contentpost')
										);
									$this->session->set_userdata('isPost', $sess_array);
									
									$this->session->set_userdata('warning',validation_errors());
									echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/content/new-post\" />";
									exit;
								} else {
									$title=str_replace("'","&rsquo;",$this->input->post('titlepost'));
									$title=str_replace('"',"&rdquo;",$title);
									$contentpost=$this->input->post('contentpost');
									$url=$this->convert_url($this->input->post('titlepost'));
									$data=$this->session->userdata('isLoggedIn');
									
									// Tambah 1 untuk Kategori
									$this->db->select('Posts');
									$this->db->from('categories');
									$this->db->where('CatID',$this->input->post('categories'));
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row){
										$posts=$row->Posts;
									}
									$posts=$posts+1;
									$datasql=array('Posts'=>$posts);
									$this->db->where('CatID',$this->input->post('categories'));
									$this->db->update('categories',$datasql);	
										
									// Masukkan data ke database posts
									$datasql=array('TitlePost'=>$title, 'ContentPost'=>$contentpost, 'Author'=>$data['nama'], 'CatID'=>$this->input->post('categories'), 'RecordDate'=>date("Y-m-d"), 'Url'=>$url);
									$this->db->set($datasql);
									$this->db->insert('posts');
										
									// Dapatkan ID Post terakhir yang dimasukkan
									$this->db->select('*');
									$this->db->from('posts');
									$this->db->where('TitlePost',$title);
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row){
										$contentid=$row->PostID;
									}
										
									// Masukkan URL ke database URL
									$datasql=array('UrlType'=>'site', 'Url_1'=>'read', 'Url_2'=>$url, 'TableSrc'=>'posts', 'ContentID'=>$contentid, 'webtitle'=>$this->input->post('titlepost'), 'filename'=>'read');
									$this->db->set($datasql);
									$this->db->insert('url');
									
									echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/content/all-posts\" />";
									exit;
								}
							}
						}
						
						if($views=="edit-post"){
							
							if($this->input->post('submitcontent')){
								$this->form_validation->set_rules('titlepost', 'Judul Konten', 'trim|required|xss_clean|callback_check_post_edit');
								$this->form_validation->set_rules('contentpost', 'Isi Konten', 'trim|required|xss_clean');
								if($this->form_validation->run() == FALSE){
									$this->session->set_userdata('warning',validation_errors());
									echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/content/edit-post/?post=".$this->input->post('postid')."\" />";
									exit;
								} else {
									$titlepost=str_replace("'","&rsquo;",$this->input->post('titlepost'));
									$titlepost=str_replace('"',"&rdquo;",$titlepost);
									$url=$this->convert_url($this->input->post('titlepost'));
									$data=$this->session->userdata('isLoggedIn');
									$contentpost=$this->input->post('contentpost');
									
									$alert="";
									$this->db->select('PostID');
									$this->db->from('posts');
									$this->db->where('Url',$url);
									$result=$this->db->get();
									$result_data=$result->result();
									foreach($result_data as $row){
									if($row->PostID!=$this->input->post('postid')){
										$alert="exists";
									}
								}
					
								if($alert!="exists"){
									
									// Update di tabel posts
									$datasql=array('TitlePost'=>$titlepost, 'ContentPost'=>$contentpost, 'Author'=>$data['nama'], 'CatID'=>$this->input->post('categories'), 'RecordDate'=>date("Y-m-d"), 'Url'=>$url);
									$this->db->where('PostID',$this->input->post('postid'));
									$this->db->update('posts',$datasql);
									
									// Update di tabel categories
									if($this->input->post('categories')!=$this->input->post('oldcat')){
										// Kurangi 1 di Old Category
										$this->db->select('Posts');
										$this->db->from('categories');
										$this->db->where('CatID',$this->input->post('oldcat'));
										$result=$this->db->get();
										$result=$result->result();
										$posts=0;
										foreach($result as $row){
											$posts=$row->Posts;
										}
										$posts=$posts-1;
										$datasql=array('Posts'=>$posts);
										$this->db->where('CatID',$this->input->post('oldcat'));
										$this->db->update('categories',$datasql);
										
										// Tambah 1 di Kategori Baru
										$this->db->select('Posts');
										$this->db->from('categories');
										$this->db->where('CatID',$this->input->post('categories'));
										$result=$this->db->get();
										$result=$result->result();
										foreach($result as $row){
											$posts=$row->Posts;
										}
										$posts=$posts+1;
										$datasql=array('Posts'=>$posts);
										$this->db->where('CatID',$this->input->post('categories'));
										$this->db->update('categories',$datasql);	
									}
									
									$datasql=array('Url_2'=>$url, 'webtitle'=>$titlepost);
									$this->db->where('UrlID',$this->input->post('urlid'));
									$this->db->update('url',$datasql);
								}

									echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/content/all-posts\" />";
									exit; 
								}
							}
						}
						
					}
					
					// #new-category
					if($views=="new-category"){
						if($this->input->post('submitcategory')){
							$this->form_validation->set_rules('titlecategory', 'Nama Kategori', 'trim|required|xss_clean|callback_check_cat');
							if($this->form_validation->run() == FALSE){
								$sess_array = array();
								$sess_array = array(
									'titlecategory' => $this->input->post('titlecategory'),
									'categorydesc' => $this->input->post('categorydesc')
									);
								$this->session->set_userdata('isCategory', $sess_array);
									
								$this->session->set_userdata('warning',validation_errors());
								echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/content/new-category\" />";
								exit;
							} else {
								$titlecategory=str_replace("'","&rsquo;",$this->input->post('titlecategory'));
								$titlecategory=str_replace('"',"&rdquo;",$titlecategory);
								$slugs=$this->convert_url($this->input->post('titlecategory'));
								
								$datasql=array('Categories'=>$titlecategory, 'CatDesc'=>$this->input->post('categorydesc'), 'Slugs'=>$slugs);
								$this->db->set($datasql);
								$this->db->insert('categories');
									
								echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/content/all-categories\" />";
								exit;
							}
						}
					}
					
					// #edit-category
					if($views=="edit-category"){
							
						if($this->input->post('submitcategory')){
							$this->form_validation->set_rules('titlecategory', 'Nama Kategori', 'trim|required|xss_clean|callback_check_cat_edit');
							if($this->form_validation->run() == FALSE){
								$this->session->set_userdata('warning',validation_errors());
								echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/content/edit-cat/?post=".$this->input->post('postid')."\" />";
								exit;
							} else {
								$titlecategory=str_replace("'","&rsquo;",$this->input->post('titlecategory'));
								$titlecategory=str_replace('"',"&rdquo;",$titlecategory);
								$slugs=$this->convert_url($this->input->post('titlecategory'));
								$alert="";
								$this->db->select('CatID');
								$this->db->from('categories');
								$this->db->where('Slugs',$slugs);
								$result=$this->db->get();
								$result_data=$result->result();
								foreach($result_data as $row){
									if($row->CatID!=$this->input->post('postid')){
										$alert="exists";
									}
								}
					
								if($alert!="exists"){
									$datasql=array('Categories'=>$titlecategory, 'CatDesc'=>$this->input->post('categorydesc'), 'Slugs'=>$slugs);
									$this->db->where('CatID',$this->input->post('postid'));
									$this->db->update('categories',$datasql);
								}
								
								echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/content/all-categories\" />";
								exit;
							}
						}
					}
						
					// #newext-link
					if($views=="newext-link"){
						if($this->input->post('submitext')){
							$this->form_validation->set_rules('titleext', 'Tautan Luar', 'trim|required|xss_clean|callback_check_ext');
							if($this->form_validation->run() == FALSE){
								$sess_array = array();
								$sess_array = array(
									'titleext' => $this->input->post('titleext'),
									'extdesc' => $this->input->post('extdesc')
									);
								$this->session->set_userdata('isExtlink', $sess_array);
									
								$this->session->set_userdata('warning',validation_errors());
								echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/content/newext-link\" />";
								exit;
							} else {
								$titleext=str_replace("'","&rsquo;",$this->input->post('titleext'));
								$titleext=str_replace('"',"&rdquo;",$titleext);
								
								$datasql=array('ExtLink'=>$titleext, 'ExtDesc'=>$this->input->post('extdesc'));
								$this->db->set($datasql);
								$this->db->insert('extlink');
									
								echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/content/ext-link\" />";
								exit;
							}
						}
					}
					
					// #edit-ext
					if($views=="edit-ext"){
							
						if($this->input->post('submitext')){
							$this->form_validation->set_rules('titleext', 'Tautan Luar', 'trim|required|xss_clean|callback_check_ext_edit');
							if($this->form_validation->run() == FALSE){
								$this->session->set_userdata('warning',validation_errors());
								echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/content/edit-ext/?post=".$this->input->post('postid')."\" />";
								exit;
							} else {
								$titleext=str_replace("'","&rsquo;",$this->input->post('titleext'));
								$titleext=str_replace('"',"&rdquo;",$titleext);
								$alert="";
								$this->db->select('ExtID');
								$this->db->from('extlink');
								$this->db->where('ExtLink',$titleext);
								$result=$this->db->get();
								$result_data=$result->result();
								foreach($result_data as $row){
									if($row->ExtID!=$this->input->post('postid')){
										$alert="exists";
									}
								}
					
								if($alert!="exists"){
									$datasql=array('ExtLink'=>$titleext, 'ExtDesc'=>$this->input->post('extdesc'));
									$this->db->where('ExtID',$this->input->post('postid'));
									$this->db->update('extlink',$datasql);
								}
								
								echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/content/ext-link\" />";
								exit;
							}
						}
					}
					
					// #add-albumphoto
					if($views=="add-albumphoto"){
						if($this->input->post('submitalbum')){
							$this->form_validation->set_rules('titlealbum', 'Nama Album Foto', 'trim|required|xss_clean|callback_check_albumphoto');
							if($this->form_validation->run() == FALSE){
								$sess_array = array();
								$sess_array = array(
									'titlealbum' => $this->input->post('titlealbum'),
									'albumdesc' => $this->input->post('albumdesc')
									);
								$this->session->set_userdata('isAlbum', $sess_array);
									
								$this->session->set_userdata('warning',validation_errors());
								echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/media/new-albumphoto\" />";
								exit;
							} else {
								$titlealbum=str_replace("'","&rsquo;",$this->input->post('titlealbum'));
								$titlealbum=str_replace('"',"&rdquo;",$titlealbum);
								$url=$this->convert_url($this->input->post('titlealbum'));
								
								$datasql=array('TitleAlbum'=>$titlealbum, 'AlbumDesc'=>$this->input->post('albumdesc'), 'Url'=>$url);
								$this->db->set($datasql);
								$this->db->insert('albumphoto');
					
								$this->db->select('*');
								$this->db->from('albumphoto');
								$this->db->where('TitleAlbum',$titlealbum);
								$result=$this->db->get();
								$result_data=$result->result();
								foreach($result_data as $row){
									$albphid=$row->AlbPhID;
								}
					
								// Masuk ke URL
								$datasql=array('UrlType'=>'site', 'Url_1'=>'galeri-foto', 'Url_2'=>$url, 'filename'=>'galleryshow', 'TableSrc'=>'photos', 'ContentID'=>$albphid);
								$this->db->set($datasql);
								$this->db->insert('url');
								
								echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/media/gallery-photo\" />";
								exit;
							}
						}
					}
					
					// #add-albumvideo
					if($views=="add-albumvideo"){
						if($this->input->post('submitalbumvideo')){
							$this->form_validation->set_rules('titlealbum', 'Nama Album Video', 'trim|required|xss_clean|callback_check_albumvideo');
							if($this->form_validation->run() == FALSE){
								$sess_array = array();
								$sess_array = array(
									'titlealbum' => $this->input->post('titlealbum'),
									'albumdesc' => $this->input->post('albumdesc')
									);
								$this->session->set_userdata('isAlbumVideo', $sess_array);
									
								$this->session->set_userdata('warning',validation_errors());
								echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/media/new-albumvideo\" />";
								exit;
							} else {
								$titlealbum=str_replace("'","&rsquo;",$this->input->post('titlealbum'));
								$titlealbum=str_replace('"',"&rdquo;",$titlealbum);
								$url=$this->convert_url($this->input->post('titlealbum'));
								
								@mysql_query("INSERT INTO albumvideo (TitleAlbum, AlbumDesc, Url) VALUES ('".$titlealbum."','".$this->input->post('albumdesc')."','".$url."')");
								
								$row=@mysql_fetch_array(@mysql_query("SELECT * FROM albumvideo WHERE TitleAlbum='".$titlealbum."'"));
								$albvdid=$row['AlbVdID'];
								
								// Masuk ke URL
								@mysql_query("INSERT INTO url (UrlType, Url_1, Url_2, filename, TableSrc, ContentID) VALUES ('site', 'galeri-video', '".$url."',  'galleryshow2', 'videos', '".$albvdid."')");
								
								echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/media/gallery-video\" />";
								exit;
							}
						}
					}
					
					// #edit-albph
					if($views=="edit-albph"){
							
						if($this->input->post('submitalbum')){
							$this->form_validation->set_rules('titlealbum', 'Nama Album Foto', 'trim|required|xss_clean|callback_check_albumphoto_edit');
							if($this->form_validation->run() == FALSE){
								$this->session->set_userdata('warning',validation_errors());
								echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/media/edit-albph/?post=".$this->input->post('postid')."\" />";
								exit;
							} else {
								$titlealbum=str_replace("'","&rsquo;",$this->input->post('titlealbum'));
								$titlealbum=str_replace('"',"&rdquo;",$titlealbum);
								$url=$this->convert_url($this->input->post('titlealbum'));
								
								$alert="";
								$this->db->select('*');
								$this->db->from('albumphoto');
								$this->db->where('TitleAlbum',$this->input->post('oldname'));
								$result=$this->db->get();
								$result_data=$result->result();
								foreach($result_data as $row){
									if($row->AlbPhID!=$this->input->post('postid')){
										$alert="exists";
									}
								}
					
								if($alert!="exists"){
									$datasql=array('TitleAlbum'=>$titlealbum, 'AlbumDesc'=>$this->input->post('albumdesc'), 'Url'=>$url);
									$this->db->where('AlbPhID',$this->input->post('postid'));
									$this->db->update('albumphoto',$datasql);
									
									$url_=$this->convert_url($this->input->post('oldname'));
									
									$datasql=array('Url_2'=>$url);
									$this->db->where('UrlType','site');
									$this->db->where('Url_1','galeri-foto');
									$this->db->where('Url_2',$url_);
									$this->db->update('url',$datasql);
								}
								
								echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/media/gallery-photo\" />";
								exit;
								
							}
						}
					}
					
					// #add-photo
					if($views=="add-photo"){
						if($this->input->post('submit')){
							$filename=$_FILES['userfile']['name'];
							$upload_conf = array(
								'upload_path'   => realpath('uploads/gallery/photo/'),
								'allowed_types' => 'gif|jpg|png',
								'max_size'      => '5000',
								);
							$this->upload->initialize($upload_conf);
							foreach($_FILES['userfile'] as $key=>$val){
								$i = 1;
								foreach($val as $v){
									$field_name = "file_".$i;
									$_FILES[$field_name][$key] = $v;
									$i++;   
								}
							}
							unset($_FILES['userfile']);
							$error = "";
							$success = "";
							
							foreach($_FILES as $field_name => $file){
								if ( ! $this->upload->do_upload($field_name)){
									$error = $this->upload->display_errors('<p>','</p>');
								} else {
									// otherwise, put the upload datas here.
									// if you want to use database, put insert query in this loop
									$upload_data = $this->upload->data();
									
									$datasql=array('ImageName'=>$upload_data['file_name'], 'ImageThumb'=>'thumb_'.$upload_data['file_name'], 'ImageDesc'=>$_POST['desc-photo'], 'AlbPhID'=>$_POST['albumphoto']);
									$this->db->set($datasql);
									$this->db->insert('photos');
																
									// set the resize config
									$resize_conf = array(
										// it's something like "/full/path/to/the/image.jpg" maybe
										'source_image'  => $upload_data['full_path'], 
										// and it's "/full/path/to/the/" + "thumb_" + "image.jpg
										// or you can use 'create_thumbs' => true option instead
										'new_image'     => $upload_data['file_path'].'thumb_'.$upload_data['file_name'],
										'width'         => 180,
										'height'        => 180
										);

									// initializing
									$this->image_lib->initialize($resize_conf);

									// do it!
									if ( ! $this->image_lib->resize()){
										// if got fail.
										$error2['resize'][] = $this->image_lib->display_errors('<p>','</p>');
									} else {
										// otherwise, put each upload data to an array.
										$success[] = $upload_data;
									}
																
																
									// set the resize config
									$resize_conf_img = array(
										// it's something like "/full/path/to/the/image.jpg" maybe
										'source_image'  => $upload_data['full_path'], 
										// and it's "/full/path/to/the/" + "thumb_" + "image.jpg
										// or you can use 'create_thumbs' => true option instead
										'new_image'     => $upload_data['file_path'].$upload_data['file_name'],
										'width'         => 700,
										'height'        => 333
										);

									// initializing
									$this->image_lib->initialize($resize_conf_img);
									// do it!
									if ( ! $this->image_lib->resize()){
										// if got fail.
										$error2['resize'][] = $this->image_lib->display_errors('<p>','</p>');
									} else {
										// otherwise, put each upload data to an array.
										$success[] = $upload_data;
									}
																
									/* if($error!=""){
										$this->session->set_userdata('warning', $error);
									} else {
										$this->session->set_userdata('warning', '<p>Gambar berhasil disimpan, klik browse dan Upload File untuk menambahkan gambar baru!</p>');
									} */
															
								}
							}
							
							echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/media/add-photo/?album=".$_POST['albumphoto']."\" />";
							
						}
					}
					
					// #menu-level1
					if($views=="menu-level1"){
						/* ========= TAMBAH MENU LEVEL 1 =========== */
						if($this->input->post('submitlevel1')){
							$this->form_validation->set_rules('titlemenulvl1', 'Menu Level 1', 'trim|required|xss_clean|callback_check_menulvl1');
							$this->form_validation->set_rules('webtitle', 'Judul Halaman', 'trim|required|xss_clean');
							$this->form_validation->set_rules('ndata', 'Banyak Data', 'xss_clean|callback_check_ndata');
							if($this->input->post('haveSub')!="Ada"){
								$this->form_validation->set_rules('opt', 'Opsi Tautan (Sub Menu)', 'trim|required|xss_clean');
							}
							
							
							if($this->input->post('opt')=="pages"){
								$this->form_validation->set_rules('optinpages', 'Halaman', 'trim|required|xss_clean');
							}
							
							if($this->input->post('opt')=="category"){
								$this->form_validation->set_rules('optincat', 'Kategori', 'trim|required|xss_clean');
							}
							
							if($this->input->post('opt')=="registration"){
								$this->form_validation->set_rules('optinreg', 'Pendaftaran Online', 'trim|required|xss_clean');
							}
							
							if($this->input->post('opt')=="other"){
								$this->form_validation->set_rules('otherlink', 'Link Luar', 'trim|required|xss_clean');
							}
							
							if($this->form_validation->run() == FALSE){
								// Jika tidak tembus validasi, maka akan dikembalikan ke page menu-level1
								$sess_array = array();
								$sess_array = array(
									'titlemenulvl1' => $this->input->post('titlemenulvl1'),
									'webtitle' => $this->input->post('webtitle')
									);
								$this->session->set_userdata('isMenulvl1', $sess_array);
								
								$this->session->set_userdata('warning',validation_errors());
								echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/appereance/menu-level1\" />";
								exit;
							} else {
								// Jika tembus maka simpan ke database dengan format
								// http://www.domain.com/Url_1/Url_2/Url_3
								
								$titlemenulvl1=str_replace("'","&rsquo;",$this->input->post('titlemenulvl1'));
								$titlemenulvl1=str_replace('"',"&rdquo;",$titlemenulvl1);

								// Input Untuk memiliki Sub Menu
								if($this->input->post('haveSub')=="Ada"){
									$havesub="Ada";
									$menuname=$titlemenulvl1;
									// Tidak ada yang ditambahkan di tabel URL
									// Judul
									$webtitle=str_replace("'","&rsquo;",$this->input->post('webtitle'));
									$webtitle=str_replace('"',"&rdquo;",$webtitle);
									// Tambahkan di tabel menulvl1
									$datasql=array('MenuName'=>$menuname, 'HaveSub'=>$havesub, 'webtitle'=>$webtitle, 'Url'=>$this->convert_url($titlemenulvl1));
									$this->db->set($datasql);
									$this->db->insert('menulvl1');
								} else {
									$havesub="Tidak";
									
									// Input Jika yang dipilih adalah halaman
									if($this->input->post('opt')=="pages"){
										// Untuk tabel URL
										$urltype="site";
										// URL pertama adalah nama menu itu sendiri
										$url_1=$this->convert_url($titlemenulvl1);
										$url_2="";
										$url_3="";
										// Filename yang dibuka adalah read
										$filename="read";
										// Tabel yang dibuka adalah tabel pages
										$table="pages";
										// Content yang dibuka di tabel
										$contentid=$this->input->post('optinpages');
										// Judul
										$webtitle=str_replace("'","&rsquo;",$this->input->post('webtitle'));
										$webtitle=str_replace('"',"&rdquo;",$webtitle);
										// Untuk tabel menulvl1
										$menuname=$titlemenulvl1;
										$link=$url_1;
										$url=$url_1;
									}
									
									// Input Jika yang dipilih adalah kategori
									if($this->input->post('opt')=="category"){
										// Untuk tabel URL
										$urltype="site";
										// URL pertama adalah nama menu itu sendiri
										$url_1=$this->convert_url($titlemenulvl1);
										$url_2="";
										$url_3="";
										// Filename yang dibuka adalah read
										$filename="read";
										// Tabel yang dibuka adalah tabel categories
										$table="categories";
										// Content yang dibuka di tabel
										$contentid=$this->input->post('optincat');
										// Judul
										$webtitle=str_replace("'","&rsquo;",$this->input->post('webtitle'));
										$webtitle=str_replace('"',"&rdquo;",$webtitle);
										// Untuk tabel menulvl1
										$menuname=$titlemenulvl1;
										$link=$url_1;
										$url=$url_1;
									}
									
									// Input Jika yang dipilih adalah Pendaftaran Online
									if($this->input->post('opt')=="registration"){
										// Untuk tabel URL
										$urltype="site";
										// URL pertama adalah nama menu itu sendiri
										$url_1=$this->convert_url($titlemenulvl1);
										$url_2="";
										$url_3="";
										// Filename yang dibuka adalah registration
										$filename="registration";
										// Tabel yang dibuka adalah tabel activity
										$table="activity";
										// Content yang dibuka di tabel
										$contentid=$this->input->post('optinreg');
										// Judul
										$webtitle=str_replace("'","&rsquo;",$this->input->post('webtitle'));
										$webtitle=str_replace('"',"&rdquo;",$webtitle);
										// Untuk tabel menulvl1
										$menuname=$titlemenulvl1;
										$link=$url_1;
										$url=$url_1;
										// Tambahkan URL khusus Daftar
										// Masuk ke dalam tabel URL
										$datasql=array('UrlType'=>$urltype, 'Url_1'=>$url_1, 'Url_2'=>'registration', 'Url_3'=>$url_3, 'filename'=>'form', 'TableSrc'=>$table, 'ContentID'=>$contentid);
										$this->db->set($datasql);
										$this->db->insert('url');
										// Masuk ke dalam tabel URL
										$datasql=array('UrlType'=>$urltype, 'Url_1'=>$url_1, 'Url_2'=>'success', 'Url_3'=>$url_3, 'filename'=>'success', 'TableSrc'=>$table, 'ContentID'=>$contentid);
										$this->db->set($datasql);
										$this->db->insert('url');
										// Masuk ke dalam tabel URL
										$datasql=array('UrlType'=>$urltype, 'Url_1'=>$url_1, 'Url_2'=>'letter', 'Url_3'=>$url_3, 'filename'=>'letter', 'TableSrc'=>$table, 'ContentID'=>$contentid);
										$this->db->set($datasql);
										$this->db->insert('url');
										// Masuk ke dalam tabel URL
										$datasql=array('UrlType'=>$urltype, 'Url_1'=>$url_1, 'Url_2'=>'cancel', 'Url_3'=>$url_3, 'filename'=>'cancel', 'TableSrc'=>$table, 'ContentID'=>$contentid);
										$this->db->set($datasql);
										$this->db->insert('url');
									}
									
									// Input Jika yang dipilih adalah others
									if($this->input->post('opt')=="other"){
										// Untuk tabel URL
										$urltype="site";
										// URL pertama adalah nama menu itu sendiri
										$url_1=$this->convert_url($titlemenulvl1);
										$url_2="";
										$url_3="";
										// Filename dikosongkan
										$filename="";
										// Tabel yang dibuka adalah tabel categories
										$table="";
										// Content yang dibuka di tabel
										$contentid="";
										// Judul
										$webtitle="";
										// Untuk tabel menulvl1
										$menuname=$titlemenulvl1;
										$link="http://".$this->input->post('otherlink');
										$url=$url_1;
									}
									
									// Input Jika yang dipilih adalah photo
									if($this->input->post('opt')=="photo"){
										// Untuk tabel URL
										$urltype="site";
										// URL pertama adalah nama menu itu sendiri
										$url_1=$this->convert_url($titlemenulvl1);
										$url_2="";
										$url_3="";
										// Filename yang dibuka adalah Galeri Photo
										$filename="albumphoto";
										// Tabel yang dibuka adalah tabel albumphoto
										$table="albumphoto";
										// Content yang dibuka di tabel
										$contentid="";
										// Judul
										$webtitle=str_replace("'","&rsquo;",$this->input->post('webtitle'));
										$webtitle=str_replace('"',"&rdquo;",$webtitle);
										// Untuk tabel menulvl1
										$menuname=$titlemenulvl1;
										$link=$url_1;
										$url=$url_1;
									}
									
									// Input Jika yang dipilih adalah video
									if($this->input->post('opt')=="video"){
										// Untuk tabel URL
										$urltype="site";
										// URL pertama adalah nama menu itu sendiri
										$url_1=$this->convert_url($titlemenulvl1);
										$url_2="";
										$url_3="";
										// Filename yang dibuka adalah Galeri video
										$filename="albumvideo";
										// Tabel yang dibuka adalah tabel albumvideo
										$table="albumvideo";
										// Content yang dibuka di tabel
										$contentid="";
										// Judul
										$webtitle=str_replace("'","&rsquo;",$this->input->post('webtitle'));
										$webtitle=str_replace('"',"&rdquo;",$webtitle);
										// Untuk tabel menulvl1
										$menuname=$titlemenulvl1;
										$link=$url_1;
										$url=$url_1;
									}
									
									// Input Jika yang dipilih adalah consultation
									if($this->input->post('opt')=="consultation"){
										// Untuk tabel URL
										$urltype="site";
										// URL pertama adalah nama menu itu sendiri
										$url_1=$this->convert_url($titlemenulvl1);
										$url_2="";
										$url_3="";
										// Filename yang dibuka adalah Galeri video
										$filename="consultation";
										// Tabel yang dibuka adalah tabel albumvideo
										$table="consultation";
										// Content yang dibuka di tabel
										$contentid="";
										// Judul
										$webtitle=str_replace("'","&rsquo;",$this->input->post('webtitle'));
										$webtitle=str_replace('"',"&rdquo;",$webtitle);
										// Untuk tabel menulvl1
										$menuname=$titlemenulvl1;
										$link=$url_1;
										$url=$url_1;
									}
									
									// Masuk ke dalam tabel URL
									$datasql=array('UrlType'=>$urltype, 'Url_1'=>$url_1, 'Url_2'=>$url_2, 'Url_3'=>$url_3, 'filename'=>$filename, 'TableSrc'=>$table, 'ContentID'=>$contentid);
									$this->db->set($datasql);
									$this->db->insert('url');
									// Masuk ke dalam tabel menulvl1
									$datasql=array('MenuName'=>$menuname, 'HaveSub'=>$havesub, 'LinkTo'=>$link, 'webtitle'=>$webtitle, 'Url'=>$url);
									$this->db->set($datasql);
									$this->db->insert('menulvl1');
								}
								
								
								echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/appereance/allmenu-level1\" />";
								exit;
							}
						}
						/* ========= TAMBAH MENU LEVEL 1 =========== */
					}
					
					// #editmenulvl1
					if($views=="editmenulvl1"){
						/* ========= UBAH MENU LEVEL 1 =========== */
						if($this->input->post('submiteditlevel1')){
							$this->form_validation->set_rules('titlemenulvl1', 'Menu Level 1', 'trim|required|xss_clean|callback_check_menulvl1_edit['.$this->input->post('idpost').']');
							$this->form_validation->set_rules('webtitle', 'Judul Halaman', 'trim|required|xss_clean');
							if($this->input->post('haveSub')!="Ada"){
								$this->form_validation->set_rules('opt', 'Opsi Tautan (Sub Menu)', 'trim|required|xss_clean');
							}
							
							if($this->input->post('opt')=="pages"){
								$this->form_validation->set_rules('optinpages', 'Halaman', 'trim|required|xss_clean');
							}
							
							if($this->input->post('opt')=="category"){
								$this->form_validation->set_rules('optincat', 'Kategori', 'trim|required|xss_clean');
							}
							
							if($this->input->post('opt')=="registration"){
								$this->form_validation->set_rules('optinreg', 'Pendaftaran Online', 'trim|required|xss_clean');
							}
							
							if($this->input->post('opt')=="other"){
								$this->form_validation->set_rules('otherlink', 'Link Luar', 'trim|required|xss_clean');
							}
							
							if($this->form_validation->run() == FALSE){
								// Jika tidak tembus validasi, maka akan dikembalikan ke page menu-level1
								$this->session->set_userdata('warning',validation_errors());
								echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/appereance/editmenu-level1/?post=".$this->input->post('idpost')."\" />";
								exit;
							} else {
								// Jika tembus maka simpan ke database dengan format
								// http://www.domain.com/Url_1/Url_2/Url_3
								
								$titlemenulvl1=str_replace("'","&rsquo;",$this->input->post('titlemenulvl1'));
								$titlemenulvl1=str_replace('"',"&rdquo;",$titlemenulvl1);

								// Input Untuk memiliki Sub Menu
								if($this->input->post('haveSub')=="Ada"){
									$havesub="Ada";
									$menuname=$titlemenulvl1;
									// Tidak ada yang ditambahkan di tabel URL
									// Judul
									$webtitle=str_replace("'","&rsquo;",$this->input->post('webtitle'));
									$webtitle=str_replace('"',"&rdquo;",$webtitle);
									
									// Lakukan pengecekan di tabel URL jika ada delete
									$url_1=$this->convert_url($titlemenulvl1);
									$this->db->select('*');
									$this->db->from('url');
									$this->db->where('Url_1',$url_1);
									$this->db->where('Url_2','');
									$this->db->where('Url_3','');
									$result=$this->db->get();
									$ndata=$result->num_rows();
									if($ndata>0){
										$this->db->where('Url_1',$url_1);
										$this->db->where('Url_2','');
										$this->db->where('Url_3','');
										$this->db->delete('url');
									}
									
									// Update tabel menulvl1
									$datasql=array('MenuName'=>$menuname, 'HaveSub'=>$havesub, 'LinkTo'=>'', 'webtitle'=>$webtitle, 'Url'=>$this->convert_url($titlemenulvl1));
									$this->db->where('Lvl1ID',$this->input->post('idpost'));
									$this->db->update('menulvl1',$datasql);
								} else {
									$havesub="Tidak";
									// Hapus menu di menulvl2 & menulvl3
									// Dapatkan Lvl2ID (untuk menghapus di tabel menulvl3)
									$lvl2id="";
									$this->db->select('*');
									$this->db->from('menulvl2');
									$this->db->where('Lvl1ID',$this->input->post('idpost'));
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row){
										$lvl2id=$row->Lvl2ID;
									}
									// Hapus menu yang berkaitan dengan menulvl1 di menulvl2
									$this->db->where('Lvl1ID',$this->input->post('idpost'));
									$this->db->delete('menulvl2');
									// Hapus menu yang berkaitan dengan menulvl2 di menulvl3
									$this->db->where('Lvl2ID',$lvl2id);
									$this->db->delete('menulvl3');
									// Hapus semua yang berkaitan dengan menulvl1 di tabel URL
									$this->db->where('Url_1',$this->input->post('oldpost'));
									$this->db->delete('url');
									
									
									// Input Jika yang dipilih adalah halaman
									if($this->input->post('opt')=="pages"){
										// Untuk tabel URL
										$urltype="site";
										// URL pertama adalah nama menu itu sendiri
										$url_1=$this->convert_url($titlemenulvl1);
										$url_2="";
										$url_3="";
										// Filename yang dibuka adalah read
										$filename="read";
										// Tabel yang dibuka adalah tabel pages
										$table="pages";
										// Content yang dibuka di tabel
										$contentid=$this->input->post('optinpages');
										// Judul
										$webtitle=str_replace("'","&rsquo;",$this->input->post('webtitle'));
										$webtitle=str_replace('"',"&rdquo;",$webtitle);
										// Untuk tabel menulvl1
										$menuname=$titlemenulvl1;
										$link=$url_1;
										$url=$url_1;
									}
									
									// Input Jika yang dipilih adalah kategori
									if($this->input->post('opt')=="category"){
										// Untuk tabel URL
										$urltype="site";
										// URL pertama adalah nama menu itu sendiri
										$url_1=$this->convert_url($titlemenulvl1);
										$url_2="";
										$url_3="";
										// Filename yang dibuka adalah read
										$filename="read";
										// Tabel yang dibuka adalah tabel categories
										$table="categories";
										// Content yang dibuka di tabel
										$contentid=$this->input->post('optincat');
										// Judul
										$webtitle=str_replace("'","&rsquo;",$this->input->post('webtitle'));
										$webtitle=str_replace('"',"&rdquo;",$webtitle);
										// Untuk tabel menulvl1
										$menuname=$titlemenulvl1;
										$link=$url_1;
										$url=$url_1;
									}
									
									// Input Jika yang dipilih adalah registration
									if($this->input->post('opt')=="registration"){
										// Untuk tabel URL
										$urltype="site";
										// URL pertama adalah nama menu itu sendiri
										$url_1=$this->convert_url($titlemenulvl1);
										$url_2="";
										$url_3="";
										// Filename yang dibuka adalah read
										$filename="read";
										// Tabel yang dibuka adalah tabel activity
										$table="activity";
										// Content yang dibuka di tabel
										$contentid=$this->input->post('optinreg');
										// Judul
										$webtitle=str_replace("'","&rsquo;",$this->input->post('webtitle'));
										$webtitle=str_replace('"',"&rdquo;",$webtitle);
										// Untuk tabel menulvl1
										$menuname=$titlemenulvl1;
										$link=$url_1;
										$url=$url_1;
										
										$url_old=$this->convert_url($this->input->post('oldpost'));
										$datasql=array('UrlType'=>$urltype, 'Url_1'=>$url_1, 'Url_2'=>'registration', 'Url_3'=>$url_3, 'filename'=>'form', 'TableSrc'=>$table, 'ContentID'=>$contentid);
										$this->db->where('Url_1',$url_old);
										$this->db->where('Url_2','registration');
										$this->db->where('Url_3','');
										$this->db->update('url',$datasql);
										
										$datasql=array('UrlType'=>$urltype, 'Url_1'=>$url_1, 'Url_2'=>'success', 'Url_3'=>$url_3, 'filename'=>'success', 'TableSrc'=>$table, 'ContentID'=>$contentid);
										$this->db->where('Url_1',$url_old);
										$this->db->where('Url_2','success');
										$this->db->where('Url_3','');
										$this->db->update('url',$datasql);
										
										$datasql=array('UrlType'=>$urltype, 'Url_1'=>$url_1, 'Url_2'=>'letter', 'Url_3'=>$url_3, 'filename'=>'letter', 'TableSrc'=>$table, 'ContentID'=>$contentid);
										$this->db->where('Url_1',$url_old);
										$this->db->where('Url_2','letter');
										$this->db->where('Url_3','');
										$this->db->update('url',$datasql);
										
										$datasql=array('UrlType'=>$urltype, 'Url_1'=>$url_1, 'Url_2'=>'cancel', 'Url_3'=>$url_3, 'filename'=>'cancel', 'TableSrc'=>$table, 'ContentID'=>$contentid);
										$this->db->where('Url_1',$url_old);
										$this->db->where('Url_2','cancel');
										$this->db->where('Url_3','');
										$this->db->update('url',$datasql);
									}
									
									// Input Jika yang dipilih adalah others
									if($this->input->post('opt')=="other"){
										// Untuk tabel URL
										$urltype="site";
										// URL pertama adalah nama menu itu sendiri
										$url_1=$this->convert_url($titlemenulvl1);
										$url_2="";
										$url_3="";
										// Filename dikosongkan
										$filename="";
										// Tabel yang dibuka adalah tabel categories
										$table="";
										// Content yang dibuka di tabel
										$contentid="";
										// Judul
										$webtitle="";
										// Untuk tabel menulvl1
										$menuname=$titlemenulvl1;
										$link="http://".$this->input->post('otherlink');
										$url=$url_1;
									}
									
									// Input Jika yang dipilih adalah photo
									if($this->input->post('opt')=="photo"){
										// Untuk tabel URL
										$urltype="site";
										// URL pertama adalah nama menu itu sendiri
										$url_1=$this->convert_url($titlemenulvl1);
										$url_2="";
										$url_3="";
										// Filename yang dibuka adalah Galeri Photo
										$filename="albumphoto";
										// Tabel yang dibuka adalah tabel albumphoto
										$table="albumphoto";
										// Content yang dibuka di tabel
										$contentid="";
										// Judul
										$webtitle=str_replace("'","&rsquo;",$this->input->post('webtitle'));
										$webtitle=str_replace('"',"&rdquo;",$webtitle);
										// Untuk tabel menulvl1
										$menuname=$titlemenulvl1;
										$link=$url_1;
										$url=$url_1;
									}
									
									// Input Jika yang dipilih adalah video
									if($this->input->post('opt')=="video"){
										// Untuk tabel URL
										$urltype="site";
										// URL pertama adalah nama menu itu sendiri
										$url_1=$this->convert_url($titlemenulvl1);
										$url_2="";
										$url_3="";
										// Filename yang dibuka adalah Galeri video
										$filename="albumvideo";
										// Tabel yang dibuka adalah tabel albumvideo
										$table="albumvideo";
										// Content yang dibuka di tabel
										$contentid="";
										// Judul
										$webtitle=str_replace("'","&rsquo;",$this->input->post('webtitle'));
										$webtitle=str_replace('"',"&rdquo;",$webtitle);
										// Untuk tabel menulvl1
										$menuname=$titlemenulvl1;
										$link=$url_1;
										$url=$url_1;
									}
									
									// Input Jika yang dipilih adalah consultation
									if($this->input->post('opt')=="consultation"){
										// Untuk tabel URL
										$urltype="site";
										// URL pertama adalah nama menu itu sendiri
										$url_1=$this->convert_url($titlemenulvl1);
										$url_2="";
										$url_3="";
										// Filename yang dibuka adalah Galeri consultation
										$filename="consultation";
										// Tabel yang dibuka adalah tabel consultation
										$table="consultation";
										// Content yang dibuka di tabel
										$contentid="";
										// Judul
										$webtitle=str_replace("'","&rsquo;",$this->input->post('webtitle'));
										$webtitle=str_replace('"',"&rdquo;",$webtitle);
										// Untuk tabel menulvl1
										$menuname=$titlemenulvl1;
										$link=$url_1;
										$url=$url_1;
									}
									
									// Cek dahulu Apakah di tabel URL ada atau tidak jika tidak ada maka guakan insert, jika ada update!
									$url_old=$this->convert_url($this->input->post('oldpost'));
									$this->db->select('*');
									$this->db->from('url');
									$this->db->where('Url_1',$url_old);
									$this->db->where('Url_2','');
									$this->db->where('Url_3','');
									$result=$this->db->get();
									$ndata=$result->num_rows();
									if($ndata>0){
										$datasql=array('UrlType'=>$urltype, 'Url_1'=>$url_1, 'Url_2'=>$url_2, 'Url_3'=>$url_3, 'filename'=>$filename, 'TableSrc'=>$table, 'ContentID'=>$contentid);
										$this->db->where('Url_1',$url_old);
										$this->db->where('Url_2','');
										$this->db->where('Url_3','');
										$this->db->update('url',$datasql);
									} else {
										// Masuk ke dalam tabel URL
										$datasql=array('UrlType'=>$urltype, 'Url_1'=>$url_1, 'Url_2'=>$url_2, 'Url_3'=>$url_3, 'filename'=>$filename, 'TableSrc'=>$table, 'ContentID'=>$contentid);
										$this->db->set($datasql);
										$this->db->insert('url');
									}
									
									// Update tabel menulvl1
									$datasql=array('MenuName'=>$menuname, 'HaveSub'=>$havesub, 'LinkTo'=>$link, 'webtitle'=>$webtitle, 'Url'=>$url);
									$this->db->where('Lvl1ID',$this->input->post('idpost'));
									$this->db->update('menulvl1',$datasql);
								}
								
								
								echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/appereance/allmenu-level1\" />";
								exit;
							}
						}
						/* ========= UBAH MENU LEVEL 1 =========== */
					}
					
					// #menu-level2
					if($views=="menu-level2"){
						/* ========= TAMBAH MENU LEVEL 2 =========== */
						if($this->input->post('submitlevel2')){
							$this->form_validation->set_rules('titlemenulvl2', 'Menu Level 2', 'trim|required|xss_clean|callback_check_menulvl2');
							$this->form_validation->set_rules('menulvl1', 'Menu Level 1', 'trim|required|xss_clean');
							$this->form_validation->set_rules('webtitle', 'Judul Halaman', 'trim|required|xss_clean');
							if($this->input->post('haveSub')!="Ada"){
								$this->form_validation->set_rules('opt', 'Opsi Tautan (Sub Menu)', 'trim|required|xss_clean');
							}
							
							
							if($this->input->post('opt')=="pages"){
								$this->form_validation->set_rules('optinpages', 'Halaman', 'trim|required|xss_clean');
							}
							
							if($this->input->post('opt')=="category"){
								$this->form_validation->set_rules('optincat', 'Kategori', 'trim|required|xss_clean');
							}
							
							if($this->input->post('opt')=="registration"){
								$this->form_validation->set_rules('optinreg', 'Kategori', 'trim|required|xss_clean');
							}
							
							if($this->input->post('opt')=="other"){
								$this->form_validation->set_rules('otherlink', 'Link Luar', 'trim|required|xss_clean');
							}
							
							if($this->form_validation->run() == FALSE){
								// Jika tidak tembus validasi, maka akan dikembalikan ke page menu-level1
								$sess_array = array();
								$sess_array = array(
									'titlemenulvl2' => $this->input->post('titlemenulvl2'),
									'menulvl1' => $this->input->post('menulvl1'),
									'webtitle' => $this->input->post('webtitle')
									);
								$this->session->set_userdata('isMenulvl2', $sess_array);
								$this->session->set_userdata('warning',validation_errors());
								echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/appereance/menu-level2\" />";
								exit;
							} else {
								// Jika tembus maka simpan ke database dengan format
								// http://www.domain.com/Url_1/Url_2/Url_3
								
								$titlemenulvl2=str_replace("'","&rsquo;",$this->input->post('titlemenulvl2'));
								$titlemenulvl2=str_replace('"',"&rdquo;",$titlemenulvl2);

								// Input Untuk memiliki Sub Menu
								if($this->input->post('haveSub')=="Ada"){
									$havesub="Ada";
									$menuname=$titlemenulvl2;
									// Tidak ada yang ditambahkan di tabel URL
									// Judul
									$webtitle=str_replace("'","&rsquo;",$this->input->post('webtitle'));
									$webtitle=str_replace('"',"&rdquo;",$webtitle);
									// Dapatkan Url pertama berdasarkan menulvl1
									$this->db->select('*');
									$this->db->from('menulvl1');
									$this->db->where('Lvl1ID',$this->input->post('menulvl1'));
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row){
										$url_lvl1=$row->Url;
									}
									
									
									// Tambahkan di tabel menulvl1
									$datasql=array('Lvl1ID'=>$this->input->post('menulvl1'),'MenuName'=>$menuname, 'HaveSub'=>$havesub, 'webtitle'=>$webtitle, 'Url'=>$url_lvl1."/".$this->convert_url($titlemenulvl2));
									$this->db->set($datasql);
									$this->db->insert('menulvl2');
								} else {
									$havesub="Tidak";
									
									// Input Jika yang dipilih adalah halaman
									if($this->input->post('opt')=="pages"){
										// Untuk tabel URL
										$urltype="site";
										// URL pertama adalah nama menu itu sendiri
										// Dapatkan Url pertama berdasarkan menulvl1
										$this->db->select('*');
										$this->db->from('menulvl1');
										$this->db->where('Lvl1ID',$this->input->post('menulvl1'));
										$result=$this->db->get();
										$result=$result->result();
										foreach($result as $row){
											$url_lvl1=$row->Url;
										}
										$url_1=$url_lvl1;
										$url_2=$this->convert_url($titlemenulvl2);
										$url_3="";
										// Filename yang dibuka adalah read
										$filename="read";
										// Tabel yang dibuka adalah tabel pages
										$table="pages";
										// Content yang dibuka di tabel
										$contentid=$this->input->post('optinpages');
										// Judul
										$webtitle=str_replace("'","&rsquo;",$this->input->post('webtitle'));
										$webtitle=str_replace('"',"&rdquo;",$webtitle);
										// Untuk tabel menulvl2
										$menuname=$titlemenulvl2;
										$link=$url_1."/".$url_2;
										$url=$url_1."/".$url_2;
									}
									
									// Input Jika yang dipilih adalah kategori
									if($this->input->post('opt')=="category"){
										// Untuk tabel URL
										$urltype="site";
										// Dapatkan Url pertama berdasarkan menulvl1
										$this->db->select('*');
										$this->db->from('menulvl1');
										$this->db->where('Lvl1ID',$this->input->post('menulvl1'));
										$result=$this->db->get();
										$result=$result->result();
										foreach($result as $row){
											$url_lvl1=$row->Url;
										}
										$url_1=$url_lvl1;
										$url_2=$this->convert_url($titlemenulvl2);
										$url_3="";
										// Filename yang dibuka adalah read
										$filename="read";
										// Tabel yang dibuka adalah tabel categories
										$table="categories";
										// Content yang dibuka di tabel
										$contentid=$this->input->post('optincat');
										// Judul
										$webtitle=str_replace("'","&rsquo;",$this->input->post('webtitle'));
										$webtitle=str_replace('"',"&rdquo;",$webtitle);
										// Untuk tabel menulvl2
										$menuname=$titlemenulvl2;
										$link=$url_1."/".$url_2;
										$url=$url_1."/".$url_2;
										
									}
									
									// Input Jika yang dipilih adalah registration
									if($this->input->post('opt')=="registration"){
										// Untuk tabel URL
										$urltype="site";
										// Dapatkan Url pertama berdasarkan menulvl1
										$this->db->select('*');
										$this->db->from('menulvl1');
										$this->db->where('Lvl1ID',$this->input->post('menulvl1'));
										$result=$this->db->get();
										$result=$result->result();
										foreach($result as $row){
											$url_lvl1=$row->Url;
										}
										$url_1=$url_lvl1;
										$url_2=$this->convert_url($titlemenulvl2);
										$url_3="";
										// Filename yang dibuka adalah read
										$filename="read";
										// Tabel yang dibuka adalah tabel activity
										$table="activity";
										// Content yang dibuka di tabel
										$contentid=$this->input->post('optinreg');
										// Judul
										$webtitle=str_replace("'","&rsquo;",$this->input->post('webtitle'));
										$webtitle=str_replace('"',"&rdquo;",$webtitle);
										// Untuk tabel menulvl2
										$menuname=$titlemenulvl2;
										$link=$url_1."/".$url_2;
										$url=$url_1."/".$url_2;
										// Tambahkan URL khusus Daftar
										// Masuk ke dalam tabel URL
										$datasql=array('UrlType'=>$urltype, 'Url_1'=>$url_1, 'Url_3'=>'registration', 'Url_2'=>$url_2, 'filename'=>'form', 'TableSrc'=>$table, 'ContentID'=>$contentid);
										$this->db->set($datasql);
										$this->db->insert('url');
										
										// Masuk ke dalam tabel URL
										$datasql=array('UrlType'=>$urltype, 'Url_1'=>$url_1, 'Url_3'=>'success', 'Url_2'=>$url_2, 'filename'=>'success', 'TableSrc'=>$table, 'ContentID'=>$contentid);
										$this->db->set($datasql);
										$this->db->insert('url');
										
										$datasql=array('UrlType'=>$urltype, 'Url_1'=>$url_1, 'Url_3'=>'letter', 'Url_2'=>$url_2, 'filename'=>'letter', 'TableSrc'=>$table, 'ContentID'=>$contentid);
										$this->db->set($datasql);
										$this->db->insert('url');
										
										$datasql=array('UrlType'=>$urltype, 'Url_1'=>$url_1, 'Url_3'=>'cancel', 'Url_2'=>$url_2, 'filename'=>'cancel', 'TableSrc'=>$table, 'ContentID'=>$contentid);
										$this->db->set($datasql);
										$this->db->insert('url');
									}
									
									// Input Jika yang dipilih adalah others
									if($this->input->post('opt')=="other"){
										// Untuk tabel URL
										$urltype="site";
										// Dapatkan Url pertama berdasarkan menulvl1
										$this->db->select('*');
										$this->db->from('menulvl1');
										$this->db->where('Lvl1ID',$this->input->post('menulvl1'));
										$result=$this->db->get();
										$result=$result->result();
										foreach($result as $row){
											$url_lvl1=$row->Url;
										}
										$url_1=$url_lvl1;
										$url_2=$this->convert_url($titlemenulvl2);
										$url_3="";
										// Filename dikosongkan
										$filename="";
										// Tabel yang dibuka adalah tabel categories
										$table="";
										// Content yang dibuka di tabel
										$contentid="";
										// Judul
										$webtitle="";
										// Untuk tabel menulvl2
										$menuname=$titlemenulvl2;
										$link="http://".$this->input->post('otherlink');
										$url=$url_1."/".$url_2;
									}
									
									// Input Jika yang dipilih adalah photo
									if($this->input->post('opt')=="photo"){
										// Untuk tabel URL
										$urltype="site";
										// Dapatkan Url pertama berdasarkan menulvl1
										$this->db->select('*');
										$this->db->from('menulvl1');
										$this->db->where('Lvl1ID',$this->input->post('menulvl1'));
										$result=$this->db->get();
										$result=$result->result();
										foreach($result as $row){
											$url_lvl1=$row->Url;
										}
										$url_1=$url_lvl1;
										$url_2=$this->convert_url($titlemenulvl2);
										$url_3="";
										// Filename yang dibuka adalah Galeri Photo
										$filename="albumphoto";
										// Tabel yang dibuka adalah tabel albumphoto
										$table="albumphoto";
										// Content yang dibuka di tabel
										$contentid="";
										// Judul
										$webtitle=str_replace("'","&rsquo;",$this->input->post('webtitle'));
										$webtitle=str_replace('"',"&rdquo;",$webtitle);
										// Untuk tabel menulvl2
										$menuname=$titlemenulvl2;
										$link=$url_1."/".$url_2;
										$url=$url_1."/".$url_2;
									}
									
									// Input Jika yang dipilih adalah video
									if($this->input->post('opt')=="video"){
										// Untuk tabel URL
										$urltype="site";
										// Dapatkan Url pertama berdasarkan menulvl1
										$this->db->select('*');
										$this->db->from('menulvl1');
										$this->db->where('Lvl1ID',$this->input->post('menulvl1'));
										$result=$this->db->get();
										$result=$result->result();
										foreach($result as $row){
											$url_lvl1=$row->Url;
										}
										$url_1=$url_lvl1;
										$url_2=$this->convert_url($titlemenulvl2);
										$url_3="";
										// Filename yang dibuka adalah Galeri video
										$filename="albumvideo";
										// Tabel yang dibuka adalah tabel albumvideo
										$table="albumvideo";
										// Content yang dibuka di tabel
										$contentid="";
										// Judul
										$webtitle=str_replace("'","&rsquo;",$this->input->post('webtitle'));
										$webtitle=str_replace('"',"&rdquo;",$webtitle);
										// Untuk tabel menulvl2
										$menuname=$titlemenulvl2;
										$link=$url_1."/".$url_2;
										$url=$url_1."/".$url_2;
									} 
									
									// Input Jika yang dipilih adalah consultation
									if($this->input->post('opt')=="consultation"){
										// Untuk tabel URL
										$urltype="site";
										// Dapatkan Url pertama berdasarkan menulvl1
										$this->db->select('*');
										$this->db->from('menulvl1');
										$this->db->where('Lvl1ID',$this->input->post('menulvl1'));
										$result=$this->db->get();
										$result=$result->result();
										foreach($result as $row){
											$url_lvl1=$row->Url;
										}
										$url_1=$url_lvl1;
										$url_2=$this->convert_url($titlemenulvl2);
										$url_3="";
										// Filename yang dibuka adalah Galeri consultation
										$filename="consultation";
										// Tabel yang dibuka adalah tabel albumvideo
										$table="consultation";
										// Content yang dibuka di tabel
										$contentid="";
										// Judul
										$webtitle=str_replace("'","&rsquo;",$this->input->post('webtitle'));
										$webtitle=str_replace('"',"&rdquo;",$webtitle);
										// Untuk tabel menulvl2
										$menuname=$titlemenulvl2;
										$link=$url_1."/".$url_2;
										$url=$url_1."/".$url_2;
									} 
									
									// Masuk ke dalam tabel URL
									$datasql=array('UrlType'=>$urltype, 'Url_1'=>$url_1, 'Url_2'=>$url_2, 'Url_3'=>$url_3, 'filename'=>$filename, 'TableSrc'=>$table, 'ContentID'=>$contentid);
									$this->db->set($datasql);
									$this->db->insert('url');
									// Masuk ke dalam tabel menulvl2
									$datasql=array('Lvl1ID'=>$this->input->post('menulvl1'), 'MenuName'=>$menuname, 'HaveSub'=>$havesub, 'LinkTo'=>$link, 'webtitle'=>$webtitle, 'Url'=>$url);
									$this->db->set($datasql);
									$this->db->insert('menulvl2'); 
								}
								
								
								echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/appereance/allmenu-level2\" />";
								exit;
							}
						}
						/* ========= TAMBAH MENU LEVEL 2 =========== */
					
					}
					
					// #editmenulvl2
					if($views=="editmenulvl2"){
						/* ========= UBAH MENU LEVEL 2 =========== */
						if($this->input->post('submiteditlevel2')){
							$this->form_validation->set_rules('titlemenulvl2', 'Menu Level 2', 'trim|required|xss_clean|callback_check_menulvl2_edit['.$this->input->post('idpost').']');
							$this->form_validation->set_rules('menulvl1', 'Menu Level 1', 'trim|required|xss_clean');
							$this->form_validation->set_rules('webtitle', 'Judul Halaman', 'trim|required|xss_clean');
							
							if($this->input->post('haveSub')!="Ada"){
								$this->form_validation->set_rules('opt', 'Opsi Tautan (Sub Menu)', 'trim|required|xss_clean');
							}
							
							if($this->input->post('opt')=="pages"){
								$this->form_validation->set_rules('optinpages', 'Halaman', 'trim|required|xss_clean');
							}
							
							if($this->input->post('opt')=="category"){
								$this->form_validation->set_rules('optincat', 'Kategori', 'trim|required|xss_clean');
							}
							
							if($this->input->post('opt')=="registration"){
								$this->form_validation->set_rules('optinreg', 'Kategori', 'trim|required|xss_clean');
							}
							
							if($this->input->post('opt')=="other"){
								$this->form_validation->set_rules('otherlink', 'Link Luar', 'trim|required|xss_clean');
							}
							
							if($this->form_validation->run() == FALSE){
								// Jika tidak tembus validasi, maka akan dikembalikan ke page menu-level2
								$this->session->set_userdata('warning',validation_errors());
								echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."appereance/editmenu-level2/?post=".$this->input->post('idpost')."\" />";
								exit;
							} else {
								// Jika tembus maka simpan ke database dengan format
								// http://www.domain.com/Url_1/Url_2/Url_3
								
								$titlemenulvl2=str_replace("'","&rsquo;",$this->input->post('titlemenulvl2'));
								$titlemenulvl2=str_replace('"',"&rdquo;",$titlemenulvl2);

								// Input Untuk memiliki Sub Menu
								if($this->input->post('haveSub')=="Ada"){
									$havesub="Ada";
									$menuname=$titlemenulvl2;
									// Tidak ada yang ditambahkan di tabel URL
									// Judul
									$webtitle=str_replace("'","&rsquo;",$this->input->post('webtitle'));
									$webtitle=str_replace('"',"&rdquo;",$webtitle);
									
									// Lakukan pengecekan di tabel URL jika ada delete
									// Dapatkan Url pertama berdasarkan menulvl1
									$this->db->select('*');
									$this->db->from('menulvl1');
									$this->db->where('Lvl1ID',$this->input->post('menulvl1'));
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row){
										$url_lvl1=$row->Url;
									}
									$url_1=$url_lvl1;
									$url_2=$this->convert_url($titlemenulvl2);
									$this->db->select('*');
									$this->db->from('url');
									$this->db->where('Url_1',$url_1);
									$this->db->where('Url_2',$url_2);
									$this->db->where('Url_3','');
									$result=$this->db->get();
									$ndata=$result->num_rows();
									if($ndata>0){
										$this->db->where('Url_1',$url_1);
										$this->db->where('Url_2',$url_2);
										$this->db->where('Url_3','');
										$this->db->delete('url');
									}
									
									// Update tabel menulvl2
									$datasql=array('Lvl1ID'=>$this->input->post('menulvl1'), 'MenuName'=>$menuname, 'HaveSub'=>$havesub, 'LinkTo'=>'', 'webtitle'=>$webtitle, 'Url'=>$url_1."/".$this->convert_url($titlemenulvl2));
									$this->db->where('Lvl2ID',$this->input->post('idpost'));
									$this->db->update('menulvl2',$datasql);
								} else {
									$havesub="Tidak";
									// Hapus menu yang berkaitan dengan menulvl2 di menulvl3
									$this->db->where('Lvl2ID',$this->input->post('idpost'));
									$this->db->delete('menulvl3');
									// Hapus semua yang berkaitan dengan menulvl2 di tabel URL
									$this->db->select('*');
									$this->db->from('menulvl1');
									$this->db->where('Lvl1ID',$this->input->post('menulvl1'));
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row){
										$url_lvl1=$row->Url;
									}
									$this->db->where('Url_1',$url_lvl1);
									$this->db->where('Url_2',$this->input->post('oldpost'));
									$this->db->where('Url_3','');
									$this->db->delete('url');
									
									
									// Input Jika yang dipilih adalah halaman
									if($this->input->post('opt')=="pages"){
										// Untuk tabel URL
										$urltype="site";
										// Dapatkan Url pertama berdasarkan menulvl1
										$this->db->select('*');
										$this->db->from('menulvl1');
										$this->db->where('Lvl1ID',$this->input->post('menulvl1'));
										$result=$this->db->get();
										$result=$result->result();
										foreach($result as $row){
											$url_lvl1=$row->Url;
										}
										$url_1=$url_lvl1;
										$url_2=$this->convert_url($titlemenulvl2);
										$url_3="";
										// Filename yang dibuka adalah read
										$filename="read";
										// Tabel yang dibuka adalah tabel pages
										$table="pages";
										// Content yang dibuka di tabel
										$contentid=$this->input->post('optinpages');
										// Judul
										$webtitle=str_replace("'","&rsquo;",$this->input->post('webtitle'));
										$webtitle=str_replace('"',"&rdquo;",$webtitle);
										// Untuk tabel menulvl2
										$menuname=$titlemenulvl2;
										$link=$url_1."/".$url_2;
										$url=$url_1."/".$url_2;
									}
									
									// Input Jika yang dipilih adalah kategori
									if($this->input->post('opt')=="category"){
										// Untuk tabel URL
										$urltype="site";
										// Dapatkan Url pertama berdasarkan menulvl1
										$this->db->select('*');
										$this->db->from('menulvl1');
										$this->db->where('Lvl1ID',$this->input->post('menulvl1'));
										$result=$this->db->get();
										$result=$result->result();
										foreach($result as $row){
											$url_lvl1=$row->Url;
										}
										$url_1=$url_lvl1;
										$url_2=$this->convert_url($titlemenulvl2);
										$url_3="";
										// Filename yang dibuka adalah read
										$filename="read";
										// Tabel yang dibuka adalah tabel categories
										$table="categories";
										// Content yang dibuka di tabel
										$contentid=$this->input->post('optincat');
										// Judul
										$webtitle=str_replace("'","&rsquo;",$this->input->post('webtitle'));
										$webtitle=str_replace('"',"&rdquo;",$webtitle);
										// Untuk tabel menulvl2
										$menuname=$titlemenulvl2;
										$link=$url_1."/".$url_2;
										$url=$url_1."/".$url_2;
									}
									
									// Input Jika yang dipilih adalah registration
									if($this->input->post('opt')=="registration"){
										// Untuk tabel URL
										$urltype="site";
										// Dapatkan Url pertama berdasarkan menulvl1
										$this->db->select('*');
										$this->db->from('menulvl1');
										$this->db->where('Lvl1ID',$this->input->post('menulvl1'));
										$result=$this->db->get();
										$result=$result->result();
										foreach($result as $row){
											$url_lvl1=$row->Url;
										}
										$url_1=$url_lvl1;
										$url_2=$this->convert_url($titlemenulvl2);
										$url_3="";
										// Filename yang dibuka adalah read
										$filename="read";
										// Tabel yang dibuka adalah tabel activity
										$table="activity";
										// Content yang dibuka di tabel
										$contentid=$this->input->post('optinreg');
										// Judul
										$webtitle=str_replace("'","&rsquo;",$this->input->post('webtitle'));
										$webtitle=str_replace('"',"&rdquo;",$webtitle);
										// Untuk tabel menulvl2
										$menuname=$titlemenulvl2;
										$link=$url_1."/".$url_2;
										$url=$url_1."/".$url_2;
										
										$url_old=$this->convert_url($this->input->post('oldpost'));
										$datasql=array('UrlType'=>$urltype, 'Url_1'=>$url_1, 'Url_2'=>$url_2, 'Url_3'=>'registration', 'filename'=>'form', 'TableSrc'=>$table, 'ContentID'=>$contentid);
										$this->db->where('Url_1',$url_1);
										$this->db->where('Url_2',$url_old);
										$this->db->where('Url_3','registration');
										$this->db->update('url',$datasql);
										
										$datasql=array('UrlType'=>$urltype, 'Url_1'=>$url_1, 'Url_2'=>$url_2, 'Url_3'=>'success', 'filename'=>'success', 'TableSrc'=>$table, 'ContentID'=>$contentid);
										$this->db->where('Url_1',$url_1);
										$this->db->where('Url_2',$url_old);
										$this->db->where('Url_3','success');
										$this->db->update('url',$datasql);
										
										$datasql=array('UrlType'=>$urltype, 'Url_1'=>$url_1, 'Url_2'=>$url_2, 'Url_3'=>'letter', 'filename'=>'letter', 'TableSrc'=>$table, 'ContentID'=>$contentid);
										$this->db->where('Url_1',$url_1);
										$this->db->where('Url_2',$url_old);
										$this->db->where('Url_3','letter');
										$this->db->update('url',$datasql);
										
										$datasql=array('UrlType'=>$urltype, 'Url_1'=>$url_1, 'Url_2'=>$url_2, 'Url_3'=>'cancel', 'filename'=>'cancel', 'TableSrc'=>$table, 'ContentID'=>$contentid);
										$this->db->where('Url_1',$url_1);
										$this->db->where('Url_2',$url_old);
										$this->db->where('Url_3','cancel');
										$this->db->update('url',$datasql);
									}
									
									// Input Jika yang dipilih adalah others
									if($this->input->post('opt')=="other"){
										// Untuk tabel URL
										$urltype="site";
										// Dapatkan Url pertama berdasarkan menulvl1
										$this->db->select('*');
										$this->db->from('menulvl1');
										$this->db->where('Lvl1ID',$this->input->post('menulvl1'));
										$result=$this->db->get();
										$result=$result->result();
										foreach($result as $row){
											$url_lvl1=$row->Url;
										}
										$url_1=$url_lvl1;
										$url_2=$this->convert_url($titlemenulvl2);
										$url_3="";
										// Filename dikosongkan
										$filename="";
										// Tabel yang dibuka adalah tabel categories
										$table="";
										// Content yang dibuka di tabel
										$contentid="";
										// Judul
										$webtitle="";
										// Untuk tabel menulvl2
										$menuname=$titlemenulvl2;
										$link="http://".$this->input->post('otherlink');
										$url=$url_1."/".$url_2;
									}
									
									// Input Jika yang dipilih adalah photo
									if($this->input->post('opt')=="photo"){
										// Untuk tabel URL
										$urltype="site";
										// Dapatkan Url pertama berdasarkan menulvl1
										$this->db->select('*');
										$this->db->from('menulvl1');
										$this->db->where('Lvl1ID',$this->input->post('menulvl1'));
										$result=$this->db->get();
										$result=$result->result();
										foreach($result as $row){
											$url_lvl1=$row->Url;
										}
										$url_1=$url_lvl1;
										$url_2=$this->convert_url($titlemenulvl2);
										$url_3="";
										// Filename yang dibuka adalah Galeri Photo
										$filename="albumphoto";
										// Tabel yang dibuka adalah tabel albumphoto
										$table="albumphoto";
										// Content yang dibuka di tabel
										$contentid="";
										// Judul
										$webtitle=str_replace("'","&rsquo;",$this->input->post('webtitle'));
										$webtitle=str_replace('"',"&rdquo;",$webtitle);
										// Untuk tabel menulvl2
										$menuname=$titlemenulvl2;
										$link=$url_1."/".$url_2;
										$url=$url_1."/".$url_2;
									}
									
									// Input Jika yang dipilih adalah video
									if($this->input->post('opt')=="video"){
										// Untuk tabel URL
										$urltype="site";
										// Dapatkan Url pertama berdasarkan menulvl1
										$this->db->select('*');
										$this->db->from('menulvl1');
										$this->db->where('Lvl1ID',$this->input->post('menulvl1'));
										$result=$this->db->get();
										$result=$result->result();
										foreach($result as $row){
											$url_lvl1=$row->Url;
										}
										$url_1=$url_lvl1;
										$url_2=$this->convert_url($titlemenulvl2);
										$url_3="";
										// Filename yang dibuka adalah Galeri video
										$filename="albumvideo";
										// Tabel yang dibuka adalah tabel albumvideo
										$table="albumvideo";
										// Content yang dibuka di tabel
										$contentid="";
										// Judul
										$webtitle=str_replace("'","&rsquo;",$this->input->post('webtitle'));
										$webtitle=str_replace('"',"&rdquo;",$webtitle);
										// Untuk tabel menulvl2
										$menuname=$titlemenulvl2;
										$link=$url_1."/".$url_2;
										$url=$url_1."/".$url_2;
									}
									
									// Input Jika yang dipilih adalah consultation
									if($this->input->post('opt')=="consultation"){
										// Untuk tabel URL
										$urltype="site";
										// Dapatkan Url pertama berdasarkan menulvl1
										$this->db->select('*');
										$this->db->from('menulvl1');
										$this->db->where('Lvl1ID',$this->input->post('menulvl1'));
										$result=$this->db->get();
										$result=$result->result();
										foreach($result as $row){
											$url_lvl1=$row->Url;
										}
										$url_1=$url_lvl1;
										$url_2=$this->convert_url($titlemenulvl2);
										$url_3="";
										// Filename yang dibuka adalah Galeri consultation
										$filename="consultation";
										// Tabel yang dibuka adalah tabel consultation
										$table="consultation";
										// Content yang dibuka di tabel
										$contentid="";
										// Judul
										$webtitle=str_replace("'","&rsquo;",$this->input->post('webtitle'));
										$webtitle=str_replace('"',"&rdquo;",$webtitle);
										// Untuk tabel menulvl2
										$menuname=$titlemenulvl2;
										$link=$url_1."/".$url_2;
										$url=$url_1."/".$url_2;
									}
									
									// Cek dahulu Apakah di tabel URL ada atau tidak jika tidak ada maka gunakan insert, jika ada update!
									$url_old=$this->convert_url($this->input->post('oldpost'));
									$this->db->select('*');
									$this->db->from('url');
									$this->db->where('Url_1',$url_1);
									$this->db->where('Url_2',$url_old);
									$this->db->where('Url_3','');
									$result=$this->db->get();
									$ndata=$result->num_rows();
									if($ndata>0){
										$datasql=array('UrlType'=>$urltype, 'Url_1'=>$url_1, 'Url_2'=>$url_2, 'Url_3'=>$url_3, 'filename'=>$filename, 'TableSrc'=>$table, 'ContentID'=>$contentid);
										$this->db->where('Url_1',$url_1);
										$this->db->where('Url_2',$url_old);
										$this->db->where('Url_3','');
										$this->db->update('url',$datasql);
									} else {
										// Masuk ke dalam tabel URL
										$datasql=array('UrlType'=>$urltype, 'Url_1'=>$url_1, 'Url_2'=>$url_2, 'Url_3'=>$url_3, 'filename'=>$filename, 'TableSrc'=>$table, 'ContentID'=>$contentid);
										$this->db->set($datasql);
										$this->db->insert('url');
									}
									// Update tabel menulvl2
									$datasql=array('Lvl1ID'=>$this->input->post('menulvl1'), 'MenuName'=>$menuname, 'HaveSub'=>$havesub, 'LinkTo'=>$link, 'webtitle'=>$webtitle, 'Url'=>$url);
									$this->db->where('Lvl2ID',$this->input->post('idpost'));
									$this->db->update('menulvl2',$datasql);
								}
								
								
								echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/appereance/allmenu-level2\" />";
								exit;
							}
						}
						/* ========= UBAH MENU LEVEL 2 =========== */
					}
					
					// #menu-level3
					if($views=="menu-level3"){
						/* ========= TAMBAH MENU LEVEL 3 =========== */
						if($this->input->post('submitlevel3')){
							$this->form_validation->set_rules('titlemenulvl3', 'Menu Level 3', 'trim|required|xss_clean|callback_check_menulvl3');
							$this->form_validation->set_rules('menulvl2', 'Menu Level 2', 'trim|required|xss_clean');
							$this->form_validation->set_rules('webtitle', 'Judul Halaman', 'trim|required|xss_clean');
							
							if($this->input->post('opt')=="pages"){
								$this->form_validation->set_rules('optinpages', 'Halaman', 'trim|required|xss_clean');
							}
							
							if($this->input->post('opt')=="category"){
								$this->form_validation->set_rules('optincat', 'Kategori', 'trim|required|xss_clean');
							}
							
							if($this->input->post('opt')=="registration"){
								$this->form_validation->set_rules('optinreg', 'Kategori', 'trim|required|xss_clean');
							}
							
							if($this->input->post('opt')=="other"){
								$this->form_validation->set_rules('otherlink', 'Link Luar', 'trim|required|xss_clean');
							}
							
							if($this->form_validation->run() == FALSE){
								// Jika tidak tembus validasi, maka akan dikembalikan ke page menu-level3
								$sess_array = array();
								$sess_array = array(
									'titlemenulvl3' => $this->input->post('titlemenulvl3'),
									'menulvl2' => $this->input->post('menulvl2'),
									'webtitle' => $this->input->post('webtitle')
									);
								$this->session->set_userdata('isMenulvl3', $sess_array);
								$this->session->set_userdata('warning',validation_errors());
								echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/appereance/menu-level3\" />";
								exit;
							} else {
								// Jika tembus maka simpan ke database dengan format
								// http://www.domain.com/Url_1/Url_2/Url_3
								
								$titlemenulvl3=str_replace("'","&rsquo;",$this->input->post('titlemenulvl3'));
								$titlemenulvl3=str_replace('"',"&rdquo;",$titlemenulvl3);

								// Input Jika yang dipilih adalah halaman
								if($this->input->post('opt')=="pages"){
									// Untuk tabel URL
									$urltype="site";
									// Dapatkan Url pertama berdasarkan menulvl2
									$this->db->select('*');
									$this->db->from('menulvl2');
									$this->db->where('Lvl2ID',$this->input->post('menulvl2'));
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row){
										$url_lvl2=$row->Url;
									}
									$url=explode("/",$url_lvl2);
									$url_1=$url[0];
									$url_2=$url[1];
									$url_3=$this->convert_url($titlemenulvl3);
									// Filename yang dibuka adalah read
									$filename="read";
									// Tabel yang dibuka adalah tabel pages
									$table="pages";
									// Content yang dibuka di tabel
									$contentid=$this->input->post('optinpages');
									// Judul
									$webtitle=str_replace("'","&rsquo;",$this->input->post('webtitle'));
									$webtitle=str_replace('"',"&rdquo;",$webtitle);
									// Untuk tabel menulvl3
									$menuname=$titlemenulvl3;
									$link=$url_1."/".$url_2."/".$url_3;
									$url=$url_1."/".$url_2."/".$url_3;
								}
									
								// Input Jika yang dipilih adalah kategori
								if($this->input->post('opt')=="category"){
									// Untuk tabel URL
									$urltype="site";
									// Dapatkan Url pertama berdasarkan menulvl2
									$this->db->select('*');
									$this->db->from('menulvl2');
									$this->db->where('Lvl2ID',$this->input->post('menulvl2'));
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row){
										$url_lvl2=$row->Url;
									}
									
									$url=explode("/",$url_lvl2);
									$url_1=$url[0];
									$url_2=$url[1];
									$url_3=$this->convert_url($titlemenulvl3);
									// Filename yang dibuka adalah read
									$filename="read";
									// Tabel yang dibuka adalah tabel categories
									$table="categories";
									// Content yang dibuka di tabel
									$contentid=$this->input->post('optincat');
									// Judul
									$webtitle=str_replace("'","&rsquo;",$this->input->post('webtitle'));
									$webtitle=str_replace('"',"&rdquo;",$webtitle);
									// Untuk tabel menulvl3
									$menuname=$titlemenulvl3;
									$link=$url_1."/".$url_2."/".$url_3;
									$url=$url_1."/".$url_2."/".$url_3;
								}
								
								// Input Jika yang dipilih adalah registration
								if($this->input->post('opt')=="registration"){
									// Untuk tabel URL
									$urltype="site";
									// Dapatkan Url pertama berdasarkan menulvl2
									$this->db->select('*');
									$this->db->from('menulvl2');
									$this->db->where('Lvl2ID',$this->input->post('menulvl2'));
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row){
										$url_lvl2=$row->Url;
									}
									
									$url=explode("/",$url_lvl2);
									$url_1=$url[0];
									$url_2=$url[1];
									$url_3=$this->convert_url($titlemenulvl3);
									// Filename yang dibuka adalah read
									$filename="read";
									// Tabel yang dibuka adalah tabel activity
									$table="activity";
									// Content yang dibuka di tabel
									$contentid=$this->input->post('optinreg');
									// Judul
									$webtitle=str_replace("'","&rsquo;",$this->input->post('webtitle'));
									$webtitle=str_replace('"',"&rdquo;",$webtitle);
									// Untuk tabel menulvl3
									$menuname=$titlemenulvl3;
									$link=$url_1."/".$url_2."/".$url_3;
									$url=$url_1."/".$url_2."/".$url_3;
									// Tambahkan URL khusus Daftar
									// Masuk ke dalam tabel URL
									$datasql=array('UrlType'=>$urltype, 'Url_1'=>$url_1, 'Url_2'=>$url_2, 'Url_3'=>$url_3, 'Url_4'=>'registration', 'filename'=>'form', 'TableSrc'=>$table, 'ContentID'=>$contentid);
									$this->db->set($datasql);
									$this->db->insert('url');
									
									// Masuk ke dalam tabel URL
									$datasql=array('UrlType'=>$urltype, 'Url_1'=>$url_1, 'Url_2'=>$url_2, 'Url_3'=>$url_3, 'Url_4'=>'success', 'filename'=>'success', 'TableSrc'=>$table, 'ContentID'=>$contentid);
									$this->db->set($datasql);
									$this->db->insert('url');
									
									$datasql=array('UrlType'=>$urltype, 'Url_1'=>$url_1, 'Url_2'=>$url_2, 'Url_3'=>$url_3, 'Url_4'=>'letter', 'filename'=>'letter', 'TableSrc'=>$table, 'ContentID'=>$contentid);
									$this->db->set($datasql);
									$this->db->insert('url');
									
									$datasql=array('UrlType'=>$urltype, 'Url_1'=>$url_1, 'Url_2'=>$url_2, 'Url_3'=>$url_3, 'Url_4'=>'cancel', 'filename'=>'cancel', 'TableSrc'=>$table, 'ContentID'=>$contentid);
									$this->db->set($datasql);
									$this->db->insert('url');
								}
									
								// Input Jika yang dipilih adalah others
								if($this->input->post('opt')=="other"){
									// Untuk tabel URL
									$urltype="site";
									// Dapatkan Url pertama berdasarkan menulvl2
									$this->db->select('*');
									$this->db->from('menulvl2');
									$this->db->where('Lvl2ID',$this->input->post('menulvl2'));
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row){
										$url_lvl2=$row->Url;
									}
									
									$url=explode("/",$url_lvl2);
									$url_1=$url[0];
									$url_2=$url[1];
									$url_3=$this->convert_url($titlemenulvl3);
									// Filename dikosongkan
									$filename="";
									// Tabel yang dibuka adalah tabel categories
									$table="";
									// Content yang dibuka di tabel
									$contentid="";
									// Judul
									$webtitle="";
									// Untuk tabel menulvl3
									$menuname=$titlemenulvl3;
									$link="http://".$this->input->post('otherlink');
									$url=$url_1."/".$url_2."/".$url_3;
								}
									
								// Input Jika yang dipilih adalah photo
								if($this->input->post('opt')=="photo"){
									// Untuk tabel URL
									$urltype="site";
									// Dapatkan Url pertama berdasarkan menulvl2
									$this->db->select('*');
									$this->db->from('menulvl2');
									$this->db->where('Lvl2ID',$this->input->post('menulvl2'));
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row){
										$url_lvl2=$row->Url;
									}
									
									$url=explode("/",$url_lvl2);
									$url_1=$url[0];
									$url_2=$url[1];
									$url_3=$this->convert_url($titlemenulvl3);
									// Filename yang dibuka adalah Galeri Photo
									$filename="albumphoto";
									// Tabel yang dibuka adalah tabel albumphoto
									$table="albumphoto";
									// Content yang dibuka di tabel
									$contentid="";
									// Judul
									$webtitle=str_replace("'","&rsquo;",$this->input->post('webtitle'));
									$webtitle=str_replace('"',"&rdquo;",$webtitle);
									// Untuk tabel menulvl3
									$menuname=$titlemenulvl3;
									$link=$url_1."/".$url_2."/".$url_3;
									$url=$url_1."/".$url_2."/".$url_3;
								}
									
								// Input Jika yang dipilih adalah video
								if($this->input->post('opt')=="video"){
									// Untuk tabel URL
									$urltype="site";
									// Dapatkan Url pertama berdasarkan menulvl2
									$this->db->select('*');
									$this->db->from('menulvl2');
									$this->db->where('Lvl2ID',$this->input->post('menulvl2'));
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row){
										$url_lvl2=$row->Url;
									}
									
									$url=explode("/",$url_lvl2);
									$url_1=$url[0];
									$url_2=$url[1];
									$url_3=$this->convert_url($titlemenulvl3);
									// Filename yang dibuka adalah Galeri video
									$filename="albumvideo";
									// Tabel yang dibuka adalah tabel albumvideo
									$table="albumvideo";
									// Content yang dibuka di tabel
									$contentid="";
									// Judul
									$webtitle=str_replace("'","&rsquo;",$this->input->post('webtitle'));
									$webtitle=str_replace('"',"&rdquo;",$webtitle);
									// Untuk tabel menulvl3
									$menuname=$titlemenulvl3;
									$link=$url_1."/".$url_2."/".$url_3;
									$url=$url_1."/".$url_2."/".$url_3;
								} 
								
								// Input Jika yang dipilih adalah consultation
								if($this->input->post('opt')=="consultation"){
									// Untuk tabel URL
									$urltype="site";
									// Dapatkan Url pertama berdasarkan menulvl2
									$this->db->select('*');
									$this->db->from('menulvl2');
									$this->db->where('Lvl2ID',$this->input->post('menulvl2'));
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row){
										$url_lvl2=$row->Url;
									}
									
									$url=explode("/",$url_lvl2);
									$url_1=$url[0];
									$url_2=$url[1];
									$url_3=$this->convert_url($titlemenulvl3);
									// Filename yang dibuka adalah Galeri consultation
									$filename="consultation";
									// Tabel yang dibuka adalah tabel albumvideo
									$table="consultation";
									// Content yang dibuka di tabel
									$contentid="";
									// Judul
									$webtitle=str_replace("'","&rsquo;",$this->input->post('webtitle'));
									$webtitle=str_replace('"',"&rdquo;",$webtitle);
									// Untuk tabel menulvl3
									$menuname=$titlemenulvl3;
									$link=$url_1."/".$url_2."/".$url_3;
									$url=$url_1."/".$url_2."/".$url_3;
								} 
									
								// Masuk ke dalam tabel URL
								$datasql=array('UrlType'=>$urltype, 'Url_1'=>$url_1, 'Url_2'=>$url_2, 'Url_3'=>$url_3, 'filename'=>$filename, 'TableSrc'=>$table, 'ContentID'=>$contentid);
								$this->db->set($datasql);
								$this->db->insert('url');
								// Masuk ke dalam tabel menulvl3
								$datasql=array('Lvl2ID'=>$this->input->post('menulvl2'), 'MenuName'=>$menuname, 'LinkTo'=>$link, 'webtitle'=>$webtitle, 'Url'=>$url);
								$this->db->set($datasql);
								$this->db->insert('menulvl3'); 
								
								
								
								echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/appereance/allmenu-level3\" />";
								exit;
							}
						}
						/* ========= TAMBAH MENU LEVEL 3 =========== */
					}
					
					// #editmenulvl3
					if($views=="editmenulvl3"){
						/* ========= UBAH MENU LEVEL 3 =========== */
						if($this->input->post('submiteditlevel3')){
							$this->form_validation->set_rules('titlemenulvl3', 'Menu Level 3', 'trim|required|xss_clean|callback_check_menulvl3_edit['.$this->input->post('idpost').']');
							$this->form_validation->set_rules('menulvl2', 'Menu Level 2', 'trim|required|xss_clean');
							$this->form_validation->set_rules('webtitle', 'Judul Halaman', 'trim|required|xss_clean');
							
							if($this->input->post('opt')=="pages"){
								$this->form_validation->set_rules('optinpages', 'Halaman', 'trim|required|xss_clean');
							}
							
							if($this->input->post('opt')=="category"){
								$this->form_validation->set_rules('optincat', 'Kategori', 'trim|required|xss_clean');
							}
							
							if($this->input->post('opt')=="registration"){
								$this->form_validation->set_rules('optinreg', 'Kategori', 'trim|required|xss_clean');
							}
							
							if($this->input->post('opt')=="other"){
								$this->form_validation->set_rules('otherlink', 'Link Luar', 'trim|required|xss_clean');
							}
							
							if($this->form_validation->run() == FALSE){
								// Jika tidak tembus validasi, maka akan dikembalikan ke page menu-level3
								$this->session->set_userdata('warning',validation_errors());
								echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/appereance/editmenu-level3/?post=".$this->input->post('idpost')."\" />";
								exit;
							} else {
								// Jika tembus maka simpan ke database dengan format
								// http://www.domain.com/Url_1/Url_2/Url_3
								
								$titlemenulvl3=str_replace("'","&rsquo;",$this->input->post('titlemenulvl3'));
								$titlemenulvl3=str_replace('"',"&rdquo;",$titlemenulvl3);
									
									
								// Input Jika yang dipilih adalah halaman
								if($this->input->post('opt')=="pages"){
									// Untuk tabel URL
									$urltype="site";
									// Dapatkan Url pertama berdasarkan menulvl2
									$this->db->select('*');
									$this->db->from('menulvl2');
									$this->db->where('Lvl2ID',$this->input->post('menulvl2'));
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row){
										$url_lvl2=$row->Url;
									}
									echo $url_lvl2;
									$url=explode("/",$url_lvl2);
									$url_1=$url[0];
									$url_2=$url[1];
									$url_3=$this->convert_url($titlemenulvl3);
									// Filename yang dibuka adalah read
									$filename="read";
									// Tabel yang dibuka adalah tabel pages
									$table="pages";
									// Content yang dibuka di tabel
									$contentid=$this->input->post('optinpages');
									// Judul
									$webtitle=str_replace("'","&rsquo;",$this->input->post('webtitle'));
									$webtitle=str_replace('"',"&rdquo;",$webtitle);
									// Untuk tabel menulvl3
									$menuname=$titlemenulvl3;
									$link=$url_1."/".$url_2."/".$url_3;
									$url=$url_1."/".$url_2."/".$url_3;
								}
									
								// Input Jika yang dipilih adalah kategori
								if($this->input->post('opt')=="category"){
									// Untuk tabel URL
									$urltype="site";
									// Dapatkan Url pertama berdasarkan menulvl2
									$this->db->select('*');
									$this->db->from('menulvl2');
									$this->db->where('Lvl2ID',$this->input->post('menulvl2'));
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row){
										$url_lvl2=$row->Url;
									}
									
									$url=explode("/",$url_lvl2);
									$url_1=$url[0];
									$url_2=$url[1];
									$url_3=$this->convert_url($titlemenulvl3);
									// Filename yang dibuka adalah read
									$filename="read";
									// Tabel yang dibuka adalah tabel categories
									$table="categories";
									// Content yang dibuka di tabel
									$contentid=$this->input->post('optincat');
									// Judul
									$webtitle=str_replace("'","&rsquo;",$this->input->post('webtitle'));
									$webtitle=str_replace('"',"&rdquo;",$webtitle);
									// Untuk tabel menulvl3
									$menuname=$titlemenulvl3;
									$link=$url_1."/".$url_2."/".$url_3;
									$url=$url_1."/".$url_2."/".$url_3;
								}
								
								// Input Jika yang dipilih adalah registration
								if($this->input->post('opt')=="registration"){
									// Untuk tabel URL
									$urltype="site";
									// Dapatkan Url pertama berdasarkan menulvl2
									$this->db->select('*');
									$this->db->from('menulvl2');
									$this->db->where('Lvl2ID',$this->input->post('menulvl2'));
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row){
										$url_lvl2=$row->Url;
									}
									
									$url=explode("/",$url_lvl2);
									$url_1=$url[0];
									$url_2=$url[1];
									$url_3=$this->convert_url($titlemenulvl3);
									// Filename yang dibuka adalah read
									$filename="read";
									// Tabel yang dibuka adalah tabel activity
									$table="activity";
									// Content yang dibuka di tabel
									$contentid=$this->input->post('optinreg');
									// Judul
									$webtitle=str_replace("'","&rsquo;",$this->input->post('webtitle'));
									$webtitle=str_replace('"',"&rdquo;",$webtitle);
									// Untuk tabel menulvl3
									$menuname=$titlemenulvl3;
									$link=$url_1."/".$url_2."/".$url_3;
									$url=$url_1."/".$url_2."/".$url_3;
									
									// Cek dahulu Apakah di tabel URL ada atau tidak jika tidak ada maka gunakan insert, jika ada update!
									$url_old=$this->convert_url($this->input->post('oldpost'));
										
									$datasql=array('UrlType'=>$urltype, 'Url_1'=>$url_1, 'Url_2'=>$url_2, 'Url_3'=>$url_3, 'Url_4'=>'registration', 'filename'=>'form', 'TableSrc'=>$table, 'ContentID'=>$contentid);
									$this->db->where('Url_1',$url_1);
									$this->db->where('Url_2',$url_2);
									$this->db->where('Url_3',$url_old);
									$this->db->where('Url_4','registration');
									$this->db->update('url',$datasql);
									
									$datasql=array('UrlType'=>$urltype, 'Url_1'=>$url_1, 'Url_2'=>$url_2, 'Url_3'=>$url_3, 'Url_4'=>'success', 'filename'=>'success', 'TableSrc'=>$table, 'ContentID'=>$contentid);
									$this->db->where('Url_1',$url_1);
									$this->db->where('Url_2',$url_2);
									$this->db->where('Url_3',$url_old);
									$this->db->where('Url_4','success');
									$this->db->update('url',$datasql);
									
									$datasql=array('UrlType'=>$urltype, 'Url_1'=>$url_1, 'Url_2'=>$url_2, 'Url_3'=>$url_3, 'Url_4'=>'letter', 'filename'=>'letter', 'TableSrc'=>$table, 'ContentID'=>$contentid);
									$this->db->where('Url_1',$url_1);
									$this->db->where('Url_2',$url_2);
									$this->db->where('Url_3',$url_old);
									$this->db->where('Url_4','letter');
									$this->db->update('url',$datasql);
								}
									
								// Input Jika yang dipilih adalah others
								if($this->input->post('opt')=="other"){
									// Untuk tabel URL
									$urltype="site";
									// Dapatkan Url pertama berdasarkan menulvl2
									$this->db->select('*');
									$this->db->from('menulvl2');
									$this->db->where('Lvl2ID',$this->input->post('menulvl2'));
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row){
										$url_lvl2=$row->Url;
									}
									
									$url=explode("/",$url_lvl2);
									$url_1=$url[0];
									$url_2=$url[1];
									$url_3=$this->convert_url($titlemenulvl3);
									// Filename dikosongkan
									$filename="";
									// Tabel yang dibuka adalah tabel categories
									$table="";
									// Content yang dibuka di tabel
									$contentid="";
									// Judul
									$webtitle="";
									// Untuk tabel menulvl3
									$menuname=$titlemenulvl3;
									$link="http://".$this->input->post('otherlink');
									$url=$url_1."/".$url_2."/".$url_3;
								}
									
								// Input Jika yang dipilih adalah photo
								if($this->input->post('opt')=="photo"){
									// Untuk tabel URL
									$urltype="site";
									// Dapatkan Url pertama berdasarkan menulvl2
									$this->db->select('*');
									$this->db->from('menulvl2');
									$this->db->where('Lvl2ID',$this->input->post('menulvl2'));
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row){
										$url_lvl2=$row->Url;
									}
									
									$url=explode("/",$url_lvl2);
									$url_1=$url[0];
									$url_2=$url[1];
									$url_3=$this->convert_url($titlemenulvl3);
									// Filename yang dibuka adalah Galeri Photo
									$filename="albumphoto";
									// Tabel yang dibuka adalah tabel albumphoto
									$table="albumphoto";
									// Content yang dibuka di tabel
									$contentid="";
									// Judul
									$webtitle=str_replace("'","&rsquo;",$this->input->post('webtitle'));
									$webtitle=str_replace('"',"&rdquo;",$webtitle);
									// Untuk tabel menulvl3
									$menuname=$titlemenulvl3;
									$link=$url_1."/".$url_2."/".$url_3;
									$url=$url_1."/".$url_2."/".$url_3;
								}
									
								// Input Jika yang dipilih adalah video
								if($this->input->post('opt')=="video"){
									// Untuk tabel URL
									$urltype="site";
									// Dapatkan Url pertama berdasarkan menulvl2
									$this->db->select('*');
									$this->db->from('menulvl2');
									$this->db->where('Lvl2ID',$this->input->post('menulvl2'));
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row){
										$url_lvl2=$row->Url;
									}
									
									$url=explode("/",$url_lvl2);
									$url_1=$url[0];
									$url_2=$url[1];
									$url_3=$this->convert_url($titlemenulvl3);
									// Filename yang dibuka adalah Galeri video
									$filename="albumvideo";
									// Tabel yang dibuka adalah tabel albumvideo
									$table="albumvideo";
									// Content yang dibuka di tabel
									$contentid="";
									// Judul
									$webtitle=str_replace("'","&rsquo;",$this->input->post('webtitle'));
									$webtitle=str_replace('"',"&rdquo;",$webtitle);
									// Untuk tabel menulvl3
									$menuname=$titlemenulvl3;
									$link=$url_1."/".$url_2."/".$url_3;
									$url=$url_1."/".$url_2."/".$url_3;
								}
								
								// Input Jika yang dipilih adalah consultation
								if($this->input->post('opt')=="consultation"){
									// Untuk tabel URL
									$urltype="site";
									// Dapatkan Url pertama berdasarkan menulvl2
									$this->db->select('*');
									$this->db->from('menulvl2');
									$this->db->where('Lvl2ID',$this->input->post('menulvl2'));
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row){
										$url_lvl2=$row->Url;
									}
									
									$url=explode("/",$url_lvl2);
									$url_1=$url[0];
									$url_2=$url[1];
									$url_3=$this->convert_url($titlemenulvl3);
									// Filename yang dibuka adalah Galeri consultation
									$filename="consultation";
									// Tabel yang dibuka adalah tabel consultation
									$table="consultation";
									// Content yang dibuka di tabel
									$contentid="";
									// Judul
									$webtitle=str_replace("'","&rsquo;",$this->input->post('webtitle'));
									$webtitle=str_replace('"',"&rdquo;",$webtitle);
									// Untuk tabel menulvl3
									$menuname=$titlemenulvl3;
									$link=$url_1."/".$url_2."/".$url_3;
									$url=$url_1."/".$url_2."/".$url_3;
								}
									
								// Cek dahulu Apakah di tabel URL ada atau tidak jika tidak ada maka gunakan insert, jika ada update!
								$url_old=$this->convert_url($this->input->post('oldpost'));
									
								$datasql=array('UrlType'=>$urltype, 'Url_1'=>$url_1, 'Url_2'=>$url_2, 'Url_3'=>$url_3, 'filename'=>$filename, 'TableSrc'=>$table, 'ContentID'=>$contentid);
								$this->db->where('Url_1',$url_1);
								$this->db->where('Url_2',$url_2);
								$this->db->where('Url_3',$url_old);
								$this->db->update('url',$datasql);
									
								// Update tabel menulvl3
								$datasql=array('Lvl2ID'=>$this->input->post('menulvl2'), 'MenuName'=>$menuname, 'LinkTo'=>$link, 'webtitle'=>$webtitle, 'Url'=>$url);
								$this->db->where('Lvl3ID',$this->input->post('idpost'));
								$this->db->update('menulvl3',$datasql);
								
								
								echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/appereance/allmenu-level3\" />";
								exit;
							}
						}
						/* ========= UBAH MENU LEVEL 3 =========== */
						
					}
					
					// #add-slider
					if($views=="add-slider"){
					
							if($this->input->post('submit-file')!=""){
								$filename=$_FILES['userfile']['name'];
							
								$upload_conf = array(
									'upload_path'   => realpath('uploads/slider/'),
									'allowed_types' => 'gif|jpg|png',
									'max_size'      => '1000',
									'max_width'		=> '940',
									'max_height'	=> '300',
									);
								$this->upload->initialize($upload_conf);
							
								foreach($_FILES['userfile'] as $key=>$val){
									$i = 1;
									foreach($val as $v){
										$field_name = "file_".$i;
										$_FILES[$field_name][$key] = $v;
										$i++;   
									}
								}
								unset($_FILES['userfile']);
								$error = "";
								$success = "";
								
								foreach($_FILES as $field_name => $file){
									if ( ! $this->upload->do_upload($field_name)){
										$error = $this->upload->display_errors('<p>','</p>');
									} else {
										$datasql=array('SliderImage'=>$filename[0], 'SliderThumb'=>'thumb_'.$filename[0]);
										$this->db->set($datasql);
										$this->db->insert('slider');

										// otherwise, put the upload datas here.
										// if you want to use database, put insert query in this loop
										$upload_data = $this->upload->data();
											
										// set the resize config
										$resize_conf = array(
											// it's something like "/full/path/to/the/image.jpg" maybe
											'source_image'  => $upload_data['full_path'], 
											// and it's "/full/path/to/the/" + "thumb_" + "image.jpg
											// or you can use 'create_thumbs' => true option instead
											'new_image'     => $upload_data['file_path'].'thumb_'.$upload_data['file_name'],
											'width'         => 200,
											'height'        => 200
											);

										// initializing
										$this->image_lib->initialize($resize_conf);

										// do it!
										if ( ! $this->image_lib->resize()){
											// if got fail.
											$error2['resize'][] = $this->image_lib->display_errors('<p>','</p>');
										} else {
											// otherwise, put each upload data to an array.
											$success[] = $upload_data;
										}
											
										if($error!=""){
											$this->session->set_userdata('warning', $error);
										}
										
									}
								}
								echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/appereance/slider\" />";
								exit;
							}
						
					}
					
					// #new-tab
					if($views=="new-tab"){
						if($this->input->post('submitindex')){
							$this->form_validation->set_rules('titletab', 'Judul Index Tab', 'trim|required|xss_clean|callback_check_tab');
							$this->form_validation->set_rules('ndata', 'Banyak Data', 'xss_clean|callback_check_ndatatab');
							$this->form_validation->set_rules('category', 'Kategori', 'trim|required|xss_clean');
							
							if($this->form_validation->run() == FALSE){
								// Jika tidak tembus validasi, maka akan dikembalikan ke page menu-level1
								$sess_array = array();
								$sess_array = array(
									'titletab' => $this->input->post('titletab'),
									'category' => $this->input->post('category')
									);
								$this->session->set_userdata('isTab', $sess_array);
								$this->session->set_userdata('warning',validation_errors());
								echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/appereance/new-tab\" />";
								exit;
							} else {
								$title=str_replace("'","&rsquo;",$this->input->post('titletab'));
								$title=str_replace('"',"&rdquo;",$title);
								// Dapatkan URL dari kategori Tersebut
								$this->db->select('*');
								$this->db->from('categories');
								$this->db->where('CatID',$this->input->post('category'));
								$query=$this->db->get();
								$result=$query->result();
								foreach($result as $rowcat){
									$urlcat=$rowcat->Slugs;
								}
								
								// Masukkan data ke dalam tabel indextab
								$datasql=array('TabName'=>$title, 'CatID'=>$this->input->post('category'), 'Url'=>'index/'.$urlcat);
								$this->db->set($datasql);
								$this->db->insert('indextab');
								
								// Masuk ke dalam tabel URL
								$datasql=array('UrlType'=>'site', 'Url_1'=>'index', 'Url_2'=>$urlcat, 'Url_3'=>'', 'filename'=>'read', 'TableSrc'=>'categories', 'ContentID'=>$this->input->post('category'));
								$this->db->set($datasql);
								$this->db->insert('url');
								
								echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/appereance/index-tab\" />";
								exit;
							}
						}
					}
					
					// #edit-tab
					if($views=="edit-tab"){
						if($this->input->post('submiteditindex')){
							$this->form_validation->set_rules('titletab', 'Judul Index Tab', 'trim|required|xss_clean|callback_check_tabedit['.$this->input->post('idpost').']');
							$this->form_validation->set_rules('category', 'Kategori', 'trim|required|xss_clean');
							
							if($this->form_validation->run() == FALSE){
								$this->session->set_userdata('warning',validation_errors());
								echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/appereance/edit-tab\" />";
								exit;
							} else {
								$title=str_replace("'","&rsquo;",$this->input->post('titletab'));
								$title=str_replace('"',"&rdquo;",$title);
								
								$this->db->select('*');
								$this->db->from('categories');
								$this->db->where('CatID',$this->input->post('category'));
								$query=$this->db->get();
								$result=$query->result();
								foreach($result as $rowcat){
									$urlcat=$rowcat->Slugs;
								}
								
								$datasql=array('UrlType'=>'site', 'Url_1'=>'index', 'Url_2'=>$urlcat, 'Url_3'=>'', 'filename'=>'read', 'TableSrc'=>'categories', 'ContentID'=>$this->input->post('category'));
								$this->db->where('Url_1','index');
								$this->db->where('Url_2',$urlcat);
								$this->db->where('Url_3','');
								$this->db->update('url',$datasql);
								
								// Update data ke dalam tabel indextab
								$datasql=array('TabName'=>$title, 'CatID'=>$this->input->post('category'), 'Url'=>'index/'.$urlcat);
								$this->db->where('TabID',$this->input->post('idpost'));
								$this->db->update('indextab',$datasql);
								
								
								echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/appereance/index-tab\" />";
								exit;
							}
						}
						
					}
					
					// #general
					if($views=="general"){
						if($this->input->post('submitupdatesosmed')){
							// Update data Facebook ke dalam tabel sosmed
							$datasql=array('Url'=>$this->input->post('fblink'));
							$this->db->where('SosMed','Facebook');
							$this->db->update('sosmed',$datasql);
							
							// Update data Twitter ke dalam tabel sosmed
							$datasql=array('Url'=>$this->input->post('twtlink'));
							$this->db->where('SosMed','Twitter');
							$this->db->update('sosmed',$datasql);
							
							// Update data Google Plus ke dalam tabel sosmed
							$datasql=array('Url'=>$this->input->post('googleplus'));
							$this->db->where('SosMed','Google Plus');
							$this->db->update('sosmed',$datasql);
							
							// Update data Instagram ke dalam tabel sosmed
							$datasql=array('Url'=>$this->input->post('instagram'));
							$this->db->where('SosMed','Instagram');
							$this->db->update('sosmed',$datasql);
							
							$this->session->set_userdata('warning','Data berhasil diupdate');
							
							echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/settings/general\" />";
							exit;
						}
					}
					
					// #add-schedule
					if($views=="add-schedule"){
						if($this->input->post('submitschedule')!=""){
							$this->form_validation->set_rules('diklat', 'Diklat yang akan diselenggarakan', 'required');
							$this->form_validation->set_rules('angkatan', 'Angkatan', 'required');
							$this->form_validation->set_rules('kuota', 'Kuota Peserta', 'required|numeric|xss_clean');
							$this->form_validation->set_rules('kuotatotal', 'Kuota Total Peserta', 'required|numeric|xss_clean');
							$this->form_validation->set_message('numeric', 'Input Kuota Peserta harus berisi hanya nomor');
							$this->form_validation->set_rules('status', 'Status Diklat', 'required');
							$this->form_validation->set_rules('tempat', 'Tempat Pelaksanaan Diklat', 'required');
							
							if($this->form_validation->run() == FALSE){
								// Jika tidak tembus validasi, maka akan dikembalikan ke page sebelumnya
								$sess_array = array();
								$sess_array = array(
									'diklat' => $this->input->post('diklat')
									);
								$this->session->set_userdata('isSchedule', $sess_array);
								$this->session->set_userdata('warning',validation_errors());
								echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/bkpm/add-schedule\" />";
								exit;
							} else {
								if($this->input->post('angkatan')=="I"){ $aktnum="1"; }
								if($this->input->post('angkatan')=="II"){ $aktnum="2"; }
								if($this->input->post('angkatan')=="III"){ $aktnum="3"; }
								if($this->input->post('angkatan')=="IV"){ $aktnum="4"; }
								if($this->input->post('angkatan')=="V"){ $aktnum="5"; }
								if($this->input->post('angkatan')=="VI"){ $aktnum="6"; }
								if($this->input->post('angkatan')=="VII"){ $aktnum="7"; }
								if($this->input->post('angkatan')=="VIII"){ $aktnum="8"; }
								if($this->input->post('angkatan')=="IX"){ $aktnum="9"; }
								if($this->input->post('angkatan')=="X"){ $aktnum="10"; }
								if($this->input->post('angkatan')=="XI"){ $aktnum="11"; }
								if($this->input->post('angkatan')=="XII"){ $aktnum="12"; }
								if($this->input->post('angkatan')=="XIII"){ $aktnum="13"; }
								if($this->input->post('angkatan')=="XIV"){ $aktnum="14"; }
								if($this->input->post('angkatan')=="XV"){ $aktnum="15"; }
								if($this->input->post('angkatan')=="XVI"){ $aktnum="16"; }
								if($this->input->post('angkatan')=="XVII"){ $aktnum="17"; }
								if($this->input->post('angkatan')=="XVIII"){ $aktnum="18"; }
								if($this->input->post('angkatan')=="XIX"){ $aktnum="19"; }
								if($this->input->post('angkatan')=="XX"){ $aktnum="20"; }
								// Masukkan data ke dalam tabel jadwal
								$datasql=array('DikID'=>$this->input->post('diklat'), 'ClassDik'=>$this->input->post('angkatan'), 'StartDate'=>$this->input->post('startyear')."-".$this->input->post('startmonth')."-".$this->input->post('startdate'), 'EndDate'=>$this->input->post('endyear')."-".$this->input->post('endmonth')."-".$this->input->post('enddate'), 'Quota'=>$this->input->post('kuota'), 'Status'=>$this->input->post('status'), 'Place'=>$this->input->post('tempat'), 'TotalQuota'=>$this->input->post('kuotatotal'), 'tipe'=>$this->input->post('tipe'), 'AngkatanNumber'=>$aktnum);
								$this->db->set($datasql);
								$this->db->insert('jadwal');
									
								echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/bkpm/all-schedule\" />";
								exit;
							}
						}
					}
					
					// #edit-schedule
					if($views=="bkpm-edit"){
						if($this->input->post('submiteditschedule')!=""){
							$this->form_validation->set_rules('diklat', 'Diklat yang akan diselenggarakan', 'required');
							$this->form_validation->set_rules('angkatan', 'Angkatan', 'required');
							$this->form_validation->set_rules('kuota', 'Kuota Peserta', 'required|numeric|xss_clean');
							$this->form_validation->set_message('numeric', 'Input Kuota Peserta harus berisi hanya nomor');
							$this->form_validation->set_rules('status', 'Status Diklat', 'required');
							$this->form_validation->set_rules('tempat', 'Tempat Pelaksanaan Diklat', 'required');
							
							if($this->form_validation->run() == FALSE){
								// Jika tidak tembus validasi, maka akan dikembalikan ke page sebelumnya
								$this->session->set_userdata('warning',validation_errors());
								echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/bkpm/bkpm-edit/?post=".$this->input->post('idpost')."\" />";
								exit;
							} else {
								if($this->input->post('angkatan')=="I"){ $aktnum="1"; }
								if($this->input->post('angkatan')=="II"){ $aktnum="2"; }
								if($this->input->post('angkatan')=="III"){ $aktnum="3"; }
								if($this->input->post('angkatan')=="IV"){ $aktnum="4"; }
								if($this->input->post('angkatan')=="V"){ $aktnum="5"; }
								if($this->input->post('angkatan')=="VI"){ $aktnum="6"; }
								if($this->input->post('angkatan')=="VII"){ $aktnum="7"; }
								if($this->input->post('angkatan')=="VIII"){ $aktnum="8"; }
								if($this->input->post('angkatan')=="IX"){ $aktnum="9"; }
								if($this->input->post('angkatan')=="X"){ $aktnum="10"; }
								if($this->input->post('angkatan')=="XI"){ $aktnum="11"; }
								if($this->input->post('angkatan')=="XII"){ $aktnum="12"; }
								if($this->input->post('angkatan')=="XIII"){ $aktnum="13"; }
								if($this->input->post('angkatan')=="XIV"){ $aktnum="14"; }
								if($this->input->post('angkatan')=="XV"){ $aktnum="15"; }
								if($this->input->post('angkatan')=="XVI"){ $aktnum="16"; }
								if($this->input->post('angkatan')=="XVII"){ $aktnum="17"; }
								if($this->input->post('angkatan')=="XVIII"){ $aktnum="18"; }
								if($this->input->post('angkatan')=="XIX"){ $aktnum="19"; }
								if($this->input->post('angkatan')=="XX"){ $aktnum="20"; }
								// Masukkan data ke dalam tabel jadwal
								$datasql=array('DikID'=>$this->input->post('diklat'), 'ClassDik'=>$this->input->post('angkatan'), 'StartDate'=>$this->input->post('startyear')."-".$this->input->post('startmonth')."-".$this->input->post('startdate'), 'EndDate'=>$this->input->post('endyear')."-".$this->input->post('endmonth')."-".$this->input->post('enddate'), 'Quota'=>$this->input->post('kuota'), 'Status'=>$this->input->post('status'), 'Place'=>$this->input->post('tempat'), 'TotalQuota'=>$this->input->post('kuotatotal'), 'tipe'=>$this->input->post('tipe'), 'AngkatanNumber'=>$aktnum);
								$this->db->where('SchedID',$this->input->post('idpost'));
								$this->db->update('jadwal',$datasql);
									
								echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/bkpm/all-schedule\" />";
								exit;
							}
						}
					}
					
					if($views=="consultation-cpanel"){
						if($this->input->post('submitconsultation')){
							$this->form_validation->set_rules('message', 'Jawaban Konsultasi', 'trim|required|xss_clean');
							if($this->form_validation->run() == FALSE){
								$this->session->set_userdata('warning',validation_errors());
								echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/bkpm/consultation\" />";
								exit;
							} else {
								$message=str_replace("'","&rsquo;",$this->input->post('message'));
								$message=str_replace('"',"&rdquo;",$message);
								
								$datasql=array('Nama'=>'Admin Website Pusdiklat BKPM', 'email'=>'pusdiklat@bkpm.go.id', 'Instansi'=>'Pusdiklat BKPM', 'Statement'=>$this->input->post('message'), 'RecorDate'=>date("Y-m-d h:i:s A"), 'ClassUser'=>'admin');
								$this->db->set($datasql);
								$this->db->insert('consultation');
									
								echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel/bkpm/consultation\" />";
								exit;
							}
						}
					}
					
					if(!$this->session->userdata('isLoggedIn')){
						$this->template->set('navigation',$views);
						$this->template->load('template-login','cpanel/'.$views);
					} else {
					
						$this->template->set('navigation',$views);
						$this->template->set('jquery',$jquery);
						/* Load File ke dalam template */
						$this->template->load('template-admin',"cpanel/".$views);
					}
					
				}
				
				/* ========= Syntax yang berkaitan dengan CPANEL ==== */
				
				/* ========= Syntax yang berkaitan dengan WEBSITE ==== */
				if($row['UrlType']=="site"){
					//$this->load->view('underconstruction');
					// New Code for Consultation
					if($views=="forum"){
						if($this->input->post('submit')){
							// Hindari karakter ' dan "
							$nama=str_replace("'","&rsquo;",$this->input->post('nama'));
							$nama=str_replace('"',"&rdquo;",$nama);
							
							$email=str_replace("'","&rsquo;",$this->input->post('email'));
							$email=str_replace('"',"&rdquo;",$email);
							
							$row=@mysql_fetch_array(@mysql_query("SELECT * FROM dept WHERE id='".$this->input->post('instansi')."'"));
							$instansi=$row['dept'];
							
							$officecity=$this->input->post('wil_kantor');
	
							$rowcity=@mysql_fetch_array(@mysql_query("SELECT * FROM city WHERE id='".$officecity."'"));
							if($this->input->post('daerahkantor')=="kota"){$wilayah="Kota ".$rowcity['city']; }
							if($this->input->post('daerahkantor')=="kabupaten"){$wilayah="Kabupaten ".$rowcity['city']; }
							if($this->input->post('daerahkantor')=="provinsi"){$wilayah="Provinsi ".$officecity; }
							if($this->input->post('daerahkantor')=="pusat"){$wilayah="Pusat"; }
							$instansi=$instansi." ".$wilayah;
							
							$question=str_replace("'","&rsquo;",$this->input->post('message'));
							$question=str_replace('"',"&rdquo;",$question);
							
							$title_forum=str_replace("'","&rsquo;",$this->input->post('title'));
							$title_forum=str_replace('"',"&rdquo;",$title_forum);
							
							$datasql=array('Nama'=>$nama, 'email'=>$email, 'Instansi'=>$instansi, 'Question'=>$question, 'RecordDate'=>date("Y-m-d h:i:s A"), 'title'=>$title_forum, 'Category'=>$this->input->post('kategori'));
							$this->db->set($datasql);
							$this->db->insert('question');
							
							echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."forum/consultation-success\" />";
							exit;
						}
					}
					
					
					if($views=="consultation"){
						if($this->input->post('submit')){
							// Input data ke tabel consultation
							$row=@mysql_fetch_array(@mysql_query("SELECT * FROM dept WHERE id='".$this->input->post('instansi')."'"));
							$instansi=$row['dept'];
			
							$officecity=$this->input->post('wil_kantor');
	
							$rowcity=@mysql_fetch_array(@mysql_query("SELECT * FROM city WHERE id='".$officecity."'"));
							if($this->input->post('daerahkantor')=="kota"){$wilayah="Kota;".$rowcity['city']; }
							if($this->input->post('daerahkantor')=="kabupaten"){$wilayah="Kabupaten ".$rowcity['city']; }
							if($this->input->post('daerahkantor')=="provinsi"){$wilayah="Provinsi ".$officecity; }
							if($this->input->post('daerahkantor')=="pusat"){$wilayah="Pusat"; }
							$instansi=$instansi." ".$wilayah;
							
							
							$datasql=array('Nama'=>$this->input->post('nama'), 'email'=>$this->input->post('email'), 'Instansi'=>$instansi, 'Statement'=>$this->input->post('message'), 'RecorDate'=>date("Y-m-d h:i:s A"), 'ClassUser'=>'user');
							$this->db->set($datasql);
							$this->db->insert('consultation');
							
							echo "<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."forum/consultation-success\" />";
							exit;
						}
					}
					
					$data['table']=$row['TableSrc'];
					$data['contentid']=$row['ContentID'];
					$data['url1']=$row['Url_1'];
					$data['url2']=$row['Url_2'];
					$data['url3']=$row['Url_3'];
					$data['url4']=$row['Url_4'];
					
					if($views=="home"){ $this->template->set('jsactive','home'); }
					elseif($views=="galleryshow"){ $this->template->set('jsactive','galleryshow'); } 
					else { $this->template->set('jsactive',''); }
					$this->template->load('template-site','site/'.$views,$data);
				}
				/* ========= Syntax yang berkaitan dengan WEBSITE ==== */
				
				/* ========= Syntax yang berkaitan dengan SIAD ==== */
				if($row['UrlType']=="siad"){
					
					$data['actname']='';
					$data['deptname']='';
					$data['lectname']='';
					$data['positionname']='';
					$data['gradename']='';
					$data['username']='';
					$data['realname']='';
					$data['acttipe']='';
					
					if($this->session->userdata('isLoggedIn')==FALSE && $views!="login"){
						echo"<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."siad/login\" />";
						exit;
					} 
					
					// #login-form-cpanel#
					if($views=="login"){
						if($this->session->userdata('isLoggedIn')){
							//Jika session ada maka redirect ke siad
							echo"<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."siad\" />";
							exit;
						} else {
							$data['username']=$this->input->post('username',true);
							$data['password']=$this->input->post('password',true);
										
							$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
							$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');
										
							if($this->form_validation->run() == FALSE){
								$this->session->set_userdata('warning',validation_errors());
							} else {
								//Bila sudah login tetapi coba akses menu login akan diredirect ke Dashboard
								echo"<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."siad\" />";
							}
						}
					}
					
					if($views=="adddiklat1" || $views=="adddiklat2"){
						if($this->input->post('diklat1-submit')){
							//Cek terlebih dahulu apakah sudah ada diklat tersebut?
							$this->db->select('*');
							$this->db->from('bkpm_events');
							$this->db->where('bkpm_events',$this->input->post('diklat'));
							$this->db->where('angkatan',$this->input->post('angkatan'));
							$this->db->where('thnakt',$this->input->post('tahun'));	
							$result=$this->db->get();
							$ndata=$result->num_rows();
							
							if($ndata>0){
								$this->session->set_userdata('warning','Diklat yang Anda masukkan sudah Ada, silahkan gunakan nama lain!');
								if($views=="adddiklat1"){
									echo"<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."siad/input/berkesinambungan/adddiklat\" />";
								} else {
									echo"<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."siad/input/insidental/adddiklat\" />";
								}
								exit;
							} else {
								$i=1;
								$lecture="";
								while($i<=$this->input->post('nlecture')){
									$lecture=$lecture.$this->input->post('lecture'.$i).";";
									$i++;
								}
								
								$kota=$this->input->post('kota').$this->input->post('kabupaten').$this->input->post('kota2');
								
								$this->db->select('*');
								$this->db->from('activity');
								$this->db->where('id',$this->input->post('diklat'));
								$result=$this->db->get();
								$result=$result->result();
								foreach($result as $row_diklat){
									$syarat=explode(';',$row_diklat->syarat);
								}
								$i=1;
								$showsyarat="";
								while($syarat[$i]!=""){
									if($syarat[$i]==0){
										$showsyarat=$showsyarat."Tidak Ada";
									} else {
										$this->db->select('*');
										$this->db->from('activity');
										$this->db->where('id',$syarat[$i]);
										$result=$this->db->get();
										$result=$result->result();
										foreach($result as $row_diklat2){
											$showsyarat=$showsyarat.$row_diklat2->activity.";";
										}
									}
									$i++;
								}
								
								
								if($kota==1000){
									$provinsi="Tidak ada info";
								}
								
								if($kota!=1000){
									$this->db->select('*');
									$this->db->from('city');
									$this->db->where('id',$kota);
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row_prov){
										$provinsi=$row_prov->provinsi;
									}
								}
								
								$time_do=$this->input->post('startdate')." ".$this->input->post('startmonth')." ".$this->input->post('startyear')." S/D ".$this->input->post('enddate')." ".$this->input->post('endmonth')." ".$this->input->post('endyear');
								
								// Masukkan data ke database bkpm_events
								$datasql=array('thnakt'=>$this->input->post('tahun'), 'angkatan'=>$this->input->post('angkatan'), 'bkpm_events'=>$this->input->post('diklat'), 'time_do'=>$time_do, 'place_do'=>$this->input->post('place'), 'city'=>$kota, 'province'=>$provinsi, 'lecture'=>$lecture, 'pra_syarat'=>$showsyarat);
								$this->db->set($datasql);
								$this->db->insert('bkpm_events');
								
								$sess_array = array();
								$sess_array = array(
									'diklat' => $this->input->post('diklat'),
									'tahun' => $this->input->post('tahun'),
									'angkatan' => $this->input->post('angkatan')
									);
									$this->session->set_userdata('addMemberDiklat1', $sess_array);
								if($views=="adddiklat1"){
									echo"<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."siad/input/berkesinambungan/addmember\" />";
								} else {
									echo"<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."siad/input/insidental/addmember\" />";
								}
								exit;
							}
						}
					}
					
					if($views=="addmember1"){
						if($this->input->post('member-submit')){
							$nip=$this->input->post('nip_part1')." ".$this->input->post('nip_part2')." ".$this->input->post('nip_part3')." ".$this->input->post('nip_part4');							
							
							$provhome="";
							$homecity=$this->input->post('homecity');
							if(is_numeric($homecity)){
								$this->db->select('*');
								$this->db->from('city');
								$this->db->where('id',$homecity);
								$result=$this->db->get();
								$result=$result->result();
								foreach($result as $row_city){
									$provhome=$row_city->provinsi;
								}
							} else {
								$this->db->select('*');
								$this->db->from('datapeserta');
								$this->db->where('nip',$nip);
								$result=$this->db->get();
								$ndatapeserta=$result->num_rows();
								
								$this->db->select('*');
								$this->db->from('dataalumni');
								$this->db->where('nip',$nip);
								$result=$this->db->get();
								$ndataalumni=$result->num_rows();
								
								if($ndatapeserta!=0 && $ndataalumni !=0){
									$this->db->select('*');
									$this->db->from('datapeserta');
									$this->db->where('nip',$nip);
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row_city){
										$homecity=$row_city->rumah_kota;
									}
								}
								
								if($ndatapeserta!=0 && $ndataalumni ==0){
									$this->db->select('*');
									$this->db->from('datapeserta');
									$this->db->where('nip',$nip);
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row_city){
										$homecity=$row_city->rumah_kota;
									}
								}
								
								if($ndatapeserta==0 && $ndataalumni !=0){
									$this->db->select('*');
									$this->db->from('dataalumni');
									$this->db->where('nip',$nip);
									$result=$this->db->get();
									$result=$result->result();
									foreach($result as $row_city){
										$homecity=$row_city->rumah_kota;
									}
								}
								
								$this->db->select('*');
								$this->db->from('city');
								$this->db->where('id',$homecity);
								$result=$this->db->get();
								$result=$result->result();
								foreach($result as $row_city){
									$provhome=$row_city->provinsi;
								}
								
							}
							if($provhome==""){
								$homecity="1000";
								$provhome="Tidak Ada Info";
							}
							$telp=$this->input->post('kodearea')."-".$this->input->post('notelp');
							$nhp=$this->input->post('ncontact');
							$nohp="";
							$i=1;
							while($i<=$nhp){
								$nohp=$nohp.$this->input->post('contact'.$i).";";
								$i++;
							}
							
							// Office 1 untuk wilayah
							$officecity=$this->input->post('wil_kantor');
						
							if(is_numeric($officecity)){
								$this->db->select('*');
								$this->db->from('city');
								$this->db->where('id',$officecity);
								$result=$this->db->get();
								$result=$result->result();
								foreach($result as $row_city){
									$city=$row_city->city;
								}
								if($this->input->post('daerahkantor')=="kota"){$wilayah="Kota;".$city.";"; }
								if($this->input->post('daerahkantor')=="kabupaten"){$wilayah="Kabupaten;".$city.";"; }
								if($this->input->post('daerahkantor')=="provinsi"){$wilayah="Provinsi;".$officecity.";"; }
								if($this->input->post('daerahkantor')=="pusat"){$wilayah="pusat;pusat;"; }
							} else {
								if($this->input->post('daerahkantor')=="kota"){$wilayah="Kota;".$officecity.";"; }
								if($this->input->post('daerahkantor')=="kabupaten"){$wilayah="Kabupaten;".$officecity.";"; }
								if($this->input->post('daerahkantor')=="provinsi"){$wilayah="Provinsi;".$officecity.";"; }
								if($this->input->post('daerahkantor')=="pusat"){$wilayah="pusat;pusat;"; }
							}
							
							// Ini sudah OK
							// Office2 untuk kota kantor
							$officecity2="";
							if($this->input->post('officecity2')!=""){
								$officecity2=$this->input->post('officecity2');
								$this->db->select('*');
								$this->db->from('city');
								$this->db->where('id',$officecity2);
								$result=$this->db->get();
								$result=$result->result();
								foreach($result as $row_city){
									$officeprovx=$row_city->provinsi;
								}
							} else {
								if($this->input->post('officecity2')==""){
									$officecity2="";
								} else {
									$row000=@mysql_fetch_array(@mysql_query("SELECT * FROM dataalumni WHERE nip='".$nip."'"));
									$office=explode(';',$row000['kantor_kota']);
									$i=1;
									while($office[$i]!=""){
										$i++;
									}
									$officecity2=$office[$i-1];
								}
								
								
								if($officecity2==""){ 
									$officecity2="1000";
									$officeprovx="Tidak Ada Info";
								} else {
									$prov2=@mysql_fetch_array(@mysql_query("SELECT * FROM city WHERE id='".$officecity2."'"));
									$officeprovx=$prov2['provinsi'];
								}

							}
							
							if($this->input->post('jabatan')==""){ $jabatan="1000"; } else { $jabatan=$this->input->post('jabatan'); }
							if($this->input->post('pangkat')==""){ $pangkat="1000"; } else { $pangkat=$this->input->post('pangkat'); }
							if($this->input->post('bagian')==""){ $bagian=" "; } else { $bagian=$this->input->post('bagian'); }
							if($this->input->post('addresoffice')==""){ $addresoffice=" "; } else { $addresoffice=$this->input->post('addresoffice'); }
							if($this->input->post('website')==""){ $website=" "; } else { $website=$this->input->post('website'); }
							
							
							$telp2=$this->input->post('kodeareaoffice')."-".$this->input->post('notelpoffice');
							$fax=$this->input->post('kodeareaofficefax')."-".$this->input->post('notelpofficefax');
							
							if($_SESSION['member']=="new"){
								
								$datasql=array(
									'nama'=>$this->input->post('nama'), 
									'nip'=>$nip, 
									'rumah'=>$this->input->post('addreshome'), 
									'rumah_kota'=>$homecity, 
									'rumah_prov'=>$provhome, 
									'rumah_telp'=>$telp, 
									'hp_telp'=>$nohp, 
									'email'=>$this->input->post('email'), 
									'facebook'=>$this->input->post('facebook'), 
									'jabatan'=>"0;".$jabatan.";", 
									'instansi'=>"0;".$this->input->post('instansi').";", 
									'wilayah'=>$wilayah, 
									'bagian'=>"0;".$bagian.";", 
									'pangkat'=>"0;".$pangkat.";", 
									'kantor_alamat'=>"0;".$addresoffice.";", 
									'kantor_kota'=>"0;".$officecity2.";", 
									'kantor_prov'=>"0;".$officeprovx.";", 
									'kantor_telp'=>"0;".$telp2.";", 
									'fax'=>"0;".$fax.";", 
									'ttl'=>$this->input->post('birthplace').";".$this->input->post('birthdate')." ".$this->input->post('birthmonth')." ".$this->input->post('birthyear'), 
									'diklat'=>"0;".$this->input->post('diklat').";", 
									'angkatan'=>"0;".$this->input->post('angkatan').";",
									'tahun'=>"0;".$this->input->post('tahun').";",
									'website'=>"0;".$website.";");
								$this->db->set($datasql);
								$this->db->insert('dataalumni');
							}
							
							if($_SESSION['member']=="update"){
								
								$datasql=array(
									'nama'=>$this->input->post('nama'), 
									'rumah'=>$this->input->post('addreshome'), 
									'rumah_kota'=>$homecity, 
									'rumah_prov'=>$provhome, 
									'rumah_telp'=>$telp, 
									'hp_telp'=>$nohp, 
									'email'=>$this->input->post('email'), 
									'facebook'=>$this->input->post('facebook'),
									'ttl'=>$this->input->post('birthplace').";".$this->input->post('birthdate')." ".$this->input->post('birthmonth')." ".$this->input->post('birthyear'));
								$this->db->where('nip',$nip);
								$this->db->update('dataalumni',$datasql);
								
								$query=@mysql_query("UPDATE dataalumni SET jabatan=CONCAT(jabatan,'".$jabatan.";'), instansi=CONCAT(instansi,'".$this->input->post('instansi').";'), wilayah=CONCAT(wilayah,'".$wilayah."'), bagian=CONCAT(bagian,'".$bagian.";'), pangkat=CONCAT(pangkat,'".$pangkat.";'), kantor_alamat=CONCAT(kantor_alamat,'".$addresoffice.";'), kantor_kota=CONCAT(kantor_kota,'".$officecity2.";'), kantor_prov=CONCAT(kantor_prov,'".$officeprovx.";'), kantor_telp=CONCAT(kantor_telp,'".$telp2.";'), fax=CONCAT(fax,'".$fax.";'), diklat=CONCAT(diklat,'".$this->input->post('diklat').";'), angkatan=CONCAT(angkatan,'".$this->input->post('angkatan').";'), tahun=CONCAT(tahun,'".$this->input->post('tahun').";'), website=CONCAT(website,'".$website.";') WHERE nip='".$nip."'");
								
							}
 						}
					}
					
					
					if($views=="edit" || $views=="editdata"){
						if($this->input->post('member-update')){
							//echo 'member-update';
							// parameter yang tak perlu diubah
							$nip=$this->input->post('nip_part1')." ".$this->input->post('nip_part2')." ".$this->input->post('nip_part3')." ".$this->input->post('nip_part4');
							$ttl=$this->input->post('birthplace').";".$this->input->post('birthdate')." ".$this->input->post('birthmonth')." ".$this->input->post('birthyear');
							$rumah=$this->input->post('addreshome');
							
							$homecity=$this->input->post('homecity');
							if(is_numeric($homecity)){
								$this->db->select('*');
								$this->db->from('city');
								$this->db->where('id',$homecity);
								$result=$this->db->get();
								$result=$result->result();
								foreach($result as $row_city){
									$provhome=$row_city->provinsi;
								}
							} else {
								$this->db->select('*');
								$this->db->from('dataalumni');
								$this->db->where('nip',$nip);
								$result=$this->db->get();
								$result=$result->result();
								foreach($result as $row_alumni){
									$homecity=$row_alumni->rumah_kota;
								}
								
								$this->db->select('*');
								$this->db->from('city');
								$this->db->where('id',$homecity);
								$result=$this->db->get();
								$result=$result->result();
								foreach($result as $row_city){
									$provhome=$row_city->provinsi;
								}
	
							}
							$telp=$this->input->post('kodearea')."-".$this->input->post('notelp');
							$nhp=$this->input->post('ncontact');
							$nohp="";
							$i=1;
							while($i<=$nhp){
								$nohp=$nohp.$_POST['contact'.$i].";";
								$i++;
							}
   
							$datasql=array(
									'nip'=>$nip, 
									'nama'=>$this->input->post('nama'), 
									'rumah'=>$this->input->post('addreshome'), 
									'rumah_kota'=>$homecity, 
									'rumah_prov'=>$provhome, 
									'rumah_telp'=>$telp, 
									'hp_telp'=>$nohp, 
									'email'=>$this->input->post('email'), 
									'facebook'=>$this->input->post('facebook'),
									'ttl'=>$this->input->post('birthplace').";".$this->input->post('birthdate')." ".$this->input->post('birthmonth')." ".$this->input->post('birthyear'));
							$this->db->where('id',$this->input->post('idmember'));
							$this->db->update('dataalumni',$datasql);
							   
							//Update data instansi
							// Office 1 untuk wilayah
							$officecity=$this->input->post('wil_kantor');
	
							if(is_numeric($officecity)){
								$this->db->select('*');
								$this->db->from('city');
								$this->db->where('id',$officecity);
								$result=$this->db->get();
								$result=$result->result();
								foreach($result as $row_city){
									$cityoff=$row_city->city;
								}
								if($this->input->post('daerahkantor')=="kota"){$wilayah="Kota;".$cityoff.";"; }
								if($this->input->post('daerahkantor')=="kabupaten"){$wilayah="Kabupaten;".$cityoff.";"; }
								if($this->input->post('daerahkantor')=="provinsi"){$wilayah="Provinsi;".$officecity.";"; }
								if($this->input->post('daerahkantor')=="pusat"){$wilayah="pusat;pusat;"; }
							} else {
								if($this->input->post('daerahkantor')=="kota"){$wilayah="Kota;".$officecity.";"; }
								if($this->input->post('daerahkantor')=="kabupaten"){$wilayah="Kabupaten;".$officecity.";"; }
								if($this->input->post('daerahkantor')=="provinsi"){$wilayah="Provinsi;".$officecity.";"; }
								if($this->input->post('daerahkantor')=="pusat"){$wilayah="pusat;pusat;"; }
							}
	
							// Office2 untuk kota kantor
							$officecity2=$this->input->post('officecity2');
							if(is_numeric($officecity2)){
								$this->db->select('*');
								$this->db->from('city');
								$this->db->where('id',$officecity2);
								$result=$this->db->get();
								$result=$result->result();
								foreach($result as $row_city){
									$prov2=$row_city->provinsi;
								}
							} else {
								$this->db->select('*');
								$this->db->from('dataalumni');
								$this->db->where('nip',$nip);
								$result=$this->db->get();
								$result=$result->result();
								foreach($result as $row_alumni){
									$kantorkota=$row_alumni->kantor_kota;
								}
								$office=explode(';',$kantorkota);
								$i=1;
								while($office[$i]!=""){
									$i++;
								}
								$officecity2=$office[$i-1];
								$this->db->select('*');
								$this->db->from('city');
								$this->db->where('id',$officecity2);
								$result=$this->db->get();
								$result=$result->result();
								foreach($result as $row_city){
									$prov2=$row_city->provinsi;
								}	
							}
   
							$telp2=$this->input->post('kodeareaoffice')."-".$this->input->post('notelpoffice');
							$fax=$this->input->post('kodeareaofficefax')."-".$this->input->post('notelpofficefax');
	
							$this->db->select('*');
							$this->db->from('dataalumni');
							$this->db->where('id',$this->input->post('idmember'));
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row_alumni){
								//$diklat=$row_alumni->diklat;
							}	
							
							$str_diklat=explode(";",$row_alumni->diklat);
							$i=1;
							$diklat="";
							while($str_diklat[$i]!=""){
								if($str_diklat[$i]==$this->input->post('iddiklat')){
									$pos=$i;
								}
								$i++;
							}
						   
						   $str_jabatan=explode(";",$row_alumni->jabatan);
						   $jabatan="0;";
						   $i=1;
						   while($str_jabatan[$i]!=""){
								if($i==$pos){
									$jabatan=$jabatan.$this->input->post('jabatan').";";
								} else {
									$jabatan=$jabatan.$str_jabatan[$i].";";
								}
								$i++;
						   }
   
						   $str_instansi=explode(";",$row_alumni->instansi);
						   $instansi="0;";
						   $i=1;
						   while($str_instansi[$i]!=""){
								if($i==$pos){
									$instansi=$instansi.$this->input->post('instansi').";";
								} else {
									$instansi=$instansi.$str_instansi[$i].";";
								}
								$i++;
						   }
   
   
						   if($this->input->post('bagian')=="") { $postbagian=" "; } else { $postbagian=$this->input->post('bagian'); }
						   
						   $str_bagian=explode(";",$row_alumni->bagian);
						   $bagian="0;";
						   $i=1;
						   while($str_bagian[$i]!=""){
								if($i==$pos){
									$bagian=$bagian.$postbagian.";";
								} else {
									$bagian=$bagian.$str_bagian[$i].";";
								}
								$i++;
						   }
						   
						   $str_pangkat=explode(";",$row_alumni->pangkat);
						   $pangkat="0;";
						   $i=1;
						   while($str_pangkat[$i]!=""){
								if($i==$pos){
									$pangkat=$pangkat.$this->input->post('pangkat').";";
								} else {
									$pangkat=$pangkat.$str_pangkat[$i].";";
								}
								$i++;
						   }
						   
						   if($this->input->post('addresoffice')=="") { $postaddresoffice=" "; } else { $postaddresoffice=$this->input->post('addresoffice'); }
						   $str_office=explode(";",$row_alumni->kantor_alamat);
						   $office="0;";
						   $i=1;
						   while($str_office[$i]!=""){
								if($i==$pos){
									$office=$office.$postaddresoffice.";";
								} else {
									$office=$office.$str_office[$i].";";
								}
								$i++;
						   }
						   
						   $str_officecity=explode(";",$row_alumni->kantor_kota);
						   $officecity3="0;";
						   $i=1;
						   while($str_officecity[$i]!=""){
								if($i==$pos){
									$officecity3=$officecity3.$officecity2.";";
								} else {
									$officecity3=$officecity3.$str_office[$i].";";
								}
								$i++;
						   }
						   
						   $str_officeprov=explode(";",$row_alumni->kantor_prov);
						   $officeprov="0;";
						   $i=1;
						   while($str_officeprov[$i]!=""){
								if($i==$pos){
									$officeprov=$officeprov.$prov2.";";
								} else {
									$officeprov=$officeprov.$str_officeprov[$i].";";
								}
								$i++;
						   }
						   
						   if($row_alumni->kantor_telp!="0;" || $row_alumni->kantor_telp!=""){
							   $str_officephone=explode(";",$row_alumni->kantor_telp);
							   $officephone="0;";
							   $i=1;
							   while($str_officephone[$i]!=""){
									if($i==$pos){
										//echo "kosong";
										$officephone=$officephone.$telp2.";";
									} else {
										//echo "isi";
										$officephone=$officephone.$str_officephone[$i].";";
									}
									$i++;
							   }
						   } else {
								$officephone="0;";
								$officephone=$officephone.$telp2.";";
						   }
						   
						   $str_officefax=explode(";",$row_alumni->fax);
						   $officefax="0;";
						   $i=1;
						   while($str_officefax[$i]!=""){
								if($i==$pos){
									$officefax=$officefax.$fax.";";
								} else {
									$officefax=$officefax.$str_officefax[$i].";";
								}
								$i++;
						   }
						   
						   
						   if($this->input->post('website')=="") { $postweb=" "; } else { $postweb=$this->input->post('website'); }
						   $str_officeweb=explode(";",$row_alumni->website);
						   $officeweb="0;";
						   $i=1;
						   while($str_officeweb[$i]!=""){
								if($i==$pos){
									$officeweb=$officeweb.$postweb.";";
								} else {
									$officeweb=$officeweb.$str_officeweb[$i].";";
								}
								$i++;
						   }
							
							$inputwilayah=explode(';',$wilayah);
							$strwilayah=explode(';',$row_alumni->wilayah);
							$wilayahinstansi="";
							$i=0;
							while($strwilayah[$i]!=""){
								if($i==2*$pos-2){
									$wilayahinstansi=$wilayahinstansi.$inputwilayah[0].";";
								} elseif($i==2*$pos-1){
									$wilayahinstansi=$wilayahinstansi.$inputwilayah[1].";";
								} else {
									$wilayahinstansi=$wilayahinstansi.$strwilayah[$i].";";
								}
								$i++;
							}
							
							$datasql=array(
									'jabatan'=>$jabatan, 
									'instansi'=>$instansi, 
									'wilayah'=>$wilayahinstansi, 
									'bagian'=>$bagian, 
									'pangkat'=>$pangkat, 
									'kantor_alamat'=>$office, 
									'kantor_kota'=>$officecity3, 
									'kantor_prov'=>$officeprov, 
									'kantor_telp'=>$officephone,
									'fax'=>$officefax,
									'website'=>$officeweb
									);
							$this->db->where('id',$this->input->post('idmember'));
							$this->db->update('dataalumni',$datasql);
							
						}
					}
					
					if($views=="addmember2"){
						if($this->input->post('member-submit')){
							$nip=$this->input->post('nip_part1')." ".$this->input->post('nip_part2')." ".$this->input->post('nip_part3')." ".$this->input->post('nip_part4');
							
							// Office 1 untuk wilayah
							$officecity=$this->input->post('wil_kantor');
							echo "ini adalah ".$officecity;
							if(is_numeric($officecity)){
								$this->db->select('*');
								$this->db->from('city');
								$this->db->where('id',$officecity);
								$result=$this->db->get();
								$result=$result->result();
								foreach($result as $row_city){
									$city=$row_city->city;
								}
								if($this->input->post('daerahkantor')=="kota"){$wilayah="Kota;".$city.";"; }
								if($this->input->post('daerahkantor')=="kabupaten"){$wilayah="Kabupaten;".$city.";"; }
								if($this->input->post('daerahkantor')=="provinsi"){$wilayah="Provinsi;".$officecity.";"; }
								if($this->input->post('daerahkantor')=="pusat"){$wilayah="pusat;pusat;"; }
							} else {
								if($this->input->post('daerahkantor')=="kota"){$wilayah="Kota;".$officecity.";"; }
								if($this->input->post('daerahkantor')=="kabupaten"){$wilayah="Kabupaten;".$officecity.";"; }
								if($this->input->post('daerahkantor')=="provinsi"){$wilayah="Provinsi;".$officecity.";"; }
								if($this->input->post('daerahkantor')=="pusat"){$wilayah="pusat;pusat;"; }
							}
							
							if($_SESSION['member']=="new"){
								// 19830414
								// Dapatkan nama bulan
								if(substr($this->input->post('nip_part1'),-4,2)=="01"){ $birthmonth="January"; }
								if(substr($this->input->post('nip_part1'),-4,2)=="02"){ $birthmonth="February"; }
								if(substr($this->input->post('nip_part1'),-4,2)=="03"){ $birthmonth="March"; }
								if(substr($this->input->post('nip_part1'),-4,2)=="04"){ $birthmonth="April"; }
								if(substr($this->input->post('nip_part1'),-4,2)=="05"){ $birthmonth="May"; }
								if(substr($this->input->post('nip_part1'),-4,2)=="06"){ $birthmonth="June"; }
								if(substr($this->input->post('nip_part1'),-4,2)=="07"){ $birthmonth="July"; }
								if(substr($this->input->post('nip_part1'),-4,2)=="08"){ $birthmonth="August"; }
								if(substr($this->input->post('nip_part1'),-4,2)=="09"){ $birthmonth="September"; }
								if(substr($this->input->post('nip_part1'),-4,2)=="10"){ $birthmonth="October"; }
								if(substr($this->input->post('nip_part1'),-4,2)=="11"){ $birthmonth="November"; }
								if(substr($this->input->post('nip_part1'),-4,2)=="12"){ $birthmonth="December"; }
								$ttl=substr($this->input->post('nip_part1'),-2,2)." ".$birthmonth." ".substr($this->input->post('nip_part1'),0,4);
								//echo $ttl;
								
								if($this->input->post('bagian')==""){
									$bagian=" ";
								} else {
									$bagian=$this->input->post('bagian');
								}
								$datasql=array(
									'nama'=>$this->input->post('nama'), 
									'nip'=>$nip,
									'rumah_kota'=>"1000",
									'rumah_prov'=>"Tidak Ada Info",
									'rumah_telp'=>"-",
									'jabatan'=>"0;".$this->input->post('jabatan').";", 
									'instansi'=>"0;".$this->input->post('instansi').";", 
									'wilayah'=>$wilayah, 
									'bagian'=>"0;".$bagian.";", 
									'pangkat'=>"0;".$this->input->post('pangkat').";", 
									'kantor_alamat'=>"0;Tidak Ada Info;", 
									'kantor_kota'=>"0;1000;", 
									'kantor_prov'=>"0;Tidak Ada Info;", 
									'kantor_telp'=>"0;-;", 
									'fax'=>"0;-;", 
									'ttl'=>";".$ttl, 
									'diklat'=>"0;".$this->input->post('diklat').";", 
									'angkatan'=>"0;".$this->input->post('angkatan').";",
									'tahun'=>"0;".$this->input->post('tahun').";",
									'website'=>"0; ;");
								$this->db->set($datasql);
								$this->db->insert('dataalumni');
							}
							
							
							// Keperluan Update
							
							
							$this->db->select('*');
							$this->db->from('dataalumni');
							$this->db->where('nip',$nip);
							$result=$this->db->get();
							$result=$result->result();
							foreach($result as $row_alumni){
								$diklat_alumni=$row_alumni->diklat;
								$jabatan_alumni=$row_alumni->jabatan;
								$bagian_alumni=$row_alumni->bagian;
								$pangkat_alumni=$row_alumni->pangkat;
								$alamatkantor_alumni=$row_alumni->kantor_alamat;
								$kotakantor_alumni=$row_alumni->kantor_kota;
								$provkantor_alumni=$row_alumni->kantor_prov;
								$telpkantor_alumni=$row_alumni->kantor_telp;
								$faxkantor_alumni=$row_alumni->fax;
								$webkantor_alumni=$row_alumni->website;
							}
							
							$diklat_str=explode(";",$diklat_alumni);
							$i=1;
							while($diklat_str[$i]!=""){
								$i++;
							}
							$pos=$i-1;
							
							# Untuk alamat kantor
							$alamatkantor_str=explode(";",$alamatkantor_alumni);
							$addresoffice=$alamatkantor_str[$pos];
							
							# Untuk kota kantor
							$kotakantor_str=explode(";",$kotakantor_alumni);
							$officecity2=$kotakantor_str[$pos];
							
							# Untuk prov kantor
							$provkantor_str=explode(";",$provkantor_alumni);
							$officeprovx=$provkantor_str[$pos];
							
							# Untuk No Telp Kantor
							$telpkantor_str=explode(";",$telpkantor_alumni);
							$telp2=$telpkantor_str[$pos];
							
							# Untuk Fax Kantor
							$faxkantor_str=explode(";",$faxkantor_alumni);
							$fax=$faxkantor_str[$pos];
							
							# Untuk Website
							$webkantor_str=explode(";",$webkantor_alumni);
							$website=$webkantor_str[$pos];
							
							if($this->input->post('bagian')==""){
								$bagian=" ";
							} else {
								$bagian=$this->input->post('bagian');
							}
							
							if($_SESSION['member']=="update"){
								$datasql=array('nama'=>$this->input->post('nama'));
								$this->db->where('nip',$nip);
								$this->db->update('dataalumni',$datasql);
								
								$query=@mysql_query("UPDATE dataalumni SET jabatan=CONCAT(jabatan,'".$this->input->post('jabatan').";'), instansi=CONCAT(instansi,'".$this->input->post('instansi').";'), wilayah=CONCAT(wilayah,'".$wilayah."'), bagian=CONCAT(bagian,'".$bagian.";'), pangkat=CONCAT(pangkat,'".$this->input->post('pangkat').";'), kantor_alamat=CONCAT(kantor_alamat,'".$addresoffice.";'), kantor_kota=CONCAT(kantor_kota,'".$officecity2.";'), kantor_prov=CONCAT(kantor_prov,'".$officeprovx.";'), kantor_telp=CONCAT(kantor_telp,'".$telp2.";'), fax=CONCAT(fax,'".$fax.";'), diklat=CONCAT(diklat,'".$this->input->post('diklat').";'), angkatan=CONCAT(angkatan,'".$this->input->post('angkatan').";'), tahun=CONCAT(tahun,'".$this->input->post('tahun').";'), website=CONCAT(website,'".$website.";') WHERE nip='".$nip."'");
								
							}
							
 						}
					}
					
					if($views=="adddiklat"){
					
						if($this->input->post('act-submit')){
							$this->form_validation->set_rules('act-name', 'Nama Kegiatan Diklat', 'trim|required|xss_clean');
							$this->form_validation->set_rules('tipe', 'Tipe Kegiatan Diklat', 'trim|required|xss_clean');
							
							// Untuk menghindari karakter ' dan "
							$activity=str_replace("'","&rsquo;",$this->input->post('act-name'));
							$activity=str_replace('"',"&rdquo;",$activity);
							
							$data['actname']=$activity;
							$data['acttipe']=$this->input->post('tipe');							
							
							if($this->form_validation->run() == FALSE){
								$this->session->set_userdata('warning',validation_errors());
							} else {
								
								$this->db->select('*');
								$this->db->from('activity');
								$this->db->where('activity',$activity);
								$result=$this->db->get();
								$ncheck_name=$result->num_rows();
								
								if($ncheck_name!=0){
									$this->session->set_userdata('warning','<p>Peringatan! Nama Acara/Kegiatan Diklat yang Anda masukkan sudah ada, gunakan nama lain!</p>');
								} else {
									$syarat="0;";
									if($this->input->post('nsyarat')!=""){
										$i=1;
										while($i<=$this->input->post('nsyarat')){
											$syarat=$syarat.$this->input->post('prasyarat'.$i).";";
											$i++;
										}
									} else {
										$syarat=$syarat."0;";
									}
									
									
									
									$operator="0;";
									if($this->input->post('nsyarat')!=""){
										$i=1;
										while($i<$this->input->post('nsyarat')){
											$operator=$operator.$this->input->post('operator'.$i).";";
											$i++;
										}
									}
									
									$datasql=array(
										'activity'=>$activity, 
										'syarat'=>$syarat,
										'log'=>date("d-m-Y"),
										'tipe'=>$this->input->post('tipe'),
										'operator'=>$operator);
									$this->db->set($datasql);
									$this->db->insert('activity');
									
									echo"<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."siad/master/activity\" />";
									exit;
								}
							}
						}
					}
					
					if($views=="editdiklat"){
					
						if($this->input->post('act-editsubmit')){
							$this->form_validation->set_rules('act-name', 'Nama Kegiatan Diklat (Nama Baru)', 'trim|required|xss_clean');
							$this->form_validation->set_rules('tipe', 'Tipe Kegiatan Diklat', 'trim|required|xss_clean');
							
							// Untuk menghindari karakter ' dan "
							$activity=str_replace("'","&rsquo;",$this->input->post('act-name'));
							$activity=str_replace('"',"&rdquo;",$activity);
							
							$data['actname']=$activity;
							$data['acttipe']=$this->input->post('tipe');							
							
							if($this->form_validation->run() == FALSE){
								$this->session->set_userdata('warning',validation_errors());
							} else {
								
								$this->db->select('*');
								$this->db->from('activity');
								$this->db->where('activity',$activity);
								$result=$this->db->get();
								$ncheck_name=$result->num_rows();
								
								if($ncheck_name!=0){
									$this->session->set_userdata('warning','<p>Peringatan! Nama Acara/Kegiatan Diklat yang Anda masukkan sudah ada, gunakan nama lain!</p>');
								} else {
									$syarat="0;";
									if($this->input->post('nsyarat')!=""){
										$i=1;
										while($i<=$this->input->post('nsyarat')){
											$syarat=$syarat.$this->input->post('prasyarat'.$i).";";
											$i++;
										}
									} else {
										$syarat=$syarat."0;";
									}
									
									
									
									$operator="0;";
									if($this->input->post('nsyarat')!=""){
										$i=1;
										while($i<$this->input->post('nsyarat')){
											$operator=$operator.$this->input->post('operator'.$i).";";
											$i++;
										}
									}
									
									//$datasql=array('TitlePage'=>$titlepage, 'ContentPage'=>$this->input->post('contentpage'), 'Author'=>$data['nama'], 'RecordDate'=>date("Y-m-d"), 'Url'=>$url);
									//	$this->db->where('PageID',$this->input->post('postid'));
									//	$this->db->update('pages',$datasql);
									
									$datasql=array(
										'activity'=>$activity, 
										'syarat'=>$syarat,
										'log'=>date("d-m-Y"),
										'tipe'=>$this->input->post('tipe'),
										'operator'=>$operator);
									$this->db->where('id',$this->input->post('idact'));
									$this->db->update('activity',$datasql);
									
									$this->session->set_userdata('warning','<p>Data Berhasil di ubah</p>');
								}
							}
						}
					}
					
					if($views=="adddept"){
					
						if($this->input->post('dept-submit')){
							$this->form_validation->set_rules('dept-name', 'Nama Instansi', 'trim|required|xss_clean');
							
							// Untuk menghindari karakter ' dan "
							$dept=str_replace("'","&rsquo;",$this->input->post('dept-name'));
							$dept=str_replace('"',"&rdquo;",$dept);
							
							$data['deptname']=$dept;						
							
							if($this->form_validation->run() == FALSE){
								$this->session->set_userdata('warning',validation_errors());
							} else {
								
								$this->db->select('*');
								$this->db->from('dept');
								$this->db->where('dept',$dept);
								$result=$this->db->get();
								$ncheck_name=$result->num_rows();
								
								if($ncheck_name!=0){
									$this->session->set_userdata('warning','<p>Peringatan! Nama Instansi yang Anda masukkan sudah ada, gunakan nama lain!</p>');
								} else {
									
									$datasql=array(
										'dept'=>$dept, 
										'log'=>date("d-m-Y"));
									$this->db->set($datasql);
									$this->db->insert('dept');
									
									echo"<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."siad/master/departement\" />";
									exit;
								}
							}
						}
					}
					
					if($views=="editdept"){
					
						if($this->input->post('dept-editsubmit')){
							$this->form_validation->set_rules('dept-name', 'Nama Kegiatan Diklat (Nama Baru)', 'trim|required|xss_clean');
							
							// Untuk menghindari karakter ' dan "
							$dept=str_replace("'","&rsquo;",$this->input->post('dept-name'));
							$dept=str_replace('"',"&rdquo;",$dept);
							
							$data['deptname']=$dept;			
							
							if($this->form_validation->run() == FALSE){
								$this->session->set_userdata('warning',validation_errors());
							} else {
								
								$this->db->select('*');
								$this->db->from('dept');
								$this->db->where('dept',$dept);
								$result=$this->db->get();
								$ncheck_name=$result->num_rows();
								
								if($ncheck_name!=0){
									$this->session->set_userdata('warning','<p>Peringatan! Nama Instansi yang Anda masukkan sudah ada, gunakan nama lain!</p>');
								} else {
									
									$datasql=array(
										'dept'=>$dept, 
										'log'=>date("d-m-Y"));
									$this->db->where('id',$this->input->post('iddept'));
									$this->db->update('dept',$datasql);
									
									$this->session->set_userdata('warning','<p>Data Berhasil di ubah</p>');
								}
							}
						}
					}
					
					if($views=="addlecture"){
					
						if($this->input->post('lect-submit')){
							$this->form_validation->set_rules('lect-name', 'Nama Pengajar (Widiaiswara)', 'trim|required|xss_clean');
							
							// Untuk menghindari karakter ' dan "
							$lect=str_replace("'","&rsquo;",$this->input->post('lect-name'));
							$lect=str_replace('"',"&rdquo;",$lect);
							
							$data['lectname']=$lect;						
							
							if($this->form_validation->run() == FALSE){
								$this->session->set_userdata('warning',validation_errors());
							} else {
								
								$this->db->select('*');
								$this->db->from('lecture');
								$this->db->where('lecture',$lect);
								$result=$this->db->get();
								$ncheck_name=$result->num_rows();
								
								if($ncheck_name!=0){
									$this->session->set_userdata('warning','<p>Peringatan! Nama Pengajar (Widiaiswara) yang Anda masukkan sudah ada, gunakan nama lain!</p>');
								} else {
									
									$datasql=array(
										'lecture'=>$lect, 
										'log'=>date("d-m-Y"));
									$this->db->set($datasql);
									$this->db->insert('lecture');
									
									echo"<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."siad/master/lecture\" />";
									exit;
								}
							}
						}
					}
					
					if($views=="editlecture"){
					
						if($this->input->post('lect-editsubmit')){
							$this->form_validation->set_rules('lect-name', 'Nama Pengajar/Widiaiswara (Nama Baru)', 'trim|required|xss_clean');
							
							// Untuk menghindari karakter ' dan "
							$lect=str_replace("'","&rsquo;",$this->input->post('lect-name'));
							$lect=str_replace('"',"&rdquo;",$lect);
							
							$data['lectname']=$lect;			
							
							if($this->form_validation->run() == FALSE){
								$this->session->set_userdata('warning',validation_errors());
							} else {
								
								$this->db->select('*');
								$this->db->from('lecture');
								$this->db->where('lecture',$lect);
								$result=$this->db->get();
								$ncheck_name=$result->num_rows();
								
								if($ncheck_name!=0){
									$this->session->set_userdata('warning','<p>Peringatan! Nama Pengajar (Widiaiswara) yang Anda masukkan sudah ada, gunakan nama lain!</p>');
								} else {
									
									$datasql=array(
										'lecture'=>$lect, 
										'log'=>date("d-m-Y"));
									$this->db->where('id',$this->input->post('idlect'));
									$this->db->update('lecture',$datasql);
									
									$this->session->set_userdata('warning','<p>Data Berhasil di ubah</p>');
								}
							}
						}
					}
					
					if($views=="addposition"){
					
						if($this->input->post('position-submit')){
							$this->form_validation->set_rules('position-name', 'Nama Jabatan', 'trim|required|xss_clean');
							
							// Untuk menghindari karakter ' dan "
							$position=str_replace("'","&rsquo;",$this->input->post('position-name'));
							$position=str_replace('"',"&rdquo;",$position);
							
							$data['positionname']=$position;						
							
							if($this->form_validation->run() == FALSE){
								$this->session->set_userdata('warning',validation_errors());
							} else {
								
								$this->db->select('*');
								$this->db->from('position');
								$this->db->where('position',$position);
								$result=$this->db->get();
								$ncheck_name=$result->num_rows();
								
								if($ncheck_name!=0){
									$this->session->set_userdata('warning','<p>Peringatan! Nama Jabatan yang Anda masukkan sudah ada, gunakan nama lain!</p>');
								} else {
									
									$datasql=array(
										'position'=>$position, 
										'log'=>date("d-m-Y"));
									$this->db->set($datasql);
									$this->db->insert('position');
									
									echo"<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."siad/master/position\" />";
									exit;
								}
							}
						}
					}
					
					if($views=="editposition"){
					
						if($this->input->post('position-editsubmit')){
							$this->form_validation->set_rules('position-name', 'Nama Jabatan (Nama Baru)', 'trim|required|xss_clean');
							
							// Untuk menghindari karakter ' dan "
							$position=str_replace("'","&rsquo;",$this->input->post('position-name'));
							$position=str_replace('"',"&rdquo;",$position);
							
							$data['positionname']=$position;			
							
							if($this->form_validation->run() == FALSE){
								$this->session->set_userdata('warning',validation_errors());
							} else {
								
								$this->db->select('*');
								$this->db->from('position');
								$this->db->where('position',$position);
								$result=$this->db->get();
								$ncheck_name=$result->num_rows();
								
								if($ncheck_name!=0){
									$this->session->set_userdata('warning','<p>Peringatan! Nama Jabatan yang Anda masukkan sudah ada, gunakan nama lain!</p>');
								} else {
									
									$datasql=array(
										'position'=>$position, 
										'log'=>date("d-m-Y"));
									$this->db->where('id',$this->input->post('idposition'));
									$this->db->update('position',$datasql);
									
									$this->session->set_userdata('warning','<p>Data Berhasil di ubah</p>');
								}
							}
						}
					}
					
					if($views=="addgrade"){
					
						if($this->input->post('grade-submit')){
							$this->form_validation->set_rules('grade-name', 'Golongan/Pangkat', 'trim|required|xss_clean');
							
							// Untuk menghindari karakter ' dan "
							$grade=str_replace("'","&rsquo;",$this->input->post('grade-name'));
							$grade=str_replace('"',"&rdquo;",$grade);
							
							$data['gradename']=$grade;						
							
							if($this->form_validation->run() == FALSE){
								$this->session->set_userdata('warning',validation_errors());
							} else {
								
								$this->db->select('*');
								$this->db->from('grade');
								$this->db->where('grade',$grade);
								$result=$this->db->get();
								$ncheck_name=$result->num_rows();
								
								if($ncheck_name!=0){
									$this->session->set_userdata('warning','<p>Peringatan! Golongan/Pangkat yang Anda masukkan sudah ada, gunakan nama lain!</p>');
								} else {
									
									$datasql=array(
										'grade'=>$grade, 
										'log'=>date("d-m-Y"));
									$this->db->set($datasql);
									$this->db->insert('grade');
									
									echo"<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."siad/master/grade\" />";
									exit;
								}
							}
						}
					}
					
					if($views=="editgrade"){
					
						if($this->input->post('grade-editsubmit')){
							$this->form_validation->set_rules('grade-name', 'Golongan/Pangkat (Nama Baru)', 'trim|required|xss_clean');
							
							// Untuk menghindari karakter ' dan "
							$grade=str_replace("'","&rsquo;",$this->input->post('grade-name'));
							$grade=str_replace('"',"&rdquo;",$grade);
							
							$data['gradename']=$grade;			
							
							if($this->form_validation->run() == FALSE){
								$this->session->set_userdata('warning',validation_errors());
							} else {
								
								$this->db->select('*');
								$this->db->from('grade');
								$this->db->where('grade',$grade);
								$result=$this->db->get();
								$ncheck_name=$result->num_rows();
								
								if($ncheck_name!=0){
									$this->session->set_userdata('warning','<p>Peringatan! Golongan/Pangkat yang Anda masukkan sudah ada, gunakan nama lain!</p>');
								} else {
									
									$datasql=array(
										'grade'=>$grade, 
										'log'=>date("d-m-Y"));
									$this->db->where('id',$this->input->post('idgrade'));
									$this->db->update('grade',$datasql);
									
									$this->session->set_userdata('warning','<p>Data Berhasil di ubah</p>');
								}
							}
						}
					}
					
					if($views=="addcity"){
					
						if($this->input->post('city-submit')){
							
							if($this->input->post('prov')!="lainnya"){
								$prov=$this->input->post('prov');
							}
							if($this->input->post('prov-optional')!=""){
								$prov=$this->input->post('prov-optional');
							}
							
							if($this->input->post('tipe')=="provinsi"){
								$tipe="Provinsi";
							} 
							if($this->input->post('tipe')=="kota"){
								$tipe="Kota";
							} 
							if($this->input->post('tipe')=="kabupaten"){
								$tipe="Kabupaten";
							}
							
							$this->form_validation->set_rules('city-name', 'Nama Kota/Kabupaten', 'trim|required|xss_clean');
							if($this->input->post('prov')!="lainnya"){
								$this->form_validation->set_rules('prov', 'Provinsi', 'trim|required|xss_clean');
							}
							if($this->input->post('prov-optional')!=""){
								$this->form_validation->set_rules('prov-optional', 'Provinsi', 'trim|required|xss_clean');
							}
							
							
							
							
							// Untuk menghindari karakter ' dan "
							$city=str_replace("'","&rsquo;",$this->input->post('city-name'));
							$city=str_replace('"',"&rdquo;",$city);
							
							$data['cityname']=$city;						
							
							if($this->form_validation->run() == FALSE){
								$this->session->set_userdata('warning',validation_errors());
							} else {
								
								$this->db->select('*');
								$this->db->from('city');
								$this->db->where('city',$city);
								$result=$this->db->get();
								$ncheck_name=$result->num_rows();
								
								if($ncheck_name!=0){
									$this->session->set_userdata('warning','<p>Peringatan! Nama Kota/Kabupaten yang Anda masukkan sudah ada, gunakan nama lain!</p>');
								} else {
									if($this->input->post('tipe')==""){
										$this->session->set_userdata('warning','<p>Peringatan! Isi Wilayah Administrasi!</p>');
									} else {
										$datasql=array(
											'city'=>$city,
											'provinsi'=>$prov,
											'tipe'=>$tipe,
											'log'=>date("d-m-Y"));
										$this->db->set($datasql);
										$this->db->insert('city');
										
										echo"<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."siad/master/city\" />";
										exit;
									}
								}
							}
						}
					}
					
					if($views=="editcity"){
					
						if($this->input->post('city-editsubmit')){
							if($this->input->post('prov')!="lainnya"){
								$prov=$this->input->post('prov');
							}
							if($this->input->post('prov-optional')!=""){
								$prov=$this->input->post('prov-optional');
							}
							
							if($this->input->post('tipe')=="provinsi"){
								$tipe="Provinsi";
							} 
							if($this->input->post('tipe')=="kota"){
								$tipe="Kota";
							} 
							if($this->input->post('tipe')=="kabupaten"){
								$tipe="Kabupaten";
							}
							
							$this->form_validation->set_rules('city-name', 'Nama Kota/Kabupaten', 'trim|required|xss_clean');
							if($this->input->post('prov')!="lainnya"){
								$this->form_validation->set_rules('prov', 'Provinsi', 'trim|required|xss_clean');
							}
							if($this->input->post('prov-optional')!=""){
								$this->form_validation->set_rules('prov-optional', 'Provinsi', 'trim|required|xss_clean');
							}
							
							// Untuk menghindari karakter ' dan "
							$city=str_replace("'","&rsquo;",$this->input->post('city-name'));
							$city=str_replace('"',"&rdquo;",$city);
							
							$data['cityname']=$city;			
							
							if($this->form_validation->run() == FALSE){
								$this->session->set_userdata('warning',validation_errors());
							} else {
								
								$this->db->select('*');
								$this->db->from('city');
								$this->db->where('city',$city);
								$result=$this->db->get();
								$ncheck_name=$result->num_rows();
								
								if($ncheck_name!=0){
									$this->session->set_userdata('warning','<p>Peringatan! Nama Kota/Kabupaten yang Anda masukkan sudah ada, gunakan nama lain!</p>');
								} else {
									
									$datasql=array(
										'city'=>$city,
										'provinsi'=>$prov,
										'tipe'=>$tipe,
										'log'=>date("d-m-Y"));
									$this->db->where('id',$this->input->post('idcity'));
									$this->db->update('city',$datasql);
									
									$this->session->set_userdata('warning','<p>Data Berhasil di ubah</p>');
								}
							}
						}
					}
					
					if($views=="adduser"){
					
						if($this->input->post('user-submit')){
							$this->form_validation->set_rules('user-name', 'Nama Pengguna (User Name)', 'trim|required|xss_clean');
							$this->form_validation->set_rules('real-name', 'Nama Asli Pengguna (Real Name)', 'trim|required|xss_clean');
							$this->form_validation->set_rules('password', 'Password/Kata Kunci', 'trim|required|xss_clean');
							$this->form_validation->set_rules('password-repeat', 'Password/Kata Kunci Konfirmasi', 'trim|required|xss_clean');
							
							// Untuk menghindari karakter ' dan "
							$user=str_replace("'","&rsquo;",$this->input->post('user-name'));
							$user=str_replace('"',"&rdquo;",$user);
							
							$data['username']=$user;

							// Untuk menghindari karakter ' dan "
							$real=str_replace("'","&rsquo;",$this->input->post('real-name'));
							$real=str_replace('"',"&rdquo;",$real);
							
							$data['realname']=$real;							
							
							if($this->form_validation->run() == FALSE){
								$this->session->set_userdata('warning',validation_errors());
							} else {
								
								$this->db->select('*');
								$this->db->from('user');
								$this->db->where('user',$user);
								$result=$this->db->get();
								$ncheck_name=$result->num_rows();
								
								if($ncheck_name!=0){
									$this->session->set_userdata('warning','<p>Peringatan! User Name yang Anda masukkan sudah ada, gunakan nama lain!</p>');
								} else {
									
									if($this->input->post('password')!=$this->input->post('password-repeat')){
										$this->session->set_userdata('warning','<p>Peringatan! Password yang Anda masukkan tidak sesuai dengan Password Konfirmasi!</p>');
									} else {
										$datasql=array(
											'user'=>$user,
											'name'=>$real,
											'password'=>$this->input->post('password'),
											'log'=>date("d-m-Y"));
										$this->db->set($datasql);
										$this->db->insert('user');
										
										echo"<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."siad/master/user\" />";
										exit;
									}
								}
							}
						}
					}
					
					if($views=="adddocument"){
					
						if($this->input->post('doc-submit')){
							$this->form_validation->set_rules('doc-name', 'Nama Dokumen Terkait', 'trim|required|xss_clean');
							
							// Untuk menghindari karakter ' dan "
							$doc=str_replace("'","&rsquo;",$this->input->post('doc-name'));
							$doc=str_replace('"',"&rdquo;",$doc);
							
							$data['docname']=$doc;				
							
							if($this->form_validation->run() == FALSE){
								$this->session->set_userdata('warning',validation_errors());
							} else {
								
								if($_FILES['doc-file']['name']==""){
									$this->session->set_userdata('warning','<p>Peringatan! File Dokumen Terkait belum dipilih!</p>');
								} else {
									$this->db->select('*');
									$this->db->from('schedule');
									$this->db->where('activity',$doc);
									$result=$this->db->get();
									$ncheck_name=$result->num_rows();
									
									if($ncheck_name!=0){
										$this->session->set_userdata('warning','<p>Peringatan! Nama Dokumen Terkait yang Anda masukkan sudah ada, gunakan nama lain!</p>');
									} else {
										//echo $_FILES['doc-file']['name'][0];
										$this->db->select('*');
										$this->db->from('schedule');
										$this->db->where('filename',$_FILES['doc-file']['name'][0]);
										$result=$this->db->get();
										$ncheck_file=$result->num_rows();
										
										if($ncheck_file!=0){
											$this->session->set_userdata('warning','<p>Peringatan! File yang Anda masukkan sudah ada!</p>');
										} else {
											// masukkan dahulu file ke folder Media
											
												if ($_FILES['doc-file']['size'][0] > 0 && $_FILES['doc-file']['size'][0] <= 2097152) {
													
													$filename=$_FILES['doc-file']['name'];
													$upload_conf = array(
														'upload_path'   => realpath('public/media/'),
														'allowed_types' => 'xls|xlsx|doc|docx',
														'max_size'      => '5000',
														);
													$this->upload->initialize($upload_conf);
													foreach($_FILES['doc-file'] as $key=>$val){
														$i = 1;
														foreach($val as $v){
															$field_name = "file_".$i;
															$_FILES[$field_name][$key] = $v;
															$i++;   
														}
													}
													unset($_FILES['doc-file']);
													$error = "";
													$success = "";
													
													foreach($_FILES as $field_name => $file){
														if ( ! $this->upload->do_upload($field_name)){
															$error = $this->upload->display_errors('<p>','</p>');
														} else {
															// otherwise, put the upload datas here.
															// if you want to use database, put insert query in this loop
															$upload_data = $this->upload->data();
															
															$datasql=array(
																'activity'=>$doc,
																'filename'=>$upload_data['file_name'],
																'descriptions'=>$this->input->post('doc-desc'),
																'log'=>date("d-m-Y"));
															$this->db->set($datasql);
															$this->db->insert('schedule');
																					
														}
													}
													
													/* $datasql=array(
														'activity'=>$doc,
														'filename'=>$_FILES['doc-file']['name'],
														'descriptions'=>$this->input->post('password'),
														'log'=>date("d-m-Y"),
														'file'=>$data);
													$this->db->set($datasql);
													$this->db->insert('schedule'); */
												
													echo"<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."siad/document\" />";
													exit;
											
												} else {
													$this->session->set_userdata('warning','<p>Peringatan! Ukuran File dokumen yang Anda masukkan harus kurang dari 2 MB!</p>');
												}
									
										}
									}
								}
							}
						}
					}
					
					
					
					
					if(!$this->session->userdata('isLoggedIn')){
						$this->template->load('template-login','siad/'.$views);
					} else {
						if($views=="input" || $views=="diklat1" || $views=="adddiklat1" || $views=="addmember1" || $views=="list" || $views=="edit" || $views=="diklat2" || $views=="adddiklat2" || $views=="addmember2"){ $this->template->set('navigasi','input'); }
						if($views=="report" || $views=="results"){ $this->template->set('navigasi','report'); }
						if($views=="home"){ $this->template->set('navigasi','home'); }
						if($views=="master" || $views=="activity" || $views=="adddiklat" || $views=="editdiklat" || $views=="dept" || $views=="adddept" || $views=="editdept" || $views=="lecture" || $views=="addlecture" || $views=="editlecture"  || $views=="position" || $views=="addposition" || $views=="editposition" || $views=="user" || $views=="adduser" || $views=="edituser" || $views=="grade" || $views=="addgrade" || $views=="editgrade" || $views=="city" || $views=="addcity" || $views=="editcity"){ $this->template->set('navigasi','master'); }
						if($views=="memory"){ $this->template->set('navigasi','memory'); }
						if($views=="document" || $views=="adddocument"){ $this->template->set('navigasi','document'); }
						if($views=="statistics"){ $this->template->set('navigasi','statistics'); }
						$this->template->load('template-siad','siad/'.$views,$data);
					}
				}
				/* ========= Syntax yang berkaitan dengan SIAD ==== */
			}	
		} else {
				$baseurl="revise/";
				if($url1!="revise"){
				/* URL tidak ditemukan di database, tampilkan Error 404 */
					$views="error";
					$this->template->set('title','Error 404 | Ooops ... Halaman yang Anda cari tidak ditemukan');
					$this->template->load('template-error',$views);
				} else {
					// Nanti sesuaikan url ya!
					if($url2==""){
						$this->template->set('title','Pusat Pendidikan dan Pelatihan BKPM Republik Indonesia ~ Selamat Datang');
						$this->template->set('url','home');
						$views=$baseurl."site/home";
					} else {
						
					}
					$this->template->set('baseurl',$baseurl);
					$this->template->load($baseurl.'site',$views);
				}
		} 
		
		
		
		/* ========================= NEW CODE ================================ */
		
		
		if($url1=="cpanel2"){
			if($this->input->post('submit-login')){
			
				$data['username']=$this->input->post('username',true);
				$data['password']=$this->input->post('password',true);
										
				$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
				$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database2');
										
				if($this->form_validation->run() == FALSE){
					$this->session->set_userdata('warning',validation_errors());
				} else {
					//Bila sudah login tetapi coba akses menu login akan diredirect ke Dashboard
					echo"<meta http-equiv=\"refresh\" content=\"0;url=".base_url()."cpanel2/dashboard\" />";
					exit;
				}
			}
			
			if($this->session->userdata('isLoggedIn')==FALSE){
				$view="admin/login";
				$this->template->set('title','Login CPanel Website Pusdiklat BKPM');
				$this->load->view($view);
			} else {
				$css="
					<link rel=\"stylesheet\" type=\"text/css\" href=\"".base_url()."public/css/admin/adminstyle.css\" />
					<link rel=\"stylesheet\" type=\"text/css\" href=\"".base_url()."public/css/admin/utopia-white.css\" />
					<link rel=\"stylesheet\" type=\"text/css\" href=\"".base_url()."public/css/admin/utopia-responsive.css\" />";
				$js="
					<script type=\"text/javascript\" src=\"".base_url()."public/js/admin/admin_000.js\"></script>
					<script type=\"text/javascript\" src=\"".base_url()."public/js/admin/admin_001.js\"></script>
					<script type=\"text/javascript\" src=\"".base_url()."public/js/admin/admin_003.js\"></script>
	
					<script type=\"text/javascript\" src=\"".base_url()."public/js/admin/tinymce/tinymce.min.js\"></script>
						<script type=\"text/javascript\">
							tinymce.init({
								selector: \"textarea\",
								theme: \"modern\",
								plugins: [
									\"advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker\",
									\"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking\",
									\"table contextmenu directionality emoticons template textcolor paste textcolor filemanager\"
									],
								toolbar1: \"bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | formatselect fontselect fontsizeselect | image\",
								menubar: false,
								resize: false

							});
					</script>";
					
				$this->template->set('css',$css);
				$this->template->set('js',$js);
				$this->template->set('nav',$url2);
				$data=$this->session->userdata('isLoggedIn');
				$this->template->load('template-admin2',$view,$data);
			}
		}
	}
	
}
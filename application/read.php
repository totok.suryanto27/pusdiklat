<div id="container">
	<!-- Left Panel -->
	<div class="left-panel">
		<div id="tabBar">
			<div id="courseNav">
				<?php
					if($table!="categories"){
						// Cari ini masuk kategori apa
						$this->db->select('*');
						$this->db->from($table);
						if($table=="posts"){ $id="PostID"; }
						if($table=="pages"){ $id="PageID"; }
						if($table=="activity"){ $id="id"; }
						$this->db->where($id, $contentid);
						$query=$this->db->get();
						$query=$query->result();
						foreach($query as $row){
							if($table=="posts"){
								$catid=$row->CatID;
								$title=$row->TitlePost;
								$content=$row->ContentPost;
							}
							if($table=="pages"){
								$title=$row->TitlePage;
								$content=$row->ContentPage;
							}
							if($table=="activity"){
								$title=$row->activity;
								$content=
									"<div class=\"flexigrid\" style=\"width: 650px;\">
										<div class=\"mDiv\">
											<div class=\"ftitle\">Jadwal dan Waktu Pelaksanaan Diklat</div>
										</div>
										<div class=\"hDiv\">
											<div class=\"hDivBox\">
												<table cellspacing=\"0\" cellpadding=\"0\">
													<thead>
														<tr>
															<th class=\"\" align=\"center\" axis=\"col0\">
																<div style=\"text-align: center; width: 40px;\">Angk</div>
															</th>
															<th class=\"\" align=\"center\" axis=\"col1\">
																<div style=\"text-align: center; width: 140px;\">Tanggal<br />Pelatihan</div>
															</th>
															<th class=\"\" align=\"center\" axis=\"col2\">
																<div style=\"text-align: center; width: 50px;\">Kuota<br />Peserta</div>
															</th>
															<th align=\"center\" axis=\"col3\">
																<div style=\"text-align: center; width: 50px;\">Jumlah<br />Peserta</div>
															</th>
															<th align=\"center\" axis=\"col4\">
																<div style=\"text-align: center; width: 50px;\">Telah<br />Konfirmasi</div>
															</th>
															<th align=\"center\" axis=\"col4\">
																<div style=\"text-align: center; width: 85px;\">Daftar<br />Online</div>
															</th>
															<th align=\"center\" axis=\"col4\">
																<div style=\"text-align: center; width: 70px;\">Surat<br />Pemanggilan</div>
															</th>
															<th align=\"center\" axis=\"col4\">
																<div style=\"text-align: center; width: 65px;\">Batal</div>
															</th>
														</tr>
													</thead>
												</table>
											</div>
										</div>
										
										<div class=\"bDiv\" style=\"min-height: 50px;\">
											<table class=\"flexme4\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"\">
												<tbody>";
								$i=1;
								$j=1;
								$this->db->select('*');
								$this->db->from('jadwal');
								$this->db->where('DikID', $contentid);
								$this->db->order_by("ClassDik", "ASC"); 
								$query=$this->db->get();
								$query=$query->result();
								foreach($query as $row){
								
									if($url1!=""){
										$urllink=base_url().$url1;
									}
									if($url2!=""){
										$urllink=$urllink."/".$url2;
									}
									if($url3!=""){
										$urllink=$urllink."/".$url3;
									}
									
									$link3="<input type=\"button\" value=\"BATAL\" disabled/>";
									$link="<input type=\"button\" value=\"DITUTUP\" disabled/>";
									$link2="<img src=\"".base_url()."public/images/doc.png\" width=\"20px\" />";
									
									if($row->Status==1 && $row->StartDate>date("Y-m-d") && $row->Confirm<$row->Quota){
										$link="<input type=\"button\" onclick=\"document.location='".$urllink."/registration/?id=".$row->SchedID."'\" value=\"DAFTAR\"/>";
										$link2="<img src=\"".base_url()."public/images/doc.png\" width=\"20px\" onclick=\"document.location='".$urllink."/letter/?id=".$row->SchedID."'\" style=\"cursor: pointer\"/>";
										
									}
									
									$datenow=strtotime(date("Y-m-d"));
									$datestarting=strtotime($row->StartDate);
									
									if((($datestarting-$datenow)/3600/24)>4){
										$link3="<input type=\"button\" onclick=\"document.location='".$urllink."/cancel/?id=".$row->SchedID."'\" value=\"BATAL\"/>";
									}
									
									$date=explode('-',$row->StartDate);
									$startdate=$date[2];
									$date2=explode('-',$row->EndDate);
									$enddate=$date2[2];
									if($date[1]==$date2[1]){
										$timetodo=$startdate." s.d ".$enddate." ".date("M", mktime(0,0,0,$date2[1],1,$date2[0]))." ".$date2[0];
									} else {
										$timetodo=$startdate." ".date("M", mktime(0,0,0,$date[1],1,$date[0]))." ".$date[0]." s.d ".$enddate." ".date("M", mktime(0,0,0,$date2[1],1,$date2[0]))." ".$date2[0];
									}
									
									if($i==2*$j-1){
										$content=$content. 
											"<tr id=\"row".$i."\">
												<td align=\"center\"><div style=\"text-align: center; width: 40px;\">".$row->ClassDik."</div></td>
												<td align=\"center\"><div style=\"text-align: center; width: 140px;\">".$timetodo."</div></td>
												<td align=\"center\"><div style=\"text-align: center; width: 50px;\">".$row->Quota."</div></td>
												<td align=\"center\"><div style=\"text-align: center; width: 50px;\">".$row->Apply."</div></td>
												<td align=\"center\"><div style=\"text-align: center; width: 50px;\">".$row->Confirm."</div></td>
												<td align=\"center\"><div style=\"text-align: center; width: 85px;\">".$link."</div></td>
												<td align=\"center\"><div style=\"text-align: center; width: 70px;\">".$link2."</div></td>
												<td align=\"center\"><div style=\"text-align: center; width: 65px;\">".$link3."</div></td>
											</tr>";
										$j++;
									} else {
										$content=$content.
											"<tr id=\"row".$i."\" class=\"erow\">
												<td align=\"center\"><div style=\"text-align: center; width: 40px;\">".$row->ClassDik."</div></td>
												<td align=\"center\"><div style=\"text-align: center; width: 140px;\">".$timetodo."</div></td>
												<td align=\"center\"><div style=\"text-align: center; width: 50px;\">".$row->Quota."</div></td>
												<td align=\"center\"><div style=\"text-align: center; width: 50px;\">".$row->Apply."</div></td>
												<td align=\"center\"><div style=\"text-align: center; width: 50px;\">".$row->Confirm."</div></td>
												<td align=\"center\"><div style=\"text-align: center; width: 85px;\">".$link."</div></td>
												<td align=\"center\"><div style=\"text-align: center; width: 70px;\">".$link2."</div></td>
												<td align=\"center\"><div style=\"text-align: center; width: 65px;\">".$link3."</div></td>
											</tr>";
									}
									$i++;
								}

								$content=$content.					
									"			</tbody>
											</table>
											<div class=\"iDiv\" style=\"display: none;\"></div>
										</div>
									
									
									</div>
									
									<div class=\"warning\" style=\"color: red;\">
										<h2>PERHATIAN!</h2>
										<p>MOHON PERHATIKAN JADWAL DAN WAKTU PELAKSANAAN DIKLAT. APA BILA TELAH MENDAFTAR DAN TIDAK HADIR PADA DIKLAT YANG DIPILIH SERTA TIDAK MEMBERIKAN PEMBATALAN, MAKA ANDA TIDAK DIPRIORITASKAN DAPAT MENGIKUTI DIKLAT DI TAHUN YANG SAMA.</p>
										<p>JIKA TELAH BERHASIL MELAKUKAN PENDAFTARAN AKAN MELAKUKAN PEMBATALAN, MAKA KLIK TOMBOL <strong>BATAL</strong> KEMUDIAN ISIKAN NIP ANDA. LINK PEMBATALAN AKAN DIKIRIMKAN VIA EMAIL. DAN JIKA AKAN PINDAH KE ANGKATAN YANG LAIN, MAKA LAKUKAN PEMBATALAN TERLEBIH DAHULU KEMUDIAN PROSES PENDAFTARAN KEMBALI. PROSES PEMBATALAN DIPERBOLEHKAN DALAM WAKTU SEBELUM 4 HARI WAKTU PELAKSANAAN DIKLAT.</p>
										<p>PENDAFTARAN AKAN DITUTUP APABILA JUMLAH PESERTA YANG TELAH KONFIRMASI SESUAI DENGAN KUOTA DAN ATAU MELEBIHI TANGGAL PELAKSANAAN DIKLAT</p>
										<p>PESERTA YANG DIPERBOLEHKAN MENGIKUTI DIKLAT ADALAH PESERTA YANG TELAH MELAKUKAN KONFIRMASI VIA EMAIL</p>
										<p>JADWAL DAPAT BERUBAH SETIAP SAAT, MOHON DIMONITOR MELALUI WEBSITE INI DAN FACEBOOK PUSDIKLAT BKPM</p>
									</div>";
							}
						}
						
						if($table=="posts"){
							// Cari nama kategorinya
							$this->db->select('*');
							$this->db->from('categories');
							$this->db->where('CatID', $catid);
							$query=$this->db->get();
							$query=$query->result();
							foreach($query as $row){
								$title=$row->Categories;
							}
						}
					} else {
						$this->db->select('*');
						$this->db->from('categories');
						$this->db->where('CatID', $contentid);
						$query=$this->db->get();
						$query=$query->result();
						foreach($query as $row){
							$title=$row->Categories;
						}
						
						$content="<table id=\"results1\">";
						
						$this->db->select('*');
						$this->db->from('posts');
						$this->db->where('CatID',$contentid);
						$this->db->order_by("postid","desc");
						$this->db->limit(15);
						$query1=$this->db->get();
						$result1=$query1->result();
						foreach($result1 as $row1){
							$content=$content.
								"<tr><td><div class=\"courseTitle\" style=\"padding: 0; margin-bottom: 10px;\">".$row1->TitlePost."</div>".trim(character_limiter($row1->ContentPost,200))."<br /><a href=\"".base_url()."read/".$row1->Url."\">Selanjutnya</a><div style=\"padding-top: 20px; clear: both;\"></div></td></tr>";
							
						}
						$content=$content.
							"</table>
							<div id=\"pageNavPosition1\" style=\"float: left\"></div>
							<div clear=\"both\"></div>

							<script type=\"text/javascript\">
								var pager1 = new Pager('results1', 3);
								pager1.init();
								pager1.showPageNav('pager1', 'pageNavPosition1');
								pager1.showPage(1);
							</script>";
						
					}
				?>
				<div class="inactive2Tab" id="courseNav1" ><?php echo $title; ?></div>
				<div class="myclear"></div>
				
				<div id="courseNav1_area" class="courseContentSmall off" style="min-height: 300px;">
					<div class="courseTitle" style="padding: 0; margin-bottom: 10px; border-bottom: 2px solid blue; padding-bottom: 15px;"><?php echo $title; ?></div>
					<?php echo $content; ?>
				</div>
				
			</div>
		</div>
	</div>
	
	<!-- Right Panel -->
	<div class="right-panel">
		<div class="certColumn">
			<div class="certBox">
				<h3>Link Eksternal</h3>
                    <div class="certBoxContent">
                        <div style="padding: 10px; font-size: 90%;">
							<ul>
								
								<?php
									$this->db->select('*');
									$this->db->from('extlink');
									$this->db->order_by("ExtID","desc");
									$this->db->limit(10);
									$query=$this->db->get();
									$result=$query->result();
									foreach($result as $row){
										echo"<li><a href=\"http://".$row->ExtLink."\" target=\"_blank\">".$row->ExtLink."</a></li>";
									}
								?>
								
							</ul>
						</div>
						<div class="myclear"></div>
                    </div>
                    <div class="certBoxFooter"></div>
			</div>
        </div>
		
		<div class="certColumn">
			<div class="certBox">
				<h3>Ikuti Kami ...</h3>
					<div class="certBoxContent">
						<div style="padding: 10px; font-size: 90%;" align="center">
							<?php
								$this->db->select('*');
								$this->db->from('sosmed');
								$query=$this->db->get();
								$result=$query->result();
								foreach($result as $row){
									if($row->Url!=""){
										if($row->SosMed=="Facebook"){ $img="fb-icon.png"; }
										if($row->SosMed=="Twitter"){ $img="tw-icon.png"; }
										if($row->SosMed=="Google Plus"){ $img="gp-icon.png"; }
										if($row->SosMed=="Instagram"){ $img="insta-icon.png"; }
										echo
											"<a href=\"".$row->Url."\" title=\"Pusdiklat BKPM di ".$row->SosMed."\"	target=\"_blank\">
												<img src=\"".base_url()."public/images/".$img."\" /></a>";
									}
								}
							?>
						</div>
						<div class="myclear"></div>
					</div>
					<div class="certBoxFooter"></div>
			</div>
		</div>
		
		<div class="certColumn">
			<div class="certBox">
				<h3>Galeri Foto & Video</h3>
					<div class="certBoxContent">
						<div style="padding: 10px; font-size: 90%;" align="center">
							<a href="<?php echo base_url(); ?>galeri-foto" title="Galeri Foto Kegiatan Pusdiklat BKPM"><img src="<?php echo base_url(); ?>public/images/pg.png" width="100px" /></a>
							<a href="<?php echo base_url(); ?>galeri-video" title="Galeri Video Kegiatan Pusdiklat BKPM"><img src="<?php echo base_url(); ?>public/images/vg.png" width="100px" /></a>
						</div>
						<div class="myclear"></div>
					</div>
					<div class="certBoxFooter"></div>
			</div>
		</div>
	</div>
	
</div>